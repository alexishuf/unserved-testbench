package br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service;

import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.Condition;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.lazy.EquivalenceSets;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.lazy.SetNode;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.discovery.NodeOutput;
import br.ufsc.lapesd.unserved.testbench.model.Variable;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.util.*;
import java.util.stream.Stream;

/**
 * A set of nodes whose outputs can be used to obtain values for a set of variables.
 *
 * The mapping between the Nodes outputs and the variables to be assigned is represented by
 * a Copies object.
 */
@Immutable
public interface ProviderSet extends Iterable<Node> {
    @Nonnull
    Stream<Node> stream();
    boolean contains(Node node);

    /**
     * A Provider Set is satisfied iff all target variables have at least one provider node.
     *
     * @return true iff the ProviderSet is satisfied.
     */
    default boolean isSatisfied() { return getUnsatisfiedTargets().isEmpty(); }

    /**
     * Verifies if the given set of provider nodes satisfy all preconditions.
     * @param providers set of providers
     *
     * @return true iff all preconditions are satisfied, false otherwise.
     */
    boolean satisfyPreconditions(Collection<Node> providers);

    class PrecondMap {
        public final @Nonnull Map<Variable, NodeOutput> inMap;
        public final @Nonnull Map<Condition, NodePostCond> preCondMap;

        public PrecondMap(@Nonnull Map<Variable, NodeOutput> inMap,
                          @Nonnull Map<Condition, NodePostCond> preCondMap) {
            this.inMap = inMap;
            this.preCondMap = preCondMap;
        }

        @SuppressWarnings("CopyConstructorMissesField")
        public PrecondMap(PrecondMap other) {
            this(new HashMap<>(other.inMap), new HashMap<>(other.preCondMap));
        }

        public Set<Node> getNodes() {
            Set<Node> nodes = new HashSet<>();
            inMap.values().forEach(no -> nodes.add(no.getNode()));
            preCondMap.values().forEach(no -> nodes.add(no.getNode()));
            return nodes;
        }
    }

    /**
     * Gets a stream of possible mappings between provider outputs and precondition variables
     * (inputs) that imply in satisfying the preconditions wholly. If the preconditions are not
     * satisfiable, an empty stream is returned. the stream is not ordered and is not distinct.
     * @param successorEqSets
     */
    @Nonnull
    Stream<PrecondMap> getPreconditionMappings(EquivalenceSets successorEqSets);

    /**
     * Uses the provided mappings to build equivalence sets. This will show which variables MUST
     * be equal and which MUST NOT be. Not necessarily all variables are added to an equivalence
     * set (i.e., there is no restriction to them being equal or not being equal to any other
     * variable).
     */
    @Nonnull
    EquivalenceSets getEquivalenceSets(@Nonnull PrecondMap map,
                                       @Nullable EquivalenceSets parent);

    @Nonnull
    Set<Variable> getTargets();

    @Nonnull Set<Condition> getConditions();

    /**
     * An unsatisfied target is a target for which there are no providers.
     *
     * @return Set of unsatisfied targets of this instance.
     */
    @Nonnull
    Set<Variable> getUnsatisfiedTargets();

    /**
     * An unsatisfied condition is one for which therea rea no providers.
     *
     * @return Possibly empty set of unsatified {@link Condition} instances.
     */
    @Nonnull
    Set<Condition> getUnsatisfiedConditions();

    /**
     *
     * @param target Target variable.
     * @return (possibly empty) set of Nodes with outputs that can be assigned to target.
     */
    @Nonnull
    Set<Node> getProviders(@Nonnull Variable target);
    /**
     *
     * @param target Target Variable to be assigned from a node's output
     * @param provider A provider node for target
     * @return A output of node that can be assigned to target.
     * @throws NoSuchElementException if <code>!getProviders(target).contains(node)</code>.
     */
    @Nonnull
    Variable getOutput(@Nonnull Variable target, @Nonnull Node provider);

    Set<Variable> getOutputs(@Nonnull Variable target, @Nonnull Node provider);

    @Nonnull
    Set<Node> getProviders(@Nonnull Condition condition);

    @Nonnull
    Condition getPostcondition(@Nonnull Condition precondition, @Nonnull Node provider);

    @Nonnull
    Set<Condition> getPostconditions(@Nonnull Condition precondition, @Nonnull Node provider);

    void bindConstants(@Nonnull Map<Variable, NodeOutput> fullMap,
                       SetNode successor, @Nonnull EquivalenceSets equivalenceSets,
                       @Nonnull Set<IndexedNode> nodes);

    class FilterMap {
        public @Nullable Variable mapTarget(@Nonnull Variable target) {return target;}
        public @Nullable Assignment mapAssignment(@Nonnull Assignment a) {return a;}
    }

    ProviderSet without(@Nonnull IOProviderHashSet.FilterMap f);

    @Nonnull
    Stream<Assignment> getAssignmentsStream();

    final class Assignment {
        public @Nonnull Variable target;
        public @Nonnull Node node;
        public @Nonnull Variable output;

        public Assignment(@Nonnull Variable target, @Nonnull Node node, @Nonnull Variable output) {
            this.target = target;
            this.node = node;
            this.output = output;
        }

        @Nonnull
        public Variable getTarget() {
            return target;
        }

        @Nonnull
        public Node getNode() {
            return node;
        }

        @Nonnull
        public Variable getOutput() {
            return output;
        }

        @Override
        public String toString() {
            return String.format("%s(%s) -> %s", node, output, target);
        }
    }
}

package br.ufsc.lapesd.unserved.testbench.io_composer;

import org.apache.jena.rdf.model.Resource;

import javax.annotation.Nonnull;
import java.util.Collection;

/**
 * The result from executing an Composition
 * //TODO Composition actions still do not deliver results to the Interpreter
 */
public interface IOCompositionResult extends AutoCloseable {
    Collection<Resource> getInputWantedVariables();
    Resource getWantedVariable(Resource inputResource);

    @Override
    void close();

    @Nonnull IOComposerTimes getTimes();
}

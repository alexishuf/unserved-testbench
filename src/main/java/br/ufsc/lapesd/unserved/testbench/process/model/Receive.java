package br.ufsc.lapesd.unserved.testbench.process.model;

import br.ufsc.lapesd.unserved.testbench.model.Message;

/**
 * Enhanced node interface for unserved-p:Receive
 */
public interface Receive extends Action {
    Message getMessage();
    int getInvocationIndex();
}

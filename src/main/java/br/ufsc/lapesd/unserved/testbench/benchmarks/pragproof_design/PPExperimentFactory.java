package br.ufsc.lapesd.unserved.testbench.benchmarks.pragproof_design;

import br.ufsc.lapesd.unserved.testbench.AlgorithmFamily;
import br.ufsc.lapesd.unserved.testbench.benchmarks.experiment.Experiment;
import br.ufsc.lapesd.unserved.testbench.benchmarks.experiment.ExperimentFactory;
import br.ufsc.lapesd.unserved.testbench.benchmarks.experiment.Factors;

public class PPExperimentFactory implements ExperimentFactory {

    @Override
    public Experiment createExperiment(Factors factors) {
        PPFactors f = (PPFactors) factors;
        if (f.getAlgorithm().getFamily() == AlgorithmFamily.RESTdescPP) {
            return new RESTdescPPExperiment(f);
        } else if (f.getAlgorithm().getFamily() == AlgorithmFamily.IOGraphPath) {
            return new ComposeWorkflowAppExperiment(f);
        }
        throw new UnsupportedOperationException();
    }
}

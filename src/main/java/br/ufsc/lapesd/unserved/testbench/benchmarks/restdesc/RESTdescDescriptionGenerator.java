package br.ufsc.lapesd.unserved.testbench.benchmarks.restdesc;

import com.google.common.base.Preconditions;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;

public class RESTdescDescriptionGenerator {
    @Option(name = "--ex-iri")
    private String exIRI = "http://example.org/#";

    @Option(name = "--chain-length", required = true)
    private int chainLength;

    @Option(name = "--alternatives")
    private int alternatives = 1;

    @Option(name = "--conditions")
    private int conditions = 1;

    @Option(name = "--rel")
    private String rel = "rel";

    @Option(name = "--out")
    private File out;

    @Option(name = "--initial-out")
    private File initialOut;

    @Option(name = "--goal-out")
    private File goalOut;

    @Option(name = "--use-existentials", usage = "Enforce that the consequent of a rule has " +
            "only existential variables. Verborgh original benchmark used universals for " +
            "expressing post-conditions.")
    private boolean useExistentials = false;

    private int nextIndexValue = 0;

    public static class Builder {
        private RESTdescDescriptionGenerator g = new RESTdescDescriptionGenerator();

        public Builder setChainLength(int chainLength) {
            Preconditions.checkState(g != null);
            g.chainLength = chainLength;
            return this;
        }

        public Builder setAlternatives(int alternatives) {
            Preconditions.checkState(g != null);
            g.alternatives = alternatives;
            return this;
        }

        public Builder setConditions(int conditions) {
            Preconditions.checkState(g != null);
            g.conditions = conditions;
            return this;
        }

        public Builder setRel(String rel) {
            Preconditions.checkState(g != null);
            g.rel = rel;
            return this;
        }

        public Builder setUseExistentials(boolean useExistentials) {
            Preconditions.checkState(g != null);
            g.useExistentials = useExistentials;
            return this;
        }

        public Builder setOut(File out) {
            Preconditions.checkState(g != null);
            g.out = out;
            return this;
        }

        public Builder setInitialOut(File file) {
            Preconditions.checkState(g != null);
            g.initialOut = file;
            return this;
        }

        public Builder setGoalOut(File file) {
            Preconditions.checkState(g != null);
            g.goalOut = file;
            return this;
        }

        public RESTdescDescriptionGenerator build() {
            Preconditions.checkState(g != null);
            RESTdescDescriptionGenerator result = this.g;
            g = null;
            return result;
        }
    }

    public static void main(String[] args) throws Exception {
        RESTdescDescriptionGenerator generator = new RESTdescDescriptionGenerator();
        new CmdLineParser(generator).parseArgument(args);
        generator.run();
    }

    public void run() throws Exception {
        lateCheckOptions();
        if (out != null) writeDescriptions();
        if (initialOut != null) writeInitial();
        if (goalOut != null) writeGoal();
    }

    private void writeDescriptions() throws IOException {
        try (FileOutputStream outFileStream = new FileOutputStream(this.out);
             PrintStream out = new PrintStream(outFileStream)) {
            out.printf("@prefix ex: <" + exIRI + ">.\n" +
                    "@prefix http: <http://www.w3.org/2011/http#>.\n" +
//                    "@prefix log: <https://www.w3.org/2000/10/swap/log#>.\n" +
                    "\n");
            generateDescriptionNode(out, 1, 2, 1, conditions, chainLength == 1);
            nextIndexValue = 3;
            generateDescriptionTree(out, 2, 2);
        }
    }

    private void lateCheckOptions() throws Exception {
        if (out == null && initialOut == null && goalOut == null) {
            throw new Exception("At least one of --out, --goal-out and --initial-out must " +
                    "be provided");
        }
    }

    private void writeInitial() throws IOException {
        try (FileOutputStream fileStream = new FileOutputStream(this.initialOut);
             PrintStream out = new PrintStream(fileStream)) {
            out.printf("@prefix ex: <%s>.\n\nex:x ex:%s1 ex:y.\n", exIRI, rel);
        }
    }

    private void writeGoal() throws IOException {
        try (FileOutputStream fileStream = new FileOutputStream(this.goalOut);
             PrintStream out = new PrintStream(fileStream)) {
            if (!useExistentials) {
                out.printf("@prefix ex: <%s>.\n\n" +
                        "{\n  ex:x ex:relGoal ex:y.\n}\n=>\n" +
                        "{\n  ex:x ex:relGoal ex:y.\n}.\n", exIRI);
            } else {
                out.printf("@prefix ex: <%s>.\n\n" +
                        "{ex:x ex:relGoal ?y.} => {ex:x ex:relGoal ?y.}.\n", exIRI);
            }
        }
    }

    private int nextIndex() {
        return nextIndexValue++;
    }

    private void generateDescriptionTree(PrintStream out, int previousIndex, int pathIndex) {
        Preconditions.checkArgument(pathIndex <= chainLength);

        int index = nextIndex();
        boolean isLast = pathIndex == chainLength;
        generateDescriptionNode(out, previousIndex, index, conditions, conditions, isLast);
        for (int i = 0; !isLast && i < alternatives; i++)
            generateDescriptionTree(out, index, pathIndex+1);
    }

    private void generateDescriptionNode(PrintStream out, int previousIndex, int index,
                                         int inConditions, int outConditions, boolean withGoal) {
        if (useExistentials) {
            generateDescriptionNodeExistentials(out, previousIndex, index, inConditions, outConditions, withGoal);
        } else{
            generateDescriptionNodeUniversals(out, previousIndex, index, inConditions, outConditions, withGoal);
        }
    }

    private void generateDescriptionNodeUniversals(PrintStream out, int previousIndex, int index,
                                                   int inConditions, int outConditions,
                                                   boolean withGoal) {
        out.printf("{\n");
        for (int i = 1; i <= inConditions; i++)
            out.printf("  ?a%d ex:%s%d ?b%d.\n", i, rel, previousIndex, i);
        out.printf("}\n=>\n{\n");
        out.printf("  _:request http:methodName \"GET\";\n");
        out.printf("            http:requestURI ?a1;\n");
        out.printf("            http:resp [ http:body ?b1 ].\n");
        for (int i = 1; i <= outConditions; i++)
            out.printf("  ?a%d ex:%s%d ?b%d.\n", i, rel, index, i);
        if (withGoal)
            out.printf("  ?a1 ex:relGoal ?b1.\n");
        out.printf("}.\n\n");
    }

    private void generateDescriptionNodeExistentials(PrintStream out, int previousIndex, int index,
                                                     int inConditions, int outConditions,
                                                     boolean withGoal) {
        out.printf("{\n");
        for (int i = 1; i <= inConditions; i++)
            out.printf("  ?a%d ex:%s%d ?b%d.\n", i, rel, previousIndex, i);
        out.printf("} => {\n");
        out.printf("  _:request http:methodName \"GET\";\n");
        out.printf("            http:requestURI ?a1;\n");
        out.printf("            http:resp [ http:body _:e1 ].\n");
        if (outConditions > 0)
            out.printf("  ?a1 ex:%s%d _:f1.\n", rel, index);
        for (int i = 2; i <= outConditions; i++)
            out.printf("  _:e%d ex:%s%d _:f%d.\n", i, rel, index, i);
        if (withGoal)
            out.printf("  ?a1 ex:relGoal _:f%d.\n", outConditions+1);
        out.printf("}.\n\n");
    }
}

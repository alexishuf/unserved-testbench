package br.ufsc.lapesd.unserved.testbench.benchmarks.design1;

import br.ufsc.lapesd.unserved.testbench.benchmarks.compose.TimesExperimentResult;
import br.ufsc.lapesd.unserved.testbench.benchmarks.design1.data.D1ExperimentData;
import br.ufsc.lapesd.unserved.testbench.benchmarks.design1.data.DescriptionsData;
import br.ufsc.lapesd.unserved.testbench.benchmarks.experiment.Experiment;
import br.ufsc.lapesd.unserved.testbench.benchmarks.experiment.ExperimentException;
import br.ufsc.lapesd.unserved.testbench.benchmarks.experiment.ExperimentResult;
import br.ufsc.lapesd.unserved.testbench.input.RDFInputFile;
import com.google.gson.Gson;
import org.apache.commons.io.FileUtils;
import org.apache.jena.riot.RDFFormat;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public abstract class BasicD1Experiment implements Experiment {
    private D1Factors factors;
    private D1ExperimentData experimentData;
    protected DescriptionsData descriptionsData;
    private File descriptions;
    private File taxonomy;

    public BasicD1Experiment(D1ExperimentData experimentData, D1Factors factors) {
        this.factors = factors;
        this.experimentData = experimentData;
    }

    @Override
    public void setUp() throws Exception {
        this.descriptionsData = experimentData.getDescriptions(getFactors());
        this.descriptions = descriptionsData.getDescriptionsFile();
        this.taxonomy = descriptionsData.getTaxonomyFile();
    }

    @Override
    public ExperimentResult run() throws ExperimentException {
        try {
            File workflowFile = Files.createTempFile("workflow", ".ttl").toFile();
            workflowFile.deleteOnExit();
            File timesFile = Files.createTempFile("times", ".json").toFile();
            timesFile.deleteOnExit();

            List<File> fileList = new ArrayList<>();
            if (taxonomy != null)
                fileList.add(taxonomy);
            fileList.add(descriptions);
            run(workflowFile, timesFile, fileList);

            try (RDFInputFile input = RDFInputFile.createOwningFile(workflowFile, RDFFormat.TURTLE)) {
                if (!factors.getExecute() && input.getModel().isEmpty())
                    throw new ExperimentException("Empty workflow model!");
            }

            ExperimentResult result;
            result = new Gson().fromJson(new FileReader(timesFile), TimesExperimentResult.class);
            FileUtils.forceDelete(timesFile);
            return result;
        } catch (IOException e) {
            throw new ExperimentException(e);
        }
    }

    @Override
    public D1Factors getFactors() {
        return factors;
    }

    @Override
    public void close() throws Exception {
    }

    protected void runChildJVM(List<String> mainArgs, List<File> descriptions)
            throws ExperimentException {
        String separator = System.getProperty("file.separator");
        String classpath = System.getProperty("java.class.path");
        String java = System.getProperty("java.home")
                + separator + "bin" + separator + "java";

        List<String> args = new ArrayList<>(Arrays.asList(java, "-cp", classpath
//                , "-agentlib:jdwp=transport=dt_socket,server=y,address=5005"
        ));
        args.addAll(mainArgs);
        for (File d : descriptions) args.add(d.getAbsolutePath());

        try {
            Process child = new ProcessBuilder().command(args)
                    .redirectError(ProcessBuilder.Redirect.INHERIT)
                    .redirectOutput(ProcessBuilder.Redirect.INHERIT)
                    .start();
            int exitCode = child.waitFor();
            if (exitCode != 0)
                throw new ExperimentException("Child did not executed correctly.");
        } catch (IOException | InterruptedException e) {
            throw new ExperimentException("Process management exception", e);
        }
    }

    protected abstract void run(File workflowFile, File timesFile, List<File> fileList)
            throws ExperimentException;
}

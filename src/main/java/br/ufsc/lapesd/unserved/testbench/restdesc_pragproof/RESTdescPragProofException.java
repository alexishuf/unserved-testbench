package br.ufsc.lapesd.unserved.testbench.restdesc_pragproof;

public class RESTdescPragProofException extends Exception {
    public RESTdescPragProofException(String s) {
        super(s);
    }

    public RESTdescPragProofException(String s, Throwable throwable) {
        super(s, throwable);
    }
}

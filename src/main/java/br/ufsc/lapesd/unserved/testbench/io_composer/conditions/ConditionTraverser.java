package br.ufsc.lapesd.unserved.testbench.io_composer.conditions;

import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.atomic.TriplePattern;
import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.atomic.VariableSpec;

import javax.annotation.Nonnull;
import java.util.Collections;
import java.util.List;

public class ConditionTraverser {
    public static void visitInOrder(@Nonnull Condition condition,
                                      @Nonnull ConditionVisitor visitor) {
        List<Condition> children = getChildren(condition);
        if (!children.isEmpty())
            visitInOrder(children.get(0), visitor);
        visit(condition, visitor);
        children.stream().skip(1).forEach(c -> visitInOrder(c, visitor));
    }

    public static void visitPreOrder(@Nonnull Condition condition,
                                       @Nonnull ConditionVisitor visitor) {
        List<Condition> children = getChildren(condition);
        visit(condition, visitor);
        children.forEach(c -> visitPreOrder(c, visitor));
    }

    public static void visitPostOrder(@Nonnull Condition condition,
                                        @Nonnull ConditionVisitor visitor) {
        List<Condition> children = getChildren(condition);
        children.forEach(c -> visitPostOrder(c, visitor));
        visit(condition, visitor);
    }

    public static List<Condition> getChildren(Condition condition) {
        if (!(condition instanceof ConditionList)) return Collections.emptyList();
        return ((ConditionList)condition).getMembers();
    }

    public static boolean visit(@Nonnull Condition c,
                               @Nonnull ConditionVisitor v) {
        return v.visit(c.getClass(), c)
                || c instanceof VariableSpec && v.visitVariableSpec((VariableSpec) c)
                || c instanceof TriplePattern && v.visitTriplePattern((TriplePattern) c)
                || c instanceof AtomicCondition && v.visitAtomicCondition((AtomicCondition) c)
                || c instanceof Or && v.visitOr((Or) c)
                || c instanceof And && v.visitAnd((And) c)
                || c instanceof ConditionList && v.visitConditionList((ConditionList) c)
                || v.visitCondition(c);

    }
}

package br.ufsc.lapesd.unserved.testbench.components.expressions;

import org.apache.jena.rdf.model.Resource;

import javax.annotation.Nonnull;
import java.util.Set;

abstract class AbstractClassExpression implements ClassExpression {
    @Nonnull private final Type type;
    @Nonnull private final Set<Resource> names;

    protected AbstractClassExpression(@Nonnull Type type, @Nonnull Set<Resource> names) {
        this.type = type;
        this.names = names;
    }

    @Nonnull
    @Override
    public Type getType() {
        return type;
    }

    @Override
    @Nonnull
    public Set<Resource> getNames() {
        return names;
    }

    @Override
    public boolean equals(Object o) {
        throw new UnsupportedOperationException();
    }

    @Override
    public int hashCode() {
        throw new UnsupportedOperationException();
    }
}

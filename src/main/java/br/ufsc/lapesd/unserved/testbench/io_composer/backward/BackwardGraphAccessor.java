package br.ufsc.lapesd.unserved.testbench.io_composer.backward;

import br.ufsc.lapesd.unserved.testbench.io_composer.graph.GraphAccessor;

/**
 * Tags a GraphAccessor as of a backward nature
 */
public interface BackwardGraphAccessor<State, Cost, Transition>
        extends GraphAccessor<State, Cost, Transition> {
}

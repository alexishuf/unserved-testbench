package br.ufsc.lapesd.unserved.testbench.model.http;

import br.ufsc.lapesd.unserved.testbench.model.Message;
import br.ufsc.lapesd.unserved.testbench.model.http.modifiers.AsHttpPropertyModifierHandlerFactory;
import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;

import java.util.List;

public interface HttpMessageNode extends Message {
    void setAsHttpPropertyModifierHandler(AsHttpPropertyModifierHandlerFactory factory);
    AsHttpPropertyModifierHandlerFactory getAsHttpPropertyModifierHandlerFactory();

    /**
     * Gets o in (resource, property, o), applying the binding through ux-http:AsHttpProperty of any
     * Variable that is part of this ux-http:HttpMessage.
     *
     * @param resource subject of which the property will be get.
     * @param property property to get
     * @return object of the (resource, property, object) triple
     */
    RDFNode getHttpProperty(Resource resource, Property property);

    List<Resource> getHttpPropertyList(Property property);

    default RDFNode getHttpProperty(Property property) {
        return getHttpProperty(this, property);
    }
    default Resource getHttpPropertyResource(Property property) {
        RDFNode node = getHttpProperty(property);
        return node != null && node.isResource() ? node.asResource() : null;
    }
    default Literal getHttpPropertyLiteral(Property property) {
        RDFNode node = getHttpProperty(property);
        return node != null && node.isLiteral() ? node.asLiteral() : null;
    }
    default Resource getHttpPropertyResource(Resource resource, Property property) {
        RDFNode node = getHttpProperty(resource, property);
        return node != null && node.isResource() ? node.asResource() : null;
    }
    default Literal getHttpPropertyLiteral(Resource resource, Property property) {
        Resource node = getHttpPropertyResource(resource, property);
        return node != null && node.isLiteral() ? node.asLiteral() : null;
    }


}

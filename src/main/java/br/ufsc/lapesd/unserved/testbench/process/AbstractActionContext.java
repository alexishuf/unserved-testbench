package br.ufsc.lapesd.unserved.testbench.process;

public abstract class AbstractActionContext implements ActionContext {
    private final ActionRunnersContext runnersContext;
    private final Object key;

    protected AbstractActionContext(ActionRunnersContext runnersContext, Object key) {
        this.runnersContext = runnersContext;
        this.key = key;
        runnersContext.storeActionContext(key, this);
    }

    @Override
    public ActionRunnersContext getRunnersContext() {
        return runnersContext;
    }

    @Override
    public Object getKey() {
        return key;
    }
}

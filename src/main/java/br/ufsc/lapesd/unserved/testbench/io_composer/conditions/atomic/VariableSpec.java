package br.ufsc.lapesd.unserved.testbench.io_composer.conditions.atomic;

import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.Condition;
import br.ufsc.lapesd.unserved.testbench.model.Variable;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.OWL2;

import javax.annotation.Nonnull;
import java.util.Map;
import java.util.Objects;

/**
 * A Variable Spec is a combination of type and representation.
 */
public class VariableSpec implements Condition {
    private final @Nonnull Resource type;
    private final Resource representation;

    public VariableSpec(@Nonnull Resource type, Resource representation) {
        this.type = type;
        this.representation = representation;
    }

    public VariableSpec(@Nonnull Variable var) {
        Resource representation = var.getValueRepresentation();
        if (representation == null)
            representation = var.getRepresentation();
        this.representation = representation;
        Resource type = var.getType();
        this.type = type == null ? OWL2.Thing : type;
    }

    @Nonnull
    @Override
    public Condition replacing(@Nonnull Map<Variable, Variable> current2replacement) {
        return new VariableSpec(type, representation);
    }

    @Nonnull
    public Resource getType() {
        return type;
    }
    public Resource getRepresentation() {
        return representation;
    }

    boolean strictlySatisfiedBy(@Nonnull VariableSpec other) {
        return type.equals(other.type) &&
                (representation == null || Objects.equals(representation, other.representation));
    }

    //TODO create a dedicated class to efficiently check subsumption instead of writing a
    // satisfiedBy() method that does it slowly.

    @Override
    public String toString() {
        return String.format("VarSpec(%s, %s)", type, String.valueOf(representation));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        VariableSpec that = (VariableSpec) o;

        if (!type.equals(that.type)) return false;
        return representation != null ? representation.equals(that.representation) : that.representation == null;
    }

    @Override
    public int hashCode() {
        int result = type.hashCode();
        result = 31 * result + (representation != null ? representation.hashCode() : 0);
        return result;
    }
}

package br.ufsc.lapesd.unserved.testbench.benchmarks.wsc08;

import br.ufsc.lapesd.unserved.testbench.Algorithm;
import br.ufsc.lapesd.unserved.testbench.benchmarks.compose.TimesExperimentResult;
import br.ufsc.lapesd.unserved.testbench.benchmarks.experiment.AbstractExperimentDesignRunner;
import br.ufsc.lapesd.unserved.testbench.benchmarks.experiment.AbstractExperimentLevelsDesignRunner;
import br.ufsc.lapesd.unserved.testbench.benchmarks.experiment.ExperimentResult;
import br.ufsc.lapesd.unserved.testbench.benchmarks.experiment.Factors;
import br.ufsc.lapesd.unserved.testbench.benchmarks.pragproof_design.PPExperimentDesignRunner;
import org.kohsuke.args4j.CmdLineParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static br.ufsc.lapesd.unserved.testbench.io_composer.layered.LayeredServiceSetGraphIOComposer.*;

public class WscRawExperimentRunner extends AbstractExperimentDesignRunner {
    private static Logger logger = LoggerFactory.getLogger(WscRawExperimentRunner.class);

    public WscRawExperimentRunner() {
        super(new WscExperimentFactory(false));
    }

    public static void main(String[] args) throws Exception {
        WscRawExperimentRunner runner = new WscRawExperimentRunner();
        new CmdLineParser(runner).parseArgument(args);
        runner.run();
    }

    private File getDesignRelativeFile(@Nullable String path) {
        if (path == null || path.isEmpty()) return null;
        File dir = designCsv.getAbsoluteFile().getParentFile();
        assert dir != null;
        File file = new File(path);
        return file.isAbsolute() ? file : new File(dir, path);
    }

    @Override
    protected Factors createFactors(Map<String, String> factors) {
        File wscConvertedRoot = getDesignRelativeFile(factors.get("wscConvertedRoot"));
        File failPoints = getDesignRelativeFile(factors.getOrDefault("failPoints", null));
        Algorithm algorithm = Algorithm.valueOf(factors.get("algorithm"));

        WscFactors.Builder builder = new WscFactors.Builder(wscConvertedRoot, algorithm);
        if (failPoints != null) builder.withFailPoints(failPoints);
        String str = factors.get("serviceCompression");
        if (str != null) builder.withServiceCompression(Boolean.parseBoolean(str));
        str = factors.get("backwardClosureOpt");
        if (str != null) builder.withBackwardClosureOpt(Boolean.parseBoolean(str));
        str = factors.get("preconditions");
        if (str != null) builder.withPreconditions(Boolean.parseBoolean(str));
        str = factors.get("onlyGraph");
        if (str != null) builder.withOnlyGraph(Boolean.parseBoolean(str));
        str = factors.get("naiveIO");
        if (str != null) builder.withNaiveIO(Boolean.parseBoolean(str));
        str = factors.get("combinedJN");
        if (str != null) builder.withCombinedJN(Boolean.parseBoolean(str));
        str = factors.get("listSubclasses");
        if (str != null) builder.withListSubclasses(Boolean.parseBoolean(str));
        str = factors.get("naiveTCG");
        if (str != null) builder.withNaiveTCG(Boolean.parseBoolean(str));
        str = factors.get("dismissOutput");
        if (str != null) builder.withDismissOutput(Boolean.parseBoolean(str));
        str = factors.get("childXms");
        if (str != null && !str.isEmpty()) builder.withChildXms(str);
        str = factors.get("childXmx");
        if (str != null && !str.isEmpty()) builder.withChildXmx(str);
        str = factors.get("childJVMOpts");
        if (str != null && !str.isEmpty()) builder.withChildJVMOpts(str);
        str = factors.get("execute");
        if (str != null) builder.withExecute(Boolean.parseBoolean(str));
        str = factors.get("problem");
        if (str != null) builder.withProblem(Integer.parseInt(str));
        str = factors.get("subdir");
        if (str != null) builder.withSubDir(str);
        str = factors.get("known");
        if (str != null) builder.withKnown(str);
        str = factors.get("wanted");
        if (str != null) builder.withWanted(str);
        str = factors.get("preheatCount");
        if (str != null) builder.withPreheatCount(Integer.parseInt(str));
        return builder.build();
    }

    @Override
    protected List<String> getResultHeaders() {
        return Arrays.asList("initialization", "graphBuilderSetup", "graphConstruction",
                "graphOptimization", "output", "composition", "firstReplan", "blacklisting",
                "execution", "graphAdaptation", "executionValidation", "total",
                graphConstructionMem, graphOptimizationMem, planningMem, compositionMem, "maxMem");
    }

    @Override
    protected List<String> getResultValues(ExperimentResult result) {
        TimesExperimentResult r = (TimesExperimentResult) result;
        List<Double> times = r.getCompositionTimes();
        double second = times.size() > 1 ? times.get(1) : 0.0;
        double setup = r.getTotal(graphBuilderSetupTime);
        double construction = r.getTotal(graphConstructionTime);
        double optimization = r.getTotal(graphOptimizationTime);
        long maxMem = Stream.of(r.getMaxMem(graphConstructionMem),
                r.getMaxMem(graphOptimizationMem), r.getMaxMem(planningMem),
                r.getMaxMem(compositionMem)).max(Long::compareTo).get();

        return Stream.of(r.getInitialization(), setup, construction, optimization, r.getOutput(),
                r.getComposition(), second, r.getBlacklisting(), r.getExecution(),
                r.getTotal(graphAdaptation, 0), r.getTotal(executionValidation, 0),
                r.getTotal(), r.getMaxMem(graphConstructionMem), r.getMaxMem(graphOptimizationMem),
                r.getMaxMem(planningMem), r.getMaxMem(compositionMem), maxMem
        ).map(String::valueOf).collect(Collectors.toList());
    }

    public static class Builder extends AbstractExperimentLevelsDesignRunner.Builder {
        public Builder(File designCsv, File levelsJson, File resultObjectsDir) {
            super(designCsv, levelsJson, resultObjectsDir);
        }

        @Override
        public PPExperimentDesignRunner build() {
            return setupAbstractRunner(new PPExperimentDesignRunner());
        }
    }
}

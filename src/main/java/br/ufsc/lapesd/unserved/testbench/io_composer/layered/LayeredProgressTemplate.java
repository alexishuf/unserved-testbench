package br.ufsc.lapesd.unserved.testbench.io_composer.layered;

import com.google.common.base.Preconditions;

import javax.annotation.Nullable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LayeredProgressTemplate {
    private final Double completeServices;
    private final Integer totalServices;
    private final Double completeEdges;
    private final Integer totalEdges;
    private static final Pattern parserRx = Pattern.compile("(\\*|[0-9]+|[0-9]?\\.[0-9]+)/(\\*|[0-9]+)/(\\*|[0-9]+|[0-9]?\\.[0-9]+)/(\\*|[0-9]+)");

    public LayeredProgressTemplate(@Nullable Double completeServices,
                                   @Nullable Integer totalServices,
                                   @Nullable Double completeEdges, @Nullable Integer totalEdges) {
        Preconditions.checkArgument(completeServices == null || isPercent(completeServices)
                || isInt(completeServices));
        Preconditions.checkArgument(completeEdges == null || isPercent(completeEdges)
                || isInt(completeEdges));
        this.completeServices = completeServices;
        this.totalServices = totalServices;
        this.completeEdges = completeEdges;
        this.totalEdges = totalEdges;
    }

    private static boolean isPercent(Double value) {
        return value != null && Math.floor(value) == 0 && value - Math.floor(value) > 0;
    }

    private static boolean isInt(Double value) {
        return value != null && value - Math.floor(value) == 0;
    }

    public boolean match(LayeredProgress progress) {
        return match(completeServices, progress.getCompleteServices(), progress.getTotalServices())
                && match(completeEdges, progress.getCompleteEdges(), progress.getTotalEdges())
                && match(totalServices, progress.getTotalServices())
                && match(totalEdges, progress.getTotalEdges());
    }

    private boolean match(Double tpl, int value, int total) {
        if (isInt(tpl)) {
            return value == tpl.intValue();
        } else if (isPercent(tpl)) {
            return Math.round(total * tpl) == value;
        } else {
            assert tpl == null;
            return true;
        }
    }

    private boolean match(Integer tpl, int value) {
        return tpl == null || value == tpl;
    }

    @Nullable
    public static LayeredProgressTemplate parse(String text) {
        Matcher m = parserRx.matcher(text);
        if (!m.matches()) return null;
        String g1 = m.group(1), g2 = m.group(2), g3 = m.group(3), g4 = m.group(4);
        Double completeServices = g1.equals("*") ? null : Double.parseDouble(g1);
        Integer totalServices = g2.equals("*") ? null : Integer.parseInt(g2);
        Double completeEdges = g3.equals("*") ? null : Double.parseDouble(g3);
        Integer totalEdges = g4.equals("*") ? null : Integer.parseInt(g4);
        try {
            return new LayeredProgressTemplate(completeServices, totalServices, completeEdges,
                    totalEdges);
        } catch (IllegalArgumentException e) {
            return null;
        }
    }

    private String toString(Double value) {
        if (value == null) return "*";
        return (value - Math.floor(value) == 0) ? String.valueOf(value.intValue())
                : String.valueOf(value.doubleValue());
    }

    @Override
    public String toString() {
        return String.format("%s/%s/%s/%s",
                toString(completeServices),
                totalServices == null ? "*" : String.valueOf(totalServices),
                toString(completeEdges),
                totalEdges == null ? "*" : String.valueOf(totalEdges));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LayeredProgressTemplate that = (LayeredProgressTemplate) o;

        if (completeServices != null ? !completeServices.equals(that.completeServices) : that.completeServices != null)
            return false;
        if (totalServices != null ? !totalServices.equals(that.totalServices) : that.totalServices != null)
            return false;
        if (completeEdges != null ? !completeEdges.equals(that.completeEdges) : that.completeEdges != null)
            return false;
        return totalEdges != null ? totalEdges.equals(that.totalEdges) : that.totalEdges == null;
    }

    @Override
    public int hashCode() {
        int result = completeServices != null ? completeServices.hashCode() : 0;
        result = 31 * result + (totalServices != null ? totalServices.hashCode() : 0);
        result = 31 * result + (completeEdges != null ? completeEdges.hashCode() : 0);
        result = 31 * result + (totalEdges != null ? totalEdges.hashCode() : 0);
        return result;
    }
}

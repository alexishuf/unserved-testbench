package br.ufsc.lapesd.unserved.testbench.composer.impl;

import br.ufsc.lapesd.unserved.testbench.composer.Composition;
import br.ufsc.lapesd.unserved.testbench.io_composer.impl.IOComposerInput;
import br.ufsc.lapesd.unserved.testbench.process.data.DataContext;
import br.ufsc.lapesd.unserved.testbench.process.data.SimpleDataContext;
import br.ufsc.lapesd.unserved.testbench.replication.action.ReplicationContainer;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;

import javax.annotation.Nonnull;

public class ReplicationComposition implements Composition {
    @Nonnull private IOComposerInput composerInput;
    @Nonnull private final ReplicationContainer container;
    private boolean last = false;

    public ReplicationComposition(@Nonnull ReplicationContainer container,
                                  @Nonnull IOComposerInput composerInput) {
        this.container = container;
        this.composerInput = composerInput;
    }

    @Nonnull
    public ReplicationContainer getContainer() {
        return container;
    }

    @Override
    public boolean isNull() {
        return false;
    }

    @Nonnull
    @Override
    public Resource getWorkflowRoot() {
        return container.getAction();
    }

    @Nonnull
    @Override
    public DataContext createDataContext() {
        return new SimpleDataContext(composerInput.getSkolemizedUnion(), r -> {
            Resource resolved = composerInput.getSkolemizerMap().resolve(r);
            return resolved == null ? r : resolved;
        });
    }

    @Override
    public void close() {
        composerInput.close();
        container.close();
    }

    @Override
    public Model takeModel() {
        throw new UnsupportedOperationException();
    }

    public void setLast(boolean last) {
        this.last = last;
    }

    public boolean isLast() {
        return last;
    }
}

package br.ufsc.lapesd.unserved.testbench.benchmarks.design1;


import br.ufsc.lapesd.unserved.testbench.Algorithm;
import br.ufsc.lapesd.unserved.testbench.AlgorithmFamily;
import br.ufsc.lapesd.unserved.testbench.benchmarks.ComposeWorkflowApp;
import br.ufsc.lapesd.unserved.testbench.benchmarks.design1.data.D1ExperimentData;
import br.ufsc.lapesd.unserved.testbench.benchmarks.experiment.ExperimentException;
import com.google.common.base.Preconditions;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ComposeWorkflowAppD1Experiment extends BasicD1Experiment {
    private File wantedTurtle;
    private File knownTurtle;

    public ComposeWorkflowAppD1Experiment(D1ExperimentData experimentData, D1Factors factors) {
        super(experimentData, factors);
        Preconditions.checkArgument(factors.getAlgorithm().getFamily()
                == AlgorithmFamily.IOGraphPath);
        Algorithm.valueOf(factors.getAlgorithm().name()); //precondition
    }


    @Override
    public void setUp() throws Exception {
        super.setUp();
        wantedTurtle = descriptionsData.getWantedFile(); //TODO use actual files
        knownTurtle = descriptionsData.getKnownFile(); //TODO use actual files
    }

    @Override
    protected void run(File workflowFile, File timesFile, List<File> fileList) throws ExperimentException {

        List<String> args = new ArrayList<>(Arrays.asList(ComposeWorkflowApp.class.getName(),
                "--algorithm", getFactors().getAlgorithm().name(),
                "--preheat-count", String.valueOf(getFactors().getPreheatCount())));
        if (getFactors().getExecute()) args.add("--execute");
        args.addAll(Arrays.asList(
                "--wanted", "https://alexishuf.bitbucket.io/2016/04/unserved/var#wanted",
                "--workflow-out", workflowFile.getAbsolutePath(),
                "--times-out", timesFile.getAbsolutePath(),
                knownTurtle.getAbsolutePath(), wantedTurtle.getAbsolutePath()));
        runChildJVM(args, fileList);
    }
}

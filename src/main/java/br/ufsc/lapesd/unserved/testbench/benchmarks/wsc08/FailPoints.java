package br.ufsc.lapesd.unserved.testbench.benchmarks.wsc08;

import br.ufsc.lapesd.unserved.testbench.io_composer.layered.LayeredProgressTemplate;

public class FailPoints {
    public static final class FailPoint {
        private LayeredProgressTemplate template;
        private int count;
    }
}

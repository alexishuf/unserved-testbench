package br.ufsc.lapesd.unserved.testbench.io_composer.state;

import br.ufsc.lapesd.unserved.testbench.model.Message;
import br.ufsc.lapesd.unserved.testbench.model.Variable;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.Objects;

/**
 * A Pair of consequent and antecedent messages, with following assignments of other
 * variables from the results of the consequent.
 */
public class MessagePairAction implements Action {
    private final @Nonnull Message antecedent, consequent;
    private final int index;
    private final @Nonnull Copies assignments = new Copies();


    public MessagePairAction(@Nonnull Message antecedent, @Nonnull Message consequent, int index) {
        this.antecedent = antecedent;
        this.consequent = consequent;
        this.index = index;
    }
    public MessagePairAction(@Nonnull Message antecedent, @Nonnull Message consequent) {
        this(antecedent, consequent, 0);
    }

    public MessagePairAction(MessagePairAction pair) {
        this(pair.antecedent, pair.consequent, pair.index);
    }

    public MessagePairAction withoutCopies() {
        return new MessagePairAction(antecedent, consequent, index);
    }

    @Nonnull
    public Message getAntecedent() {
        return antecedent;
    }

    @Nonnull
    public Message getConsequent() {
        return consequent;
    }

    public int getIndex() {
        return index;
    }

    public MessagePairAction addAssignment(Variable result, Variable target) {
        assignments.add(result, target);
        return this;
    }

    public MessagePairAction addAssignments(MessagePairAction other) {
        assignments.addAll(other.getAssignments());
        return this;
    }

    public MessagePairAction addAssignments(Collection<Copy> collection) {
        assignments.addAll(collection);
        return this;
    }

    @Nonnull
    public Copies getAssignments() {
        return assignments;
    }

    @Override
    public String toString() {
        return String.format("MessagePair(%s, %s, %s, %d)", antecedent, consequent, assignments, index);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MessagePairAction that = (MessagePairAction) o;
        return index == that.index &&
                Objects.equals(antecedent, that.antecedent) &&
                Objects.equals(consequent, that.consequent) &&
                Objects.equals(assignments, that.assignments);
    }

    @Override
    public int hashCode() {

        return Objects.hash(antecedent, consequent, index, assignments);
    }
}

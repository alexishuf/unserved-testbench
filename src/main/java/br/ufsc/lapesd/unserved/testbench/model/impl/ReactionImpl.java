package br.ufsc.lapesd.unserved.testbench.model.impl;

import br.ufsc.lapesd.unserved.testbench.iri.Unserved;
import br.ufsc.lapesd.unserved.testbench.model.Cardinality;
import br.ufsc.lapesd.unserved.testbench.model.Message;
import br.ufsc.lapesd.unserved.testbench.model.Reaction;
import br.ufsc.lapesd.unserved.testbench.model.UnservedFactory;
import br.ufsc.lapesd.unserved.testbench.util.ImplementationByType;
import org.apache.jena.enhanced.EnhGraph;
import org.apache.jena.enhanced.EnhNode;
import org.apache.jena.enhanced.Implementation;
import org.apache.jena.graph.Node;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.impl.ResourceImpl;

import java.util.Arrays;

public class ReactionImpl extends ResourceImpl implements Reaction {
    public static Implementation factory = new ImplementationByType(Unserved.Reaction.asNode(),
            Arrays.asList(Unserved.reactionTo.asNode(), Unserved.reactionCardinality.asNode())) {
        @Override
        public EnhNode wrap(Node node, EnhGraph eg) {
            return new ReactionImpl(node, eg);
        }
    };

    public ReactionImpl(Node n, EnhGraph m) {
        super(n, m);
    }

    @Override
    public Message getReactionTo() {
        Statement statement = getProperty(Unserved.reactionTo);
        return statement == null ? null : statement.getResource().as(Message.class);
    }

    @Override
    public Cardinality getReactionCardinality() {
        Statement statement = getProperty(Unserved.reactionCardinality);
        return statement == null ? null : UnservedFactory.asCardinality(statement.getResource());
    }
}

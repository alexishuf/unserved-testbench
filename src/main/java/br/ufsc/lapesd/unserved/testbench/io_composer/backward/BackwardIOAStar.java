package br.ufsc.lapesd.unserved.testbench.io_composer.backward;

import br.ufsc.lapesd.unserved.testbench.composer.ComposerException;
import br.ufsc.lapesd.unserved.testbench.composer.Composition;
import br.ufsc.lapesd.unserved.testbench.io_composer.IOComposer;
import br.ufsc.lapesd.unserved.testbench.io_composer.impl.AbstractIOComposerBuilder;
import br.ufsc.lapesd.unserved.testbench.io_composer.impl.IOComposerInput;
import br.ufsc.lapesd.unserved.testbench.io_composer.state.State;
import br.ufsc.lapesd.unserved.testbench.io_composer.state.UnservedPSequenceWriter;
import es.usc.citius.hipster.model.Node;
import es.usc.citius.hipster.model.Transition;

import java.util.function.Function;

/**
 * Composition algorithm using A* to perform a backward (from the objective) search on a graph
 * determined by the I/O math between services.
 * <p>
 * The goal state is one in which all wanted variables are bound
 */
public class BackwardIOAStar extends AbstractBackwardIOAStar {
    public BackwardIOAStar(IOComposerInput composerInput,
                           BackwardGraphAccessor<State, Double, Transition<Void, State>>
                                   graphAccessor) throws ComposerException {
        super(composerInput, graphAccessor);
    }

    @Override
    protected <A, NodeType extends Node<A, State, NodeType>> Composition
    toComposition(Node<A, State, NodeType> goalNode) {
        assert !goalNode.state().isGoal() : "Goal should be initial state";

        UnservedPSequenceWriter seqWriter = new UnservedPSequenceWriter();
        Node<A, State, NodeType> node = goalNode.previousNode();
        while (node != null) {
            seqWriter.append(node.state().getActions());
            node = node.previousNode();
        }
        return seqWriter.toIOComposition(model, ci.getSkolemizerMap());
    }

    public static class Builder extends AbstractIOComposerBuilder {
        private Function<IOComposerInput, BackwardGraphAccessor<State, Double,
                Transition<Void, State>>> accessorFactory = NaiveBackwardGraphAccessor::new;

        public Builder setAccessorFactory(Function<IOComposerInput, BackwardGraphAccessor<State, Double, Transition<Void, State>>> accessorFactory) {
            this.accessorFactory = accessorFactory;
            return this;
        }

        @Override
        protected IOComposer buildImpl() throws Exception {
            return new BackwardIOAStar(composerInput, accessorFactory.apply(composerInput));
        }
    }
}

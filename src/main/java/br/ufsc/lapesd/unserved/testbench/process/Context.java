package br.ufsc.lapesd.unserved.testbench.process;

import br.ufsc.lapesd.unserved.testbench.components.ComponentRegistry;
import br.ufsc.lapesd.unserved.testbench.process.data.DataContext;

import java.util.Map;

public interface Context extends AutoCloseable {
    DataContext data();
    ActionRunnersContext runners();
    ComponentRegistry componentRegistry();
    Interpreter interpreter();
    boolean isCancelled();
    boolean isAborted();
    void cancel();
    void abort();

    /**
     * Global map, with arbitrary, non-closeable objects mapped to unique
     * identifying strings (URIs are STRONGLY recommended). This may be used, for example
     * to override default configurations of specific components.
     *
     * The map may be modified by components, but no value should require special treatment,
     * such as AutoCloseable or Closeable interfaces.
     *
     * @return map from Strings to Objects
     */
    Map<String, Object> globalContext();
    void close();
}

package br.ufsc.lapesd.unserved.testbench.io_composer.conditions;

import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.atomic.TriplePattern;
import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.atomic.VariableSpec;

import javax.annotation.Nonnull;

public abstract class ConditionVisitor {
    public boolean visitCondition(@Nonnull Condition cond)             { return false; }
    public boolean visitConditionList(@Nonnull ConditionList cond)     { return false; }
    public boolean visitAnd(@Nonnull And cond)                         { return false; }
    public boolean visitOr(@Nonnull Or cond)                           { return false; }
    public boolean visitAtomicCondition(@Nonnull AtomicCondition cond) { return false; }
    public boolean visitTriplePattern(@Nonnull TriplePattern cond)     { return false; }
    public boolean visitVariableSpec(@Nonnull VariableSpec cond)       { return false; }
    public boolean visit(@Nonnull Class<? extends Condition> aClass,
                         @Nonnull Condition cond)                            { return false; }
}

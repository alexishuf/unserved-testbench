package br.ufsc.lapesd.unserved.testbench.benchmarks.pragproof_design;

import br.ufsc.lapesd.unserved.testbench.Algorithm;
import br.ufsc.lapesd.unserved.testbench.AlgorithmFamily;
import br.ufsc.lapesd.unserved.testbench.benchmarks.ComposeWorkflowApp;
import br.ufsc.lapesd.unserved.testbench.benchmarks.DescriptionsGenerator;
import br.ufsc.lapesd.unserved.testbench.benchmarks.experiment.ExperimentException;
import br.ufsc.lapesd.unserved.testbench.benchmarks.wsc08.WscComposerBuilderFactory;
import br.ufsc.lapesd.unserved.testbench.util.ResourceExtractor;
import com.google.common.base.Preconditions;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ComposeWorkflowAppExperiment extends BasicExperiment {
    private File wantedTurtle;
    private File knownTurtle;

    public ComposeWorkflowAppExperiment(PPFactors factors) {
        super(factors);
        Preconditions.checkArgument(factors.getAlgorithm().getFamily()
                == AlgorithmFamily.IOGraphPath);
        Algorithm.valueOf(factors.getAlgorithm().name()); //precondition check
    }

    @Override
    public void setUp() throws Exception {
        super.setUp();
        ResourceExtractor ex = new ResourceExtractor();
        wantedTurtle = ex.extract("benchmarks/description-generator-wanted.ttl");
        knownTurtle = ex.extract("benchmarks/description-generator-known.ttl");
    }

    @Override
    public void close() throws Exception {
        FileUtils.forceDelete(wantedTurtle);
        FileUtils.forceDelete(knownTurtle);
        super.close();
    }

    @Override
    protected void run(File workflowFile, File timesFile, List<File> fileList) throws ExperimentException {
        List<String> args = new ArrayList<>(Arrays.asList(ComposeWorkflowApp.class.getName(),
                "--algorithm", getFactors().getAlgorithm().name(),
                "--preheat-count", String.valueOf(getFactors().getPreheatCount())));
        if (getFactors().getExecute()) args.add("--execute");
        String facArgs = "basicReasoning=true";
        if (getFactors().getPreconditions())
            facArgs += ";preconditions=true";
        args.addAll(Arrays.asList(
                "--builder-factory", WscComposerBuilderFactory.class.getName(),
                "--builder-factory-args", facArgs,
                "--wanted", "https://alexishuf.bitbucket.io/2016/04/unserved/var#wanted",
                "--workflow-out", workflowFile.getAbsolutePath(),
                "--times-out", timesFile.getAbsolutePath(),
                knownTurtle.getAbsolutePath(), wantedTurtle.getAbsolutePath()));
        runChildJVM(args, fileList);
    }

    @Override
    protected File createDescription(int chainLength, int alternatives, int conditions,
                                     String rel, boolean preconditions) throws Exception {
        File file = Files.createTempFile("description", ".ttl").toFile();
        file.deleteOnExit();
        File drain = Files.createTempFile("drain", ".n3").toFile();
        drain.deleteOnExit();
        DescriptionsGenerator.Builder builder = new DescriptionsGenerator.Builder(chainLength)
                .setPreconditions(preconditions)
                .setRulesOut(drain)
                .setUnservedOut(file)
                .setConditions(conditions)
                .setAlternatives(alternatives);
        if (rel != null) {
            builder.setSkIRI("http://example.org/skolem-" + rel + "#");
            builder.setRel(rel);
        }
        builder.build().run();
        FileUtils.deleteQuietly(drain);
        return file;
    }
}

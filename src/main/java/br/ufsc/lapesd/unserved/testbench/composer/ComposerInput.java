package br.ufsc.lapesd.unserved.testbench.composer;

import br.ufsc.lapesd.unserved.testbench.input.RDFInput;
import br.ufsc.lapesd.unserved.testbench.util.SkolemizerMap;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.reasoner.rulesys.Rule;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.IOException;
import java.util.List;

public interface ComposerInput extends AutoCloseable {
    List<RDFInput> getInputs();

    void addInput(RDFInput input);
    void addRulesFile(File file) throws IOException;

    @Nonnull
    Model initSkolemizedUnion(boolean basicReasoning, boolean inputHasNoSkolemized) throws IOException;

    @Nonnull
    Model getSkolemizedUnion();

    @Nonnull
    List<Rule> customRules();

    SkolemizerMap getSkolemizerMap();

    void addDuplicate(ComposerInput duplicate);

    void removeDuplicate(ComposerInput duplicate);

    @Override
    void close() throws ComposerException;
}

package br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.discovery;

import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.ConditionEvaluator;
import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.NoConditionEvaluator;
import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.atomic.VariableSpec;
import br.ufsc.lapesd.unserved.testbench.io_composer.impl.IOComposerInput;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.NaiveAutoFixProviderSetsSupplier;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.Node;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.ProviderSetsSupplier;
import br.ufsc.lapesd.unserved.testbench.model.Variable;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class IOLayersState implements LayersState {
    protected @Nonnull Set<VariableSpec> known = new HashSet<>();
    protected @Nonnull Set<VariableSpec> newlyKnown = new HashSet<>();
    private  @Nonnull Set<VariableSpec> newlyKnown2 = new HashSet<>();

    public IOLayersState() { }
    public IOLayersState(@Nonnull IOComposerInput ignored) { }

    @Nonnull
    public Set<VariableSpec> getKnown() {
        return known;
    }

    @Nonnull
    public Set<VariableSpec> getNewlyKnown() {
        return newlyKnown;
    }

    public boolean supportsSPINConditions() {
        return false;
    }

    @Nonnull
    @Override
    public ConditionEvaluator getVariableSpecsEvaluator() {
        throw new UnsupportedOperationException("Not implemented, yet");
    }

    @Nonnull
    @Override
    public ConditionEvaluator getEvaluator() {
        return new NoConditionEvaluator();
    }

    @Nonnull
    @Override
    public ProviderSetsSupplier createProviderSetsSupplier() {
        return new NaiveAutoFixProviderSetsSupplier();
    }

    @Nonnull
    @Override
    public NodesSupplier<? extends LayersState> createNodesSupplier() {
        return new InputIndexedNodesSupplier();
    }

    public void advance(@Nonnull Collection<Node> nodes) {
        newlyKnown2.clear();
        for (Node node : nodes) {
            for (Variable out : node.getOutputs()) {
                VariableSpec spec = out.asSpec();
                if (!known.contains(spec))
                    newlyKnown2.add(spec);
            }
        }
        known.addAll(newlyKnown);
        Set<VariableSpec> tmp = newlyKnown;
        newlyKnown = newlyKnown2;
        newlyKnown2 = tmp;
    }
}

package br.ufsc.lapesd.unserved.testbench.components.expressions.evaluator.instance;

import br.ufsc.lapesd.unserved.testbench.components.expressions.Connective;
import br.ufsc.lapesd.unserved.testbench.components.expressions.evaluator.ClassExpressionEvaluator;
import br.ufsc.lapesd.unserved.testbench.components.expressions.evaluator.Step;
import com.google.common.base.Preconditions;
import org.apache.jena.rdf.model.RDFNode;

import javax.annotation.Nonnull;
import java.util.Collection;

public class ConnectiveInstanceStep extends Step<RDFNode> {
    @Nonnull protected final Connective expr;

    public ConnectiveInstanceStep(@Nonnull Collection<Step<RDFNode>> children,
                                  @Nonnull Connective expression) {
        super(children);
        this.expr = expression;
    }

    @Nonnull
    @Override
    public ClassExpressionEvaluator.Result performEvaluate(RDFNode node) {
        Preconditions.checkState(hasAllResults(node));
        if (Helper.isA(node, expr.getNames())) return ClassExpressionEvaluator.Result.TRUE;

        if (expr.connectiveType() == Connective.ConnectiveType.INTERSECTION) {
            if (all(node, ClassExpressionEvaluator.Result.TRUE)) return ClassExpressionEvaluator.Result.TRUE;
            if (allOrIndeterminate(node, ClassExpressionEvaluator.Result.TRUE)) return ClassExpressionEvaluator.Result.INDETERMINATE;
            assert some(node, ClassExpressionEvaluator.Result.FALSE);
            return ClassExpressionEvaluator.Result.FALSE;
        } else if (expr.connectiveType() == Connective.ConnectiveType.UNION) {
            if (all(node, ClassExpressionEvaluator.Result.FALSE)) return ClassExpressionEvaluator.Result.FALSE;
            if (allOrIndeterminate(node, ClassExpressionEvaluator.Result.FALSE)) return ClassExpressionEvaluator.Result.INDETERMINATE;
            assert some(node, ClassExpressionEvaluator.Result.TRUE);
            return ClassExpressionEvaluator.Result.TRUE;
        } else if (expr.connectiveType() == Connective.ConnectiveType.COMPLEMENT) {
            ClassExpressionEvaluator.Result result = getChildren().iterator().next().getEvaluatedResult(node);
            Preconditions.checkState(result != null);
            return result.negated();
        }

        throw new UnsupportedOperationException();
    }

    private boolean hasAllResults(RDFNode node) {
        return getChildren().stream().allMatch(c -> c.getEvaluatedResult(node) != null);
    }

    private boolean all(RDFNode node, ClassExpressionEvaluator.Result value) {
        return getChildren().stream().allMatch(c -> c.getEvaluatedResult(node) == value);
    }

    private boolean some(RDFNode node, ClassExpressionEvaluator.Result value) {
        return getChildren().stream().anyMatch(c -> c.getEvaluatedResult(node) == value);
    }

    private boolean allOrIndeterminate(RDFNode node, ClassExpressionEvaluator.Result value) {
        return getChildren().stream().map(s -> s.getEvaluatedResult(node))
                .allMatch(r -> r == value || r == ClassExpressionEvaluator.Result.INDETERMINATE);
    }

}

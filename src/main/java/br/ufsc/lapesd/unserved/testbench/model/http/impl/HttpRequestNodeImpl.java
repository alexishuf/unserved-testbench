package br.ufsc.lapesd.unserved.testbench.model.http.impl;

import br.ufsc.lapesd.unserved.testbench.iri.UnservedX;
import br.ufsc.lapesd.unserved.testbench.model.http.HttpRequestNode;
import br.ufsc.lapesd.unserved.testbench.util.ImplementationByType;
import org.apache.jena.enhanced.EnhGraph;
import org.apache.jena.enhanced.EnhNode;
import org.apache.jena.enhanced.Implementation;
import org.apache.jena.graph.Node;

public class HttpRequestNodeImpl extends AbstractHttpMessageNode implements HttpRequestNode {
    public static Implementation factory = new ImplementationByType(
            UnservedX.HTTP.HttpRequest.asNode()) {
        @Override
        public EnhNode wrap(Node node, EnhGraph eg) {
            return new HttpRequestNodeImpl(node, eg);
        }
    };

    public HttpRequestNodeImpl(Node n, EnhGraph m) {
        super(n, m);
    }
}

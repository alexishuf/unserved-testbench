package br.ufsc.lapesd.unserved.testbench.benchmarks.experiment;

public class ExperimentDesignRunnerException extends Exception {
    public ExperimentDesignRunnerException() {
    }

    public ExperimentDesignRunnerException(String s) {
        super(s);
    }

    public ExperimentDesignRunnerException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public ExperimentDesignRunnerException(Throwable throwable) {
        super(throwable);
    }
}

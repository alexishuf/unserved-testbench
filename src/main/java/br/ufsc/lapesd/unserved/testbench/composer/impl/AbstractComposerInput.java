package br.ufsc.lapesd.unserved.testbench.composer.impl;

import br.ufsc.lapesd.unserved.testbench.composer.ComposerException;
import br.ufsc.lapesd.unserved.testbench.composer.ComposerInput;

import java.util.HashSet;

public abstract class AbstractComposerInput implements ComposerInput {
    private HashSet<ComposerInput> duplicates = new HashSet<>();
    private boolean closeCalled = false;
    protected final ComposerInput parent;
    protected boolean closed = false;

    protected AbstractComposerInput(ComposerInput parent) {
        this.parent = parent;
        if (parent != null)
            parent.addDuplicate(this);
    }

    @Override
    public void addDuplicate(ComposerInput duplicate) {
        duplicates.add(duplicate);
    }

    @Override
    public void removeDuplicate(ComposerInput duplicate) {
        duplicates.remove(duplicate);
        checkAndClose();
    }

    protected void checkAndClose() {
        if (!closed && closeCalled && duplicates.isEmpty()) {
            closeImpl();
            if (parent != null)
                parent.removeDuplicate(this);
            closed = true;
        }
    }

    @Override
    public void close() throws ComposerException {
        closeCalled = true;
        checkAndClose();
    }

    protected abstract void closeImpl() throws ComposerException;
}

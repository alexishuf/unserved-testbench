package br.ufsc.lapesd.unserved.testbench.io_composer.conditions.parsers;

import org.apache.jena.rdf.model.Resource;

import javax.annotation.Nullable;

public class ConditionParsingException extends IllegalArgumentException {
    private final @Nullable Resource conditional;
    public ConditionParsingException(String message, @Nullable Resource conditional1) {
        super(message);
        this.conditional = conditional1;
    }

    @Nullable
    public Resource getConditional() {
        return conditional;
    }
}

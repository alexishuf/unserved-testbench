package br.ufsc.lapesd.unserved.testbench.restdesc_pragproof.n3;

import br.ufsc.lapesd.unserved.testbench.util.jena.UncloseableGraph;
import ch.ethz.inf.vs.semantics.parser.elements.*;
import ch.ethz.inf.vs.semantics.parser.elements.Literal;
import ch.ethz.inf.vs.semantics.parser.visitors.N3BaseElementVisitor;
import org.apache.jena.graph.compose.Delta;
import org.apache.jena.rdf.model.*;
import org.apache.jena.vocabulary.RDF;

import javax.annotation.Nonnull;
import java.util.*;

public class N3Model {
    private final N3Element root;
    private final Model model = ModelFactory.createDefaultModel();
    private final HashMap<Object, Resource> blankNodeMap = new HashMap<>();
    private final HashMap<Formula, Resource> formulaResourceMap = new HashMap<>();
    private final HashMap<Resource, N3Model> formulaModelMap = new HashMap<>();

    private static final String clusterPrefix = "https://alexishuf.bitbucket.io/unserved-testbench/N3ModelCluster#";
    private static final Property clusterHint = ResourceFactory.createProperty(clusterPrefix + "hint");
    private Map<String, Set<Resource>> formulaModelsClusters = null;

    public N3Model(N3Document document) {
        this.root = document;
        read(document);
    }
    protected N3Model(Formula formula) {
        this.root = formula;
        read(formula);
    }

    public N3Element getRoot() {
        return root;
    }

    public Model getModel() {
        return model;
    }

    public Resource getBNode(@Nonnull BlankNode parsed) {
        return blankNodeMap.get(parsed);
    }
    public Resource getBNode(@Nonnull BlankNodePropertyList parsed) {
        return blankNodeMap.get(parsed);
    }
    public Resource getBNode(@Nonnull Exvar exvar) {
        return blankNodeMap.get(exvar.toString());
    }

    private Resource createBNode(N3Element parsed) {
        Object key = parsed;
        if (parsed instanceof Exvar) {
            //TODO handle universals specially: they are not existentials
            key = parsed.toString();
        } else {
            boolean is = parsed instanceof BlankNode || parsed instanceof BlankNodePropertyList;
            if (!is) return null;
        }
        Resource resource = blankNodeMap.get(key);
        if (resource == null) {
            resource = model.createResource();
            blankNodeMap.put(key, resource);
        }
        return resource;
    }

    private Resource createResource(N3Element subject) {
        Resource resource = createBNode(subject);
        if (resource == null && subject instanceof Iri)
            resource = model.createResource(N3Helper.expandIRI((Iri) subject));
        return resource;
    }

    public Resource getFormulaResource(Formula formula) {
        return formulaResourceMap.get(formula);
    }
    public N3Model getFormulaModel(Resource resource) {
        return formulaModelMap.get(resource);
    }
    public N3Model getFormulaModel(Formula formula) {
        Resource resource = formulaResourceMap.get(formula);
        return resource == null ? null : formulaModelMap.get(resource);
    }

    private Resource createFormula(Formula formula) {
        Resource resource = formulaResourceMap.get(formula);
        if (resource == null) {
            resource = this.model.createResource().addProperty(RDF.type, Log.Formula);
            formulaResourceMap.put(formula, resource);
            formulaModelMap.put(resource, new N3Model(formula));
        }
        return resource;
    }

    public boolean isIsomorphicWith(N3Model other) {
        Map<String, Set<Resource>> mine = clusterFormulaModels();
        Map<String, Set<Resource>> theirs = other.clusterFormulaModels();
        if (mine.size() != theirs.size()) return false; //save CPU
        if (mine.isEmpty())  //trivial case
            return getModel().isIsomorphicWith(other.getModel());

        HashMap<String, String> translation = findTranslation(other);
        if (translation == null) return false; //no mapping between clusters
        mine = applyTranslation(mine, translation);

        Model myJena = withClusterHints(mine);
        Model theirJena = other.withClusterHints(theirs);
        return myJena.isIsomorphicWith(theirJena);
    }

    private Model withClusterHints(Map<String, Set<Resource>> clusters) {
        Model with = ModelFactory.createModelForGraph(
                new Delta(new UncloseableGraph(getModel().getGraph())));
        for (Map.Entry<String, Set<Resource>> e : clusters.entrySet()) {
            for (Resource f : e.getValue())
                with.add(f, clusterHint, with.createResource(e.getKey()));
        }
        return with;
    }

    private Map<String, Set<Resource>> applyTranslation(Map<String, Set<Resource>> map,
                                                        HashMap<String, String> translation) {
        HashMap<String, Set<Resource>> translated = new HashMap<>();
        for (Map.Entry<String, Set<Resource>> e : map.entrySet())
            translated.put(translation.get(e.getKey()), e.getValue());
        return translated;
    }

    private HashMap<String, String> findTranslation(N3Model other) {
        Map<String, Set<Resource>> mine = clusterFormulaModels();
        Map<String, Set<Resource>> theirs = other.clusterFormulaModels();

        HashMap<String, String> translation = new HashMap<>();
        for (String iri : mine.keySet()) {
            N3Model myModel = getFormulaModel(mine.get(iri).iterator().next());
            for (Map.Entry<String, Set<Resource>> entry : theirs.entrySet()) {
                N3Model theirModel = other.getFormulaModel(entry.getValue().iterator().next());
                if (myModel.isIsomorphicWith(theirModel)) {
                    translation.put(iri, entry.getKey());
                    break;
                }
            }
            if (!translation.containsKey(iri)) return null; //no mapping between formula models
        }

        return translation;
    }

    private Map<String, Set<Resource>> clusterFormulaModels() {
        if (formulaModelsClusters != null) return formulaModelsClusters;

        formulaModelsClusters = new HashMap<>();
        int clusterIndex = 0;
        List<Resource> list = getModel().listSubjectsWithProperty(RDF.type, Log.Formula).toList();
        for (int i = 0; i < list.size(); i++) {
            Resource f1 = list.get(i);

            String cluster = formulaModelsClusters.entrySet().stream()
                    .filter(e -> e.getValue().contains(f1))
                    .map(Map.Entry::getKey).findFirst().orElse(null);
            if (cluster != null) continue;
            String clusterIri = clusterPrefix + (clusterIndex++);
            formulaModelsClusters.put(clusterIri, new HashSet<>());
            formulaModelsClusters.get(clusterIri).add(f1);

            N3Model f1Model = getFormulaModel(f1);
            for (int j = i+1; j < list.size(); j++) {
                Resource f2 = list.get(j);
                cluster = formulaModelsClusters.entrySet().stream()
                        .filter(e -> e.getValue().contains(f1))
                        .map(Map.Entry::getKey).findFirst().orElse(null);
                if (cluster != null) continue;

                if (f1Model.isIsomorphicWith(getFormulaModel(f2)))
                    formulaModelsClusters.get(clusterIri).add(f2);
            }
        }
        return formulaModelsClusters;
    }

    private void read(N3Element root) {
        root.accept(new N3BaseElementVisitor<RDFNode>() {
            @Override
            public RDFNode visitBlankNode(BlankNode blankNode) {
                return createBNode(blankNode);
            }

            @Override
            public RDFNode visitBlankNodePropertyList(BlankNodePropertyList bn) {
                Resource resource = createBNode(bn);
                assert resource != null;
                addVerbObjects(resource, bn);
                return resource;
            }

            @Override
            public RDFNode vistExvar(Exvar exvar) {
                return createBNode(exvar);
            }

            @Override
            public RDFNode visitIri(Iri iri) {
                return model.createResource(N3Helper.expandIRI(iri));
            }

            @Override
            public RDFNode visitLiteral(Literal literal) {
                return ResourceFactory.createPlainLiteral(literal.text);
            }

            @Override
            public RDFNode visitRDFResource(RDFResource r) {
                RDFNode node = ((N3Element) r.subject).accept(this);
                if (node == null) return null;
                assert node.isResource();
                Resource subject = node.asResource();
                addVerbObjects(subject, r.verbObjects);
                return subject;
            }

            @Override
            public RDFNode visitPrefix(Prefix p) {
                String name = p.getName();
                if (name.endsWith(":")) name = name.substring(0, name.length()-1);
                model.setNsPrefix(name, p.getUri());
                return null;
            }

            @Override
            public RDFNode vistFormula(Formula statements) {
                if (statements == root) {
                    return super.vistFormula(statements);
                } else {
                    return createFormula(statements);
                }
            }

            private void addVerbObjects(@Nonnull Resource subject, @Nonnull Iterable<VerbObject> vos) {
                for (VerbObject vo : vos) {
                    Property property = model.createProperty(N3Helper.verbIRI(vo.verb));
                    N3Helper.toStream(vo.object)
                            .map(o -> ((N3Element) o).accept(this))
                            .filter(rdfNode -> rdfNode != null)
                            .forEach(rdfNode -> model.add(subject, property, rdfNode));
                }
            }
        });

    }


}

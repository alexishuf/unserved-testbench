package br.ufsc.lapesd.unserved.testbench.process.model;

import br.ufsc.lapesd.unserved.testbench.process.model.impl.CopyImpl;
import br.ufsc.lapesd.unserved.testbench.process.model.impl.ReceiveImpl;
import br.ufsc.lapesd.unserved.testbench.process.model.impl.SendImpl;
import br.ufsc.lapesd.unserved.testbench.process.model.impl.SequenceImpl;
import com.google.common.base.Preconditions;
import org.apache.jena.enhanced.BuiltinPersonalities;
import org.apache.jena.enhanced.Implementation;
import org.apache.jena.enhanced.Personality;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;

import javax.annotation.Nonnull;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.stream.Stream;

/**
 * Register interfaces and implementations with {@link BuiltinPersonalities}.
 */
public class ActionFactory {
    private static ActionFactory instance = new ActionFactory();
    private static Map<Class<? extends Action>, Class<? extends Action>> actions = null;
    private static HashSet<Class<? extends Action>> additionalActionIfs = new HashSet<>();

    private static Map<Class<? extends Action>, Class<? extends Action>> getActions() {
        if (actions == null) {
            actions = new HashMap<>();
            actions.put(Copy.class, CopyImpl.class);
            actions.put(Receive.class, ReceiveImpl.class);
            actions.put(Send.class, SendImpl.class);
            actions.put(Sequence.class, SequenceImpl.class);
        }
        return actions;
    }

    public static void install() {
        instance.doInstall();
    }

    public static void install(Personality<RDFNode> personality) {
        try {
            for (Map.Entry<Class<? extends Action>, Class<? extends Action>> entry : getActions().entrySet()) {
                personality.add(entry.getKey(),
                        (Implementation) entry.getValue().getField("factory").get(null));
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private synchronized void doInstall() {
        install(BuiltinPersonalities.model);
    }

    public static boolean addActionInterface(@Nonnull Class<? extends Action> aClass) {
        Preconditions.checkArgument(aClass.isInterface());
        return additionalActionIfs.add(aClass);
    }

    public static Action asAction(Resource resource) {
        if (resource == null) return null;
        Class<? extends Action> aClass = Stream.concat(getActions().keySet().stream(),
                additionalActionIfs.stream()).filter(resource::canAs).findFirst().orElse(null);
        return aClass == null ? null : resource.as(aClass);
    }

    static {
        install();
    }
}

package br.ufsc.lapesd.unserved.testbench.process.data.action;

import br.ufsc.lapesd.unserved.testbench.model.Variable;
import br.ufsc.lapesd.unserved.testbench.process.data.DataContext;

import javax.annotation.Nonnull;
import java.util.Collections;
import java.util.Set;

public interface DataContextAction {
    void apply(DataContext context);
    @Nonnull
    default Set<Variable> changedVariables() {
        return Collections.emptySet();
    }
}

package br.ufsc.lapesd.unserved.testbench.util;

import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;

import javax.annotation.Nonnull;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Stream;

public class NaiveTransitiveClosureGetter extends AbstractTransitiveClosureGetter {
    public NaiveTransitiveClosureGetter(Property predicate, boolean backward) {
        super(predicate, backward);
    }

    @Nonnull
    @Override
    public Set<Resource> getClosureSet(@Nonnull Resource start) {
        return createIterator(start).toCollection(HashSet::new);
    }

    @Nonnull
    @Override
    public Stream<Resource> getClosureStream(@Nonnull Resource start) {
        return createIterator(start).toStream();
    }

    @Nonnull
    @Override
    public TransitiveClosureGetter createFor(Property property, boolean isBackward) {
        return new NaiveTransitiveClosureGetter(property, isBackward);
    }

    private BFSBGPIterator createIterator(@Nonnull Resource start) {
        BFSBGPIterator.Builder builder = BFSBGPIterator.from(start).withInitial();
        return isBackward() ? builder.backward(getPredicate()) : builder.forward(getPredicate());
    }
}

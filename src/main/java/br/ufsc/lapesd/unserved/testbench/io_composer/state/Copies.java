package br.ufsc.lapesd.unserved.testbench.io_composer.state;

import br.ufsc.lapesd.unserved.testbench.model.Variable;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * A set of copies between pairs of variables
 */
public class Copies  implements Action {
    private final  @Nonnull List<Copy> pairs = new ArrayList<>();

    public Copies() {
    }

    public Copies(Copies other) {
        addAll(other);
    }

    public List<Copy> asList() {
        return pairs;
    }

    public Copies add(@Nonnull Variable source,
                      @Nonnull Variable target) {
        pairs.add(new Copy(source, target));
        return this;
    }

    public Copies addAll(Copies other) {
        pairs.addAll(other.asList());
        return this;
    }

    public Copies addAll(Collection<Copy> collection) {
        pairs.addAll(collection);
        return this;
    }

    public Set<Variable> getTargets() {
        return pairs.stream().map(Copy::getTo).collect(Collectors.toSet());
    }

    @Override
    public String toString() {
        return String.format("Copies{%s}", pairs.stream().map(Copy::toString)
                .reduce((l, r) -> l + ", " + r).orElse(""));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Copies copies = (Copies) o;
        return pairs.equals(copies.pairs);
    }

    @Override
    public int hashCode() {
        return pairs.hashCode();
    }
}

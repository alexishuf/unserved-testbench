package br.ufsc.lapesd.unserved.testbench.io_composer;

import br.ufsc.lapesd.unserved.testbench.composer.Composer;

/**
 * A Composition algorithm that performs a backward (from the objective) search on a graph
 * determined by the I/O match between services.
 * <p>
 * The goal state is one in which all wanted variables are bound
 */
public interface IOComposer extends Composer {
    String planningTime = "planning";
    String planningMem = "planningMem";
    String compositionMem = "compositionMem";
}

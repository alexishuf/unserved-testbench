package br.ufsc.lapesd.unserved.testbench.util.jena;

import org.apache.jena.ext.com.google.common.base.Supplier;
import org.apache.jena.graph.*;
import org.apache.jena.shared.AddDeniedException;
import org.apache.jena.shared.DeleteDeniedException;
import org.apache.jena.shared.PrefixMapping;
import org.apache.jena.util.iterator.ExtendedIterator;

public class GraphReference implements Graph {
    private Supplier<Graph> refereeAccessor;

    public GraphReference(Supplier<Graph> graphAccessor) {
        this.refereeAccessor = graphAccessor;
    }

    public Graph getReferee() {
        return getRefereeAccessor().get();
    }

    public void setReferee(Graph graph) {
        setRefereeAccessor(() -> graph);
    }

    public Supplier<Graph> getRefereeAccessor() {
        return refereeAccessor;
    }

    public void setRefereeAccessor(Supplier<Graph> refereeAccessor) {
        this.refereeAccessor = refereeAccessor;
    }

    @Override
    public boolean dependsOn(Graph other) {
        return getReferee().dependsOn(other);
    }

    @Override
    public TransactionHandler getTransactionHandler() {
        return getReferee().getTransactionHandler();
    }

    @Override
    public Capabilities getCapabilities() {
        return getReferee().getCapabilities();
    }

    @Override
    public GraphEventManager getEventManager() {
        return getReferee().getEventManager();
    }

    @Override
    public GraphStatisticsHandler getStatisticsHandler() {
        return getReferee().getStatisticsHandler();
    }

    @Override
    public PrefixMapping getPrefixMapping() {
        return getReferee().getPrefixMapping();
    }

    @Override
    public void add(Triple t) throws AddDeniedException {
        getReferee().add(t);
    }

    @Override
    public void delete(Triple t) throws DeleteDeniedException {
        getReferee().delete(t);
    }

    @Override
    public ExtendedIterator<Triple> find(Triple m) {
        return getReferee().find(m);
    }

    @Override
    public ExtendedIterator<Triple> find(Node s, Node p, Node o) {
        return getReferee().find(s, p, o);
    }

    @Override
    public boolean isIsomorphicWith(Graph g) {
        return getReferee().isIsomorphicWith(g);
    }

    @Override
    public boolean contains(Node s, Node p, Node o) {
        return getReferee().contains(s, p, o);
    }

    @Override
    public boolean contains(Triple t) {
        return getReferee().contains(t);
    }

    @Override
    public void clear() {
        getReferee().clear();
    }

    @Override
    public void remove(Node s, Node p, Node o) {
        getReferee().remove(s, p, o);
    }

    @Override
    public void close() {
        getReferee().close();
    }

    @Override
    public boolean isEmpty() {
        return getReferee().isEmpty();
    }

    @Override
    public int size() {
        return getReferee().size();
    }

    @Override
    public boolean isClosed() {
        return getReferee().isClosed();
    }
}

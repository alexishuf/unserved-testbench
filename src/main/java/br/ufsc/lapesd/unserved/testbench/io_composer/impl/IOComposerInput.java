package br.ufsc.lapesd.unserved.testbench.io_composer.impl;

import br.ufsc.lapesd.unserved.testbench.composer.ComposerInput;
import br.ufsc.lapesd.unserved.testbench.model.Variable;
import org.apache.jena.rdf.model.Resource;

import javax.annotation.Nonnull;
import java.util.Set;

public interface IOComposerInput extends ComposerInput {
    @Nonnull
    Set<Resource> getWanted();
    @Nonnull
    Set<Variable> getWantedAsVariables();
    @Nonnull
    Set<Resource> getKnown();
    @Nonnull
    Set<Variable> getKnownAsVariables();

    void addWanted(@Nonnull Resource resource);

}

package br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph;


import com.google.common.base.Preconditions;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashSet;

public class LazyAlternatives<T extends Alternative<?>> implements Alternatives<T> {
    private Iterator<Iterator<T>> levelsIterator;
    private LinkedHashSet<T> instantiated = new LinkedHashSet<>();
    private Iterator<T> delegate = null;

    public LazyAlternatives(Collection<Iterator<T>> layers) {
        levelsIterator = layers.iterator();
    }

    @Override
    public Alternatives<T> duplicate() {
        //FIXME not implemented
        throw new UnsupportedOperationException("Not implemented, FIXME");
    }

    @Override
    public Iterator<T> iterator() {
        return new LazyIterator();
    }
    @Override
    public boolean remove(T object) {
        return instantiated.remove(object);
    }


    private class LazyIterator implements Iterator<T> {
        private Iterator<T> instantiatedIterator;
        private T next = null;

        @Override
        public boolean hasNext() {
            if (instantiatedIterator != null && instantiatedIterator.hasNext()) return true;
            instantiatedIterator = null; //exhausted
            if (next != null)
                return true; //has last delegate's result ready
            /* obtain a delegate, if not already has one */
            while ((delegate == null || !delegate.hasNext()) && levelsIterator.hasNext())
                delegate = levelsIterator.next();
            if (delegate == null || !delegate.hasNext())
                return false; //failed to obtain a delegate, give up
            /* call the delegate, and store the result */
            next = delegate.next();
            instantiated.add(next);
            return true;
        }

        @Override
        public T next() {
            /* Check consistency and initialize this.next, if needed. */
            Preconditions.checkState(hasNext());
            if (instantiatedIterator != null && instantiatedIterator.hasNext())
                return instantiatedIterator.next();
            assert next != null;
            return next;
        }
    }
}

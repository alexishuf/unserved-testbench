package br.ufsc.lapesd.unserved.testbench.io_composer.graph;

import br.ufsc.lapesd.unserved.testbench.io_composer.impl.IOComposerInput;

public interface CompositionWorkingGraphBuilder<V, E, A> {
    final class Result<V, E, A> {
        final CompositionWorkingGraph<V, E, A> graph;
        final V initial;
        final V goal;

        public Result(CompositionWorkingGraph<V, E, A> graph, V initial, V goal) {
            this.graph = graph;
            this.initial = initial;
            this.goal = goal;
        }
        public CompositionWorkingGraph<V, E, A> getGraph() {
            return graph;
        }
        public V getInitial() {
            return initial;
        }
        public V getGoal() {
            return goal;
        }
    }

    Result<V, E, A> build(IOComposerInput composerInput);
}

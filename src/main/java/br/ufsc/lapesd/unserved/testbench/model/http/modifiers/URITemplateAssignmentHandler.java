package br.ufsc.lapesd.unserved.testbench.model.http.modifiers;

import br.ufsc.lapesd.unserved.testbench.iri.UnservedX;
import br.ufsc.lapesd.unserved.testbench.model.Variable;
import com.github.fge.uritemplate.URITemplate;
import com.github.fge.uritemplate.URITemplateException;
import com.github.fge.uritemplate.vars.VariableMap;
import com.github.fge.uritemplate.vars.VariableMapBuilder;
import com.google.common.base.Preconditions;
import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.rdf.model.*;
import org.apache.jena.vocabulary.RDF;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.util.HashMap;

public class URITemplateAssignmentHandler implements AsHttpPropertyModifierHandler {
    private static Logger logger = LoggerFactory.getLogger(URITemplateAssignmentHandler.class);
    private HashMap<String, State> uriTemplates = new HashMap<>();
    private String resultURI = null;

    private class State {
        final String uriTemplate;
        final VariableMapBuilder mapBuilder = VariableMap.newBuilder();

        private State(String uriTemplate) {
            this.uriTemplate = uriTemplate;
        }

        public String expand() {
            try {
                URITemplate template = new URITemplate(this.uriTemplate);
                return template.toString(mapBuilder.freeze());
            } catch (URITemplateException e) {
                logger.debug("Failed to expand template {}", uriTemplate);
                return null;
            }
        }
    }


    @Override
    public void handle(Resource modifier, Variable variable) {
        if (!modifier.hasProperty(RDF.type, UnservedX.HTTP.URITemplateAssignment)) return;

        Statement statement = modifier.getProperty(UnservedX.HTTP.uriTemplate);
        if (statement == null || !statement.getObject().isLiteral()) return;
        String uriTemplate = statement.getLiteral().getLexicalForm();

        statement = modifier.getProperty(UnservedX.HTTP.uriTemplateVariable);
        if (statement == null || !statement.getObject().isLiteral()) return;
        String variableName = statement.getLiteral().getLexicalForm();

        String variableValue = getVariableStringValue(variable);
        if (variableValue == null) return;

        resultURI = null; // state will surely change

        State state = uriTemplates.getOrDefault(uriTemplate, null);
        if (state == null) uriTemplates.put(uriTemplate, state = new State(uriTemplate));
        state.mapBuilder.addScalarValue(variableName, variableValue);
    }

    @Override
    public boolean isComplete() {
        /* com.github.fge.uritemplate API has no way to get variables of a template */
        return false;
    }

    @Override
    public boolean canGet() {
        return getResultURI() != null;
    }

    @Override
    public RDFNode getResultNode(@Nonnull Model model) {
        Preconditions.checkState(canGet());
        return model.createTypedLiteral(getResultURI(), XSDDatatype.XSDstring);
    }

    @Override
    public void close() {
    }

    private String getVariableStringValue(Variable variable) {
        Literal literalValue = variable.getLiteralValue();
        return literalValue != null ? literalValue.getLexicalForm() : null;
    }

    private String getResultURI() {
        if (resultURI == null) {
            for (State state : uriTemplates.values()) {
                String expanded = state.expand();
                if (expanded != null) {
                    resultURI = expanded;
                    break;
                }
            }
        }
        return resultURI;
    }
}

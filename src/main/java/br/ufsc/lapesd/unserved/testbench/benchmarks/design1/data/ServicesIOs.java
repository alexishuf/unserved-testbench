package br.ufsc.lapesd.unserved.testbench.benchmarks.design1.data;

import com.google.common.base.Preconditions;
import org.apache.commons.lang3.tuple.ImmutablePair;

import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.util.Comparator.comparingInt;

public class ServicesIOs implements Serializable {
    private ServiceAltGraph graph;
    private int nextClassId = 1;
    private Map<Integer, Description> descriptions = new LinkedHashMap<>();
    private Set<Integer> firstInputClassIds = new LinkedHashSet<>();
    private Map<Integer, List<Integer>> outputsOfInterest = new HashMap<>();

    private ServicesIOs(ServiceAltGraph graph) {
        this.graph = graph;
    }

    public Collection<Description> getDescriptions() {
        return Collections.unmodifiableCollection(descriptions.values());
    }

    public Set<Integer> getInputsClassIds() {
        return Collections.unmodifiableSet(firstInputClassIds);
    }

    public Set<Integer> getOutputClassIds(int parallelSequences, int length) {
        Preconditions.checkArgument(outputsOfInterest.containsKey(length));
        List<Integer> list = outputsOfInterest.get(length);
        Preconditions.checkArgument(parallelSequences <= list.size());
        return list.stream().limit(parallelSequences).collect(Collectors.toSet());
    }

    public Set<Integer> getClassIds() {
        return IntStream.range(1, nextClassId).mapToObj(Integer::new).collect(Collectors.toSet());
    }

    public static ServicesIOs withRandomIOs(ServiceAltGraph graph,
                                            ServicesIOFactors factors,
                                            Set<Integer> lengths) {

        ServicesIOs descriptions = new ServicesIOs(graph);
        descriptions.initRandom(factors, lengths);
        return descriptions;
    }

    private void initRandom(ServicesIOFactors factors, Set<Integer> lengths) {
        Preconditions.checkState(graph.getEdges().stream().map(ImmutablePair::getRight)
                .map(Collection::size).allMatch(i -> i == 1));
        graph.getServices().forEach(this::getDescription);
        assignRandomIOs(factors);
        assignSpecialIOs(lengths);
    }

    private void assignSpecialIOs(Set<Integer> lengths) {
        firstInputClassIds.clear();
        Description first = getDescription(graph.getFirst());
        first.addInput(getNextClassId());
        firstInputClassIds.addAll(first.getInputs());

        outputsOfInterest.clear();
        lengths.forEach(length ->
            outputsOfInterest.put(length, graph.getParallelSequences().stream().map(seq -> {
                int classId = getNextClassId();
                ServiceGraph.Service service = seq.get(length - 1);
                Stream.concat(Stream.of(service), graph.getAlternatives(service).stream())
                        .map(this::getDescription).forEach(d -> d.addOutput(classId));
                return classId;
            }).collect(Collectors.toList())));
    }

    public static boolean
    canAssignRandomIOs(Collection<ServiceGraph.OutgoingConnector> outgoingConnectors,
                       int inputSetsSize) {
        List<Set<ServiceGraph.Service>> crossingSets = getCrossingSets(outgoingConnectors);
        return crossingSets.stream().map(Set::size).max(Integer::compareTo)
                .orElse(0) <= inputSetsSize;
    }

    private void assignRandomIOs(ServicesIOFactors factors) {
        int inputSetsSize = factors.getServiceAltGraphFactors().getGraphFactors().getInputSetsSize();

        for (ServiceGraph.Service service : graph.getServices()) {
            if (service.equals(graph.getFirst())) continue;
            List<ServiceGraph.OutgoingConnector> outConns = service.getIncoming().stream()
                    .map(ServiceGraph.IncomingConnector::getPrev).collect(Collectors.toList());
            int size = outConns.stream().map(Collection::size).findFirst().orElse(0);
            List<List<Integer>> connected = IntStream.range(0, size)
                    .mapToObj(i -> IntStream.range(0, factors.getConnectedIOs())
                            .mapToObj(j -> getNextClassId()).collect(Collectors.toList()))
                    .collect(Collectors.toList());

            Description description = getDescription(service);
            connected.stream().flatMap(List::stream).forEach(description::addInput);

            for (int i = 0; i < factors.getFreeOutputs(); i++)
                description.addOutput(getNextClassId());

            Map<ServiceGraph.Service, List<Integer>> assignedOutputs = new HashMap<>();
            List<Set<ServiceGraph.Service>> crossingSets = getCrossingSets(outConns);
            boolean canAssign = canAssignRandomIOs(outConns, inputSetsSize);
            Preconditions.checkArgument(canAssignRandomIOs(outConns, inputSetsSize));

            crossingSets.forEach(set -> {
                Iterator<List<Integer>> listIt = connected.iterator();
                set.forEach(pre -> {
                    assert listIt.hasNext();
                    Description desc = getDescription(pre);
                    List<Integer> outputs = listIt.next();
                    outputs.forEach(desc::addOutput);
                    assignedOutputs.put(pre, outputs);
                });
            });

            for (ServiceGraph.OutgoingConnector outConn : outConns) {
                Set<List<Integer>> assignedLists = outConn.stream()
                        .map(s -> assignedOutputs.getOrDefault(s, Collections.emptyList()))
                        .collect(Collectors.toSet());
                List<List<Integer>> toAssign = connected.stream()
                        .filter(list -> !assignedLists.contains(list)).collect(Collectors.toList());
                Iterator<List<Integer>> listIt = toAssign.iterator();
                while (listIt.hasNext()) {
                    outConn.stream()
                            .sorted(comparingInt(s -> getDescription(s).getOutputs().size()))
                            .forEach(pre ->{
                        if (listIt.hasNext()) {
                            Description preDesc = getDescription(pre);
                            listIt.next().forEach(preDesc::addOutput);
                        }
                    });
                }
            }
        }
    }

    private static boolean isCrossing(Collection<ServiceGraph.OutgoingConnector> conns,
                               ServiceGraph.Service service) {
        return conns.stream().filter(conn -> conn.contains(service)).count() > 2;
    }

    private static List<Set<ServiceGraph.Service>>
    getCrossingSets(Collection<ServiceGraph.OutgoingConnector> outConns) {
        List<Set<ServiceGraph.Service>> crossingSets = outConns.stream().flatMap(Set::stream)
                .filter(s -> isCrossing(outConns, s))
                .map(s -> {
                    HashSet<ServiceGraph.Service> set = new HashSet<>();
                    set.add(s);
                    for (int old = set.size(); old != set.size(); old = set.size()) {
                        set.forEach(s2 -> outConns.stream().filter(conn -> conn.contains(s2))
                                .forEach(conn -> conn.stream()
                                        .filter(s3 -> isCrossing(outConns, s3)).forEach(set::add)));
                    }
                    return set;
                }).distinct().collect(Collectors.toList());
        boolean noIntersection = crossingSets.stream().reduce((a, b) -> {
            Set<ServiceGraph.Service> intersection = new TreeSet<>(a);
            intersection.retainAll(b);
            return intersection;
        }).orElse(Collections.emptySet()).isEmpty();
        assert crossingSets.size() < 2 || noIntersection;
        return crossingSets;
    }

    private int getNextClassId() {
        return nextClassId++;
    }

    private Description getDescription(ServiceGraph.Service service) {
        Description d = descriptions.getOrDefault(service.getId(), null);
        if (d == null) {
            d = new Description(service);
            descriptions.put(service.getId(), d);
        }
        return d;
    }

    public static class Description implements Serializable {
        private final ServiceGraph.Service service;
        private LinkedHashSet<Integer> inputs = new LinkedHashSet<>();
        private LinkedHashSet<Integer> outputs = new LinkedHashSet<>();

        public Description(ServiceGraph.Service service) {
            this.service = service;
        }

        public ServiceGraph.Service getService() {
            return service;
        }

        private void addInput(int classId) {
            inputs.add(classId);
        }

        private void addOutput(int classId) {
            outputs.add(classId);
        }

        public LinkedHashSet<Integer> getInputs() {
            return inputs;
        }

        public LinkedHashSet<Integer> getOutputs() {
            return outputs;
        }
    }
}

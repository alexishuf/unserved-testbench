package br.ufsc.lapesd.unserved.testbench.benchmarks.experiment;

public interface Experiment extends AutoCloseable {
    void setUp() throws Exception;
    ExperimentResult run() throws ExperimentException;
    Factors getFactors();
}

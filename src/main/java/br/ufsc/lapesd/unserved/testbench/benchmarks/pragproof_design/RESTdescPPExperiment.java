package br.ufsc.lapesd.unserved.testbench.benchmarks.pragproof_design;

import br.ufsc.lapesd.unserved.testbench.Algorithm;
import br.ufsc.lapesd.unserved.testbench.AlgorithmFamily;
import br.ufsc.lapesd.unserved.testbench.benchmarks.experiment.ExperimentException;
import br.ufsc.lapesd.unserved.testbench.benchmarks.restdesc.RESTdescDescriptionGenerator;
import br.ufsc.lapesd.unserved.testbench.benchmarks.restdesc.RESTdescPragProofApp;
import com.google.common.base.Preconditions;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RESTdescPPExperiment extends BasicExperiment {
    private File restdescGoal;
    private File restdescInitial;

    public RESTdescPPExperiment(PPFactors factors) {
        super(factors);
        Preconditions.checkArgument(getFactors().getAlgorithm().getFamily()
                == AlgorithmFamily.RESTdescPP);
    }

    @Override
    public void setUp() throws Exception {
        super.setUp();
        restdescGoal = Files.createTempFile("restdesc-goal", ".n3").toFile();
        restdescInitial = Files.createTempFile("restdesc-initial", ".ttl").toFile();
        new RESTdescDescriptionGenerator.Builder().setChainLength(1)
                .setUseExistentials(getFactors().getAlgorithm() == Algorithm.RESTdescPPExistentials)
                .setGoalOut(restdescGoal)
                .setInitialOut(restdescInitial)
                .build().run();
    }

    @Override
    public void close() throws Exception {
        FileUtils.forceDelete(restdescGoal);
        FileUtils.forceDelete(restdescInitial);
        super.close();
    }

    @Override
    protected void run(File workflowFile, File timesFile, List<File> fileList) throws ExperimentException {
        List<String> args = new ArrayList<>(Arrays.asList(RESTdescPragProofApp.class.getName(),
                "--preheat-count", String.valueOf(getFactors().getPreheatCount()),
                "--measure-reasoner-spawn", "--measure-reasoner-spawn-and-parse"));
        if (getFactors().getAlgorithm() != Algorithm.RESTdescPPWithUnstableReasoner)
            args.add("--cheat-unstable-reasoner");
        if (getFactors().getExecute()) args.add("--execute");
        args.addAll(Arrays.asList(
                "--workflow-out", workflowFile.getAbsolutePath(),
                "--times-out", timesFile.getAbsolutePath(),
                "--goal", restdescGoal.getAbsolutePath(),
                restdescInitial.getAbsolutePath()));
        runChildJVM(args, fileList);
    }

    @Override
    public File createDescription(int chainLength, int alternatives, int conditions,
                                  String rel, boolean preconditions) throws Exception {
        File file = Files.createTempFile("description", ".n3").toFile();
        file.deleteOnExit();
        RESTdescDescriptionGenerator.Builder builder = new RESTdescDescriptionGenerator.Builder()
                .setChainLength(chainLength)
                .setConditions(conditions)
                .setAlternatives(alternatives)
                .setUseExistentials(getFactors().getAlgorithm() == Algorithm.RESTdescPPExistentials)
                .setOut(file);
        if (rel != null) builder.setRel(rel);
        builder.build().run();
        return file;
    }
}

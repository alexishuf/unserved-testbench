package br.ufsc.lapesd.unserved.testbench.httpcomponents.extractors;

import br.ufsc.lapesd.unserved.testbench.process.Context;

/**
 * Result of an {@link HttpResponseExtractor}
 */
public interface HttpResponseExtraction {
    void apply(Context context);
}

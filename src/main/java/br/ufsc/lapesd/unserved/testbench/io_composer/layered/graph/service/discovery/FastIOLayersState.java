package br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.discovery;

import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.Condition;
import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.ConditionEvaluator;
import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.atomic.VariableSpec;
import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.index.VariableSpecIndex;
import br.ufsc.lapesd.unserved.testbench.io_composer.impl.IOComposerInput;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.*;
import br.ufsc.lapesd.unserved.testbench.model.Message;
import br.ufsc.lapesd.unserved.testbench.model.Variable;
import br.ufsc.lapesd.unserved.testbench.util.NaiveTransitiveClosureGetter;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.jena.vocabulary.RDFS;

import javax.annotation.Nonnull;
import java.util.*;

import static org.apache.commons.lang3.tuple.ImmutablePair.of;

public class FastIOLayersState extends IOLayersState  {
    protected InputServiceIndex inputIndex;
    protected  @Nonnull Set<Node> psBlacklist = new HashSet<>();
    protected MyVariableSpecEvaluator myVariableSpecEvaluator = new MyVariableSpecEvaluator();
    protected VariableSpecIndex<ImmutablePair<Variable, Node>> outputs;

    public FastIOLayersState(InputServiceIndex inputIndex) {
        this(inputIndex, false);
    }

    public FastIOLayersState(InputServiceIndex inputIndex, boolean listingSubclasses) {
        this.inputIndex = inputIndex;
        if (listingSubclasses)
            outputs = new VariableSpecIndex<>(new NaiveTransitiveClosureGetter(
                    RDFS.subClassOf, true));
        else {
            outputs = VariableSpecIndex.withClosureOnPut(inputIndex.getSuperClassGetter());
        }
    }

    @Override
    public void advance(@Nonnull Collection<Node> nodes) {
        super.advance(nodes);
//        Stopwatch x = Stopwatch.createStarted();
        psBlacklist.clear();
        psBlacklist.addAll(nodes);
//        ForwardLayeredServiceGraphBuilder.outputsIndexGet += x.elapsed(TimeUnit.MICROSECONDS);
        for (Node node : nodes) {
            for (Variable output : node.getOutputs())
                outputs.put(output.asSpec(), of(output, node));
        }
    }

    @Nonnull
    @Override
    public ConditionEvaluator getVariableSpecsEvaluator() {
        return myVariableSpecEvaluator;
    }

    @Nonnull
    @Override
    public NodesSupplier<? extends LayersState> createNodesSupplier() {
        return new MyNodesSupplier();
    }

    @Nonnull
    @Override
    public ProviderSetsSupplier createProviderSetsSupplier() {
        return new MyProviderSetsSupplier();
    }

    private class MyNodesSupplier implements NodesSupplier<FastIOLayersState> {
        boolean active = false;

        @Nonnull
        @Override
        public Set<Node> getNodes(@Nonnull FastIOLayersState state, @Nonnull Set<Message> blacklist) {
            assert active;
            assert state == FastIOLayersState.this;
            ConditionEvaluator vsEvaluator = state.getVariableSpecsEvaluator();

            HashSet<Node> set = new HashSet<>(128);
            Set<Node> visited = new HashSet<>(512);
            for (VariableSpec spec : state.getNewlyKnown())
                inputIndex.getConsumerInputs().getNonDistinctStream(spec).forEach(p -> {

                    if (!visited.contains(p.right)) {
                        visited.add(p.right);
                        boolean blacklisted = blacklist.contains(p.right.getConsequent());

                        boolean unsatisfied = false;
                        for (Variable in : p.right.getInputs()) {
                            if ((unsatisfied = !vsEvaluator.evaluate(in.asSpec()))) break;
                        }
                        if (!blacklisted && !unsatisfied) set.add(p.right);
                    }
                });
            state.advance(set);
            return set;
        }

        @Override
        public boolean isActive() {
            return active;
        }

        @Override
        public void begin(IOComposerInput composerInput, @Nonnull StartNode startNode, @Nonnull EndNode endNode) {
            inputIndex.addModel(composerInput.getSkolemizedUnion());
            active = true;
        }

        @Override
        public void end() {
            inputIndex.clear();
            active = false;
        }
    }

    protected class MyProviderSetsSupplier implements ProviderSetsSupplier {
        boolean active = false;

        @Nonnull
        @Override
        public Map<Node, ProviderSet> get(@Nonnull Collection<Node> targetNodes) {
            Map<Node, ProviderSet> result = new HashMap<>(targetNodes.size(), .9f);
            for (Node node : targetNodes) {
                result.put(node, get(node));
            }
            return result;
        }

        public ProviderSet get(@Nonnull Node node) {
            assert isActive();
            IOProviderHashSet ps = new IOProviderHashSet();
            for (Variable in : node.getInputs()) {
                ps.addTarget(in);
                outputs.getNonDistinctStream(in.asSpec()).forEach(p -> {
                    if (!psBlacklist.contains(p.right))
                        ps.add(in, p.right, p.left);
                });
            }
            return ps;
        }

        @Override
        public void update(@Nonnull Collection<Node> nodes) {
            psBlacklist.removeAll(nodes);
        }

        @Override
        public boolean isActive() {
            return active;
        }

        @Override
        public void begin(IOComposerInput composerInput,
                          @Nonnull StartNode startNode, @Nonnull EndNode endNode) {
            active = true;
        }

        @Override
        public void end() {
            active = false;
        }
    }

    private class MyVariableSpecEvaluator implements ConditionEvaluator {
        @Override
        public <T extends Condition> boolean evaluate(@Nonnull T condition) throws IllegalArgumentException {
            assert condition instanceof VariableSpec;
            VariableSpec spec = (VariableSpec) condition;
            return outputs.getFirst(spec) != null;
        }

        @Override
        public <T extends Condition> boolean canEvaluate(@Nonnull T condition) {
            return condition instanceof VariableSpec;
        }
    }
}

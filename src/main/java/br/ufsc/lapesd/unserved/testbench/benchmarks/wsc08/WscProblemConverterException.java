package br.ufsc.lapesd.unserved.testbench.benchmarks.wsc08;

public class WscProblemConverterException extends Exception {
    public WscProblemConverterException() {
    }

    public WscProblemConverterException(String s) {
        super(s);
    }

    public WscProblemConverterException(String s, Throwable throwable) {
        super(s, throwable);
    }
}

package br.ufsc.lapesd.unserved.testbench.benchmarks.pragproof_design;

import br.ufsc.lapesd.unserved.testbench.Algorithm;
import br.ufsc.lapesd.unserved.testbench.benchmarks.experiment.ExperimentDesignLevels;
import br.ufsc.lapesd.unserved.testbench.benchmarks.experiment.Factors;
import com.google.common.base.Preconditions;

import java.util.Map;

public class PPExperimentDesignLevels implements ExperimentDesignLevels {
    private Values low, high, fallback;

    @Override public Factors createFactors(Map<String, String> levels) {
        int chain = getValue(levels, "ChainLength", Integer.class),
                alternatives = getValue(levels, "Alternatives", Integer.class),
                ioCount = getValue(levels, "IOCount", Integer.class);
        PPFactors.Builder builder = new PPFactors.Builder(
                chain,
                getValue(levels, "Algorithm", Algorithm.class))
                .withAlternatives(alternatives)
                .withIoCount(ioCount)
                .withPreheatCount(getValue(levels, "PreheatCount", Integer.class))
                .withExecute(getValue(levels, "Execute", Boolean.class));

        DummiesParameters parameters = getValue(levels, "Dummies", DummiesParameters.class);
        int nonDummyChain = parameters.nonDummyChain == -2 ? chain : parameters.nonDummyChain;
        int nonDummyIOCount = parameters.nonDummyIOCount == -2 ? ioCount
                : parameters.nonDummyIOCount;
        int nonDummyAlternatives = parameters.nonDummyAlternatives == -2 ? alternatives
                : parameters.nonDummyAlternatives;
        builder.withNonDummyChain(nonDummyChain)
                .withNonDummyIOCount(nonDummyIOCount)
                .withNonDummyAlternatives(nonDummyAlternatives);

        return builder.build();
    }

    private <T> T getValue(Map<String, String> map, String name, Class<T> theClass) {
        return getValue(name, map.getOrDefault(name, null), theClass);
    }
    private <T> T getValue(String name, String value, Class<T> theClass) {
        int level = value == null ? 0 : Integer.parseInt(value);
        Values values = level == -1 ? low : (level == 1 ? high : fallback);
        return values.getValue(name, theClass);
    }

    private static class Values {
        private int chainLength, alternatives, ioCount, preheatCount;
        private DummiesParameters dummies;
        private boolean execute;
        private Algorithm algorithm;

        private Object getValue(String name) {
            switch (name) {
                case "ChainLength": return chainLength;
                case "Alternatives": return alternatives;
                case "IOCount": return ioCount;
                case "Dummies": return dummies;
                case "Algorithm": return algorithm;
                case "PreheatCount": return preheatCount;
                case "Execute": return execute;
            }
            throw new IllegalArgumentException("Unknown name " + name);
        }

        private <T> T getValue(String name, Class<T> theClass) {
            Object o = getValue(name);
            if (o != null)
                Preconditions.checkArgument(theClass.isAssignableFrom(o.getClass()));
            //noinspection unchecked
            return (T)o;
        }
    }

    private static class DummiesParameters {
        private int nonDummyChain, nonDummyAlternatives, nonDummyIOCount;
    }
}

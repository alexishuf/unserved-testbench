package br.ufsc.lapesd.unserved.testbench.iri;

import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SPARMediaType {
    public static final String PREFIX = "http://w3id.org/spar/mediatype/";

    private static final Pattern mediaTypePattern =
            Pattern.compile("https?://w3id\\.org/spar/mediatype/([^/]+/[^/]+)");
    public static String extractMediaType(String uri) {
        Matcher matcher = mediaTypePattern.matcher(uri);
        return matcher.matches() ? matcher.group(1) : null;
    }

    public static Resource getMediaType(String name) {
        return ResourceFactory.createResource(PREFIX + name);
    }
}

package br.ufsc.lapesd.unserved.testbench.reasoner.impl;

import br.ufsc.lapesd.unserved.testbench.input.RDFInput;
import br.ufsc.lapesd.unserved.testbench.reasoner.N3Reasoner;
import br.ufsc.lapesd.unserved.testbench.reasoner.N3ReasonerException;
import br.ufsc.lapesd.unserved.testbench.util.N3FormatShim;
import com.google.common.base.Preconditions;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFLanguages;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Wraps a N3Reasoner to support any input format that is not supported by the reasoner, but
 * which is supported by Jena riot API.
 */
public class RiotEnhancedN3Reasoner implements N3Reasoner {
    private final N3Reasoner delegate;

    public RiotEnhancedN3Reasoner(N3Reasoner delegate) {
        Preconditions.checkArgument(!delegate.supportedInputLanguages().isEmpty());
        this.delegate = delegate;
    }

    @Override
    public List<Lang> supportedInputLanguages() {
        List<Lang> list = new ArrayList<>();
        list.addAll(RDFLanguages.getRegisteredLanguages());
        return list;
    }

    @Override
    public List<Lang> supportedDownloadLanguages() {
        return delegate.supportedInputLanguages();
    }

    @Override
    public void addInput(RDFInput input) throws IOException {
        input = N3FormatShim.convertInput(delegate.supportedInputLanguages(), input);
        delegate.addInput(input);
    }

    @Override
    public void setQueryFile(@Nonnull RDFInput file) {
        delegate.setQueryFile(file);
    }

    @Override
    public void queryDeductiveClosure() {
        delegate.queryDeductiveClosure();
    }

    @Override
    public boolean getExplainProof() {
        return delegate.getExplainProof();
    }

    @Override
    public void setExplainProof(boolean explainProof) {
        delegate.setExplainProof(explainProof);
    }

    @Override
    public InputStream run() throws N3ReasonerException {
        return delegate.run();
    }

    @Override
    public boolean isRunning() {
        return delegate.isRunning();
    }

    @Override
    public N3ReasonerException getError() {
        return delegate.getError();
    }

    @Override
    public EYEProcess abort() {
        return delegate.abort();
    }

    @Override
    public void waitFor() throws InterruptedException, N3ReasonerException {
        delegate.waitFor();
    }

    @Override
    public boolean waitFor(long timeout, TimeUnit unit) throws InterruptedException,
            N3ReasonerException {
        return delegate.waitFor(timeout, unit);
    }
}

package br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service;


/**
 * A {@link ProviderSetsSupplier} that implements this interface, does not generate ProviderSets
 * that differ only on the choice of a known variable. Such choices are expected to be done late,
 * by auto fix mechanisms during execution.
 */
public interface AutoFixProviderSetsSupplier extends ProviderSetsSupplier {
}

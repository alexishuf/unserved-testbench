package br.ufsc.lapesd.unserved.testbench.composer;

public class NoPlanComposerException extends ComposerException {
    public NoPlanComposerException(String s) {
        super(s);
    }

    public NoPlanComposerException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public NoPlanComposerException(Throwable throwable) {
        super(throwable);
    }
}

package br.ufsc.lapesd.unserved.testbench.model.http.impl;

import br.ufsc.lapesd.unserved.testbench.iri.HTTP;
import br.ufsc.lapesd.unserved.testbench.iri.UnservedX;
import br.ufsc.lapesd.unserved.testbench.model.http.AsHttpProperty;
import br.ufsc.lapesd.unserved.testbench.util.ImplementationByType;
import org.apache.jena.enhanced.EnhGraph;
import org.apache.jena.enhanced.EnhNode;
import org.apache.jena.enhanced.Implementation;
import org.apache.jena.graph.Node;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.impl.ResourceImpl;

public class AsHttpPropertyImpl extends ResourceImpl implements AsHttpProperty {
    public static Implementation factory
            = new ImplementationByType(UnservedX.HTTP.AsHttpProperty.asNode()) {
        @Override
        public EnhNode wrap(Node node, EnhGraph eg) {
            return new AsHttpPropertyImpl(node, eg);
        }
    };

    public AsHttpPropertyImpl(Node n, EnhGraph m) {
        super(n, m);
    }

    @Override
    public Property getHttpProperty() {
        Statement statement = getProperty(UnservedX.HTTP.httpProperty);
        if (statement == null || !statement.getObject().isResource()
                || !statement.getResource().canAs(Property.class)) {
            return null;
        }
        Property property = statement.getResource().as(Property.class);
        if (HTTP.getInstance().getModel().containsResource(property))
            return HTTP.getInstance().getModel().createProperty(property.getURI());
        return property;
    }

    @Override
    public Resource getHttpResource() {
        Statement stmt = getProperty(UnservedX.HTTP.httpResource);
        return stmt == null || !stmt.getObject().isResource() ? null : stmt.getResource();
    }

    @Override
    public Resource getModifier() {
        Statement stmt = getProperty(UnservedX.HTTP.modifier);
        return stmt == null || !stmt.getObject().isResource() ? null : stmt.getResource();
    }
}

package br.ufsc.lapesd.unserved.testbench.process.model.impl;

import br.ufsc.lapesd.unserved.testbench.iri.UnservedP;
import br.ufsc.lapesd.unserved.testbench.process.model.Action;
import br.ufsc.lapesd.unserved.testbench.process.model.ActionFactory;
import br.ufsc.lapesd.unserved.testbench.process.model.Sequence;
import br.ufsc.lapesd.unserved.testbench.util.ImplementationByType;
import org.apache.jena.enhanced.EnhGraph;
import org.apache.jena.enhanced.EnhNode;
import org.apache.jena.enhanced.Implementation;
import org.apache.jena.graph.Node;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.rdf.model.impl.ResourceImpl;
import org.apache.jena.vocabulary.RDF;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SequenceImpl extends ResourceImpl implements Sequence {
    public static Implementation factory = new ImplementationByType(UnservedP.Sequence.asNode()) {
        @Override
        public EnhNode wrap(Node node, EnhGraph eg) {
            return new SequenceImpl(node, eg);
        }
    };

    public SequenceImpl(Node n, EnhGraph m) {
        super(n, m);
    }

    @Override
    public List<List<Action>> getMembersLists() {
        List<List<Action>> lists = new ArrayList<>();
        StmtIterator it = getModel().listStatements(this, UnservedP.members, (RDFNode) null);
        while (it.hasNext()) {
            List<Action> members = new ArrayList<>();
            Resource list = it.next().getResource();
            while (!list.equals(RDF.nil)) {
                Statement stmt = list.getProperty(RDF.first);
                if (stmt != null) {
                    Action action = ActionFactory.asAction(stmt.getResource());
                    if (action != null) members.add(action);
                }
                stmt = list.getProperty(RDF.rest);
                list = stmt == null ? RDF.nil : stmt.getResource();
            }
            lists.add(members);
        }
        return lists;
    }

    @Override
    public List<Action> getMembers() {
        return getMembersLists().stream().reduce((l, r) -> {
            List<Action> list = new ArrayList<>(l.size() + r.size());
            list.addAll(l);
            list.addAll(r);
            return list;
        }).orElse(Collections.emptyList());
    }
}

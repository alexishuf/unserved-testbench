package br.ufsc.lapesd.unserved.testbench.util;

import br.ufsc.lapesd.unserved.testbench.input.ConvertedRDFInput;
import br.ufsc.lapesd.unserved.testbench.input.RDFInput;
import br.ufsc.lapesd.unserved.testbench.input.RDFInputModel;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFFormat;
import org.apache.jena.riot.RDFWriterRegistry;

import java.util.List;

public class N3FormatShim {
    public static RDFInput convertInput(List<Lang> supportedLangs, RDFInput input) {
        if (supportedLangs.contains(input.getLang())) return input;
        return new ConvertedRDFInput(input, getFormat(supportedLangs));
    }

    public static RDFInput createInput(List<Lang> supportedLangs, Model model) {
        return new RDFInputModel(model, getFormat(supportedLangs));
    }

    private static RDFFormat getFormat(List<Lang> supportedLangs) {
        return supportedLangs.stream().map(RDFWriterRegistry::defaultSerialization)
                .filter(RDFWriterRegistry::contains).findFirst().orElseThrow(() ->
                        new RuntimeException("No format with a riot writer"));
    }
}

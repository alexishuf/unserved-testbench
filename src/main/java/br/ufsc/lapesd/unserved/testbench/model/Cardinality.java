package br.ufsc.lapesd.unserved.testbench.model;

import org.apache.jena.rdf.model.Resource;

/**
 * Enhanced node for unserved:Cardinality
 */
public interface Cardinality extends Resource {
}

package br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service;

import javax.annotation.Nonnull;

public interface JumpNode extends Node{
    @Nonnull
    ProviderSet getProviderSet();
}

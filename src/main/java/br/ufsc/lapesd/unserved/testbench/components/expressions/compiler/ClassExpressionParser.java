package br.ufsc.lapesd.unserved.testbench.components.expressions.compiler;

import br.ufsc.lapesd.unserved.testbench.components.expressions.*;
import br.ufsc.lapesd.unserved.testbench.util.BFSBGPIterator;
import com.google.common.base.Preconditions;
import org.apache.jena.rdf.model.*;
import org.apache.jena.vocabulary.OWL;
import org.apache.jena.vocabulary.OWL2;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static br.ufsc.lapesd.unserved.testbench.util.ModelStreams.objectsOfProperty;

/**
 * Parses an RDF/OWL class into an {@link ClassExpression} instance.
 */
public class ClassExpressionParser {
    private static final Set<Property> connectiveHints = Stream.of(OWL2.unionOf,
            OWL2.intersectionOf, OWL2.complementOf).collect(Collectors.toSet());
    private static final Set<Property> cardinalityHints = Stream.of(OWL2.maxCardinality,
            OWL2.maxQualifiedCardinality, OWL2.minCardinality, OWL2.minQualifiedCardinality,
            OWL2.cardinality, OWL2.qualifiedCardinality).collect(Collectors.toSet());
    private static final Set<Property> propertyRestrictionHints = Stream.of(OWL2.someValuesFrom,
            OWL2.allValuesFrom, OWL2.hasValue, OWL2.hasSelf).collect(Collectors.toSet());
    private static final Set<Property> enumerationHints = Collections.singleton(OWL2.oneOf);

    /**
     * Produce a ClassRule instance that when evaluated will be equivalent to the provided class.
     *
     * @param aClass The RDFS or OWL class.
     * @return a {@link ClassExpression}
     */
    @Nonnull
    public ClassExpression parse(@Nonnull Resource aClass) throws ClassExpressionParserException {
        return parse(aClass, Collections.singleton(aClass));
    }

    private ClassExpression parse(@Nonnull Resource aClass, Set<Resource> names) throws ClassExpressionParserException {
        assert names.contains(aClass);
        Preconditions.checkNotNull(aClass);
        Preconditions.checkArgument(aClass.getModel() != null);

        Set<Property> properties = aClass.listProperties().toList().stream()
                .map(Statement::getPredicate).collect(Collectors.toSet());
        ArrayList<Property> connective = new ArrayList<>(), cardinality = new ArrayList<>(),
                propertyRestriction = new ArrayList<>(), enumeration = new ArrayList<>();
        for (Property observed : properties) {
            if (connectiveHints.contains(observed)) connective.add(observed);
            if (cardinalityHints.contains(observed)) cardinality.add(observed);
            if (propertyRestrictionHints.contains(observed)) propertyRestriction.add(observed);
            if (enumerationHints.contains(observed)) enumeration.add(observed);
        }
        int totalCount = connective.size() + cardinality.size() + propertyRestriction.size()
                + enumeration.size();
        if (totalCount > 1)
            throw ex("%s has conflicting CE hints.", aClass);

        Set<Resource> equiv = objectsOfProperty(aClass, OWL.equivalentClass, Resource.class)
                .collect(Collectors.toSet());
        if (!equiv.isEmpty())
            return getEquivalent(aClass, names, totalCount, equiv);
        if (!connective.isEmpty())
            return getConnective(aClass, names, connective.get(0));
        if (!cardinality.isEmpty())
            return getCardinality(aClass, names, cardinality.get(0));
        if (!propertyRestriction.isEmpty())
            return getPropertyRestriction(aClass, names, propertyRestriction.get(0));
        if (!enumeration.isEmpty())
            return getEnumeration(aClass, names, enumeration.get(0));
        return new AtomicClassExpression(aClass);
    }

    private ClassExpression getEquivalent(@Nonnull Resource aClass, Set<Resource> names, int totalCount, Set<Resource> equiv) throws ClassExpressionParserException {
        if (totalCount >= 1)
            throw ex("%s has both owl:equivalentClass and CE hints.", aClass);
        if (equiv.size() > 1)
            throw ex("Multiple owl:equivalentClass properties are not supported (%s).", aClass);
        Resource equivClass = equiv.iterator().next();
        if (names.contains(equivClass))
            throw ex("%s has an owl:equivalentClass loop", aClass);
        HashSet<Resource> newNames = new HashSet<>(names);
        newNames.add(equivClass);
        return parse(equivClass, newNames);
    }

    private ClassExpression getConnective(Resource aClass, Set<Resource> names, Property property)
            throws ClassExpressionParserException {
        Connective.ConnectiveType type = Connective.ConnectiveType.fromProperty(property);
        assert type != null;
        ArrayList<ClassExpression> children = new ArrayList<>();
        Resource r = aClass.getPropertyResourceValue(property);
        if (r == null) throw ex("?x in {%s %s ?x} is not an resource");

        switch (type) {
            case COMPLEMENT:
                children.add(parse(r));
                break;
            case UNION:
            case INTERSECTION:
                if (!r.canAs(RDFList.class))
                    throw ex("?x in {%s %s ?x} is not a list", aClass, property);
                RDFList list = r.as(RDFList.class);
                for (RDFNode node : list.iterator().toList()) {
                    if (!node.isResource())
                        throw ex("?list in {%s %s ?list} has a non-resource", aClass, property);
                    children.add(parse(node.asResource()));
                }
                break;
        }
        return new Connective(type, children, names);
    }

    @Nonnull
    private ClassExpression getCardinality(Resource aClass, Set<Resource> names, Property property)
                throws ClassExpressionParserException {
        Cardinality.CardinalityType type = Cardinality.CardinalityType.fromProperty(property);
        assert type != null;
        long value = aClass.getProperty(property).getLiteral().getLong();
        Resource onPropertyR = aClass.getProperty(OWL2.onProperty).getResource();
        if (!onPropertyR.canAs(Property.class))
            throw ex("?x in {%s owl:onProperty ?x} should be a RDF property", aClass);
        Property onProperty = onPropertyR.as(Property.class);

        if (type.isQualified()) {
            Resource onClass = aClass.getProperty(OWL2.onClass).getResource();
            return new Cardinality(type, onProperty, value, names, parse(onClass));
        } else {
            return new Cardinality(type, onProperty, value, names);
        }
    }

    @Nonnull
    private ClassExpression
    getPropertyRestriction(Resource aClass, Set<Resource> names, Property property)
            throws ClassExpressionParserException {
        PropertyRestriction.RestrictionType type;
        type = PropertyRestriction.RestrictionType.fromProperty(property);
        assert type != null;
        Resource r = aClass.getProperty(OWL2.onProperty).getResource();
        Property onProperty = r.canAs(Property.class) ? r.as(Property.class) : null;
        if (onProperty == null)
            throw ex("?x in {%s owl:onProperty ?x} must be a RDF Property", aClass);

        if (type == PropertyRestriction.RestrictionType.HAS_VALUE) {
            RDFNode value = aClass.getProperty(OWL2.hasValue).getObject();
            return new PropertyRestriction(type, onProperty, names, value);
        } else if (type == PropertyRestriction.RestrictionType.HAS_SELF) {
            boolean hasSelf = aClass.getProperty(OWL2.hasSelf).getLiteral().getBoolean();
            return new PropertyRestriction(type, onProperty, names, hasSelf);
        } else {
            Resource rangeClass = aClass.getProperty(property).getResource();

            boolean isLiteral = BFSBGPIterator.from(rangeClass, RDF.type).forward(RDFS.subClassOf)
                    .toStream().anyMatch(x -> x.equals(RDFS.Literal));
            if (isLiteral) {
                throw ex("%s owl:%s %s is not supported yet as %s is a literal class",
                        aClass, property.getLocalName(), rangeClass, rangeClass);
            }
            return new PropertyRestriction(type, onProperty, names, parse(rangeClass));
        }
    }

    public ClassExpression getEnumeration(Resource aClass, Set<Resource> names, Property property)
            throws ClassExpressionParserException {
        Resource r = aClass.getProperty(OWL2.oneOf).getResource();
        RDFList list = r.canAs(RDFList.class) ? r.as(RDFList.class) : null;
        if (list == null)
            throw ex("?x in {%s owl:oneOf ?x} is not a list.", aClass);

        Set<Resource> individuals = list.asJavaList().stream().filter(RDFNode::isResource)
                .map(RDFNode::asResource).collect(Collectors.toSet());
        if (individuals.size() != list.size()) {
            throw ex("list of %s owl:oneOf has %d non-resource members.", aClass,
                    list.size()-individuals.size() );
        }

        return new Enumeration(individuals, names);
    }

    private ClassExpressionParserException ex(String message, Object... objects) {
        return new ClassExpressionParserException(String.format(message, objects));
    }

}

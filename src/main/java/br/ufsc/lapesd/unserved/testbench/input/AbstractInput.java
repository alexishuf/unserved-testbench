package br.ufsc.lapesd.unserved.testbench.input;

import javax.annotation.Nullable;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public abstract class AbstractInput implements Input {
    private boolean closed = false;
    private final String mediaType;
    private String baseURI = null;

    protected AbstractInput(String mediaType) {
        this.mediaType = mediaType;
    }

    @Override
    public String getMediaType() {
        return mediaType;
    }

    @Override
    public String toURI() throws IOException {
        return "file://" + toFile().toURI().getRawPath();
    }

    @Override
    public InputStream createInputStream() throws IOException {
        return new FileInputStream(toFile());
    }

    public void setBaseURI(String baseURI) {
        this.baseURI = baseURI;
    }

    @Nullable
    @Override
    public String getBaseURI() {
        return baseURI;
    }

    @Override
    public void close() throws IOException {
        closed = true;
    }
}

package br.ufsc.lapesd.unserved.testbench.process.data.action;

import br.ufsc.lapesd.unserved.testbench.model.Variable;
import br.ufsc.lapesd.unserved.testbench.process.data.DataContext;
import com.google.common.base.Preconditions;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collections;
import java.util.Set;

public class SetVariable implements DataContextAction {
    private static Logger logger = LoggerFactory.getLogger(SetVariable.class);
    @Nonnull private final Variable var;
    private final Resource representation;
    private final Resource resourceValue;
    private final Literal literalValue;

    public SetVariable(@Nonnull Variable var, @Nullable Resource representation,
                       @Nonnull Resource resourceValue) {
        Preconditions.checkNotNull(var);
        Preconditions.checkNotNull(resourceValue);
        this.var = var;
        this.representation = representation;
        this.resourceValue = resourceValue;
        this.literalValue = null;
    }

    public SetVariable(@Nonnull Variable var, @Nullable Resource representation,
                       @Nonnull Literal literalValue) {
        Preconditions.checkNotNull(var);
        Preconditions.checkNotNull(literalValue);
        this.var = var;
        this.representation = representation;
        this.resourceValue = null;
        this.literalValue = literalValue;
    }

    @Nonnull
    public Variable getVariable() {
        return var;
    }

    @Nonnull
    @Override
    public Set<Variable> changedVariables() {
        return Collections.singleton(var);
    }

    @Override
    public void apply(DataContext context) {
        Preconditions.checkState(resourceValue != null || literalValue != null);
        if (representation != null && context.getResource(representation) == null) {
            logger.info("Will set variable with representation = {} not present in DataContext",
                    representation);
        }
        if (resourceValue != null) {
            context.setVariable(var, representation, resourceValue);
        } else {
            context.setVariable(var, representation, literalValue);
        }
    }

    @Override
    public String toString() {
        RDFNode value = literalValue;
        if (value == null) value = resourceValue;
        return String.format("SetVariable(%s %s %s)", var.toString(),
                String.valueOf(representation), value);
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof SetVariable)) return false;
        SetVariable rhs = (SetVariable) o;
        if (!var.equals(rhs.var)) return false;
        if ((representation == null) ^ (rhs.representation == null)) return false;
        if (representation != rhs.representation && !representation.equals(rhs.representation))
            return false;
        RDFNode value = resourceValue != null ? resourceValue : literalValue;
        RDFNode rhsValue = rhs.resourceValue != null ? rhs.resourceValue : rhs.literalValue;
        return (value == null) == (rhsValue == null)
                && !(value != rhsValue && !value.equals(rhsValue));
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(var).append(representation).append(resourceValue).hashCode();
    }
}

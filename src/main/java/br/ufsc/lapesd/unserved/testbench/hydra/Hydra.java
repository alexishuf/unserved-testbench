package br.ufsc.lapesd.unserved.testbench.hydra;

import br.ufsc.lapesd.unserved.testbench.BackgroundProvider;
import br.ufsc.lapesd.unserved.testbench.util.ResourcesBackground;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.riot.RDFFormat;

import java.util.Collections;

/**
 * Hydra vocabulary and uNSERVED transformation rules.
 */
@BackgroundProvider(vocab = "http://www.w3.org/ns/hydra/core#")
public class Hydra extends ResourcesBackground {
    public Hydra() {
        super(Collections.singletonList(
                ImmutablePair.of("unserved/hydra/hydra.jsonld", RDFFormat.JSONLD)),
                true);
    }

    private static Hydra instance = new Hydra();
    public static Hydra getInstance() {
        return instance;
    }

    public static final String IRI = "http://www.w3.org/ns/hydra/core";
    public static final String PREFIX = IRI + "#";
    public static final String PREFIX_SHORT = "hydra";

    public static final Property expects = getInstance().getModel().createProperty(PREFIX + "expects");
    public static final Property returns = getInstance().getModel().createProperty(PREFIX + "returns");

    //TODO more
}

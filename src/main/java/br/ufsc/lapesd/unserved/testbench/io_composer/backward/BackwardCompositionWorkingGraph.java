package br.ufsc.lapesd.unserved.testbench.io_composer.backward;

import br.ufsc.lapesd.unserved.testbench.io_composer.graph.HashBasedCompositionWorkingGraph;
import br.ufsc.lapesd.unserved.testbench.io_composer.graph.SimpleChangeSet;
import br.ufsc.lapesd.unserved.testbench.io_composer.state.Action;
import br.ufsc.lapesd.unserved.testbench.io_composer.state.ExecutionPath;
import br.ufsc.lapesd.unserved.testbench.io_composer.state.State;
import br.ufsc.lapesd.unserved.testbench.process.data.DataContextDiff;

import javax.annotation.Nonnull;
import java.util.Collections;

public class BackwardCompositionWorkingGraph
        extends HashBasedCompositionWorkingGraph<State, Double, Action> {
    @Override
    public BackwardCompositionWorkingGraph duplicate() {
        BackwardCompositionWorkingGraph other = new BackwardCompositionWorkingGraph();
        deepCopy(other);
        return other;
    }

    @Override
    protected SimpleChangeSet createChangeSet(@Nonnull DataContextDiff diff,
                                              @Nonnull ExecutionPath<Action> compositionPath) {
        //FIXME detect failures and act on them
        return new SimpleChangeSet(Collections.emptyList());
    }
}

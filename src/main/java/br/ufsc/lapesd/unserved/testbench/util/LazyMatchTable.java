package br.ufsc.lapesd.unserved.testbench.util;

import com.google.common.base.Preconditions;
import org.apache.commons.collections15.iterators.FilterIterator;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.RDFS;

import javax.annotation.Nonnull;
import java.util.*;

import static br.ufsc.lapesd.unserved.testbench.util.MatchTable.Relation.*;

public class LazyMatchTable implements MatchTable {
    private final Model model;
    private final MatchTable impl;

    public LazyMatchTable(Model model, MatchTable implementation) {
        this.model = model;
        this.impl = implementation;
    }

    @Nonnull
    @Override
    public Relation get(@Nonnull Resource left, @Nonnull Resource right) {
        Relation relation = impl.get(left, right);
        if (relation == UNKNOWN) relation = compute(left, right);
        return relation;
    }

    @Override
    public void set(@Nonnull Resource left, @Nonnull Resource right, @Nonnull Relation relation) {
        throw new UnsupportedOperationException("LazyMatchTable is read-only");
    }

    @Override
    public boolean contains(@Nonnull Resource resource) {
        return impl.contains(resource);
    }

    @Override
    public Iterator<Resource> rightIterator(@Nonnull Resource left, @Nonnull Relation relation) {
        return new FilterIterator<>(closureIterator(left, relation), r -> !r.equals(left));
    }

    @Override
    public Iterator<Resource> closureIterator(@Nonnull Resource leftResource,
                                            @Nonnull Relation relation) {
        if (!impl.contains(leftResource)) return Collections.singleton(leftResource).iterator();
        if (relation == SAME) return Collections.singleton(leftResource).iterator();
        if (relation == MISMATCH || relation == UNKNOWN) {
            throw new UnsupportedOperationException("MISMATCH and UNKNOWN closure iteration " +
                    "would be too slow on a LazyMatchTable");
        }
        Resource left = getWithModel(leftResource);
        Preconditions.checkArgument(left != null);

        Queue<Resource> queue = new LinkedList<>();
        queue.add(left);
        Set<Resource> visited = new HashSet<>();
        return new Iterator<Resource>() {
            @Override
            public boolean hasNext() {
                while (!queue.isEmpty()) {
                    if (visited.contains(queue.peek()))
                        queue.remove();
                    else
                        return true;
                }
                return false;
            }

            @Override
            public Resource next() {
                Preconditions.checkState(hasNext());
                Resource r = queue.remove();
                visited.add(r);

                if (relation == SUBCLASS) {
                    r.listProperties(RDFS.subClassOf).forEachRemaining(s -> {
                        RDFNode o = s.getObject();
                        if (o.isResource()) {
                            Resource superClass = o.asResource();
                            queue.add(superClass);
                            impl.set(r, superClass, SUBCLASS);
                            impl.set(left, superClass, SUBCLASS);
                        }
                    });
                } else if (relation == SUPERCLASS) {
                    r.getModel().listStatements(null, RDFS.subClassOf, r)
                            .forEachRemaining(s -> {
                                queue.add(s.getSubject());
                                impl.set(r, s.getSubject(), SUPERCLASS);
                                impl.set(left, s.getSubject(), SUPERCLASS);
                            });
                }

                return r;
            }
        };
    }

    protected Relation compute(@Nonnull Resource left, @Nonnull Resource right) {
        left = getWithModel(left);
        right = getWithModel(right);
        Preconditions.checkArgument(left != null);
        Preconditions.checkArgument(right != null);
        Preconditions.checkArgument(!left.equals(right));

        Queue<ImmutablePair<Resource, Relation>> queue = new LinkedList<>();
        Set<ImmutablePair<Resource, Relation>> visited = new HashSet<>();
        queue.add(ImmutablePair.of(left, SUBCLASS));
        queue.add(ImmutablePair.of(right, SUPERCLASS));
        while (!queue.isEmpty()) {
            ImmutablePair<Resource, Relation> st = queue.remove();
            if (visited.contains(st)) continue;
            visited.add(st);

            if (st.getValue() == SUPERCLASS) {
                if (!right.equals(st.getKey()))
                    impl.set(right, st.getKey(), SUBCLASS);
                if (st.getKey().equals(left) || impl.get(st.getKey(), left) == SUBCLASS) {
                    impl.set(left, right, SUPERCLASS);
                    return SUPERCLASS;
                }
            } else if (st.getValue() == SUBCLASS) {
                if (!left.equals(st.getKey()))
                    impl.set(left, st.getKey(), SUBCLASS);
                if (st.getKey().equals(right) || impl.get(st.getKey(), right) == SUBCLASS) {
                    impl.set(left, right, SUBCLASS);
                    return SUBCLASS;
                }
            }

            st.getKey().listProperties(RDFS.subClassOf).forEachRemaining(s -> {
                RDFNode o = s.getObject();
                if (o.isResource()) {
                    Resource superClass = o.asResource();
                    impl.set(st.getKey(), superClass, SUBCLASS);
                    queue.add(ImmutablePair.of(superClass, st.right));
                }
            });
        }

        impl.set(left, right, MISMATCH);
        return MISMATCH;
    }

    private Resource getWithModel(Resource resource) {
        if (resource.getModel() == model)
            return resource;
        if (!model.containsResource(resource))
            return null;
        if (resource.isAnon()) return model.createResource(resource.getId());
        else return model.createResource(resource.getURI());
    }
}

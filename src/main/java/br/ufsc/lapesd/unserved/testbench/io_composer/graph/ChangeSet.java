package br.ufsc.lapesd.unserved.testbench.io_composer.graph;

public interface ChangeSet {
    void undo();

    /**
     * An empty ChangeSet represents no change. undo() is a no-op.
     * @return true iff this is an empty ChangeSet.
     */
    boolean isEmpty();
}

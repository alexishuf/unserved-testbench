package br.ufsc.lapesd.unserved.testbench.process.data;

import br.ufsc.lapesd.unserved.testbench.input.RDFInput;
import br.ufsc.lapesd.unserved.testbench.input.RDFInputModel;
import br.ufsc.lapesd.unserved.testbench.model.Variable;
import br.ufsc.lapesd.unserved.testbench.process.data.action.*;
import br.ufsc.lapesd.unserved.testbench.util.jena.UncloseableGraph;
import com.google.common.base.Preconditions;
import org.apache.jena.graph.compose.Delta;
import org.apache.jena.rdf.model.*;
import org.apache.jena.riot.RDFFormat;
import org.apache.jena.sparql.graph.UnmodifiableGraph;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.ListIterator;

/**
 * A {@link DataContext} implementation that wraps a {@link DataContext} and records
 * all mutations performed on a diff. Accessor methods  will be able to observe the
 * changes, but the original {@link DataContext} will not be affected. A
 * {@link DataContextDiff} can be obtained from this implementation and the diff can be
 * inspected or later applied atomically to the original {@link DataContext}.
 */
public class DiffingDataContext extends BaseDataContext {
    @Nonnull private final DataContext original;
    @Nonnull private final Model delta;
    @Nonnull private final LinkedList<DataContextAction> changes = new LinkedList<>();
    @Nonnull private final Model unmodifiable;


    public DiffingDataContext(@Nonnull DataContext original) {
        Preconditions.checkNotNull(original);
        this.original = original;
        Delta graph = new Delta(new UncloseableGraph(original.getUnmodifiableModel().getGraph()));
        delta = ModelFactory.createModelForGraph(graph);
        unmodifiable = ModelFactory.createModelForGraph(
                new UnmodifiableGraph(new UncloseableGraph(graph)));
    }

    @Nonnull
    public DataContext getOriginal() {
        return original;
    }

    @Nonnull
    public DataContextDiff getDiff() {
        return new DataContextDiff(changes);
    }

    private void addChange(@Nonnull DataContextAction action) {
        Preconditions.checkNotNull(action);

        ListIterator<DataContextAction> it = changes.listIterator(changes.size());
        if (it.hasPrevious() && it.previous().equals(action)) return;
        changes.add(action);
    }

    @Override
    protected void baseAdd(@Nonnull Statement statement) {
        delta.add(statement);
    }

    @Override
    protected void baseRemove(@Nonnull Statement statement) {
        delta.remove(statement);
    }

    @Override
    protected void baseRemoveAll(@Nullable Resource subject, @Nullable Property predicate,
                                 @Nullable RDFNode object) {
        delta.removeAll(subject, predicate, object);
    }

    @Override
    protected void baseClose() {
        /* neither original nor its unmodifiableModel are closed */
        delta.close();
    }

    @Override
    protected Model getModifiableModel() {
        return delta;
    }

    @Nonnull
    @Override
    public Model getUnmodifiableModel() {
        return unmodifiable;
    }

    @Nonnull
    @Override
    public DiffingDataContext createNonIsolatedCopy() {
        return new DiffingDataContext(this);
    }

    @Nonnull
    @Override
    public Collection<RDFInput> getInputs() {
        return Collections.singletonList(
                new RDFInputModel(getUnmodifiableModel(), RDFFormat.TURTLE));
    }

    @Override
    public void addStatements(@Nonnull StmtIterator iterator) {
        super.addStatements(iterator);
        addChange(new AddStatements(iterator));
    }

    @Override
    public void addStatement(@Nonnull Statement statement) {
        super.addStatement(statement);
        addChange(new AddStatement(statement));
    }

    @Override
    public void unsetVariable(@Nonnull Variable var) {
        super.unsetVariable(var);
        addChange(new UnsetVariable(var));
    }

    @Override
    public void setVariable(@Nonnull Variable var, @Nullable Resource representation, @Nonnull Literal literal) {
        super.setVariable(var, representation, literal);
        addChange(new SetVariable(var, representation, literal));
    }

    @Override
    public void setVariable(@Nonnull Variable var, @Nullable Resource representation, @Nonnull Resource resource) {
        super.setVariable(var, representation, resource);
        addChange(new SetVariable(var, representation, resource));
    }
}

package br.ufsc.lapesd.unserved.testbench.model.http.modifiers;

import br.ufsc.lapesd.unserved.testbench.model.Variable;
import com.google.common.base.Preconditions;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public abstract class DelegatingAsHttpPropertyModifierHandler implements AsHttpPropertyModifierHandler {
    private static Logger logger = LoggerFactory.getLogger(DelegatingAsHttpPropertyModifierHandler.class);
    private Map<Class<? extends AsHttpPropertyModifierHandler>, AsHttpPropertyModifierHandler> handlers
            = new HashMap<>();
    private AsHttpPropertyModifierHandler completeHandler = null;

    protected abstract AsHttpPropertyModifierHandler createHandler(Resource modifier);

    @Override
    public void handle(Resource modifier, Variable variable) {
        if (isComplete()) return;

        AsHttpPropertyModifierHandler delegate = createHandler(modifier);
        if (delegate == null) return;
        AsHttpPropertyModifierHandler oldDelegate = handlers.getOrDefault(delegate.getClass(), null);
        if (oldDelegate == null) {
            handlers.put(delegate.getClass(), delegate);
        } else {
            delegate = oldDelegate;
        }
        delegate.handle(modifier, variable);
        if (delegate.isComplete()) {
            completeHandler = delegate;
        }
    }

    @Override
    public boolean isComplete() {
        return completeHandler != null;
    }

    @Override
    public boolean canGet() {
        return !getReadyDelegates().isEmpty();
    }

    @Override
    public RDFNode getResultNode(@Nonnull Model model) {
        Preconditions.checkNotNull(model);
        if (isComplete()) return completeHandler.getResultNode(model);
        List<AsHttpPropertyModifierHandler> cans = getReadyDelegates();
        assert canGet() == !cans.isEmpty();
        Preconditions.checkState(!cans.isEmpty());
        if (cans.size() > 1)
            logger.warn("More than one delegate handler can be get. Will chose arbitrarily");
        return cans.get(0).getResultNode(model);
    }

    @Override
    public void close() {
        handlers.values().forEach(AsHttpPropertyModifierHandler::close);
    }

    private List<AsHttpPropertyModifierHandler> getReadyDelegates() {
        return handlers.values().stream()
                .filter(AsHttpPropertyModifierHandler::canGet).collect(Collectors.toList());
    }
}

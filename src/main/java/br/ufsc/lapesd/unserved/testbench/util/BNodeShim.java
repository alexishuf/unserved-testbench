package br.ufsc.lapesd.unserved.testbench.util;

import org.apache.jena.rdf.model.Resource;

public class BNodeShim {
    public static String sparqlResource(Resource resource) {
        if (resource.isAnon()) {
            /* jena-specific */
            return "<_:" + resource.getId() + ">";
        } else {
            return "<" + resource.toString() + ">";
        }
    }
}

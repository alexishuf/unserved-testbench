package br.ufsc.lapesd.unserved.testbench.io_composer.conditions.index;

import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.atomic.TriplePattern;
import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.atomic.TriplePattern.Pos;
import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.atomic.VariableSpec;
import br.ufsc.lapesd.unserved.testbench.model.Variable;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.Weigher;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.SetMultimap;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.jena.rdf.model.Literal;

import javax.annotation.Nonnull;
import java.util.*;
import java.util.function.Supplier;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toSet;
import static org.apache.commons.lang3.tuple.ImmutablePair.of;

/**
 * A {@link TriplePattern} index where all variables are {@link Variable} instances.
 *
 * Search is done with {@link TriplePattern} instances that are slso retricted to using
 * {@link Variable} instances for variables. Variables of the query pattern are matched to
 * variables of the indexed patterns using the same rules as in {@link VariableSpecIndex}.
 *
 * @param <V> Values type.
 */
public class UnservedTriplePatternIndex<V> implements ConditionIndex<TriplePattern, V> {
    public static final double VSINDEXCACHE_IDEAL = 200.0;
    public static final int VSINDEXCACHE_MAXSIZE = 60000;
    private final int vsIndexCacheIdeal;
    private final @Nonnull VariableSpecIndex<ImmutablePair<Pos, TriplePattern>> vsIndex;
    private final @Nonnull Cache<VariableSpec, Set<ImmutablePair<Pos, TriplePattern>>> vsIndexCache;
    private final @Nonnull Map<Pos, TransitiveIndex<TriplePattern>> transIndexes;
    private final @Nonnull SetMultimap<Literal, TriplePattern> literalIndex;
//    private final @Nonnull SetMultimap<TriplePattern.Term, TriplePattern>
//            sIdx=HashMultimap.create(),  pIdx=HashMultimap.create(),  oIdx=HashMultimap.create();
    private final SetMultimap<TriplePattern, V> valueMap = HashMultimap.create();


    public UnservedTriplePatternIndex(
            @Nonnull Supplier<VariableSpecIndex<ImmutablePair<Pos, TriplePattern>>> vsIndexFactory,
            int vsIndexCacheMaxSize, double vsIndexCacheIdeal,
            @Nonnull Supplier<TransitiveIndex<TriplePattern>> typeIndexFactory,
            @Nonnull Supplier<TransitiveIndex<TriplePattern>> propIndexFactory) {
        this.vsIndex = vsIndexFactory.<ImmutablePair<Pos, TriplePattern>>get();
        transIndexes = new HashMap<>();
        transIndexes.put(Pos.Subject, typeIndexFactory.get());
        transIndexes.put(Pos.Predicate, propIndexFactory.get());
        transIndexes.put(Pos.Object, typeIndexFactory.get());
        literalIndex = HashMultimap.create();

        this.vsIndexCacheIdeal = (int)vsIndexCacheIdeal;
        vsIndexCache = CacheBuilder.newBuilder()
                .maximumWeight((long) (vsIndexCacheMaxSize/vsIndexCacheIdeal))
                .weigher((Weigher<VariableSpec, Set<ImmutablePair<Pos, TriplePattern>>>) (k, v)
                        // - Sets below IDEAL elements are lighter the closer to IDEAL
                        // - Sets with IDEAL have wight in units of IDEAL's
                        -> (int)(100*(v.size() < vsIndexCacheIdeal
                                ? (1-v.size()/vsIndexCacheIdeal) : v.size()/vsIndexCacheIdeal))
                ).build();
    }

    public UnservedTriplePatternIndex(
            @Nonnull Supplier<VariableSpecIndex<ImmutablePair<Pos, TriplePattern>>> vsIndexFactory,
            @Nonnull Supplier<TransitiveIndex<TriplePattern>> typeIndexFactory,
            @Nonnull Supplier<TransitiveIndex<TriplePattern>> propIndexFactory){
        this(vsIndexFactory, VSINDEXCACHE_MAXSIZE, VSINDEXCACHE_IDEAL,
                typeIndexFactory, propIndexFactory);
    }

    private Set<TriplePattern> getPatterns(TriplePattern pattern, Pos pos) {
        TriplePattern.Term term = pos.get(pattern);
        if (term.isUnservedVariable() && pos != Pos.Predicate) {
            VariableSpec spec = term.asUnservedVariable().asSpec();
            Set<ImmutablePair<Pos, TriplePattern>> set = vsIndexCache.getIfPresent(spec);
            if (set != null)
                return set.stream().filter(p -> p.left == pos).map(p -> p.right).collect(toSet());
            set = vsIndex.get(spec);
            if (set.size() >= vsIndexCacheIdeal) vsIndexCache.put(spec, set);
            return set.stream().filter(p -> p.left == pos).map(p -> p.right).collect(toSet());
        } else if (term.isResource()) {
            return transIndexes.get(pos).get(term.asResource());
        } else {
            assert term.isLiteral();
            return literalIndex.get(term.asLiteral());
        }
    }

    private static boolean hasOnlyUnservedVariable(@Nonnull TriplePattern p) {
        return (p.getSubject().isUnservedVariable() || !p.getSubject().isVariable()) &&
                (p.getPredicate().isUnservedVariable() || !p.getPredicate().isVariable()) &&
                (p.getObject().isUnservedVariable() || !p.getObject().isVariable());
    }

    @Override
    public void put(@Nonnull TriplePattern cond, @Nonnull V value) {
        assert hasOnlyUnservedVariable(cond);

        if (cond.getSubject().isUnservedVariable())
            vsIndex.put(cond.getSubject().asUnservedVariable().asSpec(), of(Pos.Subject, cond));
        else if (cond.getSubject().isResource())
            transIndexes.get(Pos.Subject).put(cond.getSubject().asResource(), cond);

        if (cond.getPredicate().isResource())
            transIndexes.get(Pos.Predicate).put(cond.getPredicate().asResource(), cond);

        if (cond.getObject().isUnservedVariable())
            vsIndex.put(cond.getObject().asUnservedVariable().asSpec(), of(Pos.Object, cond));
        else if (cond.getObject().isResource())
            transIndexes.get(Pos.Object).put(cond.getObject().asResource(), cond);
        else
            literalIndex.put(cond.getObject().asLiteral(), cond);

        valueMap.put(cond, value);
    }

    @Override
    public void optimize() { }

    @Override
    public boolean remove(@Nonnull TriplePattern cond) {
        assert hasOnlyUnservedVariable(cond);

        boolean changed = !valueMap.removeAll(cond).isEmpty();
        if (!changed) return false;

        if (cond.getSubject().isUnservedVariable()) {
            VariableSpec spec = cond.getSubject().asUnservedVariable().asSpec();
            vsIndexCache.invalidate(spec);
            vsIndex.remove(spec, of(Pos.Subject, cond));
        } else if (cond.getSubject().isResource()) {
            transIndexes.get(Pos.Subject).remove(cond.getSubject().asResource(), cond);
        }

        if (cond.getPredicate().isResource())
            transIndexes.get(Pos.Predicate).remove(cond.getPredicate().asResource(), cond);

        if (cond.getObject().isUnservedVariable()) {
            VariableSpec spec = cond.getObject().asUnservedVariable().asSpec();
            vsIndexCache.invalidate(spec);
            vsIndex.remove(spec, of(Pos.Object, cond));
        } else if (cond.getObject().isResource()) {
            transIndexes.get(Pos.Object).remove(cond.getObject().asResource(), cond);
        } else if (cond.getObject().isLiteral()) {
            literalIndex.remove(cond.getObject().asLiteral(), cond);
        }

        return true;
    }

    @Override
    public boolean remove(@Nonnull TriplePattern condition, @Nonnull V value) {
        assert hasOnlyUnservedVariable(condition);

        boolean changed = valueMap.remove(condition, value);
        if (!valueMap.containsKey(condition)) remove(condition);
        return changed;
    }

    public Stream<V> getStream(@Nonnull TriplePattern p) {
        assert hasOnlyUnservedVariable(p);

        ArrayList<Set<TriplePattern>> l = new ArrayList<>(3);
        l.add(getPatterns(p, Pos.Subject));
        if (l.get(0).isEmpty()) return Stream.empty();
        l.add(getPatterns(p, Pos.Predicate));
        if (l.get(1).isEmpty()) return Stream.empty();
        l.add(getPatterns(p, Pos.Object));
        if (l.get(2).isEmpty()) return Stream.empty();

        if (l.get(2).size() < l.get(1).size()) Collections.swap(l, 2, 1);
        if (l.get(1).size() < l.get(0).size()) Collections.swap(l, 1, 0);

       return l.get(0).stream().filter(x -> l.get(1).contains(x) && l.get(2).contains(x))
                .flatMap(tp -> valueMap.get(tp).stream());
    }

    public Set<V> get(@Nonnull TriplePattern p) {
        return getStream(p).collect(toSet());
    }
}

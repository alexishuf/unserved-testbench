package br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.lazy;

import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.IndexedNode;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.JumpNode;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.Node;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.ProviderSet;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.cost.ResponseTime;
import br.ufsc.lapesd.unserved.testbench.io_composer.state.Copies;
import br.ufsc.lapesd.unserved.testbench.model.Variable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.util.Comparator;
import java.util.HashSet;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.stream.Stream;

public class SingleLeastRTBackwardSetNodeProvider extends NaiveBackwardSetNodeProvider {
    private static Logger logger = LoggerFactory.getLogger(SingleLeastRTBackwardSetNodeProvider.class);

    @Override
    protected Stream<ProtoNode> getProtoNodes(@Nonnull SetNode successor) {
        if (getUseConditions()) {
            logger.info("Delegating getProtoNodes() to super class");
            return super.getProtoNodes(successor);
        }
        ProviderSet ps = getProviderSetWithJumpNodes(successor.getNodes());
        if (ps == null) return Stream.empty();

        Comparator<Node> rtComparator = Comparator.comparing(p -> {
            try {
                return p.getCostInfo().get(ResponseTime.class).getMilliseconds();
            } catch (NoSuchElementException e) {
                return Double.POSITIVE_INFINITY;
            }
        });
        Set<IndexedNode> nodes = new HashSet<>();
        Copies copies = new Copies();
        for (Variable t : ps.getTargets()) {
            Node best = ps.getProviders(t).stream().min(rtComparator).get(); //never throws
            nodes.add(IndexedNode.wrap(best));
            if (!(best instanceof JumpNode))
                copies.add(ps.getOutput(t, best), t);
        }
        return Stream.of(new ProtoNode(nodes, copies));
    }
}

package br.ufsc.lapesd.unserved.testbench.process.model;

import br.ufsc.lapesd.unserved.testbench.model.Message;

/**
 * Enhanced node interface for unserved-p:Send
 */
public interface Send extends Action {
    Message getMessage();
    int getInvocationIndex();
}

package br.ufsc.lapesd.unserved.testbench.process;

import br.ufsc.lapesd.unserved.testbench.components.ComponentRegistry;
import br.ufsc.lapesd.unserved.testbench.process.data.DataContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

public class SimpleContext implements Context {
    private static Logger logger = LoggerFactory.getLogger(SimpleContext.class);

    private final DataContext dataContext;
    private final Interpreter interpreter;
    private HashMap<String, Object> globalContext = new HashMap<>();
    private final ComponentRegistry componentRegistry;
    private DefaultActionRunnersContext runnersContext = new DefaultActionRunnersContext();
    private boolean cancelled = false;
    private boolean aborted = false;

    public SimpleContext(DataContext dataContext, ComponentRegistry componentRegistry,
                         Interpreter interpreter) {
        this.dataContext = dataContext;
        this.componentRegistry = componentRegistry;
        this.interpreter = interpreter;
    }

    @Override
    public DataContext data() {
        return dataContext;
    }

    @Override
    public ActionRunnersContext runners() {
        return runnersContext;
    }

    @Override
    public ComponentRegistry componentRegistry() {
        return componentRegistry;
    }

    public Interpreter interpreter() {
        return interpreter;
    }

    @Override
    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public boolean isAborted() {
        return aborted;
    }

    @Override
    public void cancel() {
        cancelled = true;
    }

    @Override
    public void abort() {
        aborted = true;
    }

    @Override
    public Map<String, Object> globalContext() {
        return globalContext;
    }

    @Override
    public void close() {
        dataContext.close();
        try {
            runnersContext.close();
        } catch (ActionRunnersContext.ActionContextsCloseException e) {
            logger.error("ActionContexts closed with exceptions", e);
        }
    }
}

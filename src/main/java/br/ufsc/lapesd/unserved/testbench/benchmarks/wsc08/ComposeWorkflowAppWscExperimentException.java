package br.ufsc.lapesd.unserved.testbench.benchmarks.wsc08;

public class ComposeWorkflowAppWscExperimentException extends Exception {
    public ComposeWorkflowAppWscExperimentException(String s) {
        super(s);
    }
}

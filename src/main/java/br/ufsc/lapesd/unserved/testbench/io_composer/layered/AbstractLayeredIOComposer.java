package br.ufsc.lapesd.unserved.testbench.io_composer.layered;

import br.ufsc.lapesd.unserved.testbench.composer.ComposerException;
import br.ufsc.lapesd.unserved.testbench.composer.Composition;
import br.ufsc.lapesd.unserved.testbench.composer.NoPlanComposerException;
import br.ufsc.lapesd.unserved.testbench.composer.ReplicatedComposer;
import br.ufsc.lapesd.unserved.testbench.composer.impl.NullComposition;
import br.ufsc.lapesd.unserved.testbench.composer.impl.ReplicationComposition;
import br.ufsc.lapesd.unserved.testbench.composer.impl.ReplicationCompositionWithPath;
import br.ufsc.lapesd.unserved.testbench.io_composer.AbstractIOComposer;
import br.ufsc.lapesd.unserved.testbench.io_composer.IOCompositionResult;
import br.ufsc.lapesd.unserved.testbench.io_composer.impl.*;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.LayeredGraph;
import br.ufsc.lapesd.unserved.testbench.io_composer.replicated.ReplicatedHelper;
import br.ufsc.lapesd.unserved.testbench.io_composer.state.Action;
import br.ufsc.lapesd.unserved.testbench.io_composer.state.ExecutionPath;
import br.ufsc.lapesd.unserved.testbench.io_composer.state.MessagePairAction;
import br.ufsc.lapesd.unserved.testbench.process.CompleteTask;
import br.ufsc.lapesd.unserved.testbench.process.Interpreter;
import br.ufsc.lapesd.unserved.testbench.process.actions.ActionExecutionException;
import br.ufsc.lapesd.unserved.testbench.process.data.DataContext;
import br.ufsc.lapesd.unserved.testbench.process.data.DataContextDiff;
import br.ufsc.lapesd.unserved.testbench.process.data.SimpleDataContext;
import br.ufsc.lapesd.unserved.testbench.replication.ReplicatedExecution;
import br.ufsc.lapesd.unserved.testbench.replication.ReplicatedExecutionException;
import br.ufsc.lapesd.unserved.testbench.util.MemoryUsageSampler;
import br.ufsc.lapesd.unserved.testbench.util.jena.UncloseableGraph;
import com.google.common.base.Preconditions;
import com.google.common.base.Stopwatch;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;

import static java.util.concurrent.TimeUnit.NANOSECONDS;

public abstract class AbstractLayeredIOComposer<Edge, Vertex> extends AbstractIOComposer
        implements ReplicatedComposer {
    public static final String graphAdaptation = "graphAdaptation";
    public static final String executionValidation = "executionValidation";
    protected static Logger logger = LoggerFactory.getLogger(AbstractLayeredIOComposer.class);
    private MemoryUsageSampler memoryUsageSampler = new MemoryUsageSampler();

    protected AbstractLayeredIOComposer(@Nonnull IOComposerInput ci) {
        super(ci);
    }

    protected DefaultIOComposerTimes createTimes() {
        DefaultIOComposerTimes times = new DefaultIOComposerTimes();
        memoryUsageSampler.setConsumer(bytes ->  times.addMemSample(compositionMem, bytes));
        memoryUsageSampler.start();
        return times;
    }

    public void shutdownMemoryUsageSampler() {
        memoryUsageSampler.shutdown();
    }

    @Override
    public void close() throws ComposerException {
        memoryUsageSampler.shutdown();
        super.close();
    }

    @Nonnull
    @Override
    public Composition run() throws ComposerException {
        Execution<Edge, Vertex> execution = createExecution();
        return execution.setGraphPath(execution.timedFindPath());
    }

    @Override
    public Interpreter.Task run(Interpreter interpreter) throws ReplicatedExecutionException {
        logger.warn("run(Interpreter) is deprecated. Replication not supported!");
        try (Composition comp = run();
             DataContext dataContext = comp.isNull() ? null : comp.createDataContext()) {
            if (comp.isNull()) {
                interpreter.deliverResult(createExecution().createResult());
                return new CompleteTask();
            } else {
                assert dataContext != null;
                interpreter.interpret(dataContext, comp.getWorkflowRoot()).waitForDone();
                return new CompleteTask();
            }
        } catch (InterruptedException | ActionExecutionException e) {
            throw new ReplicatedExecutionException(e);
        }
    }

    public interface CostFunction<Edge, Vertex> {
        double get(@Nonnull LayeredGraph<Action, Edge, Vertex> g, @Nonnull Vertex from,
                   @Nonnull Vertex to);
    }

    protected abstract Execution<Edge, Vertex> createExecution();

    protected static abstract class Execution<Edge, Vertex> implements ReplicatedExecution {
        protected  @Nonnull IOComposerInputDelta ci;
        protected final @Nonnull LayeredGraph<Action, Edge, Vertex> graph;
        protected final @Nonnull DefaultIOComposerTimes times;
        protected boolean closed = false;

        protected  @Nonnull LayeredProgress progress;
        protected DataContextDiff diff = null;
        protected DataContextDiff edgeDiff = null;
        protected Edge edge = null;
        protected @Nonnull Vertex start;
        protected @Nonnull ExecutionPath<Edge> graphPath;
        /** Path of actions executed that caused the changes in diff */
        protected @Nonnull ExecutionPath<Action> diffPath;
        /** Path of actions that followed diffPath in edgePath */
        protected @Nonnull ExecutionPath<Action> remainingEdgePath;
        protected LayeredGraph.Validator<Action, Vertex> validator;

        protected final @Nonnull CostFunction<Edge, Vertex> cost;
        protected final @Nonnull CostFunction<Edge, Vertex> heuristic;

        protected DataContext dataContext = null;

        public Execution(@Nonnull IOComposerInput ci,
                         @Nonnull LayeredGraph<Action, Edge, Vertex> graph,
                         @Nonnull CostFunction<Edge, Vertex> cost,
                         @Nonnull CostFunction<Edge, Vertex> heuristic,
                         @Nonnull DefaultIOComposerTimes times) {
            this.ci = new IOComposerInputDelta(ci);
            this.graph = graph;
            this.cost = cost;
            this.heuristic = heuristic;
            this.start = graph.getStart();
            this.times = times;
            graphPath = new ExecutionPath<>();
            diffPath = new ExecutionPath<>();
            remainingEdgePath = new ExecutionPath<>();
            progress = new LayeredProgress();
        }

        protected Execution(@Nonnull IOComposerInputDelta ci,
                            @Nonnull LayeredGraph<Action, Edge, Vertex> graph,
                            @Nonnull CostFunction<Edge, Vertex> cost,
                            @Nonnull CostFunction<Edge, Vertex> heuristic,
                            @Nonnull DefaultIOComposerTimes times,
                            @Nonnull ExecutionPath<Edge> graphPath,
                            @Nonnull ExecutionPath<Action> diffPath,
                            @Nonnull ExecutionPath<Action> remainingEdgePath,
                            @Nonnull Vertex start,
                            @Nonnull LayeredProgress progress,
                            LayeredGraph.Validator<Action, Vertex> validator,
                            DataContextDiff diff,
                            Edge edge) {
            this.ci = ci;
            this.graph = graph;
            this.cost = cost;
            this.heuristic = heuristic;
            this.times = times;
            this.diff = diff;
            this.edge = edge;
            this.start = start;
            this.graphPath = graphPath;
            this.diffPath = diffPath;
            this.progress = new LayeredProgress(progress);
            this.validator = validator;
            this.remainingEdgePath = remainingEdgePath;
            /* the following are transient wrt duplicate() */
            this.dataContext = null;
        }

        @Override
        @Nonnull
        public DefaultIOComposerTimes getTimes() {
            return times;
        }

        @Override
        public DataContext getDataContext() {
            Preconditions.checkState(!closed);
            if (dataContext == null) {
                Model model = ModelFactory.createModelForGraph(new UncloseableGraph(
                        ci.getSkolemizedUnion().getGraph()));
                dataContext = new SimpleDataContext(model, r -> {
                    Resource resolved = ci.getSkolemizerMap().resolve(r);
                    return resolved == null ? r : resolved;
                });
                if (edgeDiff != null)
                    edgeDiff.apply(dataContext);
            }
            return dataContext;
        }

        @Override
        public void putDiff(DataContextDiff diff) {
            Preconditions.checkState(!closed);
            Preconditions.checkState(this.diff == null);
            this.diff = diff;
        }

        /**
         * Updates fields that depend on graphPath: edge, edgePath, remainingEdgePath and diffPath.
         *
         * In the case graphPath is not already an empty path, if the first edge yields an empty
         * Action path, that edge is removed from graphPath, this continues until either all edges
         * have been removed or there are actions on diffPath and the returned Composition object.
         */
        @Nonnull
        private Composition setGraphPath(@Nonnull ExecutionPath<Edge> graphPath) {
            this.graphPath = graphPath;

            int totalMessagePairs = (int)graphPath.asList().stream().map(graph::getEdgeActions)
                    .flatMap(p -> p.asList().stream()).filter(a -> a instanceof MessagePairAction)
                    .count();
            progress = new LayeredProgress(0, totalMessagePairs,
                    0, graphPath.asList().size());

            for (; true; graphPath.removeHead(1)) {
                if (graphPath.size() == 0) { //end of path
                    edge = null;
                    validator = null;
                    remainingEdgePath = new ExecutionPath<>();
                    diffPath = new ExecutionPath<>();
                    return new NullComposition(); //break
                } else {
                    edge = graphPath.asList().get(0);
                    remainingEdgePath = graph.getEdgeActions(edge);
                    if (remainingEdgePath.size() > 0) {
                        ReplicationCompositionWithPath<Action> repComp;
                        repComp = ReplicatedHelper.createReplicationComposition(ci,
                                this, remainingEdgePath,
                                (c, ci, p) -> new ReplicationCompositionWithPath<Action>(c, ci, p) {
                                    @Nonnull
                                    @Override
                                    public DataContext createDataContext() {
                                        return getDataContext();
                                    }
                                });
                        validator = graph.createValidator(edge, getDataContext());
                        diffPath = repComp.getPath();
                        remainingEdgePath.removeHeadStrictly(diffPath);
                        return repComp; //break
                    } else {
                        graphPath.removeHead(1);
                    }
                }
            }
        }

        @Nonnull
        private Composition getNextComposition(boolean failed) {

            Composition comp;
            if (!failed) {
                /* no changes in the graph, no need for a findPath() call */
                remainingEdgePath.removeHeadStrictly(diffPath);
                if (remainingEdgePath.size() == 0) { //if edge actions are complete
                    ci.apply(edgeDiff); //apply all edge-related changes at once
                    edgeDiff = null;
                    graphPath.removeHead(1); //advance on graphPath
                    comp = setGraphPath(graphPath);
                } else {
                    /* edge not yet complete, create composition for next slice */
                    ReplicationCompositionWithPath<Action> repComp;
                    repComp = ReplicatedHelper.createReplicationComposition(ci,
                            this, remainingEdgePath);
                    diffPath = repComp.getPath();
                    remainingEdgePath.removeHeadStrictly(diffPath);
                    comp = repComp;
                }
            } else {
                /* a failure induced changes on the graph, re-plan */
                comp = null;
                boolean lastChance = false;
                while (comp == null && !lastChance) {
                    try {
                        comp = setGraphPath(timedFindPath());
                    } catch (NoPlanComposerException e) {
                        lastChance = !adapt("Previously adapted graph did not contained " +
                                        "a plan");
                    }
                }
                if (comp == null)
                    throw new ComposerException("Exhausted all graph adaptation possibilities.");
            }

            return comp;
        }

        private boolean adapt(String logReason) {
            boolean withAlternatives;
            Stopwatch watch = Stopwatch.createStarted();
            try {
                try {
                    beginGraphAdaptation();
                    if (!validator.adaptGraph())
                        throw new ComposerException("No more adaptations can be done.");
                    withAlternatives = validator.adaptedWithAlternatives();
                } finally {
                    endGraphAdaptation();
                }
                Vertex newStart = validator.getNewStart();
                DataContextDiff adaptationDiff = validator.getAdaptationDiff();

                logger.info("{}, found{} alternatives. {}Applying {}.", logReason,
                        withAlternatives ? "" : " no", !newStart.equals(start)
                                ? String.format("Moving from %s to %s. ", start, newStart) : "",
                        adaptationDiff);

                ci.apply(adaptationDiff);
                start = newStart;
                return withAlternatives;
            } finally {
                times.addTime(graphAdaptation, watch.elapsed(NANOSECONDS), NANOSECONDS);
            }
        }

        protected void beginGraphAdaptation() {}
        protected void endGraphAdaptation() {}

        private boolean validateDiff() {
            boolean failedFlag = false;
            if (diff == null) return false;
            if (!timeValidate(diff, diffPath)) {
                adapt("Validator short-circuited");
                failedFlag = true;
            } else {
                edgeDiff = edgeDiff == null ? diff : DataContextDiff.concat(edgeDiff, diff);
            }
            return failedFlag;
        }

        private boolean timeValidate(DataContextDiff diff, ExecutionPath<Action> diffPath) {
            Stopwatch watch = Stopwatch.createStarted();
            try {
                return validator.validateActions(diff, diffPath);
            } finally {
                times.addTime(executionValidation, watch.elapsed(NANOSECONDS), NANOSECONDS);
            }
        }

        @Override
        public void resumeInterpretation() {
            throw new UnsupportedOperationException();
        }

        @Override
        public void resumeInterpretation(Interpreter interpreter)
                throws ReplicatedExecutionException {
            Preconditions.checkState(!closed);
            Composition comp = null;

            try {
//                System.err.printf("resumeInterpretation()\n");
                boolean failedFlag = validateDiff();

                /* Get/advance a plan and get a composition with the next slice */
                try {
                    progress.completeServices((int)diffPath.asList().stream()
                            .filter(a -> a instanceof MessagePairAction).count());
                    progress.completeEdges(remainingEdgePath.size() > 0 ? 0 : 1);
                    comp = getNextComposition(failedFlag);
                } catch (ComposerException e) {
                    throw new ReplicatedExecutionException("No suitable plan for " +
                            "continuing the composition.", e);
                }
//                if (dataContext != null)
//                    System.err.printf("execution.dataContext known types:\n%s\n\n", dataContext.getUnmodifiableModel().listSubjectsWithProperty(RDF.type, Unserved.ValueBoundVariable).toList().stream().map(r -> r.as(Variable.class)).map(Variable::getType).map(Resource::toString).reduce((a, b) -> a + "\n" + b).orElse(""));
                assert validator == null || !validator.isFailed();

                /* deliver result or interpret composition */
                try (DataContext dataContext = comp.isNull() ? null : comp.createDataContext()) {
                    if (comp.isNull()) {
                        interpreter.deliverResult(createResult());
                    } else {
                        assert validator != null;
                        assert dataContext != null;
                        assert comp instanceof ReplicationComposition;
//                        System.err.printf("comp.createDataContext() known types:\n%s\n\n", dataContext.getUnmodifiableModel().listSubjectsWithProperty(RDF.type, Unserved.ValueBoundVariable).toList().stream().map(r -> r.as(Variable.class)).map(Variable::getType).map(Resource::toString).reduce((a, b) -> a + "\n" + b).orElse(""));
                        @SuppressWarnings("unchecked")
                        ReplicationComposition repComp = (ReplicationComposition) comp;
                        diff = null;
                        if (edgeDiff != null)
                            edgeDiff.apply(dataContext);
                        repComp.getContainer().setProgress(progress);
                        interpreter.interpret(dataContext, comp.getWorkflowRoot()).waitForDone();
//                        if (repComp.getContainer().wasForked()) {
//                            repComp.getContainer().join();
//                        }
                    }
                } catch (ActionExecutionException | InterruptedException e) {
                    throw new ReplicatedExecutionException(e);
                }
            } finally {
                if (comp != null)
                    comp.close();
            }
        }

        @Nonnull
        protected abstract ExecutionPath<Edge> findPath() throws NoPlanComposerException;
        @Nonnull
        private ExecutionPath<Edge> timedFindPath() throws NoPlanComposerException {
            Stopwatch watch = Stopwatch.createStarted();
            try {
                return findPath();
            } finally {
                long ns = watch.elapsed(NANOSECONDS);
                times.takeMemSample(planningMem);
                times.addTime(planningTime, ns, NANOSECONDS);
                logger.info("Path planning from {} took {} msecs", start,
                        ns/1000000.0);
            }
        }


        private IOCompositionResult createResult() {
            return new DefaultIOCompositionResult(new IOComposerInputReference(ci), times);
        }

        @Override
        public void close() {
            if (!closed) {
                closed = true;
                ci.close();
            }
        }
    }
}

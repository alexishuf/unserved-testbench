package br.ufsc.lapesd.unserved.testbench.model;

import org.apache.jena.rdf.model.Resource;

/**
 * Enhanced node for unserved:When
 */
public interface When extends Resource {

}

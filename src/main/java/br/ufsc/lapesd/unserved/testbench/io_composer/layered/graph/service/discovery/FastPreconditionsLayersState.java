package br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.discovery;

import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.*;
import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.atomic.TriplePattern;
import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.atomic.VariableSpec;
import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.index.TransitiveIndex;
import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.index.UnservedTriplePatternIndex;
import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.index.VariableSpecIndex;
import br.ufsc.lapesd.unserved.testbench.io_composer.impl.IOComposerInput;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.*;
import br.ufsc.lapesd.unserved.testbench.iri.UnservedX;
import br.ufsc.lapesd.unserved.testbench.model.Message;
import br.ufsc.lapesd.unserved.testbench.model.Part;
import br.ufsc.lapesd.unserved.testbench.model.Variable;
import br.ufsc.lapesd.unserved.testbench.util.NaiveTransitiveClosureGetter;
import br.ufsc.lapesd.unserved.testbench.util.TransitiveClosureGetter;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.RDFS;

import javax.annotation.Nonnull;
import java.util.*;

import static org.apache.commons.lang3.tuple.ImmutablePair.of;

public class FastPreconditionsLayersState extends FastIOLayersState {
    private MyConditionEvaluator myConditionEvaluator = new MyConditionEvaluator();
    private @Nonnull UnservedTriplePatternIndex<ImmutablePair<Condition, Node>> postconditions;

    public FastPreconditionsLayersState(InputServiceIndex inputIndex) {
        this(inputIndex, false);
    }

    public FastPreconditionsLayersState(InputServiceIndex inputIndex, boolean listingSubclasses) {
        super(inputIndex, listingSubclasses);
        if (listingSubclasses) {
            NaiveTransitiveClosureGetter tcg =
                    new NaiveTransitiveClosureGetter(RDFS.subClassOf, true);
            NaiveTransitiveClosureGetter tcgProp =
                    new NaiveTransitiveClosureGetter(RDFS.subPropertyOf, true);
            postconditions = new UnservedTriplePatternIndex<>(() -> new VariableSpecIndex<>(tcg),
                    () -> new TransitiveIndex<>(tcg),
                    () -> new TransitiveIndex<>(tcgProp));

        } else {
            TransitiveClosureGetter tcgProp = inputIndex.getSuperClassGetter()
                    .createFor(RDFS.subPropertyOf, false);
            postconditions = new UnservedTriplePatternIndex<>(
                    () -> VariableSpecIndex.withClosureOnPut(inputIndex.getSuperClassGetter()),
                    () -> TransitiveIndex.withClosureOnPut(inputIndex.getSuperClassGetter()),
                    () -> TransitiveIndex.withClosureOnPut(tcgProp));
        }
    }

    @Override
    public boolean supportsSPINConditions() {
        return true;
    }

    @Nonnull
    @Override
    public ConditionEvaluator getEvaluator() {
        return myConditionEvaluator;
    }

    private class MyConditionEvaluator extends BaseConditionEvaluator {
        public MyConditionEvaluator() {
            this.add(TriplePattern.class, this::evaluateTriplePattern);
            this.add(VariableSpec.class, this::evaluateVariableSpec);
        }

        public boolean evaluateTriplePattern(@Nonnull TriplePattern tp) {
            return postconditions.getStream(tp).findAny().isPresent();
        }
        public boolean evaluateVariableSpec(@Nonnull VariableSpec spec) {
            return outputs.getFirst(spec) != null;
        }
    }

    @Override
    public void advance(@Nonnull Collection<Node> nodes) {
        super.advance(nodes);
        for (Node n : nodes) {
            getOutputTriplePatterns(n).forEach(tp -> postconditions.put(tp, of(tp, n)));
        }
    }

    @Nonnull
    @Override
    public ProviderSetsSupplier createProviderSetsSupplier() {
        return new MyPreconditionProviderSetsSupplier();
    }

    @Nonnull
    @Override
    public NodesSupplier<? extends LayersState> createNodesSupplier() {
        return new MyPreconditionNodesSupplier();
    }

    static List<TriplePattern> getTriplePatterns(@Nonnull And and) {
        List<TriplePattern> list = new ArrayList<>();
        ConditionTraverser.visitPreOrder(and, new ConditionVisitor() {
            @Override
            public boolean visitTriplePattern(@Nonnull TriplePattern cond) {
                list.add(cond);
                return true;
            }
        });
        return list;
    }

    static List<TriplePattern> getOutputTriplePatterns(@Nonnull Node node) {
        List<TriplePattern> list = getTriplePatterns(node.getPostConditions());
        /* part relations also imply sp:TriplePatterns when there is a ux-rdf:property */
        addTriplePatternsFromParts(list, node.getOutputs());
        return list;
    }

    static private void addTriplePatternsFromParts(List<TriplePattern> list, Set<Variable> outputs) {
        /* Important: for StartNode and EndNode, getOutputs does not use getPartsRecursive(). */
        Set<Variable> visited = new HashSet<>();
        Stack<Variable> stack = new Stack<>();
        outputs.forEach(stack::push);
        while (!stack.isEmpty()) {
            Variable var = stack.pop();
            if (visited.contains(var)) continue;
            visited.add(var);
            for (Part part : var.getParts()) {
                Variable partVariable = part.getVariable();
                stack.push(partVariable);

                Resource pm = part.getPartModifier();
                if (pm == null) continue;
                Resource property = pm.getPropertyResourceValue(UnservedX.RDF.property);
                Resource propertyOf = pm.getPropertyResourceValue(UnservedX.RDF.propertyOf);
                if (property != null && propertyOf.equals(var))
                    list.add(new TriplePattern(var, property, partVariable));
            }
        }
    }

    private class MyPreconditionProviderSetsSupplier extends MyProviderSetsSupplier {
        @Override
        public ProviderSet get(@Nonnull Node node) {
            assert isActive();
            ConditionProviderHashSet ps = new ConditionProviderHashSet();
            Set<Node> validSet = new HashSet<>();
            for (Variable in : node.getInputs()) {
                ps.addTarget(in);
                VariableSpec spec = in.asSpec();
                outputs.getStream(spec).filter(p -> !psBlacklist.contains(p.right))
                        .forEach(p -> {
                            ps.add(in, p.right, p.left);
                            validSet.add(p.right);
                        });
            }

            for (TriplePattern tp : getTriplePatterns(node.getPreConditions())) {
                ps.addCondition(tp);
                postconditions.getStream(tp).filter(p -> validSet.contains(p.right))
                        .forEach(p -> ps.add(tp, p.right, p.left));
            }
            return ps;
        }
    }

    private class MyPreconditionNodesSupplier implements NodesSupplier<FastIOLayersState> {
        boolean active = false;

        @Nonnull
        @Override
        public Set<Node> getNodes(@Nonnull FastIOLayersState state, @Nonnull Set<Message> blacklist) {
            assert active;
            assert state == FastPreconditionsLayersState.this;
            ConditionEvaluator evaluator = state.getEvaluator();

            HashSet<Node> set = new HashSet<>(128);
            Set<Node> visited = new HashSet<>(512);
            for (VariableSpec spec : state.getNewlyKnown()) {
                inputIndex.getConsumerInputs().getNonDistinctStream(spec).forEach(p -> {
                    if (!visited.contains(p.right)) {
                        visited.add(p.right);
                        boolean blacklisted = blacklist.contains(p.right.getConsequent());
                        if (blacklisted) return;

                        boolean unsatisfied = false;
                        for (Variable in : p.right.getInputs()) {
                            if ((unsatisfied = !evaluator.evaluate(in.asSpec()))) break;
                        }
                        unsatisfied = unsatisfied
                                    || !evaluator.evaluate(p.right.getPreConditions());
                        if (!unsatisfied) set.add(p.right);
                    }
                });
            }
            state.advance(set);
            return set;
        }

        @Override
        public boolean isActive() {
            return false;
        }

        @Override
        public void begin(IOComposerInput composerInput, @Nonnull StartNode startNode, @Nonnull EndNode endNode) {
            inputIndex.addModel(composerInput.getSkolemizedUnion());
            active = true;
        }

        @Override
        public void end() {
            inputIndex.clear();
            active = false;
        }
    }
}

package br.ufsc.lapesd.unserved.testbench.io_composer.state;

import br.ufsc.lapesd.unserved.testbench.composer.Composition;
import br.ufsc.lapesd.unserved.testbench.composer.impl.DefaultComposition;
import br.ufsc.lapesd.unserved.testbench.iri.UnservedP;
import br.ufsc.lapesd.unserved.testbench.util.RDFListWriter;
import br.ufsc.lapesd.unserved.testbench.util.SkolemizerMap;
import com.google.common.base.Preconditions;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.RDF;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static org.apache.jena.rdf.model.ResourceFactory.createTypedLiteral;

/**
 * Helper that writes a unserved-p:Sequence from actions
 */
public class UnservedPSequenceWriter {
    private boolean closed = false;
    private Model model = ModelFactory.createDefaultModel();
    private List<Resource> rootMembers = new ArrayList<>();

    private Resource createSequence(List<Resource> list) {
        return model.createResource(UnservedP.Sequence)
                .addProperty(RDF.type, UnservedP.Action)
                .addProperty(UnservedP.members, RDFListWriter.write(model, list));
    }

    private List<Resource> toActions(Collection<MessagePairAction> pairs) {
        List<Resource> list = pairs.stream().map(pair -> model.createResource(UnservedP.Send)
                .addProperty(RDF.type, UnservedP.Action)
                .addProperty(UnservedP.message, pair.getAntecedent())
                .addProperty(UnservedP.invocationIndex, createTypedLiteral(pair.getIndex()))
        ).collect(Collectors.toList());
        for (MessagePairAction pair : pairs) {
            list.add(model.createResource(UnservedP.Receive)
                    .addProperty(RDF.type, UnservedP.Action)
                    .addProperty(UnservedP.message, pair.getConsequent())
                    .addProperty(UnservedP.invocationIndex, createTypedLiteral(pair.getIndex())));
            list.addAll(toActions(pair.getAssignments()));
        }
        return list;
    }

    private List<Resource> toActions(Copies copies) {
        return copies.asList().stream()
                .map(copy -> model.createResource(UnservedP.Copy)
                        .addProperty(RDF.type, UnservedP.Action)
                        .addProperty(UnservedP.copyFrom, copy.from)
                        .addProperty(UnservedP.copyFromIndex, createTypedLiteral(copy.from.index))
                        .addProperty(UnservedP.copyTo, copy.to)
                        .addProperty(UnservedP.copyToIndex, createTypedLiteral(copy.to.index)))
                .collect(Collectors.toList());
    }

    public void append(StateActions actions) {
        Preconditions.checkState(!closed);
        rootMembers.addAll(toActions(actions.getCopies()));
        rootMembers.addAll(toActions(actions.getMessagePairs()));
    }

    public void append(Action a) {
        if (a instanceof Copies) {
            rootMembers.addAll(toActions((Copies)a));
        } else if (a instanceof MessagePairAction) {
            rootMembers.addAll(toActions(Collections.singletonList((MessagePairAction)a)));
        } else {
            throw new IllegalArgumentException("Don't know how to handle " + a.getClass().getName());
        }
    }


    public Composition toIOComposition(Model data, SkolemizerMap skolemizerMap) {
        Preconditions.checkState(!closed);
        DefaultComposition composition;
        composition = new DefaultComposition(data, skolemizerMap,
                model, createSequence(rootMembers));
        closed = true;
        return composition;
    }
}

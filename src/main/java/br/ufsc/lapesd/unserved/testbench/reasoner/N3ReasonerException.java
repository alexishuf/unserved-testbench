package br.ufsc.lapesd.unserved.testbench.reasoner;

/**
 * Base exception class for problems related to N3Reasoner implementations.
 */
public class N3ReasonerException extends Exception {
    public N3ReasonerException(String message, Exception exception) {super(message, exception);}
    public N3ReasonerException(String message) {super(message);}
    public N3ReasonerException(Exception exception) {super(exception);}
}

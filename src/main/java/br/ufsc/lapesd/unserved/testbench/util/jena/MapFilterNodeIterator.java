package br.ufsc.lapesd.unserved.testbench.util.jena;

import org.apache.jena.rdf.model.NodeIterator;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.util.iterator.ExtendedIterator;
import org.apache.jena.util.iterator.MapFilter;
import org.apache.jena.util.iterator.MapFilterIterator;

import java.util.NoSuchElementException;

public class MapFilterNodeIterator<T> extends MapFilterIterator<T, RDFNode> implements NodeIterator {
    /**
     * Creates a sub-Iterator.
     *
     * @param fl An object is included if it is accepted by this Filter.
     * @param e  The parent Iterator.
     */
    public MapFilterNodeIterator(MapFilter<T, RDFNode> fl, ExtendedIterator<T> e) {
        super(fl, e);
    }

    @Override
    public RDFNode nextNode() throws NoSuchElementException {
        return next();
    }
}

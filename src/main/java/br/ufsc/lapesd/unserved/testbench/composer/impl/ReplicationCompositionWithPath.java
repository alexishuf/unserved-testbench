package br.ufsc.lapesd.unserved.testbench.composer.impl;

import br.ufsc.lapesd.unserved.testbench.io_composer.impl.IOComposerInput;
import br.ufsc.lapesd.unserved.testbench.io_composer.state.ExecutionPath;
import br.ufsc.lapesd.unserved.testbench.replication.action.ReplicationContainer;

import javax.annotation.Nonnull;

public class ReplicationCompositionWithPath<ActionType> extends ReplicationComposition {
    @Nonnull private final ExecutionPath<ActionType> path;

    public ReplicationCompositionWithPath(@Nonnull ReplicationContainer container,
                                          @Nonnull IOComposerInput composerInput,
                                          @Nonnull ExecutionPath<ActionType> path) {
        super(container, composerInput);
        this.path = path;
    }

    @Nonnull
    public ExecutionPath<ActionType> getPath() {
        return path;
    }
}

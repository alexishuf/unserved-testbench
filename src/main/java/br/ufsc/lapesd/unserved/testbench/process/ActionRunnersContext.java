package br.ufsc.lapesd.unserved.testbench.process;

import java.util.Collection;

public interface ActionRunnersContext extends AutoCloseable {
    void storeActionContext(Object key, ActionContext context);
    <T extends ActionContext> Collection<T> getActionContexts(Object key, Class<T> type);
    Collection<? extends ActionContext> getActionContexts(Object key);
    void removeActionContext(ActionContext context);

    @Override
    void close() throws ActionContextsCloseException;

    class ActionContextsCloseException extends Exception {
        private final Collection<Exception> exceptions;

        public ActionContextsCloseException(Collection<Exception> exceptions) {
            super(String.format("%d exceptions thrown while closing ActionContext instances.",
                    exceptions.size()));
            this.exceptions = exceptions;
        }

        public Collection<Exception> getExceptions() {
            return exceptions;
        }
    }
}

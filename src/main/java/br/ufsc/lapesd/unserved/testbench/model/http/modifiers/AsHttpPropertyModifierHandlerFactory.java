package br.ufsc.lapesd.unserved.testbench.model.http.modifiers;

public interface AsHttpPropertyModifierHandlerFactory {
    AsHttpPropertyModifierHandler newInstance();
}

package br.ufsc.lapesd.unserved.testbench.httpcomponents.extractors;

import br.ufsc.lapesd.unserved.testbench.model.Part;
import br.ufsc.lapesd.unserved.testbench.process.Context;
import org.apache.http.HttpResponse;

public interface HttpResponseExtractor {
    HttpResponseExtraction extract(HttpResponse response, Part part, Context context)
            throws ExtractionException;
}

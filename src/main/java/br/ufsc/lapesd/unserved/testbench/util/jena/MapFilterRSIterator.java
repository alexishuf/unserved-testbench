package br.ufsc.lapesd.unserved.testbench.util.jena;

import org.apache.jena.rdf.model.RSIterator;
import org.apache.jena.rdf.model.ReifiedStatement;
import org.apache.jena.util.iterator.ExtendedIterator;
import org.apache.jena.util.iterator.MapFilter;
import org.apache.jena.util.iterator.MapFilterIterator;

public class MapFilterRSIterator<T> extends MapFilterIterator<T, ReifiedStatement>
        implements RSIterator {
    /**
     * Creates a sub-Iterator.
     *
     * @param fl An object is included if it is accepted by this Filter.
     * @param e  The parent Iterator.
     */
    public MapFilterRSIterator(MapFilter<T, ReifiedStatement> fl, ExtendedIterator<T> e) {
        super(fl, e);
    }

    @Override
    public ReifiedStatement nextRS() {
        return next();
    }
}

package br.ufsc.lapesd.unserved.testbench.replication.action;

import br.ufsc.lapesd.unserved.testbench.components.SimpleActionRunnerFactory;
import br.ufsc.lapesd.unserved.testbench.iri.Unserved;
import br.ufsc.lapesd.unserved.testbench.iri.UnservedP;
import br.ufsc.lapesd.unserved.testbench.model.*;
import br.ufsc.lapesd.unserved.testbench.process.Context;
import br.ufsc.lapesd.unserved.testbench.process.Interpreter;
import br.ufsc.lapesd.unserved.testbench.process.OverriddenContext;
import br.ufsc.lapesd.unserved.testbench.process.actions.ActionExecutionException;
import br.ufsc.lapesd.unserved.testbench.process.actions.ActionRunner;
import br.ufsc.lapesd.unserved.testbench.process.data.DiffingDataContext;
import br.ufsc.lapesd.unserved.testbench.process.model.Receive;
import br.ufsc.lapesd.unserved.testbench.replication.ReplicatedExecutionException;
import com.google.common.base.Preconditions;
import org.apache.jena.rdf.model.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;


public class ReplicationReceiveRunner implements ActionRunner {
    private static Logger logger = LoggerFactory.getLogger(ReplicationReceiveRunner.class);

    public static class Factory extends SimpleActionRunnerFactory {

        public Factory() {
            super(ReplicationReceiveRunner.class, Factory.class, UnservedP.Receive);
        }
    }

    @Override
    public void run(Resource action, Context context) throws ActionExecutionException {
        Preconditions.checkNotNull(action);
        Preconditions.checkNotNull(context);
        Preconditions.checkArgument(action.canAs(Receive.class));
        Receive rcv = action.as(Receive.class);

        Collection<ReplicationActionRunnerContext> contexts = context.runners()
                .getActionContexts(rcv, ReplicationActionRunnerContext.class);
        if (contexts.size() != 1) {
            throw new ActionExecutionException("Expected a single ReplicationActionRunnerContext, " +
                    "got " + contexts.size());
        }
        ReplicationActionRunnerContext runnerContext = contexts.iterator().next();

        DiffingDataContext dataContext = runnerContext.getDataContext();
        ReplicationContainer container = runnerContext.getContainer();
        Context outerContext = runnerContext.getOuterContext();

        long count = getReceiveCount(rcv.getMessage());
        if (count > 1) {
            receiveMultiple(count, context, rcv, dataContext, container, outerContext);
        } else {
            try (DiffingDataContext myDataContext = dataContext.createNonIsolatedCopy();
                 Context myContext = new OverriddenContext(context).setDataContext(myDataContext)
                         .setComponentRegistry(outerContext.componentRegistry())) {
                ActionRunner runner = myContext.componentRegistry().createActionRunner(rcv);
                if (runner == null)
                    throw new ActionExecutionException("No runner for receive " + rcv.toString());
                runner.run(rcv, myContext);
                container.getExecution().putDiff(myDataContext.getDiff());
                try {
                    container.getExecution().resumeInterpretation(context.interpreter());
                } catch (ReplicatedExecutionException e) {
                    throw new ActionExecutionException(e);
                }
            }
        }

    }

    private void receiveMultiple(long count, Context context, Receive rcv,
                                 DiffingDataContext dataContext, ReplicationContainer container,
                                 Context outerContext) throws ActionExecutionException {
        ExecutorService executorService = Executors.newWorkStealingPool();
        Queue<Future<?>> queue = new LinkedList<>();
        for (int i = 0; i < count && !context.isCancelled(); i++) {
            queue.removeIf(Future::isDone);
            ReplicationContainer duplicateContainer = container.fork();
            try (DiffingDataContext myDataContext = dataContext.createNonIsolatedCopy();
                 Context myContext = new OverriddenContext(context, false)
                         .setDataContext(myDataContext)
                         .setComponentRegistry(outerContext.componentRegistry())) {
                ActionRunner runner = myContext.componentRegistry().createActionRunner(rcv);
                if (runner == null)
                    throw new ActionExecutionException("No runner for receive " + rcv.toString());
                runner.run(rcv, myContext);
                duplicateContainer.getExecution().putDiff(myDataContext.getDiff());

                final Interpreter interpreter = context.interpreter();
                queue.add(executorService.submit(() -> {
                    try {
                        duplicateContainer.getExecution().resumeInterpretation(interpreter);
                    } catch (ReplicatedExecutionException e) {
                        logger.error("continuation failed", e);
                    } finally {
                        duplicateContainer.close();
                    }
                }));
            }
        }

        if (context.isCancelled()) {
            boolean aborting = context.isAborted();
            queue.forEach(f -> f.cancel(aborting));
            if (aborting)
                executorService.shutdownNow();
            else
                executorService.shutdown();
            try {
                executorService.awaitTermination(aborting ? 1000 : Long.MAX_VALUE,
                        TimeUnit.MILLISECONDS);
            } catch (InterruptedException e) {
                throw new ActionExecutionException(e);
            }
        }
    }

    private long getReceiveCount(Message message) throws ActionExecutionException {
        When when = message.getWhen();
        if (when.canAs(Reaction.class)) {
            Cardinality cardinality = when.as(Reaction.class).getReactionCardinality();
            if (cardinality.equals(Unserved.one))
                return 1;
            if (cardinality.equals(Unserved.many))
                return Long.MAX_VALUE;
            if (cardinality.canAs(ValuedCardinality.class))
                return cardinality.as(ValuedCardinality.class).getCardinalityValue();
        }
        throw new ActionExecutionException("Could not determine receipt cardinality of message " + message);
    }
}

package br.ufsc.lapesd.unserved.testbench.pragproof.context;

import br.ufsc.lapesd.unserved.testbench.input.RDFInput;
import org.apache.jena.rdf.model.Resource;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.List;

public interface PragProofRules extends AutoCloseable {
    /**
     * Get a list of N3 RDFInputs which contain the rules
     * @return list of rule N3 files.
     */
    @Nonnull
    List<RDFInput> getInputs();

    void removeInteractionRule(Resource interaction) throws IOException;

    /**
     * Creates a new {@link PragProofRules} that contains the same query as this but
     * changes on the copy are not visible on this object. An implementation may allow
     * changes in this object to be reflected on the copy.
     *
     * @return A non-isolated copy
     */
    PragProofRules createNonIsolatedCopy();
}

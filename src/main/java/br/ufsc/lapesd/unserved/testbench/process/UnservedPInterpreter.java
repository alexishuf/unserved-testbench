package br.ufsc.lapesd.unserved.testbench.process;

import br.ufsc.lapesd.unserved.testbench.components.ComponentRegistry;
import br.ufsc.lapesd.unserved.testbench.process.actions.ActionExecutionException;
import br.ufsc.lapesd.unserved.testbench.process.actions.ActionRunner;
import br.ufsc.lapesd.unserved.testbench.process.data.DataContext;
import br.ufsc.lapesd.unserved.testbench.process.data.SimpleDataContext;
import br.ufsc.lapesd.unserved.testbench.process.data.UncloseableDataContext;
import com.google.common.base.Preconditions;
import com.google.common.base.Stopwatch;
import org.apache.jena.rdf.model.Resource;

import javax.annotation.Nonnull;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.function.Consumer;

public class UnservedPInterpreter extends AbstractInterpreter {
    @Nonnull
    private final ComponentRegistry componentRegistry;

    public UnservedPInterpreter(@Nonnull ComponentRegistry componentRegistry) {
        this.componentRegistry = componentRegistry;
    }

    @Override
    public Task interpret(@Nonnull Resource resource) {
        try (SimpleDataContext dataContext = new SimpleDataContext()) {
            return interpret(dataContext, resource);
        }
    }

    @Override
    public Task interpret(@Nonnull DataContext dataContext, @Nonnull Resource resource) {
        NestedInterpreter nested = new NestedInterpreter();
        SimpleContext context = new SimpleContext(new UncloseableDataContext(dataContext),
                componentRegistry, nested);
        nested.setContext(context);
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        Task task = new Task(context, executorService);
        nested.setTask(task);
        executorService.execute(() -> interpretImpl(task, executorService, context, resource));
        return task;
    }

    private void interpretImpl(@Nonnull Task task, @Nonnull ExecutorService executorService,
                               @Nonnull Context context, @Nonnull Resource resource) {
        ActionRunner runner = componentRegistry.createActionRunner(resource);
        if (runner == null) {
            task.complete(new ActionExecutionException("No runner"));
        } else {
            ActionExecutionException exception = null;
            try {
                runner.run(resource, context);
            } catch (ActionExecutionException e) {
                exception = e;
            } finally {
                context.close();
            }
            task.complete(exception);
            executorService.shutdown();
        }
    }

    private class NestedInterpreter implements Interpreter {
        private UnservedPInterpreter.Task task;
        private Context context;

        public void setTask(UnservedPInterpreter.Task task) {
            this.task = task;
        }
        public void setContext(Context context) {
            this.context = context;
        }

        @Override
        public <T> void setResultConsumer(Class<T> aClass, Consumer<T> consumer) {
            UnservedPInterpreter.this.setResultConsumer(aClass, consumer);
        }

        @Override
        public Task interpret(@Nonnull Resource resource) {
            try (SimpleDataContext dataContext = new SimpleDataContext()) {
                return interpret(dataContext, resource);
            }
        }

        @Override
        public Task interpret(@Nonnull DataContext dataContext, @Nonnull Resource resource) {
            try (OverriddenContext context = new OverriddenContext(this.context,
                    false)
                    .setDataContext(dataContext)
                    .setActionRunnersContext(new DefaultActionRunnersContext())) {
                ActionRunner runner = componentRegistry.createActionRunner(resource);
                if (runner == null)
                    return new CompleteTask(new ActionExecutionException("No runner"));
                try {
                    runner.run(resource, context);
                } catch (ActionExecutionException e) {
                    return new CompleteTask(e);
                }
                return new CompleteTask();
            }
        }

        @Override
        public boolean deliverResult(Object result) {
            return UnservedPInterpreter.this.deliverResult(result);
        }
    }

    private static class Task implements Interpreter.Task {
        private ActionExecutionException exception = null;
        boolean complete = false;
        private final Context context;
        private final ExecutorService executorService;

        private Task(Context context, ExecutorService executorService) {
            this.context = context;
            this.executorService = executorService;
        }

        public synchronized void complete(ActionExecutionException exception) {
            Preconditions.checkState(!complete);
            this.exception = exception;
            this.complete = true;
            notifyAll();
        }

        @Override
        public synchronized Interpreter.Task cancel() {
            context.cancel();
            return this;
        }

        @Override
        public synchronized Interpreter.Task abort() {
            context.abort();
            executorService.shutdownNow();
            return this;
        }

        @Override
        public synchronized boolean isDone() throws ActionExecutionException {
            if (complete && exception != null) throw exception;
            return complete;
        }

        @Override
        public synchronized void waitForDone() throws InterruptedException, ActionExecutionException {
            while (!isDone()) wait(Integer.MAX_VALUE);
            if (!executorService.isTerminated())
                executorService.awaitTermination(Integer.MAX_VALUE, TimeUnit.MILLISECONDS);
        }

        @Override
        public synchronized void waitForDone(long count, TimeUnit timeUnit) throws InterruptedException, ActionExecutionException, TimeoutException {
            if (TimeUnit.SECONDS.convert(1, timeUnit) >= 1) {
                count = TimeUnit.MILLISECONDS.convert(count, timeUnit);
                timeUnit = TimeUnit.MILLISECONDS;
            }

            Stopwatch watch = Stopwatch.createStarted();
            while (!isDone()) {
                long credit = count - watch.elapsed(timeUnit);
                if (credit > 0) {
                    wait(TimeUnit.MILLISECONDS.convert(credit, timeUnit));
                } else {
                    throw new TimeoutException();
                }
            }

            if (!executorService.isTerminated()) {
                long credit = count - watch.elapsed(timeUnit);
                executorService.awaitTermination(credit, timeUnit);
            }
        }
    }
}

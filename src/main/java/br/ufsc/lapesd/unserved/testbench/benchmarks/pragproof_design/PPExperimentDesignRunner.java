package br.ufsc.lapesd.unserved.testbench.benchmarks.pragproof_design;

import br.ufsc.lapesd.unserved.testbench.benchmarks.compose.TimesExperimentResult;
import br.ufsc.lapesd.unserved.testbench.benchmarks.experiment.AbstractExperimentDesignRunner;
import br.ufsc.lapesd.unserved.testbench.benchmarks.experiment.AbstractExperimentLevelsDesignRunner;
import br.ufsc.lapesd.unserved.testbench.benchmarks.experiment.ExperimentDesignLevels;
import br.ufsc.lapesd.unserved.testbench.benchmarks.experiment.ExperimentResult;
import com.google.gson.Gson;
import org.kohsuke.args4j.CmdLineParser;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class PPExperimentDesignRunner extends AbstractExperimentLevelsDesignRunner {
    public PPExperimentDesignRunner() {
        super(new PPExperimentFactory());
    }

    public static void main(String[] args) throws Exception {
        AbstractExperimentDesignRunner runner = new PPExperimentDesignRunner();
        new CmdLineParser(runner).parseArgument(args);
        runner.run();
    }

    @Override
    protected ExperimentDesignLevels loadLevels() throws FileNotFoundException {
        return new Gson().fromJson(new FileReader(levelsJson), PPExperimentDesignLevels.class);
    }

    @Override
    protected List<String> getResultHeaders() {
        return Arrays.asList("initialization", "output", "composition", "blacklisting",
                "execution", "total");
    }

    @Override
    protected List<String> getResultValues(ExperimentResult result) {
        TimesExperimentResult r = (TimesExperimentResult) result;
        return Stream.of(r.getInitialization(), r.getOutput(), r.getComposition(),
                r.getBlacklisting(), r.getExecution(), r.getTotal()).map(String::valueOf)
                .collect(Collectors.toList());
    }

    public static class Builder extends AbstractExperimentLevelsDesignRunner.Builder {
        public Builder(File designCsv, File levelsJson, File resultObjectsDir) {
            super(designCsv, levelsJson, resultObjectsDir);
        }

        @Override
        public PPExperimentDesignRunner build() {
            return setupAbstractRunner(new PPExperimentDesignRunner());
        }
    }

}

package br.ufsc.lapesd.unserved.testbench.benchmarks.wsc08;

import br.ufsc.lapesd.unserved.testbench.iri.HTTP;
import br.ufsc.lapesd.unserved.testbench.iri.Unserved;
import br.ufsc.lapesd.unserved.testbench.iri.UnservedX;
import br.ufsc.lapesd.unserved.testbench.model.Message;
import br.ufsc.lapesd.unserved.testbench.model.Part;
import br.ufsc.lapesd.unserved.testbench.model.Variable;
import br.ufsc.lapesd.unserved.testbench.util.BFSBGPIterator;
import br.ufsc.lapesd.unserved.testbench.util.FrequencyCappedLogger;
import br.ufsc.lapesd.unserved.testbench.util.RDFListWriter;
import com.google.common.base.Preconditions;
import com.google.gson.Gson;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.rdf.model.*;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFFormat;
import org.apache.jena.vocabulary.OWL2;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;
import org.kohsuke.args4j.Argument;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.spinrdf.vocabulary.SP;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

import static br.ufsc.lapesd.unserved.testbench.iri.UnservedX.RDF.AsProperty;
import static java.lang.Boolean.parseBoolean;
import static java.util.Arrays.asList;
import static java.util.Objects.requireNonNull;
import static java.util.stream.IntStream.range;
import static org.apache.jena.rdf.model.ResourceFactory.createProperty;
import static org.apache.jena.vocabulary.RDFS.subClassOf;

public class ShadowPreconditionAssigner implements PreconditionAssigner {
    private static Logger logger = LoggerFactory.getLogger(ShadowPreconditionAssigner.class);

    public static class Config {
        public enum PredicateStrategy {
            SINGLE {
                @Override
                public PredicateStrategyState build(Map<String, String> args) {
                    return new SinglePredicateStrategyState();
                }
            },
            FIXED_SET {
                @Override
                public PredicateStrategyState build(Map<String, String> args) {
                    return new FixedPredicateStrategyState(args);
                }
            },
            SHADOW {
                @Override
                public PredicateStrategyState build(Map<String, String> args) {
                    return new ShadowPredicateStrategyState();
                }
            };

            public abstract PredicateStrategyState build(Map<String, String> args);
        }
        public enum ConditionStrategy {
            NO_CONDITION {
                @Override
                public ConditionStrategyState build(Map<String, String> args) {
                    return new NoConditionConditionStrategyState(args);
                }
            },
            NO_DISJUNCTION {
                @Override
                public ConditionStrategyState build(Map<String, String> args) {
                    return new NoDisjunctionConditionStrategyState(args);
                }
            },
            DIVIDE {
                @Override
                public ConditionStrategyState build(Map<String, String> args) {
                    return new DivideConditionStrategyState(args);
                }
            };

            public abstract ConditionStrategyState build(Map<String, String> args);
        }

        int shadowTaxonomies = 1;
        PredicateStrategy predicateStrategy;
        Map<String, String> predicateStrategyArgs;

        ConditionStrategy conditionStrategy;
        Map<String, String> conditionStrategyArgs;

        @Override
        public String toString() {
            return String.format("Config(shadowTaxonomies=%d, predicate=%s%s, condition=%s%s)",
                    shadowTaxonomies, predicateStrategy, mapToString(predicateStrategyArgs),
                    conditionStrategy, mapToString(conditionStrategyArgs));
        }

        private static String mapToString(Map<String, String> map) {
            if (map == null) return "";
            String inner = map.entrySet().stream().map(e -> e.getKey() + "=" + e.getValue())
                    .reduce((l, r) -> l + ";" + r).orElse(null);
            return inner == null ? "" : " [" + inner + "]";
        }
    }

    protected interface PredicateStrategyState {
        void setup(@Nonnull File dir, @Nonnull Model taxonomy);
        Property getPredicate(@Nonnull Resource type);
    }

    protected interface ConditionStrategyState {
        void setup(@Nonnull File dir, @Nonnull Model taxonomy);
        void apply(@Nonnull File dir, @Nonnull PredicateStrategyState psState) throws IOException;
    }

    protected static class SinglePredicateStrategyState implements PredicateStrategyState {
        private static final Property predicate = createProperty(
                "http://example.org/#predicate");

        @Override
        public void setup(@Nonnull File dir, @Nonnull Model taxonomy) {}

        @Override
        public Property getPredicate(@Nonnull Resource type) {
            return predicate;
        }
    }

    protected static class FixedPredicateStrategyState implements PredicateStrategyState {
        private final @Nonnull Map<Resource, Integer> categoryMap = new HashMap<>();
        private Property rootProperty = null;
        private final int propertiesCount;
        private final @Nonnull List<Property> properties = new ArrayList<>();

        public FixedPredicateStrategyState(@Nonnull Map<String, String> args) {
            Preconditions.checkArgument(args.containsKey("count"), "Missing parameter: count");
            propertiesCount = Integer.parseInt(args.get("count"));
        }

        @Override
        public void setup(@Nonnull File dir, @Nonnull Model tax) {
            rootProperty = tax.createResource("http://example.org/#predicate-root")
                    .addProperty(RDF.type, RDF.Property).as(Property.class);

            Resource rootType = tax.listSubjectsWithProperty(RDF.type, OWL2.Class).next();
            while (rootType.getProperty(subClassOf) != null)
                rootType = rootType.getProperty(subClassOf).getResource();

            Queue<Resource> queue = new LinkedList<>();
            queue.add(rootType);
            while (!queue.isEmpty() && queue.size() < propertiesCount)
                queue.addAll(tax.listSubjectsWithProperty(subClassOf, queue.remove()).toList());
            Preconditions.checkArgument(queue.size() >= propertiesCount, "Insufficient types!");
            for (int i = 0; i < propertiesCount; i++) {
                final int category = i;
                properties.add(tax.createResource("http://example.org/#predicate-" + category)
                        .addProperty(RDF.type, RDF.Property)
                        .addProperty(RDFS.subPropertyOf, rootProperty).as(Property.class));
                BFSBGPIterator.from(queue.remove()).withInitial().backward(RDFS.subClassOf)
                        .toStream().forEach(type -> categoryMap.put(type, category));
            }
        }

        @Override
        public Property getPredicate(@Nonnull Resource type) {
            int idx = categoryMap.getOrDefault(type, -1);
            return idx < 0 ? rootProperty : properties.get(idx);
        }
    }

    protected static class ShadowPredicateStrategyState implements PredicateStrategyState {
        private Property mapToProperty(@Nonnull Resource type) {
            Preconditions.checkArgument(type.isURIResource());
            String u2 = type.getURI().replaceAll("taxonomy\\.owl#", "taxonomy.owl#has");
            Preconditions.checkArgument(!u2.equals(type.getURI()));
            return type.getModel().createProperty(u2);
        }

        @Override
        public void setup(@Nonnull File dir, @Nonnull Model taxonomy) {
            for (Resource type : taxonomy.listSubjectsWithProperty(RDF.type, OWL2.Class).toList()) {
                Resource prop = mapToProperty(type).addProperty(RDF.type, RDF.Property)
                        .addProperty(RDF.type, OWL2.ObjectProperty);
                type.getModel().listObjectsOfProperty(type, subClassOf).toList().forEach(
                        p -> prop.addProperty(RDFS.subPropertyOf, mapToProperty(p.asResource())));
            }
        }

        @Override
        public Property getPredicate(@Nonnull Resource type) {
            Preconditions.checkArgument(type.isURIResource());
            String u2 = type.getURI().replaceAll("taxonomy\\.owl#", "taxonomy.owl#has");
            Preconditions.checkArgument(!u2.equals(type.getURI()));
            return type.getModel().createProperty(u2);
        }
    }

    protected static class NoConditionConditionStrategyState implements ConditionStrategyState {
        private static final Logger staticLogger = LoggerFactory.getLogger(NoConditionConditionStrategyState.class);
        private final FrequencyCappedLogger logger = new FrequencyCappedLogger(staticLogger, 5000);
        private final int shadowCount;

        public NoConditionConditionStrategyState(Map<String, String> args) {
            if (args != null) {
                String str = args.getOrDefault("shadowCount", "1");
                shadowCount = Integer.parseInt(str);
                Preconditions.checkArgument(shadowCount > 0);
            } else {
                shadowCount = 1;
            }
        }

        @Override
        public void setup(@Nonnull File dir, @Nonnull Model taxonomy) { /* pass */ }

        @Override
        public void apply(@Nonnull File dir, @Nonnull PredicateStrategyState psState)
                throws IOException {
            if (!(psState instanceof SinglePredicateStrategyState))
                staticLogger.warn("{} ignores the PredicateStrategyState!");
            File servicesFile = new File(dir, "services.ttl");
            Model m = loadTurtle(servicesFile);
            addInputs(m);
            addOutputs(m);
            addKnown(dir);
            addWanted(dir);

            try (FileOutputStream out = new FileOutputStream(servicesFile)) {
                RDFDataMgr.write(out, m, RDFFormat.TURTLE_FLAT);
            }
            long expected = m.size();
            m.close();
            if (loadTurtle(servicesFile).size() != expected)
                throw new IOException("Lost triples after writing " + servicesFile);
        }

        private void addInputs(@Nonnull Model m) {
            List<Resource> list = new ArrayList<>();
            try (QueryExecution ex = QueryExecutionFactory.create(QueryFactory.create(
                    "PREFIX u: <" + Unserved.PREFIX + ">\n" +
                            "SELECT ?req { ?req ^u:reactionTo/^u:when/a u:Message. }"), m)) {
                ex.execSelect().forEachRemaining(s -> list.add(s.getResource("req")));
            }
            logger.setMax(list.size());
            long done = 0;
            for (Resource r : list) {
                for (Part part : r.as(Message.class).getParts()) {
                    for (int i = 0; i < shadowCount; i++) duplicatePart(r, part, i);
                }
                logger.info("Added inputs to {} of messages", logger.p(++done));
            }
        }

        private void addOutputs(@Nonnull Model m) {
            List<Resource> list = new ArrayList<>();
            try (QueryExecution ex = QueryExecutionFactory.create(QueryFactory.create(
                    "PREFIX u: <" + Unserved.PREFIX + ">\n" +
                            "SELECT ?rep WHERE { ?rep u:when/u:reactionTo/a u:Message.}"), m)) {
                ex.execSelect().forEachRemaining(s -> list.add(s.getResource("rep")));
            }
            logger.setMax(list.size());
            long done = 0;
            for (Resource r : list) {
                List<Variable> singleton = r.as(Message.class).getVariables();
                Preconditions.checkArgument(singleton.size() == 1);
                Variable replyBody = singleton.get(0);
                for (Part part : replyBody.getParts()) {
                    for (int i = 0; i < shadowCount; i++) duplicatePart(replyBody, part, i);
                }
                logger.info("Added outputs to {} of messages", logger.p(++done));
            }
        }

        private void addKnown(@Nonnull File dir) throws IOException {
            File file = new File(dir, "known.ttl");
            Model m = loadTurtle(file);

            List<Resource> list = m.listSubjectsWithProperty(RDF.type, Unserved.Variable).toList();
            for (Resource r : list) {
                Variable var = r.as(Variable.class);
                for (int i = 0; i < shadowCount; i++) {
                    Resource dup = var.isAnon() ? m.createResource()
                            : m.createResource(var.getURI() + "-shadow" + i);
                    Resource shadowType = mapTypeToShadow(var.getType(), i);
                    dup.addProperty(RDF.type, Unserved.Variable)
                            .addProperty(RDF.type, Unserved.ResourceBoundVariable)
                            .addProperty(RDF.type, Unserved.ValueBoundVariable)
                            .addProperty(Unserved.type, shadowType)
                            .addProperty(Unserved.representation, var.getRepresentation())
                            .addProperty(Unserved.resourceValue, m.createResource()
                                    .addProperty(RDF.type, shadowType));
                }
            }

            try (FileOutputStream out = new FileOutputStream(file)) {
                RDFDataMgr.write(out, m, Lang.TURTLE);
            }
            long expected = m.size();
            m.close();
            if (loadTurtle(file).size() != expected)
                throw new IOException("Lost triples on written " + file);
        }

        private void addWanted(@Nonnull File dir) throws IOException {
            File file = new File(dir, "wanted.ttl");
            Model m = loadTurtle(file);

            List<Resource> list = m.listSubjectsWithProperty(RDF.type, Unserved.Variable).toList();
            for (Resource r : list) {
                Variable var = r.as(Variable.class);
                for (int i = 0; i < shadowCount; i++) {
                    Resource dup = var.isAnon() ? m.createResource()
                            : m.createResource(var.getURI() + "-shadow" + i);
                    dup.addProperty(RDF.type, Unserved.Variable)
                            .addProperty(RDF.type, Unserved.Wanted)
                            .addProperty(Unserved.type, mapTypeToShadow(var.getType(), i))
                            .addProperty(Unserved.representation, var.getRepresentation());
                }
            }

            try (FileOutputStream out = new FileOutputStream(file)) {
                RDFDataMgr.write(out, m, Lang.TURTLE);
            }
            long expected = m.size();
            if (loadTurtle(file).size() != expected)
                throw new IOException("Lost triples serializing " + file);
        }

        private void duplicatePart(@Nonnull Resource partOf, @Nonnull Part part, int shadowIndex) {
            Model m = partOf.getModel();
            Resource shadowType = mapTypeToShadow(part.getVariable().getType(), shadowIndex);
            Resource newPart = m.createResource(Unserved.Part)
                    .addProperty(Unserved.variable, m.createResource(Unserved.Variable)
                        .addProperty(Unserved.type, shadowType)
                        .addProperty(Unserved.representation, Unserved.Resource));
            Resource modifier = part.getPartModifier();
            if (modifier != null)
                newPart.addProperty(Unserved.partModifier, modifier);

            partOf.addProperty(Unserved.part, newPart);
        }
    }

    protected static class NoDisjunctionConditionStrategyState implements ConditionStrategyState {
        private final Logger staticLogger = LoggerFactory.getLogger(NoDisjunctionConditionStrategyState.class);
        private FrequencyCappedLogger logger = new FrequencyCappedLogger(staticLogger, 5000);
        private final boolean spinPostconditions;

        public NoDisjunctionConditionStrategyState(Map<String, String> args) {
            if (args != null) {
                String string = args.getOrDefault("spinPostconditions", "false");
                spinPostconditions = parseBoolean(string);
            } else {
                spinPostconditions = false;
            }
        }

        @Override
        public void setup(@Nonnull File dir, @Nonnull Model taxonomy) {}

        @Override
        public void apply(@Nonnull File dir, @Nonnull PredicateStrategyState psState)
                throws IOException {
            File servicesFile = new File(dir, "services.ttl");
            Model m = loadTurtle(servicesFile);
            addPostConditions(psState, m);
            addPreconditions(psState, m);
            addGoalConditions(psState, dir);
            addKnownConditions(psState, dir);
            try (FileOutputStream out = new FileOutputStream(servicesFile)) {
                RDFDataMgr.write(out, addPrefixes(m), RDFFormat.TURTLE_FLAT);
            }

            long expected = m.size();
            m.close();
            if (loadTurtle(servicesFile).size() != expected)
                throw new IOException("Triples lost on serialization of " + servicesFile);
        }

        private void addKnownConditions(@Nonnull PredicateStrategyState psState,
                                        @Nonnull File dir) throws IOException {
            File knownFile = new File(dir, "known.ttl");
            Model m = loadTurtle(knownFile);
            List<Variable> vars = m.listSubjectsWithProperty(RDF.type, Unserved.Variable).toList()
                    .stream().map(r -> r.as(Variable.class)).collect(Collectors.toList());
            if (spinPostconditions) {
                addConditions(psState, m, m.createResource(), vars);
            } else {
                addParts(psState, m, vars);
            }

            try (FileOutputStream out = new FileOutputStream(knownFile)) {
                RDFDataMgr.write(out, m, Lang.TURTLE);
            }
            long expected = m.size();
            m.close();
            if (loadTurtle(knownFile).size() != expected)
                throw new IOException("Lost triples when saving " + knownFile);
        }

        private void addGoalConditions(@Nonnull PredicateStrategyState psState,
                                       @Nonnull File dir) throws IOException {
            File wantedFile = new File(dir, "wanted.ttl");
            Model m = loadTurtle(wantedFile);
            List<Variable> vars = m.listSubjectsWithProperty(RDF.type, Unserved.Variable).toList()
                    .stream().map(r -> r.as(Variable.class)).collect(Collectors.toList());
            Resource wanted = m.createResource(Unserved.Wanted);
            List<Variable> objs  = addConditions(psState, m, wanted, vars);
            objs.forEach(v -> v.addProperty(RDF.type, Unserved.Wanted));

            try (FileOutputStream out = new FileOutputStream(wantedFile)) {
                RDFDataMgr.write(out, addPrefixes(m), Lang.TURTLE);
            }
            if (loadTurtle(wantedFile).size() != m.size())
                throw new IOException("Lost triples when saving " + wantedFile);
        }

        private void addPreconditions(@Nonnull PredicateStrategyState psState, @Nonnull Model m) {
            List<Resource> list = new ArrayList<>();
            try (QueryExecution ex = QueryExecutionFactory.create(QueryFactory.create(
                    "PREFIX u: <" + Unserved.PREFIX + ">\n" +
                            "SELECT ?req WHERE { ?req ^u:reactionTo/^u:when/a u:Message. }\n"), m)) {
                ex.execSelect().forEachRemaining(s -> list.add(s.getResource("req")));
            }
            logger.setMax(list.size());
            int done = 0;
            for (Resource msg : list) {
                addConditions(psState, m, msg, msg.as(Message.class).getVariables());
                logger.info("Added preconditions to {} messages", logger.p(++done));
            }
        }

        private List<Variable> addConditions(@Nonnull PredicateStrategyState psState,
                                             @Nonnull Model m, @Nonnull Resource subject,
                                             @Nonnull List<Variable> vars) {
            List<Resource> tps = new ArrayList<>(vars.size());
            List<Variable> newVariables = new ArrayList<>();
            for (Variable in : vars) {
                Variable obj = mapToPostfixShadow(m, in, Unserved.Variable)
                        .addProperty(Unserved.type, mapTypeToShadow(in.getType()))
                        .addProperty(Unserved.representation, Unserved.Resource)
                        .as(Variable.class);
                duplicateVarPartsOf(in, obj, m);
                newVariables.add(obj);
                tps.add(m.createResource().addProperty(SP.subject, in)
                        .addProperty(SP.predicate, psState.getPredicate(in.getType()))
                        .addProperty(SP.object, obj));
                if (in.getValue() != null && in.getValue().isResource()) {
                    Resource objValue = m.createResource(mapTypeToShadow(in.getType()));
                    obj.set(-1, Unserved.Resource, objValue);
                    in.getResourceValue().addProperty(psState.getPredicate(in.getType()), objValue);
                }
            }
            subject.addProperty(Unserved.condition, RDFListWriter.write(m, tps));
            return newVariables;
        }

        private void addPostConditions(@Nonnull PredicateStrategyState psState, @Nonnull Model m) {
            if (spinPostconditions) {
                List<Message> list = new ArrayList<>();
                try (QueryExecution ex = QueryExecutionFactory.create(QueryFactory.create(
                        "PREFIX u: <" + Unserved.PREFIX + ">\n" +
                                "SELECT ?rep WHERE { ?rep u:when/u:reactionTo ?req.}"), m)) {
                    ex.execSelect().forEachRemaining(
                            s -> list.add(s.getResource("rep").as(Message.class)));
                }
                for (Message msg : list) {
                    List<Variable> bodies = msg.getVariables();
                    Preconditions.checkArgument(bodies.size() == 1);
                    List<Variable> outs = new ArrayList<>(bodies.get(0).getVariablesRecursive());
                    addConditions(psState, m, msg, outs);
                }
            } else {
                List<Variable> list = new ArrayList<>();
                try (QueryExecution execution = QueryExecutionFactory.create(QueryFactory.create(
                        "PREFIX u: <" + Unserved.PREFIX + ">\n" +
                                "SELECT ?out ?type WHERE {\n" +
                                "  ?out ^u:variable/^u:part/^u:variable/^u:part/u:when/u:reactionTo ?req." +
                                "}"), m)) {
                    execution.execSelect().forEachRemaining(
                            s -> list.add(s.getResource("out").as(Variable.class)));
                }
                addParts(psState, m, list);
            }
        }

        private void addParts(PredicateStrategyState psState, Model m, List<Variable> list) {
            logger.setMax(list.size());
            int done = 0;
            for (Variable var : list) {
                Variable obj = mapToPostfixShadow(m, var, Unserved.Variable)
                        .addProperty(Unserved.representation, Unserved.Resource)
                        .addProperty(Unserved.type, mapTypeToShadow(var.getType()))
                        .as(Variable.class);
                var.addProperty(Unserved.part, m.createResource(Unserved.Part)
                        .addProperty(Unserved.variable, obj)
                        .addProperty(Unserved.partModifier, m.createResource(AsProperty)
                                .addProperty(UnservedX.RDF.property, psState.getPredicate(var.getType()))
                                .addProperty(UnservedX.RDF.propertyOf, var)));
                if (var.getValue() != null && var.getValue().isResource()) {
                    Resource objVal = m.createResource(mapTypeToShadow(var.getType()));
                    obj.set(-1, Unserved.Resource, objVal);
                    var.getResourceValue().addProperty(psState.getPredicate(var.getType()), objVal);
                }
                logger.info("Added postconditions to {} outputs", logger.p(++done));
            }
        }
    }

    protected static class DivideConditionStrategyState implements ConditionStrategyState {
        private static final Logger staticLogger = LoggerFactory.getLogger(DivideConditionStrategyState.class);
        private final FrequencyCappedLogger logger = new FrequencyCappedLogger(staticLogger, 5000);

        private final @Nonnull Map<Resource, Integer> groupMap = new HashMap<>();
        private final int groups;
        private final @Nonnull Random random;

        public DivideConditionStrategyState(Map<String, String> args) {
            String groupsString = args.getOrDefault("groups", null);
            Preconditions.checkArgument(groupsString != null, "groups arg is missing");
            groups = Integer.parseInt(groupsString);

            String seedStr = args.getOrDefault("seed", null);
            random = seedStr == null ? new Random() : new Random(Long.parseLong(seedStr));
        }

        @Override
        public void setup(@Nonnull File dir, @Nonnull Model taxonomy) {
            List<Resource> list = taxonomy.listSubjectsWithProperty(RDF.type, OWL2.Class).toList();
            Collections.shuffle(list, random);
            int g = 0;
            for (Resource type : list) {
                groupMap.put(type, g);
                g = (g+1) % groups;
            }
        }

        @Override
        public void apply(@Nonnull File dir,
                          @Nonnull PredicateStrategyState psState) throws IOException {
            File servicesFile = new File(dir, "services.ttl");
            Model m = loadTurtle(servicesFile);
            addPostConditions(psState, m);
            addPreConditions(psState, m);
            addGoalConditions(psState, dir);
            addKnownConditions(psState, dir);
            try (FileOutputStream out = new FileOutputStream(servicesFile)) {
                RDFDataMgr.write(out, addPrefixes(m), RDFFormat.TURTLE_FLAT);
            }

            long expected = m.size();
            m.close();
            if (loadTurtle(servicesFile).size() != expected)
                throw new IOException("Triples lost when serializing to "  + servicesFile);

        }

        private void addKnownConditions(@Nonnull PredicateStrategyState psState,
                                        @Nonnull File dir) throws IOException {
            File knownFile = new File(dir, "known.ttl");
            Model m = loadTurtle(knownFile);
            m.listSubjectsWithProperty(RDF.type, Unserved.Variable).toList().forEach(r -> {
                Variable v = r.as(Variable.class);
                Variable obj = mapToPostfixShadow(m, v, Unserved.Variable)
                        .addProperty(Unserved.representation, Unserved.Resource)
                        .addProperty(Unserved.type, mapTypeToShadow(v.getType()))
                        .as(Variable.class);
                v.addProperty(Unserved.part, m.createResource(Unserved.Part)
                        .addProperty(Unserved.variable, obj)
                        .addProperty(Unserved.partModifier, m.createResource(AsProperty)
                                .addProperty(UnservedX.RDF.property, psState.getPredicate(v.getType()))
                                .addProperty(UnservedX.RDF.propertyOf, v)));
                if (v.getValue() != null && v.getValue().isResource()) {
                    Resource objValue = m.createResource(mapTypeToShadow(v.getType()));
                    obj.set(-1, Unserved.Resource, objValue);
                    v.getResourceValue().addProperty(psState.getPredicate(v.getType()), objValue);
                }

            });

            try (FileOutputStream out = new FileOutputStream(knownFile)) {
                RDFDataMgr.write(out, addPrefixes(m), Lang.TURTLE);
            }
            if (loadTurtle(knownFile).size() != m.size())
                throw new IOException("Lost triples when serializing " + knownFile);
        }

        private void addGoalConditions(@Nonnull PredicateStrategyState psState,
                                       @Nonnull File dir) throws IOException {
            File wantedFile = new File(dir, "wanted.ttl");
            Model m = loadTurtle(wantedFile);
            List<Variable> vars = m.listSubjectsWithProperty(RDF.type, Unserved.Variable)
                    .toList().stream().map(r -> r.as(Variable.class)).collect(Collectors.toList());
            addConditions(m.createResource(Unserved.Wanted), vars, psState, m);

            try (FileOutputStream out = new FileOutputStream(wantedFile)) {
                RDFDataMgr.write(out, addPrefixes(m), Lang.TURTLE);
            }
            if (loadTurtle(wantedFile).size() != m.size())
                throw new IOException("Lost triples when serializing " + wantedFile);
        }

        private void addPreConditions(@Nonnull PredicateStrategyState psState, @Nonnull Model m) {
            try (QueryExecution ex = QueryExecutionFactory.create(QueryFactory.create(
                    "PREFIX u: <" + Unserved.PREFIX + ">\n" +
                            "SELECT ?msg WHERE { ?msg ^u:reactionTo/^u:when/a u:Message. }\n"), m)) {
                List<Resource> list = new ArrayList<>();
                ex.execSelect().forEachRemaining(s -> list.add(s.getResource("msg")));
                logger.setMax(list.size());
                int done = 0;
                for (Resource msg : list) {
                    addConditions(msg, msg.as(Message.class).getVariablesRecursive(), psState, m);
                    logger.info("Added pre-conditions to {} messages", logger.p(++done));
                }
            }
        }

        private void addPostConditions(@Nonnull PredicateStrategyState psState, @Nonnull Model m) {
            try (QueryExecution ex = QueryExecutionFactory.create(QueryFactory.create(
                    "PREFIX u: <" + Unserved.PREFIX + ">\n" +
                            "SELECT ?msg WHERE { ?msg  u:when/u:reactionTo ?other. }\n"), m)) {
                List<Resource> list = new ArrayList<>();
                ex.execSelect().forEachRemaining(s -> list.add(s.getResource("msg")));
                logger.setMax(list.size());
                int done = 0;
                for (Resource msg : list) {
                    addConditions(msg, msg.as(Message.class).getVariablesRecursive(), psState, m);
                    logger.info("Added post-conditions to {} messages", logger.p(++done));
                }
            }
        }

        private void addConditions(@Nonnull Resource subject, @Nonnull Collection<Variable> vars,
                                   @Nonnull PredicateStrategyState psState, @Nonnull Model m) {
            List<Resource> rdfLists = range(0, groups).mapToObj(i -> vars.stream()
                    .filter(v -> v.getType().isURIResource() && groupMap.get(v.getType()) == i)
                    .map(v -> {
                        Variable obj = mapToPostfixShadow(m, v, Unserved.Variable)
                                .addProperty(Unserved.type, mapTypeToShadow(v.getType()))
                                .addProperty(Unserved.representation, Unserved.Resource)
                                .as(Variable.class);
                        duplicateVarPartsOf(v, obj, m);
                        return m.createResource()
                                .addProperty(SP.subject, v)
                                .addProperty(SP.predicate, psState.getPredicate(v.getType()))
                                .addProperty(SP.object, obj);
                    })
                    .collect(Collectors.toList()))
                    .filter(l -> !l.isEmpty())
                    .map(l -> RDFListWriter.write(m, l))
                    .collect(Collectors.toList());
            subject.addProperty(Unserved.condition, m.createResource(SP.Union)
                    .addProperty(SP.elements, RDFListWriter.write(m, rdfLists)));
        }
    }

    private final @Nonnull Config config;
    private PredicateStrategyState psState = null;
    private ConditionStrategyState csState = null;


    public ShadowPreconditionAssigner(@Nonnull Config config) {
        Preconditions.checkArgument(config.shadowTaxonomies > 0);
        this.config = config;
    }

    public static ShadowPreconditionAssigner fromFile(@Nonnull File file)  throws IOException {
        try (FileReader reader = new FileReader(file)) {
            return new ShadowPreconditionAssigner(new Gson().fromJson(reader, Config.class));
        }
    }

    public static class App {
        private static final Logger logger = LoggerFactory.getLogger(App.class);

        @Option(name = "--help", aliases = {"-h"}, help = true, usage = "Shows this help message")
        private boolean help = false;

        @Option(name = "--config", aliases = {"-c"}, required = true,
                usage = "Path to the JSON config file")
        private File config;

        @Argument(metaVar = "DIR", usage = "Path to the root of WSC converted problems " +
                "(containing sub dirs 01, 02, ...) or to a particular WSC problem directory")
        private File dir;

        public static void main(String[] args) throws Exception {
            App app = new App();
            CmdLineParser parser = new CmdLineParser(app);
            parser.parseArgument(args);
            if (app.help) parser.printUsage(System.out);
            else app.run();
        }

        private void run() throws IOException {
            dir = dir.getCanonicalFile();
            config = config.getCanonicalFile();
            Preconditions.checkArgument(dir.exists() && dir.isDirectory(),
                    "Directory "+dir+" must exist (and contain a WSC08 copy)!");
            Preconditions.checkArgument(config.exists() && config.isFile(),
                    "Configuration file "+config+" must exist!");

            ShadowPreconditionAssigner assigner = fromFile(config);

            File[] problemDirs = dir.listFiles(f -> f.getName().matches("^0[0-9]$"));
            if (problemDirs == null) throw new IOException("Problem reading " + dir);
            if (problemDirs.length > 0) {
                assigner.addTo(dir);
            } else {
                logger.info("Config: {}", assigner.config);
                assigner.addToProblem(dir);
            }
        }
    }

    @Override
    public void addTo(@Nonnull File convertedRoot) throws IOException {
        convertedRoot = convertedRoot.getCanonicalFile();
        Preconditions.checkArgument(convertedRoot.isDirectory());
        List<File> problems = asList(requireNonNull(convertedRoot.listFiles(File::isDirectory)));
        Collections.sort(problems);
        logger.info("Config: {}", config);
        for (File problem : problems) addToProblem(problem);
    }

    public void addToProblem(File dir) throws IOException {
        Preconditions.checkState(!(new File(dir, "shadow_preconditions.stamp")).exists());
        String problem = dir.getName();
        logger.info("Adding conditions to problem {}...", problem);

        createShadowTaxonomy(dir);
        csState.apply(dir, psState);

        new FileOutputStream(new File(dir, "shadow_preconditions.stamp")).close();

        Model m = loadTurtle(new File(dir, "services.ttl"));
        long count[] = {0};
        m.listSubjectsWithProperty(Unserved.condition).forEachRemaining(x -> ++count[0]);
        logger.info("DONE. Added conditions to {} messages in problem {}.", count[0], problem);
    }

    private static RDFNode mapToShadow(@Nullable RDFNode node) {
        return mapToShadow(node, 0);
    }
    private static RDFNode mapToShadow(@Nullable RDFNode node, int index) {
        if (node == null) return null;
        Preconditions.checkArgument(node.getModel() != null);
        if (!node.isURIResource()) return node;
        Resource r = node.asResource();
        if (!r.isURIResource() || !r.hasProperty(RDF.type, OWL2.Class)) return r;
        if (!r.getURI().contains("taxonomy.owl")) return r;
        String shadowOwl = String.format("taxonomy-shadow%d.owl", index);
        String u2 = r.getURI().replaceAll("taxonomy\\.owl", shadowOwl);
        return node.getModel().createResource(u2);
    }

    private static Resource mapTypeToShadow(@Nonnull Resource type) {
        return mapTypeToShadow(type, 0);
    }
    private static Resource mapTypeToShadow(@Nonnull Resource type, int index) {
        Preconditions.checkArgument(type.isURIResource());
        Preconditions.checkArgument(type.getURI().contains("taxonomy.owl"));
        String shadowOwl = String.format("taxonomy-shadow%d.owl", index);
        String u2 = type.getURI().replaceAll("taxonomy\\.owl", shadowOwl);
        return type.getModel().createResource(u2);
    }

    private static Resource mapToPostfixShadow(@Nonnull Model m, @Nullable Resource resource,
                                               @Nullable Resource type) {
        if (resource == null) return null;
        if (!resource.isURIResource())
            return type == null ? m.createResource() : m.createResource(type);
        return type == null ? m.createResource(resource.getURI() + "-shadow")
                            : m.createResource(resource.getURI() + "-shadow", type);
    }

    private static void duplicateVarPartsOf(@Nonnull Variable main, @Nonnull Variable shadow,
                                            @Nonnull Model m) {
        List<ImmutablePair<Resource, Resource>> pairs = new ArrayList<>();
        m.listSubjectsWithProperty(Unserved.variable, main).forEachRemaining(
                part -> m.listSubjectsWithProperty(Unserved.part, part).forEachRemaining(
                        owner -> pairs.add(ImmutablePair.of(owner, part))));
        pairs.forEach(p -> {
            Resource part = m.createResource(Unserved.Part).addProperty(Unserved.variable, shadow);
            p.left.addProperty(Unserved.part, part);
            if (p.right.hasProperty(Unserved.partModifier)) {
                part.addProperty(Unserved.partModifier,
                        p.right.getPropertyResourceValue(Unserved.partModifier));
            }
        });
    }

    protected void createShadowTaxonomy(File dir) throws IOException {
        Model m = loadTurtle(new File(dir, "taxonomy.ttl"));
        psState = config.predicateStrategy.build(config.predicateStrategyArgs);
        psState.setup(dir, m);
        csState = config.conditionStrategy.build(config.conditionStrategyArgs);
        csState.setup(dir, m);
        for (int i = 0; i < config.shadowTaxonomies; i++) {
            for (Resource type : m.listSubjectsWithProperty(RDF.type, OWL2.Class).toList()) {
                Resource shadow = (Resource) mapToShadow(type, i);
                for (StmtIterator it = type.listProperties(); it.hasNext(); ) {
                    Statement stmt = it.next();
                    shadow.addProperty(stmt.getPredicate(), mapToShadow(stmt.getObject(), i));
                }
            }
        }
        try (FileOutputStream out = new FileOutputStream(new File(dir, "taxonomy.ttl"))) {
            RDFDataMgr.write(out, addPrefixes(m), Lang.TURTLE);
        }
        if (loadTurtle(new File(dir, "taxonomy.ttl")).size() != m.size())
            throw new IOException("Written taxonomy.ttl lost triples!");
    }

    @Nonnull
    static protected Model loadTurtle(@Nonnull File file) throws IOException {
        Model m = ModelFactory.createDefaultModel();
        try (FileInputStream in = new FileInputStream(file)) {
            RDFDataMgr.read(m, in, Lang.TURTLE);
        }
        return m;
    }

    static protected @Nonnull Model addPrefixes(@Nonnull Model model) {
        String mappings[][] = {{"sp", SP.NS}, {"rdf", RDF.getURI()}, {"rdfs", RDFS.getURI()},
                {"owl", OWL2.getURI()}, {"http", HTTP.PREFIX}};

        for (String[] mapping : mappings) {
            if (model.getNsURIPrefix(mapping[1]) == null) {
                Preconditions.checkArgument(model.getNsURIPrefix(mapping[0]) == null);
                model.setNsPrefix(mapping[0], mapping[1]);
            }
        }

        String uri = model.getNsPrefixURI("tax");
        Preconditions.checkArgument(uri != null);
        Preconditions.checkArgument(uri.contains("taxonomy.owl#"));
        model.setNsPrefix("tax-shadow",
                uri.replaceAll("taxonomy\\.owl#", "taxonomy-shadow.owl#"));
        return model;
    }

    static {
        Unserved.init();
    }
}

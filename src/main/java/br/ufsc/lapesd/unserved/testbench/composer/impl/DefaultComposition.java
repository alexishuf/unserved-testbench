package br.ufsc.lapesd.unserved.testbench.composer.impl;

import br.ufsc.lapesd.unserved.testbench.composer.Composition;
import br.ufsc.lapesd.unserved.testbench.process.data.DataContext;
import br.ufsc.lapesd.unserved.testbench.process.data.SimpleDataContext;
import br.ufsc.lapesd.unserved.testbench.util.SkolemizerMap;
import br.ufsc.lapesd.unserved.testbench.util.jena.UncloseableGraph;
import com.google.common.base.Preconditions;
import org.apache.jena.graph.compose.Delta;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;

import javax.annotation.Nonnull;

public class DefaultComposition implements Composition {
    private boolean closed = false;
    private Model model;
    @Nonnull private final SkolemizerMap skolemizerMap;
    @Nonnull private final Resource root;

    public DefaultComposition(@Nonnull Model unmodifiableModel,
                              @Nonnull SkolemizerMap skolemizerMap,
                              @Nonnull Model compositionModel, @Nonnull Resource root) {
        Model data = ModelFactory.createModelForGraph(new Delta(
                new UncloseableGraph(unmodifiableModel.getGraph())));
        model = ModelFactory.createUnion(data, compositionModel);
        this.skolemizerMap = skolemizerMap;
        this.root = root.isAnon() ? model.createResource(root.getId())
                : model.createResource(root.getURI());
    }

    @Override
    public boolean isNull() {
        return false;
    }

    @Nonnull
    @Override
    public Resource getWorkflowRoot() {
        Preconditions.checkState(!closed);
        return root;
    }

    @Nonnull
    @Override
    public DataContext createDataContext() {
        Preconditions.checkState(!closed);
        Model model = ModelFactory.createModelForGraph(new UncloseableGraph(this.model.getGraph()));
        return new SimpleDataContext(model, r -> {
            Resource resolved = skolemizerMap.resolve(r);
            return resolved == null ? r : resolved;
        });
    }

    @Override
    public void close() {
        if (!closed) {
            model.close();
            closed = true;
        }
    }

    @Override
    public Model takeModel() {
        Preconditions.checkState(!closed);
        Model model = this.model;
        this.model = null;
        return model;
    }
}

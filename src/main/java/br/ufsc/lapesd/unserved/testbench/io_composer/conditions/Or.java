package br.ufsc.lapesd.unserved.testbench.io_composer.conditions;

import br.ufsc.lapesd.unserved.testbench.model.Variable;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public class Or extends ConditionList {
    public static final Or EMPTY = new Or();

    public Or(Condition... conditions) {
        super("|", conditions);
    }
    public static Or empty() { return EMPTY; }

    public Or(@Nonnull Collection<Condition> collection) {
        super("|", collection);
    }

    @Nonnull
    @Override
    public Condition replacing(@Nonnull Map<Variable, Variable> current2replacement) {
        List<Condition> replaced = new ArrayList<>(getMembers().size());
        for (Condition child : getMembers())
            replaced.add(child.replacing(current2replacement));
        return new Or(replaced);
    }
}

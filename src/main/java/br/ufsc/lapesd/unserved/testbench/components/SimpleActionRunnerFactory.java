package br.ufsc.lapesd.unserved.testbench.components;

import br.ufsc.lapesd.unserved.testbench.input.NonClosingRDFInput;
import br.ufsc.lapesd.unserved.testbench.input.RDFInput;
import br.ufsc.lapesd.unserved.testbench.input.RDFInputModel;
import br.ufsc.lapesd.unserved.testbench.iri.Testbench;
import br.ufsc.lapesd.unserved.testbench.iri.UnservedC;
import br.ufsc.lapesd.unserved.testbench.iri.UnservedJ;
import br.ufsc.lapesd.unserved.testbench.process.actions.ActionRunner;
import br.ufsc.lapesd.unserved.testbench.process.actions.ActionRunnerFactory;
import com.google.common.base.Preconditions;
import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.graph.compose.Union;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;
import org.apache.jena.riot.RDFFormat;
import org.apache.jena.vocabulary.RDF;

import javax.annotation.Nonnull;
import java.lang.reflect.Constructor;

public class SimpleActionRunnerFactory implements ActionRunnerFactory {
    private final RDFInput description;
    private final Resource runner;
    private Constructor<? extends ActionRunner> constructor;

    public SimpleActionRunnerFactory(@Nonnull Class<? extends ActionRunner> implementation,
                                     @Nonnull Class<? extends ActionRunnerFactory> factory,
                                     @Nonnull Resource actionClass) {
        Preconditions.checkArgument(actionClass.getModel() != null);
        try {
            constructor = implementation.getConstructor();
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e);
        }

        Model newModel = ModelFactory.createDefaultModel();
        newModel.setNsPrefix("tb", Testbench.PREFIX);
        Union union = new Union(newModel.getGraph(), actionClass.getModel().getGraph());
        Model model = ModelFactory.createModelForGraph(union);

        runner = model.createResource(Testbench.PREFIX + implementation.getSimpleName());
        model.add(runner, RDF.type, UnservedC.ActionRunner);
        model.add(runner, RDF.type, UnservedC.Component);
        model.add(runner, UnservedJ.factoryClassName, ResourceFactory.createTypedLiteral(
                factory.getName(), XSDDatatype.XSDstring));
        model.add(runner, UnservedC.actionClass, actionClass);

        description = new RDFInputModel(model, RDFFormat.TURTLE);
    }

    @Override
    public @Nonnull RDFInput getDescription() {
        return new NonClosingRDFInput(description);
    }

    @Nonnull
    @Override
    public Resource getComponentResource() {
        return runner;
    }

    @Override
    public ActionRunner newInstance() {
        try {
            return constructor.newInstance();
        } catch (ReflectiveOperationException e) {
            throw new RuntimeException(e);
        }
    }
}

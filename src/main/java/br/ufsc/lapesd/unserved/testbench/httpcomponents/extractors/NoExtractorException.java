package br.ufsc.lapesd.unserved.testbench.httpcomponents.extractors;

public class NoExtractorException extends Exception {
    public NoExtractorException() {
        super("Repository has no registered extractor able to handle given Part");
    }
}

package br.ufsc.lapesd.unserved.testbench.util;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;
import java.util.stream.Stream;

/**
 * A Quick & dirty Set over an ArrayList.
 *
 * WARNING: this class does not enforce elements are unique, it blindly believes it is
 * fed with distinct values.
 */
public class DangerousArrayListSet<E> implements Set<E> {
    private ArrayList<E> al;

    public DangerousArrayListSet(ArrayList<E> al) {
        this.al = al;
    }

    public DangerousArrayListSet() {
        al = new ArrayList<>();
    }

    @Override
    public int size() {
        return al.size();
    }

    @Override
    public boolean isEmpty() {
        return al.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return al.contains(o);
    }

    @Nonnull
    @Override
    public Iterator<E> iterator() {
        return al.iterator();
    }

    @Nonnull
    @Override
    public Object[] toArray() {
        return al.toArray();
    }

    @Nonnull
    @Override
    public <T> T[] toArray(@Nonnull T[] a) {
        //noinspection SuspiciousToArrayCall
        return al.toArray(a);
    }

    @Override
    public boolean add(E e) {
        return al.add(e);
    }

    @Override
    public boolean remove(Object o) {
        return al.remove(o);
    }

    @Override
    public boolean containsAll(@Nonnull Collection<?> c) {
        return al.containsAll(c);
    }

    @Override
    public boolean addAll(@Nonnull Collection<? extends E> c) {
        return al.addAll(c);
    }

    @Override
    public boolean retainAll(@Nonnull Collection<?> c) {
        return al.retainAll(c);
    }

    @Override
    public boolean removeAll(@Nonnull Collection<?> c) {
        return al.removeAll(c);
    }

    @Override
    public void clear() {
        al.clear();
    }

    @SuppressWarnings("EqualsWhichDoesntCheckParameterClass")
    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Collection)) return false;
        @SuppressWarnings("unchecked") Collection<E> other = (Collection<E>) o;
        return stream().allMatch(other::contains) && other.size() == size();
    }

    @Override
    public int hashCode() {
        return al.hashCode();
    }

    @Override
    public String toString() {
        return al.toString();
    }

    @Override
    public Stream<E> stream() {
        return al.stream();
    }
}

package br.ufsc.lapesd.unserved.testbench.input;

import org.apache.commons.io.IOUtils;

import javax.annotation.Nullable;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;

public class InputURI extends AbstractInput {
    private final String uri;
    private File tempFile;

    public InputURI(String mediaType, String uri) {
        super(mediaType);
        this.uri = uri;
    }

    @Override
    public File toFile() throws IOException {
        if (tempFile != null) return tempFile;

        File file = Files.createTempFile("unserved-testbench", "").toFile();
        String uri = toURI();
        if (tryDownloadURL(uri))
            tempFile = file;
        else
            throw new IOException("Don't know how to handle uri \"" + uri + "\"");

        return tempFile;
    }

    @Nullable
    @Override
    public String getBaseURI() {
        return uri;
    }

    @Override
    public String toURI() throws IOException {
        return uri;
    }

    @Override
    public void close() throws IOException {
        /* pass */
    }

    @Override
    public String toString() {
        return String.format("InputURI(%s, %s)", getMediaType(), uri);
    }

    private boolean tryDownloadURL(String uri) throws IOException {
        URL url = null;
        try {
            url = new URL(uri);
        } catch (MalformedURLException e) {
            return false;
        }

        try (FileOutputStream out = new FileOutputStream(tempFile)) {
            URLConnection conn = url.openConnection();
            conn.connect();

            IOUtils.copy(conn.getInputStream(), out);
        }
        return true;
    }
}

package br.ufsc.lapesd.unserved.testbench.components;

public class ComponentRegistrationException extends Exception {
    public ComponentRegistrationException(String s) {
        super(s);
    }

    public ComponentRegistrationException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public ComponentRegistrationException(Throwable throwable) {
        super(throwable);
    }
}

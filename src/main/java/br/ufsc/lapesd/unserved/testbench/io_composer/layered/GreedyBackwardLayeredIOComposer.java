package br.ufsc.lapesd.unserved.testbench.io_composer.layered;

import br.ufsc.lapesd.unserved.testbench.composer.NoPlanComposerException;
import br.ufsc.lapesd.unserved.testbench.io_composer.IOComposer;
import br.ufsc.lapesd.unserved.testbench.io_composer.impl.DefaultIOComposerTimes;
import br.ufsc.lapesd.unserved.testbench.io_composer.impl.IOComposerInput;
import br.ufsc.lapesd.unserved.testbench.io_composer.impl.IOComposerInputDelta;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.LayeredGraph;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.lazy.BackwardSetNodeProvider;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.lazy.LazyLayeredServiceSetGraph;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.lazy.SetNode;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.lazy.SetNodeEdge;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.LayeredServiceGraph;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.discovery.IOLayersState;
import br.ufsc.lapesd.unserved.testbench.io_composer.state.Action;
import br.ufsc.lapesd.unserved.testbench.io_composer.state.ExecutionPath;
import br.ufsc.lapesd.unserved.testbench.process.data.DataContextDiff;
import com.google.common.base.Preconditions;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Function;

public class GreedyBackwardLayeredIOComposer extends LayeredServiceSetGraphIOComposer {
    public GreedyBackwardLayeredIOComposer(@Nonnull IOComposerInput ci,
                                           @Nonnull Function<IOComposerInput, IOLayersState> layersStateFactory,
                                           @Nonnull BackwardSetNodeProvider backwardSetNodeProvider,
                                           @Nonnull List<Function<LayeredServiceGraph, LayeredServiceGraph>> optimizers) {
        super(ci, layersStateFactory, backwardSetNodeProvider, optimizers);
    }

    @Override
    protected AbstractLayeredIOComposer.Execution<SetNodeEdge, SetNode>
    createExecution() {
        DefaultIOComposerTimes times = createTimes();
        LazyLayeredServiceSetGraph graph = createGraph(times);
        return new Execution(ci, graph, cost, heuristic, times);
    }

    protected static class Execution
            extends AbstractLayeredIOComposer.Execution<SetNodeEdge, SetNode> {
        public Execution(@Nonnull IOComposerInput ci,
                         @Nonnull LayeredGraph<Action, SetNodeEdge, SetNode> graph,
                         @Nonnull CostFunction<SetNodeEdge, SetNode> cost,
                         @Nonnull CostFunction<SetNodeEdge, SetNode> heuristic,
                         @Nonnull DefaultIOComposerTimes t) {
            super(ci, graph, cost, heuristic, t);
        }

        public Execution(@Nonnull IOComposerInputDelta ci,
                         @Nonnull LayeredGraph<Action, SetNodeEdge, SetNode> graph,
                         @Nonnull CostFunction<SetNodeEdge, SetNode> cost,
                         @Nonnull CostFunction<SetNodeEdge, SetNode> heuristic,
                         @Nonnull DefaultIOComposerTimes times,
                         @Nonnull ExecutionPath<SetNodeEdge> graphPath,
                         @Nonnull ExecutionPath<Action> diffPath,
                         @Nonnull ExecutionPath<Action> remainingEdgePath,
                         @Nonnull SetNode start, LayeredProgress progress,
                         LayeredGraph.Validator<Action, SetNode> validator,
                         DataContextDiff diff, SetNodeEdge edge) {
            super(ci, graph, cost, heuristic, times, graphPath, diffPath, remainingEdgePath, start,
                    progress, validator, diff, edge);
        }

        @Override
        public Execution duplicate() {
            Preconditions.checkState(!closed);
            LayeredGraph.Duplicator<Action, SetNodeEdge, SetNode> duplicator;
            duplicator = graph.createWriteIsolatedDuplicator();
            LazyLayeredServiceSetGraph graphDup;
            graphDup = (LazyLayeredServiceSetGraph)duplicator.getDuplicate();

            ExecutionPath<SetNodeEdge> graphPathDuplicate = new ExecutionPath<>(graphPath.asList());
            LayeredGraph.Validator<Action, SetNode> validatorDuplicate = validator == null ? null :
                    duplicator.translateValidator(validator);

            return new Execution(new IOComposerInputDelta(ci),
                    graphDup, cost, heuristic, times.duplicate(),
                    graphPathDuplicate, new ExecutionPath<>(diffPath),
                    new ExecutionPath<>(remainingEdgePath), start,
                    progress, validatorDuplicate, diff, edge);
        }

        @Nonnull
        @Override
        protected ExecutionPath<SetNodeEdge> findPath() {
            LazyLayeredServiceSetGraph g = (LazyLayeredServiceSetGraph) this.graph;

            SetNode node = g.getEnd();
            List<SetNodeEdge> edges = new LinkedList<>();
            while (node != start) {
                SetNode finalNode = node;
                SetNode predecessor = g.predecessors(node).stream()
                        .min(Comparator.comparing(p -> cost.get(g, p, finalNode)))
                        .orElseThrow(() -> new NoPlanComposerException("No path from "
                                + finalNode + " to start node"));
                edges.add(0, g.edge(predecessor, node));
                node = predecessor;
            }
            assert !edges.isEmpty();
            assert edges.get(0).getFrom().equals(start);
            return new ExecutionPath<>(new ArrayList<>(edges));
        }
    }

    public static class Builder extends LayeredServiceSetGraphIOComposer.Builder {
        @Override
        protected IOComposer buildImpl() {
            return new GreedyBackwardLayeredIOComposer(composerInput, layersStateFactory,
                    backwardSetNodeProvider, optimizers);
        }
    }
}

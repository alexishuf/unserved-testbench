package br.ufsc.lapesd.unserved.testbench.reasoner.impl;

import br.ufsc.lapesd.unserved.testbench.input.RDFInput;
import br.ufsc.lapesd.unserved.testbench.reasoner.N3Reasoner;
import br.ufsc.lapesd.unserved.testbench.reasoner.N3ReasonerException;
import com.google.common.base.Preconditions;
import org.apache.commons.io.IOUtils;
import org.apache.jena.riot.Lang;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Wrapper for an EYE reasoner process.
 */
public class EYEProcess implements N3Reasoner {
    private final String path;
    private List<RDFInput> inputs = new ArrayList<>();
    private RDFInput queryFile = null;
    private boolean explainProof = false;
    private Process process;
    private InputStream errorStream;

    public EYEProcess(@Nonnull String path) {
        Preconditions.checkNotNull(path);
        this.path = path;
    }

    @Override
    public List<Lang> supportedInputLanguages() {
        ArrayList<Lang> list = new ArrayList<>();
        list.add(Lang.TURTLE);
        list.add(Lang.N3);
        list.add(Lang.NTRIPLES);
        return list;
    }

    @Override
    public List<Lang> supportedDownloadLanguages() {
        return supportedInputLanguages();
    }

    @Override
    public void addInput(@Nonnull RDFInput input) {
        inputs.add(input);
    }

    @Override
    public void setQueryFile(@Nonnull RDFInput input) {
        Preconditions.checkArgument(input.getLang().equals(Lang.N3));
        queryFile = input;
    }

    @Override
    public void queryDeductiveClosure() {
        queryFile = null;
    }

    @Override
    public boolean getExplainProof() {
        return explainProof;
    }

    @Override
    public void setExplainProof(boolean explainProof) {
        this.explainProof = explainProof;
    }

    @Override
    public synchronized InputStream run() throws N3ReasonerException {
        Preconditions.checkState(!isRunning());

        List<String> command = new ArrayList<>();
        command.add(path);
        if (!explainProof) command.add("--nope");

        addInputArguments(command);

        if (queryFile == null) {
            command.add("--pass");
        } else {
            command.add("--query");
            assert queryFile.getLang().equals(Lang.N3);
            try {
                command.add(queryFile.toURI());
            } catch (IOException e) {
                throw new N3ReasonerException("Couldn't get URI for " + queryFile, e);
            }
        }

        try {
            process = new ProcessBuilder().command(command).start();
        } catch (IOException e) {
            throw new N3ReasonerException("Problem starting EYE process.", e);
        }

        errorStream = process.getErrorStream();

        return process.getInputStream();
    }

    @Override
    public synchronized boolean isRunning() {
        return process != null && process.isAlive();
    }

    @Override
    public N3ReasonerException getError() {
        if (process == null) return null;
        if (process.exitValue() == 0) return null;

        String errorOutput = "";
        try {
            errorOutput = IOUtils.toString(errorStream);
        } catch (IOException ignored) { }
        return new N3ReasonerException(String.format("Non-zero EYE exit code=%1$d.%2$s",
                process.exitValue(),
                errorOutput != null ? "Error message:\n"+errorOutput : ""));
    }

    @Override
    public synchronized EYEProcess abort() {
        if (process == null || !process.isAlive()) return this;
        process.destroyForcibly();
        return this;
    }

    @Override
    public synchronized void waitFor() throws InterruptedException, N3ReasonerException {
        if (process != null)
            process.waitFor();
        throwIfError();
    }

    private void throwIfError() throws N3ReasonerException {
        N3ReasonerException error = getError();
        if (error != null)
            throw error;
    }

    @Override
    public synchronized boolean waitFor(long timeout, TimeUnit unit) throws InterruptedException, N3ReasonerException {
        boolean ended = process == null || process.waitFor(timeout, unit);
        if (ended)
            throwIfError();
        return ended;
    }

    private void addInputArguments(List<String> command) throws N3ReasonerException {
        for (RDFInput input : inputs) {
            if (input.getLang().equals(Lang.TURTLE))
                command.add("--turtle");
            try {
                command.add(input.toURI());
            } catch (IOException e) {
                throw new N3ReasonerException("Couldn't to get URI for " + input);
            }
        }
    }
}

package br.ufsc.lapesd.unserved.testbench.components.expressions.evaluator.subclass;

import br.ufsc.lapesd.unserved.testbench.components.expressions.ClassExpression;
import br.ufsc.lapesd.unserved.testbench.util.BFSBGPIterator;
import com.google.common.base.Preconditions;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.RDFS;

import javax.annotation.Nonnull;

class Helper {
    static boolean isSubclass(@Nonnull Resource aClass, @Nonnull ClassExpression expr) {
        Preconditions.checkArgument(aClass.getModel() != null);
        assert !expr.getNames().isEmpty();
        return expr.getNames().contains(aClass) ||
                BFSBGPIterator.from(aClass).forward(RDFS.subClassOf).toStream()
                        .anyMatch(expr.getNames()::contains);
    }
}

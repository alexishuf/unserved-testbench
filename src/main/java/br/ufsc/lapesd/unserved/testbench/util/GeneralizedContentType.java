package br.ufsc.lapesd.unserved.testbench.util;

import org.apache.commons.lang.builder.HashCodeBuilder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class GeneralizedContentType {
    public static class Parameter {
        private final String name, value;

        public Parameter(String name, String value) {
            this.name = name;
            this.value = value;
        }

        public String getName() {
            return name;
        }

        public String getValue() {
            return value;
        }

        @Override
        public boolean equals(Object o) {
            if (!(o instanceof Parameter)) return false;
            Parameter rhs = (Parameter) o;
            return ((name == null && rhs.name == null)
                    || (name != null && rhs.name != null && name.equals(rhs.name))) &&
                   ((value == null && rhs.value == null)
                    || (value != null && rhs.value != null && value.equals(rhs.value)));
        }

        @Override
        public int hashCode() {
            return new HashCodeBuilder().append(name).append(value).hashCode();
        }

        @Override
        public String toString() {
            return String.format("%s=%s", name, value);
        }
    }
    public static class Element {
        private final String type;
        private final String subtype;
        private final Parameter[] parameters;

        public Element(String type, String subtype, Parameter[] parameters) {
            this.type = type;
            this.subtype = subtype;
            this.parameters = parameters;
        }

        public boolean isRange() {
            return type.equals("*") || subtype.equals("*");
        }

        public Parameter[] getParameters() {
            return parameters;
        }

        public String getType() {
            return type;
        }

        public String getSubtype() {
            return subtype;
        }

        public String getName() {
            return getType() + "/" + getSubtype();
        }

        public String getParameter(String name) {
            for (Parameter parameter : parameters) {
                if (parameter.getName().equals(name))
                    return parameter.getValue();
            }
            return null;
        }

        @Override
        public boolean equals(Object o) {
            if (!(o instanceof Element)) return false;
            Element rhs = (Element) o;
            if (!type.equals(rhs.type) && subtype.equals(rhs.subtype)) return false;
            for (Parameter p : parameters) {
                if (!Arrays.stream(rhs.parameters).anyMatch(q -> q.equals(p))) return false;
            }
            return true;
        }

        @Override
        public int hashCode() {
            return new HashCodeBuilder().append(type).append(subtype).append(parameters).hashCode();
        }

        @Override
        public String toString() {
            return String.format("%s/%s%s", type, subtype,
                    Arrays.stream(parameters).map(p -> "; " + p.toString())
                            .reduce((l, r) -> l + r).orElse("")
            );
        }
    }

    private final Element[] elements;

    public GeneralizedContentType(Element[] elements) {
        this.elements = elements;
    }

    public Element[] getElements() {
        return elements;
    }

    public boolean isConcrete() {
        return getElements().length == 1 && !getElements()[0].isRange();
    }

    public boolean accepts(GeneralizedContentType other) {
        throw new UnsupportedOperationException("FIXME. Not implemented");
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof GeneralizedContentType)) return false;
        GeneralizedContentType rhs = (GeneralizedContentType) o;
        for (Element element : elements)
            if (!Arrays.stream(rhs.elements).anyMatch(r -> r.equals(element))) return false;
        return true;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(elements).hashCode();
    }

    @Override
    public String toString() {
        return Arrays.stream(elements).map(Element::toString)
                .reduce((l, r) -> l + ", " + r).orElse("");
    }

    /**
     * Parse a content type that is possibly multi-element, with ranges and parameters
     *
     * @param string the string
     * @return An equivalent GeneralizedContentType
     */
    public static GeneralizedContentType parse(String string) {
        String[] elementStrings = string.split(",");
        List<Element> elements = new ArrayList<>();
        for (String elementString : elementStrings) {
            String[] parts = elementStrings[0].split(";");
            Matcher matcher = Pattern.compile("(.*)/(.*)").matcher(parts[0]);
            if (!matcher.matches()) return null;
            String type = matcher.group(1);
            String subtype = matcher.group(2);

            List<String[]> splits = Arrays.stream(parts).skip(1).map(p -> p.split("="))
                    .collect(Collectors.toList());
            if (!splits.stream().allMatch(p -> p.length != 2))
                return null;

            List<Parameter> params = splits.stream()
                    .map(split -> new Parameter(split[0], split[1]))
                    .collect(Collectors.toList());
            elements.add(new Element(type, subtype, params.toArray(new Parameter[params.size()])));
        }
        return new GeneralizedContentType(elements.toArray(new Element[elements.size()]));
    }
}

package br.ufsc.lapesd.unserved.testbench.input;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFFormat;

import javax.annotation.Nonnull;
import java.io.*;
import java.nio.file.Files;

public abstract class AbstractRDFInput implements RDFInput {
    protected boolean closed = false;
    @Nonnull private final RDFFormat format;
    private File tempFile = null;
    private Model model;

    protected AbstractRDFInput(@Nonnull RDFFormat format) {
        this.format = format;
    }

    @Override
    @Nonnull
    public RDFFormat getFormat() {
        return format;
    }

    @Override
    public Model getModel() throws IOException {
        if (closed) throw new ClosedException();
        if (model != null) return model;
        model = loadModel();
        return model;
    }

    @Override
    public Model takeModel() throws IOException {
        Model model = getModel();
        this.model = null;
        close();
        return model;
    }

    @Override
    public File toFile() throws IOException {
        if (closed) throw new ClosedException();
        try (FileOutputStream out = new FileOutputStream(tempFile = createTempFile())) {
            writeData(out);
        }
        return tempFile;
    }

    protected File createTempFile() throws IOException {
        String ext = "." + format.getLang().getFileExtensions().get(0);
        File file = Files.createTempFile("unserved-testbench-rdfinput", ext).toFile();
        file.deleteOnExit();
        return file;
    }

    protected void writeData(OutputStream outputStream) throws IOException {
        RDFDataMgr.write(outputStream, getModel(), format);
    }

    @Override
    public String toURI() throws IOException {
        if (closed) throw new ClosedException();
        /* file:// seems to be more widely accepted */
        return "file://" + toFile().toURI().getRawPath();
    }

    @Override
    public InputStream createInputStream() throws IOException {
        return new FileInputStream(toFile());
    }

    @Override
    public String getBaseURI() {
        return null;
    }

    @Override
    public void close() throws IOException {
        if (closed) return;
        closed = true;
        if (tempFile != null)
            Files.deleteIfExists(tempFile.toPath());
        if (model != null && ownsLoadedModel())
            model.close();
    }

    protected abstract Model loadModel() throws IOException;

    protected boolean ownsLoadedModel() {
        return true;
    }
}

package br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.lazy;

import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.ActionsHelper;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.IndexedNode;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.Node;
import br.ufsc.lapesd.unserved.testbench.io_composer.state.MessagePairAction;
import br.ufsc.lapesd.unserved.testbench.model.Variable;
import br.ufsc.lapesd.unserved.testbench.util.MinimalSemanticDistance;
import com.google.common.base.Preconditions;
import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.SetMultimap;

import javax.annotation.Nonnull;
import java.util.*;
import java.util.function.Function;

import static br.ufsc.lapesd.unserved.testbench.util.MatchTable.Relation.*;

class AdaptedSetNode extends FullSetNode {
    private final @Nonnull BiMap<IndexedNode, IndexedNode> replacements;
    private final @Nonnull SetMultimap<Variable, Variable> fromCanonic;
    private final @Nonnull Map<Variable, Variable> toCanonic;

    public AdaptedSetNode(@Nonnull Set<IndexedNode> nodes,
                          @Nonnull Collection<MessagePairAction> messagePairs,
                          @Nonnull EquivalenceSets equivalenceSets,
                          @Nonnull BiMap<IndexedNode, IndexedNode> replacements,
                          @Nonnull SetMultimap<Variable, Variable> fromCanonic,
                          @Nonnull Map<Variable, Variable> toCanonic) {
        super(nodes, messagePairs, equivalenceSets);
        this.replacements = replacements;
        this.fromCanonic = fromCanonic;
        this.toCanonic = toCanonic;
    }

    @Nonnull
    public BiMap<IndexedNode, IndexedNode> getReplacements() {
        return replacements;
    }

    @Nonnull
    public SetMultimap<Variable, Variable> getFromCanonic() {
        return fromCanonic;
    }

    @Nonnull
    public Map<Variable, Variable> getToCanonic() {
        return toCanonic;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        AdaptedSetNode that = (AdaptedSetNode) o;

        if (!replacements.equals(that.replacements)) return false;
        if (!fromCanonic.equals(that.fromCanonic)) return false;
        return toCanonic.equals(that.toCanonic);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + replacements.hashCode();
        result = 31 * result + fromCanonic.hashCode();
        result = 31 * result + toCanonic.hashCode();
        return result;
    }

    public static class Builder {
        private final @Nonnull SetNode original;
        private final @Nonnull BiMap<IndexedNode, IndexedNode> replacements = HashBiMap.create();
        private final @Nonnull SetMultimap<Variable, Variable> fromCanonic = HashMultimap.create();
        private final @Nonnull Map<Variable, Variable> toCanonic = new HashMap<>();
        private final @Nonnull Function<IndexedNode, IndexedNode> canonicGetter;

        public Builder(@Nonnull SetNode original,
                       @Nonnull Function<IndexedNode, IndexedNode> canonicGetter) {
            this.original = original;
            this.canonicGetter = canonicGetter;
        }

        public Builder replace(IndexedNode original, IndexedNode replacement) {
            replacements.put(original, replacement);
            mapVariables(original, replacement, Node::getInputs, true);
            mapVariables(original, replacement, Node::getOutputs, false);
            return this;
        }

        private void mapVariables(IndexedNode node, IndexedNode replacement,
                                  Function<IndexedNode, Set<Variable>> getter,
                                  boolean preferCanonicalAsSuperclass) {
            IndexedNode canonic = canonicGetter.apply(node);
            Set<Variable> cVars = getter.apply(canonic);
            Set<Variable> rVars = getter.apply(replacement);
            if (rVars.isEmpty()) return;
            Preconditions.checkArgument(!cVars.isEmpty() || rVars.isEmpty());

            MinimalSemanticDistance msd = new MinimalSemanticDistance();
            if (preferCanonicalAsSuperclass)
                msd.withPreferences(SAME, SUPERCLASS, SUBCLASS);
            else
                msd.withPreferences(SAME, SUBCLASS, SUPERCLASS);

            for (Variable rIn : rVars) {
                Variable cIn = msd.select(cVars, rIn);
                toCanonic.put(rIn, cIn);
                fromCanonic.put(cIn, rIn);
            }
        }

        public AdaptedSetNode build() {
            HashSet<IndexedNode> set = new HashSet<>(original.getNodes());
            set.removeAll(replacements.keySet());
            set.addAll(replacements.values());
            Collection<MessagePairAction> actions;
            actions = ActionsHelper.getMessagePairs(replacements.values()).values();
            EquivalenceSets equivalenceSets = new EquivalenceSets(original.getEquivalenceSets());
            for (Map.Entry<Variable, Variable> e : fromCanonic.entries())
                equivalenceSets.setEquivalent(e.getKey(), e.getValue());
            for (Variable cVar : fromCanonic.keySet())
                equivalenceSets.removeVar(cVar);
            return new AdaptedSetNode(set, actions, equivalenceSets, replacements, fromCanonic,
                                      toCanonic);
        }

    }
}

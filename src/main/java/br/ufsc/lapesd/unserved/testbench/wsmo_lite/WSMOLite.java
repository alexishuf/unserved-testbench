package br.ufsc.lapesd.unserved.testbench.wsmo_lite;

import br.ufsc.lapesd.unserved.testbench.util.ResourcesBackground;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;
import org.apache.jena.riot.RDFFormat;

import java.util.Collections;

public class WSMOLite extends ResourcesBackground {
    public WSMOLite() {
        super(Collections.singletonList(ImmutablePair.of("wunserved-lite.ttl", RDFFormat.TURTLE)), true);
    }

    public static String IRI = "http://www.wsmo.org/ns/wsmo-lite";
    public static String PREFIX = IRI + "#";

    public static Resource FunctionalClassificationRoot = ResourceFactory.createResource(PREFIX + "FunctionalClassificationRoot");
    public static Resource NonfunctionalParameter = ResourceFactory.createResource(PREFIX + "NonfunctionalParameter");
    public static Resource Condition = ResourceFactory.createResource(PREFIX + "Condition");
    public static Resource Effect = ResourceFactory.createResource(PREFIX + "Effect");

    public static Property usesOntology = ResourceFactory.createProperty(PREFIX + "usesOntology");
}

package br.ufsc.lapesd.unserved.testbench.composer;

import javax.annotation.Nonnull;

/**
 * A Composition algorithm.
 */
public interface Composer extends AutoCloseable {
    @Nonnull
    Composition run() throws ComposerException;

    @Override
    void close() throws ComposerException;
}

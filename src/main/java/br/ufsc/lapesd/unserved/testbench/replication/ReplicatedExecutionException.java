package br.ufsc.lapesd.unserved.testbench.replication;

public class ReplicatedExecutionException extends Exception {
    public ReplicatedExecutionException() {
    }

    public ReplicatedExecutionException(String s) {
        super(s);
    }

    public ReplicatedExecutionException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public ReplicatedExecutionException(Throwable throwable) {
        super(throwable);
    }
}

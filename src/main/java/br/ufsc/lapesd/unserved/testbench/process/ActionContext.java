package br.ufsc.lapesd.unserved.testbench.process;


public interface ActionContext extends AutoCloseable {
    ActionRunnersContext getRunnersContext();
    Object getKey();

    default void removeFromRunnersContext() {
        getRunnersContext().removeActionContext(this);
    }

    /**
     * Removes from the runners context and release resources.
     *
     * @throws Exception if something goes wrong
     */
    @Override
    void close() throws Exception;
}

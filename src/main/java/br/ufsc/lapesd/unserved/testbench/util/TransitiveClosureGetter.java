package br.ufsc.lapesd.unserved.testbench.util;

import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;

import javax.annotation.Nonnull;
import java.util.Set;
import java.util.stream.Stream;

public interface TransitiveClosureGetter {
    /**
     * The predicate evaluated by the getter.
     */
    Property getPredicate();

    /**
     * If the closure is backward, it returns all matches for ?member in the following SPARQL BGP:
     *   <code>:start (^:predicate)* ?member</code>
     * Otherwise, a forward getter evaluates this SPARQL BGP:
     *   <code>:start (:predicate)* ?member</code>
     */
    boolean isBackward();

    /**
     * Get the set of resources in the getPredicate()-closure starting from start.
     */
    @Nonnull Set<Resource> getClosureSet(@Nonnull Resource start);
    /**
     * Same as getClosureSet, but the result never contains start.
     */
    @Nonnull Set<Resource> getStrictClosureSet(@Nonnull Resource start);

    /**
     * Get a distinct stream of resources in the getPredicate()-closure starting from start.
     */
    @Nonnull Stream<Resource> getClosureStream(@Nonnull Resource start);

    /**
     * Same as getClosureStream(), but the result never contains start.
     */
    @Nonnull Stream<Resource> getStrictClosureStream(@Nonnull Resource start);

    /**
     * Creates a {@link TransitiveClosureGetter} for the given property and backwardness using
     * the same implementation as this one.
     *
     * @throws UnsupportedOperationException: if unsupported
     */
    @Nonnull TransitiveClosureGetter createFor(Property property, boolean isBackward)
            throws UnsupportedOperationException;
}

package br.ufsc.lapesd.unserved.testbench.benchmarks;

import br.ufsc.lapesd.unserved.testbench.input.RDFInputModel;
import br.ufsc.lapesd.unserved.testbench.iri.HTTP;
import br.ufsc.lapesd.unserved.testbench.iri.Unserved;
import br.ufsc.lapesd.unserved.testbench.iri.UnservedP;
import br.ufsc.lapesd.unserved.testbench.iri.UnservedX;
import br.ufsc.lapesd.unserved.testbench.pragproof.context.PragProofRules;
import br.ufsc.lapesd.unserved.testbench.pragproof.context.impl.DefaultPragProofRulesFactory;
import br.ufsc.lapesd.unserved.testbench.util.Skolemizer;
import br.ufsc.lapesd.unserved.testbench.util.SkolemizerMap;
import com.google.common.base.Preconditions;
import org.apache.commons.io.IOUtils;
import org.apache.jena.query.*;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFFormat;
import org.apache.jena.vocabulary.OWL2;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;
import org.apache.jena.vocabulary.XSD;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;
import org.spinrdf.vocabulary.SP;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import static java.lang.String.format;

public class DescriptionsGenerator {
    static {
        Unserved.init();
        UnservedP.init();
    }

    @Option(name = "--ex-iri")
    private String exIRI = "http://example.org/#";
    @Option(name = "--sk-iri")
    private String skIRI = "http://example.org/skolem#";

    @Option(name = "--unserved-out")
    private File unservedOut;

    @Option(name = "--rules-out")
    private File rulesOut;

    @Option(name = "--merged-out")
    private File mergedOut;

    @Option(name = "--copy-rules")
    private boolean copyRules = false;

    @Option(name = "--rules-ground-children")
    private boolean rulesGroundChildren = false;

    @Option(name = "--chain-length", required = true)
    private int chainLength;

    @Option(name = "--alternatives")
    private int alternatives = 1;

    @Option(name = "--conditions")
    private int conditions = 1;

    @Option(name = "--universal")
    private boolean universal = false; //true in the original benchmark

    @Option(name = "--repeat-relations")
    private boolean repeatRelations = false; //true in the original benchmark

    @Option(name = "--with-preconditions")
    private boolean preconditions = false;

    @Option(name = "--rel")
    private String rel = "rel";

    private int nextIndex = 0;

    private int getNextIndex() {
        return nextIndex++;
    }


    private String prefixes() {
        return  "@prefix sk: <" + skIRI + ">.\n" +
                "@prefix ex: <" + exIRI + ">.\n" +
                "@prefix u: <" + Unserved.PREFIX  + ">.\n" +
                "@prefix ux-http: <" + UnservedX.HTTP.PREFIX + ">.\n" +
                "@prefix ux-rdf: <" + UnservedX.RDF.PREFIX + ">.\n" +
                "@prefix ux-mt: <" + UnservedX.MediaType.PREFIX + ">.\n" +
                "@prefix http: <" + HTTP.PREFIX + ">.\n" +
                "@prefix http-methods: <" + HTTP.Methods.PREFIX + ">.\n" +
                "@prefix xsd: <" + XSD.getURI() + ">.\n" +
                "@prefix sp: <" + SP.NS + ">.\n" +
                "@prefix owl: <" + OWL2.getURI() + ">.\n" +
                "@prefix rdfs: <" + RDFS.getURI() + ">.\n";
    }

    public static void main(String[] args) throws Exception {
        DescriptionsGenerator generator = new DescriptionsGenerator();
        new CmdLineParser(generator).parseArgument(args);
        generator.run();
    }

    public static class Builder {
        DescriptionsGenerator generator = new DescriptionsGenerator();

        public Builder(int chainLength) {
            generator.chainLength = chainLength;
        }

        public Builder setUnservedOut(File file) {
            generator.unservedOut = file;
            return this;
        }
        public Builder setRulesOut(File file) {
            generator.rulesOut = file;
            return this;
        }
        public Builder setMergedOut(File file) {
            generator.mergedOut = file;
            return this;
        }
        public Builder setCopyRules(boolean value) {
            generator.copyRules = value;
            return this;
        }
        public Builder setRulesGroundChildren(boolean value) {
            generator.rulesGroundChildren = value;
            return this;
        }
        public Builder setConditions(int value) {
            generator.conditions = value;
            return this;
        }
        public Builder setAlternatives(int value) {
            generator.alternatives = value;
            return this;
        }
        public Builder setUniversal(boolean value) {
            generator.universal = value;
            return this;
        }
        public Builder setRepeatRelations(boolean value) {
            generator.repeatRelations = value;
            return this;
        }
        public Builder setPreconditions(boolean value) {
            generator.preconditions = value;
            return this;
        }
        public Builder setRel(String rel) {
            generator.rel = rel;
            return this;
        }
        public Builder setSkIRI(String skIRI) {
            generator.skIRI = skIRI;
            return this;
        }

        public DescriptionsGenerator build() {
            DescriptionsGenerator generator = this.generator;
            this.generator = null;
            return generator;
        }
    }

    public void run() throws Exception {
        checkOptions();

        Model unserved = generateUnserved();
        if (unservedOut != null) {
            try (FileOutputStream stream = new FileOutputStream(unservedOut)) {
                RDFDataMgr.write(stream, unserved, Lang.TURTLE);
            }
        }
        PragProofRules rules = generateUnservedRules(unserved);
        assert rules.getInputs().size() == 1;
        File rulesFile = rules.getInputs().iterator().next().toFile();
        if (copyRules) {
            appendCopyRules(rulesFile, unserved);
        }
        if (rulesGroundChildren)
            rulesGroundChildren(rulesFile, unserved);
        if (rulesOut != null) {
            Files.copy(rules.getInputs().iterator().next().toFile().toPath(), rulesOut.toPath(),
                    StandardCopyOption.REPLACE_EXISTING);
        }
        if (mergedOut != null) {
            String merged = mergePrefixes(unserved, rulesFile);
            try (FileOutputStream stream = new FileOutputStream(mergedOut)) {
                IOUtils.write(merged, stream);
            }
        }
    }

    private void appendCopyRules(File rulesFile, Model unserved) throws FileNotFoundException {
        try (PrintStream stream = new PrintStream(new FileOutputStream(rulesFile, true))) {
            appendCopyRules(stream, unserved);
        }
    }

    private void appendCopyRules(PrintStream out, Model unserved) {
        try (QueryExecution execution = QueryExecutionFactory.create(
                QueryFactory.create("PREFIX sk: <" + skIRI + ">\n" +
                        "PREFIX ex: <" + exIRI + ">\n" +
                        "PREFIX u: <" + Unserved.PREFIX + ">\n" +
                        "SELECT ?src ?dst WHERE {\n" +
                        "  ?src a u:Variable.\n" +
                        "  ?dst a u:Variable.\n" +
                        "  ?src u:type ?type.\n " +
                        "  ?dst u:type ?type.\n" +
                        "  ?msg a u:Message.\n" +
                        "  ?msg u:part ?part.\n" +
                        "  ?part u:variable ?dst.\n" +
                        "}\n"), unserved)) {

            ResultSet resultSet = execution.execSelect();
            while (resultSet.hasNext()) {
                QuerySolution row = resultSet.next();
                Resource src = row.get("src").asResource(), dst = row.get("dst").asResource();
                Preconditions.checkArgument(src.isURIResource());
                Preconditions.checkArgument(dst.isURIResource());
                Preconditions.checkArgument(src.getURI().startsWith(skIRI));
                Preconditions.checkArgument(dst.getURI().startsWith(skIRI));

                out.printf("{ %1$s a ppo:Grounded. } => { %2$s a ppo:Grounded." +
                        "[ a unserved-p:Copy, unserved-p:Action;  " +
                        "unserved-p:copyFrom %1$s; " +
                        "unserved-p:copyTo %2$s ]. }.\n",
                        "<" + src.getURI() + ">", "<" + dst.getURI() + ">");
            }
        }
    }

    private void rulesGroundChildren(File rulesFile, Model unserved) throws FileNotFoundException {
        try (PrintStream stream = new PrintStream(new FileOutputStream(rulesFile, true))) {
            rulesGroundChildren(stream, unserved);
        }
    }
    private void rulesGroundChildren(PrintStream out, Model unserved) {
        StmtIterator varStmtIt = unserved.listStatements(null, RDF.type, Unserved.Variable);
        while (varStmtIt.hasNext()) {
            Resource var = varStmtIt.next().getSubject();
            try (QueryExecution execution = QueryExecutionFactory.create(
                    QueryFactory.create("PREFIX u: <" + Unserved.PREFIX + ">\n" +
                            "SELECT ?child WHERE {\n" +
                            "  <" + var.getURI() + "> u:part ?part.\n" +
                            "  ?part u:variable ?child.\n" +
                            "}\n"), unserved)) {
                List<String> children = new ArrayList<>();
                ResultSet results = execution.execSelect();
                while (results.hasNext()) {
                    Resource child = results.next().get("child").asResource();
                    Preconditions.checkArgument(child.isURIResource());
                    Preconditions.checkArgument(child.getURI().startsWith(skIRI));

                    children.add(child.getURI().substring(skIRI.length()));
                }
                if (!children.isEmpty()) {
                    out.printf("{ <%1$s> a ppo:Grounded. } => {%2$s.}.\n", var.getURI(), children
                            .stream().map(n -> "sk:" + n + " a ppo:Grounded")
                            .reduce((l, r) -> l + ". " + r).orElseThrow(RuntimeException::new));
                }
            }
        }
    }

    private String mergePrefixes(Model model, File rulesFile) throws IOException {
        final Pattern pattern;
        pattern = Pattern.compile("\\s*@prefix\\s*([\\w-\\d]*)\\s*:\\s*<(.*)>\\s*.\\s*");

        List<String> readLines, keptLines;
        try (FileInputStream input = new FileInputStream(rulesFile)) {
            readLines = IOUtils.readLines(input, Charset.forName("UTF-8"));
        }
        keptLines = new ArrayList<>(readLines.size());
        for (String line : readLines) {
            Matcher matcher = pattern.matcher(line);
            if (!matcher.matches()) {
                keptLines.add(line);
            } else {
                String uri = model.getNsPrefixURI(matcher.group(1));
                if (uri != null && !uri.equals(matcher.group(2))) {
                    System.err.printf("WARNING: mismatch for prefix %s: %s!=%s\n",
                            matcher.group(1), uri, matcher.group(2));
                }
                model.setNsPrefix(matcher.group(1), matcher.group(2));
            }
        }

        String result;
        try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
            RDFDataMgr.write(out, model, Lang.TURTLE);
            result = out.toString("UTF-8");
        }
        return Stream.concat(Stream.of(result), keptLines.stream()).reduce((l, r) -> l + "\n" + r)
                .orElseThrow(NoSuchElementException::new);
    }

    private void checkOptions() throws Exception {
        boolean hasOutput = false;
        if ((unservedOut != null && rulesOut != null)) {
            checkIsFile(unservedOut, "unservedOut");
            checkIsFile(rulesOut, "rulesOut");
            hasOutput = true;
        }
        if (mergedOut != null) {
            checkIsFile(mergedOut, "mergedOut");
            hasOutput = true;
        }
        if (!hasOutput)
            throw new Exception("Provide at least --unserved-out and --rules-out or --merged-out");
    }

    private void checkIsFile(File file, String name) throws Exception {
        if (file.exists() && !file.isFile())
            throw new Exception(name + "=" + file + " exists but is not a file.");
    }

    public PragProofRules generateUnservedRules(Model unserved) throws IOException {
        return new DefaultPragProofRulesFactory().createRules(
                RDFInputModel.notOwningModel(unserved, RDFFormat.TURTLE));
    }

    public Model generateUnserved() {
        Model model = ModelFactory.createDefaultModel();

        generateRequestResponse(model, 0, 0, 0, conditions, rel);
        nextIndex = 1;
        generateUnservedNode(model, 0, 1);

        Model skolemized = ModelFactory.createDefaultModel();
        new Skolemizer(() -> new SkolemizerMap(skIRI)).skolemizeModels(skolemized, model);
        return skolemized;
    }

    private void generateUnservedNode(Model model, int previousIndex, int pathIndex) {
        if (pathIndex  >= chainLength) return;

        int index = getNextIndex();
        generateRequestResponse(model, previousIndex, index, conditions, conditions, rel);
        if (pathIndex == chainLength-1) {
            addGoalLink(index, rel, model);
        } else {
            for (int i = 0; i < alternatives; i++)
                generateUnservedNode(model, index, pathIndex + 1);
        }
    }

    private void generateRequestResponse(Model out, int previousIndex, int index,
                                         int requestParts, int replyParts, String rel) {
        String replyBody = format(universal ? "sk:univ-%d" : "sk:reply-%d-body", index);
        String ttl  = format(prefixes() + "\n" +
                (requestParts == 0
                        ? "ex:Type-%1$d a owl:Class, rdfs:Class; rdfs:subClassOf owl:Thing.\n"
                        : ""
                ) +
                "ex:Type-Reply-%1$d a owl:Class, rdfs:Class; rdfs:subClassOf owl:Thing.\n" +
                "sk:from-%1$d a u:Variable.\n" +
                "sk:to-%1$d a u:Variable.\n" +
                "sk:request-%1$d a u:Message, ux-http:HttpRequest, http:Request;\n" +
                "  u:from sk:from-%1$d;\n" +
                "  u:to sk:to-%1$d;\n" +
                "  u:when u:spontaneous.\n" +
                "\n" +
                "%2$s a u:Variable;\n" +
                "  u:type ex:Type-Reply-%1$d;\n" +
                (preconditions ? "  u:representation u:Resource.\n"
                               : "  u:representation [\n" +
                                 "    a ux-rdf:RDF, ux-mt:MediaType;\n" +
                                 "    ux-mt:mediaTypeValue \"text/turtle\"^^xsd:string;\n" +
                                 "    ux-rdf:rdfMediaType <http://w3id.org/spar/mediatype/text/turtle>\n" +
                                 "  ].\n"
                ) +
                "sk:reply-%1$d a u:Message, ux-http:HttpResponse, http:Response;\n" +
                "  u:from sk:to-%1$d;\n" +
                "  u:to sk:from-%1$d;\n" +
                "  u:part [\n" +
                "    u:variable %2$s;\n" +
                "    u:partModifier [\n" +
                "      a ux-http:AsHttpProperty;\n" +
                "      ux-http:httpProperty http:body;\n" +
                "      ux-http:httpResource sk:reply-%1$d\n" +
                "    ]\n" +
                "  ];\n" +
                "  u:when [\n" +
                "    a u:Reaction;" +
                "    u:reactionTo sk:request-%1$d;\n" +
                "    u:reactionCardinality u:one\n" +
                "  ].\n", index, replyBody);
        RDFDataMgr.read(out, IOUtils.toInputStream(ttl), Lang.TURTLE);
        String requestBody = format(universal ? "sk:univ-%d" : "sk:request-%d-body", index);
        if (requestParts == 0) {
            ttl = format(prefixes() + "\n" +
                    "sk:request-%1$d-tgt a u:Variable;\n" +
                    "  u:type ex:Type-%1$d;\n" +
                    "  u:representation u:Resource.\n" +
                    "sk:request-%1$d http:mthd http-methods:GET;\n" +
                    "  u:part [\n" +
                    "    u:variable sk:request-%1$d-tgt;\n" +
                    "    u:partModifier [\n" +
                    "        a ux-http:AsHttpProperty;\n" +
                    "        ux-http:httpProperty http:requestURI;\n" +
                    "        ux-http:httpResource sk:request-%1$d\n" +
                    "    ]\n" +
                    "  ].\n", index);
        } else if (preconditions) {
            ttl = format(prefixes() + "\n" +
                    "%3$s a u:Variable;\n" +
                    "  u:type ex:Type-Reply-%1$d;\n" +
                    "  u:representation u:Resource.\n" +
                    "sk:request-%2$d http:mthd http-methods:POST;\n" +
                    "  http:requestURI \"http://example.org/post-%2$d\"^^xsd:anyURI;\n" +
                    "  u:part [\n" +
                    "    u:variable %3$s;\n" +
                    "    u:partModifier [\n" +
                    "        a ux-http:AsHttpProperty;\n" +
                    "        ux-http:httpProperty http:body;\n" +
                    "        ux-http:httpResource sk:request-%2$d\n" +
                    "    ]\n" +
                    "  ].\n",
            previousIndex, index, requestBody);
        } else {
            ttl = format(prefixes() + "\n" +
                    "%2$s a u:Variable;\n" +
                    "  u:type ex:Type-Reply-%1$d;\n" +
                    "  u:representation u:Resource.\n" +
                    "sk:request-%1$d-body-binding a u:Binding, u:PropertyBinding;\n" +
                    "  u:bindingResource sk:request-%1$d;\n" +
                    "  u:bindingProperty http:body;\n" +
                    "  u:bindingVariable %2$s.\n" +
                    "sk:request-%1$d http:mthd http-methods:POST;\n" +
                    "  http:requestURI \"http://example.org/post-%1$d\"^^xsd:anyURI.\n",
                    index, requestBody);
        }
        RDFDataMgr.read(out, IOUtils.toInputStream(ttl), Lang.TURTLE);
        for (int i = 0; i < requestParts; i++) {
            addPart(out, "sk:request-" + index, requestBody,
                    rel + previousIndex + "-" + i,
                    requestBody + "-i" + i,
                    "sk:request-" + index, false);
        }
        for (int i = 0; i < replyParts; i++) {
            addPart(out, replyBody, replyBody,
                    rel + index + (repeatRelations ? "" : "-" + i),
                    replyBody + "-o" + i, "sk:reply-" + index, true);
        }
    }

    private void addGoalLink(int index, String rel, Model model) {
        String body = format(universal ? "sk:univ-%d" : "sk:reply-%d-body", index);
        addPart(model, body, body, rel+"Goal", body + "-oGoal",
                "sk:reply-" + index, true);
    }

    private void addPart(Model out, String partOf, String propertyOf, String relLocalName,
                         String partName, String conditionOf, boolean isReaction) {
        String ttl = format(prefixes()  + "\n" +
                        "ex:Type-%3$s a owl:Class, rdfs:Class; rdfs:subClassOf owl:Thing.\n" +
                        "%1$s u:part [\n" +
                        "  u:variable %4$s;\n" +
                        "  u:partModifier [\n" +
                        "    a ux-rdf:AsProperty;\n" +
                        "    ux-rdf:property ex:%3$s;\n" +
                        "    ux-rdf:propertyOf %2$s\n" +
                        "  ]\n" +
                        "].\n" +
                        "%4$s a u:Variable;\n" +
                        "  u:representation u:Resource;\n" +
                        "  u:type ex:Type-%3$s.\n",
                partOf, propertyOf, relLocalName, partName);
        if (preconditions && !isReaction) {
            ttl += format("%1$s u:condition ([\n" +
                          "  sp:subject %2$s;\n" +
                          "  sp:predicate ex:%3$s;\n" +
                          "  sp:object %4$s\n" +
                          "]).\n",
                    conditionOf, propertyOf, relLocalName, partName);
        }
        RDFDataMgr.read(out, IOUtils.toInputStream(ttl), Lang.TURTLE);
    }
}

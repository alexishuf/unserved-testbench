package br.ufsc.lapesd.unserved.testbench;

import br.ufsc.lapesd.unserved.testbench.hydra.Hydra;
import br.ufsc.lapesd.unserved.testbench.input.RDFInput;
import br.ufsc.lapesd.unserved.testbench.input.RDFInputFile;
import br.ufsc.lapesd.unserved.testbench.iri.Unserved;
import br.ufsc.lapesd.unserved.testbench.reasoner.N3Reasoner;
import br.ufsc.lapesd.unserved.testbench.reasoner.N3ReasonerException;
import br.ufsc.lapesd.unserved.testbench.reasoner.N3ReasonerFactory;
import br.ufsc.lapesd.unserved.testbench.reasoner.impl.EYEProcessFactory;
import br.ufsc.lapesd.unserved.testbench.reasoner.impl.RiotEnhancedN3ReasonerFactory;
import org.apache.commons.io.IOUtils;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFFormat;
import org.apache.jena.riot.RDFLanguages;
import org.apache.jena.riot.RDFWriterRegistry;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;

import java.io.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Hello world!
 *
 */
public class App  {
    @Option(name = "--output", required = false, aliases = {"-o"},
            usage = "Print the output of the uNSERVED tool to the given file. The file will be " +
                    "truncated unless --output-append is also provided")
    private File output = null;

    @Option(name = "--output-append", required = false, depends = {"--output"},
            usage = "Instead of truncating the file used on --output, append to it.")
    private boolean outputAppend = false;

    @Option(name = "--input", aliases = {"-i"}, required = false,
            usage = "Load an RDF file using any syntax supported by jena. Multiple input files " +
                    "may be given.")
    private File[] inputs = {};

    @Option(name = "--query", required = false, usage = "N3 file with a query to be run against " +
            "the reasoner, if not given, the reasoner computes the deductive closure")
    private File queryFile;

    @Option(name = "--explain", required = false, usage = "If true, the reasoner outputs an " +
            "explained proof, in the N3 proof vocabulary, of the given query.")
    private boolean explainProof = false;

    @Option(name = "--hydra", required = false,
            usage = "Load Hydra vocabulary and associated unserved rules")
    private boolean loadHydra = false;

    @Option(name = "--eye-path", required = false,
            usage = "Use EYE reasoner using the given executable path")
    private String eyePath = null;

    private static final Pattern suffixPattern = Pattern.compile("\\.([^.]+)$");

    public File getOutput() {
        return output;
    }

    public void setOutput(File output) {
        this.output = output;
    }

    public boolean getOutputAppend() {
        return outputAppend;
    }

    public void setOutputAppend(boolean outputAppend) {
        this.outputAppend = outputAppend;
    }

    public File[] getInputs() {
        return inputs;
    }

    public void setInputs(File[] inputs) {
        this.inputs = inputs;
    }

    public File getQueryFile() {
        return queryFile;
    }

    public void setQueryFile(File queryFile) {
        this.queryFile = queryFile;
    }

    public boolean getExplainProof() {
        return explainProof;
    }

    public void setExplainProof(boolean explainProof) {
        this.explainProof = explainProof;
    }

    public boolean getLoadHydra() {
        return loadHydra;
    }

    public void setLoadHydra(boolean hydra) {
        this.loadHydra = hydra;
    }

    public String getEyePath() {
        return eyePath;
    }

    public void setEyePath(String eyePath) {
        this.eyePath = eyePath;
    }

    public static void main(String[] args ) {
        App app = new App();
        CmdLineParser parser = new CmdLineParser(app);
        try {
            parser.parseArgument(args);
        } catch (CmdLineException e) {
            e.printStackTrace();
        }

        try {
            System.exit(app.run());
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    private List<RDFInput> getAllInputs() throws IOException {
        List<RDFInput> list = new ArrayList<>();
        for (File file : inputs) {
            Lang lang = RDFLanguages.filenameToLang(file.getName());
            if (lang == null) {
                System.err.printf("Could not guess RDF syntax of %1$s from filename extension. " +
                        "Ignoring the file.\n", file.getName());
            }
            list.add(new RDFInputFile(file, RDFWriterRegistry.defaultSerialization(lang)));
        }
        return list;
    }

    private Collection<Background> getAllBackgrounds() {
        LinkedHashSet<Background> set = new LinkedHashSet<>();
        set.add(new Unserved());
        if (getLoadHydra()) {
            set.add(new Hydra());
        }
        return set;
    }

    private N3ReasonerFactory getReasonerFactory() throws N3ReasonerException {
        if (eyePath != null) {
            return new RiotEnhancedN3ReasonerFactory(new EYEProcessFactory(eyePath));
        }
        return new RiotEnhancedN3ReasonerFactory(new EYEProcessFactory());
    }

    public int run() throws N3ReasonerException, IOException {
        N3Reasoner reasoner = getReasonerFactory().createReasoner();
        List<RDFInput> inputs = getAllInputs();
        Collection<Background> backgrounds = getAllBackgrounds();
        backgrounds.stream().map(Background::getInputs).forEachOrdered(l -> l.forEach(inputs::add));
        try {
            return run(reasoner, inputs);
        } finally {
            for (Background bg : backgrounds) {
                try {
                    bg.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private int run(N3Reasoner reasoner, List<RDFInput> inputs)
            throws IOException, N3ReasonerException {
        if (!setupReasoner(reasoner, inputs)) return 1;

        InputStream result = reasoner.run();

        OutputStream outputStream = System.out;
        try {
            if (output != null)
                outputStream = new FileOutputStream(output, outputAppend);
            IOUtils.copy(result, outputStream);
        } catch (IOException e) {
            System.err.printf("Problem writing output data: %1$s\n", e.getMessage());
            return 1;
        }

        try {
            reasoner.waitFor();
            return 0;
        } catch (N3ReasonerException e) {
            System.err.printf("Error running reasoner: %1$s\n", e.getMessage());
            return 1;
        } catch (InterruptedException e) {
            e.printStackTrace();
            return 1;
        }
    }

    private boolean setupReasoner(N3Reasoner reasoner, List<RDFInput> inputs) throws IOException {
        if (!checkInputFormats(reasoner, inputs)) return false;
        for (RDFInput input : inputs) reasoner.addInput(input);
        if (queryFile != null)
            reasoner.setQueryFile(new RDFInputFile(queryFile, RDFInput.N3));
        else
            reasoner.queryDeductiveClosure();
        reasoner.setExplainProof(explainProof);
        return true;
    }

    private boolean checkInputFormats(N3Reasoner reasoner, List<RDFInput> inputs) {
        String badFormats = inputs.stream().map(RDFInput::getFormat)
                .filter(f -> !reasoner.supportedInputLanguages().contains(f))
                .map(RDFFormat::toString).reduce((l, r) -> l + ", " + r).orElse(null);
        if (badFormats != null) {
            System.err.printf("Selected N3 reasoner does not support the following RDF " +
                    "formats: %1$s.\n", badFormats);
        }
        return badFormats == null;
    }


}

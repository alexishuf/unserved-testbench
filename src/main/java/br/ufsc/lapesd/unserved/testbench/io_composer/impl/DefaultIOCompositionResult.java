package br.ufsc.lapesd.unserved.testbench.io_composer.impl;

import br.ufsc.lapesd.unserved.testbench.io_composer.IOComposerTimes;
import br.ufsc.lapesd.unserved.testbench.io_composer.IOCompositionResult;
import org.apache.jena.rdf.model.Resource;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.NoSuchElementException;

public class DefaultIOCompositionResult implements IOCompositionResult {
    @Nonnull private final IOComposerInput composerInput;
    @Nonnull private final IOComposerTimes times;

    public DefaultIOCompositionResult(@Nonnull IOComposerInput composerInput,
                                      @Nonnull IOComposerTimes times) {
        this.composerInput = composerInput;
        this.times = times;
    }

    @Override
    public Collection<Resource> getInputWantedVariables() {
        return composerInput.getWanted();
    }

    @Override
    public Resource getWantedVariable(Resource inputResource) {
        Resource resource = composerInput.getSkolemizerMap().resolve(inputResource);
        if (resource == null) {
            throw new NoSuchElementException(inputResource.toString()
                    + " is not known as an input wanted variable");
        }
        if (resource.isAnon())
            return composerInput.getSkolemizedUnion().createResource(resource.getId());
        else if (resource.isURIResource())
            return composerInput.getSkolemizedUnion().createResource(resource.getURI());
        return null;
    }

    @Override
    @Nonnull
    public IOComposerTimes getTimes() {
        return times;
    }

    @Override
    public void close() {
        composerInput.close();
    }
}

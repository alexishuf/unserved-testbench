package br.ufsc.lapesd.unserved.testbench.benchmarks.pragproof_design;

import br.ufsc.lapesd.unserved.testbench.Algorithm;
import br.ufsc.lapesd.unserved.testbench.benchmarks.compose.TimesExperimentResult;
import br.ufsc.lapesd.unserved.testbench.benchmarks.experiment.AbstractExperimentDesignRunner;
import br.ufsc.lapesd.unserved.testbench.benchmarks.experiment.ExperimentResult;
import br.ufsc.lapesd.unserved.testbench.benchmarks.experiment.Factors;
import br.ufsc.lapesd.unserved.testbench.restdesc_pragproof.RESTdescPragProof;
import org.kohsuke.args4j.CmdLineParser;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static br.ufsc.lapesd.unserved.testbench.io_composer.layered.LayeredServiceSetGraphIOComposer.*;

public class PPRawExperimentDesignRunner extends AbstractExperimentDesignRunner {
    public PPRawExperimentDesignRunner() {
        super(new PPExperimentFactory());
    }

    public static void main(String[] args) throws Exception {
        PPRawExperimentDesignRunner app = new PPRawExperimentDesignRunner();
        new CmdLineParser(app).parseArgument(args);
        app.run();
    }

    @SuppressWarnings("SimplifiableIfStatement")
    @Override
    protected Factors createFactors(Map<String, String> factors) {
        int chainLength = Integer.parseInt(factors.get("chainLength"));
        Algorithm algorithm = Algorithm.valueOf(factors.get("algorithm"));
        boolean execute, preconditions;
        if (factors.get("execute").trim().compareToIgnoreCase("true") == 0)
            execute = true;
        else if (factors.get("execute").trim().compareToIgnoreCase("false") == 0)
            execute = false;
        else
            execute = Integer.parseInt(factors.get("execute")) > 0;

        if (factors.get("preconditions").trim().compareToIgnoreCase("true") == 0)
            preconditions = true;
        else if (factors.get("preconditions").trim().compareToIgnoreCase("false") == 0)
            preconditions = false;
        else
            preconditions = Integer.parseInt(factors.get("preconditions")) > 0;

        return new PPFactors.Builder(chainLength, algorithm)
                .withAlternatives(Integer.parseInt(factors.get("alternatives")))
                .withIoCount(Integer.parseInt(factors.get("ioCount")))
                .withPreheatCount(Integer.parseInt(factors.get("preheatCount")))
                .withPreconditions(preconditions)
                .withExecute(execute)
                .withNonDummyChain(Integer.parseInt(factors.get("nonDummyChain")))
                .withNonDummyIOCount(Integer.parseInt(factors.get("nonDummyIOCount")))
                .withNonDummyAlternatives(Integer.parseInt(factors.get("nonDummyAlternatives")))
                .withChildXmx(factors.getOrDefault("childXmx", null))
                .withChildXms(factors.getOrDefault("childXms", null))
                .withChildXss(factors.getOrDefault("childXss", null))
                .withChildJVMOpts(factors.getOrDefault("childJVMOpts", null))
                .build();
    }

    @Override
    protected List<String> getResultHeaders() {
        return Arrays.asList("initialization", "reasonerSpawnAvg", "reasonerSpawnStdDev",
                "reasonerSpawnSum", "reasonerSpawnAndParseAvg", "reasonerSpawnAndParseStdDev",
                "reasonerSpawnAndParseSum", "graphBuilderSetup", "graphConstruction", "graphOptimization",
                "output", "composition", "reasoning", "firstReplan", "blacklisting", "execution", "total");
    }

    @Override
    protected List<String> getResultValues(ExperimentResult result) {
        TimesExperimentResult r = (TimesExperimentResult) result;
        List<Double> times = r.getCompositionTimes();
        double reasoning = r.getTotalOrElse(RESTdescPragProof.n3ReasoningTime, 0.0);
        double second = times.size() > 1 ? times.get(1) : 0.0;
        double setup = r.getTotalOrElse(graphBuilderSetupTime, 0);
        double construction = r.getTotalOrElse(graphConstructionTime, 0);
        double optimization = r.getTotalOrElse(graphOptimizationTime, 0);
        double sumSpawn = r.getReasonerSpawnTimes().stream().reduce(Double::sum).orElse(0.0);
        double avgSpawn = sumSpawn / r.getReasonerSpawnTimes().size();
        double stdDevSpawn = Math.sqrt(r.getReasonerSpawnTimes().stream()
                .map(t -> Math.pow(t - avgSpawn, 2)).reduce(Double::sum).orElse(0.0)
                / (double)(r.getReasonerSpawnTimes().size()-1));

        times = r.getTimesOrEmpty(RESTdescPragProof.n3ReasonerSpawnAndParseTime);
        double sumSpawnAndParse = times.stream().reduce(Double::sum).orElse(0.0);
        double avgSpawnAndParse = sumSpawnAndParse / times.size();
        double stdDevSpawnAndParse = Math.sqrt(times.stream().map(t -> Math.pow(t - avgSpawn, 2))
                .reduce(Double::sum).orElse(0.0) / (double)(times.size()-1));



        return Stream.of(r.getInitialization(), avgSpawn, stdDevSpawn, sumSpawn,
                avgSpawnAndParse, stdDevSpawnAndParse, sumSpawnAndParse, setup,
                construction, optimization, r.getOutput(), r.getComposition(), reasoning, second,
                r.getBlacklisting(), r.getExecution(), r.getTotal()
        ).map(String::valueOf).collect(Collectors.toList());
    }
}

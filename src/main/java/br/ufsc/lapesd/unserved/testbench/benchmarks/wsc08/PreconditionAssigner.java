package br.ufsc.lapesd.unserved.testbench.benchmarks.wsc08;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.IOException;

public interface PreconditionAssigner {
    void addTo(@Nonnull File convertedRoot) throws IOException;
}

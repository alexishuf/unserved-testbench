package br.ufsc.lapesd.unserved.testbench.benchmarks.design1.data;

import br.ufsc.lapesd.unserved.testbench.benchmarks.design1.D1Factors;
import br.ufsc.lapesd.unserved.testbench.iri.HTTP;
import br.ufsc.lapesd.unserved.testbench.iri.Testbench;
import br.ufsc.lapesd.unserved.testbench.iri.Unserved;
import br.ufsc.lapesd.unserved.testbench.iri.UnservedX;
import com.google.common.base.Preconditions;
import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.rdf.model.*;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;

import java.io.*;
import java.util.Collection;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import static br.ufsc.lapesd.unserved.testbench.iri.Unserved.*;

public class DescriptionsData implements Serializable {
    private File knownFile, wantedFile, descriptionsFile, taxonomyFile;
    private static final String TAXONOMY_PREFIX = Testbench.IRI + "/design1/taxonomy#";
    private static final String INSTANCES_PREFIX = Testbench.IRI + "/design1/example#";
    private static final String SK_PREFIX = Testbench.IRI + "/design1/sk#";
    private static final Property genericMember =
            ResourceFactory.createProperty(INSTANCES_PREFIX, "part");

    public static DescriptionsData create(D1Factors factors, ServicesIOs ios,
                                          File dir, String id) throws IOException {
        if ((!dir.exists() && !dir.mkdirs()) || !dir.isDirectory())
            throw new IOException("Failed to mkdir " + dir);
        DescriptionsData d = new DescriptionsData();
        d.knownFile = createNewFile(dir, id, "known", "ttl");
        d.taxonomyFile = createNewFile(dir, id, "taxonomy", "ttl");
        switch (factors.getAlgorithm().getFamily()) {
            case RESTdescPP:
                d.wantedFile = createNewFile(dir, id, "wanted", "n3");
                d.descriptionsFile = createNewFile(dir, id, "descriptions", "n3");
                writeRESTdescKnown(d.knownFile, ios);
                writeRESTdescWanted(d.wantedFile, factors, ios);
                writeRESTdescDescriptions(d.descriptionsFile, ios);
                break;
            case IOGraphPath:
                d.wantedFile = createNewFile(dir, id, "wanted", "ttl");
                d.descriptionsFile = createNewFile(dir, id, "descriptions", "ttl");
                writeUnservedKnown(d.knownFile, ios);
                writeUnservedWanted(d.wantedFile, factors, ios);
                writeUnservedDescriptions(d.descriptionsFile, ios);
                break;
            default:
                throw new IllegalArgumentException("Unexpected AlgorithmFamily");
        }
        writeTaxonomy(d.taxonomyFile, ios);
        return d;
    }

    private static void writeUnservedDescriptions(File file, ServicesIOs ios) throws IOException {
        Model m = ModelFactory.createDefaultModel();
        m.setNsPrefix("tax", TAXONOMY_PREFIX);
        m.setNsPrefix("ex", INSTANCES_PREFIX);
        m.setNsPrefix("sk", SK_PREFIX);
        m.setNsPrefix("u", Unserved.PREFIX);
        m.setNsPrefix("ux-rdf", UnservedX.RDF.PREFIX);
        m.setNsPrefix("ux-http", UnservedX.HTTP.PREFIX);
        m.setNsPrefix("ux-mt", UnservedX.MediaType.PREFIX);

        ios.getDescriptions().forEach(des -> {
            Resource from = m.createResource(Variable),
                    to = m.createResource(Variable);
            Resource req = m.createResource(Message)
                    .addProperty(RDF.type, UnservedX.HTTP.HttpRequest)
                    .addProperty(RDF.type, HTTP.Request)
                    .addProperty(Unserved.from, from).addProperty(Unserved.to, to)
                    .addProperty(when, spontaneous)
                    .addProperty(HTTP.mthd, HTTP.Methods.POST)
                    .addProperty(HTTP.requestURI, ResourceFactory.createTypedLiteral("http://example.org/post-" + des.getService().getId(), XSDDatatype.XSDanyURI));
            Resource reqBody = m.createResource(Variable)
                    .addProperty(type, m.createResource(RDFS.Class))
                    .addProperty(representation, Resource);
            m.createResource(Binding).addProperty(RDF.type, PropertyBinding)
                    .addProperty(bindingResource, req).addProperty(bindingVariable, reqBody)
                    .addProperty(bindingProperty, HTTP.body);
            Resource res = m.createResource(Message)
                    .addProperty(RDF.type, UnservedX.HTTP.HttpResponse)
                    .addProperty(RDF.type, HTTP.Response)
                    .addProperty(Unserved.from, to).addProperty(Unserved.to, from)
                    .addProperty(when, m.createResource(Reaction)
                            .addProperty(reactionTo, req)
                            .addProperty(reactionCardinality, one));
            Resource resBody = m.createResource(Variable)
                    .addProperty(type, m.createResource(RDFS.Class))
                    .addProperty(representation, m.createResource(UnservedX.MediaType.MediaType)
                            .addProperty(UnservedX.MediaType.mediaTypeValue, ResourceFactory.createTypedLiteral("text/turtle", XSDDatatype.XSDstring))
                            .addProperty(UnservedX.RDF.rdfMediaType, r("http://w3id.org/spar/mediatype/text/turtle")));
            res.addProperty(part, m.createResource(Part)
                    .addProperty(variable, resBody)
                    .addProperty(partModifier, m.createResource(UnservedX.HTTP.AsHttpProperty)
                            .addProperty(UnservedX.HTTP.httpProperty, HTTP.body)
                            .addProperty(UnservedX.HTTP.httpResource, res)));

            des.getInputs().forEach(typeId -> req.addProperty(part, m.createResource(Part)
                    .addProperty(variable, m.createResource(Variable)
                            .addProperty(representation, Resource)
                            .addProperty(type, m.createResource(TAXONOMY_PREFIX + "Class" + typeId)
                                    .addProperty(RDF.type, RDFS.Class)))
                    .addProperty(partModifier, m.createResource(Part)
                            .addProperty(RDF.type, UnservedX.RDF.AsProperty)
                            .addProperty(UnservedX.RDF.property, genericMember)
                            .addProperty(UnservedX.RDF.propertyOf, reqBody))));
            addParts(des.getInputs(), m, req, reqBody);
            addParts(des.getOutputs(), m, resBody, resBody);
        });

        try (FileOutputStream out = new FileOutputStream(file)) {
            RDFDataMgr.write(out, m, Lang.TURTLE);
        }
    }

    private static void addParts(Collection<Integer> typeIds, Model m, Resource partOf, Resource propertyOf) {
        typeIds.forEach(typeId -> partOf.addProperty(part, m.createResource(Part)
                .addProperty(variable, m.createResource(Variable)
                        .addProperty(representation, Resource)
                        .addProperty(type, m.createResource(TAXONOMY_PREFIX + "Class" + typeId)
                                .addProperty(RDF.type, RDFS.Class)))
                .addProperty(partModifier, m.createResource(Part)
                        .addProperty(RDF.type, UnservedX.RDF.AsProperty)
                        .addProperty(UnservedX.RDF.property, genericMember)
                        .addProperty(UnservedX.RDF.propertyOf, propertyOf))));
    }

    private static Resource r(String uri) {
        return ResourceFactory.createResource(uri);
    }
    private static Resource r(String ns, String localName) {
        return r(ns + localName);
    }

    private static void writeUnservedWanted(File file, D1Factors factors, ServicesIOs ios)
            throws IOException {
        try (FileOutputStream fileOut = new FileOutputStream(file);
             PrintStream out = new PrintStream(fileOut)) {
            out.printf("@prefix tax: <%s>.\n", TAXONOMY_PREFIX);
            out.printf("@prefix u: <%s>.\n", Unserved.PREFIX);
            out.printf("@prefix ex: <%s>.\n\n", INSTANCES_PREFIX);
            int sequences = factors.getSolutionParallelSequences();
            out.println(ios.getOutputClassIds(sequences, factors.getSolutionLength()).stream().
                    map(id -> String.format("ex:wanted%1$d a u:Variable;\n" +
                            "  u:representation u:Resource;\n" +
                            "  u:type tax:Class%1$d.\n", id))
                    .reduce((a, b) -> a + "\n" + b).orElse(""));
        }
    }

    private static void writeUnservedKnown(File file, ServicesIOs ios) throws IOException {
        try (FileOutputStream fileOut = new FileOutputStream(file);
             PrintStream out = new PrintStream(fileOut)) {
            out.printf("@prefix tax: <%s>.\n", TAXONOMY_PREFIX);
            out.printf("@prefix u: <%s>.\n", Unserved.PREFIX);
            out.printf("@prefix ex: <%s>.\n\n", INSTANCES_PREFIX);
            out.println(ios.getInputsClassIds().stream().map(id ->
                    String.format("ex:in%1$dRes a tax:Class%1$d.\n" +
                            "ex:in%1$d a u:Variable, u:ValueBoundVariable, u:ResourceBoundVariable;\n" +
                            "  u:type tax:Class%1$d;\n" +
                            "  u:representation u:Resource;\n" +
                            "  u:resourceValue ex:in%1$dRes.", id))
                    .reduce((a, b) -> a + "\n" + b).orElse(""));
        }
    }

    private static void writeRESTdescDescriptions(File file, ServicesIOs ios) throws IOException {
        try (FileOutputStream fileOut = new FileOutputStream(file);
             PrintStream out = new PrintStream(fileOut)) {
            out.printf("@prefix tax: <%s>.\n", TAXONOMY_PREFIX);
            out.printf("@prefix ex: <%s>.\n\n", INSTANCES_PREFIX);
            ios.getDescriptions().forEach(d -> {
                Function<Stream<Integer>, String> f =
                        s -> s.map(i -> String.format("?x a tax:Class%d.", i))
                                .reduce((a, b) -> a + " " + b).orElse("");
                String pre = f.apply(d.getInputs().stream());
                String post = f.apply(d.getOutputs().stream());
                out.printf("{%s} => {%s}.\n", pre, post);
            });
        }
    }

    private static void writeRESTdescWanted(File file, D1Factors f, ServicesIOs ios)
            throws IOException {
        Preconditions.checkArgument(ios.getInputsClassIds().size() == 1,
                "Only a single input is supported for RESTdesc");
        try (FileOutputStream fileOut = new FileOutputStream(file);
             PrintStream out = new PrintStream(fileOut)) {
            int sequences = f.getSolutionParallelSequences();
            out.printf("@prefix tax: <%s>.\n", TAXONOMY_PREFIX);
            out.printf("@prefix ex: <%s>.\n\n", INSTANCES_PREFIX);
            String content = ios.getOutputClassIds(sequences, f.getSolutionLength()).stream()
                    .map(id -> String.format("ex:in a tax:Class%1$d.", id))
                    .reduce((a, b) -> a + " " + b).orElse("");
            out.printf("{%1$s} => {%1$s}.", content);
        }
    }

    private static void writeRESTdescKnown(File file, ServicesIOs ios) throws IOException {
        Model model = ModelFactory.createDefaultModel();
        ios.getInputsClassIds().forEach(id -> model.createResource(INSTANCES_PREFIX + "in")
                .addProperty(RDF.type, model.createResource(TAXONOMY_PREFIX + "Class" + id)));
        writeAndClose(file, model);
    }

    private static void writeTaxonomy(File file, ServicesIOs ios) throws IOException {
        Model model = ModelFactory.createDefaultModel();
        ios.getClassIds().forEach(i -> model.createResource(TAXONOMY_PREFIX + "Class" + i)
                .addProperty(RDF.type, RDFS.Class));
        writeAndClose(file, model);
    }

    private static void writeAndClose(File file, Model model) throws IOException {
        try (FileOutputStream out = new FileOutputStream(file)) {
            RDFDataMgr.write(out, model, Lang.TURTLE);
        } finally {
            model.close();
        }
    }

    private static File createNewFile(File dir, String id, String role,
                                      String extension) throws IOException {
        File file = new File(dir, id + "-" + role + "." + extension);
        if (file.exists())
            throw new IOException("File " + file + " already exists");
        return file;
    }

    private static File getFileByIdAndRole(File dir, String id, String role) {
        File[] array = dir.listFiles(file -> Pattern.compile(id + "-" + role + ".*")
                .matcher(file.getName()).matches());
        return array == null ? null : (array.length != 1 ? null : array[0]);
    }

    public static DescriptionsData load(File dir, String id) throws IOException {
        DescriptionsData d = new DescriptionsData();
        d.knownFile = getFileByIdAndRole(dir, id, "known");
        d.wantedFile = getFileByIdAndRole(dir, id, "wanted");
        d.descriptionsFile = getFileByIdAndRole(dir, id, "descriptions");
        d.taxonomyFile = getFileByIdAndRole(dir, id, "taxonomy");

        boolean ok = d.knownFile != null && d.knownFile.exists() && d.knownFile.canRead()
                && d.wantedFile != null && d.wantedFile.exists() && d.wantedFile.canRead()
                && d.descriptionsFile != null && d.descriptionsFile.exists() && d.descriptionsFile.canRead()
                && d.taxonomyFile != null && d.taxonomyFile.exists() && d.taxonomyFile.canRead();
        if (!ok) throw new IOException("Could not find/read some of the files");
        return d;
    }

    /**
     * Get a file with the composition inputs. A set of uNSERVED bound variables or of general
     * RDF triples (for RESTdesc).
     *
     * @return A File instance. Do not write to the file or delete it.
     */
    public File getKnownFile() {
        return knownFile;
    }

    /**
     * Get a file with the composition desired outputs. The format depends on the algorithm factor.
     * For uNSERVED it is a set of unbound variables, for RESTdesc, it is a filter rule N3 file.
     *
     * @return A File instance. Do not write to the file or delete it.
     */
    public File getWantedFile() {
        return wantedFile;
    }

    /**
     * Get the service descriptions. The format is so that it can be forwarded to the
     * composition algorithm.
     *
     * @return A File instance. Do not write to the file or delete it.
     */
    public File getDescriptionsFile() {
        return descriptionsFile;
    }

    /**
     * Get the ontology with all classes used to document services IOs.
     *
     * @return A File instance. Do not write to the file or delete it.
     */
    public File getTaxonomyFile() {
        return taxonomyFile;
    }
}

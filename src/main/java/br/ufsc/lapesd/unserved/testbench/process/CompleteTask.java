package br.ufsc.lapesd.unserved.testbench.process;

import br.ufsc.lapesd.unserved.testbench.process.actions.ActionExecutionException;

import javax.annotation.Nonnull;
import java.util.concurrent.TimeUnit;

public class CompleteTask implements Interpreter.Task {
    private final ActionExecutionException exception;

    public CompleteTask() {
        exception = null;
    }
    public CompleteTask(@Nonnull ActionExecutionException exception) {
        this.exception = exception;
    }

    @Override
    public Interpreter.Task cancel() {
        return this;
    }

    @Override
    public Interpreter.Task abort() {
        return this;
    }

    @Override
    public boolean isDone() throws ActionExecutionException {
        if (exception != null) throw exception;
        return true;
    }

    @Override
    public void waitForDone() throws ActionExecutionException {
        if (exception != null) throw exception;
    }

    @Override
    public void waitForDone(long count, TimeUnit timeUnit) throws ActionExecutionException {
        if (exception != null) throw exception;
    }
}

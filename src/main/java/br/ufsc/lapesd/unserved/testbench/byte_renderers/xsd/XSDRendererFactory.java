package br.ufsc.lapesd.unserved.testbench.byte_renderers.xsd;

import br.ufsc.lapesd.unserved.testbench.byte_renderers.Renderer;
import br.ufsc.lapesd.unserved.testbench.byte_renderers.RendererFactory;
import br.ufsc.lapesd.unserved.testbench.byte_renderers.SimpleRendererFactory;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;
import org.apache.jena.vocabulary.XSD;

import javax.annotation.Nonnull;
import java.util.Collections;

public class XSDRendererFactory extends SimpleRendererFactory {
    public XSDRendererFactory(
            @Nonnull Class<? extends Renderer> implementation,
            @Nonnull Class<? extends RendererFactory> factory,
            @Nonnull String uri) {
        super(implementation, factory, Collections.singletonList(getResource(uri)),
                Collections.singletonList(getResource(uri)));
    }

    private static Resource getResource(String uri) {
        Model model = ModelFactory.createDefaultModel();
        model.setNsPrefix("xsd", XSD.getURI());
        Resource resource = model.createResource(uri);
        model.add(resource, RDF.type, RDFS.Datatype);
        return resource;
    }
}

package br.ufsc.lapesd.unserved.testbench.util.jena;

import br.ufsc.lapesd.unserved.testbench.util.CloseableIterator;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.function.Function;

/**
 * Utility class to transform and consume a query execution and a ResultSet
 */
public class ResultSetIterator<E> implements CloseableIterator<E> {
    private final QueryExecution execution;
    private final ResultSet resultSet;
    private final Function<QuerySolution, E> transform;

    public ResultSetIterator(@Nonnull ResultSet resultSet,
                             @Nonnull java.util.function.Function<QuerySolution, E> transform,
                             @Nullable QueryExecution execution) {
        this.execution = execution;
        this.resultSet = resultSet;
        this.transform = transform;
    }

    @Override
    public boolean hasNext() {
        return resultSet.hasNext();
    }

    @Override
    public E next() {
        return transform.apply(resultSet.next());
    }

    @Override
    public void close() throws Exception {
        if (execution != null)
            execution.close();
    }
}

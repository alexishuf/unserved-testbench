package br.ufsc.lapesd.unserved.testbench.model.http;

import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;

public interface AsHttpProperty extends Resource {
    Property getHttpProperty();
    Resource getHttpResource();
    Resource getModifier();
}

package br.ufsc.lapesd.unserved.testbench.pragproof;

import br.ufsc.lapesd.unserved.testbench.input.RDFInput;
import br.ufsc.lapesd.unserved.testbench.input.RDFInputModel;
import br.ufsc.lapesd.unserved.testbench.input.RDFInputStream;
import br.ufsc.lapesd.unserved.testbench.input.RDFInputString;
import br.ufsc.lapesd.unserved.testbench.iri.UnservedP;
import br.ufsc.lapesd.unserved.testbench.pragproof.context.PragProofContext;
import br.ufsc.lapesd.unserved.testbench.pragproof.model.PPPA;
import br.ufsc.lapesd.unserved.testbench.process.model.Action;
import br.ufsc.lapesd.unserved.testbench.process.model.Sequence;
import br.ufsc.lapesd.unserved.testbench.reasoner.N3Reasoner;
import br.ufsc.lapesd.unserved.testbench.reasoner.N3ReasonerFactory;
import br.ufsc.lapesd.unserved.testbench.util.jena.UncloseableGraph;
import org.apache.jena.graph.compose.Union;
import org.apache.jena.rdf.model.*;
import org.apache.jena.riot.RDFFormat;
import org.apache.jena.vocabulary.RDF;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class ProofActions {
    private Model model;
    private Model proofModel;
    private PragProofContext proofContext = null;
    private boolean ownsProofContext = false;
    private List<Resource> interactions;

    public ProofActions(Model model, Model proofModel, List<Resource> interactions,
                        PragProofContext proofContext) {
        this.model = model;
        this.proofModel = proofModel;
        this.interactions = interactions;
        this.proofContext = proofContext;
    }
    private ProofActions(ProofActions other) {
        this.model = other.model;
        this.proofModel = other.proofModel;
        this.interactions = other.interactions;
        this.proofContext = other.proofContext;
    }

    public static ProofActions extract(String proof, N3ReasonerFactory factory,
                                       PragProofContext proofContext) throws Exception {
        return extract(new RDFInputString(proof, RDFInput.N3), factory, proofContext);
    }

    public static ProofActions extract(RDFInput proofInput, N3ReasonerFactory factory,
                                       PragProofContext proofContext) throws Exception {
        Model model, proofModel;
        N3Reasoner n3 = factory.createReasoner();
        try (PPPA rules = new PPPA();
             RDFInput input = setupInput(proofInput)) {
            n3.addInput(input);
            for (RDFInput ruleInput : rules.getInputs()) n3.addInput(ruleInput);

            n3.setExplainProof(false);

            proofModel = new RDFInputStream(n3.run(), RDFFormat.TURTLE).getModel();
            model = createModel(proofModel, proofContext);

            n3.waitFor();
        }

        // Get interactions topologically sorted
        List<Resource> list = new LinkedList<>();
        ResIterator it = model.listSubjectsWithProperty(RDF.type, UnservedP.Action);
        while (it.hasNext())
            list.add(it.next());
        list = topologicalSort(model, list);
        list = filterTopLevelActions(list);

        return new ProofActions(model, proofModel, list, proofContext);
    }

    public void setOwnsProofContext() {
        this.ownsProofContext = true;
    }

    public Model getModel() {
        return model;
    }

    public void setProofContext(PragProofContext proofContext) {
        this.ownsProofContext = false;
        model = createModel(proofModel, proofContext);
        this.proofContext = proofContext;

        List<Resource> list = new ArrayList<>(interactions.size());
        for (Resource interaction : interactions) {
            if (interaction.isAnon())
                list.add(model.createResource(interaction.getId()));
            else if (interaction.isURIResource())
                list.add(model.createResource(interaction.getURI()));
            else
                assert false : "Resource is neither anon neither URI";
        }
        this.interactions = list;
    }

    public ProofActions duplicate(Map<PragProofContext, PragProofContext> copies) {
        ProofActions dup = new ProofActions(this);
        if (dup.ownsProofContext) {
            PragProofContext proofContextCopy = dup.proofContext.createNonIsolatedCopy();
            dup.setProofContext(proofContextCopy);
            dup.setOwnsProofContext();
        } else if (copies.containsKey(dup.proofContext)) {
            dup.setProofContext(copies.get(dup.proofContext));
        } else {
            dup.setProofContext(dup.proofContext.createNonIsolatedCopy());
            dup.setOwnsProofContext();
        }
        return dup;
    }

    private static Model createModel(Model proofModel, PragProofContext proofContext) {
        return ModelFactory.createModelForGraph(new Union(
                new UncloseableGraph(proofModel.getGraph()),
                new UncloseableGraph(proofContext.getDataContext()
                        .getUnmodifiableModel().getGraph())));
    }

    private static List<Resource> filterTopLevelActions(List<Resource> list) {
        LinkedHashSet<Resource> filtered = new LinkedHashSet<>();
        filtered.addAll(list);

        list.stream().filter(r -> r.canAs(Sequence.class))
                .map(r -> r.as(Sequence.class).getMembers()).reduce((l, r) -> {
            List<Action> merged = new ArrayList<>(l.size() + r.size());
            merged.addAll(l);
            merged.addAll(r);
            return merged;
        }).orElse(Collections.emptyList()).stream().forEachOrdered(filtered::remove);

        return filtered.stream().collect(Collectors.toList());
    }

    private static RDFInput setupInput(RDFInput proofInput) throws IOException {
        Model model = ModelFactory.createDefaultModel();
        model.add(ResourceFactory.createResource(proofInput.toURI()), RDF.type,
                PPPA.Input);
        return new RDFInputModel(model, RDFFormat.TURTLE);
    }

    private static List<Resource> topologicalSort(Model model, List<Resource> list) {
        HashSet<Resource> visited = new HashSet<>();
        LinkedList<Resource> ordered = new LinkedList<>();
        for (Resource resource : list) {
            if (visited.contains(resource)) continue;
            topologicalSortVisit(visited, ordered, model, resource);
        }

        ArrayList<Resource> definitive = new ArrayList<>();
        definitive.addAll(ordered);
        return definitive;
    }

    private static void topologicalSortVisit(HashSet<Resource> visited, List<Resource> ordered,
                                             Model model, Resource resource) {
        final Property depends = PPPA.depends;

        if (visited.contains(resource)) return;
        NodeIterator it = model.listObjectsOfProperty(resource, depends);
        while (it.hasNext()) {
            RDFNode node = it.next();
            assert node.isResource();
            topologicalSortVisit(visited, ordered, model, node.asResource());
        }
        assert !visited.contains(resource) : "A Proof yields a DAG of interactions";
        visited.add(resource);
        ordered.add(0, resource);
    }

    public List<Resource> get() {
        return interactions;
    }

    public List<Resource> topologicallySorted() {
        return interactions;
    }

    public void close() {
        if (proofContext != null && ownsProofContext)
            proofContext.close();
        /* proofModel may be shared, therefore it is closed by finalizer */

    }

}

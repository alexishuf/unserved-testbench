package br.ufsc.lapesd.unserved.testbench.benchmarks.wsc08;

import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.ResourceFactory;

public class Wscc {
    public static String IRI = "https://alexishuf.bitbucket.io/2016/04/unserved/testbench/wsc-converter";
    public static String PREFIX = IRI + "#";

    public static Property wscService = ResourceFactory.createProperty(PREFIX + "wscService");
}

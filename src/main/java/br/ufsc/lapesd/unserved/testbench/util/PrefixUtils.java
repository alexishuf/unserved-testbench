package br.ufsc.lapesd.unserved.testbench.util;

import com.google.common.base.Preconditions;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;

import javax.annotation.Nonnull;
import java.util.Map;

public class PrefixUtils {
    public static @Nonnull String prefix(@Nonnull Model model, @Nonnull String uri) {
        for (Map.Entry<String, String> e : model.getNsPrefixMap().entrySet()) {
            if (uri.startsWith(e.getValue()))
                return e.getKey() + ":" + uri.substring(e.getValue().length(), uri.length());
        }
        return "<" + uri + ">";
    }

    public static @Nonnull String prefix(@Nonnull Resource uriResource) {
        Preconditions.checkArgument(uriResource.isURIResource());
        Preconditions.checkArgument(uriResource.getModel() != null);
        return prefix(uriResource.getModel(), uriResource.getURI());
    }
}

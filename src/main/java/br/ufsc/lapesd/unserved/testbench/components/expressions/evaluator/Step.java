package br.ufsc.lapesd.unserved.testbench.components.expressions.evaluator;

import br.ufsc.lapesd.unserved.testbench.components.expressions.evaluator.ClassExpressionEvaluator.Result;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collection;

public abstract class Step<T> {
    @Nonnull private final Collection<Step<T>> children;
    private T object = null;
    private Result result = Result.UNFINISHED;

    protected Step(@Nonnull Collection<Step<T>> children) {
        this.children = children;
    }

    @Nonnull
    public Collection<Step<T>> getChildren() {
        return children;
    }

    @Nullable
    public Result getEvaluatedResult(@Nonnull T object) {
        return (this.object != null && !this.object.equals(object)) ? null : result;
    }
    @Nonnull
    public Result evaluate(T object) {
        this.object = object;
        return result = performEvaluate(object);
    }

    protected abstract Result performEvaluate(T object);
}

package br.ufsc.lapesd.unserved.testbench.process.data.action;

import br.ufsc.lapesd.unserved.testbench.process.data.DataContext;
import com.google.common.base.Preconditions;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.jena.rdf.model.Statement;

import javax.annotation.Nonnull;

public class AddStatement implements DataContextAction {
    @Nonnull private final Statement statement;

    public AddStatement(@Nonnull Statement statement) {
        Preconditions.checkNotNull(statement);
        this.statement = statement;
    }

    @Override
    public void apply(DataContext context) {
        context.addStatement(statement);
    }

    @Override
    public String toString() {
        return String.format("AddStatement(%s, %s, %s)", statement.getSubject().toString(),
                statement.getPredicate().toString(), statement.getObject().toString());
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof AddStatement)) return false;
        AddStatement rhs = (AddStatement)o;
        return statement.equals(rhs.statement);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(statement).hashCode();
    }
}

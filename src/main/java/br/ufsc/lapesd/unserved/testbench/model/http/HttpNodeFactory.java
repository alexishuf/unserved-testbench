package br.ufsc.lapesd.unserved.testbench.model.http;

import br.ufsc.lapesd.unserved.testbench.model.http.impl.AsHttpPropertyImpl;
import br.ufsc.lapesd.unserved.testbench.model.http.impl.HttpRequestNodeImpl;
import br.ufsc.lapesd.unserved.testbench.model.http.impl.HttpResponseNodeImpl;
import org.apache.jena.enhanced.BuiltinPersonalities;
import org.apache.jena.enhanced.Personality;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;

import javax.annotation.Nullable;

public class HttpNodeFactory {
    public static @Nullable HttpMessageNode asMessage(@Nullable Resource resource) {
        if (resource == null) return null;
        if (resource.canAs(HttpRequestNode.class))
            return resource.as(HttpRequestNode.class);
        if (resource.canAs(HttpResponseNode.class))
            return resource.as(HttpResponseNode.class);
        return null;
    }

    private static boolean installed = false;
    public synchronized static void install() {
        if (!installed) {
            installed = true;
            install(BuiltinPersonalities.model);
        }
    }

    public static void install(Personality<RDFNode> personality) {
        personality.add(HttpRequestNode.class,  HttpRequestNodeImpl.factory );
        personality.add(HttpResponseNode.class, HttpResponseNodeImpl.factory);
        personality.add(AsHttpProperty.class,  AsHttpPropertyImpl.factory );
    }
}

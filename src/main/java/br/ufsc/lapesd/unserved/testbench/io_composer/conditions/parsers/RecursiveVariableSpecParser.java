package br.ufsc.lapesd.unserved.testbench.io_composer.conditions.parsers;

import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.atomic.VariableSpec;
import br.ufsc.lapesd.unserved.testbench.model.Variable;
import br.ufsc.lapesd.unserved.testbench.model.WithParts;
import org.apache.jena.rdf.model.Resource;

import javax.annotation.Nonnull;
import java.util.function.Consumer;

public class RecursiveVariableSpecParser extends VariableSpecParser {
    @Override
    public void parseLists(@Nonnull Consumer<Variable> variables,
                           @Nonnull Consumer<VariableSpec> specs, @Nonnull Resource resource) {
        WithParts wp = resource.as(WithParts.class);
        wp.getVariablesRecursive().forEach(v -> {variables.accept(v); specs.accept(v.asSpec());});
    }
}

package br.ufsc.lapesd.unserved.testbench.benchmarks.experiment;

public interface ExperimentFactory {
    Experiment createExperiment(Factors factors);
}

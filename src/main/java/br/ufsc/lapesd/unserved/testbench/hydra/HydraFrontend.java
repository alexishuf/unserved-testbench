package br.ufsc.lapesd.unserved.testbench.hydra;

import br.ufsc.lapesd.unserved.testbench.UnservedTranslator;
import br.ufsc.lapesd.unserved.testbench.UnservedTranslatorException;
import br.ufsc.lapesd.unserved.testbench.input.Input;
import br.ufsc.lapesd.unserved.testbench.input.RDFInput;
import br.ufsc.lapesd.unserved.testbench.input.RDFInputModel;
import br.ufsc.lapesd.unserved.testbench.iri.HTTP;
import br.ufsc.lapesd.unserved.testbench.util.BNodeShim;
import com.google.common.base.Preconditions;
import org.apache.commons.io.Charsets;
import org.apache.commons.io.IOUtils;
import org.apache.jena.graph.compose.MultiUnion;
import org.apache.jena.query.*;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFFormat;
import org.apache.jena.vocabulary.OWL2;
import org.apache.jena.vocabulary.RDFS;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class HydraFrontend implements UnservedTranslator {
    private static Logger logger = LoggerFactory.getLogger(HydraFrontend.class);

    private static String baseTpl, expectsTpl, returnsTpl, linkTpl;
    @Nonnull
    private static String getBaseTpl() {
        if (baseTpl != null) return baseTpl;
        try {
            baseTpl = IOUtils.toString(HydraFrontend.class.getClassLoader()
                    .getResourceAsStream("hydra-unserved.tpl.ttl"), Charsets.UTF_8);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return baseTpl;
    }
    @Nonnull
    private static String getExpectsTpl() {
        if (expectsTpl != null) return expectsTpl.replace("", "");
        try {
            expectsTpl = IOUtils.toString(HydraFrontend.class.getClassLoader()
                    .getResourceAsStream("hydra-unserved-expects.tpl.ttl"), Charsets.UTF_8);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return expectsTpl;
    }
    @Nonnull
    private static String getReturnsTpl() {
        if (returnsTpl != null) return returnsTpl;
        try {
            returnsTpl = IOUtils.toString(HydraFrontend.class.getClassLoader()
                    .getResourceAsStream("hydra-unserved-returns.tpl.ttl"), Charsets.UTF_8);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return returnsTpl;
    }
    @Nonnull
    private static String getLinkTpl() {
        if (linkTpl != null) return linkTpl;
        try {
            linkTpl = IOUtils.toString(HydraFrontend.class.getClassLoader()
                    .getResourceAsStream("hydra-unserved-link.tpl.ttl"), Charsets.UTF_8);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return linkTpl;
    }

    @Nonnull
    @Override
    public RDFInput compile(@Nonnull List<Input> descriptions) throws UnservedTranslatorException {
        Preconditions.checkArgument(descriptions.stream().allMatch(i -> i instanceof RDFInput),
                "HydraTranslator expects descriptions as RDFInput instances");
        try {
            return compileFromRDF(descriptions.stream()
                    .map(i -> (RDFInput)i).collect(Collectors.toList()));
        } catch (IOException e) {
            throw new UnservedTranslatorException(e);
        }
    }

    @Nonnull
    private RDFInput compileFromRDF(@Nonnull List<RDFInput> inputs) throws IOException {
        Preconditions.checkNotNull(inputs);
        ArrayList<Model> models = new ArrayList<>();
        for (RDFInput input : inputs) {
            models.add(input.takeModel());
        }
        Model in = ModelFactory.createModelForGraph(
                new MultiUnion(models.stream().map(Model::getGraph).iterator()));
        Model unserved = ModelFactory.createDefaultModel();
        getSketches(in).forEach(s -> expand(unserved, s));
        return new RDFInputModel(unserved, RDFFormat.TURTLE);
    }

    private static void expand(@Nonnull Model out, @Nonnull Sketch sketch) {
        String ttl = getBaseTpl().replaceAll("\\{\\$element\\}", "<" + sketch.element + ">")
                .replaceAll("\\{\\$methodClass\\}", "<" + sketch.method + ">");
        if (sketch.expects != null)
            ttl += getExpectsTpl().replaceAll("\\{\\$expects\\}", "<" + sketch.expects + ">");
        if (sketch.returns != null) {
            ttl += getReturnsTpl().replaceAll("\\{\\$returns\\}", "<" + sketch.returns + ">");
            for (Link link : sketch.links) {
                ttl += getLinkTpl().replaceAll("\\{\\$link\\}", "<" + link.link + ">")
                        .replaceAll("\\{\\$range\\}", "<" + link.range + ">");
            }
        }
        RDFDataMgr.read(out, IOUtils.toInputStream(ttl), Lang.TURTLE);
    }

    @Nonnull
    private static List<Sketch> getSketches(@Nonnull Model in) {
        List<Sketch> list = new ArrayList<>();
        try (QueryExecution ex = QueryExecutionFactory.create(
                QueryFactory.create("PREFIX hydra: <" + Hydra.PREFIX + ">\n" +
                        "SELECT ?element ?operation ?method WHERE {\n" +
                        "  ?element hydra:supportedOperation ?operation.\n" +
                        "  ?operation hydra:method ?method.\n" +
                        "}\n"), in)) {
            ResultSet results = ex.execSelect();
            while (results.hasNext()) {
                QuerySolution sol = results.next();
                Resource e = sol.getResource("element"), op = sol.getResource("operation");

                String methodName = sol.getLiteral("method").getLexicalForm();
                Resource method = HTTP.Methods.getMethod(methodName);
                if (method == null) {
                    logger.error("Hydra description contains unknown method name {}.", methodName);
                    continue;
                }
                Sketch sketch = new Sketch(e, op, method);

                Statement stmt = op.getProperty(Hydra.expects);
                if (stmt != null) sketch.expects = stmt.getResource();
                stmt = op.getProperty(Hydra.returns);
                if (stmt != null) {
                    sketch.returns = stmt.getResource();
                    sketch.links = findLinks(in, sketch.returns);
                }
                list.add(sketch);
            }
        }

        return list;
    }

    @Nonnull
    private static List<Link> findLinks(@Nonnull Model in, @Nonnull Resource domain) {
        List<Link> list = new ArrayList<>();
        try (QueryExecution ex = QueryExecutionFactory.create(
                QueryFactory.create(String.format("PREFIX hydra: <" + Hydra.PREFIX  +">\n" +
                        "PREFIX rdfs: <" + RDFS.getURI() + ">\n" +
                        "SELECT ?link WHERE {\n" +
                        "  %1$s hydra:supportedProperty/hydra:property ?link.\n" +
                        "  ?link a hydra:Link." +
                        "}\n", BNodeShim.sparqlResource(domain))), in)) {
            ResultSet results = ex.execSelect();
            while (results.hasNext()) {
                Resource link = results.next().getResource("link");
                Statement stmt = link.getProperty(RDFS.range);
                Resource range = stmt == null ? OWL2.Thing : stmt.getResource();
                list.add(new Link(link, range));
            }
        }
        return list;
    }

    private static class Sketch {
        @Nonnull final Resource element;
        @Nonnull final Resource operation;
        @Nonnull final Resource method;
        Resource expects = null;
        Resource returns = null;
        @Nonnull List<Link> links = new ArrayList<>();


        private Sketch(@Nonnull Resource element, @Nonnull Resource operation,
                       @Nonnull Resource method) {
            this.element = element;
            this.operation = operation;
            this.method = method;
        }
    }

    private static class Link {
        @Nonnull final Resource link;
        @Nonnull final Resource range;

        private Link(@Nonnull Resource link, @Nonnull Resource range) {
            this.link = link;
            this.range = range;
        }
    }
}

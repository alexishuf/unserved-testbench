package br.ufsc.lapesd.unserved.testbench.io_composer.backward;

import br.ufsc.lapesd.unserved.testbench.components.DefaultComponentRegistry;
import br.ufsc.lapesd.unserved.testbench.composer.ComposerException;
import br.ufsc.lapesd.unserved.testbench.composer.Composition;
import br.ufsc.lapesd.unserved.testbench.composer.ReplicatedComposer;
import br.ufsc.lapesd.unserved.testbench.io_composer.IOComposer;
import br.ufsc.lapesd.unserved.testbench.io_composer.impl.AbstractIOComposerBuilder;
import br.ufsc.lapesd.unserved.testbench.io_composer.impl.DefaultIOComposerTimes;
import br.ufsc.lapesd.unserved.testbench.io_composer.impl.IOComposerInput;
import br.ufsc.lapesd.unserved.testbench.io_composer.impl.IOComposerInputDelta;
import br.ufsc.lapesd.unserved.testbench.io_composer.replicated.AbstractExecution;
import br.ufsc.lapesd.unserved.testbench.io_composer.replicated.ReplicatedHelper;
import br.ufsc.lapesd.unserved.testbench.io_composer.state.State;
import br.ufsc.lapesd.unserved.testbench.process.CompleteTask;
import br.ufsc.lapesd.unserved.testbench.process.Interpreter;
import br.ufsc.lapesd.unserved.testbench.process.UnservedPInterpreter;
import br.ufsc.lapesd.unserved.testbench.process.data.DataContextDiff;
import br.ufsc.lapesd.unserved.testbench.replication.ReplicatedExecutionException;
import com.google.common.base.Preconditions;
import es.usc.citius.hipster.model.Node;
import es.usc.citius.hipster.model.Transition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

public class ReplicatedBackwardIOAStar extends AbstractBackwardIOAStar
        implements ReplicatedComposer, IOComposer {
    private static Logger logger = LoggerFactory.getLogger(ReplicatedBackwardIOAStar.class);
    private final Function<IOComposerInput, BackwardReplicatedGraphAccessor<State, Double, Transition<Void, State>>> graphAccessorFactory;
    private Execution execution = null;

    public ReplicatedBackwardIOAStar(@Nonnull IOComposerInput composerInput,
                              @Nonnull Function<IOComposerInput, BackwardReplicatedGraphAccessor<State, Double, Transition<Void, State>>> graphAccessorFactory)
            throws ComposerException {
        super(composerInput, graphAccessorFactory.apply(composerInput));
        this.graphAccessorFactory = graphAccessorFactory;
    }

    @Nonnull
    @Override
    public Composition run() throws ComposerException {
        if (execution == null) {
            logger.warn("Creating execution instance with interpreter from run().");
            execution = new Execution(this,
                    new UnservedPInterpreter(DefaultComponentRegistry.getInstance()));
        }
        return super.run();
    }

    @Override
    @Deprecated
    public Interpreter.Task run(Interpreter interpreter) throws ReplicatedExecutionException {
        //TODO Get the interpreter from within the action runner
        Preconditions.checkState(execution == null);
        try (Execution ex = new Execution(this, interpreter)) {
            execution = ex;
            ex.resumeInterpretation();
            return new CompleteTask();
        } finally {
            execution = null;
        }
    }

    @Override
    protected <A, NodeType extends Node<A, State, NodeType>>
    Composition toComposition(Node<A, State, NodeType> goalNode) {
        Preconditions.checkState(execution != null);
        return ReplicatedHelper.createReplicationComposition(ci, execution,
                ReplicatedHelper.toFinePath(goalNode));
    }

    protected static class Execution extends AbstractExecution {
        @Nonnull
        private Function<IOComposerInput, BackwardReplicatedGraphAccessor<State, Double, Transition<Void, State>>> graphAccessorFactory;
        private ReplicatedBackwardIOAStar algorithm = null;

        public Execution(@Nonnull ReplicatedBackwardIOAStar algorithm, @Nonnull Interpreter interpreter) {
            super(new IOComposerInputDelta(algorithm.ci), interpreter, null, null);
            this.algorithm = algorithm;
            this.graphAccessorFactory = algorithm.graphAccessorFactory;
        }

        public Execution(@Nonnull Function<IOComposerInput, BackwardReplicatedGraphAccessor<State, Double, Transition<Void, State>>> graphAccessorFactory,
                         @Nonnull IOComposerInputDelta composerInput,
                         @Nonnull Interpreter interpreter,
                         @Nonnull DefaultIOComposerTimes times,
                         DataContextDiff diff,
                         Composition lastComp) {
            super(composerInput, interpreter, diff, lastComp);
            this.times = times;
            this.graphAccessorFactory = graphAccessorFactory;
        }

        @Override
        public Execution duplicate() {
            Preconditions.checkState(!closed);
            return new Execution(graphAccessorFactory, new IOComposerInputDelta(composerInput),
                    interpreter, times.duplicate(), diff, lastComp);
        }

        @Override
        public void close() {
            if (!closed) {
                if (algorithm != null) algorithm.close();
                super.close();
            }
        }

        @Override
        protected ReplicatedBackwardIOAStar getAlgorithm() {
            if (algorithm == null) {
                algorithm = new ReplicatedBackwardIOAStar(composerInput, graphAccessorFactory);
                algorithm.execution = this;
            }
            return algorithm;
        }

        @Override
        protected void blacklistOperation(@Nonnull Composition composition) {
            times.addTime("blacklisting", 0, TimeUnit.MICROSECONDS);
            logger.error("Not implemeted, FIXME");
        }
    }

    public static class Builder extends AbstractIOComposerBuilder {
        private Function<IOComposerInput, BackwardReplicatedGraphAccessor<State, Double,
                Transition<Void, State>>> accessorFactory = NaiveBackwardGraphAccessor::new;

        public Builder setAccessorFactory(Function<IOComposerInput, BackwardReplicatedGraphAccessor<State, Double, Transition<Void, State>>> accessorFactory) {
            this.accessorFactory = accessorFactory;
            return this;
        }

        @Override
        protected IOComposer buildImpl() throws Exception {
            return new ReplicatedBackwardIOAStar(composerInput, accessorFactory);
        }
    }

}

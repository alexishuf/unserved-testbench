package br.ufsc.lapesd.unserved.testbench.io_composer.impl;

import br.ufsc.lapesd.unserved.testbench.composer.ComposerBuilderException;
import br.ufsc.lapesd.unserved.testbench.composer.ComposerException;
import br.ufsc.lapesd.unserved.testbench.input.RDFInput;
import br.ufsc.lapesd.unserved.testbench.io_composer.IOComposer;
import br.ufsc.lapesd.unserved.testbench.io_composer.IOComposerBuilder;
import br.ufsc.lapesd.unserved.testbench.model.Variable;
import com.google.common.base.Preconditions;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Helper implementing boilerplate code of a {@link IOComposerBuilder} implementation.
 */
public abstract class AbstractIOComposerBuilder implements IOComposerBuilder {
    protected final IOComposerInput composerInput = new IOComposerInputImpl();
    private boolean basicReasoning = true;
    private boolean inputHasNoSkolemized = false;
    private boolean closed = false;

    @Nonnull
    @Override
    public IOComposerBuilder withInputHasNoSkolemized(boolean inputHasNoSkolemized) {
        this.inputHasNoSkolemized = inputHasNoSkolemized;
        return this;
    }

    @Nonnull
    @Override
    public IOComposerBuilder addInput(@Nonnull RDFInput input) {
        composerInput.addInput(input);
        return this;
    }

    @Override
    @Nonnull
    public IOComposerBuilder withRulesFile(@Nonnull File rulesFile) throws IOException {
        composerInput.addRulesFile(rulesFile);
        return this;
    }

    @Nonnull
    @Override
    public IOComposerBuilder markWantedVariable(@Nonnull Resource variable) {
        composerInput.addWanted(variable);
        return this;
    }

    @Override
    public IOComposerBuilder withBasicReasoning(boolean enabled) {
        this.basicReasoning = enabled;
        return this;
    }

    protected abstract IOComposer buildImpl() throws Exception;

    @Override
    public IOComposer build() {
        Preconditions.checkState(!closed);
        try {
            composerInput.initSkolemizedUnion(basicReasoning, inputHasNoSkolemized);
            List<Resource> bad = badVariables();
            if (!bad.isEmpty()) {
                throw new ComposerBuilderException("Some wanted variables are not known as " +
                        "unserved:Variable from the inputs: " + bad.stream()
                        .map(Resource::toString).reduce((l, r) -> l + ", " + r).orElse(""));
            }
            IOComposer composer = buildImpl();
            closed = true; //ownership transferred to composer
            return composer;
        } catch (ComposerBuilderException e) {
            throw e;
        } catch (Exception e) {
            throw new ComposerBuilderException(e);
        }
    }

    private List<Resource> badVariables() {
        Model union = composerInput.getSkolemizedUnion();
        Set<Resource> ok = composerInput.getWanted().stream().filter(union::containsResource)
                .map(v -> v.isURIResource() ? union.createResource(v.getURI())
                        : union.createResource(v.getId()))
                .filter(v -> v.canAs(Variable.class)).collect(Collectors.toSet());
        return composerInput.getWanted().stream().filter(v -> !ok.contains(v))
                .collect(Collectors.toList());
    }

    @Override
    public void close() throws ComposerBuilderException {
        if (closed) return;
        closed = true;
        try {
            composerInput.close();
        } catch (ComposerException e) {
            throw new ComposerBuilderException(e);
        }
    }
}

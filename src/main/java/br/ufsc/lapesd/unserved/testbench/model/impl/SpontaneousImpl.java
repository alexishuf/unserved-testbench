package br.ufsc.lapesd.unserved.testbench.model.impl;

import br.ufsc.lapesd.unserved.testbench.iri.Unserved;
import br.ufsc.lapesd.unserved.testbench.model.Spontaneous;
import br.ufsc.lapesd.unserved.testbench.util.ImplementationByType;
import org.apache.jena.enhanced.EnhGraph;
import org.apache.jena.enhanced.EnhNode;
import org.apache.jena.enhanced.Implementation;
import org.apache.jena.graph.Node;
import org.apache.jena.rdf.model.impl.ResourceImpl;

import java.util.Collections;

public class SpontaneousImpl extends ResourceImpl implements Spontaneous {
    public static Implementation factory = new ImplementationByType(Unserved.Spontaneous.asNode(),
            Collections.emptyList()) {
        @Override
        public EnhNode wrap(Node node, EnhGraph eg) {
            return new SpontaneousImpl(node, eg);
        }
    };

    public SpontaneousImpl(Node n, EnhGraph m) {
        super(n, m);
    }
}

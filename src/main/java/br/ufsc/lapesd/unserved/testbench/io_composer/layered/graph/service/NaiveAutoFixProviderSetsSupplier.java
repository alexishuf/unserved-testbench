package br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service;

import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.atomic.VariableSpec;
import br.ufsc.lapesd.unserved.testbench.io_composer.impl.IOComposerInput;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.discovery.NodeOutput;
import br.ufsc.lapesd.unserved.testbench.model.Variable;
import br.ufsc.lapesd.unserved.testbench.util.BFSBGPIterator;
import com.google.common.base.Preconditions;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.SetMultimap;
import org.apache.jena.vocabulary.RDFS;

import javax.annotation.Nonnull;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class NaiveAutoFixProviderSetsSupplier implements AutoFixProviderSetsSupplier {
    private SetMultimap<VariableSpec, NodeOutput> cumOutputsIndex = null;

    @Override
    public boolean isActive() {
        return cumOutputsIndex != null;
    }

    @Override
    public void begin(IOComposerInput composerInput, @Nonnull StartNode startNode,
                      @Nonnull EndNode endNode) {
        cumOutputsIndex = HashMultimap.create();
    }

    @Override
    public void end() {
        cumOutputsIndex = null;
    }

    @Nonnull
    @Override
     public Map<Node, ProviderSet> get(@Nonnull Collection<Node> targetNodes) {
        Preconditions.checkState(isActive());
        Map<VariableSpec, List<NodeOutput>> inputAlts = getInputAlts(targetNodes);

        Map<Node, ProviderSet> map = new HashMap<>();
        for (Node target : targetNodes) map.put(target, getProviderSet(target, inputAlts));
        return map;
    }

    @Override
    public void update(@Nonnull Collection<Node> nodes) {
        for (Node node : nodes) {
            for (Variable var : node.getOutputs())
                cumOutputsIndex.put(var.asSpec(), new NodeOutput(node, var));
        }
    }

    private Stream<VariableSpec> getWithSubClasses(VariableSpec spec) {
        return BFSBGPIterator.from(spec.getType()).withInitial().backward(RDFS.subClassOf)
                .toStream().map(t -> new VariableSpec(t, spec.getRepresentation()));
    }

    private Map<VariableSpec, List<NodeOutput>>
    getInputAlts(@Nonnull Collection<Node> targetNodes) {
        Map<VariableSpec, List<NodeOutput>> map = new HashMap<>();
        targetNodes.stream().map(Node::getInputs).flatMap(Set::stream)
                .map(VariableSpec::new).distinct()
                .forEach(spec -> map.put(spec, getWithSubClasses(spec)
                        .flatMap(s -> cumOutputsIndex.get(s).stream())
                        .distinct().collect(Collectors.toList()))
                );
        return map;
    }

    @Nonnull
    private ProviderSet getProviderSet(Node target,
                                       Map<VariableSpec, List<NodeOutput>> specAlts) {
        Map<Variable, List<NodeOutput>> nodeAlts = new HashMap<>();
        for (Variable in : target.getInputs()) {
            VariableSpec spec = in.asSpec();
            List<NodeOutput> nodes = specAlts.get(spec);
            nodeAlts.put(in, nodes);
        }

        IOProviderHashSet providerSet = new IOProviderHashSet();
        nodeAlts.keySet().forEach(providerSet::addTarget);
        for (Map.Entry<Variable, List<NodeOutput>> e : nodeAlts.entrySet()) {
            for (NodeOutput no : e.getValue())
                providerSet.add(e.getKey(), no.getNode(), no.getOutput());
        }
        return providerSet;
    }


}

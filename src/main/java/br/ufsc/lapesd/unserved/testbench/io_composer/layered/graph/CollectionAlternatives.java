package br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph;

import javax.annotation.Nonnull;
import java.util.Iterator;
import java.util.LinkedHashSet;

public class CollectionAlternatives<T extends Alternative<?>> implements Alternatives<T> {
    private final @Nonnull LinkedHashSet<T> alternatives = new LinkedHashSet<>();

    public void add(T object) {
        alternatives.add(object);
    }

    @Override
    public CollectionAlternatives<T> duplicate() {
        CollectionAlternatives<T> dup = new CollectionAlternatives<>();
        dup.alternatives.addAll(alternatives);
        return dup;
    }

    @Override
    public Iterator<T> iterator() {
        return alternatives.iterator();
    }
    @Override
    public boolean remove(T object) {
        return alternatives.remove(object);
    }
}

package br.ufsc.lapesd.unserved.testbench;

public class UnservedTranslatorException extends Exception {
    public UnservedTranslatorException(String message) {
        super(message);
    }
    public UnservedTranslatorException(String message, Exception cause) {
        super(message, cause);
    }
    public UnservedTranslatorException(Exception cause) {
        super(cause);
    }
}

package br.ufsc.lapesd.unserved.testbench.httpcomponents.extractors;

import br.ufsc.lapesd.unserved.testbench.model.Variable;
import br.ufsc.lapesd.unserved.testbench.process.Context;
import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class SetVariableHttpResponseExtraction implements HttpResponseExtraction {
    private final @Nonnull Model model;
    private final @Nonnull
    Variable variable;
    private final @Nonnull Resource representation;
    private final @Nullable Resource resourceValue;
    private final @Nullable Literal literalValue;

    public SetVariableHttpResponseExtraction(@Nonnull Model model, @Nonnull Variable variable,
                                             @Nonnull Resource representation,
                                             @Nonnull Resource resourceValue) {
        this.model = model;
        this.variable = variable;
        this.representation = representation;
        this.resourceValue = resourceValue;
        this.literalValue = null;
    }

    public SetVariableHttpResponseExtraction(@Nonnull Model model, @Nonnull Variable variable,
                                             @Nonnull Resource representation,
                                             @Nonnull Literal literalValue) {
        this.model = model;
        this.variable = variable;
        this.representation = representation;
        this.resourceValue = null;
        this.literalValue = literalValue;
    }

    @Override
    public void apply(Context context) {
        context.data().addStatements(model.listStatements());
        if (resourceValue != null)
            context.data().setVariable(variable, representation, resourceValue);
        else if (literalValue != null)
            context.data().setVariable(variable, representation, literalValue);
        else
            assert false : "Unexpected state";
    }
}

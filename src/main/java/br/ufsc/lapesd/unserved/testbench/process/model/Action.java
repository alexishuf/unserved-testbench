package br.ufsc.lapesd.unserved.testbench.process.model;

import org.apache.jena.rdf.model.Resource;

/**
 * Enhanced node interface for unserved-p:Action
 */
public interface Action extends Resource {
}

package br.ufsc.lapesd.unserved.testbench.io_composer.conditions;

/**
 * A Condition that is not made up of other conditions
 */
public interface AtomicCondition extends Condition {
}

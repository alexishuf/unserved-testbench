package br.ufsc.lapesd.unserved.testbench.model;

/**
 * Enhanced node for unserved:Reaction
 */
public interface Reaction extends When {
    Message getReactionTo();
    Cardinality getReactionCardinality();
}

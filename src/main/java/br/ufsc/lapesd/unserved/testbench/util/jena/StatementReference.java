package br.ufsc.lapesd.unserved.testbench.util.jena;

import org.apache.jena.graph.Triple;
import org.apache.jena.rdf.model.*;

import java.util.function.Function;

/**
 * For use by {@link ModelReference}
 */
class StatementReference implements Statement {
    private final Model model;
    private final Function<RDFNode, RDFNode> nodeWrapper;
    private final Statement referee;

     StatementReference(Model model, Function<RDFNode, RDFNode> nodeWrapper, Statement referee) {
        this.model = model;
        this.nodeWrapper = nodeWrapper;
        this.referee = referee;
    }

    @SuppressWarnings("EqualsWhichDoesntCheckParameterClass")
    @Override
    public boolean equals(Object o) {
        return referee.equals(o);
    }

    @Override
    public int hashCode() {
        return referee.hashCode();
    }

    @Override
    public Resource getSubject() {
        return nodeWrapper.apply(referee.getSubject()).as(Resource.class);
    }

    @Override
    public Property getPredicate() {
        return nodeWrapper.apply(referee.getPredicate()).as(Property.class);
    }

    @Override
    public RDFNode getObject() {
        return nodeWrapper.apply(referee.getObject()).as(RDFNode.class);
    }

    @Override
    public Statement getProperty(Property p) {
        return new  StatementReference(model, nodeWrapper, referee.getProperty(p));
    }

    @Override
    public Statement getStatementProperty(Property p) {
        return new  StatementReference(model, nodeWrapper, referee.getStatementProperty(p));
    }

    @Override
    public Resource getResource() {
        return nodeWrapper.apply(referee.getResource()).as(Resource.class);
    }

    @Override
    public Literal getLiteral() {
        return nodeWrapper.apply(referee.getLiteral()).as(Literal.class);
    }

    @Override
    public boolean getBoolean() {
        return referee.getBoolean();
    }

    @Override
    public byte getByte() {
        return referee.getByte();
    }

    @Override
    public short getShort() {
        return referee.getShort();
    }

    @Override
    public int getInt() {
        return referee.getInt();
    }

    @Override
    public long getLong() {
        return referee.getLong();
    }

    @Override
    public char getChar() {
        return referee.getChar();
    }

    @Override
    public float getFloat() {
        return referee.getFloat();
    }

    @Override
    public double getDouble() {
        return referee.getDouble();
    }

    @Override
    public String getString() {
        return referee.getString();
    }

    @Override
    @Deprecated
    public Resource getResource(ResourceF f) {
        return nodeWrapper.apply(referee.getResource(f)).as(Resource.class);
    }

    @Override
    public Bag getBag() {
        return nodeWrapper.apply(referee.getBag()).as(Bag.class);
    }

    @Override
    public Alt getAlt() {
        return nodeWrapper.apply(referee.getAlt()).as(Alt.class);
    }

    @Override
    public Seq getSeq() {
        return nodeWrapper.apply(referee.getSeq()).as(Seq.class);
    }

    @Override
    public String getLanguage() {
        return referee.getLanguage();
    }

    @Override
    public boolean hasWellFormedXML() {
        return referee.hasWellFormedXML();
    }

    @Override
    public Statement changeLiteralObject(boolean o) {
        return new  StatementReference(model, nodeWrapper, referee.changeLiteralObject(o));
    }

    @Override
    public Statement changeLiteralObject(long o) {
        return new  StatementReference(model, nodeWrapper, referee.changeLiteralObject(o));
    }

    @Override
    public Statement changeLiteralObject(int o) {
        return new  StatementReference(model, nodeWrapper, referee.changeLiteralObject(o));
    }

    @Override
    public Statement changeLiteralObject(char o) {
        return new  StatementReference(model, nodeWrapper, referee.changeLiteralObject(o));
    }

    @Override
    public Statement changeLiteralObject(float o) {
        return new  StatementReference(model, nodeWrapper, referee.changeLiteralObject(o));
    }

    @Override
    public Statement changeLiteralObject(double o) {
        return new  StatementReference(model, nodeWrapper, referee.changeLiteralObject(o));
    }

    @Override
    public Statement changeObject(String o) {
        return new  StatementReference(model, nodeWrapper, referee.changeObject(o));
    }

    @Override
    public Statement changeObject(String o, boolean wellFormed) {
        return new  StatementReference(model, nodeWrapper, referee.changeObject(o, wellFormed));
    }

    @Override
    public Statement changeObject(String o, String l) {
        return new  StatementReference(model, nodeWrapper, referee.changeObject(o, l));
    }

    @Override
    public Statement changeObject(String o, String l, boolean wellFormed) {
        return new  StatementReference(model, nodeWrapper, referee.changeObject(o, l, wellFormed));
    }

    @Override
    public Statement changeObject(RDFNode o) {
        return new  StatementReference(model, nodeWrapper, referee.changeObject(o));
    }

    @Override
    public Statement remove() {
        return new  StatementReference(model, nodeWrapper, referee.remove());
    }

    @Override
    public boolean isReified() {
        return referee.isReified();
    }

    @Override
    public ReifiedStatement createReifiedStatement() {
        return nodeWrapper.apply(referee.createReifiedStatement()).as(ReifiedStatement.class);
    }

    @Override
    public ReifiedStatement createReifiedStatement(String uri) {
        return nodeWrapper.apply(referee.createReifiedStatement(uri)).as(ReifiedStatement.class);
    }

    @Override
    public RSIterator listReifiedStatements() {
        return new MapFilterRSIterator<>(s -> nodeWrapper.apply(s).as(ReifiedStatement.class),
                referee.listReifiedStatements());
    }

    @Override
    public Model getModel() {
        return this.model;
    }

    @Override
    public void removeReification() {
        referee.removeReification();
    }

    @Override
    public Triple asTriple() {
        return referee.asTriple();
    }
}

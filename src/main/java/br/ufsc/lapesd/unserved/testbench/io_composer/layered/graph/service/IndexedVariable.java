package br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service;

import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.atomic.VariableSpec;
import br.ufsc.lapesd.unserved.testbench.model.Part;
import br.ufsc.lapesd.unserved.testbench.model.Variable;
import com.google.common.base.Preconditions;
import org.apache.jena.datatypes.RDFDatatype;
import org.apache.jena.graph.Node;
import org.apache.jena.rdf.model.*;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.List;
import java.util.Objects;
import java.util.Set;

public class IndexedVariable implements Variable {
    public final @Nonnull Variable variable;
    public final int index;

    public IndexedVariable(@Nonnull Variable variable) {
        this(variable, 0);
    }

    public IndexedVariable(@Nonnull Variable variable, int index) {
        this.variable = (variable instanceof IndexedVariable)
                      ? ((IndexedVariable) variable).getVariable() : variable;
        assert !(variable instanceof IndexedVariable);
        this.index = index;
    }

    @Nonnull
    @Override
    public Variable enhance(@Nonnull Resource other) {
        Preconditions.checkArgument(Objects.equals(getURI(), other.getURI()));
        Preconditions.checkArgument(other.canAs(Variable.class));
        return new IndexedVariable(other.as(Variable.class), index);
    }

    @Nonnull
    public Variable getVariable() {
        return variable;
    }

    public int getIndex() {
        return index;
    }

    @Override
    public boolean matches(Variable other) {
        return equals(other); //only match if both refer to the same index
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IndexedVariable that = (IndexedVariable) o;
        return index == that.index &&
                Objects.equals(variable, that.variable);
    }

    @Override
    public Resource getValueRepresentation(int index) {
        return variable.getValueRepresentation(index);
    }

    @Override
    public Resource getResourceValue(int index) {
        return variable.getResourceValue(index);
    }

    @Override
    public Literal getLiteralValue(int index) {
        return variable.getLiteralValue(index);
    }

    @Override
    public RDFNode getValue(int index) {
        return variable.getValue(index);
    }

    @Override
    public Statement getRequiredProperty(Property p) {
        return variable.getRequiredProperty(p);
    }

    @Override
    public Statement getRequiredProperty(Property p, String lang) {
        return variable.getRequiredProperty(p, lang);
    }

    @Override
    public Statement getProperty(Property p) {
        return variable.getProperty(p);
    }

    @Override
    public Statement getProperty(Property p, String lang) {
        return variable.getProperty(p, lang);
    }

    @Override
    public StmtIterator listProperties(Property p) {
        return variable.listProperties(p);
    }

    @Override
    public StmtIterator listProperties(Property p, String lang) {
        return variable.listProperties(p, lang);
    }

    @Override
    public StmtIterator listProperties() {
        return variable.listProperties();
    }

    @Override
    public Resource addLiteral(Property p, boolean o) {
        return variable.addLiteral(p, o);
    }

    @Override
    public Resource addLiteral(Property p, long o) {
        return variable.addLiteral(p, o);
    }

    @Override
    public Resource addLiteral(Property p, char o) {
        return variable.addLiteral(p, o);
    }

    @Override
    public Resource addLiteral(Property value, double d) {
        return variable.addLiteral(value, d);
    }

    @Override
    public Resource addLiteral(Property value, float d) {
        return variable.addLiteral(value, d);
    }

    @Override
    public Resource addLiteral(Property p, Object o) {
        return variable.addLiteral(p, o);
    }

    @Override
    public Resource addLiteral(Property p, Literal o) {
        return variable.addLiteral(p, o);
    }

    @Override
    public Resource addProperty(Property p, String o) {
        return variable.addProperty(p, o);
    }

    @Override
    public Resource addProperty(Property p, String o, String l) {
        return variable.addProperty(p, o, l);
    }

    @Override
    public Resource addProperty(Property p, String lexicalForm, RDFDatatype datatype) {
        return variable.addProperty(p, lexicalForm, datatype);
    }

    @Override
    public Resource addProperty(Property p, RDFNode o) {
        return variable.addProperty(p, o);
    }

    @Override
    public boolean hasProperty(Property p) {
        return variable.hasProperty(p);
    }

    @Override
    public boolean hasLiteral(Property p, boolean o) {
        return variable.hasLiteral(p, o);
    }

    @Override
    public boolean hasLiteral(Property p, long o) {
        return variable.hasLiteral(p, o);
    }

    @Override
    public boolean hasLiteral(Property p, char o) {
        return variable.hasLiteral(p, o);
    }

    @Override
    public boolean hasLiteral(Property p, double o) {
        return variable.hasLiteral(p, o);
    }

    @Override
    public boolean hasLiteral(Property p, float o) {
        return variable.hasLiteral(p, o);
    }

    @Override
    public boolean hasLiteral(Property p, Object o) {
        return variable.hasLiteral(p, o);
    }

    @Override
    public boolean hasProperty(Property p, String o) {
        return variable.hasProperty(p, o);
    }

    @Override
    public boolean hasProperty(Property p, String o, String l) {
        return variable.hasProperty(p, o, l);
    }

    @Override
    public boolean hasProperty(Property p, RDFNode o) {
        return variable.hasProperty(p, o);
    }

    @Override
    public Resource removeProperties() {
        return variable.removeProperties();
    }

    @Override
    public Resource removeAll(Property p) {
        return variable.removeAll(p);
    }

    @Override
    public Resource begin() {
        return variable.begin();
    }

    @Override
    public Resource abort() {
        return variable.abort();
    }

    @Override
    public Resource commit() {
        return variable.commit();
    }

    @Override
    public Resource getPropertyResourceValue(Property p) {
        return variable.getPropertyResourceValue(p);
    }


    @Override
    public void unset(int index) {
        variable.unset(index);
    }

    @Override
    public void set(int index, @Nullable Resource representation, @Nonnull Literal literal) {
        variable.set(index, representation, literal);
    }

    @Override
    public void set(int index, @Nullable Resource representation, @Nonnull Resource resource) {
        variable.set(index, representation, resource);
    }

    @Override
    public boolean isAnon() {
        return variable.isAnon();
    }

    @Override
    public boolean isLiteral() {
        return variable.isLiteral();
    }

    @Override
    public boolean isURIResource() {
        return variable.isURIResource();
    }

    @Override
    public boolean isResource() {
        return variable.isResource();
    }

    @Override
    public <T extends RDFNode> T as(Class<T> view) {
        return variable.as(view);
    }

    @Override
    public <T extends RDFNode> boolean canAs(Class<T> view) {
        return variable.canAs(view);
    }

    @Override
    public Model getModel() {
        return variable.getModel();
    }

    @Override
    public Object visitWith(RDFVisitor rv) {
        return variable.visitWith(rv);
    }

    @Override
    public Resource asResource() {
        return variable.asResource();
    }

    @Override
    public Literal asLiteral() {
        return variable.asLiteral();
    }

    @Override
    public Node asNode() {
        return variable.asNode();
    }

    @Override
    public int hashCode() {
        return Objects.hash(variable, index);
    }

    @Override
    public Resource getType() {
        return variable.getType();
    }

    @Override
    public Resource getRepresentation() {
        return variable.getRepresentation();
    }

    @Override
    public Resource getValueRepresentation() {
        return variable.getValueRepresentation(index);
    }

    @Override
    public Resource getResourceValue() {
        return variable.getResourceValue(index);
    }

    @Override
    public Literal getLiteralValue() {
        return variable.getLiteralValue(index);
    }

    @Override
    public RDFNode getValue() {
        return variable.getValue(index);
    }

    @Override
    public VariableSpec asSpec() {
        return variable.asSpec();
    }

    @Override
    public boolean isValueBound() {
        return variable.isValueBound(index);
    }

    @Override
    public boolean isValueBound(int index) {
        return variable.isValueBound(index);
    }

    @Override
    public List<Part> getParts() {
        return variable.getParts();
    }

    @Override
    public List<Variable> getVariables() {
        return variable.getVariables();
    }

    @Override
    public Set<Part> getPartsRecursive() {
        return variable.getPartsRecursive();
    }

    @Override
    public Set<Variable> getVariablesRecursive() {
        return variable.getVariablesRecursive();
    }

    @Override
    public AnonId getId() {
        return variable.getId();
    }

    @Override
    public Resource inModel(Model m) {
        return variable.inModel(m);
    }

    @Override
    public boolean hasURI(String uri) {
        return variable.hasURI(uri);
    }

    @Override
    public String getURI() {
        return variable.getURI();
    }

    @Override
    public String getNameSpace() {
        return variable.getNameSpace();
    }

    @Override
    public String getLocalName() {
        return variable.getLocalName();
    }

    @Override
    public String toString() {
        return String.format("%s[%d]", variable, index);
    }
}

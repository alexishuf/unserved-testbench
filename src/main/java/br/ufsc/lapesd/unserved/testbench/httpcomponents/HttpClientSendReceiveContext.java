package br.ufsc.lapesd.unserved.testbench.httpcomponents;

import br.ufsc.lapesd.unserved.testbench.process.AbstractActionContext;
import br.ufsc.lapesd.unserved.testbench.process.ActionRunnersContext;
import org.apache.http.HttpResponse;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.FutureRequestExecutionService;
import org.apache.http.impl.client.HttpRequestFutureTask;
import org.apache.http.protocol.HttpContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;

class HttpClientSendReceiveContext extends AbstractActionContext {
    private static Logger logger = LoggerFactory.getLogger(HttpClientSendReceiveContext.class);

    private final CloseableHttpClient client;
    private final FutureRequestExecutionService executionSvc;
    private final HttpRequestFutureTask<HttpResponse> task;


    public HttpClientSendReceiveContext(@Nonnull ActionRunnersContext runnersContext,
                                        @Nonnull Object key,
                                        @Nonnull CloseableHttpClient client,
                                        @Nonnull HttpUriRequest request,
                                        @Nonnull HttpContext context,
                                        @Nonnull ResponseHandler<HttpResponse> handler) {
        super(runnersContext, key);
        this.client = client;
        executionSvc = new FutureRequestExecutionService(client,
                Executors.newSingleThreadExecutor());
        task = executionSvc.execute(request, context, handler);
    }

    public HttpResponse getHttpResponse() throws InterruptedException, ExecutionException {
        return task.get();
    }

    @Override
    public void close() throws Exception {
        removeFromRunnersContext();
        task.cancel(true);
        executionSvc.close();
        client.close();
    }
}

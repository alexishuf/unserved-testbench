package br.ufsc.lapesd.unserved.testbench.io_composer.layered;

import br.ufsc.lapesd.unserved.testbench.replication.action.Progress;

import javax.annotation.Nonnull;

public class LayeredProgress implements Progress, Comparable<LayeredProgress> {
    private int completeServices;
    private final int totalServices;
    private int completeEdges;
    private final int totalEdges;

    public LayeredProgress(LayeredProgress other) {
        this(other.getCompleteServices(), other.getTotalServices(), other.getCompleteEdges(),
                other.getTotalEdges());
    }

    public LayeredProgress() {
        completeServices = 0;
        totalServices = 0;
        completeEdges = 0;
        totalEdges = 0;
    }

    public LayeredProgress(int completeServices, int totalServices,
                           int completeEdges,    int totalEdges) {
        this.completeServices = completeServices;
        this.totalServices = totalServices;
        this.completeEdges = completeEdges;
        this.totalEdges = totalEdges;
    }

    public int getCompleteServices() {
        return completeServices;
    }

    public int getTotalServices() {
        return totalServices;
    }

    public int getCompleteEdges() {
        return completeEdges;
    }

    public int getTotalEdges() {
        return totalEdges;
    }

    public LayeredProgress completeServices(int count) {
        this.completeServices += count;
        return this;
    }
    public LayeredProgress completeEdges(int count) {
        this.completeEdges += count;
        return this;
    }

    @Override
    public String toString() {
        return String.format("s=%d/%d;e=%d/%d", getCompleteServices(), getTotalServices(),
                getCompleteEdges(), getTotalEdges());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LayeredProgress that = (LayeredProgress) o;

        if (completeServices != that.completeServices) return false;
        if (totalServices != that.totalServices) return false;
        if (completeEdges != that.completeEdges) return false;
        return totalEdges == that.totalEdges;
    }

    @Override
    public int hashCode() {
        int result = completeServices;
        result = 31 * result + totalServices;
        result = 31 * result + completeEdges;
        result = 31 * result + totalEdges;
        return result;
    }

    @Override
    public int compareTo(@Nonnull LayeredProgress rhs) {
        int cmp;
        cmp = Integer.compare(getCompleteServices(), rhs.getCompleteServices());
        if (cmp != 0) return cmp;
        cmp = Integer.compare(getTotalServices(), rhs.getTotalServices());
        if (cmp != 0) return cmp;
        cmp = Integer.compare(getCompleteEdges(), rhs.getCompleteEdges());
        if (cmp != 0) return cmp;
        cmp = Integer.compare(getTotalEdges(), rhs.getTotalEdges());
        if (cmp != 0) return cmp;
        return 0;
    }
}

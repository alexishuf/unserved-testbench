package br.ufsc.lapesd.unserved.testbench.util;

import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;

import javax.annotation.Nonnull;
import java.util.Set;
import java.util.stream.Stream;

public abstract class AbstractTransitiveClosureGetter implements TransitiveClosureGetter {
    protected final Property predicate;
    protected final boolean backward;

    public AbstractTransitiveClosureGetter(Property predicate, boolean backward) {
        this.predicate = predicate;
        this.backward = backward;
    }

    @Override
    public Property getPredicate() {
        return predicate;
    }

    @Override
    public boolean isBackward() {
        return backward;
    }

    @Nonnull
    @Override
    public Set<Resource> getStrictClosureSet(@Nonnull Resource start) {
        Set<Resource> set = getClosureSet(start);
        set.remove(start);
        return set;
    }

    @Nonnull
    @Override
    public Stream<Resource> getStrictClosureStream(@Nonnull Resource start) {
        return getClosureStream(start).filter(r -> !r.equals(start));
    }
}

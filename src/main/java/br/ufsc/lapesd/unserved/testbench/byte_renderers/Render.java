package br.ufsc.lapesd.unserved.testbench.byte_renderers;

import java.nio.charset.Charset;

/**
 * Result of a {@link Renderer}
 */
public interface Render {
    byte[] getBytes();
    boolean isBinary();
    boolean isString();
    Charset getCharset();

    enum Type {
        STRING,
        BINARY
    }
}

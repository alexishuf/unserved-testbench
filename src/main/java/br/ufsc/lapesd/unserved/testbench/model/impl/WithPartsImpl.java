package br.ufsc.lapesd.unserved.testbench.model.impl;

import br.ufsc.lapesd.unserved.testbench.iri.Unserved;
import br.ufsc.lapesd.unserved.testbench.model.Part;
import br.ufsc.lapesd.unserved.testbench.model.Variable;
import br.ufsc.lapesd.unserved.testbench.model.WithParts;
import org.apache.jena.enhanced.EnhGraph;
import org.apache.jena.graph.Node;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.rdf.model.impl.ResourceImpl;

import java.util.*;
import java.util.stream.Collectors;

public class WithPartsImpl extends ResourceImpl implements WithParts {
    public WithPartsImpl(Node n, EnhGraph m) {
        super(n, m);
    }

    @Override
    public List<Part> getParts() {
        ArrayList<Part> list = new ArrayList<>();
        StmtIterator it = getModel().listStatements(this, Unserved.part, (RDFNode) null);
        while (it.hasNext()) {
            RDFNode object = it.next().getObject();
            if (!object.isResource()) continue;
            Resource resource = object.asResource();
            if (!resource.canAs(Part.class)) continue;
            list.add(resource.as(Part.class));
        }
        return list;
    }

    @Override
    public List<Variable> getVariables() {
        return getParts().stream().map(Part::getVariable).collect(Collectors.toList());
    }

    @Override
    public Set<Part> getPartsRecursive() {
        Set<Part> parts = new HashSet<>();
        Set<Variable> vars = new HashSet<>();

        Stack<Variable> stack = new Stack<>();
        parts.addAll(getParts());
        parts.stream().map(Part::getVariable).forEach(stack::push);
        while (!stack.isEmpty()) {
            Variable var = stack.pop();
            if (vars.contains(var)) continue;
            vars.add(var);
            for (Part part : var.getParts()) {
                Variable partVariable = part.getVariable();
                if (parts.contains(part) || vars.contains(partVariable)) continue;
                parts.add(part);
                stack.push(partVariable);
            }
        }
        return parts;
    }

    @Override
    public Set<Variable> getVariablesRecursive() {
        return getPartsRecursive().stream().map(Part::getVariable).collect(Collectors.toSet());
    }
}

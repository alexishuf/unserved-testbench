package br.ufsc.lapesd.unserved.testbench.restdesc_pragproof.n3;

import ch.ethz.inf.vs.semantics.parser.elements.*;
import ch.ethz.inf.vs.semantics.parser.visitors.N3BaseElementVisitor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Remove any {@link Formula} or {@link RDFResource} (if target has only one statement)
 * whose N3Model is isomorphic to the model of the provided target. Nested formulae are
 * taken into account in determining isomorphism.
 */
public class RemoveFormulaTransformer {
    private final N3Document result;
    private final Formula target;
    private final N3Model targetModel;
    private List<N3Element> removed = new ArrayList<>();

    public RemoveFormulaTransformer(N3Document document, Formula target, N3Model targetModel) {
        this.target = target;
        this.targetModel = targetModel;
        result = (N3Document) document.accept(new Transformer());
    }

    public List<N3Element> getRemoved() {
        return removed;
    }

    public N3Document getResult() {
        return result;
    }

    private class Matcher extends N3BaseElementVisitor<Boolean> {
        private N3Model target;

        public Matcher(N3Model target) {
            this.target = target;
        }

        @Override
        public Boolean visitN3Document(N3Document n3Document) {
            return target.isIsomorphicWith(new N3Model(n3Document));
        }

        @Override
        public Boolean visitStatements(Statements statements) {
            Formula fake = new Formula();
            fake.addAll(statements);
            return vistFormula(fake);
        }

        @Override
        public Boolean vistFormula(Formula statements) {
            return target.isIsomorphicWith(new N3Model(statements));
        }

        @Override
        public Boolean visitRDFResource(RDFResource rdfResource) {
            Formula fake = new Formula();
            fake.add(rdfResource);
            return vistFormula(fake);
        }
    }

    private class Transformer extends N3BaseElementVisitor<Object> {
        @Override
        public Object visitChildern(N3Element node) {
            if (node.getChildern() == null) {
                return node;
            }
            throw new UnsupportedOperationException();
        }

        @Override
        public Object visitBlankNodePropertyList(BlankNodePropertyList verbObjects) {
            return visitCollection(new BlankNodePropertyList() ,verbObjects);
        }

        @Override
        public Object vistClassification(Classification classification) {
            throw new UnsupportedOperationException();
            //FIXME library does not allow this to be implemented, fix after we change/fix the parser
        }

        @Override
        public Object vistFormula(Formula statements) {
            if (statements.accept(new Matcher(targetModel))) {
                removed.add(statements);
                return null;
            }
            return visitCollection(new Formula(), statements);
        }

        @Override
        public Object visitObjectCollection(ObjectCollection objects) {
            return visitCollection(new ObjectCollection(), objects);
        }

        @Override
        public Object visitObjectList(ObjectList objects) {
            return visitCollection(new ObjectList(), objects);
        }

        @Override
        public Object visitStatements(Statements statements) {
            return visitCollection(new Statements(), statements);
        }

        @Override
        public Object visitRDFResource(RDFResource rdfResource) {
            if (target.size() == 1) {
                if (rdfResource.accept(new Matcher(targetModel))) {
                    removed.add(rdfResource);
                    return null;
                }
            }
            N3Element.Subject subject;
            subject = (N3Element.Subject) ((N3Element) rdfResource.subject).accept(this);
            ArrayList<VerbObject> vos = rdfResource.verbObjects.stream()
                    .map(vo -> (VerbObject) vo.accept(this))
                    .collect(Collectors.toCollection(ArrayList::new));
            return new RDFResource(subject, vos);
        }

        @Override
        public Object visitVerbObject(VerbObject verbObject) {
            Iterator<? extends N3Element> it = verbObject.getChildern().iterator();
            N3Element.Verb verb = (N3Element.Verb) it.next().accept(this);
            N3Element.Object object = (N3Element.Object) it.next().accept(this);
            assert !it.hasNext();
            return new VerbObject(verb, object);
        }

        @Override
        public Object visitN3Document(N3Document n3Document) {
            N3Document transformed = new N3Document();
            n3Document.getPrefixes().values().forEach(transformed::addPrefix);
            transformed.importStatements((Statements) n3Document.statements.accept(this));
            return transformed;
        }

        @SuppressWarnings("unchecked")
        private <T> Object visitCollection(Collection<T> transformed, Collection<T> source) {
            source.stream()
                    .map(child -> (T)((N3Element)child).accept(this))
                    .filter(result -> result != null)
                    .forEach(transformed::add);
            return transformed;
        }
    }
}

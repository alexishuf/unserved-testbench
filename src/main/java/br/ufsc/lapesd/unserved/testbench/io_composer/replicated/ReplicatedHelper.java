package br.ufsc.lapesd.unserved.testbench.io_composer.replicated;

import br.ufsc.lapesd.unserved.testbench.composer.Composition;
import br.ufsc.lapesd.unserved.testbench.composer.impl.ReplicationCompositionWithPath;
import br.ufsc.lapesd.unserved.testbench.io_composer.impl.IOComposerInput;
import br.ufsc.lapesd.unserved.testbench.io_composer.impl.IOComposerInputDelta;
import br.ufsc.lapesd.unserved.testbench.io_composer.state.*;
import br.ufsc.lapesd.unserved.testbench.replication.ReplicatedExecution;
import br.ufsc.lapesd.unserved.testbench.replication.action.ReplicationContainer;
import com.google.common.base.Preconditions;
import es.usc.citius.hipster.model.Node;
import org.apache.jena.rdf.model.Resource;

import javax.annotation.Nonnull;

public class ReplicatedHelper {
    public static <A, NodeType extends Node<A, State, NodeType>>  ExecutionPath<Action>
    toFinePath(@Nonnull Node<A, State, NodeType> goalNode) {
        ExecutionPath<Action> path = new ExecutionPath<>();
        Node<A, State, NodeType> node = goalNode.previousNode();
        while (node != null) {
            StateActions actions = node.state().getActions();
            path.add(actions.getCopies());
            for (MessagePairAction pair : actions.getMessagePairs())
                path.add(new MessagePairAction(pair.getAntecedent(), pair.getConsequent()));
            for (MessagePairAction pair : actions.getMessagePairs())
                path.add(pair.getAssignments());

            node = node.previousNode();
        }
        return path;
    }

    public static ExecutionPath<Action> firstReplicationSlice(ExecutionPath<Action> path) {
        ExecutionPath<Action> slice = new ExecutionPath<>();
        for (Action a : path) {
            slice.add(a);
            if (a instanceof MessagePairAction) {
                assert ((MessagePairAction) a).getAssignments().asList().isEmpty();
                break;
            }
        }
        return slice;
    }

    public static ReplicationCompositionWithPath<Action>
    createReplicationComposition(@Nonnull IOComposerInput ci,
                                 @Nonnull ReplicatedExecution execution,
                                 @Nonnull ExecutionPath<Action> actionPath) {
        return createReplicationComposition(ci, execution, actionPath,
                ReplicationCompositionWithPath::new);
    }

    @FunctionalInterface
    public interface CompositionFactory<A> {
        ReplicationCompositionWithPath<Action> create(@Nonnull ReplicationContainer container,
                                                      @Nonnull IOComposerInput composerInput,
                                                      @Nonnull ExecutionPath<A> path);
    }

    public static ReplicationCompositionWithPath<Action>
    createReplicationComposition(@Nonnull IOComposerInput ci,
                                 @Nonnull ReplicatedExecution execution,
                                 @Nonnull ExecutionPath<Action> actionPath,
                                 @Nonnull CompositionFactory<Action> factory) {
        Preconditions.checkArgument(actionPath.size() > 0);
        UnservedPSequenceWriter writer = new UnservedPSequenceWriter();
        ExecutionPath<Action> slice = firstReplicationSlice(actionPath);

        slice.forEach(writer::append);

        Composition realComposition;
        realComposition = writer.toIOComposition(ci.getSkolemizedUnion(), ci.getSkolemizerMap());
        Resource root = realComposition.getWorkflowRoot();

        ReplicationContainer container;
        container = new ReplicationContainer(realComposition.takeModel(), root, execution);
        ReplicationCompositionWithPath<Action> comp = factory.create(container,
                new IOComposerInputDelta(ci), slice);
        comp.setLast(slice.size() == actionPath.size());
        return comp;
    }


}

package br.ufsc.lapesd.unserved.testbench.util;

import br.ufsc.lapesd.unserved.testbench.input.RDFInput;
import br.ufsc.lapesd.unserved.testbench.input.RDFInputModel;
import br.ufsc.lapesd.unserved.testbench.util.jena.ModelReference;
import br.ufsc.lapesd.unserved.testbench.util.jena.UncloseableGraph;
import com.google.common.base.Preconditions;
import org.apache.jena.graph.Graph;
import org.apache.jena.graph.compose.MultiUnion;
import org.apache.jena.rdf.model.*;
import org.apache.jena.riot.RDFFormat;
import org.apache.jena.sparql.graph.UnmodifiableGraph;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.ListIterator;
import java.util.stream.Collectors;

/**
 * Used to store an updatable Model as two RDFInputs, one larger and one smaller with updates only.
 *
 * The unified model cannot be directly accessed, but operations to add/remove triples can be
 * done, affecting primarily only the secondary RDFInput's model. This is useful if the
 * RDFInput.toFile() will be used: unecessary serialization due to inputs is avoided.
 *
 * Instances can be constructed from one another, with copy-on-write semantics.
 *
 * <b>Note: The class is final due to hasty copy-on-write implementation</b>
 */
public final class UpdatableRDFInputs implements AutoCloseable {
    private static Logger logger = LoggerFactory.getLogger(UpdatableRDFInputs.class);

    private final ArrayList<LayerReference> layers;
    private RDFFormat format;
    private boolean closed = false;
    private Model transientModel = null;
    private ModelReference modelReference = null;

    private class LayerImplementation {
        private RDFInput input;
        private Model model;
        private long users = 1;
        private boolean dirty;

        public LayerImplementation(@Nonnull Model model) {
            Preconditions.checkNotNull(model);
            this.model = model;
            this.input = null;
            this.dirty = true;
        }

        /**
         * Deep copy constructor.
         *
         * @param other source LayerImplementation
         */
        public LayerImplementation(@Nonnull LayerImplementation other) {
            Preconditions.checkNotNull(other);
            model = ModelFactory.createDefaultModel();
            model.add(other.getModel());
            dirty = true;
            input = null;
        }

        public RDFInput getInput() {
            checkNotClosed();
            if (dirty) {
                input = new RDFInputModel(model, format);
                dirty = false;
            }
            return input;
        }

        public Model getModel() {
            checkNotClosed();
            return model;
        }

        public void setDirty() {
            checkNotClosed();
            dirty = true;
        }

        public void notifyNewUser() {
            checkNotClosed();
            ++users;
        }

        public boolean isShared() {
            checkNotClosed();
            return users > 1;
        }

        public void close() {
            --users;
            if (users == 0) {
                if (input != null) {
                    try {
                        input.close();
                    } catch (IOException e) {
                        logger.error("Failed to close {}. Ignoring", input, e);
                    }
                }
                model.close();
            }
        }

        private void checkNotClosed() {
            Preconditions.checkState(users > 0, "Already closed LayerImplementation");
        }
    }

    private class LayerReference {
        private LayerImplementation implementation;

        public LayerReference(LayerImplementation implementation) {
            this.implementation = implementation;
        }
        public LayerReference(LayerReference reference) {
            this.implementation = reference.implementation;
            reference.implementation.notifyNewUser();
        }

        public void acquireWritingRights() {
            if (!implementation.isShared()) return;
            implementation = new LayerImplementation(implementation);
        }

        public RDFInput getInput() {
            return implementation.getInput();
        }

        public Model getModel() {
            return implementation.getModel();
        }

        public void setDirty() {
            Preconditions.checkState(!implementation.isShared());
            implementation.setDirty();
        }

        public void close() {
            implementation.close();
        }
    }

    public UpdatableRDFInputs(@Nonnull Collection<Model> models, @Nonnull RDFFormat format)
            throws IOException {
        this.format = format;
        this.layers = new ArrayList<>(models.size()+1);
        models.forEach(m -> this.layers.add(new LayerReference(new LayerImplementation(m))));
        layers.add(new LayerReference(new LayerImplementation(ModelFactory.createDefaultModel())));
    }

    public UpdatableRDFInputs(UpdatableRDFInputs other) {
        this.format = other.format;
        this.layers = new ArrayList<>(other.layers.size());
        other.layers.forEach(otherLayer -> layers.add(new LayerReference(otherLayer)));
    }

    public Collection<RDFInput> getLayers() {
        Preconditions.checkState(!closed, "Object closed");
        return layers.stream().map(LayerReference::getInput).collect(Collectors.toList());
    }

    /**
     * Gets a model representing the union of all layers. The returned model is closed by
     * UpdatableRDFInputs whenever a change is performed. calls to this method will only return
     * the same object if no change happened.
     *
     * @return A model for the union of all layers. Ownership is not transferred.
     */
    @Nonnull private Model getTransientModel() {
        if (transientModel == null) {
            List<Graph> graphs = layers.stream().map(LayerReference::getModel).map(Model::getGraph)
                    .collect(Collectors.toList());
            transientModel = ModelFactory.createModelForGraph(new UncloseableGraph(
                    new UnmodifiableGraph(new MultiUnion(graphs.iterator()))));
        }
        return transientModel;
    }

    /**
     * Gets an unmodifiable model representing the updated inputs.
     *
     * @return unmodifiable model.
     */
    public @Nonnull Model getModel() {
        if (modelReference == null) modelReference = new ModelReference(this::getTransientModel);
        return modelReference;
    }

    private void invalidateModel() {
        if (transientModel != null) {
            transientModel.close();
            transientModel = null;
        }
    }

    public boolean contains(Statement statement) {
        Preconditions.checkState(!closed, "Object closed");
        return layers.stream().map(LayerReference::getModel).filter(m -> m.contains(statement))
                .findAny().isPresent();
    }

    public void add(Statement statement) {
        Preconditions.checkState(!closed, "Object closed");
        if (!contains(statement)) {
            LayerReference last = lastLayer();
            last.acquireWritingRights();
            last.getModel().add(statement);
            last.setDirty();
            invalidateModel();
        }
    }

    public void remove(Statement statement) {
        removeAll(statement.getSubject(), statement.getPredicate(), statement.getObject());
    }

    public void removeAll(Resource subject, Property predicate, RDFNode object) {
        Preconditions.checkState(!closed, "Object closed");

        boolean changed = false;
        ListIterator<LayerReference> it = layers.listIterator(layers.size());
        while (it.hasPrevious()) {
            LayerReference layer = it.previous();
            if (layer.getModel().contains(subject, predicate, object)) {
                layer.acquireWritingRights();
                layer.getModel().removeAll(subject, predicate, object);
                layer.setDirty();
                changed = true;
            }
        }

        if (changed)
            invalidateModel();
    }

    @Override
    public void close() throws Exception {
        if (closed) return;
        closed = true;
        invalidateModel();
        if (modelReference != null) modelReference.close();
        layers.forEach(LayerReference::close);
    }

    private LayerReference lastLayer() {
        return layers.get(layers.size()-1);
    }
}

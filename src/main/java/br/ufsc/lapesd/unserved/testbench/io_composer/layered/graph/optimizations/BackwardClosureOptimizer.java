package br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.optimizations;

import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.*;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.ProviderSet.Assignment;
import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static br.ufsc.lapesd.unserved.testbench.io_composer.layered.LayeredServiceSetGraphIOComposer.graphOptimizationMem;

public class BackwardClosureOptimizer implements Function<LayeredServiceGraph, LayeredServiceGraph> {
    @Override
    public LayeredServiceGraph apply(@Nonnull LayeredServiceGraph g) {
        Set<Node> closure = getClosure(g);
        BiMap<Node, Node> nnMap = HashBiMap.create();
        List<ServiceLayer> layers = new ArrayList<>(g.getLayers().size());
        Map<Node, ProviderSet> providerSets = new HashMap<>();
        Map<Node, Integer> nodeLayer = new HashMap<>();
        for (int i = 0; i < g.getLayers().size(); i++) {
            ServiceLayer layer = new ServiceLayer(g.getLayers().get(i).getNodes().stream()
                    .filter(closure::contains)
                    .map(n -> nnMap.computeIfAbsent(n, this::copy))
                    .collect(Collectors.toList()));
            layers.add(layer);
            for (Node nn : layer.getNodes()) {
                nodeLayer.put(nn, i);
                Node n = nnMap.inverse().get(nn);
                ProviderSet ps = g.getProviderSet(n).without(new ProviderSet.FilterMap() {
                    @Override
                    public @Nullable Assignment mapAssignment(@Nonnull Assignment a) {
                        Node nn = nnMap.getOrDefault(a.node, null);
                        return nn == null ? null : new Assignment(a.target, nn, a.output);
                    }
                });
                providerSets.put(nn, ps);
            }
        }

        LayeredServiceGraph result = new LayeredServiceGraph(layers, providerSets,
                (StartNode) Objects.requireNonNull(nnMap.get(g.getStartNode())),
                (EndNode) Objects.requireNonNull(nnMap.get(g.getEndNode())),
                nodeLayer);
        if (g.getTimes() != null) {
            result.setTimes(g.getTimes());
            g.getTimes().takeMemSample(graphOptimizationMem);
        }
        return result;
    }

    @Nonnull
    private Node copy(@Nonnull Node node) {
        if (node instanceof StartNode) {
            return new StartNode(node.getOutputs(), node.getPostConditions());
        } else if (node instanceof EndNode) {
            return new EndNode(node.getInputs(), node.getPreConditions());
        }
        assert node instanceof ServiceNode;
        return new ServiceNode((ServiceNode) node);
    }

    @Nonnull
    private Set<Node> getClosure(@Nonnull LayeredServiceGraph g) {
        Set<Node> visited = new HashSet<>();
        Queue<Node> queue = new LinkedList<>();
        queue.add(g.getEndNode());
        while (!queue.isEmpty()) {
            Node node = queue.remove();
            if (visited.contains(node))
                continue;
            visited.add(node);
            g.getProviderSet(node).stream().forEach(queue::add);
        }
        return visited;
    }
}

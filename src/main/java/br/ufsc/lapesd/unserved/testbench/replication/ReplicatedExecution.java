package br.ufsc.lapesd.unserved.testbench.replication;

import br.ufsc.lapesd.unserved.testbench.io_composer.IOComposerTimes;
import br.ufsc.lapesd.unserved.testbench.process.Interpreter;
import br.ufsc.lapesd.unserved.testbench.process.data.DataContext;
import br.ufsc.lapesd.unserved.testbench.process.data.DataContextDiff;

public interface ReplicatedExecution extends AutoCloseable {
    ReplicatedExecution duplicate();
    DataContext getDataContext();

    void putDiff(DataContextDiff diff);

    IOComposerTimes getTimes();

    void resumeInterpretation() throws ReplicatedExecutionException;
    default void resumeInterpretation(Interpreter interpreter) throws ReplicatedExecutionException {
        resumeInterpretation();
    }
}

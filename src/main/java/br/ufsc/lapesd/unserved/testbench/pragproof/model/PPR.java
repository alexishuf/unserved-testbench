package br.ufsc.lapesd.unserved.testbench.pragproof.model;

import br.ufsc.lapesd.unserved.testbench.BackgroundProvider;
import br.ufsc.lapesd.unserved.testbench.input.RDFInput;
import br.ufsc.lapesd.unserved.testbench.util.ResourcesBackground;
import org.apache.commons.lang3.tuple.ImmutablePair;

import java.util.Collections;

@BackgroundProvider(vocab = "https://alexishuf.bitbucket.io/2016/04/pragproof-rules.n3#")
public class PPR extends ResourcesBackground {
    public PPR() {
        super(Collections.singletonList(ImmutablePair.of("pragproof-rules.n3", RDFInput.N3)),
                true);
    }

    public static String IRI = "https://alexishuf.bitbucket.io/2016/04/pragproof-rules.n3";
    public static String PREFIX = IRI + "#";
}

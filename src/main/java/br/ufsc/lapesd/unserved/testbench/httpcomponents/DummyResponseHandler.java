package br.ufsc.lapesd.unserved.testbench.httpcomponents;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

public class DummyResponseHandler implements ResponseHandler<HttpResponse> {
    @Override
    public HttpResponse handleResponse(HttpResponse response)
            throws ClientProtocolException, IOException {
        HttpEntity entity = response.getEntity();
        byte[] bytes = EntityUtils.toByteArray(entity);
        ByteArrayEntity copy = new ByteArrayEntity(bytes);
        copy.setContentEncoding(entity.getContentEncoding());
        copy.setContentType(entity.getContentType());
        EntityUtils.updateEntity(response, copy);
        return response;
    }
}

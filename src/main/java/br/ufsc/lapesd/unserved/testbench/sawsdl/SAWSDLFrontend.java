package br.ufsc.lapesd.unserved.testbench.sawsdl;

import br.ufsc.lapesd.unserved.testbench.UnservedTranslator;
import br.ufsc.lapesd.unserved.testbench.UnservedTranslatorException;
import br.ufsc.lapesd.unserved.testbench.input.Input;
import br.ufsc.lapesd.unserved.testbench.input.RDFInput;
import br.ufsc.lapesd.unserved.testbench.input.RDFInputModel;
import br.ufsc.lapesd.unserved.testbench.iri.Unserved;
import br.ufsc.lapesd.unserved.testbench.iri.UnservedX;
import br.ufsc.lapesd.unserved.testbench.model.Message;
import com.google.common.base.Preconditions;
import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import org.apache.commons.io.IOUtils;
import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.datatypes.xsd.impl.XMLLiteralType;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;
import org.apache.jena.riot.RDFFormat;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.XSD;
import org.w3c.dom.*;

import javax.xml.namespace.NamespaceContext;
import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class SAWSDLFrontend implements UnservedTranslator {
    private static QName modelReferenceAttr = new QName("http://www.w3.org/ns/sawsdl", "modelReference");
    private static String wsdl11Ns = "http://schemas.xmlsoap.org/wsdl/";
    private static String wsdlSoapNs = "http://schemas.xmlsoap.org/wsdl/soap/";
    private static String sawsdlNs = "http://www.w3.org/ns/sawsdl";
    private static String xsdNs = "http://www.w3.org/2001/XMLSchema";

    @Override
    public RDFInput compile(List<Input> descriptions) throws UnservedTranslatorException {
        Preconditions.checkArgument(descriptions.stream().allMatch(i -> i != null), "Null Inputs");
        checkMediaTypes(descriptions);
        Model model = ModelFactory.createDefaultModel();
        model.setNsPrefix(Unserved.PREFIX_SHORT, Unserved.PREFIX);
        model.setNsPrefix(UnservedX.PREFIX_SHORT, UnservedX.PREFIX);
        model.setNsPrefix(UnservedX.WSDL.PREFIX_SHORT, UnservedX.WSDL.PREFIX);
        model.setNsPrefix(UnservedX.XSD.PREFIX_SHORT, UnservedX.XSD.PREFIX);

        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            factory.setNamespaceAware(true);
            factory.setXIncludeAware(true);
            DocumentBuilder db = factory.newDocumentBuilder();
            for (Input input : descriptions) {
                Document doc = db.parse(input.toFile());
                if (qName(doc.getDocumentElement()).equals(new QName(wsdl11Ns, "definitions")))
                    compileWSDL11(model, input, doc.getDocumentElement());
                else
                    throw new UnservedTranslatorException("Only WSDL 1.1 is supported");
            }
        } catch (Exception e) {
            throw new UnservedTranslatorException(e);
        }

        return new RDFInputModel(model, RDFFormat.TURTLE);
    }

    private void compileWSDL11(Model model, Input input, Element definitions)
            throws IOException, XPathExpressionException {
        Resource wsdlFile = addWsdlFile(model, input);
        compileWSDL11(model, wsdlFile, definitions);
    }

    private void compileWSDL11(Model model, Resource wsdlFile, Element definitions)
            throws XPathExpressionException {
        XPath xPath = XPathFactory.newInstance().newXPath();
        xPath.setNamespaceContext(wsdl11NamespaceContext(xPath.getNamespaceContext()));
        String expr;

        NodeList services = definitions.getElementsByTagNameNS(wsdl11Ns, "service");
        for (int i = 0; i < services.getLength(); i++) {
            Element service = (Element) services.item(i);
            NodeList ports = service.getElementsByTagNameNS(wsdl11Ns, "port");
            for (int j = 0; j < ports.getLength(); j++) {
                Element port = (Element)ports.item(j);
                String bindingName = port.getAttributeNode("binding").getValue();

                expr = String.format("//wsdl:binding[@name=\"%s\"]/@type", bindingName);
                String portTypeName = (String) xPath.evaluate(expr, definitions,
                        XPathConstants.STRING);

                expr = String.format("//wsdl:portType[@name=\"%s\"]", portTypeName);
                Element portType = (Element) xPath.evaluate(expr, definitions, XPathConstants.NODE);

                NodeList operations = portType.getElementsByTagNameNS(wsdl11Ns, "operation");
                for (int k = 0; k < operations.getLength(); k++) {
                    Element operation = (Element) operations.item(k);
                    String opName = operation.getAttributeNode("name").getValue();

                    NodeList inputs = operation.getElementsByTagNameNS(wsdl11Ns, "input");
                    if (inputs.getLength() != 1) continue;
                    NodeList outputs = operation.getElementsByTagNameNS(wsdl11Ns, "output");
                    if (outputs.getLength() != 1) continue;

                    addRequestReply(model, wsdlFile, xPath, definitions, service, port, portType,
                                    operation, (Element)inputs.item(0), (Element)outputs.item(0));
                }
            }

        }

    }

    private NamespaceContext wsdl11NamespaceContext(NamespaceContext delegate) {
        BiMap<String, String> map = HashBiMap.create();
        map.put("wsdl", wsdl11Ns);
        map.put("wsdlsoap", wsdlSoapNs);
        map.put("xsd", xsdNs);
        return new NamespaceContext() {
            @Override
            public String getNamespaceURI(String prefix) {
                if (!map.containsKey(prefix))
                    return delegate.getNamespaceURI(prefix);
                return map.get(prefix);
            }

            @Override
            public String getPrefix(String uri) {
                if (!map.inverse().containsKey(uri))
                    return delegate.getPrefix(uri);
                return map.inverse().get(uri);
            }

            @Override
            public Iterator getPrefixes(String uri) {
                if (!map.inverse().containsKey(uri))
                    return delegate.getPrefixes(uri);
                return Collections.singletonList(map.inverse().get(uri)).iterator();
            }
        };
    }

    private void addRequestReply(Model model, Resource wsdlFile, XPath xPath, Element definitions,
                                 Element service, Element port, Element portType, Element operation,
                                 Element input, Element output) throws XPathExpressionException {
        Message req = addMessage(model, wsdlFile, xPath, definitions, UnservedX.WSDL.WsdlRequest,
                                 service, port, portType, operation, input);
        Message rep = addMessage(model, wsdlFile, xPath, definitions, UnservedX.WSDL.WsdlReply,
                                 service, port, portType, operation, output);

        Resource reaction = model.createResource(Unserved.Reaction);
        reaction.addProperty(Unserved.reactionTo, req);
        reaction.addProperty(Unserved.reactionCardinality, Unserved.one);
        rep.addProperty(Unserved.when, reaction);
    }

    private Message addMessage(Model model, Resource wsdlFile, XPath xPath, Element definitions,
                               Resource msgType, Element service, Element port, Element portType,
                               Element op, Element opMessage) throws XPathExpressionException {
        Message msg = model.createResource(Unserved.Message).as(Message.class);
        msg.addProperty(RDF.type, msgType);
        msg.addProperty(UnservedX.WSDL.wsdlPortName, port.getAttribute("name"));
        msg.addProperty(UnservedX.WSDL.wsdlOperationName, op.getAttribute("name"));
        msg.addProperty(UnservedX.WSDL.sourceWsdl, wsdlFile);

        Resource to = model.createResource(Unserved.Variable);
        NodeList addresses = port.getElementsByTagNameNS(wsdlSoapNs, "address");
        if (addresses.getLength() > 0) {
            String location = ((Element) addresses.item(0)).getAttribute("location");
            if (location != null) {
                to.addProperty(Unserved.representation, XSD.anyURI);
                to.addProperty(Unserved.literalValue, ResourceFactory.createTypedLiteral(location,
                        XSDDatatype.XSDanyURI));
            }
        }
        msg.addProperty(Unserved.to, to);
        msg.addProperty(Unserved.from, model.createResource(Unserved.Variable));

        Element msgElt = (Element) xPath.evaluate(String.format("//wsdl:message[@name=\"%s\"]",
                opMessage.getAttribute("message")), definitions, XPathConstants.NODE);
        NodeList parts = msgElt.getElementsByTagNameNS(wsdl11Ns, "part");
        for (int i = 0; i < parts.getLength(); i++) {
            Element partElt = (Element) parts.item(i);
            Resource part = model.createResource(Unserved.Part);
            Resource var = createVariableForPart(model, wsdlFile, xPath, definitions, partElt);
            part.addProperty(Unserved.variable, var);
            msg.addProperty(Unserved.part, part);
        }

        return msg;
    }

    private Resource createVariableForPart(Model model, Resource wsdlFile, XPath xPath,
                                           Element definitions, Element partElt)
            throws XPathExpressionException {
        Resource var = model.createResource(Unserved.Variable);
        getModelReferences(model, partElt).forEach(r -> var.addProperty(Unserved.type, r));
        String typename = partElt.getAttribute("type");
        Element type = (Element) xPath.evaluate(String.format("//*[@name=\"%s\"]", typename),
                definitions, XPathConstants.NODE);
        getModelReferences(model, type).forEach(r -> var.addProperty(Unserved.type, r));
        Resource representation = model.createResource(UnservedX.XSD.XsdInfoset);
        representation.addProperty(UnservedX.XSD.xsdTypeName, typename);
        representation.addProperty(UnservedX.XSD.sourceFile, wsdlFile);
        var.addProperty(Unserved.representation, representation);
        return var;
    }

    private List<Resource> getModelReferences(Model model, Element elt) {
        NamedNodeMap map = elt.getAttributes();
        List<Resource> list = new ArrayList<>();
        for (int i = 0; i < map.getLength(); i++) {
            Attr attr = (Attr) map.item(i);
            if (!attr.getName().equals("modelReference")) continue;
            list.add(model.createResource(attr.getValue()));
        }
        return list;
    }

    private Resource addWsdlFile(Model model, Input input) throws IOException {
        Resource file = model.createResource(UnservedX.WSDL.WsdlFile);
        try (InputStream inputStream = input.createInputStream()) {
            String string = IOUtils.toString(inputStream);
            file.addProperty(UnservedX.WSDL.wsdlContent,
                    ResourceFactory.createTypedLiteral(string, XMLLiteralType.theXMLLiteralType));
        }
        return file;
    }

    private QName qName(Element element) {
        return new QName(element.getNamespaceURI(), element.getLocalName());
    }

    private void checkMediaTypes(List<Input> descriptions) throws UnservedTranslatorException {
        String list = descriptions.stream()
                .filter(i -> !i.getMediaType().equals("application/wsdl+xml"))
                .map(Input::toString).reduce((l, r) -> l + ", " + r).orElse(null);
        if (list != null)
            throw new UnservedTranslatorException("Some getInputs are not wsdl files: " + list);
    }
}

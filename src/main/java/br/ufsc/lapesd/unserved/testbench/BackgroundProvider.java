package br.ufsc.lapesd.unserved.testbench;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Identifies a class as providing the {@link Background} implementation for a given RDF
 * vocabulary.
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface BackgroundProvider {
    String vocab();
}

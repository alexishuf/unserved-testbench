package br.ufsc.lapesd.unserved.testbench.composer;

import br.ufsc.lapesd.unserved.testbench.process.data.DataContext;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;

import javax.annotation.Nonnull;
import javax.annotation.WillClose;

/**
 * The result of a {@link Composer} composition is a workflow in some workflow language
 * that has RDF representation.
 */
public interface Composition extends AutoCloseable {
    /**
     * The null composition represents the absence of work to do. As goal of the composition
     * is already achieved on the input.
     *
     * Creating a {@link DataContext} or calling <code>getWorkflowRoot()</code> of a null
     * composition is an error.
     *
     * @return true iff the composition is null..
     */
    boolean isNull();

    @Nonnull Resource getWorkflowRoot();

    @Nonnull DataContext createDataContext();

    @Override
    void close();

    @WillClose
    Model takeModel();
}

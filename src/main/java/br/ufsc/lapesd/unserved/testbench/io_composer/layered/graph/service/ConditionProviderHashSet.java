package br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service;

import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.Condition;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.lazy.EquivalenceSets;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.lazy.SetNode;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.discovery.NodeOutput;
import br.ufsc.lapesd.unserved.testbench.model.Variable;
import br.ufsc.lapesd.unserved.testbench.util.hipster.SetCoverIterator;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.SetMultimap;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.jena.ext.com.google.common.collect.Sets;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;
import static org.apache.commons.lang3.tuple.ImmutablePair.of;

public class ConditionProviderHashSet extends IOProviderHashSet {
    private int unsatisfiedConditions = 0;
    private final @Nonnull SetMultimap<Condition, Node> conditionProviders = HashMultimap.create();
    private final @Nonnull SetMultimap<ImmutablePair<Condition, Node>, Condition>
            postconditions = HashMultimap.create();

    @Nonnull
    @Override
    public IOProviderHashSet replace(@Nonnull Node old, @Nonnull CombinedJumpNode replacement) {
        super.replace(old, replacement);
        List<Condition> targets = new ArrayList<>();
        for (Condition condition : new ArrayList<>(conditionProviders.keySet())) {
            Set<Node> set = conditionProviders.get(condition);
            if (set.contains(old)) {
                set.remove(old);
                set.add(replacement);
                targets.add(condition);
            }
        }
        for (Condition target : targets) {
            ArrayList<Condition> values = new ArrayList<>(postconditions.get(of(target, old)));
            postconditions.removeAll(of(target, old));
            postconditions.get(of(target, replacement)).addAll(values);
        }
        return this;
    }

    public @Nonnull ConditionProviderHashSet addCondition(@Nonnull Condition condition) {
        if (conditionProviders.containsKey(condition))
            return this;
        conditionProviders.put(condition, null);
        ++unsatisfiedConditions;
        return this;
    }

    public @Nonnull ConditionProviderHashSet add(@Nonnull Condition condition, @Nonnull Node node,
                                                 @Nonnull Condition postcondition) {
        if (conditionProviders.remove(condition, null))
            --unsatisfiedConditions;
        conditionProviders.put(condition, node);
        postconditions.put(of(condition, node), postcondition);
        return this;
    }

    public static class FilterMap extends ProviderSet.FilterMap {
        public static final class ConditionAssociation {
            public @Nonnull Condition target;
            public @Nonnull Node node;
            public @Nonnull Condition output;

            public ConditionAssociation(@Nonnull Condition target, @Nonnull Node node,
                                        @Nonnull Condition output) {
                this.target = target;
                this.node = node;
                this.output = output;
            }

            @Nonnull
            public Condition getTarget() {
                return target;
            }

            @Nonnull
            public Node getNode() {
                return node;
            }

            @Nonnull
            public Condition getOutput() {
                return output;
            }

            @Override
            public String toString() {
                return String.format("%s(%s) -> %s", node, output, target);
            }
        }

        public @Nullable ConditionAssociation mapConditionAssociation(@Nonnull ConditionAssociation a) {
            return a;
        }
    }

    @Override
    public IOProviderHashSet without(@Nonnull ProviderSet.FilterMap f) {
        ConditionProviderHashSet copy = new ConditionProviderHashSet();
        getTargets().stream().map(f::mapTarget).filter(Objects::nonNull).forEach(copy::addTarget);
        getAssignmentsStream().map(f::mapAssignment).filter(Objects::nonNull)
                .forEach(a -> copy.add(a.target, a.node, a.output));

        if (!conditionProviders.isEmpty()) {
            FilterMap myF = f instanceof FilterMap ? (FilterMap) f : new FilterMap();
            getConditions().forEach(cond -> getProviders(cond)
                    .stream().map(node -> myF.mapConditionAssociation(new FilterMap
                            .ConditionAssociation(cond, node, getPostcondition(cond, node))))
                    .filter(Objects::nonNull)
                    .forEach(a -> copy.add(a.target, a.node, a.output)));
        }
        return copy;
    }

    @Override
    public IOProviderHashSet addAll(@Nonnull ProviderSet other) {
        super.addAll(other);
        for (Condition preCond : other.getConditions()) {
            for (Node provider : other.getProviders(preCond)) {
                for (Condition postCond : other.getPostconditions(preCond, provider)) {
                    add(preCond, provider, postCond);
                }
            }
        }
        return this;
    }

    @Nonnull
    @Override
    public Set<Condition> getUnsatisfiedConditions() {
        return conditionProviders.keySet().stream()
                .filter(c -> conditionProviders.containsEntry(c, null)).collect(Collectors.toSet());
    }

    @Nonnull
    @Override
    public Set<Condition> getConditions() {
        return Collections.unmodifiableSet(conditionProviders.keySet());
    }

    @Nonnull
    @Override
    public Set<Node> getProviders(@Nonnull Condition condition) {
        return conditionProviders.get(condition);
    }

    @Nonnull
    @Override
    public Condition getPostcondition(@Nonnull Condition precondition, @Nonnull Node provider) {
        Set<Condition> set = postconditions.get(of(precondition, provider));
        if (set.isEmpty()) throw new NoSuchElementException();
        return set.iterator().next();
    }

    @Nonnull
    @Override
    public Set<Condition> getPostconditions(@Nonnull Condition precondition, @Nonnull Node provider) {
        return postconditions.get(of(precondition, provider));
    }

    @Nonnull
    @Override
    public Stream<Node> stream() {
        return Stream.concat(super.stream(), conditionProviders.values().stream()).distinct();
    }

    @Override
    public boolean isSatisfied() {
        return super.isSatisfied() && unsatisfiedConditions == 0;
    }

    @Nonnull
    @Override
    public Stream<PrecondMap>
    getPreconditionMappings(@Nonnull EquivalenceSets eqSets) {
        SetMultimap<Node, Condition> prov2preCond = HashMultimap.create();
        for (Map.Entry<Condition, Node> e : conditionProviders.entries())
            prov2preCond.put(e.getValue(), e.getKey());

        List<PrecondMap> pcMaps = new ArrayList<>();
        SetMultimap<Set<Condition>, Node> groups = HashMultimap.create();
        for (Node prov : prov2preCond.keySet()) groups.put(prov2preCond.get(prov), prov);
        SetCoverIterator<Condition> setCoverIt = new SetCoverIterator<>(groups.keySet());
        while (setCoverIt.hasNext()) {
            Set<Set<Condition>> sel = setCoverIt.next();
            List<Set<ImmutablePair<Set<Condition>, Node>>> provSets = new ArrayList<>(sel.size());
            for (Set<Condition> preCondSet : sel) {
                Set<ImmutablePair<Set<Condition>, Node>> set = new HashSet<>();
                for (Node provider : groups.get(preCondSet)) set.add(of(preCondSet, provider));
                provSets.add(set);
            }
            candidate:
            for (List<ImmutablePair<Set<Condition>, Node>> preConds2prov : Sets.cartesianProduct(provSets)) {
                PrecondMap pcMap = new PrecondMap(new HashMap<>(), new HashMap<>());
                Map<Condition, Set<ImmutablePair<Condition, NodePostCond>>> rMap = new HashMap<>();
                for (ImmutablePair<Set<Condition>, Node> pair : preConds2prov) {
                    for (Condition preCond : pair.left) {
                        Set<Condition> postConds = postconditions.get(of(preCond, pair.right));
                        if (postConds.size() == 1) {
                            Condition postCond = postConds.iterator().next();
                            if (!fillPrecondMap(pcMap, postCond, preCond, pair.right)) {
                                continue candidate;
                            }
                        } else {
                            assert !rMap.containsKey(preCond);
                            Set<ImmutablePair<Condition, NodePostCond>> set = new HashSet<>();
                            for (Condition postCond : postConds)
                                set.add(of(preCond, new NodePostCond(pair.right, postCond)));
                            rMap.put(preCond, set);
                        }
                    }
                }
                remainder_candidate:
                for (List<ImmutablePair<Condition, NodePostCond>> precond2prov : Sets.cartesianProduct(new ArrayList<>(rMap.values()))) {
                    PrecondMap pcMap2 = new PrecondMap(pcMap);
                    for (ImmutablePair<Condition, NodePostCond> p : precond2prov) {
                        if (!fillPrecondMap(pcMap2, p.right.postCond, p.left, p.right.node))
                            continue remainder_candidate;
                    }
                    pcMaps.add(pcMap2);
                    continue candidate;
                }
            }
        }

        return pcMaps.stream();
    }

    boolean fillPrecondMap(@Nonnull PrecondMap pcMap, @Nonnull Condition postCond,
                           @Nonnull Condition preCond, @Nonnull Node provider) {
        Map<Variable, Variable> partial;
        if (provider instanceof JumpNode) {
            partial = new HashMap<>();
            for (Variable var : preCond.getVariables()) partial.put(var, var);
        } else {
            partial = preCond.bindFrom(postCond, true);
        }
        Map<Variable, NodeOutput> inMap = new HashMap<>();
        for (Map.Entry<Variable, Variable> in2out : partial.entrySet()) {
            if (!provider.getOutputs().contains(in2out.getValue()))
                continue; //do not map to inputs
            NodeOutput nodeOutput = new NodeOutput(provider, in2out.getValue());
            NodeOutput prev = inMap.get(in2out.getKey());
            if (prev != null) {
                if (!prev.equals(nodeOutput))
                    return false; //conflicting providers selected
            } else {
                inMap.put(in2out.getKey(), nodeOutput);
            }
        }

        pcMap.inMap.putAll(inMap);
        pcMap.preCondMap.put(preCond, new NodePostCond(provider, postCond));
        return true;
    }

    @Nonnull
    @Override
    public EquivalenceSets getEquivalenceSets(@Nonnull PrecondMap map,
                                              @Nullable EquivalenceSets parent) {
        parent = parent == null ? new EquivalenceSets() : parent;
        EquivalenceSets mine = new EquivalenceSets();
        //map each prov. vars that appears in postconditions to to a set of vars in getTargets()
        SetMultimap<Variable, Variable> provVars2Mine = HashMultimap.create();
        for (Condition preCond : conditionProviders.keySet()) {
            Condition postCond = map.preCondMap.get(preCond).postCond;
            Map<Variable, Variable> binding = preCond.bindFrom(postCond, false);
            for (Map.Entry<Variable, Variable> e : binding.entrySet()) {
                NodeOutput no = map.inMap.getOrDefault(e.getKey(), null);
                if (no != null && no.node instanceof JumpNode)
                    provVars2Mine.put(no.getOutput().getVariable(), e.getKey());
                if (!isBoundToJumpNodeDelegate(no, e.getValue()))
                    provVars2Mine.put(e.getValue(), e.getKey());
            }
        }
        //expand provVars2Mine with prent equivalences
        for (Variable provVar : provVars2Mine.keySet()) {
            Set<Variable> additional = new HashSet<>();
            for (Variable variable : provVars2Mine.get(provVar))
                additional.addAll(parent.get(variable));
            provVars2Mine.get(provVar).addAll(additional);
        }
        //build a multimap from equiv. set => prov. var
        SetMultimap<Set<Variable>, Variable> eqSet2provVars = HashMultimap.create();
        for (Variable provVar : provVars2Mine.keySet())
            eqSet2provVars.put(provVars2Mine.get(provVar), provVar);
        //feed the newly found eq. sets of prov. vars into mine
        for (Set<Variable> eqSet : eqSet2provVars.keySet())
            mine.setEquivalent(eqSet2provVars.get(eqSet));
        return mine;
    }

    @Override
    public void bindConstants(@Nonnull Map<Variable, NodeOutput> fullMap,
                              @Nonnull SetNode successor, @Nonnull EquivalenceSets equivalenceSets,
                              @Nonnull Set<IndexedNode> predecessors) {
        SetMultimap<Node, IndexedNode> splits = HashMultimap.create();
        for (IndexedNode iNode : predecessors) splits.put(iNode.node, iNode);
        splits.keySet().stream().filter(k -> splits.get(k).size() <= 1).collect(toList())
                .forEach(splits::removeAll);
        for (Node rawPredecessor : splits.keySet()) {
            for (Map.Entry<Condition, Node> e : conditionProviders.entries()) {
                if (!e.getValue().equals(rawPredecessor)) continue;
                Condition preCond = e.getKey();
                Condition postCond = getPostcondition(preCond, e.getValue());
                Map<Variable, Variable> binding = preCond.bindFrom(postCond, false, true);
                if (binding.isEmpty()) continue;
                for (Variable var : preCond.getVariables()) {
                    NodeOutput no = fullMap.getOrDefault(var, null);
                    if (no != null && no.node.equals(rawPredecessor)) {
                        for (IndexedNode iNode : splits.get(rawPredecessor)) {
                            if (iNode.index != no.output.index) continue;
                            for (Map.Entry<Variable, Variable> e2 : binding.entrySet())
                                iNode.bindInput(e2.getValue(), e2.getKey());
                        }
                    }
                }
            }
        }
    }

    private boolean isBoundToJumpNodeDelegate(NodeOutput no, Variable provVar) {
        return no != null && (no.node instanceof CombinedJumpNode)
                && ((CombinedJumpNode)no.node).getDelegate().getOutputs().contains(provVar);
    }

    @Override
    public boolean satisfyPreconditions(Collection<Node> providers) {
        return conditionProviders.keySet().stream().allMatch(c
                -> providers.stream().anyMatch(p -> conditionProviders.get(c).contains(p)));
    }
}

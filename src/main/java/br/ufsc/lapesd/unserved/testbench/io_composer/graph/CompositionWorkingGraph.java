package br.ufsc.lapesd.unserved.testbench.io_composer.graph;

import br.ufsc.lapesd.unserved.testbench.io_composer.state.ExecutionPath;
import br.ufsc.lapesd.unserved.testbench.process.data.DataContextDiff;
import es.usc.citius.hipster.graph.HipsterDirectedGraph;
import es.usc.citius.hipster.graph.HipsterMutableGraph;

import javax.annotation.Nonnull;

public interface CompositionWorkingGraph<V, E, A> extends HipsterDirectedGraph<V, E>,
                                                HipsterMutableGraph<V, E> {

    /**
     * Applies the {@link DataContextDiff} changes to the composition graph, directly.
     *
     * The changes can be reverted using the {@link ChangeSet} returned.
     *
     * @param diff {@link DataContextDiff} to apply
     * @param path execution path that yielded the diff
     * @return A {@link ChangeSet} for later reverting
     */
    @Nonnull
    ChangeSet applyDiff(@Nonnull DataContextDiff diff, ExecutionPath<A> path);

    /**
     * Gets a new instance with same content but that is independent from
     * this instance (changes on it will not affect this instance).
     *
     * @return A new {@link CompositionWorkingGraph} with the same contents
     */
    CompositionWorkingGraph<V, E, A> duplicate();
}

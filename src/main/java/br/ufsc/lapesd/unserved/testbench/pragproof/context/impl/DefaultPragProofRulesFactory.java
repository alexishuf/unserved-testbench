package br.ufsc.lapesd.unserved.testbench.pragproof.context.impl;

import br.ufsc.lapesd.unserved.testbench.input.RDFInput;
import br.ufsc.lapesd.unserved.testbench.input.RDFInputFile;
import br.ufsc.lapesd.unserved.testbench.iri.Unserved;
import br.ufsc.lapesd.unserved.testbench.iri.UnservedP;
import br.ufsc.lapesd.unserved.testbench.model.Message;
import br.ufsc.lapesd.unserved.testbench.pragproof.context.PragProofRules;
import br.ufsc.lapesd.unserved.testbench.pragproof.context.PragProofRulesFactory;
import br.ufsc.lapesd.unserved.testbench.pragproof.model.PPO;
import br.ufsc.lapesd.unserved.testbench.util.jena.UncloseableGraph;
import org.apache.jena.graph.Graph;
import org.apache.jena.graph.compose.MultiUnion;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

public class DefaultPragProofRulesFactory implements PragProofRulesFactory {
    @Override
    public PragProofRules createRules(List<RDFInput> inputs) throws IOException {
        List<Graph> graphs = new ArrayList<>(inputs.size());
        for (RDFInput input : inputs) {
            graphs.add(new UncloseableGraph(input.getModel().getGraph()));
        }
        Model union = ModelFactory.createModelForGraph(new MultiUnion(graphs.iterator()));
        return new DefaultPragProofRules(setupCompositionRules(union));
    }

    protected RDFInput setupCompositionRules(Model skolemized) throws IOException {
        final String tagsPrefix = PPO.TAGS_PREFIX;

        ResultSet msgs = QueryExecutionFactory.create(
                QueryFactory.create("PREFIX unserved: <" + Unserved.PREFIX + ">\n" +
                        "SELECT ?request ?reply WHERE {\n" +
                        "  ?reply a unserved:Message.\n" +
                        "  ?reply unserved:when ?when.\n" +
                        "  ?when unserved:reactionTo ?request.\n" +
                        "}\n"
                ), skolemized).execSelect();

        File tmp = Files.createTempFile("unserved-testbench", ".n3").toFile();
        try (FileOutputStream out = new FileOutputStream(tmp);
             PrintStream printer = new PrintStream(out)) {
            printer.printf("@prefix ppo: <" + PPO.PREFIX + ">.\n");
            printer.printf("@prefix unserved-p: <" + UnservedP.PREFIX + ">.\n");
            printer.printf("@prefix dft: <" + tagsPrefix + ">.\n");
            long tagCounter = 1L;
            while (msgs.hasNext()) {
                QuerySolution sol = msgs.next();
                Message req = sol.getResource("request").as(Message.class),
                        rep = sol.getResource("reply").as(Message.class);
                printer.printf("{%1$s} => {%2$s}.\n",
                        generateAntecedent(req), generateConsequent(req, rep, tagCounter++));
            }
        }
        return RDFInputFile.createOwningFile(tmp, RDFInput.N3);
    }

    private String generateAntecedent(Message msg) {
        String str = msg.getVariables().stream()
                .map(var -> String.format("<%1$s> a ppo:Grounded", var.toString()))
                .reduce((l, r) -> l + ". " + r).orElse("");
        return "  " + str + (!str.isEmpty() ? "." : "");
    }

    private String generateConsequent(Message request, Message reply, long tag) {
        return "  " + reply.getVariables().stream()
                .map(var -> String.format("<%1$s> a ppo:Grounded", var.toString()))
                .reduce((l, r) -> l + ". " + r).orElse("")
                + String.format("." +
                        " [ a unserved-p:Sequence, unserved-p:Action; ppo:tag dft:%1$s;" +
                        " unserved-p:members (" +
                        "  [a unserved-p:Send, unserved-p:Action;" +
                        "   unserved-p:message <%2$s> ]" +
                        "  [a unserved-p:Receive, unserved-p:Action;" +
                        "   unserved-p:message <%3$s> ]" +
                        " )].",
                tag, request.toString(), reply.toString());
    }
}

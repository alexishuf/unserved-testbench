package br.ufsc.lapesd.unserved.testbench.pragproof.context.impl;

import br.ufsc.lapesd.unserved.testbench.input.RDFInput;
import br.ufsc.lapesd.unserved.testbench.input.RDFInputFile;
import br.ufsc.lapesd.unserved.testbench.pragproof.context.PragProofRules;
import br.ufsc.lapesd.unserved.testbench.pragproof.model.PPO;
import org.apache.commons.io.IOUtils;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Collections;
import java.util.List;

public class DefaultPragProofRules implements PragProofRules {
    private RDFInput input;

    public DefaultPragProofRules(RDFInput input) {
        this.input = input;
    }

    @Nonnull
    @Override
    public List<RDFInput> getInputs() {
        return Collections.singletonList(input);
    }

    @Override
    public PragProofRules createNonIsolatedCopy() {
        try {
            return new DefaultPragProofRules(new RDFInputFile(input));
        } catch (IOException e) {
            throw new RuntimeException("Failed to copy RDFInput", e);
        }
    }

    @Override
    public void removeInteractionRule(Resource interaction) throws IOException {
        Resource tag = getInteractionTag(interaction);
        if (tag == null) return;
        assert tag.getURI().startsWith(PPO.TAGS_PREFIX);

        List<String> lines = IOUtils.readLines(new FileInputStream(input.toFile()), "UTF-8");
        File tmp = Files.createTempFile("unserved-testbench", ".n3").toFile();
        try (FileOutputStream out = new FileOutputStream(tmp)) {
            for (String line : lines) {
                if (!line.contains("dft:" + tag.getLocalName()))
                    out.write(line.getBytes("UTF-8"));
            }
        }
        input.close();
        input = RDFInputFile.createOwningFile(tmp, RDFInput.N3);
    }

    private static Resource getInteractionTag(Resource interaction) {
        StmtIterator it = interaction.listProperties(PPO.tag);
        Resource tag = null;
        while (it.hasNext()) {
            Statement stmt = it.next();
            if (stmt.getObject().isResource()) {
                tag = stmt.getObject().asResource();
                break;
            }
        }
        return tag;
    }

    @Override
    public void close() throws Exception {
        input.close();
    }
}

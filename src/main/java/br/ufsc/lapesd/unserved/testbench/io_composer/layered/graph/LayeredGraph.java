package br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph;

import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.lazy.SetNode;
import br.ufsc.lapesd.unserved.testbench.io_composer.state.ExecutionPath;
import br.ufsc.lapesd.unserved.testbench.process.data.DataContext;
import br.ufsc.lapesd.unserved.testbench.process.data.DataContextDiff;

import javax.annotation.Nonnull;

public interface LayeredGraph<A, E, V> {
    @Nonnull
    V getStart();

    @Nonnull
    SetNode getEnd();

    /**
     * Public version of <code>getLayer()</code>. Checks if the node belongs to the graph
     * @param node Vertex instance for which the layer is to be found
     * @return index (0-based) of the layer that contains node.
     */
    int getLayer(@Nonnull V node);

    @Nonnull
    V getSource(@Nonnull E edge);

    @Nonnull
    V getTarget(@Nonnull E edge);

    /**
     * Transform an edge in a path of finer-grained actions that when executed correspond to
     * transitioning from the source state to the target state.
     *
     * @param edge An edge of this graph
     * @return Nonnull, possibly empty, ExecutionPath.
     */
    @Nonnull
    ExecutionPath<A> getEdgeActions(@Nonnull E edge);

    /**
     * Creates a validator for Edge actions. The validator receives chunks of actions that
     * together will achieve side effects on a DatContext that correspond to the state
     * transition denoted by this edge.
     *
     * @param edge edge for which the validator will be created.
     * @param dataContext A {@link DataContext} with the state to be considered before execution
     *                    of edge's satisfying actions started. This context is not changed by the
     *                    validator, but must remain valid throughout the validator lifetime.
     * @return new {@link Validator}
     */
    @Nonnull
    Validator<A, V> createValidator(@Nonnull E edge, @Nonnull DataContext dataContext);


    interface Validator<A, V> {
        /**
         * Validate a set of actions which produced the changes in diff.
         *
         * @param diff
         * @param diffActions
         * @return false iff the validator determined that the whole edge will not be satisfied
         * assuming that future actions abide to the edge specification. If false is returned,
         * <code>isFailed() == true</code> will hold.
         */
        boolean validateActions(@Nonnull DataContextDiff diff,
                                @Nonnull ExecutionPath<A> diffActions);

        /**
         * A failed validator is one that assuming future actions are not re-runs of previous
         * actions that work (this time), nor actions not predicted by the validator's edge,
         * the edge will never be satisfied. Further validateActions() calls are futile.
         *
         * For failed validators, the adaptGraph() may allow further successful re-planning.
         *
         * @return true iff the validator is definitely failed.
         */
        boolean isFailed();

        /**
         * A ready validator is one for which the changes observed through validateActions()
         * satisfy the edge. See the getDiff() method.
         *
         * @return true iff the validator is ready.
         */
        boolean isReady();

        /**
         * Gets the {@link DataContextDiff} which satisfies this validator's edge. This diff
         * is based on the {@link DataContext} used on validator creation/setup.
         *
         * Preconditions: <code>isReady() == true</code>
         * @return DataContextDiff that corresponds to the validator's edge.
         */
        @Nonnull DataContextDiff getDiff();

        /**
         * Changes the graph in the hope that the changed graph will allow a re-plan that
         * reaches the goal vertex. The failing edge (the Validator's edge) and its target node
         * are removed.
         *
         * In addition, these may be replaced by alternatives. The Validator may also use the
         * actions that did not failed to create a new start state for further re-planning.
         *
         * This method does not have <code>isReady() == true</code> as side effect.
         *
         * Preconditions: <code>isFailed() == true</code>
         *
         * @return true iff the adaptation could be done.
         */
        boolean adaptGraph();

        /**
         * True iff the last adaptGraph() call used alternatives to adapt the graph.
         * When false, the only adaptation possible was removing the faulty state with an empty
         * state that allows backtracking.
         */
        boolean adaptedWithAlternatives();

        /**
         * After graph adaptation this returns either the source of the validator's edge, or a
         * new state that takes into account the actions that were successful, so that these are
         * not re-performed. If a re-planing is done starting from this state,
         * getAdaptationDiff() MUST be applied to the relevant {@link DataContext}.
         *
         * Preconditions: A previous adaptGraph() call that yielded true.
         * @return a new start vertex for re-planning, may be source of the validator's edge.
         */
        @Nonnull V getNewStart();

        /**
         * After adaptaion, some actions previously validate may remain useful. If that is the a
         * suitable new planning start state is proposed (getNewStart()) and this will return a
         * {@link DataContextDiff} which will make the {@link DataContext} used in the validator
         * construction/setup consistent with this new state.
         *
         * Preconditions: A previous adaptGraph() call that yielded true.
         * @return diff with changes to make the initial {@link DataContext} consistent with
         * getNewStart().
         */
        @Nonnull DataContextDiff getAdaptationDiff();
    }


    /**
     * Gets a Duplicator that produces an write-isolated new instance <code>x</code> with the same
     * content as <code>this</code>.
     *
     * Changes on <code>x</code> have no effect on <code>this</code>, but changes on
     * <code>this</code> MAY be visible by reads on <code>x</code>. Relying on the visibility of
     * such changes is considered a bug. This non-isolation is intended only to allow easy and
     * efficient implementation (where that is the case), since this interface initial use cases
     * do not need read isolation.
     *
     * @return A Duplicator
     */
    Duplicator<A, E, V> createWriteIsolatedDuplicator();

    interface Duplicator<A, E, V> {
        /**
         * Gets the duplicate graph, this always returns the same graph, as the duplicator
         * only duplicates once.
         *
         * @return duplicate graph
         */
        @Nonnull
        LayeredGraph<A, E, V> getDuplicate();

        @Nonnull
        Validator<A, V> translateValidator(@Nonnull Validator<A, V> validator);
    }
}

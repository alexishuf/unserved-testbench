package br.ufsc.lapesd.unserved.testbench.components.expressions.evaluator.subclass;

import br.ufsc.lapesd.unserved.testbench.components.expressions.Connective;
import br.ufsc.lapesd.unserved.testbench.components.expressions.evaluator.ClassExpressionEvaluator.Result;
import br.ufsc.lapesd.unserved.testbench.components.expressions.evaluator.Step;
import com.google.common.base.Preconditions;
import org.apache.jena.rdf.model.Resource;

import javax.annotation.Nonnull;
import java.util.Collection;

public class ConnectiveSubclassStep extends Step<Resource> {
    @Nonnull private final Connective expr;

    public ConnectiveSubclassStep(@Nonnull Collection<Step<Resource>> children,
                                  @Nonnull Connective expression) {
        super(children);
        this.expr = expression;
    }

    @Override
    protected Result performEvaluate(Resource aClass) {
        Preconditions.checkArgument(hasAllResults(aClass));
        if (Helper.isSubclass(aClass, expr)) return Result.TRUE;

        switch (expr.connectiveType()) {
            case UNION:
                return some(aClass, Result.TRUE) ? Result.TRUE : Result.INDETERMINATE;
            case INTERSECTION:
                return Result.INDETERMINATE;
            case COMPLEMENT:
                return Result.INDETERMINATE;
        }

        throw new UnsupportedOperationException();
    }

    private boolean hasAllResults(Resource object) {
        return getChildren().stream().allMatch(c -> c.getEvaluatedResult(object) != null);
    }

    private boolean some(Resource aClass, Result value) {
        return getChildren().stream().anyMatch(c -> c.getEvaluatedResult(aClass) == value);
    }
}

package br.ufsc.lapesd.unserved.testbench.io_composer.layered;

import br.ufsc.lapesd.unserved.testbench.composer.NoPlanComposerException;
import br.ufsc.lapesd.unserved.testbench.io_composer.IOComposer;
import br.ufsc.lapesd.unserved.testbench.io_composer.impl.DefaultIOComposerTimes;
import br.ufsc.lapesd.unserved.testbench.io_composer.impl.IOComposerInput;
import br.ufsc.lapesd.unserved.testbench.io_composer.impl.IOComposerInputDelta;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.LayeredGraph;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.lazy.BackwardSetNodeProvider;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.lazy.LazyLayeredServiceSetGraph;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.lazy.SetNode;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.lazy.SetNodeEdge;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.LayeredServiceGraph;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.discovery.IOLayersState;
import br.ufsc.lapesd.unserved.testbench.io_composer.state.Action;
import br.ufsc.lapesd.unserved.testbench.io_composer.state.ExecutionPath;
import br.ufsc.lapesd.unserved.testbench.process.data.DataContextDiff;
import br.ufsc.lapesd.unserved.testbench.replication.ReplicatedExecution;
import br.ufsc.lapesd.unserved.testbench.util.DStarLite;
import com.google.common.base.Preconditions;

import javax.annotation.Nonnull;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;

public class DStarLiteLayeredIOComposer extends LayeredServiceSetGraphIOComposer {

    public DStarLiteLayeredIOComposer(@Nonnull IOComposerInput ci,
                  @Nonnull Function<IOComposerInput, IOLayersState> layersStateFactory,
                  @Nonnull BackwardSetNodeProvider backwardSetNodeProvider,
                  @Nonnull List<Function<LayeredServiceGraph, LayeredServiceGraph>> optimizers) {
        super(ci, layersStateFactory, backwardSetNodeProvider, optimizers);
    }

    @Override
    protected Execution createExecution() {
        DefaultIOComposerTimes times = createTimes();
        LazyLayeredServiceSetGraph graph = createGraph(times);
        DStarLite<SetNode, Double> dStar = DStarLite.withDoubleCost()
                .withGraphAccessor(new LazyGraphAccessor(graph, cost)).withGoal(graph.getEnd())
                .withHeuristic((p, s) -> heuristic.get(graph, p, s))
                .build();
        dStar.setMultiResults(false);
//        graph.addListener(new LazyGraphListener(graph, dStar));
        return new Execution(ci, graph, cost, heuristic, times, dStar);
    }

    protected static class Execution extends AbstractLayeredIOComposer.Execution<SetNodeEdge, SetNode> {
        private @Nonnull DStarLite<SetNode, Double> dStar;
        private AdaptationGraphListener adaptationGraphListener = null;

        public Execution(@Nonnull IOComposerInput ci,
                         @Nonnull LayeredGraph<Action, SetNodeEdge, SetNode> graph,
                         @Nonnull CostFunction<SetNodeEdge, SetNode> cost,
                         @Nonnull CostFunction<SetNodeEdge, SetNode> heuristic,
                         DefaultIOComposerTimes times, @Nonnull DStarLite<SetNode, Double> dStar) {
            super(ci, graph, cost, heuristic, times);
            this.dStar = dStar;
        }

        public Execution(@Nonnull IOComposerInputDelta ci,
                         @Nonnull LayeredGraph<Action, SetNodeEdge, SetNode> graph,
                         @Nonnull CostFunction<SetNodeEdge, SetNode> cost,
                         @Nonnull CostFunction<SetNodeEdge, SetNode> heuristic,
                         @Nonnull DefaultIOComposerTimes times,
                         @Nonnull ExecutionPath<SetNodeEdge> graphPath,
                         @Nonnull ExecutionPath<Action> diffPath,
                         @Nonnull ExecutionPath<Action> remainingEdgePath, @Nonnull SetNode start,
                         @Nonnull LayeredProgress progress,
                         LayeredGraph.Validator<Action, SetNode> validator,
                         DataContextDiff diff,
                         SetNodeEdge edge,
                         @Nonnull DStarLite<SetNode, Double> dStar) {
            super(ci, graph, cost, heuristic, times, graphPath, diffPath, remainingEdgePath, start,
                    progress, validator, diff, edge);
            this.dStar = dStar;
        }

        @Override
        public ReplicatedExecution duplicate() {
            Preconditions.checkState(!closed);
            LayeredGraph.Duplicator<Action, SetNodeEdge, SetNode> duplicator;
            duplicator = graph.createWriteIsolatedDuplicator();
            LazyLayeredServiceSetGraph graphDup;
            graphDup = (LazyLayeredServiceSetGraph)duplicator.getDuplicate();

            ExecutionPath<SetNodeEdge> graphPathDuplicate = new ExecutionPath<>(graphPath.asList());
            LayeredGraph.Validator<Action, SetNode> validatorDuplicate = validator == null ? null :
                    duplicator.translateValidator(validator);
            DStarLite<SetNode, Double> dStarDup = dStar.duplicateTo(
                    new LazyGraphAccessor(graphDup, cost),
                    (p, s) -> heuristic.get(graphDup, p, s)
            );

            return new Execution(new IOComposerInputDelta(ci),
                    graphDup, cost, heuristic, times.duplicate(),
                    graphPathDuplicate, new ExecutionPath<>(diffPath),
                    new ExecutionPath<>(remainingEdgePath),
                    start, progress, validatorDuplicate, diff, edge, dStarDup);
        }

        @Override
        protected void beginGraphAdaptation() {
            super.beginGraphAdaptation();
            this.adaptationGraphListener = new AdaptationGraphListener(dStar);
            ((LazyLayeredServiceSetGraph)graph).addListener(adaptationGraphListener);
        }

        @Override
        protected void endGraphAdaptation() {
            super.endGraphAdaptation();
            ((LazyLayeredServiceSetGraph)graph).removeListener(adaptationGraphListener);
            adaptationGraphListener = null;
        }

        @Nonnull
        @Override
        protected ExecutionPath<SetNodeEdge> findPath() {
            Preconditions.checkState(!closed);
            assert graph instanceof LazyLayeredServiceSetGraph;

            DStarLite.Result<SetNode> result = dStar.runFrom(start).getResult();
            if (!result.hasPath()) throw new NoPlanComposerException("D* Lite found no path");
            List<SetNode> path = result.getPath();
            if (path.isEmpty()) return new ExecutionPath<>();

            return toEdgePath((LazyLayeredServiceSetGraph)graph, path);
        }
    }

    public static class Builder extends LayeredServiceSetGraphIOComposer.Builder {
        @Override
        protected IOComposer buildImpl() {
            return new DStarLiteLayeredIOComposer(composerInput, layersStateFactory,
                    backwardSetNodeProvider, optimizers);
        }
    }

    protected static class AdaptationGraphListener implements LazyLayeredServiceSetGraph.Listener {
        private final @Nonnull DStarLite<SetNode, Double> dStar;

        public AdaptationGraphListener(@Nonnull DStarLite<SetNode, Double> dStar) {
            this.dStar = dStar;
        }

        @Override
        public void newSuccessors(@Nonnull LazyLayeredServiceSetGraph.NewSuccessors successors) {
            dStar.notifyChangedEdgeSources(Collections.singleton(successors.getPredecessor()));
        }

        @Override
        public void removedSuccessors(@Nonnull LazyLayeredServiceSetGraph.RemovedSuccessors removed) {
            dStar.notifyChangedEdgeSources(Collections.singleton(removed.getPredecessor()));
        }

        @Override
        public void removedNode(@Nonnull SetNode node) {
            dStar.notifyChangedEdgeSources(Collections.singleton(node));
        }

        @Override
        public void newNode(@Nonnull SetNode node) {
            dStar.notifyChangedEdgeSources(Collections.singleton(node));
        }
    }

//    protected static class LazyGraphListener implements LazyLayeredServiceSetGraph.Listener {
//        private final @Nonnull LazyLayeredServiceSetGraph graph;
//        private final @Nonnull DStarLite<SetNode, Double> dStar;
//
//        LazyGraphListener(@Nonnull LazyLayeredServiceSetGraph graph,
//                                  @Nonnull DStarLite<SetNode, Double> dStar) {
//            this.graph = graph;
//            this.dStar = dStar;
//        }
//
//        @Override
//        public void newSuccessors(@Nonnull LazyLayeredServiceSetGraph.NewSuccessors successors) {
//            dStar.notifyNewSuccessors(successors.getPredecessor(), successors::getNew);
//        }
//        @Override
//        public void removedSuccessors(@Nonnull LazyLayeredServiceSetGraph.RemovedSuccessors removed) {
//            dStar.notifyRemovedSuccessors(removed.getPredecessor(), removed::getRemoved);
//        }
//        @Override
//        public void removedNode(@Nonnull SetNode node) {
//            /* ignore: removedSuccessors() is delivered before this and that does it (unless the
//             * start node of a search could be removed, which does not happen in our case).      */
//        }
//        @Override
//        public void newNode(@Nonnull SetNode node) {
//            /* ignore */
//        }
//    }
}

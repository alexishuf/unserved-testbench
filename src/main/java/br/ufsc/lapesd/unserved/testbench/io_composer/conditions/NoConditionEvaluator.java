package br.ufsc.lapesd.unserved.testbench.io_composer.conditions;

import javax.annotation.Nonnull;

public class NoConditionEvaluator implements ConditionEvaluator {
    @Override
    public <T extends Condition> boolean evaluate(@Nonnull T condition) throws IllegalArgumentException {
        throw new IllegalArgumentException();
    }

    @Override
    public <T extends Condition> boolean canEvaluate(@Nonnull T condition) {
        return false;
    }
}

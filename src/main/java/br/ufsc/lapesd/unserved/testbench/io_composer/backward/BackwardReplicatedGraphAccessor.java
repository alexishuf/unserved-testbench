package br.ufsc.lapesd.unserved.testbench.io_composer.backward;

import br.ufsc.lapesd.unserved.testbench.io_composer.graph.ReplicatedGraphAccessor;

public interface BackwardReplicatedGraphAccessor<State, Cost, Transition>
        extends ReplicatedGraphAccessor<State, Cost, Transition>,
                BackwardGraphAccessor<State, Cost, Transition> {
}

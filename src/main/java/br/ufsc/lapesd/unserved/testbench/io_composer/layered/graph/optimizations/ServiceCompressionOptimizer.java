package br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.optimizations;

import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.Condition;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.*;
import br.ufsc.lapesd.unserved.testbench.model.Variable;
import com.google.common.base.Preconditions;
import com.google.common.collect.*;
import org.apache.commons.lang3.tuple.ImmutablePair;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.*;
import java.util.function.Function;

import static br.ufsc.lapesd.unserved.testbench.io_composer.layered.LayeredServiceSetGraphIOComposer.graphOptimizationMem;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;
import static java.util.stream.Stream.concat;
import static org.apache.commons.lang3.tuple.ImmutablePair.of;

public class ServiceCompressionOptimizer
        implements Function<LayeredServiceGraph, LayeredServiceGraph> {
    private boolean selfAlternative = false;

    /**
     * If true, the optimizer will add the representative nodes as alternatives to themselves.
     * In practice, this forces all failed services to be retried once.
     */
    public boolean getSelfAlternative() {
        return selfAlternative;
    }

    public ServiceCompressionOptimizer setSelfAlternative(boolean selfAlternative) {
        this.selfAlternative = selfAlternative;
        return this;
    }

    @Override
    public LayeredServiceGraph apply(LayeredServiceGraph g) {
        return new Stateful(g).apply();
    }

//    private void putariaDebugLayer(ServiceLayer layer, int index) {
//        try (FileOutputStream fileOut = new FileOutputStream("/tmp/unserved-compressed", true);
//             PrintStream out = new PrintStream(fileOut)) {
//            out.printf("layer: %d\nservices: %s\n\n", index,
//                    layer.getNodes().stream().map(n -> {
//                        if (n instanceof ServiceNode) {
//                            return ((ServiceNode) n).getAntecedent()
//                                    .getProperty(Wscc.wscService).getLiteral().getLexicalForm()
//                                    .split("#")[1];
//                        } else return n.toString();
//                    }).sorted().reduce((l, r) -> l + ", " + r).orElse("<empty>")
//            );
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }

    private final class Stateful {
        private final LayeredServiceGraph g;
        private List<ServiceLayer> layers = new ArrayList<>();
        private Map<Node, ProviderSet> providerSets = new HashMap<>();
        private Map<Node, Integer> nodeLayer = new HashMap<>();
        private BiMap<Node, Node> newNodes = HashBiMap.create();

        public Stateful(LayeredServiceGraph g) {
            this.g = g;
        }

        public LayeredServiceGraph apply() {
            for (int i = 0; i < g.getLayers().size(); i++) {
                ServiceLayer layer = getOptimizedLayer(i);
//            putariaDebugLayer(layer, i);
                assert !layer.getNodes().isEmpty();
                layers.add(layer);
                for (Node newNode : layer.getNodes()) {
                    Node n = newNodes.inverse().get(newNode);
                    nodeLayer.put(newNode, i);
                    ProviderSet ps = g.getProviderSet(n).without(new ConditionProviderHashSet.FilterMap() {
                        @Nullable
                        @Override
                        public ProviderSet.Assignment mapAssignment(@Nonnull ProviderSet.Assignment a) {
                            Node newNode = newNodes.getOrDefault(a.node, null);
                            return newNode == null ? null
                                    : new ProviderSet.Assignment(a.target, newNode, a.output);
                        }
                        @Nullable
                        @Override
                        public ConditionAssociation
                        mapConditionAssociation(@Nonnull ConditionAssociation a) {
                            Node node = newNodes.getOrDefault(a.node, null);
                            if (node == null) return null;
                            return new ConditionAssociation(a.target, node, a.output);
                        }
                    });
                    assert  ps.isSatisfied();
                    providerSets.put(newNode, ps);
                }
            }

            LayeredServiceGraph sg = new LayeredServiceGraph(layers, providerSets,
                    (StartNode) newNodes.get(g.getStartNode()),
                    (EndNode) newNodes.get(g.getEndNode()), nodeLayer);
            if (g.getTimes()!= null) {
                sg.setTimes(g.getTimes());
                g.getTimes().takeMemSample(graphOptimizationMem);
            }
            return sg;
        }


        private ServiceLayer getOptimizedLayer(int currentIndex) {
            Collection<Set<Node>> groups = getEquivalenceGroups(g.getLayers().get(currentIndex));
            Set<Node> selected = new HashSet<>();
            for (Set<Node> group : groups) {
                checkSamePredecessors(group);
                DominationGraph dg = new DominationGraph(group);
                Set<Node> chosen, extra;
                chosen = dg.nonDominated.stream().flatMap(Set::stream).collect(toSet());
                extra = group.stream().filter(n -> !chosen.contains(n)).collect(toSet());
                assert checkDisjointSets(Arrays.asList(chosen, extra));
                assert Sets.union(chosen, extra).equals(group);
                for (Set<Node> equiv : dg.nonDominated) {
                    List<Node> tail = dg.getSortedAlternatives(equiv);
                    assert tail.stream().noneMatch(equiv::contains);
                    selected.add(createAlternativesNode(equiv, tail));
                }

            }
            return new ServiceLayer(selected);
        }

        @Nonnull
        private Node createAlternativesNode(Set<Node> alternativesSet, List<Node> tail) {
            Preconditions.checkArgument(!alternativesSet.isEmpty());
            Preconditions.checkArgument(alternativesSet.stream().map(Node::getClass)
                    .distinct().count() == 1, "All alternatives must be of the same class");
            assert new HashSet<>(tail).size() == tail.size(); //actually a set
            assert !(new HashSet<>(alternativesSet)).removeAll(tail); // no intersection
            assert alternativesSet.stream() //consistent NodeStableComparator
                    .flatMap(a -> alternativesSet.stream().filter(a2 -> a2 != a)
                                                          .map(a2 -> of(a, a2)))
                    .allMatch(p -> new NodeStableComparator().compare(p.left, p.right) != 0);

            List<Node> alternatives = alternativesSet.stream().sorted(new NodeStableComparator())
                                                     .collect(toList());
            checkSamePredecessors(alternatives);
            checkSameSuccessors(alternatives);

            Node repr = alternatives.iterator().next();

            // repr must not loose any possible transition, backward or forward
            assert new HashSet<>(g.getPartialSuccessors(repr))
                    .containsAll(concat(alternatives.stream(), tail.stream()).skip(1)
                            .flatMap(n -> g.getPartialSuccessors(n).stream()).collect(toSet()));
            assert g.getProviderSet(repr).stream().collect(toSet())
                    .containsAll(concat(alternatives.stream(), tail.stream()).skip(1)
                            .flatMap(n -> g.getProviderSet(n).stream()).collect(toSet()));

            Node result;
            if (repr instanceof StartNode) {
                result = new StartNode(repr.getOutputs(), repr.getPostConditions());
            } else if (repr instanceof EndNode) {
                result = new EndNode(repr.getInputs(), repr.getPreConditions());
            } else if (repr instanceof ServiceNode) {
                List<List<Node>> lists = new ArrayList<>();
                List<ServiceNode> nodes = concat(alternatives.stream(), tail.stream())
                        .map(n -> (ServiceNode) n).collect(toList());
                assert nodes.get(0) == repr;
                for (int i = 0; i < nodes.size(); i++) {
                    ServiceNode n = nodes.get(i);
                    List<Node> list = new ArrayList<>();
                    lists.add(list);
                    nodes.set(i, new ServiceNode(n, list));
                }
                for (int i = 0; i < lists.size(); i++) {
                    List<Node> list = lists.get(i);
                    if (selfAlternative)
                        list.add(nodes.get(i));
                    for (int j = i+1; j < nodes.size(); j++) list.add(nodes.get(j));
                }
                result = nodes.iterator().next();
            } else {
                throw new IllegalArgumentException("Unsupported Node implementation");
            }

            newNodes.put(repr, result);
            return result;
        }

        private boolean checkSamePredecessors(Collection<Node> alternatives) {
            List<Set<Node>> predecessors = alternatives.stream()
                    .map(n -> g.getProviderSet(n).stream().collect(toSet()))
                    .collect(toList());
            for (Set<Node> a : predecessors) {
                for (Set<Node> b : predecessors) assert a.equals(b);
            }
            return true;
        }
        private boolean checkSameSuccessors(Collection<Node> alternatives) {
            List<Set<Node>> successors = alternatives.stream()
                    .map(n -> new HashSet<>(g.getPartialSuccessors(n)))
                    .collect(toList());
            for (Set<Node> a : successors) {
                for (Set<Node> b : successors) assert a.equals(b);
            }
            return true;
        }
        private <T> boolean checkDisjointSets(Collection<Set<T>> sets) {
            for (Set<T> a : sets) {
                for (Set<T> b : sets) assert a == b || !new HashSet<>(a).removeAll(b);
            }
            return true;
        }

        private Collection<Set<Node>> getEquivalenceGroups(ServiceLayer layer) {
            SetMultimap<Set<Set<Node>>, Node> eqs = HashMultimap.create();
            layer.getNodes().forEach(n -> {
                Set<Set<Node>> sets = new HashSet<>();
                ProviderSet ps = g.getProviderSet(n);
                for (Variable  var  : ps.getTargets()   ) sets.add(ps.getProviders(var ));
                for (Condition cond : ps.getConditions()) sets.add(ps.getProviders(cond));
                eqs.put(sets, n);
            });
            List<Set<Node>> lst = new ArrayList<>(eqs.keys().size());
            for (Set<Set<Node>> pre : eqs.keySet()) lst.add(eqs.get(pre));

            assert checkDisjointSets(lst);
            assert lst.stream().flatMap(Set::stream).allMatch(layer.getNodes()::contains);
            assert lst.stream().flatMap(Set::stream).collect(toSet()).containsAll(layer.getNodes());
            return lst;
        }

        private final class DominationGraph {
            private final SetMultimap<Node, Node> dominates = HashMultimap.create();
            private Set<Set<Node>> nonDominated;

            public DominationGraph(Set<Node> nodes) {
                // No domination to find
                if (nodes.size() <= 1) {
                    nonDominated = Collections.singleton(new HashSet<>(nodes));
                    return;
                }

                // Get all partial successors. This speeds up later phases
                Set<Node> successors = nodes.stream().map(g::getPartialSuccessors)
                        .flatMap(Set::stream).collect(toSet());
                if (successors.isEmpty()) {
                    checkSamePredecessors(nodes);
                    nonDominated = Collections.singleton(new HashSet<>(nodes));
                    return;
                }

                // conditions/inputs of successors -> nodes
                SetMultimap<Set<Condition>, Node> conds2prov;
                conds2prov = getConditionSetProviders(nodes, successors);
                assert nodes.containsAll(conds2prov.values());

                // Assume all nodes are equivalent
                SetMultimap<Node, Node> flatEquiv = HashMultimap.create();
                for (Node left : nodes) {
                    for (Node right : nodes) {
                        if (!left.equals(right))
                            flatEquiv.put(left, right);
                    }
                }

                // Find dominance relations and equivalence violations
                ArrayList<Set<Condition>> keyList = new ArrayList<>(conds2prov.keySet());
                for (int i = 0; i < keyList.size(); i++) {
                    Set<Condition> in1 = keyList.get(i);
                    for (int j = i+1; j < keyList.size(); j++) {
                        Set<Condition> in2 = keyList.get(j);
                        if (extractDomination(conds2prov, dominates, in1, in2)) {
                            for (Node left : conds2prov.get(in1)) {
                                for (Node right : conds2prov.get(in2)) {
                                    flatEquiv.remove(left, right);
                                    flatEquiv.remove(right, left);
                                }
                            }
                        }
                    }
                }

                //Build nonDominated
                nonDominated = new HashSet<>();
                Map<Node, Set<Node>> helper = new HashMap<>();
                Stack<ImmutablePair<Node, Node>> stack = new Stack<>();
                flatEquiv.entries().forEach(e -> stack.push(of(e.getKey(), e.getValue())));
                while (!stack.empty()) {
                    ImmutablePair<Node, Node> e = stack.pop();
                    Set<Node> set = helper.get(e.left);
                    if (set == null) {
                        (set = new HashSet<>()).add(e.left);
                        helper.put(e.left, set);
                        nonDominated.add(set);
                    }
                    helper.put(e.right, set);
                    if (set.add(e.right))
                        flatEquiv.get(e.right).forEach(n -> stack.push(of(e.right, n)));
                }

                // Add nodes that are not dominated and have no equivalent
                HashSet<Node> missing = new HashSet<>(nodes);
                missing.removeAll(helper.keySet());
                missing.removeAll(dominates.values());
                for (Node node : missing) nonDominated.add(Collections.singleton(node));

                assert checkDisjointSets(nonDominated);
                assert nonDominated.stream().allMatch(Stateful.this::checkSamePredecessors);
                assert nonDominated.stream().allMatch(Stateful.this::checkSameSuccessors);

                assert nonDominated.stream().flatMap(Set::stream).allMatch(nodes::contains);
                assert nodes.containsAll(dominates.values());
                assert concat(nonDominated.stream().flatMap(Set::stream),
                              dominates.values().stream()).collect(toSet()).containsAll(nodes);
            }

            /**
             * @return true iff in1 and in2 are not equivalent
             */
            private boolean extractDomination(@Nonnull SetMultimap<Set<Condition>, Node> conds2prov,
                                              @Nonnull SetMultimap<Node, Node> flatDominates,
                                              @Nonnull Set<Condition> in1,
                                              @Nonnull Set<Condition> in2) {
                int r = tryDominationPriv(conds2prov, flatDominates, in1, in2);
                if (r == 0)
                    r = tryDominationPriv(conds2prov, flatDominates, in2, in1);
                return r != 2;
            }

            private int tryDominationPriv(@Nonnull SetMultimap<Set<Condition>, Node> conds2prov,
                                          @Nonnull SetMultimap<Node, Node> flatDominates,
                                          @Nonnull Set<Condition> dominatorConds,
                                          @Nonnull Set<Condition> dominatedConds) {
                if (dominatorConds.containsAll(dominatedConds)) {
                    if (dominatorConds.size() > dominatedConds.size()) {
                        for (Node dominator : conds2prov.get(dominatorConds)) {
                            for (Node dominated : conds2prov.get(dominatedConds))
                                flatDominates.put(dominator, dominated);
                        }
                        return 1;
                    } else { return 2; }
                }
                return 0;
            }

            private SetMultimap<Set<Condition>, Node>
            getConditionSetProviders(@Nonnull Set<Node> noi, @Nonnull Collection<Node> successors)  {
                SetMultimap<Node, Condition> prov2cond = HashMultimap.create();
                for (Node successor : successors) {
                    ProviderSet ps = g.getProviderSet(successor);
                    ps.getAssignmentsStream().filter(a -> noi.contains(a.node))
                            .forEach(a -> prov2cond.put(a.node, a.target.asSpec()));
                    for (Condition condition : ps.getConditions()) {
                        for (Node provider : ps.getProviders(condition)) {
                            if (noi.contains(provider)) prov2cond.put(provider, condition);
                        }
                    }
                }

                SetMultimap<Set<Condition>, Node> cond2prov = HashMultimap.create();
                for (Node prov : prov2cond.keySet()) cond2prov.put(prov2cond.get(prov), prov);
                return cond2prov;
            }

            @Nonnull
            List<Node> getSortedAlternatives(@Nonnull Set<Node> equiv) {
                Preconditions.checkArgument(!equiv.isEmpty());

                LinkedList<Node> list = new LinkedList<>();
                Stack<ImmutablePair<Node, Integer>> stack = new Stack<>();
                for (Node node : equiv) stack.add(of(node, 0));
                Set<Node> closed = new HashSet<>(), open = new HashSet<>();
                while (!stack.isEmpty()) {
                    ImmutablePair<Node, Integer> state = stack.pop();
                    Node node = state.left;
                    if (state.right == 0) {
                        if (open.contains(node)) throw new IllegalStateException("Not a DAG");
                        if (closed.contains(node)) continue;
                        open.add(node);
                        stack.push(of(node, 1));
                        dominates.get(node).forEach(child -> stack.push(of(child, 0)));
                    } else if (state.right == 1) {
                        closed.add(node);
                        open.remove(node);
                        if (!equiv.contains(node))
                            list.addFirst(node);
                    }
                }

                assert new HashSet<>(list).size() == list.size();
                return new ArrayList<>(list);
            }
        }
    }

    @Override
    public String toString() {
        return "ServiceCompressionOptimizer";
    }
}

package br.ufsc.lapesd.unserved.testbench.io_composer.backward;

import br.ufsc.lapesd.unserved.testbench.composer.ComposerException;
import br.ufsc.lapesd.unserved.testbench.composer.Composition;
import br.ufsc.lapesd.unserved.testbench.composer.impl.NullComposition;
import br.ufsc.lapesd.unserved.testbench.io_composer.AbstractIOComposer;
import br.ufsc.lapesd.unserved.testbench.io_composer.graph.GraphAccessor;
import br.ufsc.lapesd.unserved.testbench.io_composer.impl.IOComposerInput;
import br.ufsc.lapesd.unserved.testbench.io_composer.state.State;
import es.usc.citius.hipster.algorithm.AStar;
import es.usc.citius.hipster.algorithm.Hipster;
import es.usc.citius.hipster.model.Node;
import es.usc.citius.hipster.model.Transition;
import es.usc.citius.hipster.model.impl.WeightedNode;
import es.usc.citius.hipster.model.problem.ProblemBuilder;
import es.usc.citius.hipster.model.problem.SearchProblem;

import javax.annotation.Nonnull;

public abstract class AbstractBackwardIOAStar extends AbstractIOComposer {
    @Nonnull protected final GraphAccessor<State, Double, Transition<Void, State>> accessor;

    public AbstractBackwardIOAStar(@Nonnull IOComposerInput composerInput,
                                   @Nonnull BackwardGraphAccessor<State, Double, Transition<Void, State>>
                                           graphAccessor) {
        super(composerInput);
        this.accessor = graphAccessor;
    }

    @Nonnull
    @Override
    public Composition run() throws ComposerException {
        SearchProblem<Void, State, WeightedNode<Void, State, Double>> problem;
        problem = ProblemBuilder.create()
                .initialState(accessor.getInitial())
                .defineProblemWithoutActions()
                .useTransitionFunction(accessor::getTransitions)
                .useCostFunction(accessor::getCost)
                .build();
        AStar<Void, State, Double, WeightedNode<Void, State, Double>>.Iterator it;
        it = Hipster.createAStar(problem).iterator();
        while (it.hasNext()) {
            WeightedNode<Void, State, Double> node = it.next();
            if (accessor.isGoal(node.state()))
                return toComposition(node);
        }
        return new NullComposition();
    }

    @Override
    public void close() throws ComposerException {
        super.close();
        accessor.close();
    }

    protected abstract <A, NodeType extends Node<A, State, NodeType>>
    Composition toComposition(Node<A, State, NodeType> goalNode);
}

package br.ufsc.lapesd.unserved.testbench.process.actions;

/**
 * An error while executing action. Maybe it was just this action's fault and further actions
 * should still work.
 */
public class ActionExecutionException extends Exception {
    public ActionExecutionException(String s) {
        super(s);
    }

    public ActionExecutionException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public ActionExecutionException(Throwable throwable) {
        super(throwable);
    }
}

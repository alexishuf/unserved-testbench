package br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service;

import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.And;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.cost.CostInfo;
import br.ufsc.lapesd.unserved.testbench.model.Variable;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Set;

public class SingleJumpNode extends AbstractNode implements JumpNode {
    private final @Nonnull Set<Variable> inputSingleton;
    private final IOProviderHashSet providers = new IOProviderHashSet();

    public SingleJumpNode(@Nonnull Variable input, @Nonnull Node delegate,
                          @Nonnull Variable delegateOutput) {
        super(new ArrayList<>());
        this.inputSingleton = Collections.singleton(input);
        providers.add(input, delegate, delegateOutput);
    }

    public SingleJumpNode(@Nonnull ProviderSet.Assignment assignment) {
        this(assignment.target, assignment.node, assignment.output);
    }

    @Override
    @Nonnull
    public ProviderSet getProviderSet() {
        return providers;
    }

    @Nonnull
    @Override
    public Set<Variable> getInputs() {
        return inputSingleton;
    }

    @Nonnull
    @Override
    public Set<Variable> getOutputs() {
        return inputSingleton;
    }

    @Nonnull
    @Override
    public And getPreConditions() {
        return And.empty();
    }
    @Nonnull
    @Override
    public And getPostConditions() {
        return And.empty();
    }

    @Nonnull
    @Override
    public CostInfo getCostInfo() {
        return CostInfo.EMPTY;
    }

    @Override
    public Object getId() {
        return this;
    }

    @Override
    public String toString() {
        return String.format("JumpNode(%s)", inputSingleton.iterator().next());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SingleJumpNode rhs = (SingleJumpNode) o;

        return inputSingleton.equals(rhs.inputSingleton);
    }

    @Override
    public int hashCode() {
        return inputSingleton.hashCode();
    }
}

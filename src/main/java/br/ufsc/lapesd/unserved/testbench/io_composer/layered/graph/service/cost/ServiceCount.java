package br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.cost;

import javax.annotation.Nonnull;

public class ServiceCount implements CostParameter {
    private int value;
    public ServiceCount(int value) {
        this.value = value;
    }


    public static final String NAME = "ServiceCount";
    public static ServiceCount IDENTITY = new ServiceCount(0);

    @Nonnull
    @Override
    public String getName() {
        return NAME;
    }

    public int getValue() {
        return value;
    }

    @Override
    public CostParameter aggregateSequence(CostParameter right) {
        return new ServiceCount(value + ((ServiceCount)right).value);
    }

    @Override
    public CostParameter aggregateParallel(CostParameter right) {
        return aggregateSequence(right);
    }

    @Override
    public int compare(CostParameter right) {
        return Integer.compare(value, ((ServiceCount)right).value);
    }

    @Override
    public String toString() {
        return String.format("%s(%d)", NAME, value);
    }
}

package br.ufsc.lapesd.unserved.testbench.reasoner.impl;

import br.ufsc.lapesd.unserved.testbench.reasoner.N3Reasoner;
import br.ufsc.lapesd.unserved.testbench.reasoner.N3ReasonerFactory;

/**
 * Creates RiotEnhancedN3Reasoner instances.
 */
public class RiotEnhancedN3ReasonerFactory implements N3ReasonerFactory {
    private final N3ReasonerFactory delegate;

    public RiotEnhancedN3ReasonerFactory(N3ReasonerFactory delegate) {
        this.delegate = delegate;
    }

    @Override
    public N3Reasoner createReasoner() {
        return new RiotEnhancedN3Reasoner(delegate.createReasoner());
    }
}

package br.ufsc.lapesd.unserved.testbench.benchmarks.wsc08;

import br.ufsc.lapesd.unserved.testbench.benchmarks.ComponentRegistrySetup;
import br.ufsc.lapesd.unserved.testbench.components.ComponentRegistrationException;
import br.ufsc.lapesd.unserved.testbench.components.ComponentRegistry;
import br.ufsc.lapesd.unserved.testbench.components.SimpleActionRunnerFactory;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.LayeredProgress;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.LayeredProgressTemplate;
import br.ufsc.lapesd.unserved.testbench.iri.UnservedP;
import br.ufsc.lapesd.unserved.testbench.model.Message;
import br.ufsc.lapesd.unserved.testbench.model.Variable;
import br.ufsc.lapesd.unserved.testbench.process.Context;
import br.ufsc.lapesd.unserved.testbench.process.actions.ActionExecutionException;
import br.ufsc.lapesd.unserved.testbench.process.actions.ActionRunner;
import br.ufsc.lapesd.unserved.testbench.process.actions.mock.MockReceiveRunner;
import br.ufsc.lapesd.unserved.testbench.process.data.DataContext;
import br.ufsc.lapesd.unserved.testbench.process.model.Receive;
import br.ufsc.lapesd.unserved.testbench.replication.action.Progress;
import br.ufsc.lapesd.unserved.testbench.replication.action.ReplicationActionRunnerContext;
import com.google.common.base.Preconditions;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.FileReader;
import java.lang.reflect.Type;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class WscComponentRegistrySetup implements ComponentRegistrySetup {
    private @Nonnull Map<LayeredProgressTemplate, Integer> failPoints = new HashMap<>();
    private static Map<Integer, Map<LayeredProgressTemplate, Integer>> failPointsGlobal = new HashMap<>();

    private Map<String, String> parseArgs(@Nonnull String args) {
        List<List<String>> kvs = Stream.of(args.split(";"))
                .map(kv -> Arrays.asList(kv.split("="))).collect(Collectors.toList());
        Preconditions.checkArgument(kvs.stream().allMatch(l -> l.size() > 0 && l.size() <= 2),
                "Malformed kv list");
        Map<String, String> config = new HashMap<>();
        kvs.stream().map(kv -> kv.size() < 2 ? Arrays.asList(kv.get(0), "true") : kv)
                .forEach(kv -> config.put(kv.get(0), kv.get(1)));
        return config;
    }

    @Override
    public void setArgs(@Nonnull String args) {
        failPoints.clear();

        Map<String, String> config = parseArgs(args);
        String path = config.getOrDefault("failPoints", null);
        if (path != null) {
            try (FileReader reader = new FileReader(new File(path))) {
                Type type = new TypeToken<Map<String, Integer>>() {}.getType();
                Map<String, Integer> map = new Gson().fromJson(reader, type);
                for (Map.Entry<String, Integer> e : map.entrySet())
                    failPoints.put(LayeredProgressTemplate.parse(e.getKey()), e.getValue());
            } catch (Exception e) {
                throw new IllegalArgumentException("Failed to read fail points from " + path, e);
            }
        }
    }

    @Override
    public void setup(@Nonnull ComponentRegistry registry) throws ComponentRegistrationException {
        registry.unregisterComponent(MockReceiveRunner.Factory.class);

        if (failPointsGlobal.size() == 0) {
            failPointsGlobal.put(1, failPoints);
            registry.registerComponent(ReceiveRunnerFactory1.class);
        } else if (failPointsGlobal.size() == 1) {
            failPointsGlobal.put(2, failPoints);
            registry.registerComponent(ReceiveRunnerFactory2.class);
        } else if (failPointsGlobal.size() == 2) {
            failPointsGlobal.put(3, failPoints);
            registry.registerComponent(ReceiveRunnerFactory3.class);
        } else if (failPointsGlobal.size() == 3) {
            failPointsGlobal.put(4, failPoints);
            registry.registerComponent(ReceiveRunnerFactory4.class);
        } else if (failPointsGlobal.size() == 4) {
            failPointsGlobal.put(5, failPoints);
            registry.registerComponent(ReceiveRunnerFactory5.class);
        } else if (failPointsGlobal.size() == 5) {
            failPointsGlobal.put(6, failPoints);
            registry.registerComponent(ReceiveRunnerFactory6.class);
        } else {
            throw new ComponentRegistrationException("Workaround on WscComponentRegistrySetup " +
                    "does not support more setups in this JVM.");
        }
    }


    public static class ReceiveRunnerFactory1 extends SimpleActionRunnerFactory {
        public ReceiveRunnerFactory1() {
            super(MockReceiveRunner.class, ReceiveRunnerFactory1.class, UnservedP.Receive);
        }
        @Override
        public ActionRunner newInstance() {
            return new Runner(1);
        }
    }
    public static class ReceiveRunnerFactory2 extends SimpleActionRunnerFactory {
        public ReceiveRunnerFactory2() {
            super(MockReceiveRunner.class, ReceiveRunnerFactory2.class, UnservedP.Receive);
        }
        @Override
        public ActionRunner newInstance() {
            return new Runner(2);
        }
    }
    public static class ReceiveRunnerFactory3 extends SimpleActionRunnerFactory {
        public ReceiveRunnerFactory3() {
            super(MockReceiveRunner.class, ReceiveRunnerFactory3.class, UnservedP.Receive);
        }
        @Override
        public ActionRunner newInstance() {
            return new Runner(3);
        }
    }
    public static class ReceiveRunnerFactory4 extends SimpleActionRunnerFactory {
        public ReceiveRunnerFactory4() {
            super(MockReceiveRunner.class, ReceiveRunnerFactory4.class, UnservedP.Receive);
        }
        @Override
        public ActionRunner newInstance() {
            return new Runner(4);
        }
    }
    public static class ReceiveRunnerFactory5 extends SimpleActionRunnerFactory {
        public ReceiveRunnerFactory5() {
            super(MockReceiveRunner.class, ReceiveRunnerFactory5.class, UnservedP.Receive);
        }
        @Override
        public ActionRunner newInstance() {
            return new Runner(5);
        }
    }
    public static class ReceiveRunnerFactory6 extends SimpleActionRunnerFactory {
        public ReceiveRunnerFactory6() {
            super(MockReceiveRunner.class, ReceiveRunnerFactory6.class, UnservedP.Receive);
        }
        @Override
        public ActionRunner newInstance() {
            return new Runner(6);
        }
    }

    private static class Runner extends MockReceiveRunner {
        private boolean running = false;
        private boolean failing = false;
        private final int index;
        private static Logger logger = LoggerFactory.getLogger(Runner.class);

        private Runner(int index) {
            this.index = index;
        }

        private LayeredProgress getProgress(Resource action, Context context)
                throws ActionExecutionException {
            Collection<ReplicationActionRunnerContext> contexts = context.runners()
                    .getActionContexts(action, ReplicationActionRunnerContext.class);
            if (contexts.size() != 1) {
                throw new ActionExecutionException("Expected a single " +
                        "ReplicationActionRunnerContext, got " + contexts.size());
            }
            ReplicationActionRunnerContext repCtxt = contexts.iterator().next();
            Progress genericProgress = repCtxt.getContainer().getProgress();
            if (genericProgress == null)
                throw new ActionExecutionException("Expected a LayeredProgress, got null");
            if (!(genericProgress instanceof LayeredProgress)) {
                throw new ActionExecutionException("Expected a LayeredProgress, got "
                        + genericProgress.toString());
            }
            return (LayeredProgress) genericProgress;
        }

        @Override
        public void run(Resource resource, Context context) throws ActionExecutionException {
            try {
                Preconditions.checkState(!running);
                running = true;
                LayeredProgress progress = getProgress(resource, context);

                Map<LayeredProgressTemplate, Integer> map = failPointsGlobal.get(index);
                LayeredProgressTemplate triggered = map.keySet().stream()
                        .filter(tpl -> tpl.match(progress)).findFirst().orElse(null);
                if (triggered != null) {
                    assert map.get(triggered) > 0;
                    failing = true;
                    int updated = map.get(triggered) - 1;
                    if (updated > 0) map.put(triggered, updated);
                    else map.remove(triggered);
                }

                Receive rcv = resource.as(Receive.class);
                Statement statement = rcv.getMessage().getProperty(Wscc.wscService);
                String wscSvc = null;
                if (statement != null)
                    wscSvc = statement.getLiteral().getLexicalForm().split("#")[1];
                if (failing && wscSvc != null) {
                    logger.info("Failing receive from {}. Affected variables: {} ", wscSvc,
                            rcv.getMessage().getVariablesRecursive().stream()
                                    .map(Resource::toString).reduce((l, r) -> l + ", " + r)
                                    .orElse(""));
                }

                super.run(resource, context);
            } finally {
                failing = false;
                running = false;
            }
        }

        @Override
        protected void setResource(DataContext context, Message reaction, Variable var) {
            if (!failing)
                super.setResource(context, reaction, var);
        }

        @Override
        protected void setLiteral(DataContext context, Variable var) {
            if (!failing)
                super.setLiteral(context, var);
        }
    }
}

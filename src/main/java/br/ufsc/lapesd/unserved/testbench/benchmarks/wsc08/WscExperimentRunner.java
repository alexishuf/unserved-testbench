package br.ufsc.lapesd.unserved.testbench.benchmarks.wsc08;

import br.ufsc.lapesd.unserved.testbench.benchmarks.compose.TimesExperimentResult;
import br.ufsc.lapesd.unserved.testbench.benchmarks.experiment.AbstractExperimentLevelsDesignRunner;
import br.ufsc.lapesd.unserved.testbench.benchmarks.experiment.ExperimentDesignLevels;
import br.ufsc.lapesd.unserved.testbench.benchmarks.experiment.ExperimentResult;
import br.ufsc.lapesd.unserved.testbench.benchmarks.pragproof_design.PPExperimentDesignRunner;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class WscExperimentRunner extends AbstractExperimentLevelsDesignRunner {
    private static Logger logger = LoggerFactory.getLogger(WscExperimentRunner.class);

    public WscExperimentRunner() {
        super(new WscExperimentFactory());
    }

    @Override
    protected ExperimentDesignLevels loadLevels() throws FileNotFoundException {
        FileReader reader = null;
        try {
            reader = new FileReader(levelsJson);
            WscExperimentDesignLevels levels =
                    new Gson().fromJson(reader, WscExperimentDesignLevels.class);
            File dir = levelsJson.getParentFile();
            assert dir != null;
            levels.setBaseDir(dir);
            return levels;
        } finally {
            try {
                if (reader != null)
                    reader.close();
            } catch (IOException e) {
                logger.warn("Failed to close levels json, ignoring.", e);
            }
        }
    }

    @Override
    protected List<String> getResultHeaders() {
        return Arrays.asList("initialization", "output", "composition", "blacklisting",
                "execution", "total");
    }

    @Override
    protected List<String> getResultValues(ExperimentResult result) {
        TimesExperimentResult r = (TimesExperimentResult) result;
        return Stream.of(r.getInitialization(), r.getOutput(), r.getComposition(),
                r.getBlacklisting(), r.getExecution(), r.getTotal()).map(String::valueOf)
                .collect(Collectors.toList());
    }

    public static class Builder extends AbstractExperimentLevelsDesignRunner.Builder {
        public Builder(File designCsv, File levelsJson, File resultObjectsDir) {
            super(designCsv, levelsJson, resultObjectsDir);
        }

        @Override
        public PPExperimentDesignRunner build() {
            return setupAbstractRunner(new PPExperimentDesignRunner());
        }
    }
}

package br.ufsc.lapesd.unserved.testbench.input;

import br.ufsc.lapesd.unserved.testbench.iri.Unserved;
import org.apache.commons.io.IOUtils;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFFormat;

import javax.annotation.Nonnull;
import java.io.*;
import java.nio.file.Files;

public class RDFInputStream extends AbstractRDFInput {
    final private InputStream stream;
    private String streamData = null;
    private File file;

    public RDFInputStream(@Nonnull InputStream stream, @Nonnull RDFFormat format) {
        super(format);
        this.stream = stream;
    }

    /**
     * Forces the stream to be fully read.
     *
     * This method is blocking and will close the stream. Subsequent calls to toFile() should not
     * throw exceptions. Subsequent getModel() calls may only throw parsing exceptions.
     *
     * @throws IOException
     */
    public void readFully() throws IOException {
        readStream();
    }

    private String readStream() throws IOException {
        if (streamData != null) return streamData;
        streamData = IOUtils.toString(stream);
        stream.close();
        return streamData;
    }

    @Override
    protected Model loadModel() throws IOException {
        if (closed) throw new ClosedException();
        Model model = ModelFactory.createDefaultModel();
        RDFDataMgr.read(model, IOUtils.toInputStream(readStream()), getFormat().getLang());
        stream.close();
        return model;
    }

    @Override
    public File toFile() throws IOException {
        if (closed) throw new ClosedException();
        String ext = "." + getLang().getFileExtensions().get(0);
        file = Files.createTempFile("unserved-testbench", ext).toFile();
        try (FileOutputStream out = new FileOutputStream(file);
             OutputStreamWriter writer = new OutputStreamWriter(out, "UTF-8")) {
            writer.write(readStream());
        }
        return file;
    }

    @Override
    public void close() throws IOException {
        streamData = null;
        if (file != null)
            file.delete();
        super.close();
    }

    @Override
    public String toString() {
        return "RDFInputStream(" + getLang().getName() + "," + stream.toString() + ")";
    }

    static {
        Unserved.init();
    }
}

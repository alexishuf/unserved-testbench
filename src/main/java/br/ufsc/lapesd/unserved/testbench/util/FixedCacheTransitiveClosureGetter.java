package br.ufsc.lapesd.unserved.testbench.util;

import com.google.common.base.Preconditions;
import org.apache.jena.rdf.model.*;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FixedCacheTransitiveClosureGetter extends AbstractTransitiveClosureGetter {
    private static final int DEFAULT_THRESHOLD = 8;
    private static final int DEFAULT_NOCHILDREN_CACHE_SIZE = 511;
//    private static final int DEFAULT_LRUCACHE_SIZE = 256;

    private final Map<Resource, Set<Resource>> cache;
    private final LinkedHashSet<Resource> noChildrenCache;
    private int noChildrenCacheSize;
//    private final LinkedHashMap<Resource, Set<Resource>>  lruCache;
//    private final int lruCacheSize;

    protected FixedCacheTransitiveClosureGetter(Property predicate, boolean backward,
                                                Map<Resource, Set<Resource>> cache,
                                                LinkedHashSet<Resource> noChildrenCache,
                                                int noChildrenCacheSize/*, int lruCacheSize*/) {
        super(predicate, backward);
        this.cache = cache;
        this.noChildrenCache = noChildrenCache;
        this.noChildrenCacheSize = noChildrenCacheSize;
//        this.lruCacheSize = lruCacheSize;
//        lruCache = new LinkedHashMap<>(lruCacheSize);
    }

    @Nonnull
    @Override
    public TransitiveClosureGetter createFor(Property property, boolean isBackward) {
        Model model = null;
        Iterator<Resource> it = cache.keySet().iterator();
        if (it.hasNext()) model = it.next().getModel();
        it = noChildrenCache.iterator();
        if (it.hasNext()) model = it.next().getModel();

        if (model == null) throw new UnsupportedOperationException("Could not recover model");

        return new Builder(property, isBackward).visitAll(model).build();
    }

    @Nonnull
    @Override
    public Set<Resource> getClosureSet(@Nonnull Resource start) {
        Set<Resource> result = getCache(start);
        if (result == null) {
            result = getClosureStream(start).collect(Collectors.toSet());
            if (result.size() == 1) {
                noChildrenCache.add(start);
                if (noChildrenCache.size() > noChildrenCacheSize)
                    noChildrenCache.remove(noChildrenCache.iterator().next());
//            } else {
//                lruCache.put(start, Collections.unmodifiableSet(result));
//                if (lruCache.size() > lruCacheSize)
//                    lruCache.remove(lruCache.keySet().iterator().next());
            }
        }
//        Set<Resource> result = new HashSet<>();
//        Queue<Resource> queue = new LinkedList<>();
//        queue.add(start);
//        while (!queue.isEmpty()) {
//            Resource node = queue.remove();
//            if (result.contains(node)) continue;
//            result.add(node);
//            Set<Resource> cached = cache.get(node);
//            if (cached != null) result.addAll(cached);
//            else forEachChild(node, queue::add);
//        }
        return result;
    }

    @Nonnull
    @Override
    public Stream<Resource> getClosureStream(@Nonnull Resource start) {
        return getDFS(isBackward(), getPredicate(), start, this::getCache).toStream();
    }

    private Set<Resource> getCache(Resource resource) {
        Set<Resource> set = cache.get(resource);
        if (set == null) {
            if (noChildrenCache.remove(resource)) {
                noChildrenCache.add(resource);
                set = Collections.singleton(resource);
            }
        }
//        if (set == null) {
//            set = lruCache.get(resource);
//            if (set != null) {
//                lruCache.remove(resource);
//                lruCache.put(resource, set);
//            }
//        }
        return set;
    }

    public boolean isCached(@Nonnull Resource start) {
        return cache.containsKey(start);
    }

    public static @Nonnull Builder backward(@Nonnull Property predicate) {
        return new Builder(predicate, true);
    }

    public static @Nonnull Builder forward(@Nonnull Property predicate) {
        return new Builder(predicate, false);
    }

    private static BFSBGPIterator getDFS(boolean backward, @Nonnull Property predicate,
                                         @Nonnull Resource r,
                                         @Nullable Function<Resource, Set<Resource>> cache) {
        BFSBGPIterator.Builder builder = BFSBGPIterator.from(r).withInitial();
        if (cache != null)
            builder = builder.withCache(cache);
        return backward ? builder.backward(predicate) : builder.forward(predicate);
    }

    private static BFSBGPIterator getDFS(boolean backward, @Nonnull Property predicate,
                                         @Nonnull Resource r) {
        return getDFS(backward, predicate, r, null);
    }

    private static void forEach(boolean reverse, @Nonnull Property predicate,
                                @Nonnull Resource resource,
                                @Nonnull Consumer<Resource> consumer) {
        if (reverse) {
            resource.getModel().listSubjectsWithProperty(predicate, resource)
                    .forEachRemaining(consumer);
        } else {
            resource.listProperties(predicate).forEachRemaining(s -> {
                if (s.getObject().isResource())
                    consumer.accept(s.getResource());
            });
        }
    }
//    private void forEachChild(@Nonnull Resource resource,
//                              @Nonnull Consumer<Resource> consumer) {
//        forEach(backward, predicate, resource, consumer);
//    }

    public static class Builder {
        private @Nonnull Property predicate;
        private boolean backward;
        private int threshold = DEFAULT_THRESHOLD;
        private LinkedHashSet<Resource> noChildrenCache;
        private int noChildrenCacheSize = DEFAULT_NOCHILDREN_CACHE_SIZE;
//        private int lruCacheSize = DEFAULT_LRUCACHE_SIZE;

        private Map<Resource, Integer> counts = new HashMap<>(), countEstimates = null;
        private Set<Resource> leafs = new HashSet<>();
        private Map<Resource, Set<Resource>> cache = new HashMap<>();

        public Builder(@Nonnull Property predicate, boolean backward) {
            this.predicate = predicate;
            this.backward = backward;
        }

        public Builder setThreshold(int threshold) {
            this.threshold = threshold;
            return this;
        }

        public Builder setNoChildrenCacheSize(int noChildrenCacheSize) {
            this.noChildrenCacheSize = noChildrenCacheSize;
            return this;
        }

//        public Builder setLruCacheSize(int lruCacheSize) {
//            this.lruCacheSize = lruCacheSize;
//            return this;
//        }

        private void forEachChild(@Nonnull Resource resource,
                                  @Nonnull Consumer<Resource> consumer) {
            forEach(backward, predicate, resource, consumer);
        }

        private void forEachParent(@Nonnull Resource resource,
                                      @Nonnull Consumer<Resource> consumer) {
            forEach(!backward, predicate, resource, consumer);
        }

        public Builder visitAll(Model m) {
            if (backward) {
                for (NodeIterator it = m.listObjectsOfProperty(predicate); it.hasNext(); ) {
                    RDFNode node = it.next();
                    if (node.isResource())
                        visit(node.asResource());
                }
            } else {
                for (ResIterator it = m.listSubjectsWithProperty(predicate); it.hasNext(); )
                    visit(it.next());
            }
            return this;
        }

        public Builder visit(@Nonnull Resource resource) {
            Preconditions.checkArgument(resource.getModel() != null);
            Queue<Resource> queue = new LinkedList<>();
            queue.add(resource);
            while (!queue.isEmpty()) {
                Resource res = queue.remove();
                if (counts.containsKey(res) || leafs.contains(res)) continue;
                Set<Resource> children = new HashSet<>();
                forEachChild(res, children::add);
                children.remove(res);
                if (children.isEmpty()) {
                    leafs.add(res);
                } else {
                    counts.put(res, counts.getOrDefault(res, 0) + 1);
                    queue.addAll(children);
                }
                updateCounts(res);
            }
            return this;
        }

        private void updateCounts(@Nonnull Resource leaf) {
            Set<Resource> visited = new HashSet<>();
            Stack<Resource> stack = new Stack<>();
            visited.add(leaf);
            forEachParent(leaf, stack::push);
            while (!stack.empty()) {
                Resource child = stack.pop();
                if (visited.contains(child)) continue;
                visited.add(child);
                counts.put(child, counts.getOrDefault(child, 0) + 1);
                forEachParent(child, stack::push);
            }
//            System.out.println("----\n" + counts.entrySet().stream()
//                    .map(e -> e.getKey() + " -> " + e.getValue())
//                    .reduce((l, r) -> l + "\n" + r).orElse(""));
        }

        /** USE ONLY FOR TESTING PURPOSES */
        @Deprecated
        Map<Resource, Integer> getCountEstimates() {
            return countEstimates;
        }

        public FixedCacheTransitiveClosureGetter build() {
            if (getClass().desiredAssertionStatus()) {
                countEstimates = new HashMap<>(counts);
                leafs.forEach(l -> countEstimates.put(l, 1));
            }
            assert counts.entrySet().stream().noneMatch(e -> e.getValue() < 0);
            assert leafs.stream().noneMatch(counts::containsKey);
            Queue<Resource> queue = new LinkedList<>();
            Set<Resource> set = new HashSet<>();
            leafs.forEach(l -> forEachParent(l, set::add));
            noChildrenCache = new LinkedHashSet<>(noChildrenCacheSize);
            leafs.stream().limit(noChildrenCacheSize).forEach(noChildrenCache::add);
            queue.addAll(set);

            Set<Resource> visited = leafs;
            leafs = null;

            while (!queue.isEmpty()) {
                Resource r = queue.remove();
                if (visited.contains(r)) continue;
                visited.add(r);
                if (counts.get(r) > threshold)
                    compact(r);
                forEachParent(r, queue::add);
            }
            return new FixedCacheTransitiveClosureGetter(predicate, backward, cache,
                    noChildrenCache, noChildrenCacheSize/*, lruCacheSize*/);
        }

        private void compact(@Nonnull Resource r) {
            Set<Resource> set = getDFS(backward, predicate, r).toCollection(HashSet::new);
            set = Collections.unmodifiableSet(set);
            cache.put(r, set);
            int d = set.size() - 1;
            getDFS(!backward, predicate, r).forEachRemaining(p -> counts.put(p, counts.get(p)-d));
        }
    }
}

package br.ufsc.lapesd.unserved.testbench.benchmarks.wsc08;

import br.ufsc.lapesd.unserved.testbench.benchmarks.ComposeWorkflowApp;
import br.ufsc.lapesd.unserved.testbench.benchmarks.compose.TimesExperimentResult;
import br.ufsc.lapesd.unserved.testbench.benchmarks.experiment.Experiment;
import br.ufsc.lapesd.unserved.testbench.benchmarks.experiment.ExperimentException;
import br.ufsc.lapesd.unserved.testbench.benchmarks.experiment.ExperimentResult;
import br.ufsc.lapesd.unserved.testbench.input.RDFInputFile;
import br.ufsc.lapesd.unserved.testbench.iri.Unserved;
import com.google.gson.Gson;
import org.apache.commons.io.FileUtils;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFFormat;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.util.*;
import java.util.stream.Collectors;

public class ComposeWorkflowAppWscExperiment implements Experiment {
    @Nonnull private final WscFactors f;
    private WscProblemConverter.Result files;
    private boolean loadWscFiles;

    public ComposeWorkflowAppWscExperiment(@Nonnull WscFactors factors) {
        this(factors, false);
    }

    public ComposeWorkflowAppWscExperiment(@Nonnull WscFactors factors, boolean loadWscFiles) {
        this.loadWscFiles = loadWscFiles;
        this.f = factors;
    }

    @Override
    public void setUp() throws Exception {
        if (f.getSubDir() != null) {
            File d = new File(f.getWscConvertedRoot(), f.getSubDir()).getCanonicalFile();
            if (!d.exists()) throw new IOException("Dir does not exist: " + d);
            else if (!d.isDirectory()) throw new IOException("Not a dir: " + d);

            if (f.getKnown() == null)
                throw new IllegalArgumentException("\"known\" is mandatory with \"subdir\"");
            if (f.getWanted() == null)
                throw new IllegalArgumentException("\"wanted\" is mandatory with \"subdir\"");

            File[] files = {
                    new File(d, "taxonomy.ttl").getCanonicalFile(),
                    new File(d, "services.ttl").getCanonicalFile(),
                    new File(d, f.getKnown()).getCanonicalFile(),
                    new File(d, f.getWanted()).getCanonicalFile(),
            };
            for (File f : files) {
                if (!f.exists()) throw new IOException("File does not exist: " + f);
                else if (!f.isFile()) throw new IOException("Not a file: " + f);
            }

            File kb = new File(d, "kb.rules");
            if (!kb.exists()) kb = null;

            this.files = new WscProblemConverter.Result(files[0], files[1], files[2], files[3], kb);
        } else {
            File problemDir = new File(f.getWscConvertedRoot(), String.format("%02d", f.getProblem()));
            files = WscProblemConverter.load(problemDir, loadWscFiles);
        }
    }

    @Override
    public ExperimentResult run() throws ExperimentException {
        try {
            File workflowFile = Files.createTempFile("workflow", ".ttl").toFile();
            workflowFile.deleteOnExit();
            File timesFile = Files.createTempFile("times", ".json").toFile();
            timesFile.deleteOnExit();

            run(workflowFile, timesFile);

            try (RDFInputFile in = RDFInputFile.createOwningFile(workflowFile, RDFFormat.TURTLE)) {
                if (!getFactors().getDismissOutput() && !getFactors().getOnlyGraph() && !getFactors().getExecute() && in.getModel().isEmpty())
                    throw new ExperimentException("Empty workflow model!");
            }

            ExperimentResult result;
            result = new Gson().fromJson(new FileReader(timesFile), TimesExperimentResult.class);
            FileUtils.forceDelete(timesFile);
            return result;
        } catch (IOException e) {
            throw new ExperimentException(e);
        }
    }

    public void run(File workflowFile, File timesFile) throws ExperimentException {
        List<String> args = new ArrayList<>();
        if (getFactors().getChildXms() != null && !getFactors().getChildXms().isEmpty())
            args.add("-Xms" + getFactors().getChildXms());
        if (getFactors().getChildXmx() != null && !getFactors().getChildXmx().isEmpty())
            args.add("-Xmx" + getFactors().getChildXmx());
        if (getFactors().getChildJVMOpts() != null)
            args.addAll(Arrays.asList(getFactors().getChildJVMOpts().split(" ")));
        args.addAll(Arrays.asList(ComposeWorkflowApp.class.getName(),
                "--algorithm", getFactors().getAlgorithm().name(),
                "--preheat-count", String.valueOf(getFactors().getPreheatCount()),
                "--builder-factory", WscComposerBuilderFactory.class.getName(),
                "--builder-factory-args", getBuilderFactoryArgs(),
                "--component-reg-setup", WscComponentRegistrySetup.class.getName(),
                "--component-reg-setup-args", getComponentRegSetupArgs()));
        if (getFactors().getExecute()) args.add("--execute");
        if (getFactors().getDismissOutput()) args.add("--dismiss-output");
        if (getFactors().getOnlyGraph()) args.add("--only-graph");
        for (String uri : getWantedURIs()) args.addAll(Arrays.asList("--wanted", uri));
        if (files.rules != null)
            args.addAll(Arrays.asList("--rules", files.rules.getAbsolutePath()));

        args.addAll(Arrays.asList(
                "--workflow-out", workflowFile.getAbsolutePath(),
                "--times-out", timesFile.getAbsolutePath()));
        runChildJVM(args, Arrays.asList(files.taxonomyTTL, files.knownTTL, files.wantedTTL,
                files.servicesTTL));
    }

    private Set<String> getWantedURIs() throws ExperimentException {
        Model model = ModelFactory.createDefaultModel();
        try (FileInputStream inputStream = new FileInputStream(files.getWantedTTL())) {
            RDFDataMgr.read(model, inputStream, Lang.TURTLE);

            List<Resource> list = new ArrayList<>();
            try (QueryExecution ex = QueryExecutionFactory.create(QueryFactory.create(
                    "PREFIX u: <" + Unserved.PREFIX + ">\n" +
                            "SELECT ?var WHERE { ?var a u:Variable, u:Wanted. }"), model)) {
                ex.execSelect().forEachRemaining(s -> list.add(s.getResource("var")));
            }
            if (!list.stream().allMatch(Resource::isURIResource))
                throw new ExperimentException("Some wanted variables are blank nodes");
            return list.stream().map(Resource::getURI).collect(Collectors.toSet());
        } catch (IOException e) {
            throw new ExperimentException(e);
        } finally {
            model.close();
        }
    }

    private String getComponentRegSetupArgs() {
        Map<String, String> map = new HashMap<>();
        if (getFactors().getFailPoints() != null)
            map.put("failPoints", getFactors().getFailPoints().getAbsolutePath());
        return map.entrySet().stream().map(e -> e.getKey() + "=" + e.getValue())
                .reduce((l, r) -> l + ";" + r).orElse("");
    }

    private String getBuilderFactoryArgs() {
        Map<String, String> map = new HashMap<>();
        map.put("serviceCompression", getFactors().getServiceCompression() ? "true" : "false");
        map.put("backwardClosureOpt", getFactors().getBackwardClosureOpt() ? "true" : "false");
        map.put("preconditions", getFactors().getPreconditions() ? "true" : "false");
        map.put("naiveIO", getFactors().getNaiveIO() ? "true" : "false");
        map.put("combinedJN", getFactors().getCombinedJN() ? "true" : "false");
        map.put("listSubclasses", getFactors().getListSubclasses() ? "true" : "false");
        map.put("naiveTCG", getFactors().getNaiveTCG() ? "true" : "false");
        return map.entrySet().stream().map(e -> e.getKey() + "=" + e.getValue())
                .reduce((l, r) -> l + ";" + r).orElse("");
    }

    private void runChildJVM(List<String> mainArgs, List<File> descriptions)
            throws ExperimentException {
        String separator = System.getProperty("file.separator");
        String classpath = System.getProperty("java.class.path");
        String java = System.getProperty("java.home")
                + separator + "bin" + separator + "java";

        List<String> args = new ArrayList<>(Arrays.asList(java, "-cp", classpath
//                , "-ea", "-agentlib:jdwp=transport=dt_socket,server=y,address=5006"
        ));
        args.addAll(mainArgs);
        for (File d : descriptions) args.add(d.getAbsolutePath());

        try {
            Process child = new ProcessBuilder().command(args)
                    .redirectError(ProcessBuilder.Redirect.INHERIT)
                    .redirectOutput(ProcessBuilder.Redirect.INHERIT)
                    .start();
            int exitCode = child.waitFor();
            if (exitCode != 0)
                throw new ExperimentException("Child did not executed correctly.");
        } catch (IOException | InterruptedException e) {
            throw new ExperimentException("Process management exception", e);
        }
    }

    @Override
    public WscFactors getFactors() {
        return f;
    }

    @Override
    public void close() throws Exception { }
}

package br.ufsc.lapesd.unserved.testbench.process.actions;

import br.ufsc.lapesd.unserved.testbench.components.ComponentFactory;

public interface ActionRunnerFactory extends ComponentFactory {
    ActionRunner newInstance();
}

package br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service;

import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.And;
import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.Condition;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.cost.CostInfo;
import br.ufsc.lapesd.unserved.testbench.model.Variable;
import com.google.common.base.Preconditions;

import javax.annotation.Nonnull;
import java.util.*;

public class CombinedJumpNode extends AbstractNode implements JumpNode {
    private final @Nonnull Node delegate;
    private final @Nonnull Set<Variable> inputs;
    private final @Nonnull And preconditions;
    private final @Nonnull IOProviderHashSet ps;

    public CombinedJumpNode(@Nonnull Node delegate, @Nonnull ProviderSet ps) {
        super(new ArrayList<>());
        this.delegate = delegate;
        this.ps = ps.getConditions().isEmpty() ? new IOProviderHashSet()
                : new ConditionProviderHashSet();
        this.inputs = new HashSet<>();
        for (Variable t : ps.getTargets()) {
            if (!ps.getProviders(t).contains(delegate)) continue;
            this.inputs.add(t);
            this.ps.add(t, delegate, ps.getOutput(t, delegate));
        }
        Preconditions.checkArgument(!inputs.isEmpty());
        if (this.ps instanceof ConditionProviderHashSet) {
            ConditionProviderHashSet cps = (ConditionProviderHashSet) this.ps;
            List<Condition> conditions = new ArrayList<>();
            for (Condition c : ps.getConditions()) {
                if (!ps.getProviders(c).contains(delegate)) continue;
                cps.add(c, delegate, ps.getPostcondition(c, delegate));
                conditions.add(c);
            }
            this.preconditions = conditions.isEmpty() ? And.empty() : And.of(conditions);
        } else {
            this.preconditions = And.empty();
        }
    }

    @Nonnull
    public Node getDelegate() {
        return delegate;
    }

    @Nonnull
    public ProviderSet getProviderSet() {
        return ps;
    }

    @Nonnull
    @Override
    public Set<Variable> getInputs() {
        return inputs;
    }

    @Nonnull
    @Override
    public Set<Variable> getOutputs() {
        return inputs;
    }

    @Nonnull
    @Override
    public And getPreConditions() {
        return preconditions;
    }
    @Nonnull
    @Override
    public And getPostConditions() {
        return preconditions;
    }

    @Nonnull
    @Override
    public CostInfo getCostInfo() {
        return CostInfo.EMPTY;
    }

    @Override
    public Object getId() {
        return this;
    }

    @Override
    public String toString() {
        return String.format("CombinedJumpNode(%s)", delegate.toString());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CombinedJumpNode jumpNode = (CombinedJumpNode) o;
        return Objects.equals(delegate, jumpNode.delegate) &&
                Objects.equals(inputs, jumpNode.inputs) &&
                Objects.equals(preconditions, jumpNode.preconditions);
    }

    @Override
    public int hashCode() {
        return Objects.hash(delegate, inputs, preconditions);
    }
}

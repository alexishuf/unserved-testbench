package br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.lazy;

import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.Node;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.ProviderSet;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.SetMultimap;

import javax.annotation.Nonnull;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class NonEquivalentBackwardSetNodeProvider extends NaiveBackwardSetNodeProvider {
    @Nonnull
    @Override
    public Set<SetNodeEdge> getPredecessors(@Nonnull SetNode successor) {
        assert serviceGraph != null;

        SetMultimap<Set<Set<Node>>, ProtoNode> helper = HashMultimap.create();
        getProtoNodes(successor).forEach(pn -> {
            Set<Set<Node>> nextPredecessors = new HashSet<>();
            ProviderSet ps = getProviderSet(pn.nodes);
            if (ps != null)
                ps.getTargets().forEach(t -> nextPredecessors.add(ps.getProviders(t)));
            helper.put(nextPredecessors, pn);
        });

        Set<SetNodeEdge> edges = new HashSet<>();
        for (Set<Set<Node>> key : helper.keySet()) {
            List<ProtoNode> sorted = helper.get(key).stream()
                    .sorted(Comparator.comparing(pn -> pn.nodes.size()))
                    .collect(Collectors.toList());
            SetNodeEdge edge = sorted.get(0).toEdge(successor);

            List<SetNode> alts = sorted.stream().skip(1).map(ProtoNode::toSetNode)
                    .collect(Collectors.toList());

            ((FullSetNode)edge.getFrom()).setAlternatives(alts);
            for (int i = 0; i < alts.size(); i++) {
                List<SetNode> list = alts.stream().skip(i + 1).collect(Collectors.toList());
                ((FullSetNode)alts.get(i)).setAlternatives(list);
            }

            edges.add(edge);
        }
        return edges;
    }
}

package br.ufsc.lapesd.unserved.testbench.io_composer.backward;

import br.ufsc.lapesd.unserved.testbench.io_composer.graph.ReplicatedGraphAccessor;
import br.ufsc.lapesd.unserved.testbench.io_composer.impl.IOComposerInput;
import br.ufsc.lapesd.unserved.testbench.io_composer.state.MessagePairAction;
import br.ufsc.lapesd.unserved.testbench.io_composer.state.State;
import br.ufsc.lapesd.unserved.testbench.iri.Unserved;
import br.ufsc.lapesd.unserved.testbench.model.Message;
import br.ufsc.lapesd.unserved.testbench.model.Variable;
import br.ufsc.lapesd.unserved.testbench.util.BFSBGPIterator;
import br.ufsc.lapesd.unserved.testbench.util.BNodeShim;
import br.ufsc.lapesd.unserved.testbench.util.ModelStreams;
import es.usc.citius.hipster.model.Transition;
import org.apache.jena.query.*;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.OWL2;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;

import javax.annotation.Nullable;
import java.util.HashSet;
import java.util.Set;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * Provides access to a graph through an initial state, a goal state,
 * and a transition function
 */
public class NaiveBackwardGraphAccessor extends AbstractBackwardGraphAccessor implements BackwardReplicatedGraphAccessor<State, Double, Transition<Void, State>> {

    public NaiveBackwardGraphAccessor(Model model, Set<Variable> wanted) {
        super(model, wanted);
    }

    public NaiveBackwardGraphAccessor(IOComposerInput input) {
        this(input.getSkolemizedUnion(), input.getWantedAsVariables());
    }

    @Override
    protected Stream<Resource> getSubclassesOf(Resource aClass) {
        return StreamSupport.stream(Spliterators.spliteratorUnknownSize(
                BFSBGPIterator.from(model, aClass).backward(RDFS.subClassOf),
                Spliterator.DISTINCT | Spliterator.NONNULL), false);
    }

    @Override
    protected Stream<Variable> getKnown(@Nullable Resource type) {
        Stream<Variable> stream = ModelStreams.subjectsWithProperty(model, RDF.type,
                Unserved.ValueBoundVariable, Variable.class);
        return type == null ? stream
                : stream.filter(k -> k.getType() != null && k.getType().equals(type));
    }

    @Override
    protected Stream<MessagePairAction> getCandidates(Variable target, Set<Message> blacklist) {
        Resource type = target.getType() == null ? OWL2.Thing : target.getType();
        try (QueryExecution execution = QueryExecutionFactory.create(
                QueryFactory.create(String.format("PREFIX u: <" + Unserved.PREFIX + ">\n" +
                        "PREFIX rdfs: <" + RDFS.getURI() + ">\n" +
                        "SELECT DISTINCT ?ant ?cons ?var WHERE {\n" +
                        "  ?cons a u:Message.\n" +
                        "  ?cons u:when ?when.\n" +
                        "  ?when u:reactionTo ?ant.\n" +
                        "  {\n" +
                        "    ?cons (u:part/u:variable)+ ?var.\n" +
                        "    ?var u:type ?type.\n" +
                        "    ?type rdfs:subClassOf* %1$s.\n" +
                        "  } UNION {\n" +
                        "    ?binding u:bindingResource ?cons.\n" +
                        "     ?binding u:bindingProperty ?boundProperty.\n" +
                        "    ?binding u:bindingVariable ?boundVar.\n" +
                        "    ?boundVar (u:part/u:variable)+ ?var.\n" +
                        "    ?var u:type ?type." +
                        "    ?type rdfs:subClassOf* %1$s.\n" +
                        "  }\n" +
                        "}\n", BNodeShim.sparqlResource(type))), model)) {
            HashSet<MessagePairAction> set = new HashSet<>();
            ResultSet results = execution.execSelect();
            while (results.hasNext()) {
                QuerySolution row = results.next();
                Message cons = row.getResource("cons").as(Message.class);
                if (blacklist.contains(cons))
                    continue;
                Message ant = row.getResource("ant").as(Message.class);
                Variable resultVar = row.getResource("var").as(Variable.class);
                set.add(new MessagePairAction(ant, cons).addAssignment(resultVar, target));
            }
            return set.stream();
        }
    }

    @Override
    public void close() {
    }

    @Override
    public ReplicatedGraphAccessor duplicate(IOComposerInput input) {
        return new NaiveBackwardGraphAccessor(input);
    }
}

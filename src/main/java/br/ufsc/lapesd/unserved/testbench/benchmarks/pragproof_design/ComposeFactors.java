package br.ufsc.lapesd.unserved.testbench.benchmarks.pragproof_design;

import br.ufsc.lapesd.unserved.testbench.Algorithm;
import com.google.common.base.Preconditions;

import javax.annotation.Nonnull;
import java.io.Serializable;

public class ComposeFactors implements Serializable{
    protected final int preheatCount;
    protected final Algorithm algorithm;
    protected boolean execute;

    public ComposeFactors(int preheatCount, boolean execute, @Nonnull Algorithm algorithm) {
        Preconditions.checkNotNull(algorithm);
        this.preheatCount = preheatCount;
        this.execute = execute;
        this.algorithm = algorithm;
    }

    public int getPreheatCount() {
        return preheatCount;
    }

    public boolean getExecute() {
        return execute;
    }

    public Algorithm getAlgorithm() {
        return algorithm;
    }
}

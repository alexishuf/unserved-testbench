package br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.discovery;

import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.And;
import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.index.VariableSpecIndex;
import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.parsers.ConditionsParser;
import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.parsers.EmptyConditionsParser;
import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.parsers.SimpleSPINConditionsParser;
import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.parsers.VariableSpecParser;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.ServiceNode;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.cost.CostInfo;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.cost.CostInfoParser;
import br.ufsc.lapesd.unserved.testbench.iri.Unserved;
import br.ufsc.lapesd.unserved.testbench.model.Message;
import br.ufsc.lapesd.unserved.testbench.model.Variable;
import br.ufsc.lapesd.unserved.testbench.util.DangerousArrayListSet;
import br.ufsc.lapesd.unserved.testbench.util.NaiveTransitiveClosureGetter;
import br.ufsc.lapesd.unserved.testbench.util.TransitiveClosureGetter;
import com.google.common.base.Preconditions;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.vocabulary.RDFS;

import javax.annotation.Nonnull;
import java.util.ArrayList;

public class InputServiceIndex {
    private static final VariableSpecParser specsParser = new VariableSpecParser();
    private final CostInfoParser costInfoParser = new CostInfoParser();
    private VariableSpecIndex<ImmutablePair<Variable, ServiceNode>> inIdx;
    private TransitiveClosureGetter superClassGetter;
    private ConditionsParser conditionsParser;

    public InputServiceIndex(@Nonnull TransitiveClosureGetter superClassGetter,
                             boolean parseConditions) {
        conditionsParser = parseConditions ? new SimpleSPINConditionsParser()
                : new EmptyConditionsParser();
        Preconditions.checkArgument(superClassGetter.getPredicate().equals(RDFS.subClassOf));
        Preconditions.checkArgument(!superClassGetter.isBackward());
        this.superClassGetter = superClassGetter;
        inIdx = new VariableSpecIndex<>(superClassGetter);
    }

    public InputServiceIndex(@Nonnull TransitiveClosureGetter superClassGetter) {
        this(superClassGetter, false);
    }

    public InputServiceIndex() {
        this(new NaiveTransitiveClosureGetter(RDFS.subClassOf, false));
    }

    @Nonnull
    public TransitiveClosureGetter getSuperClassGetter() {
        return superClassGetter;
    }

    public void addModel(@Nonnull Model model) {
        inIdx = new VariableSpecIndex<>(superClassGetter);
        model.listSubjectsWithProperty(Unserved.reactionTo).forEachRemaining(when -> {
            Message ant = when.getPropertyResourceValue(Unserved.reactionTo).as(Message.class);
            DangerousArrayListSet<Variable> ins = new DangerousArrayListSet<>();
            specsParser.parseLists(ins::add, spec -> {}, ant);
            And pre = conditionsParser.parse(ant);
            model.listSubjectsWithProperty(Unserved.when, when).toList().stream()
                    .map(r -> r.as(Message.class)).forEach(cons ->
            {
                CostInfo c = costInfoParser.parse(cons);
                And post = conditionsParser.parse(cons);
                ServiceNode node = new ServiceNode(ant, cons, new ArrayList<>(), pre, post, c, ins);
                ins.forEach(in -> inIdx.put(in.asSpec(), ImmutablePair.of(in, node)));
            });
        });
    }

    public VariableSpecIndex<ImmutablePair<Variable, ServiceNode>> getConsumerInputs() {
        return inIdx;
    }

    public void clear() {
        inIdx = new VariableSpecIndex<>(superClassGetter);
    }
}

package br.ufsc.lapesd.unserved.testbench.io_composer.conditions.index;

import br.ufsc.lapesd.unserved.testbench.util.TransitiveClosureGetter;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.SetMultimap;
import org.apache.jena.rdf.model.Resource;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TransitiveIndex<V> {
    private final @Nonnull SetMultimap<Resource, V> index;
    private final @Nonnull TransitiveClosureGetter cGetter;
    private final Set<Resource> topResources = new HashSet<>();
    private boolean closureOnPut;

    public TransitiveIndex(@Nonnull TransitiveClosureGetter cGetter) {
        this(cGetter, false);
    }

    protected TransitiveIndex(@Nonnull TransitiveClosureGetter cGetter, boolean closureOnPut) {
        this.cGetter = cGetter;
        this.index = HashMultimap.create();
        this.closureOnPut = closureOnPut;
    }

    public static <V> TransitiveIndex<V> withClosureOnPut(@Nonnull TransitiveClosureGetter cGetter) {
        return new TransitiveIndex<>(cGetter, true);
    }

    public TransitiveIndex<V> addTopResource(@Nonnull Resource resource) {
        topResources.add(resource);
        return this;
    }

    public boolean isClosureOnPut() {
        return closureOnPut;
    }

    public void put(@Nonnull Resource resource, @Nonnull V value) {
        if (closureOnPut)
            cGetter.getClosureStream(resource).forEach(t -> index.put(t, value));
        else
            index.put(resource, value);
    }

    public boolean remove(@Nonnull Resource resource) {
        return !index.removeAll(resource).isEmpty();
    }

    public boolean remove(@Nonnull Resource resource, @Nonnull V value) {
        return index.remove(resource, value);
    }

    public @Nonnull Stream<V> getStream(@Nonnull Resource resource) {
        if (topResources.contains(resource)) {
            return index.values().stream();
        } else if (closureOnPut) {
            return index.get(resource).stream();
        } else {
            return cGetter.getClosureStream(resource).flatMap(t -> index.get(t).stream());
        }
    }

    @Nullable
    public V getFirst(@Nonnull Resource resource) {
        Iterator<V> it = get(resource).iterator();
        return it.hasNext() ? it.next() : null;
    }

    public @Nonnull Set<V> get(@Nonnull Resource resource) {
        if (topResources.contains(resource)) {
            return new HashSet<>(index.values());
        } else if (closureOnPut) {
            return index.get(resource);
        } else {
            return cGetter.getClosureStream(resource).flatMap(t -> index.get(t).stream())
                    .collect(Collectors.toSet());
        }
    }

}

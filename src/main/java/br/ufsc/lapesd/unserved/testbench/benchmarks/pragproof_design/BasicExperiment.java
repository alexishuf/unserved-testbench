package br.ufsc.lapesd.unserved.testbench.benchmarks.pragproof_design;

import br.ufsc.lapesd.unserved.testbench.AlgorithmFamily;
import br.ufsc.lapesd.unserved.testbench.benchmarks.compose.TimesExperimentResult;
import br.ufsc.lapesd.unserved.testbench.benchmarks.experiment.Experiment;
import br.ufsc.lapesd.unserved.testbench.benchmarks.experiment.ExperimentException;
import br.ufsc.lapesd.unserved.testbench.benchmarks.experiment.ExperimentResult;
import br.ufsc.lapesd.unserved.testbench.input.RDFInputFile;
import com.google.gson.Gson;
import org.apache.commons.io.FileUtils;
import org.apache.jena.riot.RDFFormat;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public abstract class BasicExperiment implements Experiment {
    private PPFactors factors;
    private File descriptions = null;
    private File nonDummyDescriptions = null;

    public BasicExperiment(PPFactors factors) {
        this.factors = factors;
    }

    @Override
    public void setUp() throws Exception {
        if (factors.getNonDummyChain() > 0) {
            descriptions = createDescription(factors.getChainLength(), factors.getAlternatives(),
                    factors.getIOCount(), "dummy", factors.getPreconditions());
            nonDummyDescriptions = createDescription(factors.getNonDummyChain(),
                    factors.getNonDummyAlternatives(), factors.getNonDummyIOCount(), null,
                    factors.getPreconditions());
        } else {
            descriptions = createDescription(factors.getChainLength(), factors.getAlternatives(),
                    factors.getIOCount(), null, factors.getPreconditions());
        }
    }

    @Override
    public ExperimentResult run() throws ExperimentException {
        try {
            File workflowFile = Files.createTempFile("workflow", ".ttl").toFile();
            workflowFile.deleteOnExit();
            File timesFile = Files.createTempFile("times", ".json").toFile();
            timesFile.deleteOnExit();

            List<File> fileList = new ArrayList<>();
            fileList.add(descriptions);
            if (nonDummyDescriptions != null) fileList.add(nonDummyDescriptions);
            run(workflowFile, timesFile, fileList);

            try (RDFInputFile input = RDFInputFile.createOwningFile(workflowFile, RDFFormat.TURTLE)) {
                if (!factors.getExecute())
                    if (factors.getAlgorithm().getFamily() == AlgorithmFamily.RESTdescPP) {
                        if (workflowFile.length() == 0)
                            throw new ExperimentException("Empty workflow model!");
                    } else if (input.getModel().isEmpty()) {
                        throw new ExperimentException("Empty workflow model!");
                    }
            }

            ExperimentResult result;
            result = new Gson().fromJson(new FileReader(timesFile), TimesExperimentResult.class);
            FileUtils.forceDelete(timesFile);
            return result;
        } catch (IOException e) {
            throw new ExperimentException(e);
        }
    }

    @Override
    public PPFactors getFactors() {
        return factors;
    }

    @Override
    public void close() throws Exception {
        if (descriptions != null) FileUtils.forceDelete(descriptions);
        if (nonDummyDescriptions != null) FileUtils.forceDelete(nonDummyDescriptions);
    }

    protected void runChildJVM(List<String> mainArgs, List<File> descriptions)
            throws ExperimentException {
        String separator = System.getProperty("file.separator");
        String classpath = System.getProperty("java.class.path");
        String java = System.getProperty("java.home")
                + separator + "bin" + separator + "java";

        List<String> args = new ArrayList<>(Arrays.asList(java, "-cp", classpath
//                , "-agentlib:jdwp=transport=dt_socket,server=y,address=5005"
        ));
        if (getFactors().getChildXms() != null && !getFactors().getChildXms().trim().isEmpty())
            args.add("-Xms" + getFactors().getChildXms());
        if (getFactors().getChildXmx() != null && !getFactors().getChildXmx().trim().isEmpty())
            args.add("-Xmx" + getFactors().getChildXmx());
        if (getFactors().getChildXss() != null && !getFactors().getChildXss().trim().isEmpty())
            args.add("-Xss" + getFactors().getChildXss());
        if (getFactors().getChildJVMOpts() != null)
            args.addAll(Arrays.asList(getFactors().getChildJVMOpts().split(" ")));

        args.addAll(mainArgs);
        for (File d : descriptions) args.add(d.getAbsolutePath());

        try {
            Process child = new ProcessBuilder().command(args)
                    .redirectError(ProcessBuilder.Redirect.INHERIT)
                    .redirectOutput(ProcessBuilder.Redirect.INHERIT)
                    .start();
            int exitCode = child.waitFor();
            if (exitCode != 0)
                throw new ExperimentException("Child did not executed correctly.");
        } catch (IOException | InterruptedException e) {
            throw new ExperimentException("Process management exception", e);
        }
    }

    protected abstract void run(File workflowFile, File timesFile, List<File> fileList)
            throws ExperimentException;
    protected abstract File createDescription(int chainLength, int alternatives, int conditions,
                                              String rel, boolean preconditions) throws Exception;
}

package br.ufsc.lapesd.unserved.testbench.util.jena;

import org.apache.jena.graph.Node;
import org.apache.jena.rdf.model.*;

public class RDFNodeModelReference  implements RDFNode {
    private final RDFNode referee;
    private final Model model;

    protected RDFNodeModelReference(RDFNode referee, Model model) {
        this.referee = referee;
        this.model = model;
    }

    public RDFNode getReferee() {
        return referee;
    }

    @Override
    public String toString() {
        return referee.toString();
    }

    @Override
    public boolean isAnon() {
        return referee.isAnon();
    }

    @Override
    public boolean isLiteral() {
        return referee.isLiteral();
    }

    @Override
    public boolean isURIResource() {
        return referee.isURIResource();
    }

    @Override
    public boolean isResource() {
        return referee.isResource();
    }

    @Override
    public <T extends RDFNode> T as(Class<T> view) {
        return referee.as(view);
    }

    @Override
    public <T extends RDFNode> boolean canAs(Class<T> view) {
        return referee.canAs(view);
    }

    @Override
    public Model getModel() {
        return model;
    }

    @Override
    public RDFNode inModel(Model m) {
        return referee.inModel(m);
    }

    @Override
    public Object visitWith(RDFVisitor rv) {
        return referee.visitWith(rv);
    }

    @Override
    public Resource asResource() {
        return referee.asResource();
    }

    @Override
    public Literal asLiteral() {
        return referee.asLiteral();
    }

    @Override
    public Node asNode() {
        return referee.asNode();
    }
}

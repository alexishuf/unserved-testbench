package br.ufsc.lapesd.unserved.testbench.io_composer.conditions.parsers;

import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.And;
import org.apache.jena.rdf.model.Resource;

import javax.annotation.Nonnull;

public interface ConditionsParser {
    /**
     * Parses the conditions applied to an unserved:Conditional instance.
     *
     * @param resource something that has unserved:condition properties.
     * @return A conjunction of all conditions.
     * @throws ConditionParsingException if some construct in the conditions is unsupported by
     *                                   the parser or conditions violate some constraint imposed
     *                                   by the parser.
     */
    @Nonnull And parse(@Nonnull Resource resource) throws ConditionParsingException;
}

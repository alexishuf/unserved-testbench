package br.ufsc.lapesd.unserved.testbench.components;


import br.ufsc.lapesd.unserved.testbench.input.RDFInput;
import org.apache.jena.rdf.model.Resource;

import javax.annotation.Nonnull;

public interface ComponentFactory {
    //TODO replace with getComponentResource()
    @Deprecated
    @Nonnull
    RDFInput getDescription();
    @Nonnull
    Resource getComponentResource();
}

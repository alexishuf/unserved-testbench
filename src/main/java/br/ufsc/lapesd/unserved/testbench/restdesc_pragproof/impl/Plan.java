package br.ufsc.lapesd.unserved.testbench.restdesc_pragproof.impl;

import br.ufsc.lapesd.unserved.testbench.iri.HTTP;
import br.ufsc.lapesd.unserved.testbench.restdesc_pragproof.n3.N3Model;
import br.ufsc.lapesd.unserved.testbench.restdesc_pragproof.n3.Reason;
import ch.ethz.inf.vs.semantics.parser.elements.N3Document;
import com.google.common.base.Preconditions;
import org.apache.jena.rdf.model.*;
import org.apache.jena.vocabulary.RDF;

import javax.annotation.Nonnull;
import java.util.*;
import java.util.stream.Collectors;

import static br.ufsc.lapesd.unserved.testbench.util.ModelStreams.objectsOfProperty;
import static br.ufsc.lapesd.unserved.testbench.util.ModelStreams.subjectsWithProperty;


public class Plan {
    private final N3Model n3model;
    private final ArrayList<RuleApplication> steps = new ArrayList<>();

    private Plan(N3Model n3Model, List<RuleApplication> steps) {
        this.n3model = n3Model;
        this.steps.addAll(steps);
    }

    public N3Model getN3model() {
        return n3model;
    }

    public int size() {
        return steps.size();
    }

    public RuleApplication first() {
        Preconditions.checkState(size() > 0);
        return steps.get(0);
    }

    public List<RuleApplication> asList() {
        return Collections.unmodifiableList(steps);
    }

    public static Plan fromProof(N3Document doc) {
        N3Model n3Model = new N3Model(doc);
        Resource proof = subjectsWithProperty(n3Model.getModel(),
                RDF.type, Reason.Proof, Resource.class).findAny().orElse(null);
        if (proof == null) return null; //no proof

        List<Resource> inferences = getInferencesTopologicalSort(proof);
        if (inferences == null) return null;
        List<RuleApplication> steps = inferences.stream()
                .map(i -> asRuleApplication(n3Model, i)).filter(a -> a != null)
                .collect(Collectors.toList());
        return new Plan(n3Model, steps);
    }

    private static RuleApplication asRuleApplication(@Nonnull N3Model n3model,
                                                     @Nonnull Resource inference) {
        Model consequence = ModelFactory.createDefaultModel();
        objectsOfProperty(inference, Reason.gives, Resource.class)
                .map(n3model::getFormulaModel).filter(m -> m != null)
                .map(N3Model::getModel)
                .filter(Plan::hasHttpRequest)
                .forEach(consequence::add);
        if (consequence.isEmpty()) {
            consequence.close();
            return null;
        }
        Statement stmt = inference.getProperty(Reason.rule);
        if (stmt == null || !stmt.getObject().isResource()) return null;
        Resource extraction = stmt.getResource();

        stmt = extraction.getProperty(Reason.gives);
        if (stmt == null || !stmt.getObject().isResource()) return null;
        Resource ruleFormula = stmt.getResource();

        return new RuleApplication(n3model, ruleFormula, consequence);
    }

    private static List<Property> httpRequestHints = null;
    private static List<Property> getHttpRequestHints() {
        if (httpRequestHints == null) {
            httpRequestHints = Arrays.asList(HTTP.methodName, HTTP.mthd, HTTP.resp, HTTP.body);
        }
        return httpRequestHints;
    }

    private static boolean hasHttpRequest(Model model) {
        for (Property p : getHttpRequestHints()) {
            if (subjectsWithProperty(model, p, Resource.class).findAny().isPresent())
                return true;
        }
        return false;
    }

    private static List<Resource> getInferencesTopologicalSort(Resource proof) {
        List<Resource> inferences = new ArrayList<>();
        HashSet<Resource> visited = new HashSet<>();
        Stack<Resource> stack = new Stack<>();
        stack.addAll(objectsOfProperty(proof, Reason.component, Resource.class)
                .collect(Collectors.toSet()));
        while (!stack.isEmpty()) {
            Resource step = stack.pop();
            if (visited.contains(step)) return null;
            visited.add(step);
            if (step.hasProperty(RDF.type, Reason.Inference))
                inferences.add(step);
            /* It is an RDFList, but the parser is too buggy to realize it */
            stack.addAll(objectsOfProperty(step, Reason.evidence, Resource.class)
                    .collect(Collectors.toSet()));
        }
        Collections.reverse(inferences);
        return inferences;
    }
}

package br.ufsc.lapesd.unserved.testbench.io_composer.impl;

import br.ufsc.lapesd.unserved.testbench.benchmarks.Utils;
import br.ufsc.lapesd.unserved.testbench.io_composer.IOComposerTimes;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Multimaps;

import javax.annotation.Nonnull;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class DefaultIOComposerTimes implements IOComposerTimes {
    private Map<String, Long> totals = new HashMap<>();
    private ListMultimap<String, Long> times = ArrayListMultimap.create();
    private ListMultimap<String, Long> memSamples = Multimaps.synchronizedListMultimap(
            ArrayListMultimap.create());
    private final DefaultIOComposerTimes global;

    public DefaultIOComposerTimes() {
        global = new DefaultIOComposerTimes(1);
    }

    private DefaultIOComposerTimes(int dummy) {
        global = null;
    }

    protected DefaultIOComposerTimes(DefaultIOComposerTimes other) {
        totals.putAll(other.totals);
        times.putAll(other.times);
        memSamples.putAll(other.memSamples);
        global = other.getGlobal();
    }

    @Nonnull
    @Override
    public DefaultIOComposerTimes duplicate() {
        return new DefaultIOComposerTimes(this);
    }

    @Override
    public DefaultIOComposerTimes getGlobal() {
        return global;
    }

    public synchronized void addTime(String name, long value, TimeUnit timeUnit) {
        if (global != null)
            global.addTime(name, value, timeUnit);
        long nanos = TimeUnit.NANOSECONDS.convert(value, timeUnit);
        totals.put(name, totals.getOrDefault(name, 0L) + nanos);

        times.get(name).add(nanos);
    }

    @Override
    public Set<String> getNames() {
        return totals.keySet();
    }

    @Override
    public long getTotal(String name, TimeUnit timeUnit) {
        if (!totals.containsKey(name)) throw new NoSuchElementException(name);
        return timeUnit.convert(totals.get(name), TimeUnit.NANOSECONDS);
    }

    @Override
    public List<Long> getTimes(String name, TimeUnit timeUnit) {
        if (!times.containsKey(name)) throw new NoSuchElementException(name);
        return times.get(name).stream().map(n -> timeUnit.convert(n, TimeUnit.NANOSECONDS))
                .collect(Collectors.toList());
    }

    @Override
    public Set<String> getMemNames() {
        return memSamples.keySet();
    }

    @Override
    public long getMax(@Nonnull String name, long defaultValue) {
        return memSamples.get(name).stream().max(Long::compareTo).orElse(defaultValue);
    }

    @Override
    public long getMax(@Nonnull String name) throws NoSuchElementException {
        long max = getMax(name, Long.MIN_VALUE);
        if (max == Long.MIN_VALUE) throw new NoSuchElementException(name);
        return max;
    }

    @Override
    public double getAvg(@Nonnull String name) {
        List<Long> longs = memSamples.get(name);
        return longs.stream().reduce(Long::sum).orElse(0L) / (double)longs.size();
    }

    public void takeMemSample(@Nonnull String name) {
        addMemSample(name, Utils.usedMemory());
    }

    public void addMemSample(@Nonnull String name, long bytes) {
        if (global != null)
            global.addMemSample(name, bytes);
        memSamples.get(name).add(bytes);
    }

    @Override
    public List<Long> getMemSamples(@Nonnull String name) {
        if (!memSamples.containsKey(name)) throw new NoSuchElementException(name);
        return memSamples.get(name);
    }
}

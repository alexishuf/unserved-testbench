package br.ufsc.lapesd.unserved.testbench.util.jena;


import br.ufsc.lapesd.unserved.testbench.util.CloseableIterator;
import org.apache.jena.ext.com.google.common.base.Preconditions;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Optional;
import java.util.function.Function;

/**
 * Similar to {@link ResultSetIterator}, bu the transform function may also filter out values.
 */
public class ResultSetFilteredIterator<E> implements CloseableIterator<E> {
    private final QueryExecution execution;
    private final ResultSet resultSet;
    private final Function<QuerySolution, Optional<E>> transform;
    private E next;

    public ResultSetFilteredIterator(@Nonnull ResultSet resultSet,
                                     @Nonnull Function<QuerySolution, Optional<E>> transform,
                                     @Nullable QueryExecution execution) {
        this.execution = execution;
        this.resultSet = resultSet;
        this.transform = transform;
        advance();
    }

    @Override
    public void close() throws Exception {
        if (execution != null)
            execution.close();
    }

    @Override
    public boolean hasNext() {
        return next != null;
    }

    @Override
    public E next() {
        Preconditions.checkState(hasNext());
        E current = this.next;
        advance();
        return current;
    }

    private void advance() {
        this.next = null;
        while (resultSet.hasNext()) {
            Optional<E> opt = transform.apply(resultSet.next());
            if (opt.isPresent()) {
                next = opt.get();
                return;
            }
        }
    }
}

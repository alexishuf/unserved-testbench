package br.ufsc.lapesd.unserved.testbench.components.expressions.evaluator.subclass;

import br.ufsc.lapesd.unserved.testbench.components.expressions.ClassExpression;
import br.ufsc.lapesd.unserved.testbench.components.expressions.evaluator.ClassExpressionEvaluator.Result;
import br.ufsc.lapesd.unserved.testbench.components.expressions.evaluator.Step;
import org.apache.jena.rdf.model.Resource;

import javax.annotation.Nonnull;
import java.util.Collections;

public class NamesSubclassStep extends Step<Resource> {
    @Nonnull private final ClassExpression expression;

    public NamesSubclassStep(@Nonnull ClassExpression expression) {
        super(Collections.emptySet());
        this.expression = expression;
    }

    @Override
    protected Result performEvaluate(Resource aClass) {
        if (Helper.isSubclass(aClass, expression)) return Result.TRUE;
        return Result.INDETERMINATE;
    }
}

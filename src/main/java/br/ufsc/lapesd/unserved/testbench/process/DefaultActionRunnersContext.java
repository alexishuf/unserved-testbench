package br.ufsc.lapesd.unserved.testbench.process;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.stream.Collectors;

public class DefaultActionRunnersContext implements ActionRunnersContext {
    private static Logger logger = LoggerFactory.getLogger(DefaultActionRunnersContext.class);
    private Map<Object, Set<ActionContext>> actionContexts = new HashMap<>();

    @Override
    synchronized public void storeActionContext(Object key, ActionContext context) {
        actionContexts.getOrDefault(key, actionContexts.put(key, new HashSet<>())).add(context);
    }

    @Override
    synchronized public <T extends ActionContext> Collection<T> getActionContexts(Object key, Class<T> type) {
        return Collections.unmodifiableCollection(
                actionContexts.getOrDefault(key, Collections.emptySet()).stream()
                        .filter(c -> type.isAssignableFrom(c.getClass())).map(c -> (T)c)
                        .collect(Collectors.toList()));
    }

    @Override
    synchronized public Collection<? extends ActionContext> getActionContexts(Object key) {
        return Collections.unmodifiableCollection(
                actionContexts.getOrDefault(key, Collections.emptySet()));
    }

    @Override
    synchronized public void removeActionContext(ActionContext context) {
        Set<? extends ActionContext> set = actionContexts.getOrDefault(context.getKey(), null);
        if (set == null) return;
        set.remove(context);
        if (set.isEmpty()) actionContexts.remove(context.getKey());
    }

    @Override
    synchronized public void close() throws ActionContextsCloseException {
        List<Exception> exceptions = new ArrayList<>();
        List<Set<? extends ActionContext>> sets;
        sets = actionContexts.values().stream().collect(Collectors.toList());
        for (Set<? extends ActionContext> set : sets) {
            for (ActionContext context : set) {
                try {
                    context.close();
                } catch (Exception e) {
                    logger.error("ActionContext threw from close()", e);
                    exceptions.add(e);
                }
            }
        }

        if (!exceptions.isEmpty())
            throw new ActionContextsCloseException(exceptions);
    }
}

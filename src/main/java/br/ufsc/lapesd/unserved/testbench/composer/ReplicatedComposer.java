package br.ufsc.lapesd.unserved.testbench.composer;

import br.ufsc.lapesd.unserved.testbench.process.Interpreter;
import br.ufsc.lapesd.unserved.testbench.replication.ReplicatedExecutionException;

public interface ReplicatedComposer extends Composer {
    @Deprecated
    Interpreter.Task run(Interpreter interpreter) throws ReplicatedExecutionException;
}

package br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph;

import java.util.Iterator;

public interface Alternatives<T extends Alternative<?>> {
    Iterator<T> iterator();
    boolean remove(T object);
    Alternatives<T> duplicate();
}

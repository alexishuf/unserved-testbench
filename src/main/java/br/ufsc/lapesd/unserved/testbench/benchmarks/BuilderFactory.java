package br.ufsc.lapesd.unserved.testbench.benchmarks;

import br.ufsc.lapesd.unserved.testbench.Algorithm;
import br.ufsc.lapesd.unserved.testbench.io_composer.IOComposerBuilder;

import javax.annotation.Nonnull;

public interface BuilderFactory {
    void setArgs(@Nonnull String args);
    IOComposerBuilder get(Algorithm algorithm);
}

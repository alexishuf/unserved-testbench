package br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.Map;

/**
 * Obtains lists of sets of nodes whose outputs satisfy the inputs of a node.
 */
public interface ProviderSetsSupplier extends BuilderDelegate {

    /**
     * Creates a map from all nodes in targetNodes to lists of {@link ProviderSet} instances that,
     * together with known variables, satisfy all of that node's inputs and preconditions.
     *
     * @param targetNodes nodes that must have inputs and preconditions satisfied.
     * @return A Map, as described. Unsatisfiable target nodes ARE included in the map,
     *         with unsatisfied ProviderSet instances.
     */
    @Nonnull
    Map<Node, ProviderSet> get(@Nonnull Collection<Node> targetNodes);

    /**
     * Notifies the supplier that the given nodes where changed or added.
     * @param nodes collection of nodes.
     */
    void update(@Nonnull Collection<Node> nodes) ;
}

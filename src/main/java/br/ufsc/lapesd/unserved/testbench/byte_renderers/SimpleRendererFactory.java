package br.ufsc.lapesd.unserved.testbench.byte_renderers;

import br.ufsc.lapesd.unserved.testbench.input.NonClosingRDFInput;
import br.ufsc.lapesd.unserved.testbench.input.RDFInput;
import br.ufsc.lapesd.unserved.testbench.input.RDFInputModel;
import br.ufsc.lapesd.unserved.testbench.iri.Testbench;
import br.ufsc.lapesd.unserved.testbench.iri.UnservedC;
import br.ufsc.lapesd.unserved.testbench.iri.UnservedJ;
import br.ufsc.lapesd.unserved.testbench.util.RDFListWriter;
import com.google.common.base.Preconditions;
import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.graph.Graph;
import org.apache.jena.graph.compose.MultiUnion;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;
import org.apache.jena.riot.RDFFormat;
import org.apache.jena.vocabulary.OWL;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;

import javax.annotation.Nonnull;
import java.lang.reflect.Constructor;
import java.util.Collection;
import java.util.HashSet;
import java.util.stream.Stream;

public class SimpleRendererFactory implements RendererFactory {
    private final RDFInput description;
    private final Resource runner;
    private Constructor<? extends Renderer> constructor;

    public SimpleRendererFactory(@Nonnull Class<? extends Renderer> implementation,
                                 @Nonnull Class<? extends RendererFactory> factory,
                                 @Nonnull Collection<Resource> classes,
                                 @Nonnull Collection<Resource> representationClasses) {
        Preconditions.checkArgument(!classes.isEmpty());
        Preconditions.checkArgument(!representationClasses.isEmpty());
        Preconditions.checkArgument(classes.stream().allMatch(r -> r.getModel() != null));
        Preconditions.checkArgument(representationClasses.stream().allMatch(r -> r.getModel() != null));
        try {
            constructor = implementation.getConstructor();
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e);
        }

        Model newModel = ModelFactory.createDefaultModel();
        newModel.setNsPrefix("tb", Testbench.PREFIX);
        HashSet<Graph> graphs = new HashSet<>();
        graphs.add(newModel.getGraph());
        Stream.concat(classes.stream(), representationClasses.stream())
                .map(r -> r.getModel().getGraph()).forEach(graphs::add);
        MultiUnion union = new MultiUnion(graphs.toArray(new Graph[graphs.size()]));
        Model model = ModelFactory.createModelForGraph(union);

        runner = model.createResource(Testbench.PREFIX + implementation.getSimpleName());
        model.add(runner, RDF.type, UnservedC.Renderer);
        model.add(runner, RDF.type, UnservedC.Component);
        model.add(runner, UnservedJ.factoryClassName, ResourceFactory.createTypedLiteral(
                factory.getName(), XSDDatatype.XSDstring));
        model.add(runner, UnservedC.valueClass, createUnionClass(model, classes));
        model.add(runner, UnservedC.representationClass,
                createUnionClass(model, representationClasses));
        description = new RDFInputModel(model, RDFFormat.TURTLE);
    }

    private Resource createUnionClass(Model model, Collection<Resource> classes) {
        Resource inner = model.createResource().addProperty(RDF.type, OWL.Class)
                .addProperty(RDF.type, RDFS.Class)
                .addProperty(OWL.unionOf, RDFListWriter.write(model, classes));
        return model.createResource().addProperty(RDF.type, RDFS.Class)
                .addProperty(OWL.equivalentClass, inner);
    }

    @Nonnull
    @Override
    public RDFInput getDescription() {
        return new NonClosingRDFInput(description);
    }

    @Nonnull
    @Override
    public Resource getComponentResource() {
        return runner;
    }

    @Override
    public Renderer newInstance() {
        try {
            return constructor.newInstance();
        } catch (ReflectiveOperationException e) {
            throw new RuntimeException(e);
        }
    }
}

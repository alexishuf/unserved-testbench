package br.ufsc.lapesd.unserved.testbench.composer;

public class ComposerException extends RuntimeException {
    public ComposerException(String s) {
        super(s);
    }

    public ComposerException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public ComposerException(Throwable throwable) {
        super(throwable);
    }
}

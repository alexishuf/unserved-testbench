package br.ufsc.lapesd.unserved.testbench.util.jena;

import org.apache.jena.graph.*;
import org.apache.jena.shared.AddDeniedException;
import org.apache.jena.shared.DeleteDeniedException;
import org.apache.jena.shared.PrefixMapping;
import org.apache.jena.util.iterator.ExtendedIterator;

/**
 * A Graph adaptor that swallows close() calls.
 */
public class UncloseableGraph implements Graph {
    private final Graph graph;

    public UncloseableGraph(Graph graph) {
        this.graph = graph;
    }

    @Override
    public boolean dependsOn(Graph other) {
        return graph.dependsOn(other);
    }

    @Override
    public TransactionHandler getTransactionHandler() {
        return graph.getTransactionHandler();
    }

    @Override
    public Capabilities getCapabilities() {
        return graph.getCapabilities();
    }

    @Override
    public GraphEventManager getEventManager() {
        return graph.getEventManager();
    }

    @Override
    public GraphStatisticsHandler getStatisticsHandler() {
        return graph.getStatisticsHandler();
    }

    @Override
    public PrefixMapping getPrefixMapping() {
        return graph.getPrefixMapping();
    }

    @Override
    public void add(Triple t) throws AddDeniedException {
        graph.add(t);
    }

    @Override
    public void delete(Triple t) throws DeleteDeniedException {
        graph.delete(t);
    }

    @Override
    public ExtendedIterator<Triple> find(Triple m) {
        return graph.find(m);
    }

    @Override
    public ExtendedIterator<Triple> find(Node s, Node p, Node o) {
        return graph.find(s, p, o);
    }

    @Override
    public boolean isIsomorphicWith(Graph g) {
        return graph.isIsomorphicWith(g);
    }

    @Override
    public boolean contains(Node s, Node p, Node o) {
        return graph.contains(s, p, o);
    }

    @Override
    public boolean contains(Triple t) {
        return graph.contains(t);
    }

    @Override
    public void clear() {
        graph.clear();
    }

    @Override
    public void remove(Node s, Node p, Node o) {
        graph.remove(s, p, o);
    }

    @Override
    public void close() {
        /* pass */
    }

    @Override
    public boolean isEmpty() {
        return graph.isEmpty();
    }

    @Override
    public int size() {
        return graph.size();
    }

    @Override
    public boolean isClosed() {
        return graph.isClosed();
    }
}

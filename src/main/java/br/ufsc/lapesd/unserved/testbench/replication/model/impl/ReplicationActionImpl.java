package br.ufsc.lapesd.unserved.testbench.replication.model.impl;

import br.ufsc.lapesd.unserved.testbench.iri.UnservedP;
import br.ufsc.lapesd.unserved.testbench.process.model.Action;
import br.ufsc.lapesd.unserved.testbench.process.model.ActionFactory;
import br.ufsc.lapesd.unserved.testbench.replication.model.Replication;
import br.ufsc.lapesd.unserved.testbench.replication.model.ReplicationAction;
import br.ufsc.lapesd.unserved.testbench.util.ImplementationByType;
import br.ufsc.lapesd.unserved.testbench.util.ModelStreams;
import org.apache.jena.enhanced.EnhGraph;
import org.apache.jena.enhanced.EnhNode;
import org.apache.jena.enhanced.Implementation;
import org.apache.jena.graph.Node;
import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.impl.ResourceImpl;
import org.apache.jena.vocabulary.RDF;

import java.util.Arrays;

public class ReplicationActionImpl extends ResourceImpl implements ReplicationAction {
    public static Implementation factory = new ImplementationByType(
            Replication.ReplicationAction.asNode(),
            Arrays.asList(Replication.wrappedAction.asNode(), Replication.containerId.asNode())) {
        @Override
        public EnhNode wrap(Node node, EnhGraph eg) {
            return new ReplicationActionImpl(node, eg);
        }
    };

    public ReplicationActionImpl(Node n, EnhGraph m) {
        super(n, m);
    }

    @Override
    public Action getWrappedAction() {
        return ModelStreams.objectsOfProperty(this, Replication.wrappedAction, Resource.class)
                .filter(r -> r.hasProperty(RDF.type, UnservedP.Action))
                .map(ActionFactory::asAction).findFirst().orElse(null);
    }

    @Override
    public String getContainerId() {
        return ModelStreams.objectsOfProperty(this, Replication.containerId, Literal.class)
                .map(Literal::getLexicalForm).findFirst().orElse(null);
    }
}

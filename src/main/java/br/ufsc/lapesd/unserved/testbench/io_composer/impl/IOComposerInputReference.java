package br.ufsc.lapesd.unserved.testbench.io_composer.impl;

import br.ufsc.lapesd.unserved.testbench.composer.ComposerException;
import br.ufsc.lapesd.unserved.testbench.composer.impl.AbstractComposerInput;
import br.ufsc.lapesd.unserved.testbench.input.RDFInput;
import br.ufsc.lapesd.unserved.testbench.model.Variable;
import br.ufsc.lapesd.unserved.testbench.util.SkolemizerMap;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.reasoner.rulesys.Rule;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Set;

public class IOComposerInputReference extends AbstractComposerInput implements IOComposerInput {

    public IOComposerInputReference(IOComposerInput parent) {
        super(parent);
    }

    @Override
    public List<RDFInput> getInputs() {
        return getParent().getInputs();
    }

    @Override
    public void addInput(RDFInput input) {
        getParent().addInput(input);
    }

    @Override
    public void addRulesFile(File file) throws IOException {
        getParent().addRulesFile(file);
    }

    @Nonnull
    @Override
    public List<Rule> customRules() {
        return getParent().customRules();
    }

    @Nonnull
    @Override
    public Set<Resource> getWanted() {
        return getParent().getWanted();
    }

    @Nonnull
    @Override
    public Set<Resource> getKnown() {
        return getParent().getKnown();
    }

    private IOComposerInput getParent() {
        return (IOComposerInput)parent;
    }

    @Nonnull
    @Override
    public Set<Variable> getWantedAsVariables() {
        return getParent().getWantedAsVariables();
    }

    @Nonnull
    @Override
    public Set<Variable> getKnownAsVariables() {
        return getParent().getKnownAsVariables();
    }

    @Override
    public void addWanted(@Nonnull Resource resource) {
        getParent().addWanted(resource);
    }

    @Override
    @Nonnull
    public Model initSkolemizedUnion(boolean basicReasoning, boolean inputHasNoSkolemized) throws IOException {
        return getParent().initSkolemizedUnion(true, inputHasNoSkolemized);
    }

    @Override
    @Nonnull
    public Model getSkolemizedUnion() {
        return getParent().getSkolemizedUnion();
    }

    @Override
    public SkolemizerMap getSkolemizerMap() {
        return getParent().getSkolemizerMap();
    }

    @Override
    protected void closeImpl() throws ComposerException {
        /* pass */
    }
}

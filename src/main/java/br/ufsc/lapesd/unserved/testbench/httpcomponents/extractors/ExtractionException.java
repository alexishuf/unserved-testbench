package br.ufsc.lapesd.unserved.testbench.httpcomponents.extractors;

public class ExtractionException extends Exception {
    public ExtractionException(String s) {
        super(s);
    }

    public ExtractionException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public ExtractionException(String s, Throwable throwable, boolean b, boolean b1) {
        super(s, throwable, b, b1);
    }
}

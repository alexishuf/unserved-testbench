package br.ufsc.lapesd.unserved.testbench.io_composer;

import br.ufsc.lapesd.unserved.testbench.composer.ComposerException;
import br.ufsc.lapesd.unserved.testbench.io_composer.impl.IOComposerInput;
import org.apache.jena.graph.Graph;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.sparql.graph.UnmodifiableGraph;

import javax.annotation.Nonnull;

public abstract class AbstractIOComposer implements IOComposer {
    @Nonnull protected final Model model;
    @Nonnull protected final IOComposerInput ci;

    private Model unmodifiableModel;

    public AbstractIOComposer(@Nonnull IOComposerInput ci) {
        this.ci = ci;
        this.model = ci.getSkolemizedUnion();
    }

    public Model getUnmodifiableModel() {
        if (unmodifiableModel == null) {
            Graph graph = new UnmodifiableGraph(model.getGraph());
            unmodifiableModel = ModelFactory.createModelForGraph(graph);
        }
        return unmodifiableModel;
    }

    @Override
    public void close() throws ComposerException {
        ci.close();
    }


}

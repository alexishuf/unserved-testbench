package br.ufsc.lapesd.unserved.testbench.input;

import javax.annotation.Nullable;
import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * Abstracts input source
 */
public interface Input extends Closeable {
    String getMediaType();
    File toFile() throws IOException;
    String toURI() throws IOException;

    /**
     * If relevant to the media type (RDF and XML-based formats), and known returns the base URI
     *
     * @return base URI or null if unknown or irrelevant
     */
    @Nullable String getBaseURI();

    /**
     * Gets a input stream for reading the input. Ownership is given to the caller.
     * Multiple streams may be created from the same Input object, each will give the same data.
     *
     * @return a new InputStream
     */
    InputStream createInputStream() throws IOException;
}

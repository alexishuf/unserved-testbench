package br.ufsc.lapesd.unserved.testbench.util;

import com.google.common.base.Preconditions;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class Combinations {
    @Nonnull
    public static <T> Iterator<List<T>> generateLists(List<List<T>> lists) {
        return new ListIterator<T>(lists);
    }
    @Nonnull
    public static <T> Stream<List<T>> generateListsStream(List<List<T>> lists) {
        return StreamSupport.stream(Spliterators.spliteratorUnknownSize(generateLists(lists),
                Spliterator.NONNULL), false);
    }

    @Nonnull
    public static <K, V> Iterator<Map<K, V>> generateMaps(Map<K, List<V>> map) {
        List<K> keyList = map.keySet().stream().collect(Collectors.toList());
        List<List<V>> valuesList = keyList.stream().map(map::get).collect(Collectors.toList());
        return new MapIterator<>(new ListIterator<V>(valuesList), keyList);
    }
    @Nonnull
    public static <K, V> Stream<Map<K, V>> generateMapsStream(Map<K, List<V>> map) {
        return StreamSupport.stream(Spliterators.spliteratorUnknownSize(generateMaps(map),
                Spliterator.NONNULL), false);
    }

    private static class MapIterator<K, V> implements Iterator<Map<K, V>> {
        private final @Nonnull ListIterator<V> listIterator;
        private final @Nonnull List<K> keyList;

        private MapIterator(@Nonnull ListIterator<V> listIterator, @Nonnull List<K> keyList) {
            this.listIterator = listIterator;
            this.keyList = keyList;
        }

        @Override
        public boolean hasNext() {
            return listIterator.hasNext();
        }

        @Override
        public Map<K, V> next() {
            List<V> values = listIterator.next();
            assert values.size() == keyList.size();
            HashMap<K, V> map = new HashMap<>();
            for (int i = 0; i < values.size(); i++)
                map.put(keyList.get(i), values.get(i));
            return map;
        }
    }

    private static class ListIterator<T> implements Iterator<List<T>>  {
        private List<T> next = null;
        private final List<List<T>> data;
        private final Stack<State<T>> stack = new Stack<>();

        private ListIterator(List<List<T>> data) {
            Preconditions.checkArgument(data.stream().noneMatch(List::isEmpty),
                    "At least one of lists to combine is empty.");
            this.data = data;
            if (!data.isEmpty()) {
                for (int i = 0; i < data.get(0).size(); i++) stack.add(new State<>(i));
            } //else: start with empty stack implying hasNext() == false
        }

        @Override
        public boolean hasNext() {
            if (next != null) return true;
            while (!stack.isEmpty() && this.next == null) {
                State<T> state = stack.pop();
                this.next = state.tryGet(data);
                stack.addAll(state.nextStates(data));
            }
            return this.next != null;
        }

        @Override
        public List<T> next() {
            Preconditions.checkState(hasNext());
            List<T> result = this.next;
            this.next = null;
            return result;
        }

        private static final class State<T> {
            State<T> parent;
            int index;
            int column;

            public State(int index) {
                this.parent = null;
                this.index = index;
                this.column = 0;
            }

            public State(State<T> parent, int index, int column) {
                this.parent = parent;
                this.index = index;
                this.column = column;
            }

            @Nonnull
            List<State<T>> nextStates(List<List<T>> lists) {
                if (column == (lists.size()-1)) return Collections.emptyList();
                int nextColumn = column + 1;
                int size = lists.get(nextColumn).size();
                List<State<T>> list = new ArrayList<>(size);
                for (int i = 0; i < size; i++) {
                    list.add(new State<T>(this, i, nextColumn));
                }
                return list;
            }

            @Nullable
            List<T> tryGet(List<List<T>> lists) {
                if (column != (lists.size()-1)) return null;
                List<T> list = new ArrayList<>(column + 1);
                for (State<T> state = this; state != null; state = state.parent)
                    list.add(lists.get(state.column).get(state.index));
                Collections.reverse(list);
                return list;
            }
        }
    }
}

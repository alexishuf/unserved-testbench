package br.ufsc.lapesd.unserved.testbench.httpcomponents;

import br.ufsc.lapesd.unserved.testbench.byte_renderers.Render;
import br.ufsc.lapesd.unserved.testbench.byte_renderers.Renderer;
import br.ufsc.lapesd.unserved.testbench.byte_renderers.RendererException;
import br.ufsc.lapesd.unserved.testbench.byte_renderers.SimpleRender;
import br.ufsc.lapesd.unserved.testbench.iri.HTTP;
import br.ufsc.lapesd.unserved.testbench.iri.Testbench;
import br.ufsc.lapesd.unserved.testbench.iri.UnservedX;
import br.ufsc.lapesd.unserved.testbench.model.Message;
import br.ufsc.lapesd.unserved.testbench.model.Part;
import br.ufsc.lapesd.unserved.testbench.model.Variable;
import br.ufsc.lapesd.unserved.testbench.model.http.AsHttpProperty;
import br.ufsc.lapesd.unserved.testbench.model.http.HttpRequestNode;
import br.ufsc.lapesd.unserved.testbench.process.Context;
import br.ufsc.lapesd.unserved.testbench.process.actions.ActionExecutionException;
import br.ufsc.lapesd.unserved.testbench.process.actions.ActionRunner;
import br.ufsc.lapesd.unserved.testbench.process.model.Send;
import br.ufsc.lapesd.unserved.testbench.util.GeneralizedContentType;
import br.ufsc.lapesd.unserved.testbench.util.RDFListHelper;
import com.google.common.base.Preconditions;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.vocabulary.DCTerms;
import org.apache.jena.vocabulary.RDF;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;


public class HttpClientSendRunner implements ActionRunner {
    private static Logger logger = LoggerFactory.getLogger(HttpClientSendRunner.class);

    public static class Factory extends HttpClientActionRunnerFactory {
        public Factory() {
            super(HttpClientSendRunner.class, HttpClientSendRunner.Factory.class,
                    "httpcomponents/send-runner.ttl",
                    Testbench.PREFIX + "HttpClientSendRunner-Class");
        }
    }

    @Override
    public void run(Resource action, Context context) throws ActionExecutionException {
        Preconditions.checkArgument(action.canAs(Send.class));
        Send send = action.as(Send.class);
        Message msg = send.getMessage();
        Preconditions.checkArgument(msg.canAs(HttpRequestNode.class));
        HttpRequestNode req = msg.as(HttpRequestNode.class);

        BasicHttpContext httpContext = new BasicHttpContext();
        HttpUriRequest httpRequest = createHttpRequest(req, context);
        CloseableHttpClient client = HttpClients.createDefault();
        new HttpClientSendReceiveContext(context.runners(), send.getMessage(), client, httpRequest,
                httpContext, new DummyResponseHandler());
    }

    private HttpUriRequest createHttpRequest(HttpRequestNode req, Context context) throws ActionExecutionException {
        String methodString = getMethodName(req);
        if (methodString == null)
            throw new ActionExecutionException("HttpRequest must have an explicit methodName or mthd.");

        /* query parameters are specified trough ux-http:URITemplateAssignment ux-http:modifier
         * and processed internally by HttpMessageNodeImpl.getHttpProperty*(). */
        Literal literal = req.getHttpPropertyLiteral(HTTP.absoluteURI);
        String uri = literal == null ? null : literal.getLexicalForm();
        if (uri == null)
            throw new ActionExecutionException("HttpRequest must have an absoluteURI.");

        RequestBuilder builder = RequestBuilder.create(methodString).setUri(uri);
        HttpVersion httpVersion = getHttpVersion(req);
        if (httpVersion != null)
            builder.setVersion(httpVersion);
        getHeaders(req).forEach(builder::addHeader);
        builder.setEntity(getHttpEntity(req, context));

        return builder.build();
    }

    private HttpEntity getHttpEntity(HttpRequestNode req, Context context) throws ActionExecutionException {
        List<Part> bodyParts = req.getParts().stream()
                .filter(p -> p.getPartModifier().canAs(AsHttpProperty.class))
                .filter(p -> p.getPartModifier().as(AsHttpProperty.class)
                        .getHttpProperty().equals(HTTP.body)
                ).collect(Collectors.toList());
        if (bodyParts.size() > 1)
            throw new ActionExecutionException("Multiple http:body parts.");
        if (bodyParts.isEmpty()) return null;

        Part part = bodyParts.get(0);
        String mediaType = getMediaType(part.getVariable());
        if (mediaType == null)
            throw new ActionExecutionException("Non MediaType valueRepresentation");

        ContentType contentType = parseMediaType(mediaType);
        if (contentType == null)
            throw new ActionExecutionException("Malformed mediaType \"" + mediaType + "\"");
        Render render = getValueBytes(part.getVariable(), context);

        return new ByteArrayEntity(render.getBytes(), contentType);
    }

    private @Nonnull
    Render getValueBytes(Variable variable, Context context) throws ActionExecutionException {
        RDFNode value = variable.getValue();
        if (value == null) return SimpleRender.empty();
        Renderer renderer = context.componentRegistry().createRenderer(variable);
        if (renderer == null)
            throw new ActionExecutionException("No renderer for " + variable.toString());
        try {
            return renderer.format(variable);
        } catch (RendererException e) {
            throw new ActionExecutionException(e);
        }
    }

    private ContentType parseMediaType(String mediaType) {
        GeneralizedContentType contentType = GeneralizedContentType.parse(mediaType);
        if (contentType == null || !contentType.isConcrete()) return null;
        String name = contentType.getElements()[0].getName();
        NameValuePair[] params;
        params = (NameValuePair[]) Arrays.stream(contentType.getElements()[0].getParameters())
                .map(p -> new BasicNameValuePair(p.getName(), p.getValue()))
                .collect(Collectors.toList()).toArray();
        return ContentType.create(name, params);
    }

    private String getMediaType(Variable variable) {
        Resource representation = variable.getValueRepresentation();
        if (representation == null) representation = variable.getRepresentation();

        if (!representation.hasProperty(RDF.type, UnservedX.MediaType.MediaType))
            return null;
        Statement statement = representation.getProperty(UnservedX.MediaType.mediaTypeValue);
        if (statement == null) return null;
        return statement.getLiteral().getLexicalForm();
    }

    private String getMethodName(HttpRequestNode req) {
        Resource resource = req.getHttpPropertyResource(HTTP.mthd);
        String name = null;
        if (resource != null && HTTP.Methods.getInstance().getModel().containsResource(resource)) {
            resource = HTTP.Methods.getInstance().getModel().createResource(resource.getURI());
            Statement statement = resource.getProperty(DCTerms.title);
            if (statement != null && statement.getObject().isLiteral())
                name = statement.getLiteral().getString();
        } else {
            Literal literal = req.getHttpPropertyLiteral(HTTP.methodName);
            if (literal != null)
                name = literal.getLexicalForm();
        }
        return name;
    }

    private HttpVersion getHttpVersion(HttpRequestNode req) {
        Literal literal = req.getHttpPropertyLiteral(HTTP.httpVersion);
        if (literal != null) {
            Matcher matcher = Pattern.compile("[0-9]+\\.[0-9]+").matcher(literal.getLexicalForm());
            if (matcher.matches()) {
                int major = Integer.parseInt(matcher.group(1));
                int minor = Integer.parseInt(matcher.group(2));
                return new HttpVersion(major, minor);
            }
        }
        return null;
    }

    private List<Header> getHeaders(HttpRequestNode req) {
        List<Header> list = new ArrayList<>();
        List<Resource> headers = req.getHttpPropertyList(HTTP.headers);
        for (Resource header : headers) {
            String headerName = getHeaderName(req, header);
            if (headerName == null) {
                logger.error("Could not get header name for {}. Ignoring", header);
                continue;
            }
            String headerValue = getHeaderValue(req, header);
            if (headerValue == null) {
                logger.error("Could not get header value for {}. Ignoring", header);
                continue;
            }
            list.add(new BasicHeader(headerName, headerValue));
        }
        return list;
    }

    private String getHeaderName(HttpRequestNode req, Resource header) {
        String name = req.getHttpPropertyLiteral(header, HTTP.fieldName).getLexicalForm();
        if (name == null) {
            Resource resource = req.getHttpPropertyResource(header, HTTP.hdrName);
            if (resource == null) return null;
            if (!HTTP.Headers.sharedModel().containsResource(resource)) return null;
            name = HTTP.Headers.sharedModel().createResource(resource.getURI())
                    .getProperty(DCTerms.title).getLiteral().getLexicalForm();
        }
        return name;
    }

    private String getHeaderValue(HttpRequestNode req, Resource header) {
        String fieldValue = req.getHttpPropertyLiteral(header, HTTP.fieldValue).getLexicalForm();
        if (fieldValue != null)
            return fieldValue;
        List<Resource> elements;

        elements = RDFListHelper.collect(req.getHttpPropertyResource(header, HTTP.headerElements));
        if (elements == null) return null;

        List<String> elementStrings = new ArrayList<>();
        for (Resource element : elements) {
            Literal literal = req.getHttpPropertyLiteral(element, HTTP.elementValue);
            if (literal != null) {
                elementStrings.add(literal.getLexicalForm());
                continue;
            }
            /* no elementValue, get elementName + params */
            literal = req.getHttpPropertyLiteral(element, HTTP.elementName);
            if (literal == null) return null;
            String elementValue = literal.getLexicalForm();

            List<Resource> params;
            params = RDFListHelper.collect(req.getHttpPropertyResource(element, HTTP.params));
            params = params == null ? Collections.emptyList() : params;
            for (Resource param : params) {
                Literal pName = req.getHttpPropertyLiteral(param, HTTP.paramName);
                Literal pValue = req.getHttpPropertyLiteral(param, HTTP.paramValue);
                if (pName == null || pValue == null) continue;
                elementValue += "; " + pName.getLexicalForm() + "=" + pValue.getLexicalForm();
            }
            elementStrings.add(elementValue);
        }
        return elementStrings.stream().reduce((l, r) -> l + ", " + r).orElse(null);
    }
}

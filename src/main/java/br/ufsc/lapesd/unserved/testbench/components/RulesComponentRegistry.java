package br.ufsc.lapesd.unserved.testbench.components;

import br.ufsc.lapesd.unserved.testbench.byte_renderers.Renderer;
import br.ufsc.lapesd.unserved.testbench.byte_renderers.RendererFactory;
import br.ufsc.lapesd.unserved.testbench.components.expressions.ClassExpression;
import br.ufsc.lapesd.unserved.testbench.components.expressions.compiler.ClassExpressionParser;
import br.ufsc.lapesd.unserved.testbench.components.expressions.compiler.ClassExpressionParserException;
import br.ufsc.lapesd.unserved.testbench.components.expressions.evaluator.ClassExpressionEvaluator;
import br.ufsc.lapesd.unserved.testbench.components.expressions.evaluator.ClassExpressionEvaluator.Result;
import br.ufsc.lapesd.unserved.testbench.components.expressions.evaluator.ClassExpressionInstanceEvaluator;
import br.ufsc.lapesd.unserved.testbench.components.expressions.evaluator.ClassExpressionSubclassEvaluator;
import br.ufsc.lapesd.unserved.testbench.iri.UnservedC;
import br.ufsc.lapesd.unserved.testbench.model.Variable;
import br.ufsc.lapesd.unserved.testbench.process.actions.ActionRunner;
import br.ufsc.lapesd.unserved.testbench.process.actions.ActionRunnerFactory;
import br.ufsc.lapesd.unserved.testbench.util.BFSBGPIterator;
import br.ufsc.lapesd.unserved.testbench.util.ModelStreams;
import com.google.common.base.Preconditions;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.*;
import java.util.stream.Collectors;

/**
 * A {@link ComponentRegistry} implementation that extracts rules from Component class
 * expressions and tries to apply these rules to candidate objects breadth-first.
 */
public class RulesComponentRegistry implements ComponentRegistry {
    private static Logger logger = LoggerFactory.getLogger(RulesComponentRegistry.class);
    private HashMap<Resource, ComponentInfoList> infoLists;
    private static RulesComponentRegistry instance = null;

    public RulesComponentRegistry() {
        infoLists = new HashMap<>();
        infoLists.put(UnservedC.ActionRunner, new ComponentInfoList(
                Collections.singletonList(UnservedC.actionClass)));
        infoLists.put(UnservedC.Renderer, new ComponentInfoList(
                Arrays.asList(UnservedC.representationClass, UnservedC.valueClass)));
    }

    @Nonnull
    public static synchronized RulesComponentRegistry getInstance() {
        if (instance == null) {
            instance = new RulesComponentRegistry();
            DefaultComponentFactoryHelper.scan(ClassLoader.getSystemClassLoader(), instance);
        }
        return instance;
    }

    @Override
    public ActionRunner createActionRunner(Resource anAction) {
        Map<Property, EvaluatorFactory> map = new HashMap<>();
        map.put(UnservedC.actionClass, e -> new ClassExpressionInstanceEvaluator(e, anAction));
        ComponentInfo info = findComponentInfo(infoLists.get(UnservedC.ActionRunner), map);
        return createComponent(info, ActionRunner.class, ActionRunnerFactory.class);
    }

    @Override
    public Renderer createRenderer(Variable variable) {
        Map<Property, EvaluatorFactory> map = new HashMap<>();

        Resource rep_ = variable.getValueRepresentation();
        if (rep_ == null) rep_ = variable.getRepresentation();
        if (rep_ == null) {
            logger.error("Variable has no representation, can't get a Renderer");
            return null;
        }
        final Resource rep = rep_;

        boolean datatypeRep = BFSBGPIterator.from(rep, RDF.type).withInitial()
                .forward(RDFS.subClassOf).toStream().anyMatch(r -> r.equals(RDFS.Datatype));
        if (datatypeRep) {
            map.put(UnservedC.representationClass,
                    e -> new ClassExpressionSubclassEvaluator(e, rep));
        } else {
            map.put(UnservedC.representationClass,
                    e -> new ClassExpressionInstanceEvaluator(e, rep));
        }

        RDFNode aNode = variable.getValue();
        if (aNode == null) {
            logger.error("Variable {} has no value, can't get a Renderer", variable);
            return null;
        }
        map.put(UnservedC.valueClass, e -> new ClassExpressionInstanceEvaluator(e, aNode));

        ComponentInfo info = findComponentInfo(infoLists.get(UnservedC.Renderer), map);
        return createComponent(info, Renderer.class, RendererFactory.class);
    }

    @Override
    public void registerComponent(Class<? extends ComponentFactory> factoryClass) throws ComponentRegistrationException {
        String fName = factoryClass.getName();
        try {
            Constructor<? extends ComponentFactory> constructor;
            constructor = factoryClass.getConstructor();
            ComponentFactory f = constructor.newInstance();
            Resource component = f.getComponentResource();
            Resource componentClass = ModelStreams.objectsOfProperty(component, RDF.type,
                    Resource.class).filter(infoLists::containsKey).findAny().orElse(null);
            if (componentClass == null) {
                throw new ComponentRegistrationException(fName + " documents an unknown type " +
                        "of component");
            }
            ComponentInfoList list = infoLists.get(componentClass);
            list.add(new ComponentInfo(createExprMap(component, list.classProperties, fName), f));
        } catch (ReflectiveOperationException e) {
            throw new ComponentRegistrationException(e);
        }
    }

    @Override
    public boolean unregisterComponent(Class<? extends ComponentFactory> factory) {
        boolean changed = false;
        for (ComponentInfoList infoList : infoLists.values())
            changed |= infoList.infoList.removeIf(i -> i.factory.getClass().equals(factory));
        return changed;
    }

    @Override
    public void close() throws Exception {
        /* pass */
    }


    @SuppressWarnings({"unchecked", "ConstantConditions", "AssertWithSideEffects"})
    private <T, F> T createComponent(@Nullable ComponentInfo info, @Nonnull Class<T> aClass,
                                     @Nonnull Class<F> factoryClass) {
        if (info == null) return null;
        try {
            Method method = factoryClass.getMethod("newInstance");
            Object component = method.invoke(info.factory);
            return (T)component;
        } catch (ReflectiveOperationException e) {
            boolean shouldThrow = false;
            assert shouldThrow = true;
            if (shouldThrow) throw new RuntimeException(e);
            logger.error("Reflective exception on createComponent({}, {}, {})",
                    info.factory.getClass().getName(), aClass.getName(), factoryClass.getName(), e);
        }
        return null;
    }

    private ComponentInfo findComponentInfo(@Nonnull ComponentInfoList componentInfoList,
                                            @Nonnull Map<Property, EvaluatorFactory> factories) {
        Preconditions.checkArgument(factories.size() == componentInfoList.classProperties.size());

        LinkedList<ComponentEvaluator> list = componentInfoList.createEvaluators(factories);
        while (!list.isEmpty()) {
            for (Iterator<ComponentEvaluator> it = list.iterator(); it.hasNext(); ) {
                ComponentEvaluator e = it.next();
                Result result = e.evaluateStep();
                if (result == Result.TRUE)
                    return e.componentInfo;
                else if (result != Result.UNFINISHED)
                    it.remove();
            }
        }
        return null;
    }

    private Map<Property, ClassExpression>
    createExprMap(Resource component, Collection<Property> properties,
                  String facName) throws ComponentRegistrationException {
        Map<Property, ClassExpression> exprMap = new HashMap<>();
        for (Property property : properties) {
            List<Statement> list = component.listProperties(property).toList();
            if (list.isEmpty())
                throw new ComponentRegistrationException(facName + " has no " + property);
            ClassExpression expr = null;
            try {
                expr = new ClassExpressionParser().parse(list.get(0).getResource());
            } catch (ClassExpressionParserException e) {
                throw new ComponentRegistrationException(String.format(
                        "Failed to parse rule for %s of %s", property, facName), e);
            }
            exprMap.put(property, expr);
        }
        return exprMap;
    }

//    private static class InstanceOrClass {
//        enum Type {INSTANCE, CLASS}
//        final Type type;
//        final Resource resource;
//
//        InstanceOrClass(Type type, Resource resource) {
//            this.type = type;
//            this.resource = resource;
//        }
//
//        ClassExpressionEvaluator createEvaluator(ClassExpression classExpression) {
//            if (type == Type.INSTANCE)
//                return new ClassExpressionInstanceEvaluator(classExpression, resource);
//            else if (type == Type.CLASS)
//                return new ClassExpressionSubclassEvaluator(classExpression, resource);
//            throw new IllegalStateException("type is not a Type value");
//        }
//
//        static InstanceOrClass instance(Resource instance) {
//            return new InstanceOrClass(Type.INSTANCE, instance);
//        }
//        static InstanceOrClass aClass(Resource aClass) {
//            return new InstanceOrClass(Type.CLASS, aClass);
//        }
//    }

    private static class ComponentInfo {
        final Map<Property, ClassExpression> map;
        final ComponentFactory factory;

        private ComponentInfo(Map<Property, ClassExpression> classRuleMap, ComponentFactory factory) {
            this.map = classRuleMap;
            this.factory = factory;
        }
    }

    private static class ComponentInfoList {
        final List<Property> classProperties;
        final List<ComponentInfo> infoList = new ArrayList<>();

        ComponentInfoList(List<Property> classProperties) {
            this.classProperties = classProperties;
        }

        public void add(@Nonnull ComponentInfo info) {
            Preconditions.checkArgument(classProperties.stream()
                    .allMatch(info.map.keySet()::contains));
            infoList.add(info);
        }

        LinkedList<ComponentEvaluator>
        createEvaluators(Map<Property, EvaluatorFactory> factories) {
            return infoList.stream().map(i -> new ComponentEvaluator(i, factories))
                    .collect(Collectors.toCollection(LinkedList::new));
        }
    }

    @FunctionalInterface
    private interface EvaluatorFactory {
        ClassExpressionEvaluator apply(ClassExpression expression);
    }

    private static class ComponentEvaluator {
        private final List<ClassExpressionEvaluator> evaluators;
        private final Set<ClassExpressionEvaluator> trueSet = new HashSet<>();
        private final ComponentInfo componentInfo;
        private Result lastResult = Result.UNFINISHED;

        ComponentEvaluator(@Nonnull ComponentInfo componentInfo,
                           @Nonnull Map<Property, EvaluatorFactory> factories) {
            Preconditions.checkArgument(factories.keySet().equals(componentInfo.map.keySet()));
            this.componentInfo = componentInfo;
            evaluators = componentInfo.map.entrySet().stream()
                    .map(e -> factories.get(e.getKey()).apply(e.getValue()))
                    .collect(Collectors.toList());
        }

        Result evaluateStep() {
            if (lastResult == Result.UNFINISHED)
                lastResult = innerEvaluateStep();
            return lastResult;
        }

        private Result innerEvaluateStep() {
            for (ClassExpressionEvaluator e : evaluators) {
                if (e.getResult() == Result.TRUE) {
                    assert trueSet.contains(e);
                    continue;
                }

                Result result = e.evaluateStep();
                if (result == Result.TRUE) {
                    trueSet.add(e);
                    if (trueSet.size() == evaluators.size()) {
                        assert evaluators.stream().allMatch(trueSet::contains);
                        return Result.TRUE;
                    }
                } else if (result != Result.UNFINISHED) {
                    return result; //FALSE or INDETERMINATE
                }
            }
            return Result.UNFINISHED;
        }
    }
}

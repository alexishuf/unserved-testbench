package br.ufsc.lapesd.unserved.testbench.pragproof.context;

import br.ufsc.lapesd.unserved.testbench.input.RDFInput;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

/**
 * Creates a {@link PragProofRules} instance from a RDF model.
 */
public interface PragProofRulesFactory {
    default PragProofRules createRules(RDFInput input) throws IOException {
        return createRules(Collections.singletonList(input));
    }
    PragProofRules createRules(List<RDFInput> inputs) throws IOException;
}

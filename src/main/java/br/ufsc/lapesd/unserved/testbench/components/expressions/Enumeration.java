package br.ufsc.lapesd.unserved.testbench.components.expressions;

import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.jena.rdf.model.Resource;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class Enumeration extends AbstractClassExpression {
    @Nonnull private final Set<Resource> individuals;

    public Enumeration(@Nonnull Collection<Resource> individuals,
                       @Nonnull Set<Resource> names) {
        super(Type.Enumeration, names);
        this.individuals = new HashSet<>(individuals);
    }

    @Nonnull
    public Set<Resource> getIndividuals() {
        return individuals;
    }

    @Override
    public Set<ClassExpression> getChildren() {
        return Collections.emptySet();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(individuals).hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Enumeration)) return false;
        Enumeration rhs = (Enumeration) o;
        return getIndividuals().equals(rhs.getIndividuals());
    }
}

package br.ufsc.lapesd.unserved.testbench.pragproof.model;

import br.ufsc.lapesd.unserved.testbench.BackgroundProvider;
import br.ufsc.lapesd.unserved.testbench.input.RDFInput;
import br.ufsc.lapesd.unserved.testbench.process.model.ActionFactory;
import br.ufsc.lapesd.unserved.testbench.util.ResourcesBackground;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;

import java.util.Collections;

@BackgroundProvider(vocab = "https://alexishuf.bitbucket.io/2016/04/pragproof-ontology.ttl#")
public class PPO extends ResourcesBackground {
    public PPO() {
        super(Collections.singletonList(ImmutablePair.of("pragproof-ontology.ttl", RDFInput.N3)),
                true);
    }

    public static String IRI = "https://alexishuf.bitbucket.io/2016/04/pragproof-ontology.ttl";
    public static String PREFIX = IRI + "#";
    public static final String TAGS_PREFIX =
            "https://alexishuf.bitbucket.io/2016/04/pragproof-tags#";

    public static final Resource Grounded = ResourceFactory.createResource(PREFIX + "Grounded");
    public static final Resource Assignable = ResourceFactory.createResource(PREFIX + "Assignable");
    public static final Resource Tag = ResourceFactory.createResource(PREFIX + "Tag");

    public static final Property tag = ResourceFactory.createProperty(PREFIX + "tag");

    private static boolean initialized = false;
    public static void init() {
        if (!initialized) {
            initialized = true;
            ActionFactory.install();
        }
    }

    static {
        init();
    }
}

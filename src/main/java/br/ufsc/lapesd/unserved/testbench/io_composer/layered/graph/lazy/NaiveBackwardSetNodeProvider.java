package br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.lazy;


import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.atomic.VariableSpec;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.*;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.discovery.NodeOutput;
import br.ufsc.lapesd.unserved.testbench.io_composer.state.Copies;
import br.ufsc.lapesd.unserved.testbench.model.Variable;
import br.ufsc.lapesd.unserved.testbench.util.hipster.SetCoverIterator;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.SetMultimap;
import com.google.common.collect.Sets;

import javax.annotation.Nonnull;
import java.util.*;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.ActionsHelper.getMessagePairs;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.*;

public class NaiveBackwardSetNodeProvider implements BackwardSetNodeProvider {
    protected LayeredServiceGraph serviceGraph = null;
    protected SetNode start = null, end = null;
    private boolean useConditions = false;
    private boolean forceCombinedJumpNodes = false;

    public NaiveBackwardSetNodeProvider setForceCombinedJumpNodes(boolean forceCombinedJumpNodes) {
        this.forceCombinedJumpNodes = forceCombinedJumpNodes;
        return this;
    }

    @Override
    public void setup(@Nonnull LayeredServiceGraph serviceGraph, @Nonnull SetNode start,
                      @Nonnull SetNode end) {
        this.serviceGraph = serviceGraph;
        this.start = start;
        this.end = end;
    }

    public boolean getUseConditions() {
        return useConditions;
    }

    public @Nonnull NaiveBackwardSetNodeProvider setUseConditions(boolean useConditions) {
        this.useConditions = useConditions;
        return this;
    }

    @Nonnull
    @Override
    public Set<SetNodeEdge> getPredecessors(@Nonnull SetNode successor) {
        assert serviceGraph != null;
        return getProtoNodes(successor).map(pn -> pn.toEdge(successor)).collect(toSet());
    }


    protected Stream<ProtoNode> getProtoNodes(@Nonnull SetNode successor) {
        ProviderSet providerSet = getProviderSetWithJumpNodes(successor.getNodes());
        if (providerSet == null) return Stream.empty();

        if (useConditions && providerSet instanceof ConditionProviderHashSet)
            return getConditionProtoNodes(successor, providerSet, successor.getEquivalenceSets());

        SetMultimap<Node, Variable> targets = HashMultimap.create();
        providerSet.getAssignmentsStream().forEach(a -> targets.put(a.node, a.target));
        SetMultimap<Set<VariableSpec>, Node> setProviders = HashMultimap.create();
        for (Node node : targets.keySet()) {
            setProviders.put(targets.get(node).stream().map(VariableSpec::new)
                    .collect(toSet()), node);
        }
        SetCoverIterator<VariableSpec> setCoverIt = new SetCoverIterator<>(setProviders.keySet());
        Spliterator<Set<Set<VariableSpec>>> spliterator = Spliterators.spliteratorUnknownSize(
                setCoverIt, Spliterator.NONNULL | Spliterator.DISTINCT);
        return StreamSupport.stream(spliterator, false).map(targetSets ->
                Sets.cartesianProduct(targetSets.stream().map(setProviders::get)
                        .collect(toList())
                ).stream()
//                        .filter(providerSet::satisfyPreconditions)
                        .map(nodes -> {
                            Copies c = new Copies();
                            Set<Variable> assigned = new HashSet<>();
                            nodes.stream().filter(n -> !(n instanceof JumpNode))
                                    .forEach(n -> targets.get(n).stream()
                                            .filter(t -> !assigned.contains(t))
                                            .forEach(t -> {
                                                assigned.add(t);
                                                c.add(providerSet.getOutput(t, n), t);
                                            }));
                            List<IndexedNode> iNodes = new ArrayList<>(nodes.size());
                            for (Node n : nodes) iNodes.add(IndexedNode.wrap(n));
                            return new ProtoNode(iNodes, c);
                        })
        ).flatMap(identity()).distinct();
    }

    @Nonnull
    private Stream<ProtoNode>
    getConditionProtoNodes(@Nonnull SetNode successor,
                           @Nonnull ProviderSet providerSet,
                           @Nonnull EquivalenceSets successorEqSets) {
        return providerSet.getPreconditionMappings(successorEqSets).flatMap(precondMap -> {
            HashSet<Variable> missing = new HashSet<>(providerSet.getTargets());
            missing.removeAll(precondMap.inMap.keySet());
            if (missing.isEmpty()) //all vars already assigned,
                return Stream.of(precondMap);
            //TODO: condição pra duplicar nó: 2 preconditions diferentes mapeados pra mesma postcondition

            /* There are missing variables, not involved in precondition/postcondition matching.
             * Combine the current noMap with the possible providers set for these missing vars */
            SetMultimap<Node, Variable> targets = HashMultimap.create();
            providerSet.getAssignmentsStream().forEach(a -> targets.put(a.node, a.target));
            SetMultimap<Set<VariableSpec>, Node> setProviders = HashMultimap.create();
            for (Node node : targets.keySet()) {
                setProviders.put(targets.get(node).stream().map(VariableSpec::new)
                        .collect(toSet()), node);
            }
            SetCoverIterator<VariableSpec> setCoverIt = new SetCoverIterator<>(setProviders.keySet());
            Spliterator<Set<Set<VariableSpec>>> spliterator = Spliterators.spliteratorUnknownSize(
                    setCoverIt, Spliterator.NONNULL | Spliterator.DISTINCT);
            return StreamSupport.stream(spliterator, false).flatMap(targetSets ->
                    Sets.cartesianProduct(targetSets.stream().map(setProviders::get)
                            .collect(toList())
                    ).stream()
                            .map(providers -> {
                                ProviderSet.PrecondMap fullMap;
                                fullMap = new ProviderSet.PrecondMap(precondMap);
                                boolean nonJump = false;
                                for (Node provider : providers) {
                                    nonJump = !(provider instanceof JumpNode);
//                                    if (provider instanceof JumpNode) continue;
                                    for (Variable in : targets.get(provider)) {
                                        if (!fullMap.inMap.containsKey(in)) {
                                            Variable out = providerSet.getOutput(in, provider);
                                            fullMap.inMap.put(in, new NodeOutput(provider, out));
                                        }
                                    }
                                }
                                return nonJump ? fullMap : null;
                            }).filter(Objects::nonNull)
            );
        }).distinct().map(fullMap -> { /* Convert Map<Var, NodeOutput> to ProtoNode */
            EquivalenceSets equivalenceSets;
            equivalenceSets = providerSet.getEquivalenceSets(fullMap, successorEqSets);
            Set<IndexedNode> nodes = splitServices(fullMap.inMap, successorEqSets);
            providerSet.bindConstants(fullMap.inMap, successor, equivalenceSets, nodes);

            Copies copies = new Copies();
            for (Map.Entry<Variable, NodeOutput> e : fullMap.inMap.entrySet()) {
                if (!(e.getValue().node instanceof JumpNode))
                    copies.add(e.getValue().output, e.getKey());
            }
            return new ProtoNode(nodes, copies, equivalenceSets);
        });
    }

    private Set<IndexedNode> splitServices(@Nonnull Map<Variable, NodeOutput> fullMap,
                                           @Nonnull EquivalenceSets successorEqSets) {
        Set<IndexedNode> nodes = new HashSet<>();
        SetMultimap<NodeOutput, Variable> revFullMap = HashMultimap.create();
        for (Map.Entry<Variable, NodeOutput> e : fullMap.entrySet())
            revFullMap.put(e.getValue(), e.getKey());
        for (NodeOutput no : revFullMap.keySet()) {
            ArrayList<ArrayList<Variable>> lists = new ArrayList<>();
            ArrayList<Variable> inputs = new ArrayList<>(revFullMap.get(no));
            for (Variable input : inputs) {
                boolean assigned = false;
                for (ArrayList<Variable> list : lists) {
                    if (!successorEqSets.areDistinct(list.get(0), input)) {
                        list.add(input);
                        assigned = true;
                        break;
                    }
                }
                if (!assigned) {
                    ArrayList<Variable> l = new ArrayList<>();
                    l.add(input);
                    lists.add(l);
                }
            }
            for (int i = 0; i < lists.size(); i++) {
                nodes.add(IndexedNode.wrap(no.node, i));
                for (Variable in : lists.get(i)) fullMap.put(in, new NodeOutput(no, i));
            }
        }
        return nodes;
    }

    private ProviderSet getProviderSet(@Nonnull Node node) {
        return node instanceof JumpNode ? ((JumpNode)node).getProviderSet()
                : serviceGraph.getProviderSet(node);
    }

    protected ProviderSet getProviderSet(@Nonnull Collection<IndexedNode> nodes) {
        if (nodes.equals(Collections.singleton(IndexedNode.wrap(serviceGraph.getStartNode()))))
            return null;

        IOProviderHashSet result;
        result = useConditions ? new ConditionProviderHashSet() : new IOProviderHashSet();
        for (IndexedNode n : nodes) {
            ProviderSet ps = getProviderSet(n.node);
            if (!ps.isSatisfied())
                return null; //unsatisfiable
            result.addAll(ps);
        }

        return result;
    }

    protected ProviderSet getProviderSetWithJumpNodes(@Nonnull Collection<IndexedNode> nodes) {
        IndexedNode startIN = IndexedNode.wrap(serviceGraph.getStartNode());
        int startLayer = serviceGraph.getNodeLayer(serviceGraph.getStartNode());
        if (nodes.equals(Collections.singleton(startIN)))
            return null;
        int layer = getNodesLayer(nodes.stream().map(IndexedNode::getNode).collect(toList()));
        assert layer > 0;
        int previousLayer = layer - 1;

        IOProviderHashSet result;
        boolean hasConditions = useConditions && nodes.stream().anyMatch(n -> !n.getPreConditions().isEmpty());
        if (forceCombinedJumpNodes || hasConditions) {
            result = hasConditions ? new ConditionProviderHashSet() : new IOProviderHashSet();
            for (IndexedNode node : nodes) {
                //Note: forced bindings are handled on getConditionProtoNodes
                ProviderSet ps = getProviderSet(node.node);
                if (!ps.isSatisfied())
                    return null; //unsatisfiable
                result.addAll(ps);
            }
            Map<Node, CombinedJumpNode> map = result.stream()
                    .filter(n -> serviceGraph.getNodeLayer(n) < previousLayer)
                    .collect(toMap(identity(), n -> new CombinedJumpNode(n, result)));
            map.forEach(result::replace);
        } else {
            result = new IOProviderHashSet();
            Map<VariableSpec, JumpNode> jNodes = new HashMap<>();
            for (IndexedNode node : nodes) {
                for (Map.Entry<IndexedVariable, Variable> e : node.getForcedBindings().entrySet()) {
                    if (previousLayer == startLayer) {
                        result.add(e.getKey(), startIN, e.getValue());
                    } else {
                        SingleJumpNode jn = new SingleJumpNode(e.getKey(), startIN, e.getValue());
                        result.add(e.getKey(), jn, e.getKey());
                    }
                }
                ProviderSet ps = getProviderSet(node.node);
                if (!ps.isSatisfied())
                    return null; //unsatisfiable
                ps.getAssignmentsStream().forEach(a -> {
                    if (node.boundTo(a.target) != null) return; //skip forced bindings
                    if (serviceGraph.getNodeLayer(a.getNode()) < previousLayer) {
                        VariableSpec spec = a.target.asSpec();
                        JumpNode jn = jNodes.computeIfAbsent(spec, s -> new SingleJumpNode(a));
                        result.add(a.target, jn, a.target);
                    } else {
                        result.add(a.target, a.node, a.output);
                    }
                });
            }
        }

        return result;
    }

    private int getNodesLayer(@Nonnull Collection<Node> nodes) {
        assert nodes.stream().filter(serviceGraph::containsNode)
                .map(serviceGraph::getNodeLayer).collect(toSet()).size() == 1;
        for (Node n : nodes) {
            if (serviceGraph.containsNode(n)) return serviceGraph.getNodeLayer(n);
        }
        throw new IllegalArgumentException();

    }

    private interface Duplicator {
        LayeredServiceGraph getDuplicate();
    }

    /**
     * This is a helper to filter out duplicate states early, only states with distinct set of
     * {@link Node} instances will be created, other alternatives (like direct copies and edge
     * assignments should be explored with auto-fix).
     */
    protected static class ProtoNode {
        final @Nonnull Set<IndexedNode> nodes = new HashSet<>();
        final @Nonnull Copies copies;
        final @Nonnull EquivalenceSets equivalenceSets;

        ProtoNode(@Nonnull Collection<IndexedNode> nodes, @Nonnull Copies copies,
                  @Nonnull EquivalenceSets equivalenceSets) {
            this.nodes.addAll(nodes);
            this.copies = copies;
            this.equivalenceSets = equivalenceSets;
        }

        ProtoNode(@Nonnull Collection<IndexedNode> nodes, @Nonnull Copies copies) {
            this(nodes, copies, new EquivalenceSets());
        }


        @Nonnull
        SetNodeEdge toEdge(SetNode target) {
            return new SetNodeEdge(toSetNode(), target, copies);
        }

        @Nonnull
        SetNode toSetNode() {
            return new FullSetNode(nodes, getMessagePairs(nodes).values(), equivalenceSets);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            ProtoNode protoNode = (ProtoNode) o;
            return nodes.equals(protoNode.nodes);
        }

        @Override
        public int hashCode() {
            return nodes.hashCode();
        }
    }
}

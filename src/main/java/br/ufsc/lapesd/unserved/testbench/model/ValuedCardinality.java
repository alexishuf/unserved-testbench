package br.ufsc.lapesd.unserved.testbench.model;

/**
 * Enhanced node for unserved:ValuedCardinality
 */
public interface ValuedCardinality extends Cardinality {
    long getCardinalityValue();
}

package br.ufsc.lapesd.unserved.testbench.iri;

import br.ufsc.lapesd.unserved.testbench.process.model.ActionFactory;
import br.ufsc.lapesd.unserved.testbench.util.ResourcesBackground;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.riot.RDFFormat;

import java.util.Collections;

public class UnservedP  extends ResourcesBackground {
    public static final String IRI = "https://alexishuf.bitbucket.io/2016/04/unserved/unserved-p.ttl";
    public static final String PREFIX = IRI + "#";
    public static final String PREFIX_SHORT = "unserved-p";

    public UnservedP() {
        super(Collections.singletonList(ImmutablePair.of("unserved/unserved-p.ttl",
                RDFFormat.TURTLE)), true);
    }

    private static UnservedP instance = new UnservedP();
    public static UnservedP getInstance() {
        return instance;
    }

    public static final Resource Action = getInstance().getModel().createResource(PREFIX + "Action");
    public static final Resource Copy = getInstance().getModel().createResource(PREFIX + "Copy");
    public static final Resource Assign = getInstance().getModel().createResource(PREFIX + "Assign");
    public static final Resource Receive = getInstance().getModel().createResource(PREFIX + "Receive");
    public static final Resource Send = getInstance().getModel().createResource(PREFIX + "Send");

    public static final Resource Sequence = getInstance().getModel().createResource(PREFIX + "Sequence");
    public static final Resource Repeat = getInstance().getModel().createResource(PREFIX + "Repeat");

    public static final Property copyFrom = getInstance().getModel().createProperty(PREFIX + "copyFrom");
    public static final Property copyTo = getInstance().getModel().createProperty(PREFIX + "copyTo");
    public static final Property copyFromIndex= getInstance().getModel().createProperty(PREFIX + "copyFromIndex");
    public static final Property copyToIndex= getInstance().getModel().createProperty(PREFIX + "copyToIndex");
    public static final Property members = getInstance().getModel().createProperty(PREFIX + "members");
    public static final Property assignVariable = getInstance().getModel().createProperty(PREFIX + "assignVariable");
    public static final Property assignWithResource = getInstance().getModel().createProperty(PREFIX + "assignWithResource");
    public static final Property assignWithLiteral = getInstance().getModel().createProperty(PREFIX + "assignWithLiteral");

    public static final Property message = getInstance().getModel().createProperty(PREFIX + "message");
    public static final Property invocationIndex = getInstance().getModel().createProperty(PREFIX + "invocationIndex");

    static {
        init();
    }

    public static void init() {
        ActionFactory.install();
    }
}

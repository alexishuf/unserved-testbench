package br.ufsc.lapesd.unserved.testbench.process;

import br.ufsc.lapesd.unserved.testbench.components.ComponentRegistry;
import br.ufsc.lapesd.unserved.testbench.process.data.DataContext;

import java.util.Map;

public class OverriddenContext implements Context {
    private final Context delegate;
    private DataContext dataContext = null;
    private ActionRunnersContext actionRunnersContext = null;
    private ComponentRegistry componentRegistry = null;
    private Interpreter interpreter = null;
    private Map<String, Object> globalContext = null;
    private final boolean closeDelegate;

    public OverriddenContext(Context delegate) {
        this(delegate, true);
    }
    public OverriddenContext(Context delegate, boolean closeDelegate) {
        this.delegate = delegate;
        this.closeDelegate = closeDelegate;
    }

    public OverriddenContext setDataContext(DataContext dataContext) {
        this.dataContext = dataContext;
        return this;
    }

    public OverriddenContext setActionRunnersContext(ActionRunnersContext actionRunnersContext) {
        this.actionRunnersContext = actionRunnersContext;
        return this;
    }

    public OverriddenContext setComponentRegistry(ComponentRegistry componentRegistry) {
        this.componentRegistry = componentRegistry;
        return this;
    }

    public OverriddenContext setInterpreter(Interpreter interpreter) {
        this.interpreter = interpreter;
        return this;
    }

    public OverriddenContext setGlobalContext(Map<String, Object> globalContext) {
        this.globalContext = globalContext;
        return this;
    }

    @Override
    public boolean isCancelled() {
        return delegate.isCancelled();
    }

    @Override
    public boolean isAborted() {
        return delegate.isAborted();
    }

    @Override
    public void cancel() {
        delegate.cancel();
    }

    @Override
    public void abort() {
        delegate.abort();
    }

    @Override
    public DataContext data() {
        return dataContext != null ? dataContext : delegate.data();
    }

    @Override
    public ActionRunnersContext runners() {
        return actionRunnersContext != null ?  actionRunnersContext : delegate.runners();
    }

    @Override
    public ComponentRegistry componentRegistry() {
        return componentRegistry != null ? componentRegistry : delegate.componentRegistry();
    }

    @Override
    public Interpreter interpreter() {
        return interpreter != null ? interpreter : delegate.interpreter();
    }

    @Override
    public Map<String, Object> globalContext() {
        return globalContext != null ? globalContext : delegate.globalContext();
    }

    @Override
    public void close() {
        if (closeDelegate)
            delegate.close();
    }
}

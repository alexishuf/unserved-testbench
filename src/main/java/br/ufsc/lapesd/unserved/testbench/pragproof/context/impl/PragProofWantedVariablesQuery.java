package br.ufsc.lapesd.unserved.testbench.pragproof.context.impl;

import br.ufsc.lapesd.unserved.testbench.input.RDFInput;
import br.ufsc.lapesd.unserved.testbench.input.RDFInputFile;
import br.ufsc.lapesd.unserved.testbench.pragproof.context.PragProofQuery;
import br.ufsc.lapesd.unserved.testbench.pragproof.model.PPO;
import com.google.common.base.Preconditions;
import org.apache.jena.rdf.model.Resource;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.util.List;

public class PragProofWantedVariablesQuery {
    public static PragProofQuery createQuery(@Nonnull List<Resource> wantedVariables)
            throws IOException {
        Preconditions.checkNotNull(wantedVariables);
        Preconditions.checkArgument(wantedVariables.stream().noneMatch(Resource::isAnon),
                "Blank nodes are not supported, skolemize first");

        File file = Files.createTempFile("unserved-testbench", ".n3").toFile();
        if (!wantedVariables.isEmpty()) {
            try (FileOutputStream stream = new FileOutputStream(file);
                 PrintStream printer = new PrintStream(stream)) {

                printer.printf("@prefix ppo: <%1$s> .\n" +
                                "@prefix log: <http://www.w3.org/2000/10/swap/log#> .\n" +
                                "@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .\n\n",
                        PPO.PREFIX);

                printer.printf("{\n");
                wantedVariables.forEach(
                        v -> printer.printf("  <%1$s> a ppo:Grounded .\n", v));
                printer.printf("} => {\n");
                wantedVariables.forEach(
                        v -> printer.printf("  <%1$s> a ppo:Grounded .\n", v));
                printer.printf("} .\n");
            }
        }
        return new DefaultPragProofQuery(RDFInputFile.createOwningFile(file, RDFInput.N3));
    }
}

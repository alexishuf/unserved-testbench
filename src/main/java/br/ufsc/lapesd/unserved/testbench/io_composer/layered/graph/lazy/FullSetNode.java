package br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.lazy;

import br.ufsc.lapesd.unserved.testbench.benchmarks.wsc08.Wscc;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.*;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.cost.CostInfo;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.cost.ServiceCount;
import br.ufsc.lapesd.unserved.testbench.io_composer.state.MessagePairAction;
import br.ufsc.lapesd.unserved.testbench.model.Variable;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FullSetNode implements SetNode {
    private final @Nonnull Set<IndexedNode> nodes;
    private Set<Variable> inputs = null;
    private Set<Variable> outputs = null;
    private List<MessagePairAction> messagePairs;
    private List<SetNode> alternatives = null;
    private CostInfo costInfo = null;
    private EquivalenceSets equivalenceSets = new EquivalenceSets();

    public FullSetNode(@Nonnull Set<IndexedNode> nodes, @Nonnull Collection<MessagePairAction> messagePairs) {
        this.nodes = nodes;
        this.messagePairs = new ArrayList<>(messagePairs);
    }
    public FullSetNode(@Nonnull Set<IndexedNode> nodes,
                       @Nonnull Collection<MessagePairAction> messagePairs,
                       @Nonnull EquivalenceSets equivalenceSets) {
        this(nodes, messagePairs);
        this.equivalenceSets = equivalenceSets;
    }

    @Nonnull
    @Override
    public EquivalenceSets getEquivalenceSets() {
        return equivalenceSets;
    }

    @Override
    @Nonnull
    public Set<IndexedNode> getNodes() {
        return nodes;
    }

    @Nonnull
    @Override
    public Stream<Node> streamPlainNodes() {
        return nodes.stream().map(IndexedNode::getNode);
    }

    /**
     * Get a read-only list of the message pairs of this node. DO NOT modify the
     * {@link MessagePairAction} instances on the list.
     */
    @Override
    @Nonnull
    public List<MessagePairAction> getMessagePairs() {
        return messagePairs;
    }

    @Nonnull
    @Override
    public Set<Variable> getInputs() {
        if (inputs == null) {
            inputs = nodes.stream().map(Node::getInputs).flatMap(Set::stream)
                    .collect(Collectors.toSet());
        }
        return inputs;
    }

    @Nonnull
    @Override
    public Set<Variable> getOutputs() {
        if (outputs == null) {
            outputs = nodes.stream().map(Node::getOutputs).flatMap(Set::stream)
                    .collect(Collectors.toSet());
        }
        return outputs;
    }

    @Nonnull
    @Override
    public CostInfo getCostInfo() {
        if (costInfo == null) {
            costInfo = nodes.stream().map(Node::getCostInfo)
                    .reduce(CostInfo::aggregateParallel).orElse(CostInfo.EMPTY)
                    .with(new ServiceCount(messagePairs.size()));
        }
        return costInfo;
    }

    @Nonnull
    @Override
    public List<SetNode> getAlternatives() {
        return alternatives == null ? (alternatives = new ArrayList<>()) : alternatives;
    }

    public void setAlternatives(List<SetNode> alternatives) {
        this.alternatives = alternatives;
    }

    @Override
    public String toString() {
        boolean isWsc = getNodes().stream().filter(n -> n.node instanceof ServiceNode).allMatch(n ->
                ((ServiceNode) n.node).getAntecedent().getProperty(Wscc.wscService) != null);
        if (isWsc) {
            return String.format("[%s]", getNodes().stream().map(n -> {
                if (n.node instanceof StartNode) {
                    return "StartNode";
                } else if (n.node instanceof EndNode) {
                    return "EndNode";
                } else if (n.node instanceof ServiceNode) {
                    return ((ServiceNode) n.node).getAntecedent().getProperty(Wscc.wscService)
                            .getLiteral().getLexicalForm().split("#")[1];
                } else {
                    return n.toString();
                }
            }).reduce((l, r) -> l + ", " + r).orElse(""));
        } else {
            return String.format("SetNode(%s)", getNodes().stream().map(Object::toString)
                    .reduce((l, r) -> l + ", " + r).orElse(""));
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FullSetNode setNode = (FullSetNode) o;

        return nodes.equals(setNode.nodes) && messagePairs.equals(setNode.messagePairs);
    }

    @Override
    public int hashCode() {
        int result = nodes.hashCode();
        result = 31 * result + messagePairs.hashCode();
        return result;
    }
}

package br.ufsc.lapesd.unserved.testbench.replication.action;


import br.ufsc.lapesd.unserved.testbench.process.actions.ActionExecutionException;

public class ReplicationActionRunnerResumeException extends ActionExecutionException {
    public ReplicationActionRunnerResumeException(String s) {
        super(s);
    }
    public ReplicationActionRunnerResumeException(Throwable e) {
        super(e);
    }
}

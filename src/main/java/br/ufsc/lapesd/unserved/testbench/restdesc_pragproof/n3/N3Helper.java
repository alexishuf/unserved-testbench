package br.ufsc.lapesd.unserved.testbench.restdesc_pragproof.n3;

import br.ufsc.lapesd.unserved.testbench.input.RDFInput;
import br.ufsc.lapesd.unserved.testbench.input.RDFInputString;
import ch.ethz.inf.vs.semantics.parser.N3TransformationVisitor;
import ch.ethz.inf.vs.semantics.parser.elements.*;
import ch.ethz.inf.vs.semantics.parser.notation3.N3Lexer;
import ch.ethz.inf.vs.semantics.parser.notation3.N3Parser;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.apache.commons.io.IOUtils;
import org.apache.jena.vocabulary.OWL;
import org.apache.jena.vocabulary.RDF;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.IOException;
import java.io.InputStream;
import java.util.function.Predicate;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class N3Helper {
    public static RDFInput workaroundBadParser(InputStream in) throws IOException {
        String contents = IOUtils.toString(in);
        String old = null;
        while (old == null || !contents.equals(old)) {
            old = contents;
            contents = contents.replaceAll("(\\?|_:)([^_\\s]*)_(\\S*)", "$1$2z$3");
        }

        return new RDFInputString(contents, RDFInput.N3);
    }

    public static N3Document parseN3(RDFInput proof) throws IOException {
        N3Lexer nl = new N3Lexer(new ANTLRInputStream(proof.createInputStream()));
        CommonTokenStream ts = new CommonTokenStream(nl);
        N3Parser np = new N3Parser(ts);
        N3Parser.N3DocContext doc = np.n3Doc();
        if (np.getNumberOfSyntaxErrors() > 0) return null;
        N3TransformationVisitor tv = new N3TransformationVisitor();
        return tv.visitN3Doc(doc);
    }

    public static String expandIRI(Iri iri) {
        Prefix p = iri.getPrefix();
        return (p == null ? "" : p.getUri()) + iri.text;
    }

    public static String verbIRI(N3Element.Verb verb) {
        if (verb instanceof Verb) {
            //buggy Verb.getGlobalName()
            switch (verb.toString()) {
                case "a": return RDF.type.getURI();
                case "=>":
                case "<=": return "http://www.w3.org/2000/10/swap/log#implies";
                case "=": return OWL.sameAs.getURI();
                case "!=": return OWL.differentFrom.getURI();
            }
            throw new IllegalArgumentException("Unknnown keyword " + verb.toString());
        } else if (verb instanceof Iri) {
            return expandIRI((Iri)verb);
        }
        throw new IllegalArgumentException("Unknown N3Element.Verb class");
    }

    public static boolean matchVerb(N3Element.Verb verb, String uri) {
        return verbIRI(verb).equals(uri);
    }

    public static boolean matchObject(@Nonnull N3Element.Object object,
                                      @Nonnull Predicate<N3Element.Object> predicate) {
        if (object instanceof ObjectList) {
            for (N3Element.Object o : ((ObjectList) object)) {
                if (matchObject(o, predicate)) return true;
            }
        } else {
            return predicate.test(object);
        }
        return false;
    }

    public static boolean matchObjectURI(@Nonnull N3Element.Object object, @Nonnull String uri) {
        return matchObject(object, o -> o instanceof Iri && expandIRI((Iri)o).equals(uri));
    }

    public static Stream<RDFResource> topLevelRDFResources(@Nonnull N3Document document) {
        return StreamSupport.stream(document.getChildern().spliterator(), false)
                .filter(e -> e instanceof Statements)
                .flatMap(e -> StreamSupport.stream(e.getChildern().spliterator(), false)
                        .filter(e2 -> e2 instanceof RDFResource))
                .map(e -> (RDFResource)e);
    }

    public static Stream<N3Element.Object> toStream(N3Element.Object object) {
        return (object instanceof ObjectList) ? ((ObjectList) object).stream() : Stream.of(object);
    }

    @Nullable
    public static Stream<VerbObject> getVerbObjects(N3Document doc, N3Element.Object o) {
        return null;
    }
}

package br.ufsc.lapesd.unserved.testbench.io_composer.state;

import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.IndexedVariable;
import br.ufsc.lapesd.unserved.testbench.model.Variable;
import com.google.common.base.Preconditions;

import javax.annotation.Nonnull;
import java.util.Objects;

public final class Copy implements Action {
    public final @Nonnull IndexedVariable from;
    public final @Nonnull IndexedVariable to;

    public Copy(@Nonnull Variable from, @Nonnull Variable to) {
        Preconditions.checkArgument(!from.equals(to));
        this.from = from instanceof IndexedVariable ? (IndexedVariable)from
                                                    : new IndexedVariable(from);
        this.to = to instanceof IndexedVariable ? (IndexedVariable)to
                : new IndexedVariable(to);
    }

    @Nonnull
    public Variable getFrom() {
        return from;
    }

    @Nonnull
    public Variable getTo() {
        return to;
    }

    @Override
    public String toString() {
        return String.format("%s => %s", from, to);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Copy copy = (Copy) o;
        return Objects.equals(from, copy.from) &&
                Objects.equals(to, copy.to);
    }

    @Override
    public int hashCode() {
        return Objects.hash(from, to);
    }
}

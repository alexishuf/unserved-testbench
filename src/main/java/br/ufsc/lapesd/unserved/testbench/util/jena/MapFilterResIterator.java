package br.ufsc.lapesd.unserved.testbench.util.jena;

import org.apache.jena.rdf.model.ResIterator;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.util.iterator.ExtendedIterator;
import org.apache.jena.util.iterator.MapFilter;
import org.apache.jena.util.iterator.MapFilterIterator;

public class MapFilterResIterator<T> extends MapFilterIterator<T, Resource> implements ResIterator {
    /**
     * Creates a sub-Iterator.
     *
     * @param fl An object is included if it is accepted by this Filter.
     * @param e  The parent Iterator.
     */
    public MapFilterResIterator(MapFilter<T, Resource> fl, ExtendedIterator<T> e) {
        super(fl, e);
    }

    @Override
    public Resource nextResource() {
        return next();
    }
}

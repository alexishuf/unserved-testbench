package br.ufsc.lapesd.unserved.testbench.components.expressions.evaluator;

import br.ufsc.lapesd.unserved.testbench.components.expressions.ClassExpression;
import com.google.common.base.Preconditions;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.Stack;

public abstract class AbstractClassExpressionEvaluator<T> implements ClassExpressionEvaluator {
    @Nonnull private ClassExpression classExpression;
    @Nonnull private Result lastResult = Result.UNFINISHED;
    private Iterator<Step<T>> stepIterator;
    @Nonnull private final Step<T> rootStep;
    @Nonnull private final T argument;


    protected AbstractClassExpressionEvaluator(@Nonnull ClassExpression classExpression,
                                               @Nonnull Step<T> rootStep, @Nonnull T argument) {
        this.classExpression = classExpression;
        this.rootStep = rootStep;
        this.argument = argument;

        Stack<Step<T>> stack = new Stack<>();
        ArrayList<Step<T>> steps = new ArrayList<>();
        stack.push(rootStep);
        while (!stack.isEmpty()) {
            Step<T> step = stack.pop();
            steps.add(step);
            step.getChildren().forEach(stack::push);
        }
        Collections.reverse(steps);
        stepIterator = steps.iterator();
    }

    @Nonnull
    public ClassExpression getClassExpression() {
        return classExpression;
    }

    @Nonnull
    @Override
    public Result getResult() {
        return lastResult;
    }

    @Nonnull
    @Override
    public Result evaluateStep() {
        if (lastResult != Result.UNFINISHED)
            return lastResult;
        Preconditions.checkState(stepIterator.hasNext());
        if (stepIterator.hasNext()) stepIterator.next().evaluate(argument);
        Result rootResult = rootStep.getEvaluatedResult(argument);
        assert rootResult != null;
        return lastResult = rootResult;
    }
}

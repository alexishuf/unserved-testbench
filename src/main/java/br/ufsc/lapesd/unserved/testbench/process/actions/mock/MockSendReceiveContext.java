package br.ufsc.lapesd.unserved.testbench.process.actions.mock;

import br.ufsc.lapesd.unserved.testbench.model.Message;
import br.ufsc.lapesd.unserved.testbench.process.AbstractActionContext;
import br.ufsc.lapesd.unserved.testbench.process.ActionRunnersContext;

public class MockSendReceiveContext extends AbstractActionContext {
    public MockSendReceiveContext(ActionRunnersContext runnersContext, Message message) {
        super(runnersContext, message);
    }

    public void close() {
        removeFromRunnersContext();
    }
}

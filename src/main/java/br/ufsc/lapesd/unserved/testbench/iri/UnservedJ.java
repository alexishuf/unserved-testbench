package br.ufsc.lapesd.unserved.testbench.iri;

import br.ufsc.lapesd.unserved.testbench.util.ResourcesBackground;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.riot.RDFFormat;

import java.util.Collections;

public class UnservedJ extends ResourcesBackground {
    public static final String IRI = "https://alexishuf.bitbucket.io/2016/04/unserved/unserved-j.ttl";
    public static final String PREFIX = IRI + "#";
    public static final String PREFIX_SHORT = "unserved-j";

    public UnservedJ() {
        super(Collections.singletonList(ImmutablePair.of("unserved/unserved-j.ttl",
                RDFFormat.TURTLE)), true);
    }
    static UnservedJ instance = new UnservedJ();
    public static UnservedJ getInstance() {
        return instance;
    }

    public static final Property factoryClassName = getInstance().getModel().createProperty(PREFIX + "factoryClassName");
}

package br.ufsc.lapesd.unserved.testbench.httpcomponents.extractors;

import br.ufsc.lapesd.unserved.testbench.iri.Testbench;
import br.ufsc.lapesd.unserved.testbench.model.Part;

import javax.annotation.Nonnull;

public interface HttpResponseExtractorRepository {
    String CONFIG_ID = Testbench.PREFIX + "HttpResponseExtractorRepository";

    @Nonnull HttpResponseExtractor getExtractor(Part part)
            throws IncompletePartException, NoExtractorException;
}

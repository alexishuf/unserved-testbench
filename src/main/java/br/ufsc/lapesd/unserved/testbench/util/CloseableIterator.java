package br.ufsc.lapesd.unserved.testbench.util;

import java.util.Iterator;

/**
 * An {@link Iterator} and a {@link AutoCloseable}.
 */
public interface CloseableIterator<E> extends Iterator<E>, AutoCloseable {
}

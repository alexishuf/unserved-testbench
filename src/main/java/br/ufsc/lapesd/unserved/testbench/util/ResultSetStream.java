package br.ufsc.lapesd.unserved.testbench.util;

import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.RDFNode;

import javax.annotation.Nonnull;
import java.util.Iterator;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * Helper to get a Stream from a SPARQL ResultSet.
 */
public class ResultSetStream {
    public static <T extends RDFNode> Stream<T> toStream(@Nonnull QueryExecution execution,
                                         @Nonnull ResultSet resultSet, @Nonnull String varName,
                                         @Nonnull Class<T> nodeClass,
                                         int additionalCharacteristics) {
        return StreamSupport.stream(Spliterators.spliteratorUnknownSize(new Iterator<T>() {
            T current = advance();
            boolean closed = false;

            private T advance() {
                while (resultSet.hasNext()) {
                    RDFNode node = resultSet.next().get(varName);
                    if (!node.canAs(nodeClass))
                        continue;
                    return node.as(nodeClass);
                }
                if (!closed) {
                    closed  = true;
                    execution.close();
                }
                return null;
            }

            @Override
            public boolean hasNext() {
                return current != null;
            }

            @Override
            public T next() {
                T old = current;
                current = advance();
                return old;
            }
        }, Spliterator.NONNULL|additionalCharacteristics), false);
    }

    public static <T extends RDFNode> Stream<T> toStream(@Nonnull QueryExecution execution,
                                                         @Nonnull ResultSet resultSet, @Nonnull String varName,
                                                         @Nonnull Class<T> nodeClass) {
        return toStream(execution, resultSet, varName, nodeClass, 0);
    }
}

package br.ufsc.lapesd.unserved.testbench.io_composer.conditions;

import br.ufsc.lapesd.unserved.testbench.model.Variable;

import javax.annotation.Nonnull;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public interface Condition {
    default @Nonnull List<Variable> getVariables() {return Collections.emptyList();}
    @Nonnull Condition replacing(@Nonnull Map<Variable, Variable> current2replacement);
    default @Nonnull Map<Variable, Variable> bindFrom(@Nonnull Condition other) {
        return bindFrom(other, true);
    }
    default @Nonnull Map<Variable, Variable> bindFrom(@Nonnull Condition other, boolean ignoreBound) {
        return bindFrom(other, ignoreBound, false);
    }
    default @Nonnull Map<Variable, Variable> bindFrom(@Nonnull Condition other, boolean ignoreBound, boolean ignoreUnbound) {return Collections.emptyMap();}
}

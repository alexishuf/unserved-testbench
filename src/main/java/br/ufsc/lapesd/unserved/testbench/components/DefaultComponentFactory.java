package br.ufsc.lapesd.unserved.testbench.components;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Marks a {@link ComponentFactory} for auto registration by {@link DefaultComponentRegistry}
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface DefaultComponentFactory {
}

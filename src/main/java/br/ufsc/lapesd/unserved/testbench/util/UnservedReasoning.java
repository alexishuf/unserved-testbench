package br.ufsc.lapesd.unserved.testbench.util;

import br.ufsc.lapesd.unserved.testbench.input.AbstractRDFInput;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.reasoner.rulesys.GenericRuleReasoner;
import org.apache.jena.reasoner.rulesys.Rule;

import javax.annotation.Nonnull;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

public class UnservedReasoning {
    private static final @Nonnull List<Rule> defaultRules;

    public static class RDFInput extends AbstractRDFInput {
        private final @Nonnull br.ufsc.lapesd.unserved.testbench.input.RDFInput source;

        public RDFInput(@Nonnull br.ufsc.lapesd.unserved.testbench.input.RDFInput source) {
            super(source.getFormat());
            this.source = source;
        }

        @Override
        protected Model loadModel() throws IOException {
            return wrap(source.takeModel());
        }
    }

    public static RDFInput wrap(br.ufsc.lapesd.unserved.testbench.input.RDFInput input) {
        return new RDFInput(input);
    }

    public static Model wrap(Model model) {
        GenericRuleReasoner reasoner = new GenericRuleReasoner(defaultRules);
        return ModelFactory.createModelForGraph(reasoner.bind(model.getGraph()));
    }

    static {
        ClassLoader loader = UnservedReasoning.class.getClassLoader();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(loader
                .getResourceAsStream("rules/unserved.rules")))) {
            defaultRules = Rule.parseRules(Rule.rulesParserFromReader(reader));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }
}

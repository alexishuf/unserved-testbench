package br.ufsc.lapesd.unserved.testbench.io_composer.graph;

import br.ufsc.lapesd.unserved.testbench.io_composer.state.ExecutionPath;
import br.ufsc.lapesd.unserved.testbench.process.data.DataContextDiff;
import com.google.common.base.Preconditions;
import es.usc.citius.hipster.graph.GraphEdge;
import es.usc.citius.hipster.graph.HashBasedHipsterDirectedGraph;

import javax.annotation.Nonnull;

public abstract class HashBasedCompositionWorkingGraph<V, E, A>
        extends HashBasedHipsterDirectedGraph<V, E>
        implements CompositionWorkingGraph<V, E, A> {
    @Nonnull
    @Override
    public ChangeSet applyDiff(@Nonnull DataContextDiff diff, @Nonnull ExecutionPath<A> path) {
        SimpleChangeSet cs = createChangeSet(diff, path);
        cs.apply();
        return cs;
    }

    protected void deepCopy(HashBasedCompositionWorkingGraph<V, E, A> other) {
        vertices().forEach(other::add);
        edges().forEach(e -> other.connect(e.getVertex1(), e.getVertex2(), e.getEdgeValue()));
    }

    public abstract class BaseMutation implements SimpleChangeSet.Mutation {
        private boolean applied = false;

        @Override
        public void apply() {
            Preconditions.checkState(!applied);
            applied = true;
            doApply();
        }

        @Override
        public void undo() {
            Preconditions.checkState(applied);
            applied = false;
            doUndo();
        }

        protected abstract void doApply();
        protected abstract void doUndo();

    }

    public class AddVertex extends BaseMutation {
        private final V vertex;
        private boolean added = false;

        public AddVertex(V vertex) {
            this.vertex = vertex;
        }

        @Override
        public void doApply() {
            added = add(vertex);
        }

        @Override
        public void doUndo() {
            if (added) remove(vertex);
        }
    }

    public class RemoveVertex extends BaseMutation {
        private final V vertex;
        private boolean removed = false;

        public RemoveVertex(V vertex) {
            this.vertex = vertex;
        }

        @Override
        public void doApply() {
            Preconditions.checkState(!edgesOf(vertex).iterator().hasNext());
            removed = remove(vertex);
        }

        @Override
        public void doUndo() {
            if (removed)
                add(vertex);
        }
    }

    public class AddEdge extends BaseMutation {
        @Nonnull private final V from, to;
        @Nonnull private final E value;
        private GraphEdge<V, E> edge = null;

        public AddEdge(@Nonnull V from, @Nonnull V to, @Nonnull E value) {
            this.from = from;
            this.to = to;
            this.value = value;
        }

        @Override
        protected void doApply() {
            boolean has = getConnected().get(from).stream()
                    .anyMatch(e -> e.getVertex2().equals(to) && e.getEdgeValue().equals(value));
            if (has) return;
            edge = connect(from, to, value);
        }

        @Override
        protected void doUndo() {
            if (edge != null)
                getConnected().get(from).remove(edge);
        }
    }

    public class RemoveEdge extends BaseMutation {
        @Nonnull private final V from, to;
        @Nonnull private final E value;
        private boolean removed = true;

        public RemoveEdge(@Nonnull V from, @Nonnull V to, @Nonnull E value) {
            this.from = from;
            this.to = to;
            this.value = value;
        }

        @Override
        protected void doApply() {
            removed = getConnected().get(from).removeIf(e -> e.getVertex2().equals(to)
                    && e.getEdgeValue().equals(value));
        }

        @Override
        protected void doUndo() {
            if (removed)
                connect(from, to, value);
        }
    }


    protected abstract SimpleChangeSet createChangeSet(@Nonnull DataContextDiff diff,
                                                       @Nonnull ExecutionPath<A> path);
}

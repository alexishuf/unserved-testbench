package br.ufsc.lapesd.unserved.testbench.benchmarks.experiment;

import java.util.Map;

public interface ExperimentDesignLevels {
    Factors createFactors(Map<String, String> levels);
}

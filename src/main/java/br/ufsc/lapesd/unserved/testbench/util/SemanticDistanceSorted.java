package br.ufsc.lapesd.unserved.testbench.util;

import br.ufsc.lapesd.unserved.testbench.model.Variable;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.RDFS;

import javax.annotation.Nonnull;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.Stack;
import java.util.function.Consumer;

public final class SemanticDistanceSorted implements Comparable<SemanticDistanceSorted> {
    @Nonnull public final Variable variable;
    @Nonnull public final Variable target;
    private Integer distance = null;
    private boolean mismatch = false;

    public SemanticDistanceSorted(@Nonnull Variable variable, @Nonnull Variable target) {
        this.variable = variable;
        this.target = target;
    }

    @Nonnull
    public Variable getVariable() {
        return variable;
    }

    @Nonnull
    public Variable getTarget() {
        return target;
    }

    @Override
    public int compareTo(@Nonnull SemanticDistanceSorted rhs) {
        return Integer.compare(getDistance(), rhs.getDistance());
    }

    /**
     * Get number of subClassOf edges in the path from variable.getType()
     * to target.getType().
     * <p>
     * A positive value indicates variable.getType() is a subclass of target.getType().
     * A negative value indicates it is a superclass.
     */
    public int getDistance() {
        computeDistance();
        return distance;
    }

    public boolean isMismatch() {
        computeDistance();
        return mismatch;
    }

    private int computeDistance() {
        if (distance != null) return distance;

        Resource variableType = variable.getType(), targetType = target.getType();
        if (!variable.getRepresentation().equals(target.getRepresentation())) {
            mismatch = true;
            return distance = Integer.MAX_VALUE;
        }
        mismatch = false;

        if (variableType == null && targetType == null) return distance = 0;
        else if (Objects.equals(variableType, targetType)) return distance = 0;
        else if (variableType == null) return distance = Integer.MIN_VALUE;
        else if (targetType == null) return distance = Integer.MAX_VALUE;

        Stack<DistanceState> stack = new Stack<>();
        Set<Resource> visited = new HashSet<>();
        stack.push(new DistanceState(targetType, 0));
        while (!stack.isEmpty()) {
            DistanceState state = stack.pop();
            Resource type = state.resource;
            if (visited.contains(type)) continue;
            visited.add(type);
            if (type.equals(variableType))
                return distance = state.distance;

            state.forEachUpper(stack::add);
            state.forEachLower(stack::add);
        }

        mismatch = true;
        return distance = Integer.MAX_VALUE;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SemanticDistanceSorted that = (SemanticDistanceSorted) o;

        if (!variable.equals(that.variable)) return false;
        return target.equals(that.target);
    }

    @Override
    public int hashCode() {
        int result = variable.hashCode();
        result = 31 * result + target.hashCode();
        return result;
    }

    private static final class DistanceState {
        final Resource resource;
        final int distance;

        private DistanceState(Resource resource, int distance) {
            this.resource = resource;
            this.distance = distance;
        }

        private void forEachUpper(Consumer<DistanceState> consumer) {
            resource.listProperties(RDFS.subClassOf).forEachRemaining(r ->
                    consumer.accept(new DistanceState(r.getResource(), distance - 1)));
        }

        private void forEachLower(Consumer<DistanceState> consumer) {
            resource.getModel().listStatements(null, RDFS.subClassOf, resource)
                    .forEachRemaining(s -> consumer.accept(
                            new DistanceState(s.getSubject(), distance + 1)));
        }
    }

}

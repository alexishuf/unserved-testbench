package br.ufsc.lapesd.unserved.testbench.components.expressions.evaluator.instance;

import br.ufsc.lapesd.unserved.testbench.components.expressions.AtomicClassExpression;
import br.ufsc.lapesd.unserved.testbench.components.expressions.evaluator.ClassExpressionEvaluator.Result;
import br.ufsc.lapesd.unserved.testbench.components.expressions.evaluator.Step;
import org.apache.jena.rdf.model.RDFNode;

import javax.annotation.Nonnull;
import java.util.Collections;

public class AtomicInstanceStep extends Step<RDFNode> {
    @Nonnull private final AtomicClassExpression expression;

    public AtomicInstanceStep(@Nonnull AtomicClassExpression expression) {
        super(Collections.emptyList());
        this.expression = expression;
    }

    @Override
    protected Result performEvaluate(RDFNode node) {
        return Helper.isA(node, expression.getResource()) ? Result.TRUE : Result.INDETERMINATE;
    }
}

package br.ufsc.lapesd.unserved.testbench.components.expressions.evaluator;

import br.ufsc.lapesd.unserved.testbench.components.expressions.ClassExpression;
import br.ufsc.lapesd.unserved.testbench.components.expressions.Connective;
import br.ufsc.lapesd.unserved.testbench.components.expressions.evaluator.subclass.ConnectiveSubclassStep;
import br.ufsc.lapesd.unserved.testbench.components.expressions.evaluator.subclass.NamesSubclassStep;
import org.apache.jena.rdf.model.Resource;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.stream.Collectors;

public class ClassExpressionSubclassEvaluator extends AbstractClassExpressionEvaluator<Resource> {

    public ClassExpressionSubclassEvaluator(@Nonnull ClassExpression classExpression,
                                            @Nonnull Resource argument) {
        super(classExpression, createStep(classExpression), argument);
    }

    private static Step<Resource> createStep(ClassExpression expression) {
        switch (expression.getType()) {
            case Connective:
                List<Step<Resource>> children = expression.getChildren().stream()
                        .map(ClassExpressionSubclassEvaluator::createStep)
                        .collect(Collectors.toList());
                return new ConnectiveSubclassStep(children, (Connective)expression);
            case PropertyRestriction:
                return new NamesSubclassStep(expression);
            case Cardinality:
                return new NamesSubclassStep(expression);
            case Enumeration:
                return new NamesSubclassStep(expression);
            case Atomic:
                return new NamesSubclassStep(expression);
        }
        throw new UnsupportedOperationException();
    }
}

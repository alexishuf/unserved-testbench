package br.ufsc.lapesd.unserved.testbench.pragproof.execution;

import br.ufsc.lapesd.unserved.testbench.pragproof.context.PragProofContext;
import org.apache.jena.rdf.model.Resource;

import java.util.Collection;

public class PragProofResult implements AutoCloseable {
    private final PragProofContext proofContext;

    PragProofResult(PragProofContext proofContext) {
        this.proofContext = proofContext;
    }

    public Collection<Resource> getInputWantedVariables() {
        return proofContext.getInputWantedVariables();
    }

    public Resource getWantedVariable(Resource inputResource) {
        return proofContext.getWantedVariable(inputResource);
    }

    @Override
    public void close() {
        proofContext.close();
    }
}

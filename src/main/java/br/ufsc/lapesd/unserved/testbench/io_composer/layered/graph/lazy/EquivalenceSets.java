package br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.lazy;

import br.ufsc.lapesd.unserved.testbench.model.Variable;

import javax.annotation.Nonnull;
import java.util.*;
import java.util.stream.Collectors;

public class EquivalenceSets {
    private Map<Variable, Set<Variable>> eqSet = new HashMap<>();

    public EquivalenceSets() {
    }

    public EquivalenceSets(EquivalenceSets other) {
        for (Map.Entry<Variable, Set<Variable>> e : other.eqSet.entrySet())
            eqSet.put(e.getKey(), new HashSet<>(e.getValue()));
    }

    public @Nonnull Set<Variable> get(@Nonnull Variable var) {
        return eqSet.getOrDefault(var, Collections.singleton(var));
    }

    public void setEquivalent(@Nonnull Collection<Variable> vars) {
        HashSet<Variable> set = new HashSet<>(vars);
        for (Variable v : vars) {
            Set<Variable> vSet = eqSet.getOrDefault(v, null);
            if (vSet != null) set.addAll(vSet);
        }
        for (Variable v : set) eqSet.put(v, set);
    }

    public void setEquivalent(@Nonnull Variable a, @Nonnull Variable b) {
        Set<Variable> aSet = eqSet.getOrDefault(a, null), bSet = eqSet.getOrDefault(b, null);
        if (aSet != null && bSet == null) {
            aSet.add(b);
            eqSet.put(b, aSet);
        } else if (aSet == null && bSet != null) {
            bSet.add(a);
            eqSet.put(a, bSet);
        } else if (aSet == null) { //bSet == null
            aSet = new HashSet<>();
            aSet.add(a);
            aSet.add(b);
            eqSet.put(a, aSet);
            eqSet.put(b, aSet);
        } else { //aSet != null && bSet != null
            aSet.addAll(bSet);
            for (Variable bEq : bSet) eqSet.put(bEq, aSet);
        }
    }

    public void setDistinct(Variable var) {
        setDistinctEquivalenceSet(var);
    }
    public void setDistinctEquivalenceSet(Variable... vars) {
        setDistinctEquivalenceSet(Arrays.asList(vars));
    }
    public void setDistinctEquivalenceSet(Collection<Variable> vars) {
        HashSet<Variable> set = new HashSet<>(vars);
        for (Variable v : vars) {
            Set<Variable> vSet = eqSet.getOrDefault(v, null);
            if (vSet != null) vSet.remove(v);
            eqSet.put(v, set);
        }
    }

    public void removeVar(Variable var) {
        Set<Variable> old = eqSet.remove(var);
        if (old != null) {
            for (Variable equiv : old) eqSet.get(equiv).remove(var);
        }
    }

    public Set<Set<Variable>> getSets() {
        return eqSet.values().stream().distinct().collect(Collectors.toSet());
    }

    public boolean areEquivalent(@Nonnull Variable a, @Nonnull Variable b) {
        Set<Variable> set = eqSet.getOrDefault(a, null);
        if (set != null) return set.contains(b);
        set = eqSet.getOrDefault(b, null);
        return set != null && set.contains(a);
    }

    public boolean areDistinct(@Nonnull Variable a, @Nonnull Variable b) {
        Set<Variable> aSet = eqSet.getOrDefault(a, null), bSet = eqSet.getOrDefault(b, null);
        if (aSet != null && bSet != null && aSet != bSet) return true;
        return a.isValueBound() && b.isValueBound() && a.getValue().equals(b.getValue());
    }

    @Override
    public String toString() {
        return "{" + eqSet.values().stream().distinct()
                .map(s -> "{" + s.stream().map(Object::toString).reduce((l, r) -> l + ", " + r)
                        .orElse("") + "}")
                .reduce((l, r) -> l + ",  " + r).orElse("") + "}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EquivalenceSets that = (EquivalenceSets) o;
        return Objects.equals(eqSet, that.eqSet);
    }

    @Override
    public int hashCode() {
        return Objects.hash(eqSet);
    }
}

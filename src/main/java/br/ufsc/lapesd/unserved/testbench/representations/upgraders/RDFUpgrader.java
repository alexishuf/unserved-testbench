package br.ufsc.lapesd.unserved.testbench.representations.upgraders;

import br.ufsc.lapesd.unserved.testbench.iri.SPARMediaType;
import br.ufsc.lapesd.unserved.testbench.iri.UnservedX;
import br.ufsc.lapesd.unserved.testbench.representations.RepresentationUpgrader;
import br.ufsc.lapesd.unserved.testbench.representations.UpgradesRepresentation;
import br.ufsc.lapesd.unserved.testbench.util.GeneralizedContentType;
import com.google.common.base.Preconditions;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.vocabulary.RDF;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@UpgradesRepresentation("https://alexishuf.bitbucket.io/2016/04/unserved/unserved-x/media-type.ttl#MediaType")
public class RDFUpgrader implements RepresentationUpgrader {
    private final Set<String> rdfContentTypes = Stream.of(
            "application/rdf+xml",
            "application/n-triples",
            "application/n-quads",
            "text/turtle",
            "application/ld+json").collect(Collectors.toSet());

    @Override
    public boolean upgradeable(Resource representation) {
        return getMediaType(representation) != null;
    }

    @Override
    public void upgrade(Resource representation) {
        Resource mediaType = getMediaType(representation);
        Preconditions.checkArgument(mediaType != null);

        representation.addProperty(RDF.type, UnservedX.RDF.RDF);
        representation.addProperty(UnservedX.RDF.rdfMediaType, mediaType);
    }

    private Resource getMediaType(Resource representation) {
        if (!representation.hasProperty(RDF.type, UnservedX.MediaType.MediaType)) return null;
        Statement statement = representation.getProperty(UnservedX.MediaType.mediaTypeValue);
        if (statement == null || !statement.getObject().isLiteral()) return null;

        GeneralizedContentType type;
        type = GeneralizedContentType.parse(statement.getLiteral().getLexicalForm());
        if (type == null || !type.isConcrete()) return null;
        String name = type.getElements()[0].getName();
        if (!rdfContentTypes.contains(name)) return null;

        return SPARMediaType.getMediaType(name);
    }
}

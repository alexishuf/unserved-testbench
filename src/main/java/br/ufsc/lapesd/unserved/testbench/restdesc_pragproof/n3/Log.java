package br.ufsc.lapesd.unserved.testbench.restdesc_pragproof.n3;

import br.ufsc.lapesd.unserved.testbench.util.ResourcesBackground;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.riot.RDFFormat;

import java.util.Collections;

public class Log extends ResourcesBackground {
    public Log() {
        super(Collections.singletonList(ImmutablePair.of("restdesc_pragproof/n3/log.rdf",
                RDFFormat.RDFXML)), true);
    }

    public static String IRI = "http://www.w3.org/2000/10/swap/log";
    public static String PREFIX = IRI + "#";
    public static String SHORT_PREFIX = "log";

    private static Log instance = new Log();
    public static Log getInstance() {
        return instance;
    }

    public static Resource Chaff = r("Chaff");
    public static Resource Formula = r("Formula");
    public static Resource List = r("List");
    public static Resource Other = r("Other");
    public static Resource Truth = r("Truth");
    public static Property conclusion = p("conclusion");
    public static Property conjunction = p("conjunction");
    public static Property content = p("content");
    public static Property definitiveDocument = p("definitiveDocument");
    public static Property definitiveService = p("definitiveService");
    public static Property dtlit = p("dtlit");
    public static Property equalTo = p("equalTo");
    public static Property implies = p("implies");
    public static Property includes = p("includes");
    public static Property n3String = p("n3String");
    public static Property notEqualTo = p("notEqualTo");
    public static Property notIncludes = p("notIncludes");
    public static Property outputString = p("outputString");
    public static Property parsedAsN3 = p("parsedAsN3");
    public static Property racine = p("racine");
    public static Property rawType= p("rawType");
    public static Property rawUri = p("rawUri");
    public static Property semantics = p("semantics");
    public static Property semanticsOrError= p("semanticsOrError");
    public static Property uri = p("uri");

    private static Resource r(String localName) {
        return getInstance().getModel().createResource(PREFIX + localName);
    }
    private static Property p(String localName) {
        return getInstance().getModel().createProperty(PREFIX + localName);
    }
}

package br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.lazy;

import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.IndexedNode;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.Node;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.cost.CostInfo;
import br.ufsc.lapesd.unserved.testbench.io_composer.state.MessagePairAction;
import br.ufsc.lapesd.unserved.testbench.model.Variable;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

public interface SetNode {
    @Nonnull
    Set<IndexedNode> getNodes();

    @Nonnull
    Stream<Node> streamPlainNodes();

    @Nonnull
    EquivalenceSets getEquivalenceSets();

    @Nonnull
    List<MessagePairAction> getMessagePairs();

    @Nonnull
    Set<Variable> getInputs();

    @Nonnull
    Set<Variable> getOutputs();

    @Nonnull
    CostInfo getCostInfo();

    @Nonnull
    List<SetNode> getAlternatives();
}

package br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph;

import java.util.NoSuchElementException;

/**
 * An alternative, among others.
 *
 * Usually implementations are associated with some sort of collection of alternatives, these
 * should not be considered on the implementation of equals() and hashCode().
 *
 * @param <T> Type of alternatives. Must be assignable from  the implementing class.
 */
public interface Alternative<T> {
    /**
     * @return true iff there is an alternative to this object.
     */
    boolean hasAlternative();

    /**
     * Replace the object's guts with those of an alternative. The old object is removed from
     * the alternatives container (so it won't be obtainable again through successive
     * calls to this method).
     *
     * The order of alternatives iteration performed by this method is increasing w.r.t. cost.
     *
     * @throws NoSuchElementException if there is no alternative (<code>!hasAlternative()</code>).
     */
    void replaceWithAlternative() throws NoSuchElementException;
}

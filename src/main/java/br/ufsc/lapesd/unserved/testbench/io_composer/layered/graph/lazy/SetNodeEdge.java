package br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.lazy;

import br.ufsc.lapesd.unserved.testbench.io_composer.state.Copies;

import javax.annotation.Nonnull;

/**
 * Edge among {@link SetNode} instances
 */
public class SetNodeEdge {
    private final @Nonnull SetNode from;
    private final @Nonnull SetNode to;
    private final @Nonnull Copies inputAssignments;
    private final boolean noOp;

    private SetNodeEdge(@Nonnull SetNode from, @Nonnull SetNode to,
                       @Nonnull Copies inputAssignments, boolean noOp) {
        this.from = from;
        this.to = to;
        this.inputAssignments = inputAssignments;
        this.noOp = noOp;
    }

    public SetNodeEdge(@Nonnull SetNode from, @Nonnull SetNode to,
                       @Nonnull Copies inputAssignments) {
        this(from, to, inputAssignments, false);
    }

    @Nonnull
    public SetNodeEdge withFrom(@Nonnull SetNode from) {
        return new SetNodeEdge(from, to, inputAssignments, noOp);
    }

    @Nonnull
    public SetNodeEdge withTo(@Nonnull SetNode to) {
        return new SetNodeEdge(from, to, inputAssignments, noOp);
    }

    @Nonnull
    public static SetNodeEdge createNoOp(@Nonnull SetNode from, @Nonnull SetNode to) {
        return new SetNodeEdge(from, to, new Copies(), true);
    }

    @Nonnull
    public SetNode getFrom() {
        return from;
    }
    @Nonnull
    public SetNode getTo() {
        return to;
    }
    @Nonnull
    public Copies getInputAssignments() {
        return inputAssignments;
    }
    public boolean isNoOp() {
        return noOp;
    }
}

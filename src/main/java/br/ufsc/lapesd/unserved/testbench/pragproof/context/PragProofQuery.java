package br.ufsc.lapesd.unserved.testbench.pragproof.context;

import br.ufsc.lapesd.unserved.testbench.input.RDFInput;

/**
 * A goal in the form of a filter rule.
 */
public interface PragProofQuery extends AutoCloseable {
    RDFInput getInput();

    /**
     * Creates a new {@link PragProofQuery} that contains the same query as this but
     * changes on the copy are not visible on this object. An implementation may allow
     * changes in this object to be reflected on the copy.
     *
     * @return A non-isolated copy
     */
    PragProofQuery createNonIsolatedCopy();
}

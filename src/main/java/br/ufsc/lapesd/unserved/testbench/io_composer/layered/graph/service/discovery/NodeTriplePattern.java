package br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.discovery;

import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.atomic.TriplePattern;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.Node;

import javax.annotation.Nonnull;
import java.util.Objects;

public class NodeTriplePattern {
    public final @Nonnull Node node;
    public final @Nonnull TriplePattern triplePattern;

    public NodeTriplePattern(@Nonnull Node node, @Nonnull TriplePattern triplePattern) {
        this.node = node;
        this.triplePattern = triplePattern;
    }

    public @Nonnull Node getNode() {
        return node;
    }
    public @Nonnull TriplePattern getTriplePattern() {
        return triplePattern;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        NodeTriplePattern that = (NodeTriplePattern) o;
        return Objects.equals(node, that.node) &&
                Objects.equals(triplePattern, that.triplePattern);
    }

    @Override
    public int hashCode() {

        return Objects.hash(node, triplePattern);
    }
}

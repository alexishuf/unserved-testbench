package br.ufsc.lapesd.unserved.testbench.byte_renderers.xsd;

import br.ufsc.lapesd.unserved.testbench.byte_renderers.Render;
import br.ufsc.lapesd.unserved.testbench.byte_renderers.Renderer;
import br.ufsc.lapesd.unserved.testbench.byte_renderers.SimpleRender;
import br.ufsc.lapesd.unserved.testbench.components.DefaultComponentFactory;
import br.ufsc.lapesd.unserved.testbench.model.Variable;
import com.google.common.base.Preconditions;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.vocabulary.XSD;

import java.nio.charset.Charset;

public class XSDStringRenderer implements Renderer {
    @DefaultComponentFactory
    public static class Factory extends XSDRendererFactory {
        public Factory() {
            super(XSDStringRenderer.class, Factory.class, XSD.xstring.getURI());
        }
    }

    @Override
    public Render format(Variable variable) {
        RDFNode node = variable.getValue();

        Preconditions.checkArgument(node.isLiteral());
        Charset charset = Charset.forName("UTF-8");
        byte[] bytes = node.asLiteral().getString().getBytes(charset);//throws error
        return new SimpleRender(bytes, Render.Type.STRING, charset);
    }
}

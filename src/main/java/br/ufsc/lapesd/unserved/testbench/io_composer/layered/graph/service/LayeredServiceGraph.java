package br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service;

import br.ufsc.lapesd.unserved.testbench.io_composer.impl.DefaultIOComposerTimes;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.SetMultimap;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.*;
import java.util.stream.Collectors;

public class LayeredServiceGraph {
    private final @Nonnull List<ServiceLayer> layers;
    private final @Nonnull Map<Node, ProviderSet> providerSets;
    private final @Nonnull StartNode startNode;
    private final @Nonnull EndNode endNode;
    private final  @Nonnull Map<Node, Integer> nodeLayer;
    private final @Nonnull SetMultimap<Node, Node> successors;
    private @Nullable DefaultIOComposerTimes times;

    public LayeredServiceGraph(@Nonnull List<ServiceLayer> layers,
                               @Nonnull Map<Node, ProviderSet> providerSets,
                               @Nonnull StartNode startNode, @Nonnull EndNode endNode,
                               @Nonnull Map<Node, Integer> nodeLayer) {
        this(layers, providerSets, startNode, endNode, nodeLayer, computeSuccessors(providerSets));

    }

    public LayeredServiceGraph(@Nonnull List<ServiceLayer> layers,
                               @Nonnull Map<Node, ProviderSet> providerSets,
                               @Nonnull StartNode startNode, @Nonnull EndNode endNode,
                               @Nonnull Map<Node, Integer> nodeLayer,
                               @Nonnull SetMultimap<Node, Node> successors) {
        assert layers.stream().flatMap(l -> l.getNodes().stream())
                .allMatch(providerSets::containsKey);
        this.layers = layers;
        this.providerSets = providerSets;
        this.startNode = startNode;
        this.endNode = endNode;
        this.nodeLayer = nodeLayer;
        this.successors = successors;
    }

    private static SetMultimap<Node, Node> computeSuccessors(Map<Node, ProviderSet> providerSets) {
        SetMultimap<Node, Node> successors = HashMultimap.create();
        for (Map.Entry<Node, ProviderSet> e : providerSets.entrySet())
            e.getValue().forEach(n -> successors.put(n, e.getKey()));
        return successors;
    }

    @Nullable
    public DefaultIOComposerTimes getTimes() {
        return times;
    }

    public void setTimes(@Nullable DefaultIOComposerTimes times) {
        this.times = times;
    }

    public List<ServiceLayer> getLayers() {
        return Collections.unmodifiableList(layers);
    }

    @Nonnull
    public StartNode getStartNode() {
        return startNode;
    }

    @Nonnull
    public EndNode getEndNode() {
        return endNode;
    }

    /**
     * Gets a {@link ProviderSet} whose targets are all inputs of target, or null if no such
     * set exists.
     */
    @Nonnull
    public ProviderSet getProviderSet(@Nonnull Node target) {
        return providerSets.getOrDefault(target, null);
    }

    /**
     * A Partial successor is a node, at a layer after that of the predecessor, for which at least
     * one input can be satisfied with an output of predecessor.
     *
     * @param predecessor Node to check for successors.
     * @return Unmodifiable, possibly empty set of partial successors.
     */
    @Nonnull
    public Set<Node> getPartialSuccessors(@Nonnull Node predecessor) {
        return Collections.unmodifiableSet(successors.get(predecessor));
    }

    /**
     * A node is in the graph if it is placed at one of the layers.
     *
     * @param node A Node to check for
     * @return true iff node is present in this graph.
     */
    public boolean containsNode(@Nonnull Node node) {
        return nodeLayer.containsKey(node);
    }

    /**
     * Gets the index of the layer where node is contained.
     * @param node Node to be searched for.
     * @throws NoSuchElementException if there is no layer containing node in this graph.
     */
    public int getNodeLayer(@Nonnull Node node) throws NoSuchElementException {
        Integer index = nodeLayer.getOrDefault(node, null);
        if (index == null) throw new NoSuchElementException();
        return index;
    }

    public boolean replaceNode(@Nonnull Node old, @Nonnull Node replacement) {
        return layers.get(getNodeLayer(old)).replaceNode(old, replacement);
    }

    public Duplicator createWriteIsolatedDuplicator() {
        List<ServiceLayer> layers = this.layers.stream()
                .map(l -> new ServiceLayer(l.getNodes()))
                .collect(Collectors.toCollection(() -> new ArrayList<>(this.layers.size())));

        Map<Node, Integer> nodeLayer = new HashMap<>();
        for (Map.Entry<Node, Integer> e : this.nodeLayer.entrySet())
            nodeLayer.put(e.getKey(), e.getValue());

        Map<Node, ProviderSet> providerSets = new HashMap<>();
        for (Map.Entry<Node, ProviderSet> e : this.providerSets.entrySet()) {
            IOProviderHashSet dup = new IOProviderHashSet();
            e.getValue().getTargets().forEach(dup::addTarget);
            e.getValue().getAssignmentsStream().forEach(a -> dup.add(a.target, a.node, a.output));
            providerSets.put(e.getKey(), dup);
        }

        SetMultimap<Node, Node> successors = HashMultimap.create();
        for (Map.Entry<Node, Node> e : this.successors.entries())
            successors.put(e.getKey(), e.getValue());

        LayeredServiceGraph dup = new LayeredServiceGraph(layers, providerSets, startNode, endNode,
                nodeLayer, successors);
        return new Duplicator(dup);
    }

    public class Duplicator {
        private final @Nonnull LayeredServiceGraph duplicate;

        public Duplicator(@Nonnull LayeredServiceGraph duplicate) {
            this.duplicate = duplicate;
        }

        @Nonnull
        public LayeredServiceGraph getDuplicate() {
            return duplicate;
        }
    }
}

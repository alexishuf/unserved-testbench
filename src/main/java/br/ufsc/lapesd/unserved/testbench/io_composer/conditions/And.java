package br.ufsc.lapesd.unserved.testbench.io_composer.conditions;

import br.ufsc.lapesd.unserved.testbench.model.Variable;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public class And extends ConditionList {
    public static final And EMPTY = new And();

    public And(Condition... conditions) {
        super("&", conditions);
    }

    public And(@Nonnull Collection<Condition> collection) {
        super("&", collection);
    }

    public And(@Nonnull Collection<Condition> colA, @Nonnull Collection<Condition> colB) {
        super("&", colA, colB);
    }

    public static And of(Condition... conditions) {
        if (conditions.length == 0) return EMPTY;
        else if (conditions.length == 1 && conditions[0] instanceof And) return (And)conditions[0];
        else return new And(conditions);
    }

    public static And of(@Nonnull Collection<Condition> conditions) {
        if (conditions.isEmpty()) return EMPTY;
        else if (conditions.size() == 1 && conditions.iterator().next() instanceof And)
            return (And)conditions.iterator().next();
        else return new And(conditions);
    }

    public static @Nonnull And empty() { return EMPTY; }

    @Nonnull
    @Override
    public Condition replacing(@Nonnull Map<Variable, Variable> current2replacement) {
        List<Condition> replaced = new ArrayList<>(getMembers().size());
        for (Condition child : getMembers())
            replaced.add(child.replacing(current2replacement));
        return new And(replaced);
    }

    public static And add(And left, And right) {
        if (left.isEmpty() && right.isEmpty()) return EMPTY;
        else if (left.isEmpty()) return right;
        else if (right.isEmpty()) return left;
        return new And(left.getMembers(), right.getMembers());
    }
}

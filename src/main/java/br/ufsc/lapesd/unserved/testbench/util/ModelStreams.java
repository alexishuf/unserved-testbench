package br.ufsc.lapesd.unserved.testbench.util;

import com.google.common.base.Preconditions;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.util.iterator.ExtendedIterator;

import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * Helper to get streams from common queries done on {@link Model} instances.
 */
public class ModelStreams {
    public static <T extends RDFNode, U extends RDFNode> Stream<T>
    toStream(ExtendedIterator<U> it, Class<T> aClass) {
        return StreamSupport.stream(
                Spliterators.spliteratorUnknownSize(it, Spliterator.NONNULL|Spliterator.DISTINCT),
                false).filter(r -> r.canAs(aClass)).map(r -> r.as(aClass));
    }

    public static <T extends Resource> Stream<T>
    subjectsWithProperty(Model model, Property property, RDFNode object, Class<T> aClass) {
        return toStream(model.listSubjectsWithProperty(property, object), aClass);
    }
    public static <T extends Resource> Stream<T>
    subjectsWithProperty(Property property, RDFNode object, Class<T> aClass) {
        Preconditions.checkArgument(object.getModel() != null);
        return toStream(object.getModel().listSubjectsWithProperty(property, object), aClass);
    }

    public static <T extends Resource> Stream<T>
    subjectsWithProperty(Model model, Property property, Class<T> aClass) {
        return toStream(model.listSubjectsWithProperty(property), aClass);
    }

    public static <T extends RDFNode> Stream<T>
    objectsOfProperty(Model model, Property property, Class<T> aClass) {
        return toStream(model.listObjectsOfProperty(property), aClass);
    }
    public static <T extends RDFNode> Stream<T>
    objectsOfProperty(Model model, Resource subject, Property property, Class<T> aClass) {
        return toStream(model.listObjectsOfProperty(subject, property), aClass);
    }
    public static <T extends RDFNode> Stream<T>
    objectsOfProperty(Resource subject, Property property, Class<T> aClass) {
        Preconditions.checkArgument(subject.getModel() != null);
        return toStream(subject.getModel().listObjectsOfProperty(subject, property), aClass);
    }
}

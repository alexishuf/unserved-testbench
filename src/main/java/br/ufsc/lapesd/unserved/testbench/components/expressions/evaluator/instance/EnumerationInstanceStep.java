package br.ufsc.lapesd.unserved.testbench.components.expressions.evaluator.instance;

import br.ufsc.lapesd.unserved.testbench.components.expressions.Enumeration;
import br.ufsc.lapesd.unserved.testbench.components.expressions.evaluator.ClassExpressionEvaluator.Result;
import br.ufsc.lapesd.unserved.testbench.components.expressions.evaluator.Step;
import org.apache.jena.rdf.model.RDFNode;

import javax.annotation.Nonnull;
import java.util.Collections;

public class EnumerationInstanceStep extends Step<RDFNode> {
    @Nonnull private final Enumeration expr;

    public EnumerationInstanceStep(@Nonnull Enumeration expression) {
        super(Collections.emptyList());
        this.expr = expression;
    }

    @Override
    protected Result performEvaluate(RDFNode node) {
        if (Helper.isA(node, expr.getNames())) return Result.TRUE;
        return  (node.isResource() && expr.getIndividuals().contains(node.asResource()))
                ? Result.TRUE : Result.FALSE;
    }
}

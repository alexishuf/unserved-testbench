package br.ufsc.lapesd.unserved.testbench.pragproof;

//FIXME: This is being used for both io/erros and logic errors (ie: inference fuse)
public class PragProofProverException extends Exception {
    public PragProofProverException(String message) {
        super(message);
    }
    public PragProofProverException(String message, Exception cause) {
        super(message, cause);
    }

    public PragProofProverException(Exception cause) {
        super(cause);
    }
}

package br.ufsc.lapesd.unserved.testbench.model.http.modifiers;

import br.ufsc.lapesd.unserved.testbench.iri.UnservedX;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.vocabulary.RDF;

import java.util.HashMap;
import java.util.Map;

public class DefaultAsHttpPropertyModifierHandler extends DelegatingAsHttpPropertyModifierHandler {
    private Map<Resource, AsHttpPropertyModifierHandlerFactory> factories = new HashMap<>();

    public static class Factory implements AsHttpPropertyModifierHandlerFactory {
        @Override
        public AsHttpPropertyModifierHandler newInstance() {
            return new DefaultAsHttpPropertyModifierHandler();
        }
    }

    public DefaultAsHttpPropertyModifierHandler(Map<Resource, AsHttpPropertyModifierHandlerFactory> factories) {
        this.factories = factories;
    }

    public DefaultAsHttpPropertyModifierHandler() {
        this(defaultFactories());
    }

    private static Map<Resource, AsHttpPropertyModifierHandlerFactory> defaultFactoriesMap = null;
    private static Map<Resource, AsHttpPropertyModifierHandlerFactory> defaultFactories() {
        if (defaultFactoriesMap == null) {
            defaultFactoriesMap = new HashMap<>();
            defaultFactoriesMap.put(UnservedX.HTTP.URITemplateAssignment,
                    URITemplateAssignmentHandler::new);
        }
        return defaultFactoriesMap;
    }

    @Override
    protected AsHttpPropertyModifierHandler createHandler(Resource modifier) {
        StmtIterator it = modifier.listProperties(RDF.type);
        while (it.hasNext()) {
            RDFNode object = it.next().getObject();
            if (!object.isResource()) continue;
            Resource type = object.asResource();
            AsHttpPropertyModifierHandlerFactory factory = factories.get(type);
            if (factory != null) {
                return factory.newInstance();
            }
        }
        return null;
    }
}

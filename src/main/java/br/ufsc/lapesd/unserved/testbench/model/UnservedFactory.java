package br.ufsc.lapesd.unserved.testbench.model;

import br.ufsc.lapesd.unserved.testbench.iri.Unserved;
import br.ufsc.lapesd.unserved.testbench.model.impl.*;
import org.apache.jena.enhanced.BuiltinPersonalities;
import org.apache.jena.enhanced.Personality;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;

/**
 * Register interfaces and implementations with {@link BuiltinPersonalities}
 */
public class UnservedFactory {
    public static UnservedFactory instance = new UnservedFactory();

    public static void install() {
        instance.doInstall();
    }

    public static void  install(Personality<RDFNode> p) {
        p.add(Variable.class,          VariableImpl.factory         );
        p.add(Message.class,           MessageImpl.factory          );
        p.add(Reaction.class,          ReactionImpl.factory         );
        p.add(Spontaneous.class,       SpontaneousImpl.factory      );
        p.add(ValuedCardinality.class, ValuedCardinalityImpl.factory);
        p.add(Part.class,              PartImpl.factory             );
    }

    private synchronized void doInstall() {
        install(BuiltinPersonalities.model);
    }

    public static When asWhen(Resource resource) {
        if (resource.canAs(Spontaneous.class))
            return resource.as(Spontaneous.class);
        else if (resource.canAs(Reaction.class))
            return resource.as(Reaction.class);
        else return null;
    }

    public static Cardinality asCardinality(Resource resource) {
        if (resource.equals(Unserved.one))
            return Unserved.one.as(ValuedCardinality.class);
        if (resource.equals(Unserved.many))
            return Unserved.many.as(ValuedCardinality.class);
        if (resource.canAs(ValuedCardinality.class))
            return resource.as(ValuedCardinality.class);
        return null;
    }
}

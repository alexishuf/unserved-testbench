package br.ufsc.lapesd.unserved.testbench.benchmarks.design1.data;

import br.ufsc.lapesd.unserved.testbench.benchmarks.design1.D1Factors;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.io.Serializable;

class ServiceGraphFactors implements Serializable {
    private int serviceCount, inputSetsSize, inputSetsCount;

    public ServiceGraphFactors(D1Factors factors) {
        serviceCount = factors.getServiceCount();
        inputSetsSize = factors.getInputSetsSize();
        inputSetsCount = factors.getInputSetsCount();
    }

    public int getServiceCount() {
        return serviceCount;
    }
    public int getInputSetsSize() {
        return inputSetsSize;
    }
    public int getInputSetsCount() {
        return inputSetsCount;
    }

    @Override
    public String toString() {
        return String.format("ServiceGraphFactors(svcs: %d, setsSize: %d, setsCount: %d)",
                getServiceCount(), getInputSetsSize(), getInputSetsCount());
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getServiceCount()).append(getInputSetsSize())
                .append(getInputSetsCount()).hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) return true;
        if (o == null) return false;
        if (!(o instanceof ServiceGraphFactors)) return false;
        ServiceGraphFactors rhs = (ServiceGraphFactors) o;
        return getServiceCount() == rhs.getServiceCount()
                && getInputSetsSize() == rhs.getInputSetsSize()
                && getInputSetsCount() == rhs.getInputSetsCount();
    }
}

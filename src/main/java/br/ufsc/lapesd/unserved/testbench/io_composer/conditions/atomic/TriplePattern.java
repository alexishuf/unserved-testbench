package br.ufsc.lapesd.unserved.testbench.io_composer.conditions.atomic;

import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.Condition;
import br.ufsc.lapesd.unserved.testbench.iri.Unserved;
import br.ufsc.lapesd.unserved.testbench.model.Variable;
import br.ufsc.lapesd.unserved.testbench.util.PrefixUtils;
import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.spinrdf.vocabulary.SP;

import javax.annotation.Nonnull;
import java.util.*;

public class TriplePattern implements Condition {
    private final @Nonnull Term subject, predicate, object;

    public enum Pos {
        Subject,
        Predicate,
        Object ;
        public Term get(TriplePattern p) {
            switch (this) {
                case Subject:   return p.getSubject();
                case Predicate: return p.getPredicate();
                case Object:    return p.getObject();
            }
            throw new IllegalArgumentException();
        }
    }

    public static class Term {
        private final RDFNode node;

        public Term(RDFNode node) {
            this.node = node;
        }

        public boolean isVariable() {
            return isUnservedVariable() || isSPINVariable();
        }

        public boolean isUnservedVariable() {
            return asNode().canAs(Variable.class);
        }
        public boolean isSPINVariable() {
            return isResource() && asResource().hasProperty(SP.varName);
        }

        public Variable asUnservedVariable() {
            return asNode().as(Variable.class);
        }
        public org.spinrdf.model.Variable asSPINVariable() {
            return asNode().as(org.spinrdf.model.Variable.class);
        }

        public boolean isAnon() { return asNode().isAnon(); }
        public boolean isURIResource() { return asNode().isURIResource(); }
        public Resource asResource() { return asNode().asResource(); }
        public Literal asLiteral()  { return asNode().asLiteral(); }
        public boolean isResource()  { return asNode().isResource(); }
        public boolean isLiteral()  { return asNode().isLiteral(); }
        public <T extends RDFNode> boolean canAs(Class<T> c) { return asNode().canAs(c); }

        public RDFNode asNode() {
            return node;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Term term = (Term) o;
            return Objects.equals(node, term.node);
        }

        @Override
        public int hashCode() { return Objects.hash(node); }

        @Override
        public String toString() {
            if (isLiteral()) return asLiteral().getLexicalForm();
            Resource r = asResource();
            Statement s = r.getProperty(SP.varName);
            if (s != null)
                return "?" + s.getLiteral().getLexicalForm();
            if (isUnservedVariable()) {
                Variable var = asUnservedVariable();
                if (var.isURIResource()) {
                    return "?" + PrefixUtils.prefix(var);
                }
                Resource type = var.getType();
                if (type != null && type.isURIResource()) {
                    int hash = r.isURIResource() ? r.getURI().hashCode() : r.getId().hashCode();
                    return "?a" + (hash % 100) + PrefixUtils.prefix(type);
                }
            }

            return r.toString();
        }
    }

    public TriplePattern(@Nonnull Resource subject,
                         @Nonnull Resource predicate,
                         @Nonnull RDFNode object) {
        this.subject = new Term(subject);
        this.predicate = new Term(predicate);
        this.object = new Term(object);
    }

    public static boolean isTriplePattern(@Nonnull Resource r) {
        return r.hasProperty(SP.subject) && r.hasProperty(SP.predicate) && r.hasProperty(SP.object);
    }

    public static @Nonnull TriplePattern fromSPIN(@Nonnull Resource resource) {
        return new TriplePattern(resource.getPropertyResourceValue(SP.subject),
                resource.getPropertyResourceValue(SP.predicate),
                resource.getProperty(SP.object).getObject());

    }

    @Nonnull
    public Term getSubject() {
        return subject;
    }

    @Nonnull
    public Term getPredicate() {
        return predicate;
    }

    @Nonnull
    public Term getObject() {
        return object;
    }

    @Nonnull
    @Override
    public List<Variable> getVariables() {
        ArrayList<Variable> vars = new ArrayList<>(3);
        if (subject.isUnservedVariable()) vars.add(subject.asUnservedVariable());
        if (predicate.isUnservedVariable()) vars.add(predicate.asUnservedVariable());
        if (object.isUnservedVariable()) vars.add(object.asUnservedVariable());
        return vars;
    }

    private void bindFrom(@Nonnull Map<Variable, Variable> bindings,
                          @Nonnull Term mine, @Nonnull Term other,
                          boolean ignoreBound, boolean ignoreUnbound) {
        if (mine.isUnservedVariable()) {
            Variable variable = mine.asUnservedVariable();
            boolean isBound = variable.isValueBound();
            if ((!ignoreBound || !isBound) && (!ignoreUnbound || isBound)
                    && other.isUnservedVariable()) {
                bindings.put(variable, other.asUnservedVariable());
            }
        }
    }

    @Nonnull
    @Override
    public Map<Variable, Variable> bindFrom(@Nonnull Condition other, boolean ignoreBound,
                                            boolean ignoreUnbound) {
        if (!(other instanceof TriplePattern)) return Collections.emptyMap();
        Map<Variable, Variable> bindings = new HashMap<>();
        TriplePattern otp = (TriplePattern)other;
        bindFrom(bindings, subject, otp.subject, ignoreBound, ignoreUnbound);
        bindFrom(bindings, predicate, otp.predicate, ignoreBound, ignoreUnbound);
        bindFrom(bindings, object, otp.object, ignoreBound, ignoreUnbound);
        return bindings;
    }


    private RDFNode replacing(@Nonnull Map<Variable, Variable> current2replacement, 
                              @Nonnull Term term) {
        if (!term.isUnservedVariable()) return term.asNode();
        Variable var = term.asUnservedVariable();
        return current2replacement.getOrDefault(var, var);
    }
    
    @Nonnull
    @Override
    public Condition replacing(@Nonnull Map<Variable, Variable> current2replacement) {
        return new TriplePattern(replacing(current2replacement, subject  ).asResource(),
                                 replacing(current2replacement, predicate).asResource(),
                                 replacing(current2replacement, object   ));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TriplePattern that = (TriplePattern) o;
        return Objects.equals(subject, that.subject) &&
                Objects.equals(predicate, that.predicate) &&
                Objects.equals(object, that.object);
    }

    @Override
    public int hashCode() {
        return Objects.hash(subject, predicate, object);
    }

    @Override
    public String toString() {
        return subject.toString() + " " + predicate.toString() + " " + object.toString();
    }

    static {
        //noinspection ResultOfMethodCallIgnored
        SP.class.getFields();
        Unserved.init();
    }
}

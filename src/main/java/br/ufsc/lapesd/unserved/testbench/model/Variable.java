package br.ufsc.lapesd.unserved.testbench.model;

import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.atomic.VariableSpec;
import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * An enhanced node for unserved:Variable instances.
 */
public interface Variable extends WithParts {
    Resource getType();
    Resource getRepresentation();
    Resource getValueRepresentation();
    Resource getResourceValue();
    Literal getLiteralValue();
    RDFNode getValue();
    Resource getValueRepresentation(int index);
    Resource getResourceValue(int index);
    Literal getLiteralValue(int index);
    RDFNode getValue(int index);
    VariableSpec asSpec();
    boolean isValueBound();
    boolean isValueBound(int index);

    @Nonnull Variable enhance(@Nonnull Resource other);

    /**
     * Returns true if this {@link Variable} is not indexed and <code>other</code> is indexed
     * within this {@link Variable}.
     *
     * @return true if other is an indexed variable within this
     */
    boolean matches(Variable other);

    void unset(int index);
    void set(int index, @Nullable Resource representation, @Nonnull Literal literal);
    void set(int index, @Nullable Resource representation, @Nonnull Resource resource);
}

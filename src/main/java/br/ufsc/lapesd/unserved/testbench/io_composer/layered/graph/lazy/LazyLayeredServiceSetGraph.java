package br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.lazy;

import br.ufsc.lapesd.unserved.testbench.components.DefaultComponentRegistry;
import br.ufsc.lapesd.unserved.testbench.components.RulesComponentRegistry;
import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.atomic.VariableSpec;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.PathDiffValidator;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.LayeredGraph;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.IndexedNode;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.LayeredServiceGraph;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.Node;
import br.ufsc.lapesd.unserved.testbench.io_composer.state.*;
import br.ufsc.lapesd.unserved.testbench.iri.UnservedP;
import br.ufsc.lapesd.unserved.testbench.model.Variable;
import br.ufsc.lapesd.unserved.testbench.process.SimpleContext;
import br.ufsc.lapesd.unserved.testbench.process.UnservedPInterpreter;
import br.ufsc.lapesd.unserved.testbench.process.actions.ActionExecutionException;
import br.ufsc.lapesd.unserved.testbench.process.actions.CopyRunner;
import br.ufsc.lapesd.unserved.testbench.process.data.DataContext;
import br.ufsc.lapesd.unserved.testbench.process.data.DataContextDiff;
import br.ufsc.lapesd.unserved.testbench.process.data.DiffingDataContext;
import br.ufsc.lapesd.unserved.testbench.process.data.action.DataContextAction;
import br.ufsc.lapesd.unserved.testbench.util.MatchTable;
import br.ufsc.lapesd.unserved.testbench.util.MinimalSemanticDistance;
import com.google.common.base.Preconditions;
import com.google.common.base.Stopwatch;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.RDF;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static br.ufsc.lapesd.unserved.testbench.iri.UnservedP.copyFrom;
import static br.ufsc.lapesd.unserved.testbench.iri.UnservedP.copyTo;
import static org.apache.commons.lang3.tuple.ImmutablePair.of;

/**
 * This is a cranky class that provides lazy access to a set version of the
 * {@link LayeredServiceGraph}, where each node is a set of service nodes and edges correspond to
 * to I/O compatibility among the sets.
 *
 * Computing this set beforehand is impractical due to space state explosion. This implementation
 * uses backward connectivity information already present in {@link LayeredServiceGraph} to
 * lazily compute the graph backwards. This alone is sufficient for a backward A* search.
 *
 * However, some algorithms, such as D* Lite despite progressing from the goal to the start node,
 * require consistent forward access. Since there is no waste-free method for computing all
 * successors of a {@link SetNode} here, for every new possible successor discovered (new
 * {@link SetNode} instances are only created as side effect of predecessors()), a notification
 * is delivered to listeners. The notification contains an object, which again lazily computes
 * which SetNodes already instantiated in the graph are new successors of a node.
 *
 * D* Lite needs to be modified in order to cope with these changes to the graph as the graph is
 * searched, but such change is not too complicated.
 */
public class LazyLayeredServiceSetGraph implements LayeredGraph<Action, SetNodeEdge, SetNode> {
    private static final Logger logger = LoggerFactory.getLogger(LazyLayeredServiceSetGraph.class);

    private final @Nonnull LayeredServiceGraph serviceGraph;
    private final @Nonnull SetNode startNode;
    private final @Nonnull SetNode endNode;
    private final @Nonnull BackwardSetNodeProvider setNodeProvider;
    private final @Nonnull List<Set<SetNode>> layers;
    private final @Nonnull Set<SetNode> removed;
    private @Nonnull Map<SetNode, Set<SetNode>> forward = new HashMap<>();
    private @Nonnull Map<SetNode, Set<SetNode>> backward = new HashMap<>();
    private @Nonnull Map<SetNode, Set<SetNode>> reverseForward = new HashMap<>();
    private @Nonnull Map<SetNode, SetNode> adaptedOriginal = new HashMap<>();
    private @Nonnull Map<SetNode, SetNode> canonicToCurrentAdapted = new HashMap<>();
    private @Nonnull Map<SetNode, Collection<SetNodeEdge>> canonicIncoming = new HashMap<>();
    private @Nonnull LinkedHashSet<Listener> listeners = new LinkedHashSet<>();
    private final @Nonnull Map<ImmutablePair<SetNode, SetNode>, SetNodeEdge> edges;

    private LazyLayeredServiceSetGraph(@Nonnull LayeredServiceGraph serviceGraph,
                                       @Nonnull SetNode startNode, @Nonnull SetNode endNode,
                                       @Nonnull BackwardSetNodeProvider setNodeProvider,
                                       @Nonnull List<Set<SetNode>> layers,
                                       @Nonnull Set<SetNode> removed,
                                       @Nonnull Map<SetNode, Set<SetNode>> forward,
                                       @Nonnull Map<SetNode, Set<SetNode>> backward,
                                       @Nonnull Map<SetNode, Set<SetNode>> reverseForward,
                                       @Nonnull Map<ImmutablePair<SetNode, SetNode>, SetNodeEdge> edges,
                                       @Nonnull Map<SetNode, SetNode> adaptedOriginal,
                                       @Nonnull Map<SetNode, SetNode> canonicToCurrentAdapted,
                                       @Nonnull Map<SetNode, Collection<SetNodeEdge>> canonicIncoming) {
        this.serviceGraph = serviceGraph;
        this.startNode = startNode;
        this.endNode = endNode;
        this.setNodeProvider = setNodeProvider;
        this.layers = layers;
        this.removed = removed;
        this.forward = forward;
        this.backward = backward;
        this.reverseForward = reverseForward;
        this.edges = edges;
        this.adaptedOriginal = adaptedOriginal;
        this.canonicToCurrentAdapted = canonicToCurrentAdapted;
        this.canonicIncoming = canonicIncoming;
    }

    public LazyLayeredServiceSetGraph(@Nonnull LayeredServiceGraph sg,
                                      @Nonnull BackwardSetNodeProvider setNodeProvider) {
        this.edges = new HashMap<>();
        this.removed = new HashSet<>();
        this.serviceGraph = sg;
        startNode = new FullSetNode(Collections.singleton(IndexedNode.wrap(sg.getStartNode())), Collections.emptyList());
        endNode = new FullSetNode(Collections.singleton(IndexedNode.wrap(sg.getEndNode())), Collections.emptyList());
        this.setNodeProvider = setNodeProvider;

        int layerCount = sg.getLayers().size();
        layers = new ArrayList<>(layerCount);
        for (int i = 0; i < layerCount; i++) layers.add(new HashSet<>());
        layers.get(sg.getNodeLayer(sg.getStartNode())).add(startNode);
        layers.get(sg.getNodeLayer(sg.getEndNode())).add(endNode);

        setNodeProvider.setup(serviceGraph, startNode, endNode);
    }

    @Nonnull
    public LayeredServiceGraph getServiceGraph() {
        return serviceGraph;
    }

    @Nonnull
    @Override
    public SetNode getStart() {
        return startNode;
    }
    @Override
    @Nonnull
    public SetNode getEnd() {
        return endNode;
    }

    public void addListener(@Nonnull Listener listener) {
        listeners.add(listener);
    }

    public void removeListener(@Nonnull Listener listener) {
        listeners.remove(listener);
    }

    private void addForwardEdge(SetNodeEdge edge) {
        Set<SetNode> set = backward.getOrDefault(edge.getTo(), null);
        assert set == null || set.contains(edge.getFrom())
                : "Backward set is computed but doesn't contains edge.getFrom().";

        set = forward.getOrDefault(edge.getFrom(), null);
        if (set == null) forward.put(edge.getFrom(), set = new HashSet<>());
        set.add(edge.getTo());
        set = reverseForward.getOrDefault(edge.getTo(), null);
        if (set == null) reverseForward.put(edge.getFrom(), set = new HashSet<>());
        set.add(edge.getFrom());
        edges.put(of(edge.getFrom(), edge.getTo()), edge);
    }

    private void removeForwardEdge(SetNodeEdge edge) {
        Set<SetNode> set = forward.getOrDefault(edge.getFrom(), null);
        if (set != null) set.remove(edge.getTo());
        set = reverseForward.getOrDefault(edge.getTo(), null);
        if (set != null) set.remove(edge.getFrom());
        //empty sets remind that there are no forward edges (as opposed to not yet computed)
    }

    /**
     * Adds an edge to both forward and backward adjacency lists.
     *
     * @throws IllegalStateException if edge.getTo() doesn't have a computed backward set.
     */
    private void addEdge(SetNodeEdge edge) {
        assert backward.containsKey(edge.getTo());
        backward.get(edge.getTo()).add(edge.getFrom());
        addForwardEdge(edge);
        assert edges.containsKey(of(edge.getFrom(), edge.getTo()));
    }

    private void removeNode(SetNode node) {
        if (!removed.add(node)) return;

        backward.getOrDefault(node, Collections.emptySet()).stream()
                .map(p -> of(p, node)).forEach(edges::remove);
        forward.getOrDefault(node, Collections.emptySet()).stream()
                .map(s -> of(node, s)).forEach(edges::remove);
        backward.remove(node);
        forward.remove(node);
        Set<SetNode> set = reverseForward.getOrDefault(node, null);
        if (set != null) {
            reverseForward.remove(node);
            set.stream().map(p -> forward.getOrDefault(p, null)).filter(Objects::nonNull)
                    .forEach(fSet -> fSet.remove(node));
        }
    }

    private void checkReverseForward(SetNode successor) {
        Set<SetNode> set = reverseForward.getOrDefault(successor, null);
        if (set == null) return; //no work to do
        Set<SetNode> predecessors = backward.get(successor);
        if (predecessors == null) return; //no work to do

        /* apply changes */
        ArrayList<SetNodeEdge> removed = new ArrayList<>(set.size());
        for (SetNode from : set) {
            if (predecessors.contains(from)) continue;
            SetNodeEdge e = edges.get(of(from, successor));
            if (e == null) continue;
            removed.add(e);
        }
        for (SetNodeEdge e : removed) removeForwardEdge(e);
        /* notify changes */
        Set<SetNode> singleton = Collections.singleton(successor);
        for (SetNodeEdge e : removed) {
            RemovedSuccessors rs = new RemovedSuccessors(e.getFrom(), singleton);
            listeners.forEach(l -> l.removedSuccessors(rs));
        }
    }

    @Nonnull
    public Collection<SetNode> successors(@Nonnull SetNode node) {
        return forward.getOrDefault(node, Collections.emptySet());
    }

    @Override
    public int getLayer(@Nonnull SetNode node) {
        if (node instanceof EmptySetNode) return ((EmptySetNode) node).getLayer();
        assert  node.streamPlainNodes().filter(serviceGraph::containsNode)
                .map(serviceGraph::getNodeLayer).collect(Collectors.toSet()).size() == 1;
        return node.streamPlainNodes().filter(serviceGraph::containsNode)
                .map(serviceGraph::getNodeLayer).findFirst()
                .orElseThrow(IllegalArgumentException::new);
    }

    @Nonnull
    public Collection<SetNode> predecessors(@Nonnull SetNode node) {
        if (removed.contains(node)) return Collections.emptySet();

        Set<SetNode> set = backward.getOrDefault(node, null);
        if (set == null) {
            backward.put(node, set = computePredecessors(node));
            updateForwardAndLayers(node, set);
            checkReverseForward(node);
        }
        return set;
    }

    @Nonnull
    private Set<SetNode> computePredecessors(SetNode node) {
        Set<SetNodeEdge> edges = setNodeProvider.getPredecessors(node);
        assert checkExtraneousOnly(edges);

        final int layer = getLayer(node);
        if (layer > 0) {
            Map<SetNode, SetNode> self = new HashMap<>();
            layers.get(layer-1).forEach(n -> self.put(n, n));
            edges = edges.stream().map(e -> {
                assert getLayer(e.getFrom()) == layer-1;
                SetNode old = self.getOrDefault(e.getFrom(), null);
                if (old != null) {
                    assert old instanceof FullSetNode;
                    List<SetNode> alts = old.getAlternatives();
                    HashSet<SetNode> set = new HashSet<>(alts);
                    int merged = 0;
                    for (SetNode alt : e.getFrom().getAlternatives()) {
                        if (!set.contains(alt)) {
                            alts.add(alt);
                            ++merged;
                        }
                    }
                    if (merged > 0)
                        logger.debug("Merged {} new alternatives into old {}", merged, old);
                    return e.withFrom(old);
                }
                return e;
            }).collect(Collectors.toSet());
        } else {
            assert edges.isEmpty();
        }

        edges.forEach(e -> this.edges.put(of(e.getFrom(), e.getTo()), e));
        return edges.stream().map(SetNodeEdge::getFrom).collect(Collectors.toSet());
    }

    private boolean checkExtraneousOnly(Set<SetNodeEdge> edges) {
        List<SetNodeEdge> filtered = edges.stream().
                filter(e -> e.getFrom().streamPlainNodes().noneMatch(serviceGraph::containsNode))
                .collect(Collectors.toList());
        if (!filtered.isEmpty()) {
            logger.warn("Filtered {} extraneous-only predecessor nodes out of {}.",
                    filtered.size(), edges.size());
            edges.removeAll(filtered);
            return false;
        }
        return true;
    }

    private void updateForwardAndLayers(SetNode successor, Set<SetNode> predecessors) {
        Map<SetNode, Integer> nodeLayers = new HashMap<>();
        predecessors.forEach(p -> nodeLayers.put(p, getLayer(p)));
        Set<SetNode> unplaced = predecessors.stream().filter(
                p -> !layers.get(nodeLayers.get(p)).contains(p)).collect(Collectors.toSet());
        unplaced.forEach(n -> layers.get(nodeLayers.get(n)).add(n));
        unplaced.forEach(n -> listeners.forEach(l -> l.newNode(n)));

        List<SetNodeEdge> added = new ArrayList<>(predecessors.size());
        for (SetNode p : predecessors) added.add(edges.get(of(p, successor)));
        for (SetNodeEdge e : added) addForwardEdge(e);
        for (SetNodeEdge e : added) {
            NewSuccessors ns = new NewSuccessors(e.getFrom(), Collections.singleton(e.getTo()));
            listeners.forEach(l -> l.newSuccessors(ns));
        }
    }


    public SetNodeEdge edge(@Nonnull SetNode from, @Nonnull SetNode to) {
        return edges.get(of(from, to));
    }

    @Nonnull
    @Override
    public SetNode getSource(@Nonnull SetNodeEdge edge) {
        return edge.getFrom();
    }
    @Nonnull
    @Override
    public SetNode getTarget(@Nonnull SetNodeEdge edge) {
        return edge.getTo();
    }


    /**
     * The interpretation of edges with respect to actions is that the executor is currently
     * placed at edge.getFrom() and wishes to transition to edge.getTo(). Placed at a state implies
     * that all inputs of the SetNode are bound. Performing the transition ammounts to executing
     * all services in edge.getTo() and performing the edge input assignments, which will cause
     * edge.getTo() inputs to be bound.
     *
     * Use an edge validator to confirm that after execution of the actions, the transition can
     * still be made.
     *
     * @param edge An edge of this graph
     * @return Sequence of actions that are necessary for transitioning across the edge.
     */
    @Nonnull
    @Override
    public ExecutionPath<Action> getEdgeActions(@Nonnull SetNodeEdge edge) {
        if (edge.isNoOp()) return new ExecutionPath<>();
        ExecutionPath<Action> actions = new ExecutionPath<>();
        for (MessagePairAction pair : edge.getFrom().getMessagePairs()) {
            actions.add(new MessagePairAction(pair));
        }
        actions.add(edge.getInputAssignments());
        return actions;
    }

    private IndexedNode getCanonic(SetNode setNode, IndexedNode member) {
        SetNode o = setNode;
        IndexedNode canonic = member;
        while (o != null) {
            if (o instanceof AdaptedSetNode)
                canonic = ((AdaptedSetNode)o).getReplacements().getOrDefault(canonic, canonic);
            o = adaptedOriginal.getOrDefault(o, null);
        }
        return canonic;
    }

    private SetNode getCanonic(SetNode setNode) {
        SetNode canonic = null, o = setNode;
        do {
            assert o != canonic;
            canonic = o;
            o = adaptedOriginal.getOrDefault(canonic, null);
        } while (o != null);
        return canonic;
    }

    private SetNode getCurrent(SetNode canonic) {
        return canonicToCurrentAdapted.getOrDefault(canonic, canonic);
    }

    @Nonnull
    @Override
    public LayeredGraph.Validator<Action, SetNode>
    createValidator(@Nonnull SetNodeEdge edge, @Nonnull DataContext dataContext) {
        return new Validator(edge, dataContext);
    }

    private class Validator extends PathDiffValidator {
        private boolean withAlternatives = false;

        public Validator(@Nonnull SetNodeEdge edge, @Nonnull DataContext dataContext) {
            super(edge, getEdgeActions(edge), dataContext);
        }

        public Validator(Validator other) {
            super(other);
        }

        @Override
        public boolean adaptedWithAlternatives() {
            assert adapted;
            return withAlternatives;
        }

        @Override
        public boolean adaptGraph() {
            Stopwatch watch = Stopwatch.createStarted();
            assert isFailed();
            assert !adapted;
            adapted = true;

            List<SetNodeEdge> in = predecessors(edge.getFrom()).stream()
                    .map(p -> edges.get(of(p, edge.getFrom())))
                    .collect(Collectors.toList());
            List<SetNodeEdge> out = successors(edge.getFrom()).stream()
                    .map(s -> edges.get(of(edge.getFrom(), s)))
                    .filter(e -> !e.isNoOp())
                    .collect(Collectors.toList());
            Set<IndexedNode> failed = getFailedNodes();

            SetNode a = tryReplaceFailedNodes(edge.getFrom(), failed);
            if (a == null)
                a = tryReplaceSetNode(edge.getFrom(), failed);
            withAlternatives = a != null;
            if (a == null)
                a = new EmptySetNode(getLayer(edge.getFrom()));
            if (!adaptedOriginal.containsKey(edge.getFrom()))
                canonicIncoming.put(edge.getFrom(), in);
            adaptedOriginal.put(a, edge.getFrom());
            canonicToCurrentAdapted.put(getCanonic(edge.getFrom()), a);

            newStart = a;
            adaptationDiff = assignInputs(computeAdaptationDiff(failed), a, edge.getFrom());
            List<SetNodeEdge> inA = adaptEdges(a, edge.getFrom(), in);
            List<SetNodeEdge> outA = adaptEdges(a, edge.getFrom(), out);
            List<SetNodeEdge> back = fallbackEdges(a);
            int lost = in.size() - inA.size() + out.size() - outA.size();

            applyAdaptation(failed, a, inA, outA, back, lost);
            logger.info("Adapted state {} to {}. lost={} failed={} time={}",
                    edge.getFrom(), a, lost, failed, watch);
            return true;
        }

        private void applyAdaptation(Set<IndexedNode> failed, SetNode a, List<SetNodeEdge> inA,
                                     List<SetNodeEdge> outA, List<SetNodeEdge> back, int lost) {
            removeNode(edge.getFrom());
            backward.put(a, inA.stream().map(SetNodeEdge::getFrom).collect(Collectors.toSet()));
            inA.forEach(LazyLayeredServiceSetGraph.this::addEdge);
            outA.forEach(LazyLayeredServiceSetGraph.this::addEdge);
            for (SetNode n : back.stream().map(SetNodeEdge::getTo).collect(Collectors.toSet())) {
                Set<SetNode> set = backward.getOrDefault(n, null);
                if (set != null) set.add(a);
            }
            back.forEach(LazyLayeredServiceSetGraph.this::addForwardEdge);

            listeners.forEach(l -> l.newNode(a));
            inA.stream().map(SetNodeEdge::getFrom)
                    .map(from -> new NewSuccessors(from, Collections.singleton(a)))
                    .forEach(ns -> listeners.forEach(l -> l.newSuccessors(ns)));
            NewSuccessors ns = new NewSuccessors(a, Stream.concat(outA.stream(), back.stream())
                    .map(SetNodeEdge::getTo).collect(Collectors.toSet()));
            listeners.forEach(l -> l.newSuccessors(ns));


        }

        private AdaptedSetNode tryReplaceFailedNodes(SetNode current, Set<IndexedNode> failed) {
            assert edge.getFrom().getNodes().containsAll(failed);
            if (failed.stream().anyMatch(n -> n.getAlternatives().isEmpty()))
                return null;
            AdaptedSetNode.Builder builder;
            builder = new AdaptedSetNode.Builder(current, n -> getCanonic(current, n));
            failed.forEach(n -> builder.replace(n, n.getIndexedAlternatives().get(0)));
            return builder.build();
        }

        private AdaptedSetNode tryReplaceSetNode(SetNode current, Set<IndexedNode> failed) {
            Set<Node> allFailed = new HashSet<>(failed);
            SetNode o = current;
            while (o instanceof AdaptedSetNode) {
                allFailed.addAll(((AdaptedSetNode) o).getReplacements().keySet());
                o = adaptedOriginal.getOrDefault(o, null);
            }

            for (SetNode alt : current.getAlternatives()) {
                if (alt.getNodes().stream().anyMatch(allFailed::contains))
                    continue;
                return new AdaptedSetNode.Builder(alt, n -> getCanonic(alt, n)).build();
            }
            return null;
        }

        @Nonnull
        private DataContextDiff computeAdaptationDiff(Set<IndexedNode> failedNodes) {
            Set<Variable> blacklist = failedNodes.stream().map(Node::getOutputs)
                    .flatMap(Set::stream).collect(Collectors.toSet());
            List<DataContextAction> actions = diff.getActions().stream().filter(a ->
                    a.changedVariables().stream().noneMatch(blacklist::contains)
            ).collect(Collectors.toList());
            return new DataContextDiff(actions);
        }

        private DataContextDiff assignInputs(@Nonnull DataContextDiff diff,
                                             @Nonnull SetNode adapted,
                                             @Nonnull SetNode original) {
            if (adapted instanceof EmptySetNode) return diff;
            AdaptedSetNode a = (AdaptedSetNode) adapted;

            RulesComponentRegistry componentRegistry = DefaultComponentRegistry.getInstance();
            UnservedPInterpreter interpreter = new UnservedPInterpreter(componentRegistry);
            Model model = ModelFactory.createDefaultModel();
            Resource copy = model.createResource().addProperty(RDF.type, UnservedP.Copy)
                    .addProperty(RDF.type, UnservedP.Action);
            CopyRunner copyRunner = new CopyRunner();

            try (DiffingDataContext data = new DiffingDataContext(dataContext);
                 SimpleContext context = new SimpleContext(data, componentRegistry, interpreter)) {
                diff.apply(data);
                for (Node replacement : a.getReplacements().values()) {
                    for (Variable rInput : replacement.getInputs()) {
                        Variable cInput = a.getToCanonic().get(rInput);
                        Variable oInput = getInputFromCanonic(cInput, original);
                        Preconditions.checkNotNull(oInput, "This validator assumes that all " +
                                "inputs of an alternative Node are assignable from a input of " +
                                "the canonic Node.");

                        if (!oInput.equals(cInput)) {
                            copy.removeAll(copyFrom).addProperty(copyFrom, oInput)
                                    .removeAll(copyTo).addProperty(copyTo, cInput);
                            copyRunner.run(copy, context);
                        }
                    }
                }

                return data.getDiff();
            } catch (ActionExecutionException e) {
                throw new RuntimeException(e); //unexpected exception
            }
        }

        @Nullable
        private Variable getInputFromCanonic(Variable canonic, SetNode setNode) {
            Set<Variable> inputs = setNode.getInputs();

            SetNode parent = setNode;
            while (parent instanceof AdaptedSetNode) {
                AdaptedSetNode adapted = (AdaptedSetNode) parent;
                Variable maybe = adapted.getFromCanonic().get(canonic).stream()
                        .filter(inputs::contains).findFirst().orElse(null);
                if (maybe != null) return maybe;
                parent = adaptedOriginal.get(parent);
            }

            return canonic;
        }

        private List<SetNodeEdge> fallbackEdges(SetNode adapted) {
            SetNode canonic = adapted;
            while (canonic instanceof AdaptedSetNode)
                canonic = adaptedOriginal.get(canonic);
            return canonicIncoming.getOrDefault(canonic, Collections.emptySet()).stream()
                    .map(e -> SetNodeEdge.createNoOp(adapted, getCurrent(e.getFrom())))
                    .collect(Collectors.toList());
        }

        private List<SetNodeEdge> adaptEdges(SetNode adapted, SetNode original,
                                             Collection<SetNodeEdge> edges) {
            if (adapted instanceof EmptySetNode) return Collections.emptyList();
            AdaptedSetNode a = (AdaptedSetNode) adapted;
            Set<IndexedNode> failed = a.getReplacements().values();
            Set<IndexedNode> notFailed = a.getNodes().stream().filter(n -> !failed.contains(n))
                    .collect(Collectors.toSet());
            Set<Variable> trivialInputs = notFailed.stream()
                    .flatMap(n -> n.getInputs().stream()).collect(Collectors.toSet());
            Set<Variable> trivialOutputs = notFailed.stream()
                    .flatMap(n -> n.getOutputs().stream()).collect(Collectors.toSet());

            List<SetNodeEdge> result = new ArrayList<>(edges.size());
            for (SetNodeEdge e : edges) {
                SetNode to = e.getTo();
                SetNode from = e.getFrom();
                if (from.equals(original)) {
                    Copies copies = new Copies();
                    e.getInputAssignments().asList().stream()
                            .filter(c -> trivialOutputs.contains(c.getFrom()))
                            .forEach(c -> copies.add(c.getFrom(), c.getTo()));
                    if (assignReplacementOutputs(failed, e, copies))
                        result.add(new SetNodeEdge(adapted, to, copies));
                } else if (to.equals(original)) {
                    Copies assignments = new Copies();
                    e.getInputAssignments().asList().stream()
                            .filter(c -> trivialInputs.contains(c.getTo()))
                            .forEach(c -> assignments.add(c.getFrom(), c.getTo()));
                    if (assignReplacementInputs(failed, e, assignments))
                        result.add(new SetNodeEdge(from, adapted, assignments));
                }
            }

            return result;
        }

        private boolean assignReplacementOutputs(Set<IndexedNode> failed, SetNodeEdge edge,
                                                 Copies assignments) {
            Copies temp = new Copies();
            Set<Variable> assigned = assignments.asList().stream().map(Copy::getTo)
                    .collect(Collectors.toSet());
            Set<Variable> pending = edge.getTo().getInputs().stream()
                    .filter(in -> !assigned.contains(in)).collect(Collectors.toSet());

            Map<VariableSpec, Variable> helper = new HashMap<>();
            failed.stream().flatMap(f -> f.getOutputs().stream())
                    .forEach(o -> helper.put(new VariableSpec(o), o));

            for (Variable in : pending) {
                Variable out = getMostSimilar(helper.values(), in);
                if (out == null) return false;
                temp.add(out, in);
            }

            assignments.addAll(temp);
            return true;
        }

        private boolean assignReplacementInputs(Set<IndexedNode> failed, SetNodeEdge edge,
                                                Copies assignments) {
            Copies temp = new Copies();
            Map<VariableSpec, Variable> helper = new HashMap<>();
            edge.getFrom().getNodes().stream().flatMap(n -> n.getOutputs().stream())
                    .forEach(o -> helper.put(new VariableSpec(o), o));

            Iterator<Variable> it = failed.stream().flatMap(n -> n.getInputs().stream()).iterator();
            while (it.hasNext()) {
                Variable in = it.next();
                Variable out = getMostSimilar(helper.values(), in);
                if (out == null) return false;
                temp.add(out, in);
            }
            assignments.addAll(temp);
            return true;
        }

        private Variable getMostSimilar(Collection<Variable> vars, Variable target) {
            return new MinimalSemanticDistance()
                    .withPreferences(MatchTable.Relation.SAME, MatchTable.Relation.SUBCLASS)
                    .select(vars, target);
        }
    }

    @Override
    public Duplicator createWriteIsolatedDuplicator() {
        //TODO this implementation is expensive, replace with something better
        LayeredServiceGraph.Duplicator sgDuplicator;
        sgDuplicator = this.serviceGraph.createWriteIsolatedDuplicator();
        LayeredServiceGraph serviceGraph = sgDuplicator.getDuplicate();

        Map<ImmutablePair<SetNode, SetNode>, SetNodeEdge> edges = new HashMap<>();
        for (Map.Entry<ImmutablePair<SetNode, SetNode>, SetNodeEdge> e : this.edges.entrySet())
            edges.put(e.getKey(), e.getValue());

        List<Set<SetNode>> layers = this.layers.stream().map(HashSet::new)
                .collect(Collectors.toList());
        Set<SetNode> removed = new HashSet<>(this.removed);

        Map<SetNode, SetNode> adaptedOriginal = new HashMap<>();
        for (Map.Entry<SetNode, SetNode> e : this.adaptedOriginal.entrySet())
            adaptedOriginal.put(e.getKey(), e.getValue());
        Map<SetNode, SetNode> canonicToCurrentAdapted = new HashMap<>();
        for (Map.Entry<SetNode, SetNode> e : canonicToCurrentAdapted.entrySet())
            canonicToCurrentAdapted.put(e.getKey(), e.getValue());
        Map<SetNode, Collection<SetNodeEdge>> canonicIncoming = new HashMap<>();
        for (Map.Entry<SetNode, Collection<SetNodeEdge>> e : this.canonicIncoming.entrySet())
            canonicIncoming.put(e.getKey(), new ArrayList<>(e.getValue()));


        LazyLayeredServiceSetGraph dup = new LazyLayeredServiceSetGraph(
                serviceGraph, startNode, endNode, setNodeProvider, layers, removed,
                duplicateAdjacencyMap(this.forward),
                duplicateAdjacencyMap(this.backward),
                duplicateAdjacencyMap(this.reverseForward), edges, adaptedOriginal,
                canonicToCurrentAdapted, canonicIncoming);
        return new Duplicator(dup, sgDuplicator);
    }

    public class Duplicator implements LayeredGraph.Duplicator<Action, SetNodeEdge, SetNode> {
        private final @Nonnull LazyLayeredServiceSetGraph dup;
        private final @Nonnull LayeredServiceGraph.Duplicator serviceGraphDuplicator;

        public Duplicator(@Nonnull LazyLayeredServiceSetGraph dup,
                          @Nonnull LayeredServiceGraph.Duplicator serviceGraphDuplicator) {
            this.dup = dup;
            this.serviceGraphDuplicator = serviceGraphDuplicator;
        }

        @Nonnull
        @Override
        public LazyLayeredServiceSetGraph getDuplicate() {
            return dup;
        }

        @Nonnull
        @Override
        public LayeredGraph.Validator<Action, SetNode> translateValidator(
                @Nonnull LayeredGraph.Validator<Action, SetNode> validator) {
            assert validator instanceof Validator;
            return dup.duplicateValidator((Validator)validator);
        }
    }

    private Validator duplicateValidator(@Nonnull Validator validator) {
        return new Validator(validator);
    }

    @Nonnull
    private Map<SetNode, Set<SetNode>>
    duplicateAdjacencyMap(@Nonnull Map<SetNode, Set<SetNode>> map) {
        Map<SetNode, Set<SetNode>> dup = new HashMap<>();
        for (Map.Entry<SetNode, Set<SetNode>> e : map.entrySet())
            dup.put(e.getKey(), new HashSet<>(e.getValue()));
        return dup;
    }

    public class NewSuccessors {
        private final @Nonnull SetNode predecessor;
        private @Nonnull Set<SetNode> successors;

        NewSuccessors(@Nonnull SetNode predecessor, @Nonnull Set<SetNode> successors) {
            this.predecessor = predecessor;
            this.successors = successors;
        }

        @Nonnull
        public SetNode getPredecessor() {
            return predecessor;
        }

        /**
         * Computes the new successors of getPredecessor(), existing successors are not
         * affected. Actual computation only takes place on the first call.
         *
         * @return Set with only the new successors.
         */
        @Nonnull
        public Set<SetNode> getNew() {
            return successors;
        }
    }

    public class RemovedSuccessors {
        private final @Nonnull SetNode predecessor;
        private final Set<SetNode> removed;

        public RemovedSuccessors(@Nonnull SetNode predecessor, Set<SetNode> removed) {
            this.predecessor = predecessor;
            this.removed = removed;
        }

        @Nonnull
        public SetNode getPredecessor() {
            return predecessor;
        }

        /**
         * Gets which nodes were removed from the successor list of predecessor.
         */
        @Nonnull
        public Set<SetNode> getRemoved() {
            return removed;
        }
    }

    public interface Listener {
        void newSuccessors(@Nonnull NewSuccessors successors);
        void removedSuccessors(@Nonnull RemovedSuccessors removed);
        void removedNode(@Nonnull SetNode node);
        void newNode(@Nonnull SetNode node);
    }
}

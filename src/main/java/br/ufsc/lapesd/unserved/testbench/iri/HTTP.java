package br.ufsc.lapesd.unserved.testbench.iri;

import br.ufsc.lapesd.unserved.testbench.util.ResourcesBackground;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.rdf.model.*;
import org.apache.jena.riot.RDFFormat;

import javax.annotation.Nullable;
import java.util.Collections;

/**
 * Constants for http://www.w3.org/2011/http
 */
public class HTTP extends ResourcesBackground {
    public HTTP() {
        super(Collections.singletonList(ImmutablePair.of("http/http", RDFFormat.RDFXML)), true);
    }

    private static HTTP instance = new HTTP();
    public static HTTP getInstance() {
        return instance;
    }

    public static final String IRI = "http://www.w3.org/2011/http";
    public static final String PREFIX = IRI + "#";
    public static final String PREFIX_SHORT = "http";

    public static final Resource Connection = ResourceFactory.createResource(PREFIX + "Connection");
    public static final Resource HeaderElement = ResourceFactory.createResource(PREFIX + "HeaderElement");
    public static final Resource HeaderName = ResourceFactory.createResource(PREFIX + "HeaderName");
    public static final Resource Message = ResourceFactory.createResource(PREFIX + "Message");
    public static final Resource Request = ResourceFactory.createResource(PREFIX + "Request");
    public static final Resource Response = ResourceFactory.createResource(PREFIX + "Response");
    public static final Resource MessageHeader = ResourceFactory.createResource(PREFIX + "MessageHeader");
    public static final Resource EntityHeader = ResourceFactory.createResource(PREFIX + "EntityHeader");
    public static final Resource GeneralHeader = ResourceFactory.createResource(PREFIX + "GeneralHeader");
    public static final Resource RequestHeader = ResourceFactory.createResource(PREFIX + "RequestHeader");
    public static final Resource ResponseHeader = ResourceFactory.createResource(PREFIX + "ResponseHeader");
    public static final Resource Method = ResourceFactory.createResource(PREFIX + "Method");
    public static final Resource Parameter = ResourceFactory.createResource(PREFIX + "Parameter");
    public static final Resource StatusCode = ResourceFactory.createResource(PREFIX + "StatusCode");

    public static final Property body = ResourceFactory.createProperty(PREFIX + "body");
    public static final Property connectionAuthority = ResourceFactory.createProperty(PREFIX + "connectionAuthority");
    public static final Property elementName = ResourceFactory.createProperty(PREFIX + "elementName");
    public static final Property elementValue = ResourceFactory.createProperty(PREFIX + "elementValue");
    public static final Property fieldName = ResourceFactory.createProperty(PREFIX + "fieldName");
    public static final Property fieldValue = ResourceFactory.createProperty(PREFIX + "fieldValue");
    public static final Property hdrName = ResourceFactory.createProperty(PREFIX + "hdrName");
    public static final Property headerElements = ResourceFactory.createProperty(PREFIX + "headerElements");
    public static final Property headers = ResourceFactory.createProperty(PREFIX + "headers");
    public static final Property httpVersion = ResourceFactory.createProperty(PREFIX + "httpVersion");
    public static final Property methodName = ResourceFactory.createProperty(PREFIX + "methodName");
    public static final Property mthd = ResourceFactory.createProperty(PREFIX + "mthd");
    public static final Property paramName = ResourceFactory.createProperty(PREFIX + "paramName");
    public static final Property params = ResourceFactory.createProperty(PREFIX + "params");
    public static final Property paramValue = ResourceFactory.createProperty(PREFIX + "paramValue");
    public static final Property reasonPhrase = ResourceFactory.createProperty(PREFIX + "reasonPhrase");
    public static final Property requests = ResourceFactory.createProperty(PREFIX + "requests");
    public static final Property requestURI = ResourceFactory.createProperty(PREFIX + "requestURI");
    public static final Property absolutePath = ResourceFactory.createProperty(PREFIX + "absolutePath");
    public static final Property absoluteURI = ResourceFactory.createProperty(PREFIX + "absoluteURI");
    public static final Property authority = ResourceFactory.createProperty(PREFIX + "authority");
    public static final Property resp = ResourceFactory.createProperty(PREFIX + "resp");
    public static final Property sc = ResourceFactory.createProperty(PREFIX + "sc");
    public static final Property statusCodeNumber = ResourceFactory.createProperty(PREFIX + "statusCodeNumber");
    public static final Property statusCodeValue = ResourceFactory.createProperty(PREFIX + "statusCodeValue");

    public static class Methods extends ResourcesBackground {
        public Methods() {
            super(Collections.singletonList(ImmutablePair.of("http/http-methods",
                    RDFFormat.RDFXML)), false);
        }

        private static Methods instance = null;
        public static Methods getInstance() {
            if (instance == null) instance = new Methods();
            return instance;
        }

        public static final String IRI = "http://www.w3.org/2011/http-methods";
        public static final String PREFIX = IRI + "#";
        public static final String PREFIX_SHORT = "http-methods";

        public static final Resource CONNECT = getInstance().getModel().createResource(PREFIX + "CONNECT");
        public static final Resource DELETE = getInstance().getModel().createResource(PREFIX + "DELETE");
        public static final Resource GET = getInstance().getModel().createResource(PREFIX + "GET");
        public static final Resource HEAD = getInstance().getModel().createResource(PREFIX + "HEAD");
        public static final Resource OPTIONS = getInstance().getModel().createResource(PREFIX + "OPTIONS");
        public static final Resource PATCH = getInstance().getModel().createResource(PREFIX + "PATCH");
        public static final Resource POST = getInstance().getModel().createResource(PREFIX + "POST");
        public static final Resource PUT = getInstance().getModel().createResource(PREFIX + "PUT");
        public static final Resource TRACE = getInstance().getModel().createResource(PREFIX + "TRACE");

        public static @Nullable Resource getMethod(String methodName) {
            Resource r = ResourceFactory.createResource(PREFIX + methodName.toUpperCase());
            return !getInstance().getModel().containsResource(r) ? null
                    : getInstance().getModel().createResource(r.getURI());
        }
    }

    public static class StatusCodes extends ResourcesBackground {
        public StatusCodes() {
            super(Collections.singletonList(ImmutablePair.of("http/http-statusCodes",
                    RDFFormat.RDFXML)), false);
        }

        private static StatusCodes instance = null;
        public static StatusCodes getInstance() {
            if (instance == null) instance = new StatusCodes();
            return instance;
        }

        public static final String IRI = "http://www.w3.org/2011/http-statusCodes";
        public static final String PREFIX = IRI + "#";
        public static final String PREFIX_SHORT = "http-statusCodes";

        public static Resource fromCode(int code) {
            StmtIterator it = getInstance().getModel().listStatements(null, HTTP.statusCodeNumber,
                    ResourceFactory.createTypedLiteral(String.valueOf(code), XSDDatatype.XSDint));
            return it.hasNext() ? it.next().getSubject() : null;
        }
    }

    public static class Headers extends ResourcesBackground {
        public Headers() {
            super(Collections.singletonList(ImmutablePair.of("http/http-headers",
                    RDFFormat.RDFXML)), false);
        }

        private static Headers instance = new Headers();
        public static Model sharedModel() {
            return instance.getModel();
        }

        public static final String IRI = "http://www.w3.org/2011/http-headers";
        public static final String PREFIX = IRI + "#";
        public static final String PREFIX_SHORT = "http-headers";
    }
}

package br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.cost;

import javax.annotation.Nonnull;

public interface CostParameter {
    @Nonnull String getName();
    CostParameter aggregateSequence(CostParameter right);
    CostParameter aggregateParallel(CostParameter right);
    int compare(CostParameter right);
}

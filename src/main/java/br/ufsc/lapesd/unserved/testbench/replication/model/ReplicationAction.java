package br.ufsc.lapesd.unserved.testbench.replication.model;

import br.ufsc.lapesd.unserved.testbench.process.model.Action;

public interface ReplicationAction extends Action {
    Action getWrappedAction();
    String getContainerId();
}

package br.ufsc.lapesd.unserved.testbench.io_composer.graph;

/**
 * Basic graph access interface
 */
public interface GraphAccessor<State, Cost, Transition> extends AutoCloseable {
    State getInitial();
    boolean isGoal(State state);
    Iterable<Transition> getTransitions(State state);
    Cost getCost(Transition transition);
    @Override
    void close();
}

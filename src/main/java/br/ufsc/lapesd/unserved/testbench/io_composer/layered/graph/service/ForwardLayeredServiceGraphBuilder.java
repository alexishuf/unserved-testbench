package br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service;

import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.And;
import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.atomic.VariableSpec;
import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.parsers.SimpleSPINConditionsParser;
import br.ufsc.lapesd.unserved.testbench.io_composer.impl.DefaultIOComposerTimes;
import br.ufsc.lapesd.unserved.testbench.io_composer.impl.IOComposerInput;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.LayeredServiceSetGraphIOComposer;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.discovery.IOLayersState;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.discovery.LayersState;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.discovery.NodesSupplier;
import br.ufsc.lapesd.unserved.testbench.iri.Unserved;
import br.ufsc.lapesd.unserved.testbench.model.Message;
import br.ufsc.lapesd.unserved.testbench.model.Variable;
import br.ufsc.lapesd.unserved.testbench.util.BFSBGPIterator;
import com.google.common.base.Preconditions;
import com.google.common.base.Stopwatch;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ResIterator;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.stream.Collectors;

import static br.ufsc.lapesd.unserved.testbench.io_composer.layered.LayeredServiceSetGraphIOComposer.graphBuilderSetupTime;

/**
 * Builder for Forward, layered graphs as described in section 4.1 of doi:10.4018/jwsr.2012040101,
 * the actual construction of these graphs is done by subclasses that implement the abstract
 * methods.
 *
 * The builder is not expected to be used concurrently, as implementing subclasses may keep state.
 */
public class ForwardLayeredServiceGraphBuilder {
    private boolean busy = false;
    private ProviderSetsSupplier providerSetsSupplier = null;
    private NodesSupplier<? extends LayersState> nodesSupplier = null;
    private boolean stopEarly = true;
    private boolean spinConditions = false;
    private List<ServiceLayer> layers;
    private IOLayersState layersState = null;
    private HashSet<Node> all;
    private HashSet<Message> allConsequents;
    private Map<Node, Integer> nodeLayer;
    private Map<Node, ProviderSet> pSets;
    private Function<IOComposerInput, IOLayersState> layersStateFactory;
    private long delegatesSetupNanos = -1;

    public ForwardLayeredServiceGraphBuilder
    withLayersStateFactory(@Nonnull Function<IOComposerInput, IOLayersState> factory) {
        this.layersStateFactory = factory;
        return this;
    }

    public ForwardLayeredServiceGraphBuilder stopEarly(boolean stopEarly) {
        this.stopEarly = stopEarly;
        return this;
    }

    public ForwardLayeredServiceGraphBuilder withSpinConditions(boolean enabled) {
        this.spinConditions = enabled;
        return this;
    }

    public long getDelegatesSetupNanos() {
        return delegatesSetupNanos;
    }

    public LayeredServiceGraph build(IOComposerInput ci, DefaultIOComposerTimes times) {
        try {
            Preconditions.checkState(!busy,
                    "Concurrent ForwardLayeredServiceGraphBuilder usage detected");
            busy = true;

            layers = new ArrayList<>();
            Stopwatch w = Stopwatch.createStarted();
            layersState = layersStateFactory.apply(ci);
            spinConditions = layersState.supportsSPINConditions();
            delegatesSetupNanos = w.elapsed(TimeUnit.NANOSECONDS);

            EndNode end = new EndNode(ci.getWantedAsVariables(), getWantedConditions(ci));
            Set<Variable> wanted = new HashSet<>(ci.getWantedAsVariables());
            Multimap<Variable, VariableSpec> sourceSpecs = initSourceSpecs(wanted);
            StartNode start = new StartNode(ci.getKnownAsVariables(),
                    getConditions(ci.getKnownAsVariables()));

            w.reset().start();
            providerSetsSupplier = layersState.createProviderSetsSupplier();
            providerSetsSupplier.begin(ci, start, end);
            nodesSupplier = layersState.createNodesSupplier();
            nodesSupplier.begin(ci, start, end);
            delegatesSetupNanos += w.elapsed(TimeUnit.NANOSECONDS);
            times.addTime(graphBuilderSetupTime, delegatesSetupNanos, TimeUnit.NANOSECONDS);
//            new File("/tmp/unserved-layers").delete();

            all = new HashSet<>(Collections.singleton(start));
            nodeLayer = new HashMap<>();
            pSets = new HashMap<>();

            ServiceLayer l1 = new ServiceLayer(Collections.singleton(start));
            layersState.advance(l1.getNodes());
            updateWanted(wanted, sourceSpecs, start.getOutputs().stream()
                    .map(VariableSpec::new).collect(Collectors.toSet()));
            providerSetsSupplier.update(l1.getNodes());
            layers.add(l1);
            nodeLayer.put(start, 0);
            IOProviderHashSet startProviderSet = new IOProviderHashSet();
            start.getInputs().forEach(startProviderSet::addTarget);
            pSets.put(start, startProviderSet);

            allConsequents = new HashSet<>();
//            for (Node node : all) allConsequents.add(node.getId());

            ServiceLayer l2 = createNextLayer();
            updateWanted(wanted, sourceSpecs, layersState.getNewlyKnown());

            while (l2 != null && (!stopEarly || !wanted.isEmpty())) {
//                debugLayer(layers.size()-1, l2.getNodes(), layersState.getNewlyKnown());
                l2 = createNextLayer();
                updateWanted(wanted, sourceSpecs, layersState.getNewlyKnown());
            }
//            if (l2 != null) debugLayer(layers.size()-1, l2.getNodes(), newlyKnown);
            l2 = setupNextLayer(Collections.singleton(end));

            LayeredServiceGraph sg = new LayeredServiceGraph(layers, pSets, start, end, nodeLayer);
            sg.setTimes(times);
            times.takeMemSample(LayeredServiceSetGraphIOComposer.graphConstructionMem);
            return sg;
        } finally {
            busy = false;
            if (providerSetsSupplier != null) {
                providerSetsSupplier.end();
                providerSetsSupplier = null;
            }
            if (nodesSupplier != null) {
                nodesSupplier.end();
                nodesSupplier = null;
            }
            nodeLayer = null;
            layersState = null;
            layers = null;
            layersState = null;
            pSets = null;
        }
    }

    private And getConditions(@Nonnull Set<Variable> variables) {
        if (!spinConditions) return And.empty();
        return new SimpleSPINConditionsParser().parseForVariables(variables);
    }

    private And getWantedConditions(@Nonnull IOComposerInput ci) {
        if (!spinConditions) return And.empty();
        SimpleSPINConditionsParser sp = new SimpleSPINConditionsParser();
        Model u = ci.getSkolemizedUnion();
        And all = And.EMPTY;
        for (ResIterator it = u.listSubjectsWithProperty(RDF.type, Unserved.Wanted); it.hasNext();){
            Resource r = it.next();
            if (r.hasProperty(RDF.type, Unserved.Variable)) continue;
            all = And.add(all, sp.parse(r));
        }
        return all;
    }

//    private void debugLayer(int layer, List<Node> nodes, Set<Variable> newlyKnown) {
//        Preconditions.checkArgument(nodes.stream().allMatch(n -> n instanceof ServiceNode));
//        try (FileOutputStream fileOut = new FileOutputStream("/tmp/unserved-layers", true);
//             PrintStream out = new PrintStream(fileOut)) {
//            out.printf("layer: %d\nservices: %s\nnewlyKnown: %s\n\n", layer,
//                    nodes.stream().map(n -> ((ServiceNode)n).getAntecedent()
//                            .getProperty(Wscc.wscService).getLiteral().getLexicalForm()
//                            .split("#")[1]
//                    ).sorted().reduce((l, r) -> l + ", " + r).orElse("<empty>"),
//                    newlyKnown.stream().map(v -> v.getType().toString().split("#")[1])
//                            .filter(s -> s.startsWith("con")).sorted().distinct()
//                            .reduce((l, r) -> l + ", " + r).orElse("")
//            );
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }

    private Multimap<Variable, VariableSpec> initSourceSpecs(Set<Variable> variables) {
        Multimap<Variable, VariableSpec> map = HashMultimap.create();
        for (Variable v : variables) {
            Resource representation = v.asSpec().getRepresentation();
            BFSBGPIterator.from(v.getType()).withInitial().backward(RDFS.subClassOf)
                    .forEachRemaining(type -> map.put(v, new VariableSpec(type, representation)));
        }
        return map;
    }

    @SuppressWarnings("UnusedReturnValue")
    private boolean updateWanted(Set<Variable> wanted, Multimap<Variable, VariableSpec> sourceSpecs,
                                 Set<VariableSpec> newlyKnown) {
        if (!stopEarly) return false;
        for (VariableSpec spec : newlyKnown)
            wanted.removeIf(w -> sourceSpecs.get(w).contains(spec));
        return wanted.isEmpty();
    }

    @Nullable
    private ServiceLayer createNextLayer() {
        Set<Node> nodes;
        //noinspection unchecked
        nodes = ((NodesSupplier<IOLayersState>)nodesSupplier).getNodes(layersState, allConsequents);
        if (nodes.isEmpty())
            return null;
//        setupLayerWithPrevious(layers, known, newlyKnown, all, nodeLayer,
//                providerSets, lastLayer, l);
        return setupNextLayer(nodes);
    }

//    private void setupLayerWithPrevious(List<ServiceLayer> layers, Set<Variable> known,
//                                        Set<Variable> newlyKnown,
//                                        Set<Node> allNodes, Map<Node, Integer> nodeLayer,
//                                        Map<Node, Collection<ProviderSet>> providerSets,
//                                        ServiceLayer previousLayer, ServiceLayer layer) {
//        setupLayerWithNodes(layers, known, newlyKnown, allNodes, nodeLayer,
//                providerSets, previousLayer.getNodes(), layer);
//    }

    private ServiceLayer setupNextLayer(@Nonnull Set<Node> nodes) {
        Preconditions.checkState(!layers.isEmpty());

        //get provider sets of all nodes
        Map<Node, ProviderSet> map = providerSetsSupplier.get(nodes);
//        List<Node> badNodes = new ArrayList<>(map.size());
//        for (Map.Entry<Node, ProviderSet> e : map.entrySet()) {
//            if (!e.getValue().isSatisfied()) badNodes.add(e.getKey());
//        }
        /*if conditions are enabled, some nodes may fail to find condition providers */
        assert map.entrySet().stream().allMatch(e -> e.getValue().isSatisfied());
//        for (Node badNode : badNodes) map.remove(badNode);
        providerSetsSupplier.update(map.keySet());

        /* build the layer*/
        ServiceLayer layer = new ServiceLayer(map.keySet());
        layers.add(layer);
        for (Node node : layer.getNodes()) nodeLayer.put(node, layers.size()-1);
        layer.getNodes().stream().filter(n -> n instanceof ServiceNode)
                .forEach(n -> allConsequents.add(((ServiceNode)n).getConsequent()));
        all.addAll(layer.getNodes());

        /* store the provider sets*/
        pSets.putAll(map);

        return layer;
    }
}

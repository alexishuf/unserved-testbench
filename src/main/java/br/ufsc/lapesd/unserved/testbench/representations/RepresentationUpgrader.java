package br.ufsc.lapesd.unserved.testbench.representations;

import org.apache.jena.rdf.model.Resource;

public interface RepresentationUpgrader {
    boolean upgradeable(Resource representation);
    void upgrade(Resource representation);
}

package br.ufsc.lapesd.unserved.testbench.io_composer.conditions.parsers;

import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.And;
import org.apache.jena.rdf.model.Resource;

import javax.annotation.Nonnull;

public class EmptyConditionsParser implements ConditionsParser {
    @Nonnull
    @Override
    public And parse(@Nonnull Resource resource) throws ConditionParsingException {
        return And.EMPTY;
    }
}

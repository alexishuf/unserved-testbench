package br.ufsc.lapesd.unserved.testbench.restdesc_pragproof.impl;

import br.ufsc.lapesd.unserved.testbench.restdesc_pragproof.n3.N3Model;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;

public class RuleApplication {
    private final N3Model proofModel;
    private final Resource ruleFormula;
    private final Model consequence;

    public RuleApplication(N3Model proofModel, Resource ruleFormula, Model consequence) {
        this.proofModel = proofModel;
        this.ruleFormula = ruleFormula;
        this.consequence = consequence;
    }

    public N3Model getProofModel() {
        return proofModel;
    }

    public Resource getRuleFormula() {
        return ruleFormula;
    }

    public Model getConsequence() {
        return consequence;
    }
}

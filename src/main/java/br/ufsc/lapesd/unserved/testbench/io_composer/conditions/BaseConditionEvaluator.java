package br.ufsc.lapesd.unserved.testbench.io_composer.conditions;

import javax.annotation.Nonnull;
import java.util.HashMap;
import java.util.function.Function;

public class BaseConditionEvaluator implements ConditionEvaluator {
    private HashMap<Class<? extends Condition>, Function<? extends Condition, Boolean>> delegates;

    public BaseConditionEvaluator() {
        delegates = new HashMap<>();
        delegates.put(And.class, new AndEvaluator());
        delegates.put(Or.class, new OrEvaluator());
    }

    public @Nonnull <T extends Condition>
    BaseConditionEvaluator add(@Nonnull Class<T> aClass,  @Nonnull Function<T, Boolean> evaluator) {
        delegates.put(aClass, evaluator);
        return this;
    }

    @Override
    public <T extends Condition> boolean evaluate(@Nonnull T condition) throws IllegalArgumentException {
        Class<? extends Condition> cl = condition.getClass();
        if (!delegates.containsKey(cl))
            throw new IllegalArgumentException("No delegate for condition of type " + cl.getName());
        //noinspection unchecked
        return ((Function<Condition, Boolean>) delegates.get(cl)).apply(condition);
    }

    @Override
    public <T extends Condition> boolean canEvaluate(@Nonnull T condition) {
        return delegates.containsKey(condition.getClass());
    }

    private class AndEvaluator implements Function<And, Boolean> {
        @Override
        public Boolean apply(And and) {
            return and.getMembers().stream().allMatch(BaseConditionEvaluator.this::evaluate);
        }
    }

    private class OrEvaluator implements Function<Or, Boolean> {
        @Override
        public Boolean apply(Or or) {
            return or.getMembers().stream().anyMatch(BaseConditionEvaluator.this::evaluate);
        }
    }

}

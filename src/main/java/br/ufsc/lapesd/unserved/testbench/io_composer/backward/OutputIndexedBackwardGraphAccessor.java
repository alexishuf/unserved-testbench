package br.ufsc.lapesd.unserved.testbench.io_composer.backward;

import br.ufsc.lapesd.unserved.testbench.io_composer.graph.ReplicatedGraphAccessor;
import br.ufsc.lapesd.unserved.testbench.io_composer.impl.IOComposerInput;
import br.ufsc.lapesd.unserved.testbench.io_composer.state.MessagePairAction;
import br.ufsc.lapesd.unserved.testbench.io_composer.state.State;
import br.ufsc.lapesd.unserved.testbench.iri.Unserved;
import br.ufsc.lapesd.unserved.testbench.model.Message;
import br.ufsc.lapesd.unserved.testbench.model.Variable;
import br.ufsc.lapesd.unserved.testbench.util.BFSBGPIterator;
import br.ufsc.lapesd.unserved.testbench.util.ModelStreams;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import es.usc.citius.hipster.model.Transition;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;

import javax.annotation.Nullable;
import java.util.Set;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * A {@link BackwardGraphAccessor} over a {@link State} graph that uses a index from output
 * types to messages in order to speed up <code>getTransitions</code>.
 */
public class OutputIndexedBackwardGraphAccessor extends AbstractBackwardGraphAccessor
        implements BackwardReplicatedGraphAccessor<State, Double, Transition<Void, State>> {
    private Multimap<Resource, IndexedMessagePair> index;

    public OutputIndexedBackwardGraphAccessor(IOComposerInput input) {
        super(input.getSkolemizedUnion(), input.getWantedAsVariables());
        setupIndex();
    }

    @Override
    public ReplicatedGraphAccessor duplicate(IOComposerInput input) {
        return new OutputIndexedBackwardGraphAccessor(input);
    }

    private void setupIndex() {
        index = HashMultimap.create();
        model.listSubjectsWithProperty(RDF.type, Unserved.Message).forEachRemaining(resource -> {
            Statement stmt = resource.getProperty(Unserved.when);
            if (stmt == null || stmt.getObject().equals(Unserved.when) || !stmt.getObject().isResource())
                return;
            stmt = stmt.getResource().getProperty(Unserved.reactionTo);
            if (stmt == null)
                return;
            Message antecedent = stmt.getResource().as(Message.class);
            Message consequent = resource.as(Message.class);
            MessagePairAction pair = new MessagePairAction(antecedent, consequent);
            consequent.getVariablesRecursive()
                    .stream().filter(v -> v.getType() != null)
                    .forEach(v -> index.put(v.getType(), new IndexedMessagePair(pair, v)));
        });
    }

    @Override
    protected Stream<Resource> getSubclassesOf(Resource aClass) {
        return StreamSupport.stream(Spliterators.spliteratorUnknownSize(
                BFSBGPIterator.from(model, aClass).backward(RDFS.subClassOf),
                Spliterator.DISTINCT | Spliterator.NONNULL), false);
    }

    @Override
    protected Stream<Variable> getKnown(@Nullable Resource type) {
        Stream<Variable> stream = ModelStreams.subjectsWithProperty(model, RDF.type,
                Unserved.ValueBoundVariable, Variable.class);
        return type == null ? stream
                : stream.filter(k -> k.getType() != null && k.getType().equals(type));
    }

    @Override
    protected Stream<MessagePairAction> getCandidates(Variable target, Set<Message> blacklist) {
        Resource type = target.getType();
        if (type == null) return Stream.empty();
        return index.get(type).stream().filter(i -> !blacklist.contains(i.pair.getConsequent()))
                .map(i -> i.createMessagePair(target));
    }

    private static class IndexedMessagePair {
        MessagePairAction pair;
        Variable resultVar;

        IndexedMessagePair(MessagePairAction pair, Variable resultVar) {
            this.pair = pair;
            this.resultVar = resultVar;
        }

        MessagePairAction createMessagePair(Variable target) {
            return new MessagePairAction(pair.getAntecedent(), pair.getConsequent())
                    .addAssignment(resultVar, target);
        }
    }

    @Override
    public void close() {
    }
}

package br.ufsc.lapesd.unserved.testbench.restdesc_pragproof.n3;

import br.ufsc.lapesd.unserved.testbench.util.ResourcesBackground;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;
import org.apache.jena.riot.RDFFormat;

import java.util.Collections;

public class Reason extends ResourcesBackground {
    public Reason() {
        super(Collections.singletonList(ImmutablePair.of("restdesc_pragproof/n3/reason.rdf",
                RDFFormat.RDFXML)), true);
    }

    private static Reason instance = new Reason();
    public static Reason getInstance() {
        return instance;
    }

    public static String IRI = "http://www.w3.org/2000/10/swap/reason";
    public static String PREFIX = IRI + "#";
    public static String PREFIX_SHORT = "r";

    private static Property p(String localName) {
        return ResourceFactory.createProperty(PREFIX + localName);
    }
    private static Resource r(String localName) {
        return ResourceFactory.createResource(PREFIX + localName);
    }

    public static Property rule = p("rule");
    public static Property gives = p("gives");
    public static Property evidence = p("evidence");
    public static Property component = p("component");
    public static Property source = p("source");
    public static Property because = p("because");

    public static Resource Inference = r("Inference");
    public static Resource Proof = r("Proof");
    public static Resource Extraction = r("Extraction");
    public static Resource Parsing = r("Parsing");
    public static Resource Conjunction = r("Conjunction");
}

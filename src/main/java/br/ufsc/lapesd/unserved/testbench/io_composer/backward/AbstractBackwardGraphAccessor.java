package br.ufsc.lapesd.unserved.testbench.io_composer.backward;

import br.ufsc.lapesd.unserved.testbench.io_composer.state.*;
import br.ufsc.lapesd.unserved.testbench.model.Message;
import br.ufsc.lapesd.unserved.testbench.model.Variable;
import es.usc.citius.hipster.model.SimpleTransition;
import es.usc.citius.hipster.model.Transition;
import es.usc.citius.hipster.util.Predicate;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;

import javax.annotation.Nullable;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 */
public abstract class AbstractBackwardGraphAccessor
        implements BackwardGraphAccessor<State, Double, Transition<Void, State>> {
    protected final Model model;
    private final State initial;
    private State goal;

    protected AbstractBackwardGraphAccessor(Model model, Set<Variable> wanted) {
        this.model = model;
        this.initial = State.goal(wanted, null);
    }

    protected abstract Stream<Resource> getSubclassesOf(Resource aClass);
    protected abstract Stream<Variable> getKnown(@Nullable Resource type);
    protected abstract Stream<MessagePairAction> getCandidates(Variable target, Set<Message> blacklist);

    @Override
    public State getInitial() {
        return initial;
    }

    public State getGoal() {
        if (goal == null) {
            goal = State.initial(getKnown(null).collect(Collectors.toSet()));
        }
        return goal;
    }

    @Override
    public boolean isGoal(State state) {
        return state.equals(getGoal());
    }

    public Iterable<Transition<Void, State>> getTransitions(State state) {
        if (state.getInputs().isEmpty())
            return Collections.singletonList(new SimpleTransition<>(state, getGoal()));

        Combinator c;
        try {
            c = new Combinator(state);
        } catch (Combinator.NoVarAssignmentException e) {
            return Collections.emptyList();
        }

        List<Transition<Void, State>> transitions = new ArrayList<>(c.sets.size());
        for (StateActions set : c.sets) {
            Set<Message> consequentSet = set.getConsequentSet();
            Set<Variable> inputs = c.inputsMap.get(set);
                double heuristic = inputs.size() / (double) c.maxInputs;
            Set<Variable> known = new HashSet<>(set.getAssignments().getTargets());
            consequentSet.forEach(m -> known.addAll(m.getVariablesRecursive()));
            State previous = State.request(state, heuristic, inputs, known, set);
            previous.setSuccessor(state);
            transitions.add(new SimpleTransition<>(state, previous));
        }

        return transitions;
    }

    @Override
    public Double getCost(Transition<Void, State> transition) {
        return transition.getState().getCostFromInitial()
                + transition.getState().getHeuristic();
    }

    private Copies getCandidateCopy(Variable target) {
        Resource type = target.getType();
        if (type == null) return null;
        CompatibleRepresentationChecker representationChecker;
        representationChecker = new CompatibleRepresentationChecker(target.getRepresentation());

        Variable known = getKnown(type)
                .filter(representationChecker::apply)
                .findFirst().orElse(null);
        if (known != null)
            return new Copies().add(known, target);

        known = getSubclassesOf(type)
                .map(s -> getKnown(s)
                        .filter(representationChecker::apply)
                        .findFirst())
                .filter(Optional::isPresent)
                .findFirst().orElse(Optional.empty()).orElse(null);
        if (known != null)
            return new Copies().add(known, target);
        return null;
    }

    private class CompatibleRepresentationChecker implements Predicate<Variable> {
        private Set<Resource> classes;

        public CompatibleRepresentationChecker(@Nullable Resource representation) {
            if (representation == null) classes = null;
            classes = Stream.concat(Stream.of(representation), getSubclassesOf(representation))
                    .collect(Collectors.toSet());
        }

        @Override
        public boolean apply(Variable variable) {
            Resource rep = variable.getRepresentation();
            return classes == null || (rep != null && classes.contains(rep));
        }
    }

    private class Combinator {
        final Set<StateActions> sets = new HashSet<>();
        ArrayList<Variable> unknowns = null;
        ArrayList<ArrayList<? extends Action>> actionLists = null;
        Map<StateActions, Set<Variable>> inputsMap;
        int maxInputs;

        public Combinator(State state) throws Combinator.NoVarAssignmentException {
            unknowns = new ArrayList<>(state.getInputs());
            actionLists = new ArrayList<>(unknowns.size());
            Set<Message> blacklist = getAllConsequentMessages(state);
            for (Variable v : unknowns) {
                Copies copies = getCandidateCopy(v);
                //FIXME subclass copy is always preferred from a exact attribution from a message?
                if (copies != null) {
                    ArrayList<Copies> list = new ArrayList<>();
                    list.add(copies);
                    this.actionLists.add(list);
                } else {
                    ArrayList<? extends Action> candidates = getCandidates(v, blacklist)
                            .collect(Collectors.toCollection(ArrayList::new));
                    if (candidates == null)
                        throw new Combinator.NoVarAssignmentException();
                    this.actionLists.add(candidates);
                }
            }
            run(new StateActions(), 0);
            createUnknownMap();
        }

        private Set<Message> getAllConsequentMessages(State state) {
            HashSet<Message> set = new HashSet<>();
            while (state != null) {
                set.addAll(state.getActions().getConsequentSet());
                state = state.getSuccessor();
            }
            return set;
        }

        private Set<Variable> getUnknowns(Set<Message> messages) {
            Set<Variable> set = new HashSet<>();
            for (Message message : messages) {
                set.addAll(message.getVariables());
            }
            return set;
        }

        private void createUnknownMap() {
            inputsMap = new HashMap<>();
            for (StateActions set : sets) {
                Set<Variable> unknowns = getUnknowns(set.getAntecedents());
                if (maxInputs < unknowns.size())
                    maxInputs = unknowns.size();
                inputsMap.put(set, unknowns);
            }
        }

        private void run(StateActions current, int varIdx) {
            if (varIdx == unknowns.size()) {
                sets.add(current);
            } else {
                for (Action a : actionLists.get(varIdx)) {
                    StateActions next = new StateActions(current);
                    next.add(a);
                    run(next, varIdx + 1);
                }
            }
        }

        class NoVarAssignmentException extends Exception {
        }

    }
}

package br.ufsc.lapesd.unserved.testbench.util;

import org.apache.jena.rdf.model.Resource;

import java.util.HashSet;

public class TargetedSkolemizerMap extends SkolemizerMap {
    private HashSet<Resource> targets = new HashSet<>();

    public TargetedSkolemizerMap(HashSet<Resource> targets) {
        this.targets = targets;
    }
    public TargetedSkolemizerMap(String prefix, HashSet<Resource> targets) {
        super(prefix);
        this.targets = targets;
    }
    public TargetedSkolemizerMap() {
        super();
    }
    public TargetedSkolemizerMap(String prefix) {
        super(prefix);
    }

    public TargetedSkolemizerMap addTarget(Resource resource) {
        targets.add(resource);
        return this;
    }

    @Override
    public Resource get(Resource resource) {
        return targets.contains(resource) ? super.get(resource) : resource;
    }
}

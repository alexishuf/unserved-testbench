package br.ufsc.lapesd.unserved.testbench.pragproof.exceptions;

import org.apache.jena.rdf.model.Resource;

import java.util.List;

public class UnsupportedActionClasses extends PragmaticProofException {
    public UnsupportedActionClasses(List<Resource> classes) {
        super("Unsupported action classes: " + classes.stream().map(Resource::toString)
                .reduce((l, r) -> l + ", " + r));
    }
}

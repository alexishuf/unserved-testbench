package br.ufsc.lapesd.unserved.testbench.components.expressions.evaluator.instance;

import br.ufsc.lapesd.unserved.testbench.components.expressions.PropertyRestriction;
import br.ufsc.lapesd.unserved.testbench.components.expressions.evaluator.ClassExpressionEvaluator.Result;
import br.ufsc.lapesd.unserved.testbench.components.expressions.evaluator.Step;
import com.google.common.base.Preconditions;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;

import javax.annotation.Nonnull;
import java.util.Collections;

import static br.ufsc.lapesd.unserved.testbench.util.ModelStreams.objectsOfProperty;

public class PropertyRestrictionInstanceStep extends Step<RDFNode> {
    @Nonnull private final PropertyRestriction expr;
    private final Step<RDFNode> rangeClassStep;

    public PropertyRestrictionInstanceStep(@Nonnull PropertyRestriction expression) {
        super(Collections.emptySet());
        Preconditions.checkArgument(!expression.getRestrictionType().hasRangeClass());
        this.expr = expression;
        this.rangeClassStep = null;
    }

    public PropertyRestrictionInstanceStep(@Nonnull PropertyRestriction expression,
                                           @Nonnull Step<RDFNode> rangeClassStep) {
        super(Collections.emptySet());
        Preconditions.checkArgument(expression.getRestrictionType().hasRangeClass());
        this.expr = expression;
        this.rangeClassStep = rangeClassStep;
    }

    @Override
    protected Result performEvaluate(RDFNode node) {
        if (!node.isResource()) return Result.FALSE;
        Resource ind = node.asResource();
        if (Helper.isA(ind, expr.getNames())) return Result.TRUE;

        switch (expr.getRestrictionType()) {
            case ALL_VALUES_FROM:
                assert getChildren().size() == 1 && rangeClassStep != null;
                boolean fail = objectsOfProperty(ind, expr.getOnProperty(), RDFNode.class)
                        .anyMatch(n -> !n.isResource() ||
                                rangeClassStep.evaluate(n.asResource()) == Result.FALSE);
                return fail ? Result.FALSE : Result.INDETERMINATE;
            case SOME_VALUES_FROM:
                boolean ok = objectsOfProperty(ind, expr.getOnProperty(), Resource.class)
                        .anyMatch(r -> rangeClassStep.evaluate(r) == Result.TRUE);
                return ok ? Result.TRUE : Result.INDETERMINATE;
            case HAS_SELF:
                return ind.hasProperty(expr.getOnProperty(), ind) ? Result.TRUE
                        : Result.INDETERMINATE;
            case HAS_VALUE:
                return ind.hasProperty(expr.getOnProperty(), expr.getValue()) ? Result.TRUE
                        : Result.INDETERMINATE;
        }

        throw new UnsupportedOperationException();
    }
}

package br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service;

import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.And;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.cost.CostInfo;
import br.ufsc.lapesd.unserved.testbench.model.Variable;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class EndNode extends AbstractNode implements Node {
    private final @Nonnull HashSet<Variable> wanted = new HashSet<>();
    private final @Nonnull And conditions;

    public EndNode(Collection<Variable> wanted, @Nonnull And conditions) {
        super(new ArrayList<>());
        this.wanted.addAll(wanted);
        this.conditions = conditions;
    }

    protected EndNode(EndNode other) {
        super(other);
        wanted.addAll(other.wanted);
        this.conditions = other.conditions;
    }

    @Override
    public @Nonnull Set<Variable> getInputs() {
        return wanted;
    }

    @Override
    public @Nonnull Set<Variable> getOutputs() {
        return wanted;
    }

    @Nonnull
    @Override
    public And getPreConditions() {
        return conditions;
    }

    @Nonnull
    @Override
    public And getPostConditions() {
        return And.empty();
    }

    @Override
    public Object getId() {
        return this;
    }

    @Nonnull
    @Override
    public CostInfo getCostInfo() {
        return CostInfo.EMPTY;
    }

    @Override
    public String toString() {
        return String.format("EndNode(%s)", wanted.stream().map(Object::toString)
                .reduce((l, r) -> l + ", " + r).orElse(""));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EndNode endNode = (EndNode) o;

        return wanted.equals(endNode.wanted);
    }

    @Override
    public int hashCode() {
        return wanted.hashCode();
    }
}

package br.ufsc.lapesd.unserved.testbench.process;

import br.ufsc.lapesd.unserved.testbench.process.actions.ActionExecutionException;
import br.ufsc.lapesd.unserved.testbench.process.data.DataContext;
import org.apache.jena.rdf.model.Resource;

import javax.annotation.Nonnull;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.function.Consumer;

public interface Interpreter {
    interface Task {
        Task cancel();
        Task abort();
        boolean isDone() throws ActionExecutionException;
        void waitForDone() throws InterruptedException, ActionExecutionException;
        void waitForDone(long count, TimeUnit timeUnit) throws InterruptedException, ActionExecutionException, TimeoutException;
    }

    <T> void setResultConsumer(Class<T> aClass, Consumer<T> consumer);
    Task interpret(@Nonnull Resource resource);
    Task interpret(@Nonnull DataContext dataContext, @Nonnull Resource resource);

    @SuppressWarnings("unchecked")
    boolean deliverResult(Object result);
}

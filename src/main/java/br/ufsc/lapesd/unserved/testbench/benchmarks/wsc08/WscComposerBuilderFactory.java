package br.ufsc.lapesd.unserved.testbench.benchmarks.wsc08;

import br.ufsc.lapesd.unserved.testbench.Algorithm;
import br.ufsc.lapesd.unserved.testbench.benchmarks.BuilderFactory;
import br.ufsc.lapesd.unserved.testbench.io_composer.IOComposerBuilder;
import br.ufsc.lapesd.unserved.testbench.io_composer.impl.IOComposerInput;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.LayeredServiceSetGraphIOComposer;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.lazy.BackwardSetNodeProvider;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.lazy.NaiveBackwardSetNodeProvider;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.lazy.NonEquivalentBackwardSetNodeProvider;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.optimizations.BackwardClosureOptimizer;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.optimizations.ServiceCompressionOptimizer;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.discovery.*;
import br.ufsc.lapesd.unserved.testbench.util.FixedCacheTransitiveClosureGetter;
import br.ufsc.lapesd.unserved.testbench.util.NaiveTransitiveClosureGetter;
import br.ufsc.lapesd.unserved.testbench.util.TransitiveClosureGetter;
import com.google.common.base.Preconditions;
import org.apache.jena.vocabulary.RDFS;

import javax.annotation.Nonnull;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class WscComposerBuilderFactory implements BuilderFactory {
    private static Set<Algorithm> supportedAlgorithms;
    private boolean serviceCompression = false;
    private boolean backwardClosureOpt = false;
    private boolean preconditions = false;
    private boolean naiveIO = false;
    private boolean combinedJN = false;
    private boolean listSubclasses = false;
    private boolean naiveTCG = false;
    private boolean basicReasoning = false;

    private Map<String, String> parseArgs(@Nonnull String args) {
        List<List<String>> kvs = Stream.of(args.split(";"))
                .map(kv -> Arrays.asList(kv.split("="))).collect(Collectors.toList());
        Preconditions.checkArgument(kvs.stream().allMatch(l -> l.size() > 0 && l.size() <= 2),
                "Malformed kv list");
        Map<String, String> config = new HashMap<>();
        kvs.stream().map(kv -> kv.size() < 2 ? Arrays.asList(kv.get(0), "true") : kv)
                .forEach(kv -> config.put(kv.get(0), kv.get(1)));
        return config;
    }

    @Override
    public void setArgs(@Nonnull String args) {
        Map<String, String> config = parseArgs(args);
        serviceCompression = Boolean.parseBoolean(config
                .getOrDefault("serviceCompression", "false"));
        backwardClosureOpt = Boolean.parseBoolean(config
                .getOrDefault("backwardClosureOpt", "false"));
        preconditions = Boolean.parseBoolean(config
                .getOrDefault("preconditions", "false"));
        naiveIO = Boolean.parseBoolean(config
                .getOrDefault("naiveIO", "false"));
        combinedJN = Boolean.parseBoolean(config
                .getOrDefault("combinedJN", "false"));
        listSubclasses = Boolean.parseBoolean(config
                .getOrDefault("listSubclasses", "false"));
        naiveTCG = Boolean.parseBoolean(config
                .getOrDefault("naiveTCG", "false"));
        basicReasoning = Boolean.parseBoolean(config
                .getOrDefault("basicReasoning", "false"));
    }

    @Override
    public IOComposerBuilder get(Algorithm algorithm) {
        IOComposerBuilder ioBuilder = algorithm.createIOComposerBuilder();
        ioBuilder.withInputHasNoSkolemized(true);
        Preconditions.checkArgument(supportedAlgorithms.contains(algorithm));
        LayeredServiceSetGraphIOComposer.Builder builder =
                (LayeredServiceSetGraphIOComposer.Builder) ioBuilder;

        if (serviceCompression) {
            builder = builder.withOptimizer(new ServiceCompressionOptimizer()
                    .setSelfAlternative(true));
        } else {
            builder = builder.withoutOptimizers(o -> o instanceof ServiceCompressionOptimizer);
        }
        if (backwardClosureOpt) builder = builder.withOptimizer(new BackwardClosureOptimizer());
        else builder = builder.withoutOptimizers(o -> o instanceof BackwardClosureOptimizer);

        if (preconditions) {
            NaiveBackwardSetNodeProvider bwdSNP;
            bwdSNP = new NonEquivalentBackwardSetNodeProvider().setUseConditions(true)
                    .setForceCombinedJumpNodes(combinedJN);
            builder = builder.setBackwardSetNodeProvider(bwdSNP);
            if (naiveIO) {
                if (listSubclasses) throw badPair("naiveIO", "listSubclasses");
                builder = builder.setLayersStateFactory(
                        ci -> new PreconditionsLayersState(getTCG(ci, naiveTCG)));
            } else {
                builder = builder.setLayersStateFactory(ci -> new FastPreconditionsLayersState(
                        new InputServiceIndex(getTCG(ci, naiveTCG), true), listSubclasses));
            }
        } else if (naiveIO) {
            if (listSubclasses) throw badPair("naiveIO", "listSubclasses");
            if (naiveTCG) throw badPair("naiveIO", "naiveTCG");
            builder = builder.setLayersStateFactory(IOLayersState::new);
        } else {
            builder = builder.setLayersStateFactory(ci -> new FastIOLayersState(
                    new InputServiceIndex(getTCG(ci, naiveTCG)), listSubclasses));
        }

        BackwardSetNodeProvider bwsnp = builder.getBackwardSetNodeProvider();
        if (bwsnp instanceof NaiveBackwardSetNodeProvider)
            ((NaiveBackwardSetNodeProvider) bwsnp).setForceCombinedJumpNodes(combinedJN);

        builder.withBasicReasoning(basicReasoning);
        return builder;
    }

    private UnsupportedOperationException badPair(String a, String b) {
        return new UnsupportedOperationException(
                String.format("Factors %s and %b cannot be combined", a, b));
    }

    private TransitiveClosureGetter getTCG(IOComposerInput ci, boolean naiveTCG) {
        if (naiveTCG) {
            return new NaiveTransitiveClosureGetter(RDFS.subClassOf, false);
        } else {
            return FixedCacheTransitiveClosureGetter.forward(RDFS.subClassOf).setThreshold(7)
                    .visitAll(ci.getSkolemizedUnion()).build();
        }
    }

    static {
        supportedAlgorithms = new HashSet<>();
        supportedAlgorithms.add(Algorithm.BackwardAStarLayeredIO);
        supportedAlgorithms.add(Algorithm.BackwardAStarLayeredIORT);
        supportedAlgorithms.add(Algorithm.DStarLiteLayeredIO);
        supportedAlgorithms.add(Algorithm.DStarLiteLayeredIORT);
        supportedAlgorithms.add(Algorithm.GreedyBackwardLayeredIONoAdapt);
        supportedAlgorithms.add(Algorithm.GreedyBackwardLayeredIO);
        supportedAlgorithms.add(Algorithm.GreedyBackwardLayeredIORTNoAdapt);
        supportedAlgorithms.add(Algorithm.GreedyBackwardLayeredIORT);
    }
}

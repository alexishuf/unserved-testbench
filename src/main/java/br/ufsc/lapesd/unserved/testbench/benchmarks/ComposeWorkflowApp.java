package br.ufsc.lapesd.unserved.testbench.benchmarks;

import br.ufsc.lapesd.unserved.testbench.Algorithm;
import br.ufsc.lapesd.unserved.testbench.benchmarks.compose.TimesExperimentResult;
import br.ufsc.lapesd.unserved.testbench.components.ComponentRegistry;
import br.ufsc.lapesd.unserved.testbench.components.DefaultComponentRegistry;
import br.ufsc.lapesd.unserved.testbench.components.WrappingComponentRegistry;
import br.ufsc.lapesd.unserved.testbench.composer.Composer;
import br.ufsc.lapesd.unserved.testbench.composer.Composition;
import br.ufsc.lapesd.unserved.testbench.composer.ReplicatedComposer;
import br.ufsc.lapesd.unserved.testbench.composer.impl.ReplicationComposition;
import br.ufsc.lapesd.unserved.testbench.input.RDFInput;
import br.ufsc.lapesd.unserved.testbench.input.RDFInputFile;
import br.ufsc.lapesd.unserved.testbench.io_composer.IOComposer;
import br.ufsc.lapesd.unserved.testbench.io_composer.IOComposerBuilder;
import br.ufsc.lapesd.unserved.testbench.io_composer.IOComposerTimes;
import br.ufsc.lapesd.unserved.testbench.io_composer.IOCompositionResult;
import br.ufsc.lapesd.unserved.testbench.io_composer.impl.DefaultIOComposerTimes;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.AbstractLayeredIOComposer;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.LayeredServiceSetGraphIOComposer;
import br.ufsc.lapesd.unserved.testbench.iri.Unserved;
import br.ufsc.lapesd.unserved.testbench.iri.UnservedP;
import br.ufsc.lapesd.unserved.testbench.model.Variable;
import br.ufsc.lapesd.unserved.testbench.process.Interpreter;
import br.ufsc.lapesd.unserved.testbench.process.UnservedPInterpreter;
import br.ufsc.lapesd.unserved.testbench.process.actions.mock.MockReceiveRunner;
import br.ufsc.lapesd.unserved.testbench.process.actions.mock.MockSendRunner;
import br.ufsc.lapesd.unserved.testbench.process.data.DataContext;
import com.google.common.base.Preconditions;
import com.google.common.base.Stopwatch;
import com.google.gson.Gson;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFFormat;
import org.apache.jena.riot.RDFLanguages;
import org.kohsuke.args4j.Argument;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@SuppressWarnings("MismatchedReadAndWriteOfArray")
public class ComposeWorkflowApp {
    @Option(name = "--algorithm", aliases = {"-a"}, required = true)
    private Algorithm algorithm;

    @Option(name = "--wanted", aliases = {"-w"})
    private String[] wantedVarURIs;

    @Option(name = "--rules", aliases = {"-R"})
    private File rules;

    @Option(name = "--workflow-out")
    private File workflowOutput;

    @Option(name = "--times-out")
    private File timesOut = null;

    @Option(name = "--preheat-count")
    private int preheatCount = 0;

    @Option(name = "--execute", usage = "Simulates execution of the composition with mock " +
            "action runners")
    private boolean execute = false;

    @Option(name = "--only-graph", usage = "For LayeredServiceSetGraphIOComposer, only " +
            "create the dependency graph and stop.")
    private boolean onlyGraph = false;

    @Option(name = "--dismiss-output", usage = "Dismiss output of the composer")
    private boolean dismissOutput = false;

    @Option(name = "--allow-null-composition", usage = "If false (the default) null compositions " +
            "will throw an exception")
    private boolean allowNullComposition = false;

    @Option(name = "--builder-factory", usage = "Fully qualified class name for a factory to " +
            "replace Algorithm.createIOComposerBuilder()")
    private String builderFactory = null;

    @Option(name = "--builder-factory-args", usage = "Arbitrary string forwarded to the " +
            "--builder-factory", depends = {"--builder-factory"})
    private String builderFactoryArgs = null;

    @Option(name = "--component-reg-setup", usage = "Fully qualified class name for a " +
            "ComponentRegistrySetup implementation with default constructor")
    private String componentRegistrySetup = null;

    @Option(name = "--component-reg-setup-args", usage = "Args to be forwarded to the " +
            "--component-reg-setup instance", depends = {"--component-reg-setup"})
    private String componentRegistrySetupArgs = null;

    @Argument
    private File[] fileInputs;

    public static void main(String[] args) throws Exception {
        ComposeWorkflowApp app = new ComposeWorkflowApp();
        CmdLineParser parser = new CmdLineParser(app);
        parser.parseArgument(args);
        app.run();
    }

    private void run() throws Exception {
        Unserved.init();
        UnservedP.init();
        preheat();
        TimesExperimentResult time = new TimesExperimentResult();
        Stopwatch watch = Stopwatch.createStarted();
        run(time);
        time.setTotal(watch.elapsed(TimeUnit.MICROSECONDS)/1000.0);
        System.out.printf("Time: %s\n", time);
        try (PrintStream out = new PrintStream(new FileOutputStream(timesOut))) {
            new Gson().toJson(time, out);
        }
    }

    private void preheat() throws Exception {
        for (int i = 0; i < preheatCount; i++) {
            TimesExperimentResult time = new TimesExperimentResult();
            run(time);
            System.out.printf("Preheat %d/%d %s.\n", i, preheatCount, time);
            Utils.preheatCooldown();
        }
        Utils.preheatCooldown();
    }

    private void run(TimesExperimentResult time) throws Exception {
        IOComposer composer = null;
        ComponentRegistry componentRegistry = null;
        Interpreter interpreter = null;
//        new File("/tmp/unserved-neighbours").delete();
        try {
            if (execute) {
                componentRegistry = new WrappingComponentRegistry(
                        DefaultComponentRegistry.getInstance());
                componentRegistry.registerComponent(MockSendRunner.Factory.class);
                componentRegistry.registerComponent(MockReceiveRunner.Factory.class);
                setupComponentRegistry(componentRegistry);
                interpreter = new UnservedPInterpreter(componentRegistry);
            }

            Stopwatch watch = Stopwatch.createStarted();
            ArrayList<RDFInput> inputs = initRdfInputs();
            ArrayList<Variable> wantedVars = initWantedVars(inputs);
            composer = initComposer(inputs, wantedVars);
            time.setInitialization(watch.elapsed(TimeUnit.MICROSECONDS)/1000.0);

            if (onlyGraph) {
                Preconditions.checkArgument(composer instanceof LayeredServiceSetGraphIOComposer);
                DefaultIOComposerTimes times = new DefaultIOComposerTimes();
                ((LayeredServiceSetGraphIOComposer)composer).createGraph(times);
                times.addTime(IOComposer.planningTime, 0, TimeUnit.MILLISECONDS);
                setTimes(composer, time, times);
            } else if (composer instanceof ReplicatedComposer && execute) {
                Preconditions.checkArgument(interpreter != null);
                composeAndExecuteReplicated(time, composer, interpreter);
            } else {
                composeAndExecuteRegular(time, composer, interpreter);
            }


        } finally {
            if (composer != null) composer.close();
            if (componentRegistry != null) componentRegistry.close();
        }
    }

    private void setupComponentRegistry(ComponentRegistry componentRegistry) throws Exception {
        if (componentRegistrySetup == null) return;
        try {
            @SuppressWarnings("unchecked") Class<? extends ComponentRegistrySetup> aClass =
                    (Class<? extends ComponentRegistrySetup>) getClass().getClassLoader()
                            .loadClass(componentRegistrySetup);
            Constructor<? extends ComponentRegistrySetup> constructor = aClass.getConstructor();
            ComponentRegistrySetup setup = constructor.newInstance();
            if (componentRegistrySetupArgs != null)
                setup.setArgs(componentRegistrySetupArgs);
            setup.setup(componentRegistry);
        } catch (ClassNotFoundException | NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException e) {
            throw new Exception("Failed to load --component-reg-setup");
        }
    }

    private void composeAndExecuteReplicated(@Nonnull TimesExperimentResult expResult,
                                             @Nonnull IOComposer composer_,
                                             @Nonnull Interpreter interpreter) throws Exception {
        Preconditions.checkArgument(composer_ instanceof ReplicatedComposer);
        ReplicatedComposer composer = (ReplicatedComposer) composer_;

        CompletableFuture<IOCompositionResult> future = new CompletableFuture<>();
        interpreter.setResultConsumer(IOCompositionResult.class, future::complete);
        expResult.setOutput(0);

        Stopwatch watch = Stopwatch.createStarted();
        composer.run(interpreter);
        boolean timedExecution = false;
        try (IOCompositionResult result = future.get()) {
            expResult.setExecution(watch.elapsed(TimeUnit.MICROSECONDS) / 1000.0);
            timedExecution = true;
            IOComposerTimes times = result.getTimes().getGlobal();
            setTimes(composer, expResult, times);

            for (Resource inVar : result.getInputWantedVariables()) {
                Resource resource = result.getWantedVariable(inVar);
                if (!resource.canAs(Variable.class))
                    throw new Exception("Wanted variable is not a Variable!");
                if (resource.as(Variable.class).getValue() == null)
                    throw new Exception("Wanted variable without value");
            }
        } catch (Throwable t) {
            if (!timedExecution)
                expResult.setExecution(watch.elapsed(TimeUnit.MICROSECONDS) / 1000.0);
        }
    }

    private void setTimes(@Nonnull Composer composer, @Nonnull TimesExperimentResult expResult,
                          IOComposerTimes times) {
        if (composer instanceof AbstractLayeredIOComposer)
            ((AbstractLayeredIOComposer) composer).shutdownMemoryUsageSampler();
        expResult.setComposition(times
                .getTotal(IOComposer.planningTime, TimeUnit.MICROSECONDS) / 1000.0);
        times.getTimes(IOComposer.planningTime, TimeUnit.MICROSECONDS)
                .stream().map(us -> us / 1000.0).forEach(expResult::addCompositionTime);

        if (!times.getNames().contains("blacklisting")) {
            expResult.setBlacklisting(0);
        } else {
            expResult.setBlacklisting(times
                    .getTotal("blacklisting", TimeUnit.MICROSECONDS) / 1000.0);
            times.getTimes("blacklisting", TimeUnit.MICROSECONDS)
                    .stream().map(us -> us / 1000.0).forEach(expResult::addBlacklistingTime);
        }

        HashSet<String> done;
        done = new HashSet<>(Arrays.asList(IOComposer.planningTime, "blacklisting"));
        times.getNames().stream().filter(n -> !done.contains(n)).forEach(n ->
                times.getTimes(n, TimeUnit.MICROSECONDS).stream().map(us -> us/1000.0)
                        .forEach(ms -> expResult.addTime(n, ms)));
        times.getMemNames().forEach(name -> times.getMemSamples(name)
                .forEach(bytes -> expResult.addMem(name, bytes)));
    }

    private void composeAndExecuteRegular(@Nonnull TimesExperimentResult time, @Nonnull IOComposer composer,
                                          @Nullable Interpreter interpreter) throws Exception {
        Composition composition = null;
        try {
            Stopwatch watch = Stopwatch.createStarted();
            composition = composer.run();

            IOComposerTimes ioTimes = !(composition instanceof ReplicationComposition) ? null :
                    ((ReplicationComposition) composition).getContainer().getExecution().getTimes();
            if (ioTimes != null) {
                ioTimes = ioTimes.getGlobal();
                setTimes(composer, time, ioTimes);
            } else {
                double ms = watch.elapsed(TimeUnit.MICROSECONDS) / 1000.0;
                time.setComposition(ms);
                time.addCompositionTime(ms);
                time.setBlacklisting(0);
            }
            if (!allowNullComposition && composition.isNull())
                throw new Exception("Null composition");

            if (interpreter != null) {
                if (composition.isNull()) {
                    time.setExecution(0);
                } else {
                    watch.reset().start();
                    try (DataContext dataContext = composition.createDataContext()) {
                        interpreter.interpret(dataContext, composition.getWorkflowRoot()).waitForDone();
                    }
                    time.setExecution(watch.elapsed(TimeUnit.MICROSECONDS) / 1000.0);
                }
            }

            if (!dismissOutput) {
                watch.reset().start();
                Resource root = composition.getWorkflowRoot();
                if (workflowOutput != null) {
                    try (FileOutputStream out = new FileOutputStream(workflowOutput)) {
                        RDFDataMgr.write(out, root.getModel(), Lang.TURTLE);
                    }
                }
                time.setOutput(watch.elapsed(TimeUnit.MICROSECONDS) / 1000.0);
            } else {
                time.setOutput(0);
            }
        } finally {
            if (composition != null) composition.close();
        }
    }


    private IOComposer initComposer(Collection<RDFInput> inputs,
                                    Collection<Variable> wantedVars) throws Exception {
        IOComposerBuilder builder = createBuilder();
        if (rules != null) builder.withRulesFile(rules);
        builder.addInputs(inputs);
        wantedVars.forEach(builder::markWantedVariable);
        return builder.build();
    }

    private IOComposerBuilder createBuilder() throws Exception {
        if (builderFactory != null) {

            BuilderFactory factory;
            try {
                @SuppressWarnings("unchecked") Class<BuilderFactory> clazz = (Class<BuilderFactory>)
                        getClass().getClassLoader().loadClass(builderFactory);
                Constructor<BuilderFactory> constructor = clazz.getConstructor();
                factory = constructor.newInstance();
            } catch (ClassNotFoundException | NoSuchMethodException | InstantiationException | InvocationTargetException | IllegalAccessException e) {
                throw new Exception("Could not instantiate given --builder-factory");
            }
            if (builderFactoryArgs != null)
                factory.setArgs(builderFactoryArgs);
            return factory.get(algorithm);
        }
        return algorithm.createIOComposerBuilder();
    }

    private ArrayList<RDFInput> initRdfInputs() throws IOException {
        ArrayList<RDFInput> inputs = new ArrayList<>();
        for (File file : fileInputs) {
            Matcher matcher = Pattern.compile(".*\\.([^.]+)$").matcher(file.getName());
            if (!matcher.matches())
                throw new IllegalArgumentException("All inputs must have suffixes indicating a RDF syntax. Offending arg: " + file.getName());
            RDFFormat format = new RDFFormat(RDFLanguages.fileExtToLang(matcher.group(1)));
            RDFInputFile input = new RDFInputFile(file, format);
            input.getModel();
            inputs.add(input);
        }
        return inputs;
    }

    private ArrayList<Variable> initWantedVars(Collection<RDFInput> inputs) throws IOException {
        ArrayList<Variable> wantedVars = new ArrayList<>(wantedVarURIs.length);
        for (String uri : wantedVarURIs) {
            Resource resource = ResourceFactory.createResource(uri);
            for (RDFInput input : inputs) {
                if (!input.getModel().containsResource(resource)) continue;
                resource = input.getModel().createResource(uri);
                if (!resource.canAs(Variable.class))
                    throw new IllegalArgumentException(uri + " is not a Variable.");
                wantedVars.add(resource.as(Variable.class));
            }
        }
        return wantedVars;
    }
}

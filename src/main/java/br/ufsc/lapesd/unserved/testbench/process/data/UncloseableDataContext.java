package br.ufsc.lapesd.unserved.testbench.process.data;

import br.ufsc.lapesd.unserved.testbench.input.RDFInput;
import br.ufsc.lapesd.unserved.testbench.model.Variable;
import org.apache.jena.rdf.model.*;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collection;

/**
 * Wrap another DataContext and ignore close() calls
 */
public class UncloseableDataContext implements DataContext {
    private final DataContext delegate;

    public UncloseableDataContext(DataContext delegate) {
        this.delegate = delegate;
    }

    @Override
    @Nullable
    public Resource getResource(@Nullable Resource resource) {
        return delegate.getResource(resource);
    }

    @Override
    @Nullable
    public Resource getResource(@Nonnull String uri) {
        return delegate.getResource(uri);
    }

    @Override
    @Nullable
    public <T extends Resource> T getResource(@Nullable Resource resource, @Nonnull Class<T> enhanced) {
        return delegate.getResource(resource, enhanced);
    }

    @Override
    @Nullable
    public <T extends Resource> T getResource(@Nonnull String uri, @Nonnull Class<T> enhanced) {
        return delegate.getResource(uri, enhanced);
    }

    @Override
    public void addStatement(@Nonnull Statement statement) {
        delegate.addStatement(statement);
    }

    @Override
    public void addStatements(@Nonnull StmtIterator iterator) {
        delegate.addStatements(iterator);
    }

    @Override
    @Nonnull
    public Model getUnmodifiableModel() {
        return delegate.getUnmodifiableModel();
    }

    @Override
    @Nonnull
    public Collection<RDFInput> getInputs() {
        return delegate.getInputs();
    }

    @Override
    @Nonnull
    public DataContext createNonIsolatedCopy() {
        return delegate.createNonIsolatedCopy();
    }

    @Override
    public void unsetVariable(@Nonnull Variable var) {
        delegate.unsetVariable(var);
    }

    @Override
    public void setVariable(@Nonnull Variable var, @Nullable Resource representation, @Nonnull Literal literal) {
        delegate.setVariable(var, representation, literal);
    }

    @Override
    public void setVariable(@Nonnull Variable var, @Nullable Resource representation, @Nonnull Resource resource) {
        delegate.setVariable(var, representation, resource);
    }

    @Override
    public void close() {
        /* pass */
    }
}

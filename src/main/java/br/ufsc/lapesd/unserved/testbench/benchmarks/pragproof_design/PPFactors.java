package br.ufsc.lapesd.unserved.testbench.benchmarks.pragproof_design;

import br.ufsc.lapesd.unserved.testbench.Algorithm;
import br.ufsc.lapesd.unserved.testbench.benchmarks.experiment.Factors;
import com.google.common.base.Preconditions;

import java.util.Arrays;
import java.util.List;

public class PPFactors extends ComposeFactors implements Factors {
    private final int chainLength;
    private final int alternatives;
    private final int ioCount;
    private final int nonDummyChain;
    private final int nonDummyAlternatives;
    private final int nonDummyIOCount;
    private final boolean preconditions;
    private final String childXms;
    private final String childXmx;
    private final String childXss;
    private final String childJVMOpts;

    public PPFactors(int preheatCount, int chainLength, int alternatives, int ioCount,
                     int nonDummyChain, int nonDummyAlternatives, int nonDummyIOCount,
                     boolean preconditions,
                     boolean execute, Algorithm algorithm,
                     String childXms, String childXmx, String childXss, String childJVMOpts) {
        super(preheatCount, execute, algorithm);
        Preconditions.checkArgument((nonDummyChain>0 && nonDummyIOCount>0 && nonDummyAlternatives>0)
                || (nonDummyChain < 0 && nonDummyIOCount < 0 && nonDummyAlternatives < 0));
        this.chainLength = chainLength;
        this.alternatives = alternatives;
        this.ioCount = ioCount;
        this.nonDummyChain = nonDummyChain;
        this.nonDummyAlternatives = nonDummyAlternatives;
        this.nonDummyIOCount = nonDummyIOCount;
        this.preconditions = preconditions;
        this.childXms = childXms;
        this.childXmx = childXmx;
        this.childXss = childXss;
        this.childJVMOpts = childJVMOpts;
    }

    @Override
    public String getValue(String name) {
        switch (name) {
            case "PreheatCount": return String.valueOf(preheatCount);
            case "ChainLength": return String.valueOf(chainLength);
            case "Alternatives": return String.valueOf(alternatives);
            case "Preconditions": return String.valueOf(preconditions);
            case "IOCount": return String.valueOf(ioCount);
            case "NonDummyChain": return String.valueOf(nonDummyChain);
            case "NonDummyAlternatives": return String.valueOf(nonDummyAlternatives);
            case "NonDummyIOCount": return String.valueOf(nonDummyIOCount);
            case "Execute": return String.valueOf(execute);
            case "Algorithm": return String.valueOf(algorithm);
            case "ChildXms": return String.valueOf(childXms);
            case "ChildXmx": return String.valueOf(childXmx);
            case "ChildXss": return String.valueOf(childXss);

        }
        throw new IllegalArgumentException("Bad factor name");
    }

    @Override
    public List<String> getNames() {
        return Arrays.asList("PreheatCount", "ChainLength", "Alternatives",
                "IOCount", "NonDummyChain", "NonDummyAlternatives", "NonDummyIOCount",
                "Preconditions", "Execute", "Algorithm");
    }

    public int getChainLength() {
        return chainLength;
    }
    public int getAlternatives() {
        return alternatives;
    }
    public int getNonDummyChain() {
        return nonDummyChain;
    }
    public int getNonDummyAlternatives() {
        return nonDummyAlternatives;
    }
    public int getNonDummyIOCount() {
        return nonDummyIOCount;
    }
    public int getIOCount() {
        return ioCount;
    }
    public boolean getPreconditions() {
        return preconditions;
    }
    public String getChildXms() {
        return childXms;
    }
    public String getChildXmx() {
        return childXmx;
    }
    public String getChildXss() {
        return childXss;
    }
    public String getChildJVMOpts() {
        return childJVMOpts;
    }

    @Override
    public String toString() {
        String s = String.format("alt/cond/len: %d/%d/%d", getAlternatives(),
                getIOCount(), getChainLength());
        if (nonDummyChain > 0) {
            s += String.format(", nondummy: %d/%d/%d", getNonDummyAlternatives(),
                    getNonDummyIOCount(), getNonDummyChain());
        }
        s += String.format(", alg[%s%s]: %s",
                getExecute() ? "e" : "", getPreconditions() ? "p" : "e", getAlgorithm().name());
        return s;
    }

    public Builder getBuilder() {
        return new Builder(chainLength, algorithm)
                .withPreheatCount(preheatCount)
                .withAlternatives(alternatives)
                .withIoCount(ioCount)
                .withPreconditions(preconditions)
                .withNonDummyChain(nonDummyChain)
                .withNonDummyAlternatives(nonDummyAlternatives)
                .withNonDummyIOCount(nonDummyIOCount)
                .withChildXms(childXms)
                .withChildXmx(childXmx)
                .withChildXss(childXss)
                .withChildJVMOpts(childJVMOpts)
                .withExecute(execute);
    }

    public static class Builder {
        private int preheatCount = 2;
        private boolean preconditions = false;
        private int chainLength;
        private int alternatives = 1;
        private int ioCount = 1;
        private int nonDummyChain = -1;
        private int nonDummyAlternatives = -1;
        private int nonDummyIOCount = -1;
        private String childXms = null;
        private String childXmx = null;
        private String childXss = null;
        private String childJVMOpts = null;
        private boolean execute = false;

        private Algorithm algorithm;

        public Builder(int chainLength, Algorithm algorithm) {
            this.chainLength = chainLength;
            this.algorithm = algorithm;
        }

        public Builder withPreheatCount(int preheatCount) {
            this.preheatCount = preheatCount;
            return this;
        }
        public Builder withPreconditions(boolean preconditions) {
            this.preconditions = preconditions;
            return this;
        }
        public Builder withChainLength(int chainLength) {
            this.chainLength = chainLength;
            return this;
        }
        public Builder withAlternatives(int alternatives) {
            this.alternatives = alternatives;
            return this;
        }
        public Builder withIoCount(int ioCount) {
            this.ioCount = ioCount;
            return this;
        }
        public Builder withNonDummyChain(int nonDummyChain) {
            this.nonDummyChain = nonDummyChain;
            return this;
        }
        public Builder withNonDummyAlternatives(int nonDummyAlternatives) {
            this.nonDummyAlternatives = nonDummyAlternatives;
            return this;
        }
        public Builder withNonDummyIOCount(int nonDummyIOCount) {
            this.nonDummyIOCount = nonDummyIOCount;
            return this;
        }
        public Builder withExecute(boolean execute) {
            this.execute = execute;
            return this;
        }
        public Builder withAlgorithm(Algorithm algorithm) {
            this.algorithm = algorithm;
            return this;
        }
        public Builder withChildXms(String childXms) {
            this.childXms = childXms;
            return this;
        }
        public Builder withChildXmx(String childXmx) {
            this.childXmx = childXmx;
            return this;
        }
        public Builder withChildXss(String childXss) {
            this.childXss = childXss;
            return this;
        }
        public Builder withChildJVMOpts(String childJVMOpts) {
            this.childJVMOpts = childJVMOpts;
            return this;
        }

        public PPFactors build() {
            return new PPFactors(preheatCount, chainLength, alternatives, ioCount,
                    nonDummyChain, nonDummyAlternatives, nonDummyIOCount, preconditions, execute,
                    algorithm, childXms, childXmx, childXss, childJVMOpts);
        }
    }
}

package br.ufsc.lapesd.unserved.testbench.composer;

public class ComposerBuilderException extends RuntimeException {
    public ComposerBuilderException(Throwable throwable) {
        super(throwable);
    }

    public ComposerBuilderException(String s) {
        super(s);
    }

    public ComposerBuilderException(String s, Throwable throwable) {
        super(s, throwable);
    }
}

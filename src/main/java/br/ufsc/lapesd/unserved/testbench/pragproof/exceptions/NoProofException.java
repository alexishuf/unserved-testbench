package br.ufsc.lapesd.unserved.testbench.pragproof.exceptions;

public class NoProofException extends PragmaticProofException {
    public NoProofException() {
        super("No Proof");
    }
}

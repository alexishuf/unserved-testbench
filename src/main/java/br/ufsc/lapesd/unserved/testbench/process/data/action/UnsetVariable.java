package br.ufsc.lapesd.unserved.testbench.process.data.action;

import br.ufsc.lapesd.unserved.testbench.model.Variable;
import br.ufsc.lapesd.unserved.testbench.process.data.DataContext;
import com.google.common.base.Preconditions;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.annotation.Nonnull;
import java.util.Collections;
import java.util.Set;

public class UnsetVariable implements DataContextAction {
    @Nonnull private final Variable variable;

    public UnsetVariable(@Nonnull Variable variable) {
        Preconditions.checkNotNull(variable);
        this.variable = variable;
    }

    @Nonnull
    public Variable getVariable() {
        return variable;
    }

    @Nonnull
    @Override
    public Set<Variable> changedVariables() {
        return Collections.singleton(variable);
    }

    @Override
    public void apply(DataContext context) {
        context.unsetVariable(variable);
    }

    @Override
    public String toString() {
        return String.format("UnsetVariable(%s)", variable);
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof UnsetVariable)) return false;
        UnsetVariable rhs = (UnsetVariable) o;
        return variable.equals(rhs.variable);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(variable).hashCode();
    }
}

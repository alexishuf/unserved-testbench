package br.ufsc.lapesd.unserved.testbench.io_composer;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public interface IOComposerTimes {
    /**
     * In replicated composition algorithms, an {@link IOComposerTimes} represents a thread
     * of execution. However, when an instance is duplicated, global() will be shared among
     * the first instance and the duplicate. When time intervals are incremented in any of these
     * instances, the side effects also apply to the global instace.
     *
     * When the instance is not created by duplication, it has its own global object, different
     * from itself. The global objects, however, have null as their global.
     *
     * @return global
     */
    IOComposerTimes getGlobal();
    Set<String> getNames();
    long getTotal(String name, TimeUnit timeUnit);
    List<Long> getTimes(String name, TimeUnit timeUnit);

    Set<String> getMemNames();
    long getMax(String name, long defaultValue);
    long getMax(String name) throws NoSuchElementException;
    double getAvg(String name) throws NoSuchElementException;
    List<Long> getMemSamples(String name);

    @Nonnull IOComposerTimes duplicate();
}

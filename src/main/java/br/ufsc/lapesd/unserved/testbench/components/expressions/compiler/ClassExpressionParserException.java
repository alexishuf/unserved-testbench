package br.ufsc.lapesd.unserved.testbench.components.expressions.compiler;

/**
 * Created by alexis on 10/18/16.
 */
public class ClassExpressionParserException extends Exception {
    public ClassExpressionParserException(String explanation) {
        super(explanation);
    }
}

package br.ufsc.lapesd.unserved.testbench.input;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFFormat;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * Delegates all methods to a delegate and close() is a no-op. Useful for reusing a RDFInput
 */
public class NonClosingRDFInput implements RDFInput {
    private final RDFInput delegate;

    public NonClosingRDFInput(RDFInput delegate) {
        this.delegate = delegate;
    }

    @Override
    public RDFFormat getFormat() {
        return delegate.getFormat();
    }

    @Override
    public Lang getLang() {
        return delegate.getLang();
    }

    @Override
    public Model getModel() throws IOException {
        return delegate.getModel();
    }

    @Override
    public Model takeModel() throws IOException {
        /* do not close the delegate */
        return delegate.getModel();
    }

    @Override
    public File toFile() throws IOException {
        return delegate.toFile();
    }

    @Override
    public String toURI() throws IOException {
        return delegate.toURI();
    }

    @Override
    public String getBaseURI() {
        return delegate.getBaseURI();
    }

    @Override
    public InputStream createInputStream() throws IOException {
        return delegate.createInputStream();
    }

    @Override
    public void close() throws IOException {
        /* no-op */
    }
}

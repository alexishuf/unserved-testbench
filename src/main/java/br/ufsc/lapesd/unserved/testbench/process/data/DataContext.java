package br.ufsc.lapesd.unserved.testbench.process.data;

import br.ufsc.lapesd.unserved.testbench.input.RDFInput;
import br.ufsc.lapesd.unserved.testbench.model.Variable;
import org.apache.jena.rdf.model.*;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collection;

public interface DataContext extends AutoCloseable {
    /**
     * Gets a Resource that is known by the Context. If the given resource
     * does not fits this need, a instance is obtained using it's URI or
     * AnonId. If a known resource cannot be obtained, null is returned.
     *
     * @param resource resource to check for. If null causes a null to be
     *                 returned.
     * @return a Resource known to the context that has the same URI or
     *         AnonId as resource, or null otherwise.
     */
    @Nullable
    Resource getResource(@Nullable Resource resource);
    @Nullable Resource getResource(@Nonnull String uri);

    /**
     * null-safe utility for getResource(resource).as(enhanced).
     */
    default @Nullable <T extends Resource>
    T getResource(@Nullable Resource resource, @Nonnull Class<T> enhanced) {
        Resource r = getResource(resource);
        return r == null ? null : (r.canAs(enhanced) ? r.as(enhanced) : null);
    }
    default @Nullable <T extends Resource>
    T getResource(@Nonnull String uri, @Nonnull Class<T> enhanced) {
        Resource r = getResource(uri);
        return r == null ? null : (r.canAs(enhanced) ? r.as(enhanced) : null);
    }

    void addStatement(@Nonnull Statement statement);
    default void addStatements(@Nonnull StmtIterator iterator) {
        while (iterator.hasNext())
            addStatement(iterator.next());
    }

    /**
     * Returns a Model with the current knowledge. Note that this model may
     * become out-of-sync with the actual inputs as soon as mutator method of
     * Context are called.
     *
     * @return unmodifiable model representing the current knowledge.
     */
    @Nonnull
    Model getUnmodifiableModel();

    /**
     * Returns a list of RDFInputs wich together represent the same data as
     * <code>getUnmodifiableModel()</code>.
     *
     * Implementations may use multiple files to avoid unecessary re-generation of files from an
     * getUnmodifiableModel() that is invariably changed.
     *
     * @return non-null and non-empty list of RDFInputs.
     */
    @Nonnull
    Collection<RDFInput> getInputs();

    /**
     * Return a DataContext which contains all data of this context.
     *
     * Changes on the returned context MUST not affect this context. Each implementation should
     * whenever possible maximize data sharing and avoid copying. The returned data context need
     * not be isolated from this context, that is, changes in this context may be reflected in
     * the returned contexts.
     *
     * Clients, however, should not expect changes to be visible. The request for a non-isolated
     * copy is a promise that the requester will not concurrently change this context and the copy.
     * The context implementation may use this promise for a lighter (or dumber) implementation.
     *
     * @return a copy of this context, on which changes on this context MAY be visible, but
     * changes on the copy MUST not be visible in this context.
     */
    @Nonnull
    DataContext createNonIsolatedCopy();

    /**
     * Removes any value bound to the variable.
     * FIXME: handle referenced resources are leaking in the model
     * @param var variable to unset.
     */
    void unsetVariable(@Nonnull Variable var);

    /**
     * Unsets the variable, if needed, and assigns literal as its value
     *
     * @param var Variable to have a literal value set.
     * @param representation Actual concrete representation of the value being set.
     * @param literal non-null literal to be used as value.
     */
    void setVariable(@Nonnull Variable var, @Nullable Resource representation, @Nonnull Literal literal);

    /**
     * Unsets the Variabel, if needed, and assigns resource as its value.
     * @param var Variable to be set.
     * @param representation Actual concrete representation of the value being set.
     * @param resource non-null resource to used as value. May not be known to the context,
     *                 but the result of getResource() must not be null.
     */
    void setVariable(@Nonnull Variable var, @Nullable Resource representation, @Nonnull Resource resource);

    @Override
    void close();
}

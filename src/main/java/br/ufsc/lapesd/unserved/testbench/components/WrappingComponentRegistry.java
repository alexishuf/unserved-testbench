package br.ufsc.lapesd.unserved.testbench.components;

import br.ufsc.lapesd.unserved.testbench.byte_renderers.Renderer;
import br.ufsc.lapesd.unserved.testbench.model.Variable;
import br.ufsc.lapesd.unserved.testbench.process.actions.ActionRunner;
import org.apache.jena.rdf.model.Resource;

public class WrappingComponentRegistry extends DefaultComponentRegistry {
    private final ComponentRegistry wrapped;

    public WrappingComponentRegistry(ComponentRegistry wrapped) {
        super(); //empty registry
        this.wrapped = wrapped;
    }

    @Override
    public ActionRunner createActionRunner(Resource anAction) {
        ActionRunner runner = super.createActionRunner(anAction);
        if (runner == null) runner = wrapped.createActionRunner(anAction);
        return runner;
    }

    @Override
    public Renderer createRenderer(Variable variable) {
        Renderer renderer = super.createRenderer(variable);
        if (renderer == null) renderer = wrapped.createRenderer(variable);
        return renderer;
    }
}

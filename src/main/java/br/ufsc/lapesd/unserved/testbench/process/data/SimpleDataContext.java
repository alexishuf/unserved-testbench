package br.ufsc.lapesd.unserved.testbench.process.data;

import br.ufsc.lapesd.unserved.testbench.input.RDFInput;
import br.ufsc.lapesd.unserved.testbench.input.RDFInputModel;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.IndexedVariable;
import br.ufsc.lapesd.unserved.testbench.iri.Unserved;
import br.ufsc.lapesd.unserved.testbench.model.Variable;
import br.ufsc.lapesd.unserved.testbench.util.jena.UncloseableGraph;
import com.google.common.base.Preconditions;
import org.apache.jena.graph.Graph;
import org.apache.jena.graph.compose.Delta;
import org.apache.jena.rdf.model.*;
import org.apache.jena.riot.RDFFormat;
import org.apache.jena.sparql.graph.UnmodifiableGraph;
import org.apache.jena.vocabulary.RDF;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collection;
import java.util.Collections;
import java.util.function.Function;

public class SimpleDataContext extends BaseDataContext {
    private static Logger logger = LoggerFactory.getLogger(SimpleDataContext.class);
    private final Model model;
    private final Function<Resource, Resource> anonResolver;

    public SimpleDataContext() {
        this.model = ModelFactory.createDefaultModel();
        anonResolver = Function.identity();
    }

    public SimpleDataContext(@Nonnull Model model,
                             @Nullable Function<Resource, Resource> anonResolver) {
        this.model = model;
        this.anonResolver = anonResolver;
    }

    @Override
    protected Model getModifiableModel() {
        return model;
    }

    @Override
    protected Resource baseResolveAnon(Resource anon) {
        return anonResolver.apply(anon);
    }

    @Override
    protected void baseAdd(@Nonnull Statement statement) {
        model.add(statement);
    }

    @Override
    protected void baseRemove(@Nonnull Statement statement) {
        model.remove(statement);
    }

    @Override
    protected void baseRemoveAll(@Nullable Resource subject, @Nullable Property predicate, @Nullable RDFNode object) {
        model.removeAll(subject, predicate, object);
    }

    @Override
    protected void baseClose() {
        model.close();
    }

    @Override
    public void unsetVariable(@Nonnull Variable var) {
        if (var instanceof IndexedVariable) {
            IndexedVariable iVar = (IndexedVariable) var;

        } else {
            Preconditions.checkNotNull(var);

            baseRemoveAll(var, Unserved.resourceValue, null);
            baseRemoveAll(var, Unserved.literalValue, null);
            baseRemoveAll(var, Unserved.valueRepresentation, null);
            baseRemove(stmt(var, RDF.type, Unserved.LiteralBoundVariable));
            baseRemove(stmt(var, RDF.type, Unserved.ResourceBoundVariable));
            baseRemove(stmt(var, RDF.type, Unserved.ValueBoundVariable));
            logger.debug("unsetVariable({})", var.toString());
        }
    }

    @Nonnull
    @Override
    public Model getUnmodifiableModel() {
        return ModelFactory.createModelForGraph(getUnmodifiableGraph());
    }

    private Graph getUnmodifiableGraph() {
        return new UncloseableGraph(
                new UnmodifiableGraph(model.getGraph()));
    }

    @Nonnull
    @Override
    public Collection<RDFInput> getInputs() {
        return Collections.singletonList(new RDFInputModel(model, RDFFormat.TURTLE));
    }

    @Nonnull
    @Override
    public DataContext createNonIsolatedCopy() {
        return new SimpleDataContext(
                ModelFactory.createModelForGraph(new Delta(getUnmodifiableGraph())), anonResolver);
    }
}

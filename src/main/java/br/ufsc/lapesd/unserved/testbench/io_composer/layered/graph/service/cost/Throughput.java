package br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.cost;

import javax.annotation.Nonnull;

public class Throughput implements CostParameter {
    private double value;
    public Throughput(double value) {
        this.value = value;
    }


    public static final String NAME = "Throughput";
    public static Throughput IDENTITY = new Throughput(0);

    @Nonnull
    @Override
    public String getName() {
        return NAME;
    }

    public double getValue() {
        return value;
    }

    @Override
    public CostParameter aggregateSequence(CostParameter right) {
        return aggregateParallel(right);
    }

    @Override
    public CostParameter aggregateParallel(CostParameter right) {
        return new Throughput(Math.min(this.value, ((Throughput)right).value));
    }

    @Override
    public int compare(CostParameter right) {
        return Double.compare(value, ((Throughput)right).value);
    }

    @Override
    public String toString() {
        return String.format("%s(%.3f)", NAME, value);
    }
}

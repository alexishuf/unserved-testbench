package br.ufsc.lapesd.unserved.testbench.util;

import com.google.common.base.Preconditions;
import org.apache.jena.rdf.model.*;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.*;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * Depth-first Resource iterator on a basic graph pattern with one variable (either subject or object).
 */
public class BFSBGPIterator implements Iterator<Resource> {
    private HashSet<Resource> visited = new HashSet<>();
    private LinkedHashSet<Resource> result = new LinkedHashSet<>();
    private Function<Resource, StmtIterator> stmtItSupplier;
    private Function<Statement, Resource> resourceGetter;
    private @Nullable Function<Resource, Set<Resource>> cache;


    public BFSBGPIterator(Collection<Resource> initial, Function<Resource, StmtIterator> supplier,
                          Function<Statement, Resource> resourceGetter, boolean withInitial,
                          @Nullable Function<Resource, Set<Resource>> cache) {
        this.stmtItSupplier = supplier;
        this.resourceGetter = resourceGetter;
        this.cache = cache;
        initial.forEach(withInitial ? result::add: this::visit);
    }

    public static class Builder {
        private final Model model;
        private final Collection<Resource> initial;
        private boolean withInitial = false;
        private @Nullable Function<Resource, Set<Resource>> cache = null;

        public Builder(Model model, Collection<Resource> initial) {
            this.model = model;
            this.initial = initial;
        }


        /**
         * Makes the iterator also return the initial nodes. If not set, these will
         * only be returned if there is a cycle leading back to them.
         */
        public Builder withInitial() {
            this.withInitial = true;
            return this;
        }

        /**
         * Use a cache to avoid iteration. The cache is not updated during iteration.
         *
         * The cache function MUST return null when a key is not available.
         */
        public Builder withCache(@Nullable Function<Resource, Set<Resource>> cache) {
            this.cache = cache;
            return this;
        }

        public BFSBGPIterator backward(Property property) {
            return new BFSBGPIterator(initial, r -> model.listStatements(null, property, r),
                    Statement::getSubject, withInitial, cache);
        }

        public BFSBGPIterator forward(Property property) {
            return new BFSBGPIterator(initial,
                    r -> model.listStatements(r, property, (RDFNode) null),
                    s -> s.getObject().isResource() ? s.getResource() : null,
                    withInitial, cache);
        }
    }

    public static Builder from(@Nonnull Resource initial) {
        Preconditions.checkArgument(initial.getModel() != null);
        return from(initial.getModel(), initial);
    }
    public static Builder from(@Nonnull Model model, @Nonnull Resource initial) {
        return new Builder(model, Collections.singletonList(initial));
    }
    public static Builder from(@Nonnull Resource initial, Property property) {
        Preconditions.checkArgument(initial.getModel() != null);
        return from(initial.getModel(), initial, property);
    }
    public static Builder from(@Nonnull Model model, @Nonnull Resource initial, Property property) {
        ArrayList<Resource> initials = new ArrayList<>();
        initial.listProperties(property).forEachRemaining(s -> {
            if (s.getObject().isResource()) initials.add(s.getResource());
        });
        return new Builder(model, initials);
    }
    public static Builder from(@Nonnull Model model, @Nonnull Collection<Resource> collection) {
        return new Builder(model, collection);
    }

    private Resource visit(Resource resource) {
        if (!visited.contains(resource)) {
            visited.add(resource);
            Set<Resource> cached = cache != null ? cache.apply(resource) : null;
            if (cached != null) {
                result.addAll(cached);
                visited.addAll(cached);
            } else {
                stmtItSupplier.apply(resource).forEachRemaining(s -> {
                    Resource r = resourceGetter.apply(s);
                    if (r != null) result.add(r);
                });
            }
        }
        return resource;
    }

    @Override
    public boolean hasNext() {
        return !result.isEmpty();
    }

    @Override
    public Resource next() {
        Resource next = result.iterator().next();
        result.remove(next);
        return visit(next);
    }

    @Nonnull
    public Stream<Resource> toStream() {
        return StreamSupport.stream(Spliterators.spliteratorUnknownSize(this,
                Spliterator.NONNULL|Spliterator.DISTINCT), false);
    }

    @Nonnull
    public <T extends Collection<Resource>> T toCollection(@Nonnull Supplier<T> supplier) {
        T collection = supplier.get();
        while (hasNext()) collection.add(next());
        return collection;
    }
}

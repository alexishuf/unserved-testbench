package br.ufsc.lapesd.unserved.testbench.io_composer.backward;

import br.ufsc.lapesd.unserved.testbench.components.DefaultComponentRegistry;
import br.ufsc.lapesd.unserved.testbench.composer.ComposerException;
import br.ufsc.lapesd.unserved.testbench.composer.Composition;
import br.ufsc.lapesd.unserved.testbench.composer.ReplicatedComposer;
import br.ufsc.lapesd.unserved.testbench.composer.impl.NullComposition;
import br.ufsc.lapesd.unserved.testbench.composer.impl.ReplicationCompositionWithPath;
import br.ufsc.lapesd.unserved.testbench.io_composer.AbstractIOComposer;
import br.ufsc.lapesd.unserved.testbench.io_composer.IOComposer;
import br.ufsc.lapesd.unserved.testbench.io_composer.IOCompositionResult;
import br.ufsc.lapesd.unserved.testbench.io_composer.graph.ChangeSet;
import br.ufsc.lapesd.unserved.testbench.io_composer.graph.CompositionWorkingGraph;
import br.ufsc.lapesd.unserved.testbench.io_composer.graph.CompositionWorkingGraphBuilder;
import br.ufsc.lapesd.unserved.testbench.io_composer.impl.*;
import br.ufsc.lapesd.unserved.testbench.io_composer.replicated.ReplicatedHelper;
import br.ufsc.lapesd.unserved.testbench.io_composer.state.Action;
import br.ufsc.lapesd.unserved.testbench.io_composer.state.ExecutionPath;
import br.ufsc.lapesd.unserved.testbench.io_composer.state.State;
import br.ufsc.lapesd.unserved.testbench.process.CompleteTask;
import br.ufsc.lapesd.unserved.testbench.process.Interpreter;
import br.ufsc.lapesd.unserved.testbench.process.UnservedPInterpreter;
import br.ufsc.lapesd.unserved.testbench.process.actions.ActionExecutionException;
import br.ufsc.lapesd.unserved.testbench.process.data.DataContext;
import br.ufsc.lapesd.unserved.testbench.process.data.DataContextDiff;
import br.ufsc.lapesd.unserved.testbench.process.data.SimpleDataContext;
import br.ufsc.lapesd.unserved.testbench.replication.ReplicatedExecution;
import br.ufsc.lapesd.unserved.testbench.replication.ReplicatedExecutionException;
import br.ufsc.lapesd.unserved.testbench.util.jena.UncloseableGraph;
import com.google.common.base.Preconditions;
import com.google.common.base.Stopwatch;
import es.usc.citius.hipster.algorithm.ADStarForward;
import es.usc.citius.hipster.algorithm.Hipster;
import es.usc.citius.hipster.graph.GraphSearchProblem;
import es.usc.citius.hipster.model.Node;
import es.usc.citius.hipster.model.function.impl.ScalarOperation;
import es.usc.citius.hipster.model.impl.ADStarNodeImpl;
import es.usc.citius.hipster.model.problem.SearchComponents;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.util.concurrent.TimeUnit;

public class ReplicatedBackwardIOADStar extends AbstractIOComposer
        implements ReplicatedComposer, IOComposer {
    private static Logger logger = LoggerFactory.getLogger(ReplicatedBackwardIOADStar.class);
    private Execution execution = null;
    @Nonnull private final State goal, initial;
    @Nonnull private final CompositionWorkingGraph<State, Double, Action> graph;
    @Nonnull private ExecutionPath<Action> executedPath = new ExecutionPath<>();
    private ADStarForward<Double, State, Double, ADStarNodeImpl<Double, State, Double>>.Iterator adStarIt;
    private ExecutionPath<Action> putaGambiarraPath;


    public ReplicatedBackwardIOADStar(@Nonnull IOComposerInput ci,
                                      @Nonnull CompositionWorkingGraph<State, Double, Action> graph,
                                      @Nonnull State initial, @Nonnull State goal) {
        super(createComposerInputDelta(ci));
        this.graph = graph;
        this.initial = initial;
        this.goal = goal;
    }

    private static IOComposerInput createComposerInputDelta(IOComposerInput ci) {
        IOComposerInputDelta delta = new IOComposerInputDelta(ci);
        ci.close(); //will close when delta closes
        return delta;
    }

    @Nonnull
    @Override
    public Composition run() throws ComposerException {
        if (execution == null) {
            logger.warn("Creating execution instance with interpreter from run().");
            execution = new Execution(this,
                    new UnservedPInterpreter(DefaultComponentRegistry.getInstance()));
        }
        if (putaGambiarraPath != null) {
            return toComposition(new ExecutionPath<>(putaGambiarraPath));
        } else {
            if (adStarIt == null) {
                SearchComponents<Double, State, Double> components = GraphSearchProblem
                        .startingFrom(initial)
                        .goalAt(goal)
                        .in(graph)
                        .takeCostsFromEdges()
                        .useScaleAlgebra(ScalarOperation.doubleMultiplicationOp())
                        .useHeuristicFunction(state -> 0.0) //FIXME use an actual heuristic
                        .components();
                adStarIt = Hipster.createADStar(components).iterator();
            }
            while (adStarIt.hasNext()) {
                ADStarNodeImpl<Double, State, Double> node = adStarIt.next();
                if (node.state().equals(goal))
                    return toComposition(node);
            }
        }
        //FIXME check for trivial compositions and return NullComposition
        throw new ComposerException("No composition path exists");
    }

    @Override
    public Interpreter.Task run(Interpreter interpreter) throws ReplicatedExecutionException {
        //TODO Get the interpreter from within the action runner
        Preconditions.checkState(execution == null);
        try (Execution ex = new Execution(this, interpreter)) {
            execution = ex;
            ex.resumeInterpretation();
            return new CompleteTask();
        } finally {
            execution = null;
        }
    }

    private <A, NodeType extends Node<A, State, NodeType>>
    Composition toComposition(Node<A, State, NodeType> goalNode) {
        Preconditions.checkState(execution != null);
        putaGambiarraPath = ReplicatedHelper.toFinePath(goalNode);
        return toComposition(new ExecutionPath<>(putaGambiarraPath));
    }

    private Composition toComposition(ExecutionPath<Action> path) {
        if (!path.removeHeadStrictly(executedPath))
            logger.debug("!path.removeHeadStrictly(executedPath)");
        return path.size() == 0 ? new NullComposition()
                : ReplicatedHelper.createReplicationComposition(ci, execution, path);
    }

    protected static class Execution implements ReplicatedExecution {
        @Nonnull private final IOComposerInputDelta composerInput;
        @Nonnull private final CompositionWorkingGraph<State, Double, Action> graph;
        @Nonnull private final State initial, goal;
        @Nonnull private final Interpreter interpreter;
        private ReplicatedBackwardIOADStar algorithm = null;
        @Nonnull private ExecutionPath<Action> lastCompPath = new ExecutionPath<>();
        @Nonnull private ExecutionPath<Action> executedPath = new ExecutionPath<>();
        @Nonnull private DefaultIOComposerTimes times;
        private ExecutionPath<Action> putaGambiarraPath = null;
        private DataContext dataContext = null;
        private DataContextDiff diff = null;
        private boolean closed = false;

        public Execution(@Nonnull ReplicatedBackwardIOADStar algorithm,
                         @Nonnull Interpreter interpreter) {
            this.composerInput = (IOComposerInputDelta) algorithm.ci;
            this.graph = algorithm.graph;
            this.initial = algorithm.initial;
            this.goal = algorithm.goal;
            this.algorithm = algorithm;
            this.interpreter = interpreter;
            this.times = new DefaultIOComposerTimes();
        }

        public Execution(@Nonnull IOComposerInputDelta composerInput,
                         @Nonnull CompositionWorkingGraph<State, Double, Action> graph,
                         @Nonnull State initial, @Nonnull State goal,
                         @Nonnull Interpreter interpreter,
                         @Nonnull ExecutionPath<Action> lastCompPath,
                         @Nonnull ExecutionPath<Action> executedPath,
                         @Nonnull DefaultIOComposerTimes times,
                         DataContextDiff diff,
                         ExecutionPath<Action> putaGambiarraPath) {
            this.composerInput = composerInput;
            this.graph = graph;
            this.initial = initial;
            this.goal = goal;
            this.interpreter = interpreter;
            this.lastCompPath = lastCompPath;
            this.executedPath = executedPath;
            this.times = times;
            this.diff = diff;
            this.putaGambiarraPath = putaGambiarraPath;
        }

        @Override
        @Nonnull
        public DefaultIOComposerTimes getTimes() {
            return times;
        }

        @Override
        public ReplicatedExecution duplicate() {
            Preconditions.checkState(!closed);
            return new Execution(new IOComposerInputDelta(composerInput), graph.duplicate(),
                    initial, goal, interpreter, lastCompPath, executedPath, times.duplicate(),
                    diff, putaGambiarraPath);
        }

        @Override
        public DataContext getDataContext() {
            Preconditions.checkState(!closed);
            if (dataContext == null) {
                Model model = ModelFactory.createModelForGraph(new UncloseableGraph(
                        composerInput.getSkolemizedUnion().getGraph()));
                dataContext = new SimpleDataContext(model, r -> {
                    Resource resolved = composerInput.getSkolemizerMap().resolve(r);
                    return resolved == null ? r : resolved;
                });
            }
            return dataContext;
        }

        @Override
        public void putDiff(DataContextDiff diff) {
            Preconditions.checkState(!closed);
            Preconditions.checkState(this.diff == null);
            this.diff = diff;
        }

        @Override
        public void resumeInterpretation() throws ReplicatedExecutionException {
            Preconditions.checkState(!closed);
            ChangeSet changeSet = null;
            Composition comp = null;

            try {
                if (diff != null)
                    changeSet = graph.applyDiff(diff, lastCompPath);
                getAlgorithm().executedPath.append(lastCompPath);
                try {
                    comp = timedRun();
                    if (diff != null)
                        composerInput.apply(diff); //definitive
                } catch (ComposerException e) {
                    ComposerException failException = changeSet == null ? e : null;
                    try {
                        if (changeSet != null) {
                            algorithm.executedPath.removeTailStrictly(lastCompPath);
                            logger.debug("Re-planning failed with diff, blacklisting", e);
                            changeSet.undo();
                            //TODO blacklist interaction
                            times.addTime("blacklisting", 0, TimeUnit.MICROSECONDS);
                            comp = timedRun();
                        }
                    } catch (ComposerException e2) {
                        failException = e2;
                    }
                    if (failException != null) {
                        throw new ReplicatedExecutionException("Can't plan any further",
                                failException);
                    }
                }

                try (DataContext dataContext = comp.isNull() ? null : comp.createDataContext()) {
                    if (comp.isNull()) {
                        interpreter.deliverResult(createResult());
                    } else {
                        assert dataContext != null;
                        assert comp instanceof ReplicationCompositionWithPath;
                        ReplicationCompositionWithPath repComp = (ReplicationCompositionWithPath) comp;
                        putaGambiarraPath = algorithm.putaGambiarraPath;
                        executedPath = algorithm.executedPath;
                        lastCompPath = repComp.getPath();
                        diff = null;
                        interpreter.interpret(dataContext, comp.getWorkflowRoot()).waitForDone();
                        if (repComp.getContainer().wasForked()) {
                            repComp.getContainer().join();
                        }
                    }
                } catch (ActionExecutionException | InterruptedException e) {
                    throw new ReplicatedExecutionException(e);
                }
            } finally {
                if (comp != null)
                    comp.close();
            }
        }

        private Composition timedRun() {
            Stopwatch watch = Stopwatch.createStarted();
            try {
                return getAlgorithm().run();
            } finally {
                times.addTime(planningTime, watch.elapsed(TimeUnit.NANOSECONDS),
                        TimeUnit.NANOSECONDS);
            }
        }

        private IOCompositionResult createResult() {
            return new DefaultIOCompositionResult(new IOComposerInputReference(composerInput), times);
        }

        @Override
        public void close() throws ComposerException {
            if (!closed) {
                if (algorithm != null)
                    algorithm.close();
                else
                    composerInput.close();
                closed = true;
            }
        }

        protected ReplicatedBackwardIOADStar getAlgorithm() {
            if (algorithm == null) {
                algorithm = new ReplicatedBackwardIOADStar(composerInput, graph, initial, goal);
                algorithm.execution = this;
                algorithm.putaGambiarraPath = putaGambiarraPath;
                algorithm.executedPath = new ExecutionPath<>(executedPath);
            }
            return algorithm;
        }
    }

    public static class Builder extends AbstractIOComposerBuilder {
        private CompositionWorkingGraphBuilder<State, Double, Action> graphBuilder = new BackwardCompositionWorkingGraphFromAccessor();

        public Builder setGraphBuilder(CompositionWorkingGraphBuilder<State, Double, Action> graphBuilder) {
            this.graphBuilder = graphBuilder;
            return this;
        }

        @Override
        protected IOComposer buildImpl() throws Exception {
            CompositionWorkingGraphBuilder.Result<State, Double, Action> result;
            result = graphBuilder.build(composerInput);
            return new ReplicatedBackwardIOADStar(composerInput,
                    result.getGraph(), result.getInitial(), result.getGoal());
        }
    }
}

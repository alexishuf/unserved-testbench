package br.ufsc.lapesd.unserved.testbench.restdesc_pragproof;

import br.ufsc.lapesd.unserved.testbench.composer.ComposerException;
import br.ufsc.lapesd.unserved.testbench.input.RDFInput;
import br.ufsc.lapesd.unserved.testbench.input.RDFInputFile;
import br.ufsc.lapesd.unserved.testbench.input.RDFInputModel;
import br.ufsc.lapesd.unserved.testbench.input.RDFInputString;
import br.ufsc.lapesd.unserved.testbench.reasoner.N3Reasoner;
import br.ufsc.lapesd.unserved.testbench.reasoner.N3ReasonerException;
import br.ufsc.lapesd.unserved.testbench.reasoner.N3ReasonerFactory;
import br.ufsc.lapesd.unserved.testbench.restdesc_pragproof.impl.Plan;
import br.ufsc.lapesd.unserved.testbench.restdesc_pragproof.impl.RESTdescComposerInputImpl;
import br.ufsc.lapesd.unserved.testbench.restdesc_pragproof.impl.RuleApplication;
import br.ufsc.lapesd.unserved.testbench.restdesc_pragproof.n3.N3Helper;
import br.ufsc.lapesd.unserved.testbench.restdesc_pragproof.n3.N3Model;
import br.ufsc.lapesd.unserved.testbench.restdesc_pragproof.n3.RemoveFormulaTransformer;
import br.ufsc.lapesd.unserved.testbench.util.jena.UncloseableGraph;
import ch.ethz.inf.vs.semantics.parser.elements.Formula;
import ch.ethz.inf.vs.semantics.parser.elements.N3Document;
import ch.ethz.inf.vs.semantics.parser.elements.N3Element;
import com.google.common.base.Preconditions;
import com.google.common.base.Stopwatch;
import org.apache.commons.io.IOUtils;
import org.apache.jena.graph.compose.Union;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.riot.RDFFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class RESTdescPragProof implements AutoCloseable {
    private static Logger logger = LoggerFactory.getLogger(RESTdescPragProof.class);
    private final RESTdescComposerInput ci;
    private final N3ReasonerFactory reasonerFactory;
    private ArrayList<Long> compositionTimes = new ArrayList<>();
    private long totalComposition = 0;
    private ArrayList<Long> reasoningTimes = new ArrayList<>();
    private long totalReasoning = 0;
    private ArrayList<Long> blacklistingTimes = new ArrayList<>();
    private long totalBlacklisting = 0;
    private ArrayList<Long> reasonerSpawnTimes = new ArrayList<>();
    private ArrayList<Long> reasonerSpawnAndParseTimes = new ArrayList<>();
    private long totalProofs = 0;
    private boolean cheatUnstableReasoner = false;
    private boolean measureReasonerSpawnTime = false;
    private boolean measureReasonerSpawnAndParseTime = false;
//    private File saveProofDir = null;
//    private int proofIndex;

    public static final String n3ReasonerSpawnAndParseTime = "n3ReasonerSpawnAndParse";
    public static final String n3ReasoningTime = "n3Reasoning";

    private RESTdescPragProof(@Nonnull RESTdescComposerInput composerInput,
                              @Nonnull N3ReasonerFactory reasonerFactory) {
        this.ci = composerInput;
        this.reasonerFactory = reasonerFactory;
    }

    public RDFInput execute() throws IOException, N3ReasonerException,
            RESTdescPragProofException {
        Model h = ModelFactory.createDefaultModel();
        Plan pre = null;
        resetCounters();
//        createSaveProofDir();
        while (true) {
            if (pre == null)
                pre = createPreProof(h);
            if (pre == null)
                throw new RESTdescPragProofException("No proof");
            if (pre.size() == 0)
                return new RDFInputModel(h, RDFFormat.TURTLE);

            RuleApplication application = pre.first();
            Model g = application.getConsequence();
            if (cheatUnstableReasoner)
                removeChainableExistententials(g);
//            saveProofFile(RDFInputModel.notOwningModel(g, RDFFormat.TURTLE), "-cons.ttl");

            Plan post = createPostProof(h, g);
            long nPost = (post == null ? pre : post).size();
            if (nPost >= pre.size()) {
                removeRule(ci, application);
                pre = null;
            } else {
                pre = post;
                h.add(g);
            }
        }
    }

    private void resetCounters() {
        compositionTimes.clear();
        totalComposition = 0;
        reasoningTimes.clear();
        totalReasoning = 0;
        blacklistingTimes.clear();
        totalBlacklisting = 0;
        reasonerSpawnTimes.clear();
        reasonerSpawnAndParseTimes.clear();
        totalProofs = 0;
    }

    private void removeChainableExistententials(Model model) {
        Pattern pattern = Pattern.compile(".*#rel\\d+|dummy\\d+");
        List<Statement> victims = new ArrayList<>();
        StmtIterator it = model.listStatements();
        while (it.hasNext()) {
            Statement stmt = it.next();
            boolean is = stmt.getSubject().isAnon() && stmt.getObject().isResource()
                    && stmt.getResource().isAnon()
                    && pattern.matcher(stmt.getPredicate().toString()).matches();
            if (is)
                victims.add(stmt);
        }
        model.remove(victims);
    }

    public RDFInput firstProof() throws IOException, N3ReasonerException {
        Stopwatch w = Stopwatch.createStarted();
        try {
            return runReasonerRaw(ci);
        } finally {
            takeCompositionTime(w);
        }
    }

    private void removeRule(RESTdescComposerInput ci, RuleApplication application)
            throws IOException, RESTdescPragProofException {
        Stopwatch watch = Stopwatch.createStarted();
        try {
            N3Model formulaModel = application.getProofModel()
                    .getFormulaModel(application.getRuleFormula());
            N3Element offendingElement = formulaModel.getRoot();
            Preconditions.checkArgument(offendingElement instanceof Formula);
            Formula formula = (Formula) offendingElement;

            N3Document doc = N3Helper.parseN3(ci.getRules());
            if (doc == null)
                throw new RESTdescPragProofException("Condensed rules file is not parsable.");
            RemoveFormulaTransformer transformer;
            transformer = new RemoveFormulaTransformer(doc, formula, formulaModel);
            if (transformer.getRemoved().isEmpty())
                throw new RESTdescPragProofException("Failed to locate rule marked for elimination.");
            N3Document transformed = transformer.getResult();
            ci.forceRules(new RDFInputString(transformed.toString(), RDFInput.N3));
        } finally {
            long us = watch.elapsed(TimeUnit.MICROSECONDS);
            blacklistingTimes.add(us);
            totalBlacklisting += us;
        }

    }

    private Plan createPreProof(Model h) throws IOException, N3ReasonerException {
        Stopwatch watch = Stopwatch.createStarted();
        ++totalProofs;
        try {
            N3Document proof = runReasoner(ci, RDFInputModel.notOwningModel(h, RDFFormat.TURTLE));
            return Plan.fromProof(proof);
        } finally {
//            System.err.printf("## pre %.3f\n", watch.elapsed(TimeUnit.MICROSECONDS) / 1000.0);
            takeCompositionTime(watch);
        }
    }


    private Plan createPostProof(Model h, Model g) throws IOException, N3ReasonerException {
        Stopwatch watch = Stopwatch.createStarted();
        ++totalProofs;
        try {
            Model u = ModelFactory.createModelForGraph(new Union(new UncloseableGraph(g.getGraph()), new UncloseableGraph(h.getGraph())));
            N3Document n3 = runReasoner(ci, RDFInputModel.notOwningModel(u, RDFFormat.TURTLE));
            return Plan.fromProof(n3);
        } finally {
//            System.err.printf("## pst %.3f\n", watch.elapsed(TimeUnit.MICROSECONDS) / 1000.0);
            takeCompositionTime(watch);
        }
    }

    private void takeCompositionTime(Stopwatch watch) {
        long us = watch.elapsed(TimeUnit.MICROSECONDS);
        if (measureReasonerSpawnTime)
            us -= reasonerSpawnTimes.get(reasonerSpawnTimes.size()-1);
        if (measureReasonerSpawnAndParseTime)
            us -= reasonerSpawnAndParseTimes.get(reasonerSpawnAndParseTimes.size()-1);
        totalComposition += us;
        compositionTimes.add(us);
    }

    private N3Document runReasoner(RESTdescComposerInput ci, RDFInput additional)
            throws IOException, N3ReasonerException {
        try (RESTdescComposerInputImpl working = new RESTdescComposerInputImpl(ci)) {
            working.addInput(additional);
            return runReasoner(working);
        }
    }

    private long spawnReasoner() throws N3ReasonerException, IOException {
        N3Reasoner reasoner = reasonerFactory.createReasoner();
        Stopwatch watch = Stopwatch.createStarted();
        long elapsed;
        try {
            String dummy = IOUtils.toString(reasoner.run());
        } finally {
            elapsed = watch.elapsed(TimeUnit.MICROSECONDS);
            reasonerSpawnTimes.add(elapsed);
        }
        return elapsed;
    }


    private long spawnReasonerAndParse(RESTdescComposerInput ci)
            throws N3ReasonerException, IOException {
        N3Reasoner reasoner = reasonerFactory.createReasoner();

        Stopwatch watch = Stopwatch.createStarted();
        File emptyFile = Files.createTempFile("unserved-testbench", ".n3").toFile();
        emptyFile.deleteOnExit();
        long elapsed;
        try (RDFInputFile empty = RDFInputFile.createOwningFile(emptyFile, RDFInput.N3)){
            for (RDFInput input : ci.getInputs()) reasoner.addInput(input);
            reasoner.addInput(ci.getRules());
            reasoner.setQueryFile(empty);
            reasoner.setExplainProof(true);

            String dummy = IOUtils.toString(reasoner.run());
        } finally {
            elapsed = watch.elapsed(TimeUnit.MICROSECONDS);
            reasonerSpawnAndParseTimes.add(elapsed);
        }
        return elapsed;
    }

    private void runReasonerRawForReal(RESTdescComposerInput ci, CompletableFuture<Long> time,
                                           CompletableFuture<RDFInput> proof)
            throws IOException, N3ReasonerException {
        N3Reasoner reasoner = reasonerFactory.createReasoner();
        for (RDFInput input : ci.getInputs()) reasoner.addInput(input);
        reasoner.addInput(ci.getRules());
        reasoner.setQueryFile(ci.getGoal());
        reasoner.setExplainProof(true);

        try {
            Stopwatch w = Stopwatch.createStarted();
            String result;
            try {
                result = IOUtils.toString(reasoner.run(), "UTF-8");
            } finally {
                time.complete(w.elapsed(TimeUnit.MICROSECONDS));
            }
            proof.complete(N3Helper.workaroundBadParser(
                    IOUtils.toInputStream(result, "UTF-8")));
        } finally {
            try {
                reasoner.waitFor();
            } catch (InterruptedException e) {
                logger.error("waitFor() Interrupted", e);
            }
        }
    }

    @FunctionalInterface
    private interface ReasonerTask {
        public void run() throws IOException, N3ReasonerException;

    }

    private RDFInput runReasonerRaw(RESTdescComposerInput ci) throws IOException, N3ReasonerException {
//        ++proofIndex;

        //shuffle proof, spawn and spawn+parse reasoner invocations
        List<ReasonerTask> tasks = new ArrayList<>();
        CompletableFuture<Long> reasoningTimeFuture = new CompletableFuture<>();
        CompletableFuture<RDFInput> proofFuture = new CompletableFuture<>();
        tasks.add(() -> runReasonerRawForReal(ci, reasoningTimeFuture, proofFuture));
        if (measureReasonerSpawnTime)
            tasks.add(this::spawnReasoner);
        if (measureReasonerSpawnAndParseTime)
            tasks.add(() -> spawnReasonerAndParse(ci));
        Collections.shuffle(tasks);

        for (ReasonerTask task : tasks) task.run();

        long us;
        RDFInput proof;
        try {
            proof = proofFuture.get();
            us = reasoningTimeFuture.get();
        } catch (InterruptedException | ExecutionException e) {
            throw new RuntimeException(e); //never thrown
        }

        long discount = 0;
        if (measureReasonerSpawnTime)
            discount = reasonerSpawnTimes.get(reasonerSpawnTimes.size()-1);
        if (measureReasonerSpawnAndParseTime)
            discount = reasonerSpawnAndParseTimes.get(reasonerSpawnAndParseTimes.size()-1);
        us -= discount;
        reasoningTimes.add(us);
        totalReasoning += us;

        return proof;
    }

    private N3Document runReasoner(RESTdescComposerInput ci) throws IOException, N3ReasonerException {
        try (RDFInput proof = runReasonerRaw(ci)) {
            N3Document doc = N3Helper.parseN3(proof);
            if (doc == null) throw new N3ReasonerException("Reasoner output is not valid N3.");
            return doc;
        }
    }

//    private void createSaveProofDir() throws IOException {
//        saveProofDir = Files.createTempDirectory("RESTdescPragProof").toFile();
//        proofIndex = -1; //incremented on runReasonerRaw() before every proof.
//        System.err.printf("Writing proofs to %s\n", saveProofDir.getAbsolutePath());
//    }
//
//    private void saveProofFile(RDFInput input, @Nonnull String suffix) throws IOException {
//        File file = new File(saveProofDir, String.format("%03d%s", proofIndex, suffix));
//        try (InputStream in = input.createInputStream()) {
//            IOUtils.copy(in, new FileOutputStream(file));
//        }
//    }

    @Override
    public void close() throws ComposerException {
        ci.close();
    }

    public double getTotalComposition(TimeUnit timeUnit) {
        return timeUnit.convert(totalComposition, TimeUnit.MICROSECONDS);
    }

    public List<Long> getCompositionTimes(TimeUnit timeUnit) {
        return compositionTimes.stream().map(us -> timeUnit.convert(us, TimeUnit.MICROSECONDS))
                .collect(Collectors.toList());
    }

    public double getTotalReasoning(TimeUnit timeUnit) {
        return timeUnit.convert(totalReasoning, TimeUnit.MICROSECONDS);
    }

    public List<Long> getReasoningTimes(TimeUnit timeUnit) {
        return reasoningTimes.stream().map(us -> timeUnit.convert(us, TimeUnit.MICROSECONDS))
                .collect(Collectors.toList());
    }

    public long getTotalBlacklisting(TimeUnit timeUnit) {
        return timeUnit.convert(totalBlacklisting, TimeUnit.MICROSECONDS);
    }

    public List<Long> getBlacklistingTimes(TimeUnit timeUnit) {
        return blacklistingTimes.stream().map(us -> timeUnit.convert(us, TimeUnit.MICROSECONDS))
                .collect(Collectors.toList());
    }

    public List<Long> getReasonerSpawnTimes(TimeUnit timeUnit) {
        return reasonerSpawnTimes.stream().map(us -> timeUnit.convert(us, TimeUnit.MICROSECONDS))
                .collect(Collectors.toList());
    }

    public List<Long> getReasonerSpawnAndParseTimes(TimeUnit tu) {
        return reasonerSpawnAndParseTimes.stream().map(us -> tu.convert(us, TimeUnit.MICROSECONDS))
                .collect(Collectors.toList());
    }

    public long getTotalProofs() {
        return totalProofs;
    }
    public void setCheatUnstableReasoner(boolean cheatUnstableReasoner) {
        this.cheatUnstableReasoner = cheatUnstableReasoner;
    }
    public boolean getCheatUnstableReasoner() {
        return cheatUnstableReasoner;
    }
    public boolean getMeasureReasonerSpawnTime() {
        return measureReasonerSpawnTime;
    }
    public void setMeasureReasonerSpawnTime(boolean measureReasonerSpawnTime) {
        this.measureReasonerSpawnTime = measureReasonerSpawnTime;
    }
    public boolean getMeasureReasonerSpawnAndParseTime() {
        return measureReasonerSpawnAndParseTime;
    }

    public void setMeasureReasonerSpawnAndParseTime(boolean measureReasonerSpawnAndParseTime) {
        this.measureReasonerSpawnAndParseTime = measureReasonerSpawnAndParseTime;
    }

    public static class Builder implements AutoCloseable {
        private final RESTdescComposerInput composerInput = new RESTdescComposerInputImpl();
        private N3ReasonerFactory reasonerFactory;
        private boolean closed = false;
        private boolean cheatUnstableReasoner = false;
        private boolean measureReasonerSpawnTime = false;
        private boolean measureReasonerSpawnAndParseTime = false;

        public Builder(N3ReasonerFactory reasonerFactory) {
            this.reasonerFactory = reasonerFactory;
        }

        public Builder withReasonerFactory(N3ReasonerFactory reasonerFactory) {
            this.reasonerFactory = reasonerFactory;
            return this;
        }

        public Builder setCheatUnstableReasoner(boolean value) {
            cheatUnstableReasoner = value;
            return this;
        }
        public Builder setMeasureReasonerSpawnTime(boolean value) {
            measureReasonerSpawnTime = value;
            return this;
        }

        public Builder setMeasureReasonerSpawnAndParseTime(boolean measureReasonerSpawnAndParseTime) {
            this.measureReasonerSpawnAndParseTime = measureReasonerSpawnAndParseTime;
            return this;
        }

        public Builder addInput(RDFInput input) {
            Preconditions.checkState(!closed);
            composerInput.addInput(input);
            return this;
        }

        public Builder withGoal(RDFInput goal) {
            Preconditions.checkState(!closed);
            composerInput.setGoal(goal);
            return this;
        }

        public RESTdescPragProof build() throws IOException {
            Preconditions.checkState(!closed);
            composerInput.initSkolemizedUnion(true, false);
            composerInput.initRules();
            RESTdescPragProof alg = new RESTdescPragProof(composerInput, reasonerFactory);
            alg.setCheatUnstableReasoner(cheatUnstableReasoner);
            alg.setMeasureReasonerSpawnTime(measureReasonerSpawnTime);
            alg.setMeasureReasonerSpawnAndParseTime(measureReasonerSpawnAndParseTime);
            closed = true;
            return alg;
        }

        @Override
        public void close() throws Exception {
            if (!closed) {
                closed = true;
                composerInput.close();
            }
        }
    }
}

package br.ufsc.lapesd.unserved.testbench.util;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.RDF;

import javax.annotation.Nonnull;
import java.util.*;

/**
 * Faster RDFList creation from collections or iterators
 */
public class RDFListWriter {
    @Nonnull
    public static Resource write(@Nonnull Model model, @Nonnull List<Resource> list) {
        ListIterator<Resource> it = list.listIterator(list.size());
        Resource node = RDF.nil;
        while (it.hasPrevious()) {
            /* If nodes are made instances of rdf:List, jena thinks they are invalid
             * (see {@link org.apache.jena.riot.writer.TurtleShell} line 460
             * (<code>validListElement()</code>). */
            node = model.createResource().addProperty(RDF.first, it.previous())
                    .addProperty(RDF.rest, node);
        }
        return node;
    }

    @Nonnull
    public static Resource write(@Nonnull Model model,
                                 @Nonnull Collection<Resource> collection) {
        if (collection instanceof List)
            return write(model, (List<Resource>)collection);
        ArrayList<Resource> list = new ArrayList<>(collection.size());
        list.addAll(collection);
        return write(model, list);
    }

    @Nonnull
    public static Resource write(@Nonnull Model model, @Nonnull Iterator<Resource> it) {
        ArrayList<Resource> list = new ArrayList<>();
        while (it.hasNext()) {
            list.add(it.next());
        }
        return write(model, list);
    }
}

package br.ufsc.lapesd.unserved.testbench.benchmarks.design1;

import org.apache.commons.lang.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.*;

public class SampledIntegerDistribution  implements Serializable {
    private final String name;
    private LinkedHashMap<Integer, Double> probabilities;
    private long seed;
    private Random random;

    public SampledIntegerDistribution(String name, long seed,
                                      LinkedHashMap<Integer, Double> probabilities) {
        this.name = name;
        this.seed = seed;
        this.random = new Random(seed);
        this.probabilities = probabilities;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return String.format("SampledDist[%s, %d]", name, seed);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;
        if (!(o instanceof SampledIntegerDistribution)) return false;
        SampledIntegerDistribution rhs = (SampledIntegerDistribution) o;
        return Objects.equals(getName(), rhs.getName())
                && Objects.equals(probabilities, rhs.probabilities)
                && getSeed() == rhs.getSeed();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getName()).append(getSeed())
                .append(probabilities).hashCode();
    }

    public void setSeed(long seed) {
        this.seed = seed;
    }

    public long getSeed() {
        return seed;
    }

    public int sample() {
        double random = this.random.nextDouble();
        double cumulative = 0;
        for (Map.Entry<Integer, Double> entry : probabilities.entrySet()) {
            cumulative += entry.getValue();
            if (random <= cumulative)
                return entry.getKey();
        }
        throw new RuntimeException("Random value too big.");
    }

    public <T> LinkedHashMap<Integer, List<T>> distribute(Collection<T> collection) {
        int colSize = collection.size();
        LinkedHashMap<Integer, Integer> sizes = new LinkedHashMap<>();
        TreeMap<Double, Integer> remainders = new TreeMap<>(Comparator.reverseOrder());
        for (Map.Entry<Integer, Double> entry : probabilities.entrySet()) {
            double proportional = entry.getValue() * colSize;
            int floor = (int) Math.floor(proportional);
            sizes.put(entry.getKey(), floor);
            remainders.put(proportional - floor, entry.getKey());
        }

        while (sizes.values().stream().reduce(Integer::sum).orElse(0) < colSize) {
            Iterator<Map.Entry<Double, Integer>> it = remainders.entrySet().iterator();
            Integer key = it.next().getValue();
            it.remove();
            sizes.put(key, sizes.get(key) + 1);
        }
        assert sizes.values().stream().reduce(Integer::sum).orElse(0) == colSize;

        LinkedHashMap<Integer, List<T>> map = new LinkedHashMap<>();
        Iterator<T> it = collection.iterator();
        for (Map.Entry<Integer, Integer> entry : sizes.entrySet()) {
            List<T> list = new ArrayList<>();
            for (int i = 0; i < entry.getValue(); i++) list.add(it.next());
            map.put(entry.getKey(), list);
        }

        return map;
    }
}

package br.ufsc.lapesd.unserved.testbench.iri;

import br.ufsc.lapesd.unserved.testbench.BackgroundProvider;
import br.ufsc.lapesd.unserved.testbench.model.UnservedFactory;
import br.ufsc.lapesd.unserved.testbench.util.ResourcesBackground;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.riot.RDFFormat;

import java.util.Collections;

/**
 * Constants with prefixes and IRIs of the unserved.ttl file.
 */
@BackgroundProvider(vocab = Unserved.ONTOLOGY_IRI)
public final class Unserved extends ResourcesBackground {
    public static final String ONTOLOGY_IRI =
            "https://alexishuf.bitbucket.io/2016/04/unserved/unserved.ttl";
    public static final String PREFIX = ONTOLOGY_IRI + "#";
    public static final String PREFIX_SHORT = "unserved";

    public Unserved() {
        super(Collections.singletonList(ImmutablePair.of("unserved/unserved.ttl",
                RDFFormat.TURTLE)), true);
    }

    private static Unserved instance = new Unserved();
    public static Unserved getInstance() {
        return instance;
    }

    private static Resource r(String localName) {
        return getInstance().getModel().createResource(PREFIX + localName);
    }
    private static Property p(String localName) {
        return getInstance().getModel().createProperty(PREFIX + localName);
    }

    public static final Resource Message = r("Message");
    public static final Resource Variable = r("Variable");
    public static final Resource Part = r("Part");
    public static final Resource NullBoundVariable = r("NullBoundVariable");
    public static final Resource ResourceBoundVariable = r("ResourceBoundVariable");
    public static final Resource LiteralBoundVariable = r("LiteralBoundVariable");
    public static final Resource ValueBoundVariable = r("ValueBoundVariable");
    public static final Resource Representation = r("Representation");
    public static final Resource Wanted = r("Wanted");
    public static final Resource When = r("When");
    public static final Resource Spontaneous = r("Spontaneous");
    public static final Resource Reaction = r("Reaction");
    public static final Resource Cardinality = r("Cardinality");
    public static final Resource Conditional = r("Conditional");
    public static final Resource ValuedCardinality = r("ValuedCardinality");
    public static final Resource Binding = r("Binding");
    public static final Resource IndexedValue = r("IndexedValue");
    public static final Resource ResourceBinding = r("ResourceBinding");
    public static final Resource PropertyBinding = r("PropertyBinding");

    public static final Property reactionTo = p("reactionTo");
    public static final Property reactionCardinality = p("reactionCardinality");
    public static final Property cardinalityValue = p("cardinalityValue");
    public static final Property condition = p("condition");
    public static final Property when = p("when");
    public static final Property from = p("from");
    public static final Property to = p("to");
    public static final Property variable = p("variable");
    public static final Property part = p("part");
    public static final Property partModifier = p("partModifier");
    public static final Property functionalClassification = p("functionalClassification");
    public static final Property nfp = p("nfp");
    public static final Property variableModifier = p("variableModifier");
    public static final Property bindingVariable = p("bindingVariable");
    public static final Property bindingProperty = p("bindingProperty");
    public static final Property bindingResource = p("bindingResource");


    public static final Resource Resource = r("Resource");
    public static final Resource spontaneous = r("spontaneous");
    public static final Resource none = r("none");
    public static final Resource one = r("one");
    public static final Resource many = r("many");

    public static final Property type = p("type");
    public static final Property valueRepresentation = p("valueRepresentation");
    public static final Property indexedValueRepresentation = p("indexedValueRepresentation");
    public static final Property representation = p("representation");
    public static final Property valueIndex = p("valueIndex");
    public static final Property literalValue = p("literalValue");
    public static final Property indexedLiteralValue = p("indexedLiteralValue");
    public static final Property resourceValue = p("resourceValue");
    public static final Property indexedResourceValue = p("indexedResourceValue");
    public static final Property functionValue = p("functionValue");


    private static boolean initialized = false;
    public static void init() {
        if (!initialized) {
            initialized = true;
            UnservedFactory.install();
        }
    }

    static {
        init();
    }
}

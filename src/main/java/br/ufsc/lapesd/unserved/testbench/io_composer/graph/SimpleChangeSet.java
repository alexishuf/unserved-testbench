package br.ufsc.lapesd.unserved.testbench.io_composer.graph;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.ListIterator;

public class SimpleChangeSet implements ChangeSet {
    public interface Mutation {
        void apply();
        void undo();
    }

    private final List<Mutation> mutations = new ArrayList<>();

    public SimpleChangeSet(@Nonnull Collection<Mutation> mutations) {
        this.mutations.addAll(mutations);
    }

    @Override
    public boolean isEmpty() {
        return mutations.isEmpty();
    }

    public void apply() {
        mutations.forEach(Mutation::apply);
    }

    @Override
    public void undo() {
        ListIterator<Mutation> it = mutations.listIterator(mutations.size());
        while (it.hasPrevious()) it.previous().undo();
    }
}

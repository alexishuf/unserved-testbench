package br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.discovery;

import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.IndexedVariable;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.Node;
import br.ufsc.lapesd.unserved.testbench.model.Variable;

import javax.annotation.Nonnull;

public class NodeOutput {
    public final @Nonnull Node node;
    public final @Nonnull IndexedVariable output;

    public NodeOutput(@Nonnull Node node, @Nonnull Variable output) {
        this.node = node;
        this.output = (output instanceof IndexedVariable) ? (IndexedVariable) output
                                                          : new IndexedVariable(output);
    }
    public NodeOutput(@Nonnull NodeOutput other, int index) {
        this(other.node, new IndexedVariable(other.output.variable, index));
    }

    @Nonnull
    public Node getNode() {
        return node;
    }
    @Nonnull
    public IndexedVariable getOutput() {
        return output;
    }

    @Override
    public String toString() {
        return String.format("%s:%s", node, output);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NodeOutput that = (NodeOutput) o;
        if (!node.equals(that.node)) return false;
        return output.equals(that.output);
    }

    @Override
    public int hashCode() {
        int result = node.hashCode();
        result = 31 * result + output.hashCode();
        return result;
    }
}

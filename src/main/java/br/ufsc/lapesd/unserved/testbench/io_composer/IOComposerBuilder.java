package br.ufsc.lapesd.unserved.testbench.io_composer;

import br.ufsc.lapesd.unserved.testbench.composer.ComposerBuilder;
import br.ufsc.lapesd.unserved.testbench.composer.ComposerBuilderException;
import org.apache.jena.rdf.model.Resource;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.IOException;

/**
 * Builder for {@link IOComposer} implementations.
 */
public interface IOComposerBuilder extends ComposerBuilder {

    /**
     * Set a variable as wanted.
     *
     * The variable must be defined in some of the provided inputs as a unserved:Variable.
     * The implementation will only check this on build(), though.
     *
     * @param variable a variable which the {@link IOComposer} composer will try to bind through
     *                 the returned workflow.
     * @return the builder
     */
    @Nonnull
    IOComposerBuilder markWantedVariable(@Nonnull Resource variable);

    @Nonnull
    IOComposerBuilder withBasicReasoning(boolean enabled);

    @Nonnull
    IOComposerBuilder withRulesFile(@Nonnull File rulesFile) throws IOException;

    @Nonnull
    IOComposerBuilder withInputHasNoSkolemized(boolean inputHasNoSkolemizedResources);

    @Override
    IOComposer build() throws ComposerBuilderException;
}

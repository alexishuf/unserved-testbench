package br.ufsc.lapesd.unserved.testbench.byte_renderers;

import br.ufsc.lapesd.unserved.testbench.components.DefaultComponentFactory;
import br.ufsc.lapesd.unserved.testbench.iri.Content;
import br.ufsc.lapesd.unserved.testbench.iri.UnservedX;
import br.ufsc.lapesd.unserved.testbench.model.Variable;
import com.google.common.base.Preconditions;
import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Renders unserved:RDF resources
 */
public class RDFRenderer implements Renderer {
    private static Logger logger = LoggerFactory.getLogger(RDFRenderer.class);

    @DefaultComponentFactory
    public static class Factory extends SimpleRendererFactory {
        public Factory() {
            super(RDFRenderer.class, RDFRenderer.Factory.class, getClasses(),
                    Collections.singletonList(UnservedX.RDF.RDF));
        }

        private static Collection<Resource> getClasses() {
            Model model = ModelFactory.createDefaultModel();
            List<Resource> list = new ArrayList<>();

            Resource resource;
            resource = model.createResource(XSDDatatype.XSDstring.getURI());
            list.add(resource.addProperty(RDF.type, RDFS.Datatype));
            resource = model.createResource(XSDDatatype.XSDhexBinary.getURI());
            list.add(resource.addProperty(RDF.type, RDFS.Datatype));
            resource = model.createResource(XSDDatatype.XSDbase64Binary.getURI());
            list.add(resource.addProperty(RDF.type, RDFS.Datatype));

            list.add(Content.ContentAsBase64);
            list.add(Content.ContentAsText);
            list.add(Content.ContentAsXML);

            return list;
        }
    }

    @Override
    public Render format(Variable variable) throws RendererException {
        Preconditions.checkArgument(variable.getValue() != null);
        Resource rep = variable.getValueRepresentation();
        Preconditions.checkArgument(rep != null);

        Charset charset = StringHelper.getCharsetFromRepresentation(rep);
        return StringHelper.getRender(variable.getValue(), charset);
    }


}

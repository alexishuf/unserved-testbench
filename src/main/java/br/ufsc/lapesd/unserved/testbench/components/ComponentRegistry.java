package br.ufsc.lapesd.unserved.testbench.components;

import br.ufsc.lapesd.unserved.testbench.byte_renderers.Renderer;
import br.ufsc.lapesd.unserved.testbench.model.Variable;
import br.ufsc.lapesd.unserved.testbench.process.actions.ActionRunner;
import org.apache.jena.rdf.model.Resource;

public interface ComponentRegistry extends AutoCloseable {
    ActionRunner createActionRunner(Resource anAction);
    Renderer createRenderer(Variable variable);
    void registerComponent(Class<? extends ComponentFactory> factory)
            throws ComponentRegistrationException;

    boolean unregisterComponent(Class<? extends ComponentFactory> factory);
}

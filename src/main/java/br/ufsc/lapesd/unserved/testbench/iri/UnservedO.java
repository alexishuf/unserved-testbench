package br.ufsc.lapesd.unserved.testbench.iri;

import br.ufsc.lapesd.unserved.testbench.util.ResourcesBackground;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.riot.RDFFormat;

import java.util.Collections;

public class UnservedO extends ResourcesBackground {
    public static final String IRI = "https://alexishuf.bitbucket.io/2016/04/unserved/unserved-o.ttl";
    public static final String PREFIX = IRI + "#";
    public static final String PREFIX_SHORT = "unserved-o";

    public UnservedO() {
        super(Collections.singletonList(ImmutablePair.of("unserved/unserved-o.ttl",
                RDFFormat.TURTLE)), true);
    }
    static UnservedO instance = new UnservedO();
    public static UnservedO getInstance() {
        return instance;
    }

    public static final Resource OperationRepresentation = getInstance().getModel().createResource(PREFIX + "OperationRepresentation");
    public static final Resource theOperation = getInstance().getModel().createResource(PREFIX + "theOperation");
    public static final Property operatesOn = getInstance().getModel().createProperty(PREFIX + "operatesOn");
}

package br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.lazy;

import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.IndexedNode;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.JumpNode;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.Node;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.ProviderSet;
import br.ufsc.lapesd.unserved.testbench.io_composer.state.Copies;
import br.ufsc.lapesd.unserved.testbench.model.Variable;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.util.*;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toCollection;
import static org.apache.commons.lang3.tuple.ImmutablePair.of;

public class SingleLeastServicesBackwardSetNodeProvider extends NaiveBackwardSetNodeProvider {
    private static Logger logger = LoggerFactory.getLogger(SingleLeastServicesBackwardSetNodeProvider.class);

    @Override
    protected Stream<ProtoNode> getProtoNodes(@Nonnull SetNode successor) {
        if (getUseConditions()) {
            logger.info("Delegating getProtoNodes() to super class");
            return super.getProtoNodes(successor);
        }
        ProviderSet ps = getProviderSetWithJumpNodes(successor.getNodes());
        if (ps == null) return Stream.empty();

        Set<IndexedNode> nodes = new HashSet<>();
        Copies copies = new Copies();
        Map<Node, List<Variable>> cache = new HashMap<>();
        Set<Variable> pending = new HashSet<>(ps.getTargets());
        while (!pending.isEmpty()) {
            Variable target = pending.iterator().next();
            ImmutablePair<Node, List<Variable>> best = ps.getProviders(target).stream()
                    .map(n -> of(n, cache.computeIfAbsent(n,
                            n2 -> ps.getTargets().stream()
                                    .filter(t -> ps.getProviders(t).contains(n2))
                                    .collect(toCollection(ArrayList::new)))))
                    .max(Comparator.comparing(p -> p.right.size())).orElse(null);
            assert best != null;
            nodes.add(IndexedNode.wrap(best.left));
            pending.removeAll(best.right);
            if (!(best.left instanceof JumpNode))
                best.right.forEach(t -> copies.add(ps.getOutput(t, best.left), t));
        }

        return Stream.of(new ProtoNode(nodes, copies));
    }
}

package br.ufsc.lapesd.unserved.testbench.byte_renderers;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.nio.charset.Charset;

public class SimpleRender implements Render {
    public SimpleRender(@Nonnull byte[] bytes, @Nonnull Type type, @Nullable Charset charset) {
        this.bytes = bytes;
        this.type = type;
        this.charset = charset;
    }

    public static SimpleRender fromString(String string, Charset charset) {
        return new SimpleRender(string.getBytes(charset), Type.STRING, charset);
    }

    public static SimpleRender fromBytes(byte[] bytes) {
        return new SimpleRender(bytes, Type.BINARY, null);
    }

    public static SimpleRender emptyString() {
        return new SimpleRender(new byte[0], Type.STRING, Charset.forName("UTF-8"));
    }
    public static SimpleRender empty() {
        return fromBytes(new byte[0]);
    }

    private final byte[] bytes;
    private final Type type;
    private final Charset charset;

    @Override
    public byte[] getBytes() {
        return bytes;
    }

    @Override
    public boolean isBinary() {
        return type == Type.BINARY;
    }

    @Override
    public boolean isString() {
        return type == Type.STRING;
    }

    @Override
    public Charset getCharset() {
        return charset;
    }
}

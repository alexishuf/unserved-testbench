package br.ufsc.lapesd.unserved.testbench.model.impl;

import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.atomic.VariableSpec;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.IndexedVariable;
import br.ufsc.lapesd.unserved.testbench.iri.Unserved;
import br.ufsc.lapesd.unserved.testbench.model.Variable;
import br.ufsc.lapesd.unserved.testbench.util.ImplementationByType;
import com.google.common.base.Preconditions;
import org.apache.jena.enhanced.EnhGraph;
import org.apache.jena.enhanced.EnhNode;
import org.apache.jena.enhanced.Implementation;
import org.apache.jena.graph.Node;
import org.apache.jena.rdf.model.*;
import org.apache.jena.vocabulary.RDF;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import static org.apache.jena.rdf.model.ResourceFactory.createTypedLiteral;

public class VariableImpl extends WithPartsImpl implements Variable {
    private VariableSpec spec = null;

    public static Implementation factory = new ImplementationByType(Unserved.Variable.asNode(),
            Arrays.asList(Unserved.representation.asNode(), Unserved.type.asNode(),
                    Unserved.literalValue.asNode(), Unserved.resourceValue.asNode())) {
        @Override
        public EnhNode wrap(Node node, EnhGraph eg) {
            return new VariableImpl(node, eg);
        }
    };

    public VariableImpl(Node n, EnhGraph m) {
        super(n, m);
    }

    @Nonnull
    @Override
    public Variable enhance(@Nonnull Resource other) {
        Preconditions.checkArgument(Objects.equals(getURI(), other.getURI()));
        return other.as(Variable.class);
    }

    @Override
    public boolean matches(Variable other) {
        if (other instanceof IndexedVariable)
            return ((IndexedVariable) other).getVariable().equals(this);
        return equals(other); //fallback
    }

    @Override
    public Resource getType() {
        Statement statement = getProperty(Unserved.type);
        return statement != null ? statement.getResource() : null;
    }

    @Override
    public Resource getRepresentation() {
        Statement statement = getProperty(Unserved.representation);
        return statement != null ? statement.getResource() : null;
    }

    @Override
    public Resource getValueRepresentation() {
        return getValueRepresentation(0);
    }

    @Override
    public Resource getResourceValue() {
        return getResourceValue(0);
    }

    @Override
    public Literal getLiteralValue() {
        return getLiteralValue(0);
    }

    @Override
    public RDFNode getValue() {
        return getValue(0);
    }

    @Override
    public Resource getValueRepresentation(int index) {
        StmtIterator it = listProperties(Unserved.valueRepresentation);
        while (it.hasNext()) {
            RDFNode object = it.nextStatement().getObject();
            if (!object.isResource()) continue;
            Resource container = object.asResource();
            Statement statement = container.getProperty(Unserved.valueIndex);
            if (statement == null) continue;
            if (statement.getInt() != index) continue;

            Statement statement2 = container.getProperty(Unserved.indexedValueRepresentation);
            return statement2 == null ? null : statement2.getResource();
        }
        Statement statement = getProperty(Unserved.valueRepresentation);
        return statement != null ? statement.getResource() : getRepresentation();
    }

    @Override
    public Resource getResourceValue(int index) {
        StmtIterator it = listProperties(Unserved.resourceValue);
        Resource implicit = null;
        while (it.hasNext()) {
            RDFNode object = it.nextStatement().getObject();
            if (!object.isResource()) continue;
            Resource container = object.asResource();
            Statement statement = container.getProperty(Unserved.valueIndex);
            if (statement == null) {
                if (index == 0) implicit = container;
                continue;
            }
            if (statement.getInt() != index) continue;

            Statement statement2 = container.getProperty(Unserved.indexedResourceValue);
            return statement2 == null ? null : statement2.getResource();
        }
        return index == 0 ? implicit : null;
    }

    @Override
    public Literal getLiteralValue(int index) {
        StmtIterator it = listProperties(Unserved.literalValue);
        Literal implicit = null;
        while (it.hasNext()) {
            RDFNode object = it.nextStatement().getObject();
            if (!object.isResource()) {
                if (index == 0) implicit = object.asLiteral();
                continue;
            }
            Resource container = object.asResource();
            Statement statement = container.getProperty(Unserved.valueIndex);
            if (statement == null) continue;
            if (statement.getInt() != index) continue;

            Statement statement2 = container.getProperty(Unserved.indexedLiteralValue);
            return statement2 == null ? null : statement2.getObject().asLiteral();
        }
        return index == 0 ? implicit : null;
    }

    @Override
    public RDFNode getValue(int index) {
        RDFNode value = getResourceValue(index);
        if (value != null) return value;
        value = getLiteralValue(index);
        return value;
    }

    @Override
    public boolean isValueBound() {
        return getResourceValue() != null || getLiteralValue() != null;
    }

    @Override
    public boolean isValueBound(int index) {
        return getValue(index) != null;
    }

    @Override
    public void unset(int index) {
        unset(index, Unserved.literalValue);
        unset(index, Unserved.resourceValue);
    }

    public void unset(int index, Property valueProp) {
        List<RDFNode> containers = getModel().listObjectsOfProperty(this, valueProp).toList();
        for (RDFNode containerNode : containers) {
            if (!containerNode.isResource()) continue;
            Resource container = containerNode.asResource();
            Statement statement = container.getProperty(Unserved.valueIndex);
            if (statement == null) {
                if (index == 0) {
                    getModel().remove(this, valueProp, container);
                    removeAll(Unserved.valueRepresentation);
                }
                continue;
            }
            if (statement.getInt() != index) continue;

            getModel().remove(container.listProperties());
            getModel().remove(statement);
            break;
        }
        if (index == 0) {
            getModel().remove(this, RDF.type, Unserved.ValueBoundVariable);
            getModel().remove(this, RDF.type, Unserved.LiteralBoundVariable);
            getModel().remove(this, RDF.type, Unserved.ResourceBoundVariable);
        }
    }
    
    public void set(int index, @Nullable Resource representation, @Nonnull Literal literal) {
        Preconditions.checkArgument(index >= -1);
        if (index == -1) {
            unset(0);
            addProperty(Unserved.literalValue, literal);
            if (representation != null)
                addProperty(Unserved.valueRepresentation, representation);
        } else {
            unset(index);
            Resource container = getModel().createResource()
                    .addProperty(Unserved.valueIndex, createTypedLiteral(index))
                    .addProperty(Unserved.indexedLiteralValue, literal);
            if (representation != null)
                container.addProperty(Unserved.indexedValueRepresentation, representation);
            addProperty(Unserved.literalValue, container);
        }
        if (index == 0) {
            addProperty(RDF.type, Unserved.ValueBoundVariable);
            addProperty(RDF.type, Unserved.LiteralBoundVariable);
        }
    }
    
    public void set(int index, @Nullable Resource representation, @Nonnull Resource resource) {
        Preconditions.checkArgument(index >= -1);
        if (index == -1) {
            unset(0);;
            addProperty(Unserved.resourceValue, resource);
            if (representation != null)
                addProperty(Unserved.valueRepresentation, representation);
        } else {
            unset(index);
            Resource container = getModel().createResource()
                    .addProperty(Unserved.valueIndex, createTypedLiteral(index))
                    .addProperty(Unserved.indexedResourceValue, resource);
            if (representation != null)
                container.addProperty(Unserved.indexedValueRepresentation, representation);
            addProperty(Unserved.resourceValue, container);
        }
        if (index == 0) {
            addProperty(RDF.type, Unserved.ValueBoundVariable);
            addProperty(RDF.type, Unserved.ResourceBoundVariable);
        }
    }

    @Override
    public VariableSpec asSpec() {
        return spec == null ? spec = new VariableSpec(this) : spec;
    }
}

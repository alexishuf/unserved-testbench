package br.ufsc.lapesd.unserved.testbench.util;

import org.apache.jena.ext.com.google.common.base.Supplier;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Helper for skolemizing whole models. Uses {@link SkolemizerMap} internally.
 */
public class Skolemizer {
    private static final String DEFAULT_SK_PREFIX = "sk";
    private final Supplier<SkolemizerMap> skMapSupplier;

    public Skolemizer(Supplier<SkolemizerMap> supplier) {
        this.skMapSupplier = supplier;
    }
    public Skolemizer() {
        this.skMapSupplier = SkolemizerMap::new;
    }

    public SkolemizerMap skolemizeModels(Model output,  Model... inputs) {
        return skolemizeModels(output, DEFAULT_SK_PREFIX, Arrays.asList(inputs));
    }
    public SkolemizerMap skolemizeModels(Model output, Collection<Model> inputs) {
        return skolemizeModels(output, DEFAULT_SK_PREFIX, inputs);
    }
    public SkolemizerMap skolemizeModels(Model output, String skolemPrefix, Model... inputs) {
        return skolemizeModels(output, skolemPrefix, Arrays.asList(inputs));
    }
    public SkolemizerMap skolemizeModels(Model output, String skolemPrefix, Collection<Model> inputs) {
        HashMap<String, String> prefixes = new HashMap<>();

        SkolemizerMap skMap = skMapSupplier.get().addModel(output);
        inputs.forEach(skMap::addModel);

        for (Model input : inputs) {
            mergePrefixes(prefixes, input.getNsPrefixMap());
            input.listStatements().forEachRemaining(s -> {
                Resource subj = s.getSubject();
                RDFNode obj = s.getObject();
                if (s.getSubject().isAnon())
                    subj = skMap.get(s.getSubject().asResource());
                if (s.getObject().isAnon())
                    obj = skMap.get(s.getObject().asResource());
                output.add(subj, s.getPredicate(), obj);
            });
        }

        output.setNsPrefixes(prefixes);
        output.setNsPrefix(skolemPrefix, skMap.getPrefix());
        return skMap;
    }

    private void mergePrefixes(Map<String, String> prefixes,
                               Map<String, String> newPrefixes) {
        for (Map.Entry<String, String> pair : newPrefixes.entrySet()) {
            if (prefixes.containsValue(pair.getValue())) continue;
            String key = pair.getKey();
            if (prefixes.containsKey(key)) {
                int suffix = 1;
                while (prefixes.containsKey(key + suffix)) ++suffix;
                key = key + suffix;
            }
            prefixes.put(key, pair.getValue());
        }
    }
}

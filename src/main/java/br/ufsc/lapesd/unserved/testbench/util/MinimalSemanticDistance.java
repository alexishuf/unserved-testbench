package br.ufsc.lapesd.unserved.testbench.util;

import br.ufsc.lapesd.unserved.testbench.model.Variable;
import br.ufsc.lapesd.unserved.testbench.util.MatchTable.Relation;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.SetMultimap;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.RDFS;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.*;

import static br.ufsc.lapesd.unserved.testbench.util.MatchTable.Relation.*;

public class MinimalSemanticDistance {
    private List<Relation> prefs = new ArrayList<>(
            Arrays.asList(SAME, SUPERCLASS, SUBCLASS));

    public MinimalSemanticDistance forbiding(Relation relation) {
        prefs.remove(relation);
        return this;
    }

    public MinimalSemanticDistance withPreferences(Relation... preferences) {
        this.prefs = new ArrayList<>(Arrays.asList(preferences));
        return this;
    }
    public MinimalSemanticDistance withPreferences(List<Relation> preferences) {
        this.prefs = new ArrayList<>(preferences);
        return this;
    }


    @Nullable
    public Variable select(@Nonnull Collection<Variable> candidates, @Nonnull Variable variable) {
        if (candidates.isEmpty()) return null;
        boolean up = prefs.contains(SUPERCLASS), down = prefs.contains(SUBCLASS);
        Resource vType = variable.getType();
        HashMap<Relation, Variable> results = new HashMap<>();

        if (vType == null) {
            if (prefs.contains(SAME)) {
                Variable candidate = candidates.stream().filter(c -> c.getType() == null)
                        .findFirst().orElse(null);
                if (candidate != null) results.put(SAME, candidate);
            }
            if (down)
                results.put(SUBCLASS, candidates.iterator().next());
        } else {
            SetMultimap<Resource, Variable> typeToVar = HashMultimap.create();
            candidates.stream()
                    .filter(c -> Objects.equals(c.getRepresentation(), variable.getRepresentation()))
                    .forEach(c -> typeToVar.put(c.getType(), c));
            if (prefs.contains(SAME) && typeToVar.containsKey(vType))
                results.put(SAME, typeToVar.get(vType).iterator().next());

            LinkedList<ImmutablePair<Resource, Integer>> queue = new LinkedList<>();
            HashSet<Resource> visited = new HashSet<>();
            if (up) addNextUp(queue, vType);
            if (down) addNextDown(queue, vType);
            while (!queue.isEmpty()) {
                ImmutablePair<Resource, Integer> st = queue.remove();
                if (visited.contains(st.getKey())) continue;
                visited.add(st.getKey());

                if (typeToVar.containsKey(st.getKey())) {
                    results.put(st.getValue() > 0 ? SUPERCLASS : SUBCLASS,
                            typeToVar.get(st.getKey()).iterator().next());
                    queue.removeIf(p -> p.getValue().equals(st.getValue()));
                }

                if (st.getValue() == 1) addNextUp(queue, st.getKey());
                else if (st.getValue() == -1) addNextDown(queue, st.getKey());
            }
        }

        for (Relation relation : prefs) {
            Variable selected = results.getOrDefault(relation, null);
            if (selected != null) return selected;
        }
        return null;
    }

    private void addNextUp(Queue<ImmutablePair<Resource, Integer>> queue, Resource resource) {
        resource.listProperties(RDFS.subClassOf).forEachRemaining(s -> {
            RDFNode o = s.getObject();
            if (o.isResource()) queue.add(ImmutablePair.of(o.asResource(), 1));
        });
    }

    private void addNextDown(Queue<ImmutablePair<Resource, Integer>> queue, Resource resource) {
        resource.getModel().listStatements(null, RDFS.subClassOf, resource)
                .forEachRemaining(s -> queue.add(ImmutablePair.of(s.getSubject(), -1)));
    }
}

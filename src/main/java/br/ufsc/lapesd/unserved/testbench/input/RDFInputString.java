package br.ufsc.lapesd.unserved.testbench.input;

import org.apache.commons.io.IOUtils;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.riot.RDFFormat;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.io.OutputStream;

public class RDFInputString extends AbstractRDFInput {
    private String string;

    public RDFInputString(String string, @Nonnull RDFFormat format) {
        super(format);
        this.string = string;
    }

    @Override
    protected Model loadModel() throws IOException {
        Model model = ModelFactory.createDefaultModel();
        model.read(IOUtils.toInputStream(string), null, getLang().getName());
        return model;
    }

    @Override
    protected void writeData(OutputStream outputStream) throws IOException {
        outputStream.write(string.getBytes("UTF-8"));
    }
}

package br.ufsc.lapesd.unserved.testbench.process.actions.mock;

import br.ufsc.lapesd.unserved.testbench.components.SimpleActionRunnerFactory;
import br.ufsc.lapesd.unserved.testbench.iri.UnservedP;
import br.ufsc.lapesd.unserved.testbench.process.Context;
import br.ufsc.lapesd.unserved.testbench.process.actions.ActionExecutionException;
import br.ufsc.lapesd.unserved.testbench.process.actions.ActionRunner;
import br.ufsc.lapesd.unserved.testbench.process.model.Send;
import com.google.common.base.Preconditions;
import org.apache.jena.rdf.model.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MockSendRunner implements ActionRunner {
    private static Logger logger = LoggerFactory.getLogger(MockSendRunner.class);

    public static class Factory extends SimpleActionRunnerFactory {
        public Factory() {
            super(MockSendRunner.class, MockSendRunner.Factory.class,
                    new UnservedP().getModel().createResource(UnservedP.Send.getURI()));
        }
    }

    @Override
    public void run(Resource resource, Context context) throws ActionExecutionException {
        Preconditions.checkArgument(resource.canAs(Send.class));
        Send send = resource.as(Send.class);
        new MockSendReceiveContext(context.runners(), send.getMessage());
        logger.debug("Sent {}", send.getMessage());
    }
}

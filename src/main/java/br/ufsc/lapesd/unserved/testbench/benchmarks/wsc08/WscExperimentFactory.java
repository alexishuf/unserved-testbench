package br.ufsc.lapesd.unserved.testbench.benchmarks.wsc08;

import br.ufsc.lapesd.unserved.testbench.AlgorithmFamily;
import br.ufsc.lapesd.unserved.testbench.benchmarks.experiment.Experiment;
import br.ufsc.lapesd.unserved.testbench.benchmarks.experiment.ExperimentFactory;
import br.ufsc.lapesd.unserved.testbench.benchmarks.experiment.Factors;

public class WscExperimentFactory implements ExperimentFactory {
    private boolean loadWscFiles;

    public WscExperimentFactory() {
        this(false);
    }

    public WscExperimentFactory(boolean loadWscFiles) {

        this.loadWscFiles = loadWscFiles;
    }

    @Override
    public Experiment createExperiment(Factors factors) {
        WscFactors f = (WscFactors) factors;
        if (f.getAlgorithm().getFamily() == AlgorithmFamily.RESTdescPP) {
            throw new UnsupportedOperationException("RESTdesc PP is too slow for WSC08!");
        } else if (f.getAlgorithm().getFamily() == AlgorithmFamily.IOGraphPath) {
            return new ComposeWorkflowAppWscExperiment(f, loadWscFiles);
        }
        throw new UnsupportedOperationException();
    }
}

package br.ufsc.lapesd.unserved.testbench.io_composer.layered;

import br.ufsc.lapesd.unserved.testbench.composer.impl.ReplicationCompositionWithPath;
import br.ufsc.lapesd.unserved.testbench.io_composer.impl.IOComposerInput;
import br.ufsc.lapesd.unserved.testbench.io_composer.state.ExecutionPath;
import br.ufsc.lapesd.unserved.testbench.replication.action.ReplicationContainer;

import javax.annotation.Nonnull;

public class ReplicationCompositionWithPathAndEdge<Action, Edge>
        extends ReplicationCompositionWithPath<Action> {
    private final Edge edge;

    public ReplicationCompositionWithPathAndEdge(@Nonnull ReplicationContainer container,
                                                 @Nonnull IOComposerInput composerInput,
                                                 @Nonnull ExecutionPath<Action> path, Edge edge) {
        super(container, composerInput, path);
        this.edge = edge;
    }

    public Edge getEdge() {
        return edge;
    }
}

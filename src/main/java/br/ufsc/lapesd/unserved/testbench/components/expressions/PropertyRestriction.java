package br.ufsc.lapesd.unserved.testbench.components.expressions;

import com.google.common.base.Preconditions;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.OWL2;

import javax.annotation.Nonnull;
import java.util.Collections;
import java.util.Set;

public class PropertyRestriction extends AbstractClassExpression {
    public enum RestrictionType {
        SOME_VALUES_FROM {
            @Override
            public boolean hasRangeClass() {
                return true;
            }
        },
        ALL_VALUES_FROM {
            @Override
            public boolean hasRangeClass() {
                return true;
            }
        },
        HAS_VALUE,
        HAS_SELF;

        public boolean hasRangeClass() {
            return false;
        }

        public static RestrictionType fromProperty(Property property) {
            if (property.equals(OWL2.someValuesFrom))
                return SOME_VALUES_FROM;
            else if (property.equals(OWL2.allValuesFrom))
                return ALL_VALUES_FROM;
            else if (property.equals(OWL2.hasValue))
                return HAS_VALUE;
            else if (property.equals(OWL2.hasSelf))
                return HAS_SELF;
            else
                return null;
        }

    }
    @Nonnull private final RestrictionType restrictionRestrictionType;

    @Nonnull private final Property onProperty;
    private final ClassExpression rangeClass;
    private final RDFNode value;
    private final boolean hasSelf;

    public PropertyRestriction(@Nonnull RestrictionType type, @Nonnull Property onProperty,
                               @Nonnull Set<Resource> names,
                               @Nonnull ClassExpression rangeClass) {
        super(ClassExpression.Type.PropertyRestriction, names);
        Preconditions.checkArgument(type == RestrictionType.ALL_VALUES_FROM
                || type == RestrictionType.SOME_VALUES_FROM);
        this.restrictionRestrictionType = type;
        this.onProperty = onProperty;
        this.rangeClass = rangeClass;
        this.value = null;
        this.hasSelf = false;
    }

    public PropertyRestriction(@Nonnull RestrictionType type, @Nonnull Property onProperty,
                               @Nonnull Set<Resource> names, @Nonnull RDFNode value) {
        super(ClassExpression.Type.PropertyRestriction, names);
        Preconditions.checkArgument(type == RestrictionType.HAS_VALUE);
        this.restrictionRestrictionType = type;
        this.onProperty = onProperty;
        this.rangeClass = null;
        this.value = value;
        this.hasSelf = false;
    }

    public PropertyRestriction(@Nonnull RestrictionType type, @Nonnull Property onProperty,
                               @Nonnull Set<Resource> names, boolean hasSelf) {
        super(Type.PropertyRestriction, names);
        Preconditions.checkArgument(type == RestrictionType.HAS_SELF);
        this.restrictionRestrictionType = type;
        this.onProperty = onProperty;
        this.rangeClass = null;
        this.value = null;
        this.hasSelf = hasSelf;
    }

    @Nonnull
    public RestrictionType getRestrictionType() {
        return restrictionRestrictionType;
    }

    @Nonnull
    public Property getOnProperty() {
        return onProperty;
    }

    public ClassExpression getRangeClass() {
        Preconditions.checkState(getRestrictionType().hasRangeClass());
        return rangeClass;
    }

    public RDFNode getValue() {
        Preconditions.checkState(getRestrictionType() == RestrictionType.HAS_VALUE);
        return value;
    }

    public boolean hasSelf() {
        Preconditions.checkState(getRestrictionType() == RestrictionType.HAS_SELF);
        return hasSelf;
    }

    @Override
    public Set<ClassExpression> getChildren() {
        return rangeClass != null ? Collections.singleton(rangeClass) : Collections.emptySet();
    }

    @Override
    public int hashCode() {
        RestrictionType t = getRestrictionType();
        HashCodeBuilder builder = new HashCodeBuilder().append(t)
                .append(getChildren()).append(getOnProperty());
        if (t == RestrictionType.HAS_SELF)
            builder.append(hasSelf());
        if (t == RestrictionType.HAS_VALUE)
            builder.append(getValue());
        if (t == RestrictionType.ALL_VALUES_FROM || t == RestrictionType.SOME_VALUES_FROM)
            builder.append(getRangeClass());
        return builder.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) return true;
        if (!(o instanceof PropertyRestriction)) return false;
        PropertyRestriction rhs = (PropertyRestriction) o;
        RestrictionType t = getRestrictionType();
        boolean ok = t == rhs.getRestrictionType()
                && getChildren().equals(rhs.getChildren())
                && getOnProperty().equals(rhs.getOnProperty());
        if (!ok) return false;
        if (t == RestrictionType.HAS_SELF)
            return hasSelf() == rhs.hasSelf();
        if (t == RestrictionType.HAS_VALUE)
            return getValue().equals(rhs.getValue());
        if (t == RestrictionType.ALL_VALUES_FROM || t == RestrictionType.SOME_VALUES_FROM)
            return getRangeClass().equals(rhs.getRangeClass());
        assert false;
        return false;
    }
}

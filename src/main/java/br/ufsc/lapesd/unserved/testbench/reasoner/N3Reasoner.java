package br.ufsc.lapesd.unserved.testbench.reasoner;

import br.ufsc.lapesd.unserved.testbench.input.RDFInput;
import br.ufsc.lapesd.unserved.testbench.reasoner.impl.EYEProcess;
import org.apache.jena.riot.Lang;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * An N3 reasoner that takes some input files, an optional query file and returns either a
 * proof explanation of the query, statements involved in proving the query, or the deductive
 * closure of the input data.
 *
 */
public interface N3Reasoner {
    /**
     * List, ordered from most to least preferred, supported formats for input. By input
     * understand providing the file or IRI directly during reasoner invocation.
     */
    List<Lang> supportedInputLanguages();

    /**
     * List, ordered from most to least preferred, supported formats for download by the reasoner.
     * The reasoner downloads (and parses) files if an input N3 files contains triples with
     * log:semantics in the antecedent of a rule.
     */
    List<Lang> supportedDownloadLanguages();

    /**
     * Add a File with the given RDF format to the queue of files to load. The input will only
     * be loaded when run() is called, so errors due to bad files will not be thrown from
     * this method.
     *
     * @param input   An RDFFile to add
     */
    void addInput(RDFInput input) throws IOException;

    /**
     * Set a N3 file from where to load the query to be proven. Setting this automatically
     * disables deductive closure computation.
     *
     * @param file RDFInput on N3 language to be read during run().
     */
    void setQueryFile(@Nonnull RDFInput file);

    /**
     * Discards the previously set query file and instructs the reasoner to compute the deductive
     * closure (all triples that can be deduced from the input data).
     */
    void queryDeductiveClosure();

    /**
     * Indicates whether the reasoner will output an explained (using N3 proof vocabulary) proof
     * of the query or just the RDF triples involved.
     *
     * @return true iff the flag is enabled.
     */
    boolean getExplainProof();

    /**
     * Sets the explain proof flag.
     */
    void setExplainProof(boolean explainProof);

    /**
     * Runs the reasoner as configured. Reasoner output, in N3 format can be read from the
     * given stream.
     * <p>
     * Note that file-related failures may occur on this method call or they may be occur
     * in the reasoner implementation, being returned by getError().
     *
     * The reasoner execution is asynchronous, the InputStream will be streamed, but whether
     * triples are streamed or dumped at the end of the reasoning process is dependent on
     * the implementation. If this reasoner is already running, a IllegalStateException will
     * be thrown.
     *
     * @return A stream of N3 data with the reasoner output.
     */
    InputStream run() throws N3ReasonerException;

    /**
     * Indicates if the reasoner is still processing after a previous run() call.
     *
     * @return true iff the reasoner is processing.
     */
    boolean isRunning();

    /**
     * Returns an exception if the processing started by run() failed. Interpretation of this
     * exception is dependent on the reasoner implementation.
     */
    N3ReasonerException getError();

    /**
     * Aborts the reasoner, if running. If the reasoner is not running does nothing.
     *
     * This may abort the reasoner in an unfriendly way, depending on the implementation.
     * Abrupt abortions will not affect new reasoner instances, but may produce invalid RDF
     * data on the InputStream returned from run().
     *
     * This is not asynchronous, you may chain a waitFor() call.
     */
    EYEProcess abort();

    /**
     * Blocks until the processing asynchronously started by the run() call terminates. If the
     * reasoner is not running, returns immediately.
     *
     * @throws InterruptedException
     */
    void waitFor() throws InterruptedException, N3ReasonerException;

    /**
     * Blocks for at most timeout units of time until the processing asynchronously started by
     * the run() call terminates. If the reasoner is not running, returns immediately.
     *
     * @throws InterruptedException
     */
    boolean waitFor(long timeout, TimeUnit unit) throws InterruptedException, N3ReasonerException;
}
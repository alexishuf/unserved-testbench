package br.ufsc.lapesd.unserved.testbench.io_composer.layered;

import br.ufsc.lapesd.unserved.testbench.io_composer.IOComposer;
import br.ufsc.lapesd.unserved.testbench.io_composer.impl.AbstractIOComposerBuilder;
import br.ufsc.lapesd.unserved.testbench.io_composer.impl.DefaultIOComposerTimes;
import br.ufsc.lapesd.unserved.testbench.io_composer.impl.IOComposerInput;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.LayeredGraph;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.lazy.*;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.ForwardLayeredServiceGraphBuilder;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.LayeredServiceGraph;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.cost.ResponseTime;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.cost.ServiceCount;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.discovery.FastIOLayersState;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.discovery.IOLayersState;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.discovery.InputServiceIndex;
import br.ufsc.lapesd.unserved.testbench.io_composer.state.Action;
import br.ufsc.lapesd.unserved.testbench.io_composer.state.ExecutionPath;
import br.ufsc.lapesd.unserved.testbench.util.DStarLite;
import br.ufsc.lapesd.unserved.testbench.util.FixedCacheTransitiveClosureGetter;
import com.google.common.base.Stopwatch;
import org.apache.jena.vocabulary.RDFS;

import javax.annotation.Nonnull;
import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;

import static java.util.concurrent.TimeUnit.MICROSECONDS;
import static java.util.concurrent.TimeUnit.NANOSECONDS;

public abstract class LayeredServiceSetGraphIOComposer
        extends AbstractLayeredIOComposer<SetNodeEdge, SetNode> {
    public static final String graphConstructionTime = "graphConstruction";
    public static final String graphBuilderSetupTime = "graphBuilderSetup";
    public static final String graphOptimizationTime = "graphOptimization";
    public static final String graphOptimizationMem = "graphOptimizationMem";
    public static final String graphConstructionMem = "graphConstructionMem";

    protected final @Nonnull BackwardSetNodeProvider backwardSetNodeProvider;
    protected final @Nonnull Function<IOComposerInput, IOLayersState> layersStateFactory;
    protected final @Nonnull List<Function<LayeredServiceGraph, LayeredServiceGraph>> optimizers;

    protected @Nonnull CostFunction<SetNodeEdge, SetNode> heuristic = new LayersHeuristic();
    protected @Nonnull CostFunction<SetNodeEdge, SetNode> cost = new ServiceCountCost();

    public LayeredServiceSetGraphIOComposer(IOComposerInput ci,
                     @Nonnull Function<IOComposerInput, IOLayersState> layersStateFactory,
                     @Nonnull BackwardSetNodeProvider backwardSetNodeProvider,
                     @Nonnull List<Function<LayeredServiceGraph, LayeredServiceGraph>> optimizers) {
        super(ci);
        this.backwardSetNodeProvider = backwardSetNodeProvider;
        this.layersStateFactory = layersStateFactory;
        this.optimizers = optimizers;
    }

    public LazyLayeredServiceSetGraph createGraph(DefaultIOComposerTimes times) {
        Stopwatch watch = Stopwatch.createStarted();
        ForwardLayeredServiceGraphBuilder sgBuilder = new ForwardLayeredServiceGraphBuilder()
                .withLayersStateFactory(layersStateFactory);
        LayeredServiceGraph sg = sgBuilder.build(ci, times);
        long us = watch.elapsed(MICROSECONDS)
                - MICROSECONDS.convert(sgBuilder.getDelegatesSetupNanos(), NANOSECONDS);
        times.addTime(graphConstructionTime, us, MICROSECONDS);
        logger.info("Service graph built in {} ms.", us/1000.0);

        long total = 0;
        for (Function<LayeredServiceGraph, LayeredServiceGraph> optimizer : optimizers) {
            watch.reset().start();
            sg = optimizer.apply(sg);
            total += us = watch.elapsed(MICROSECONDS);
            logger.info("Optimizer {} applied in {} ms.", optimizer, us /1000.0);
        }
        times.addTime(graphOptimizationTime, total, MICROSECONDS);
//        putariaDebugDumpGraph(sg);

        return new LazyLayeredServiceSetGraph(sg, backwardSetNodeProvider);
    }



//    protected void putariaDebugDumpGraph(LayeredServiceGraph g) {
//        try (PrintStream out = new PrintStream(new FileOutputStream("/tmp/unserved-opt-levels"))) {
//            int i = 0;
//            for (ServiceLayer layer : g.getLayers()) {
//                out.printf("layer: %d\n%s\n\n", i++, layer.getNodes().stream().map(n -> {
//                    if (n instanceof ServiceNode) {
//                        return ((ServiceNode) n).getConsequent().getProperty(Wscc.wscService)
//                                .getLiteral().getLexicalForm().split("#")[1];
//                    } else if (n instanceof StartNode) {
//                        return "Source";
//                    } else if (n instanceof EndNode) {
//                        return "Sink";
//                    }
//                    return n.toString();
//                }).sorted().reduce((l, r) -> l + ", " + r).orElse(""));
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }

    @Nonnull
    protected static ExecutionPath<SetNodeEdge> toEdgePath(@Nonnull LazyLayeredServiceSetGraph graph,
                                                    @Nonnull List<SetNode> list) {
        List<SetNodeEdge> edges = new ArrayList<>(list.size()-1);
        SetNode last = list.get(0);
        for (Iterator<SetNode> it = list.stream().skip(1).iterator(); it.hasNext(); ) {
            SetNode curr = it.next();
            edges.add(graph.edge(last, curr));
            last = curr;
        }
        return new ExecutionPath<>(edges);
    }

    public static class ServiceCountCost implements CostFunction<SetNodeEdge, SetNode> {
        @Override
        public double get(@Nonnull LayeredGraph<Action, SetNodeEdge, SetNode> g,
                          @Nonnull SetNode p, @Nonnull SetNode s) {
            int pLayer = g.getLayer(p), sLayer = g.getLayer(s);
            return p.getCostInfo().get(ServiceCount.class).getValue();
//            return (double)(sLayer<pLayer ? s : p).getCostInfo().get(ServiceCount.class).getValue();
        }
    }

    public static class RTCost implements CostFunction<SetNodeEdge, SetNode> {
        public final double fallback;

        public RTCost() {
            this(Double.NaN);
        }

        public RTCost(double fallback) {
            this.fallback = fallback;
        }

        @Override
        public double get(@Nonnull LayeredGraph<Action, SetNodeEdge, SetNode> g,
                          @Nonnull SetNode p, @Nonnull SetNode s) {
            int pLayer = g.getLayer(p), sLayer = g.getLayer(s);
            SetNode t = sLayer < pLayer ? s : p;
            try {
                return t.getCostInfo().get(ResponseTime.class).getMilliseconds();
            } catch (NoSuchElementException e) {
                if (fallback == Double.NaN) {
                    throw new IllegalArgumentException(t + " has no ResponseTime and " +
                            "RTCost has no fallback");
                }
                return fallback;
            }
        }
    }

    public static class LayersHeuristic implements CostFunction<SetNodeEdge, SetNode> {
        @Override
        public double get(@Nonnull LayeredGraph<Action, SetNodeEdge, SetNode> g,
                          @Nonnull SetNode p, @Nonnull SetNode s) {
            return s.equals(g.getEnd()) ? 0.0 : (double)Math.abs(g.getLayer(s) - g.getLayer(p));
        }
    }

    protected static class LazyGraphAccessor implements DStarLite.GraphAccessor<SetNode, Double> {
        public final @Nonnull LazyLayeredServiceSetGraph graph;
        private CostFunction<SetNodeEdge, SetNode> cost;

        LazyGraphAccessor(@Nonnull LazyLayeredServiceSetGraph graph,
                          @Nonnull CostFunction<SetNodeEdge, SetNode> cost) {
            this.graph = graph;
            this.cost = cost;
        }

        @Override
        public Collection<SetNode> successors(@Nonnull SetNode setNode) {
            return graph.successors(setNode);
        }
        @Override
        public Collection<SetNode> predecessors(@Nonnull SetNode setNode) {
            return graph.predecessors(setNode);
        }
        @Override
        public Double cost(@Nonnull SetNode predecessor, @Nonnull SetNode successor) {
            return cost.get(graph, predecessor, successor);
        }
    }

    public abstract static class Builder extends AbstractIOComposerBuilder {
        protected BackwardSetNodeProvider backwardSetNodeProvider = new NonEquivalentBackwardSetNodeProvider();
        protected List<Function<LayeredServiceGraph, LayeredServiceGraph>> optimizers
                = new LinkedList<>();
        protected Function<IOComposerInput, IOLayersState> layersStateFactory =
                ci -> new FastIOLayersState(new InputServiceIndex(
                        FixedCacheTransitiveClosureGetter.forward(RDFS.subClassOf).setThreshold(7)
                                .visitAll(ci.getSkolemizedUnion()).build()));
        protected @Nonnull CostFunction heuristic = new LayersHeuristic();
        protected @Nonnull CostFunction cost = new ServiceCountCost();

        @Override
        public IOComposer build() {
            LayeredServiceSetGraphIOComposer composer;
            composer = (LayeredServiceSetGraphIOComposer)super.build();
            composer.cost = cost;
            composer.heuristic = heuristic;
            return composer;
        }

        public Builder withCost(@Nonnull CostFunction cost) {
            this.cost = cost;
            return this;
        }

        public Builder withHeuristic(@Nonnull CostFunction heuristic) {
            this.heuristic = heuristic;
            return this;
        }

        public Builder withOptimizer(Function<LayeredServiceGraph, LayeredServiceGraph> optimizer) {
            optimizers.add(optimizer);
            return this;
        }

        public Builder
        withoutOptimizers(Predicate<Function<LayeredServiceGraph, LayeredServiceGraph>> predicate) {
            optimizers.removeIf(predicate);
            return this;
        }

        public Builder setLayersStateFactory(Function<IOComposerInput, IOLayersState> layersStateFactory) {
            this.layersStateFactory = layersStateFactory;
            return this;
        }

        public BackwardSetNodeProvider getBackwardSetNodeProvider() {
            return backwardSetNodeProvider;
        }

        public Builder setBackwardSetNodeProvider(@Nonnull BackwardSetNodeProvider backwardSetNodeProvider) {
            this.backwardSetNodeProvider = backwardSetNodeProvider;
            return this;
        }
    }

}

package br.ufsc.lapesd.unserved.testbench.pragproof.exceptions;

import br.ufsc.lapesd.unserved.testbench.composer.ComposerException;

public class PragmaticProofException extends ComposerException {
    public PragmaticProofException(String s) {
        super(s);
    }

    public PragmaticProofException(String message, Throwable cause) {
        super(message, cause);
    }

    public PragmaticProofException(Throwable throwable) {
        super(throwable);
    }
}

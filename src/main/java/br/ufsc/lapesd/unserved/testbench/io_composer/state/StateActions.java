package br.ufsc.lapesd.unserved.testbench.io_composer.state;

import br.ufsc.lapesd.unserved.testbench.model.Message;
import com.google.common.base.Preconditions;

import java.util.Collection;
import java.util.HashMap;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * A consequent-indexed set of {@link MessagePairAction} instances.
 *
 * When a pair for which the consequent is already indexed is inserted, the assignments of the
 * new pair are merged into those of the already present pair.
 */
public final class StateActions {
    private HashMap<Message, MessagePairAction> consequentMap = new HashMap<>();
    private Copies copies = new Copies();

    public StateActions() {
    }

    public StateActions(StateActions other) {
        consequentMap.putAll(other.consequentMap);
    }

    public void add(Action action) {
        if (action instanceof MessagePairAction)
            add((MessagePairAction)action);
        else if (action instanceof Copies)
            add((Copies) action);
        else
            Preconditions.checkArgument(false, "Unsupported action type");
    }

    public void add(MessagePairAction candidate) {
        MessagePairAction existing = consequentMap.getOrDefault(candidate.getConsequent(), null);
        if (existing != null) {
            existing.addAssignments(candidate);
        } else {
            consequentMap.put(candidate.getConsequent(), candidate);
        }
    }

    public void add(Copies copies) {
        this.copies.addAll(copies);
    }

    public Collection<MessagePairAction> getMessagePairs() {
        return consequentMap.values();
    }

    public Copies getCopies() {
        return copies;
    }

    public int size() {
        return consequentMap.size();
    }

    public Copies getAssignments() {
        Copies merged = new Copies();
        consequentMap.values().stream().map(MessagePairAction::getAssignments)
                .forEach(merged::addAll);
        return merged;
    }

    public Set<Message> getAntecedents() {
        return consequentMap.values().stream().map(MessagePairAction::getAntecedent)
                .collect(Collectors.toSet());
    }

    public Set<Message> getConsequentSet() {
        return consequentMap.keySet();
    }

    @Override
    public String toString() {
        return String.format("StateActions(%s\n%s)", copies,
                getMessagePairs().stream().map(MessagePairAction::toString).map(s -> s + "\n")
                        .reduce((l, r) -> l + "             " + r).orElse(""));
    }
}

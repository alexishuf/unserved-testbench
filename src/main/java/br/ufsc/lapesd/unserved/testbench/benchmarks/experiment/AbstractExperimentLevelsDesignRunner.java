package br.ufsc.lapesd.unserved.testbench.benchmarks.experiment;

import com.google.common.base.Preconditions;
import org.kohsuke.args4j.Option;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Map;

public abstract class AbstractExperimentLevelsDesignRunner extends AbstractExperimentDesignRunner {
    @Option(name = "--levels-json", required = true)
    protected File levelsJson;

    protected ExperimentDesignLevels levels;

    protected AbstractExperimentLevelsDesignRunner(ExperimentFactory experimentFactory) {
        super(experimentFactory);
    }

    @Override
    protected void beforeLoop() throws Exception {
        super.beforeLoop();
        levels = loadLevels();
    }

    @Override
    protected void checkFilesAndDirs() throws ExperimentDesignRunnerException {
        super.checkFilesAndDirs();
        if (!levelsJson.exists())
            throw ex("Levels JSON file %s does not exist", levelsJson);
        if (!levelsJson.canRead())
            throw ex("Levels JSON file %s has no read permission", levelsJson);
    }

    @Override
    protected Factors createFactors(Map<String, String> factors) {
        return levels.createFactors(factors);
    }

    protected  abstract ExperimentDesignLevels loadLevels() throws FileNotFoundException;

    public static abstract class Builder extends AbstractExperimentDesignRunner.Builder {
        public Builder(File designCsv, File levelsJson, File resultObjectsDir) {
            super(designCsv, levelsJson, resultObjectsDir);
        }

        @Override
        protected <T extends AbstractExperimentDesignRunner> T
        setupAbstractRunner(T runner) {
            Preconditions.checkArgument(runner instanceof AbstractExperimentLevelsDesignRunner);
            AbstractExperimentLevelsDesignRunner levelsRunner =
                    (AbstractExperimentLevelsDesignRunner) runner;
            super.setupAbstractRunner(runner);
            levelsRunner.levelsJson = levelsJson;
            return runner;
        }
    }
}

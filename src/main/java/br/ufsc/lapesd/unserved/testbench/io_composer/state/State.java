package br.ufsc.lapesd.unserved.testbench.io_composer.state;

import br.ufsc.lapesd.unserved.testbench.model.Variable;

import javax.annotation.Nonnull;
import java.util.Collections;
import java.util.Set;

/**
 * State used in IO-based composition graph search problems
 */
public class State {
    @Nonnull private final Set<Variable> inputs;
    @Nonnull private final Set<Variable> known;
    @Nonnull private final StateActions actions;
    private final double costFromInitial;
    private final double heuristic;
    private final boolean goal;

    private State predecessor = null, successor = null;

    private State(@Nonnull Set<Variable> inputs, @Nonnull Set<Variable> known,
                  @Nonnull StateActions actions,
                  double predecessorCost, double heuristic, boolean goal) {
        this.inputs = Collections.unmodifiableSet(inputs);
        this.known = Collections.unmodifiableSet(known);
        this.actions = actions;
        this.costFromInitial = predecessorCost + actions.size();
        this.heuristic = heuristic;
        this.goal = goal;
    }

    public static State goal(Set<Variable> unknowns, State predecessor) {
        return new State(unknowns, Collections.emptySet(), new StateActions(),
                predecessor == null ? 0 : predecessor.costFromInitial + 0, 0, true);
    }

    public static State initial(Set<Variable> knowns) {
        return new State(Collections.emptySet(), knowns, new StateActions(), 0, 0, false);
    }

    public static State request(State predecessor, double toSendHeuristic, Set<Variable> unknowns,
                                Set<Variable> knowns, StateActions messageSet) {
        return new State(unknowns, knowns, messageSet, predecessor.getCostFromInitial(),
                toSendHeuristic, false);
    }

    public boolean isGoal() {
        return goal;
    }

    public State getPredecessor() {
        return predecessor;
    }

    public void setPredecessor(State predecessor) {
        this.predecessor = predecessor;
    }

    public State getSuccessor() {
        return successor;
    }

    public void setSuccessor(State successor) {
        this.successor = successor;
    }

    /**
     * Set of unknown variables that require assignment before the messages in this state
     * can be sent.
     *
     * Empty if <code>isGoal()==true</code>.
     *
     * @return An unmodifiable set.
     */
    @Nonnull
    public Set<Variable> getInputs() {
        return inputs;
    }

    /**
     * Set of variables known after the consequent messages of this state are received.
     *
     * It is supposed that no failure occurs and all declared message parts are present, even
     * those that are optional.
     *
     * Empty if <code>isGoal()==true</code>.
     *
     * @return An unmodifiable set.
     */
    @Nonnull
    public Set<Variable> getKnown() {
        return known;
    }

    /**
     * Get {@link StateActions} of this state, containing consequent/antecedent message
     * pairs as well as {@link Variable} assignments.
     *
     * The set is empty if <code>isGoal()==true</code>.
     */
    @Nonnull
    public StateActions getActions() {
        return actions;
    }

    /**
     * An heuristic evaluation stored at the node.
     * @return a double in [0, 1)
     */
    public double getHeuristic() {
        return heuristic;
    }

    /**
     * Accumulated cost from the initial state to this state.
     *
     * @return a double in [0, Double.MAX_VALUE].
     */
    public double getCostFromInitial() {
        return costFromInitial;
    }
}

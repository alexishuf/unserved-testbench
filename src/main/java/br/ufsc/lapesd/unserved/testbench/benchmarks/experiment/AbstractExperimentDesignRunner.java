package br.ufsc.lapesd.unserved.testbench.benchmarks.experiment;

import com.google.gson.Gson;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;
import org.kohsuke.args4j.Option;

import javax.annotation.Nonnull;
import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.*;
import java.util.stream.Collectors;

public abstract class AbstractExperimentDesignRunner {
    protected final ExperimentFactory experimentFactory;
    @Option(name = "--design-csv", required = true)
    protected File designCsv;
    @Option(name = "--results-csv", required = true)
    protected File resultsCsv;
    @Option(name = "--result-objects-dir", required = true)
    protected File resultObjectsDir;
    @Option(name = "--continue")
    protected File continuation = null;
    @Option(name = "--sleep", usage = "Sleep, in milliseconds between rows")
    protected int sleep = 0;
    protected Design design;


    public AbstractExperimentDesignRunner(ExperimentFactory experimentFactory) {
        this.experimentFactory = experimentFactory;
    }

    protected static ExperimentDesignRunnerException ex(String fmt, Object... args) {
        return new ExperimentDesignRunnerException(String.format(fmt, args));
    }

    public void run() throws Exception {
        checkFilesAndDirs();

        State state = initState();
        design = new Design(designCsv);
        beforeLoop();

        while (state.getRow() < design.rows.size()) {
            Factors factors = createFactors(design.getFactors(state.getRow()));
            System.out.printf("Row %d -- %s: ...\n", state.getRow(), factors.toString());
            try (Experiment experiment = experimentFactory.createExperiment(factors)) {
                experiment.setUp();
                ExperimentResult result = experiment.run();
                writeResults(design, state.getRow(), result);
                state.setResultsChecksum(State.checksum(resultsCsv));
                state.advanceRow();
                if (continuation != null) state.save(continuation);
                if (sleep > 0) {
                    System.out.printf("Sleeping for %d ms...\n", sleep);
                    Thread.sleep(sleep);
                }
            }
        }
    }

    protected abstract Factors createFactors(Map<String, String> factors);

    protected void beforeLoop() throws Exception {}



    protected void checkFilesAndDirs() throws ExperimentDesignRunnerException {
        if (resultObjectsDir.exists() && !resultObjectsDir.isDirectory())
            throw ex("Result objects dir %s exists but is not a dir", resultObjectsDir);
        if (!resultObjectsDir.exists()) {
            if (!resultObjectsDir.mkdir())
                throw ex("Could not create Result objects dir %s", resultObjectsDir);
        }
        if (!designCsv.exists())
            throw ex("Design csv file %s does not exist", designCsv);
        if (!designCsv.canRead())
            throw ex("Design csv file %s has no read permission", designCsv);
        if (resultsCsv.exists() && !resultsCsv.canWrite())
            throw ex("Results csv file %s exists but has no write permission", resultsCsv);
        if (continuation != null && continuation.exists() && !continuation.canWrite())
            throw ex("Continuation file %s exists but has no write permission", continuation);
    }

    private void writeResults(Design design, int row, ExperimentResult result)
            throws IOException, ExperimentDesignRunnerException {
        if (resultsCsv.exists()) {
            try (CSVParser resultsParser = CSVParser.parse(resultsCsv, Charset.forName("UTF-8"),
                    CSVFormat.DEFAULT.withHeader())) {
                if (resultsParser.getRecords().size() != row)
                    throw ex("Row %d is already in results csv", row);
            }
        }
        boolean writeHeaders = !resultsCsv.exists();
        try (FileWriter fileWriter = new FileWriter(resultsCsv, true);
             CSVPrinter printer = new CSVPrinter(fileWriter, CSVFormat.DEFAULT)) {
            if (writeHeaders) {
                LinkedList<String> list = new LinkedList<>(getResultHeaders());
                list.addFirst("objFile");
                printer.printRecord(list);
            }

            LinkedList<String> values = new LinkedList<>(getResultValues(result));
            values.addFirst(writeResultObject(result));
            printer.printRecord(values);
        }
    }

    protected abstract List<String> getResultHeaders();

    protected abstract List<String> getResultValues(ExperimentResult result);

    private String writeResultObject(ExperimentResult result) throws IOException {
        File objFile = Files.createTempFile(resultObjectsDir.toPath(), "ExperimentResult", ".obj").toFile();
        try (ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(objFile))) {
            out.writeObject(result);
        }
        return objFile.getName();
    }

    private State initState() throws ExperimentDesignRunnerException, IOException {
        String designChecksum = State.checksum(designCsv);
        String resultsChecksum = State.checksum(resultsCsv);
        State state = State.loadOrCreate(continuation, designChecksum, resultsChecksum);

        if (!Objects.equals(state.getDesignChecksum(), designChecksum)) {
            throw ex("Continuation checksum does not match design csv.");
        }
        if (!Objects.equals(state.getResultsChecksum(), resultsChecksum)) {
            throw ex("Continuation checksum does not match results csv.");
        }
        return state;
    }

    private static class State {
        private int row = 0;
        private String designChecksum;
        private String resultsChecksum;

        public State(String designChecksum, String resultsChecksum) {
            this.designChecksum = designChecksum;
            this.resultsChecksum = resultsChecksum;
        }

        public static String checksum(File file) throws IOException {
            if (!file.exists()) return null;
            try (FileInputStream stream = new FileInputStream(file)) {
                return Base64.getEncoder().encodeToString(DigestUtils.sha256(stream));
            }
        }

        public static State load(File file) throws FileNotFoundException {
            return new Gson().fromJson(new FileReader(file), State.class);
        }
        public static State loadOrCreate(File file, String designChecksum, String resultsChecksum) {
            if (file != null) {
                try {
                    return new Gson().fromJson(new FileReader(file), State.class);
                } catch (FileNotFoundException e) { }
            }
            return new State(designChecksum, resultsChecksum);
        }

        public void save(@Nonnull File file) throws IOException {
            File temp = Files.createTempFile("unserved-testbench", ".json").toFile();
            temp.deleteOnExit();
            try (FileWriter writer = new FileWriter(temp)) {
                new Gson().toJson(this, writer);
            }
            Files.copy(temp.toPath(), file.toPath(), StandardCopyOption.REPLACE_EXISTING);
        }

        public int getRow() {
            return row;
        }

        public String getDesignChecksum() {
            return designChecksum;
        }

        public String getResultsChecksum() {
            return resultsChecksum;
        }

        public void setResultsChecksum(String resultsChecksum) {
            this.resultsChecksum = resultsChecksum;
        }

        public void advanceRow() {
            ++row;
        }
    }

    protected static class Design {
        public final List<Map<String, String>> rows;
        public final List<String> headers;
        /** Column-ordered set of headers that indicate factors */
        public final Set<String> factorHeaders;

        public Design(File file) throws IOException, ExperimentDesignRunnerException {
            try (CSVParser parser = CSVParser.parse(file, Charset.forName("UTF-8"),
                    CSVFormat.DEFAULT.withHeader())) {

                List<String> headers = new ArrayList<>(parser.getHeaderMap().size());
                for (int i = 0; i < parser.getHeaderMap().size(); i++) headers.add(null);
                parser.getHeaderMap().entrySet().forEach(e -> headers.set(e.getValue(), e.getKey()));
                this.headers = Collections.unmodifiableList(headers);

                LinkedHashSet<String> factorHeaders = headers.stream()
                        .filter(h -> !h.equals("name") && !h.startsWith("run.")
                                && !h.equals("Blocks"))
                        .collect(Collectors.toCollection(LinkedHashSet::new));
                this.factorHeaders = Collections.unmodifiableSet(factorHeaders);

                List<Map<String, String>> rows = new ArrayList<>();
                for (CSVRecord record : parser.getRecords()) {
                    LinkedHashMap<String, String> map = new LinkedHashMap<>();
                    Iterator<String> rit = record.iterator(), hit = headers.iterator();
                    while (rit.hasNext()) {
                        if (!hit.hasNext()) throw ex("Missing headers!");
                        map.put(hit.next(), rit.next());
                    }
                    rows.add(Collections.unmodifiableMap(map));
                }
                this.rows = Collections.unmodifiableList(rows);
            }
        }

        public Map<String, String> getFactors(int row) {
            LinkedHashMap<String, String> map = new LinkedHashMap<>();
            rows.get(row).entrySet().stream().filter(e -> factorHeaders.contains(e.getKey()))
                    .forEachOrdered(e -> map.put(e.getKey(), e.getValue()));
            return map;
        }
    }

    public static abstract class Builder {
        protected File designCsv, levelsJson, resultObjectsDir, continuation = null;

        public Builder(File designCsv, File levelsJson, File resultObjectsDir) {
            this.designCsv = designCsv;
            this.levelsJson = levelsJson;
            this.resultObjectsDir = resultObjectsDir;
        }

        public Builder withContinuation(File continuation) {
            this.continuation = continuation;
            return this;
        }

        protected <T extends AbstractExperimentDesignRunner> T
        setupAbstractRunner(T runner) {
            runner.designCsv = designCsv;
            runner.resultObjectsDir = resultObjectsDir;
            runner.continuation = continuation;
            return runner;
        }

        public abstract AbstractExperimentDesignRunner build();
    }
}

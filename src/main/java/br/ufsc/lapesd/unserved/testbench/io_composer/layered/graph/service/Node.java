package br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service;

import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.And;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.cost.CostInfo;
import br.ufsc.lapesd.unserved.testbench.model.Variable;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.Set;

/**
 * There is no requirement for Nodes to provide actual alternatives.
 */
public interface Node {
    @Nonnull
    Set<Variable> getInputs();
    @Nonnull
    Set<Variable> getOutputs();
    /**
     * Gets an object that identifies the node and is equals() comparable. If the node is
     * replaced with an alternative, its Id, unlike the hashCode(), will change.
     */
    Object getId();

    /**
     * Gets the preconditions of the node.
     * @return preconditions (conjunction of u:condition of antecedent). May be empty.
     */
    @Nonnull And getPreConditions();

    /**
     * Gets the postconditions of the node.
     * @return postconditions (conjunction of u:condition of consequent). May be empty.
     */
    @Nonnull And getPostConditions();

    /**
     * Gets parameters related to cost
     */
    @Nonnull CostInfo getCostInfo();

    List<Node> getAlternatives();
}

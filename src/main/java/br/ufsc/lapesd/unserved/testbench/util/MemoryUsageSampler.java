package br.ufsc.lapesd.unserved.testbench.util;

import br.ufsc.lapesd.unserved.testbench.benchmarks.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Consumer;

public class MemoryUsageSampler extends Thread implements AutoCloseable {
    public static final int DEFAULT_INTERVAL = 10;
    private static Logger logger = LoggerFactory.getLogger(MemoryUsageSampler.class);
    private long interval = DEFAULT_INTERVAL;
    private @Nonnull Consumer<Long> consumer;
    private Lock lock = new ReentrantLock();
    private Condition stopping = lock.newCondition(), stopped = lock.newCondition();
    private boolean shutdownCalled = false;

    public MemoryUsageSampler(@Nonnull Consumer<Long> consumer) {
        this.consumer = consumer;
    }

    public MemoryUsageSampler() {
        this(usage -> {});
    }

    public MemoryUsageSampler setConsumer(@Nonnull Consumer<Long> consumer) {
        this.consumer = consumer;
        return this;
    }

    public MemoryUsageSampler setInterval(long milliseconds) {
        this.interval = milliseconds;
        return this;
    }

    @Override
    public synchronized void start() {
        consumer.accept(Utils.usedMemory());
        shutdownCalled = false;
        super.start();
    }

    public void shutdown() {
        lock.lock();
        try {
            if (!isAlive() || shutdownCalled) return;
            shutdownCalled = true;
            stopping.signalAll();
            stopped.await();
        } catch (InterruptedException e) {
            logger.error("MemoryUsageSampler.shutdown() interrupted", e);
        } finally {
            lock.unlock();
        }
    }

    @Override
    public void run() {
        //noinspection InfiniteLoopStatement
        lock.lock();
        try {
            while (!shutdownCalled && !stopping.await(interval, TimeUnit.MILLISECONDS)) {
                consumer.accept(Utils.usedMemory());
            }
            stopped.signalAll();
        } catch (InterruptedException ignored) {
        } finally {
            stopped.signalAll();
            lock.unlock();
        }
    }

    @Override
    public void close() {
        shutdown();
    }
}

package br.ufsc.lapesd.unserved.testbench.iri;

import br.ufsc.lapesd.unserved.testbench.util.ResourcesBackground;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.riot.RDFFormat;

import java.util.Collections;

public class Content extends ResourcesBackground {
    public static final String IRI = "https://www.w3.org/2011/content";
    public static final String PREFIX = IRI + "#";
    public static final String PREFIX_SHORT = "cnt";

    public Content() {
        super(Collections.singletonList(ImmutablePair.of("content", RDFFormat.RDFXML)), true);
    }

    private static Content instance = new Content();
    public static Content getInstance() {
        return instance;
    }

    public static final Resource Content = getInstance().getModel().createResource(PREFIX + "Content");
    public static final Resource ContentAsBase64 = getInstance().getModel().createResource(PREFIX + "ContentAsBase64");
    public static final Resource ContentAsText = getInstance().getModel().createResource(PREFIX + "ContentAsText");
    public static final Resource ContentAsXML = getInstance().getModel().createResource(PREFIX + "ContentAsXML");
    public static final Resource DoctypeDecl = getInstance().getModel().createResource(PREFIX + "DoctypeDecl");

    public static final Property bytes = getInstance().getModel().createProperty(PREFIX + "bytes");
    public static final Property characterEncoding = getInstance().getModel().createProperty(PREFIX + "characterEncoding");
    public static final Property chars = getInstance().getModel().createProperty(PREFIX + "chars");
    public static final Property declaredEncoding = getInstance().getModel().createProperty(PREFIX + "declaredEncoding");
    public static final Property doctypeName = getInstance().getModel().createProperty(PREFIX + "doctypeName");
    public static final Property dtDecl = getInstance().getModel().createProperty(PREFIX + "dtDecl");
    public static final Property internalSubset = getInstance().getModel().createProperty(PREFIX + "internalSubset");
    public static final Property leadingMisc = getInstance().getModel().createProperty(PREFIX + "leadingMisc");
    public static final Property publicId = getInstance().getModel().createProperty(PREFIX + "publicId");
    public static final Property rest = getInstance().getModel().createProperty(PREFIX + "rest");
    public static final Property standalone = getInstance().getModel().createProperty(PREFIX + "standalone");
    public static final Property systemId = getInstance().getModel().createProperty(PREFIX + "systemId");
    public static final Property version = getInstance().getModel().createProperty(PREFIX + "version");
}

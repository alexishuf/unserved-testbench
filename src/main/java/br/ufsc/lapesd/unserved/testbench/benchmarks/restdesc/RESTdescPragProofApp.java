package br.ufsc.lapesd.unserved.testbench.benchmarks.restdesc;

import br.ufsc.lapesd.unserved.testbench.benchmarks.Utils;
import br.ufsc.lapesd.unserved.testbench.benchmarks.compose.TimesExperimentResult;
import br.ufsc.lapesd.unserved.testbench.input.RDFInput;
import br.ufsc.lapesd.unserved.testbench.input.RDFInputFile;
import br.ufsc.lapesd.unserved.testbench.reasoner.impl.EYEProcessFactory;
import br.ufsc.lapesd.unserved.testbench.reasoner.impl.RiotEnhancedN3ReasonerFactory;
import br.ufsc.lapesd.unserved.testbench.restdesc_pragproof.RESTdescPragProof;
import com.google.common.base.Stopwatch;
import com.google.gson.Gson;
import org.apache.commons.io.IOUtils;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFFormat;
import org.apache.jena.riot.RDFLanguages;
import org.kohsuke.args4j.Argument;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.PrintStream;

import static java.util.concurrent.TimeUnit.MICROSECONDS;

public class RESTdescPragProofApp {
    @Option(name = "--goal", required = true)
    private File goal;

    @Option(name = "--workflow-out")
    private File workflowOut;

    @Option(name = "--output")
    private File output;

    @Option(name = "--times-out")
    private File timesOut = null;

    @Option(name = "--preheat-count")
    private int preheatCount = 0;

    @Option(name = "--execute")
    private boolean execute = false;

    @Option(name = "--cheat-unstable-reasoner")
    private boolean cheatUnstableReasoner = false;

    @Option(name = "--measure-reasoner-spawn")
    private boolean measureReasonerSpawn = false;

    @Option(name = "--measure-reasoner-spawn-and-parse")
    private boolean measureReasonerSpawnAndParse = false;

    @SuppressWarnings("MismatchedReadAndWriteOfArray")
    @Argument
    private File[] inputs;

    public static void main(String[] args) throws Exception {
        RESTdescPragProofApp app = new RESTdescPragProofApp();
        new CmdLineParser(app).parseArgument(args);
        app.run();
    }

    private void run() throws Exception {
        preheat();
        TimesExperimentResult time = new TimesExperimentResult();
        Stopwatch watch = Stopwatch.createStarted();
        run(time);
        time.setTotal(watch.elapsed(MICROSECONDS)/1000.0);
        System.out.printf("Time: %s\n", time);
        if (timesOut != null) {
            try (PrintStream out = new PrintStream(new FileOutputStream(timesOut))) {
                new Gson().toJson(time, out);
            }
        }
    }

    private void preheat() throws Exception {
        for (int i = 0; i < preheatCount; i++) {
            TimesExperimentResult time = new TimesExperimentResult();
            run(time);
            System.out.printf("Preheat %d/%d %s.\n", i, preheatCount, time);
        }
        Utils.preheatCooldown();
    }

    public void run(TimesExperimentResult res) throws Exception {
        Stopwatch w = Stopwatch.createStarted();
        RDFInput result;
        try (RESTdescPragProof pp = createPP()) {
            res.setInitialization(w.elapsed(MICROSECONDS) / 1000.0);

            if (execute) {
                w.reset().start();
                result = pp.execute();
                res.setExecution(deductFromExecution(w.elapsed(MICROSECONDS)/1000.0,
                        pp));
            } else {
                w.reset().start();
                result = pp.firstProof();
                assert pp.getCompositionTimes(MICROSECONDS).size() == 1;
            }

            res.setComposition(pp.getTotalComposition(MICROSECONDS) / 1000.0);
            pp.getCompositionTimes(MICROSECONDS).stream().map(us -> us / 1000.0)
                    .forEach(res::addCompositionTime);

            res.setBlacklisting(pp.getTotalBlacklisting(MICROSECONDS) / 1000.0);
            pp.getBlacklistingTimes(MICROSECONDS).stream().map(us -> us / 1000.0).
                    forEach(res::addBlacklistingTime);
            pp.getReasonerSpawnTimes(MICROSECONDS).stream().map(us -> us / 1000.0)
                    .forEach(res::addReasonerSpawnTime);
            pp.getReasonerSpawnAndParseTimes(MICROSECONDS).stream().map(us -> us / 1000.0)
                    .forEach(ms -> res.addTime(RESTdescPragProof.n3ReasonerSpawnAndParseTime, ms));
            pp.getReasoningTimes(MICROSECONDS).stream().map(us -> us / 1000.0)
                    .forEach(ms -> res.addTime(RESTdescPragProof.n3ReasoningTime, ms));
        }

        File outFile = execute ? output : workflowOut;
        if (outFile != null) {
            w.reset().start();
            try (FileOutputStream out = new FileOutputStream(outFile);
                 InputStream in = result.createInputStream()) {
                IOUtils.copy(in, out);
            } finally {
                res.setOutput(w.elapsed(MICROSECONDS) / 1000.0);
            }
        }
    }

    private double deductFromExecution(double execution, RESTdescPragProof pp) {
        if (measureReasonerSpawnAndParse) {
            execution -= pp.getReasonerSpawnAndParseTimes(MICROSECONDS).stream()
                    .reduce(Long::sum).orElse(0L) / 1000.0;
        }
        if (measureReasonerSpawn) {
            execution -= pp.getReasonerSpawnTimes(MICROSECONDS).stream()
                    .reduce(Long::sum).orElse(0L) / 1000.0;
        }
        return execution;
    }

    private RESTdescPragProof createPP() throws Exception {
        RESTdescPragProof.Builder builder = new RESTdescPragProof.Builder(
                new RiotEnhancedN3ReasonerFactory(new EYEProcessFactory()))
                .withGoal(asRDFInput(goal));
        for (File fileInput : inputs) builder.addInput(asRDFInput(fileInput));
        return  builder.setCheatUnstableReasoner(cheatUnstableReasoner)
                .setMeasureReasonerSpawnTime(measureReasonerSpawn)
                .setMeasureReasonerSpawnAndParseTime(measureReasonerSpawnAndParse)
                .build();
    }

    private RDFInput asRDFInput(File file) throws Exception {
        Lang lang = RDFLanguages.filenameToLang(file.getName());
        if (lang == null)
            throw new Exception("Cannot guess format from " + file.getName());
        return new RDFInputFile(file, new RDFFormat(lang));
    }
}

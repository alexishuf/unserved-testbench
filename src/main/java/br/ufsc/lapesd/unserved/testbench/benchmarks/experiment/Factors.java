package br.ufsc.lapesd.unserved.testbench.benchmarks.experiment;

import java.util.List;

public interface Factors {
    String getValue(String name);
    List<String> getNames();
}

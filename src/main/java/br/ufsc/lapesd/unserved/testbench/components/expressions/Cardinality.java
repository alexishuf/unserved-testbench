package br.ufsc.lapesd.unserved.testbench.components.expressions;

import com.google.common.base.Preconditions;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.OWL2;

import javax.annotation.Nonnull;
import java.util.Collections;
import java.util.Objects;
import java.util.Set;

public class Cardinality extends AbstractClassExpression {
    public enum CardinalityType {
        MIN_CARDINALITY,
        MAX_CARDINALITY,
        EXACT_CARDINALITY,
        MIN_QUALIFIED_CARDINALITY {
            @Override
            public boolean isQualified() {
                return true;
            }
        },
        MAX_QUALIFIED_CARDINALITY{
            @Override
            public boolean isQualified() {
                return true;
            }
        },
        EXACT_QUALIFIED_CARDINALITY {
            @Override
            public boolean isQualified() {
                return true;
            }
        };

        public boolean isQualified() { return false; }

        public static CardinalityType fromProperty(Property property) {
            if (property.equals(OWL2.minCardinality))
                return MIN_CARDINALITY;
            else if (property.equals(OWL2.minQualifiedCardinality))
                return MIN_QUALIFIED_CARDINALITY;
            else if (property.equals(OWL2.maxCardinality))
                return MAX_CARDINALITY;
            else if (property.equals(OWL2.maxQualifiedCardinality))
                return MAX_QUALIFIED_CARDINALITY;
            else if (property.equals(OWL2.cardinality))
                return EXACT_CARDINALITY;
            else if (property.equals(OWL2.qualifiedCardinality))
                return EXACT_QUALIFIED_CARDINALITY;
            else
                return null;
        }
    }

    @Nonnull private final CardinalityType cardinalityType;
    private final long cardinalityValue;
    @Nonnull private final Property onProperty;
    private final ClassExpression rangeQualification;

    public Cardinality(@Nonnull CardinalityType cardinalityType,
                       @Nonnull Property onProperty, long cardinalityValue,
                       @Nonnull Set<Resource> names) {
        super(ClassExpression.Type.Cardinality, names);
        Preconditions.checkArgument(!cardinalityType.isQualified());
        this.cardinalityType = cardinalityType;
        this.onProperty = onProperty;
        this.cardinalityValue = cardinalityValue;
        this.rangeQualification = null;
    }

    public Cardinality(@Nonnull CardinalityType cardinalityType,
                       @Nonnull Property onProperty, long cardinalityValue,
                       @Nonnull Set<Resource> names,
                       @Nonnull ClassExpression rangeQualification) {
        super(ClassExpression.Type.Cardinality, names);
        Preconditions.checkArgument(cardinalityType.isQualified());
        this.cardinalityType = cardinalityType;
        this.onProperty = onProperty;
        this.cardinalityValue = cardinalityValue;
        this.rangeQualification = rangeQualification;
    }

    @Nonnull
    public CardinalityType getCardinalityType() {
        return cardinalityType;
    }

    public long getValue() {
        return cardinalityValue;
    }

    @Nonnull
    public Property getOnProperty() {
        return onProperty;
    }

    public ClassExpression getRangeQualification() {
        return rangeQualification;
    }

    @Override
    public Set<ClassExpression> getChildren() {
        return getCardinalityType().isQualified() ? Collections.singleton(rangeQualification)
                : Collections.emptySet();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getCardinalityType()).append(getChildren())
                .append(getOnProperty()).append(getValue()).append(getRangeQualification())
                .hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) return true;
        if (!(o instanceof Cardinality)) return false;
        Cardinality rhs = (Cardinality) o;
        return getCardinalityType().equals(rhs.getCardinalityType())
                && getChildren().equals(rhs.getChildren())
                && getOnProperty().equals(rhs.getOnProperty())
                && getValue() == rhs.getValue()
                && Objects.equals(getRangeQualification(), rhs.getRangeQualification());
    }
}

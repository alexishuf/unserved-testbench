package br.ufsc.lapesd.unserved.testbench.util;

import br.ufsc.lapesd.unserved.testbench.Background;
import br.ufsc.lapesd.unserved.testbench.input.RDFInput;
import br.ufsc.lapesd.unserved.testbench.input.RDFInputFile;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.riot.RDFFormat;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ResourcesBackground implements Background {
    private List<RDFInput> inputs = new ArrayList<>();
    private Model model = null;
    private final boolean isSchema;

    protected ResourcesBackground(List<ImmutablePair<String, RDFFormat>> pathAndFormats,
                                  boolean isSchema) {
        this.isSchema = isSchema;
        ResourceExtractor ex = new ResourceExtractor();
        try {
            for (ImmutablePair<String, RDFFormat> p : pathAndFormats)
                inputs.add(new RDFInputFile(ex.extract(p.getKey()), p.getValue()));
        } catch (IOException e) {
            throw new RuntimeException("Resource extraction failure.", e);
        }
    }

    @Override
    public Model getModel() {
        if (model == null) {
            model = ModelFactory.createDefaultModel();
            for (RDFInput input : inputs) {
                try (FileInputStream stream = new FileInputStream(input.toFile())) {
                    model.read(stream, input.getBaseURI(), input.getLang().getName());
                } catch (IOException e) {
                    //will never fail, trust me
                    throw new RuntimeException(e);
                }
            }
        }
        return model;
    }

    @Override
    public List<RDFInput> getInputs() {
        return inputs;
    }

    @Override
    public boolean isSchema() {
        return isSchema;
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    @Override
    public void close() throws Exception {
        if (model != null)
            model.close();
        for (RDFInput input : inputs) input.close();
    }
}

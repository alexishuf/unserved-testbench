package br.ufsc.lapesd.unserved.testbench.util;

import org.apache.jena.rdf.model.Resource;

import javax.annotation.Nonnull;
import java.util.Iterator;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public interface MatchTable {
    @Nonnull
    Relation get(@Nonnull Resource left, @Nonnull Resource right);

    void set(@Nonnull Resource left, @Nonnull Resource right, @Nonnull Relation relation);

    boolean contains(@Nonnull Resource resource);

    Iterator<Resource> rightIterator(@Nonnull Resource left, @Nonnull Relation relation);
    default Stream<Resource> rightStream(@Nonnull Resource left, @Nonnull Relation relation) {
        return StreamSupport.stream(
                Spliterators.spliteratorUnknownSize(rightIterator(left, relation),
                    Spliterator.DISTINCT|Spliterator.NONNULL),
                false);
    }

    Iterator<Resource> closureIterator(@Nonnull Resource left, @Nonnull Relation relation);
    default Stream<Resource> closureStream(@Nonnull Resource left, @Nonnull Relation relation) {
        return StreamSupport.stream(
                Spliterators.spliteratorUnknownSize(closureIterator(left, relation),
                        Spliterator.DISTINCT|Spliterator.NONNULL),
                false);
    }

    enum Relation {
        UNKNOWN,
        MISMATCH,
        SUPERCLASS,
        SUBCLASS,
        SAME
    }
}

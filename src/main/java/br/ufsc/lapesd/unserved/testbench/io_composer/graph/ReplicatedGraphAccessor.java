package br.ufsc.lapesd.unserved.testbench.io_composer.graph;

import br.ufsc.lapesd.unserved.testbench.io_composer.impl.IOComposerInput;

public interface ReplicatedGraphAccessor<State, Cost, Transition>
        extends GraphAccessor<State, Cost, Transition> {
    ReplicatedGraphAccessor duplicate(IOComposerInput input);
}

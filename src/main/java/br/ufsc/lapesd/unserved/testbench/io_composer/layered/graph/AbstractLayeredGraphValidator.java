package br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph;

import br.ufsc.lapesd.unserved.testbench.process.data.DataContext;
import br.ufsc.lapesd.unserved.testbench.process.data.DataContextDiff;
import com.google.common.base.Preconditions;

import javax.annotation.Nonnull;

public abstract class AbstractLayeredGraphValidator<A, V, E>
        implements LayeredGraph.Validator<A, V> {
    protected final @Nonnull E edge;
    protected final @Nonnull DataContext dataContext;
    protected DataContextDiff diff = null;
    protected boolean failed = false;
    protected boolean ready = false;
    protected boolean adapted = false;
    protected DataContextDiff adaptationDiff = null;
    protected V newStart = null;

    protected AbstractLayeredGraphValidator(@Nonnull E edge, @Nonnull DataContext dataContext) {
        this.edge = edge;
        this.dataContext = dataContext;
    }

    protected
    AbstractLayeredGraphValidator(@Nonnull AbstractLayeredGraphValidator<A, V, E> other) {
        edge = other.edge;
        dataContext = other.dataContext; //< dataContext is never modified, so this is safe
        diff = other.diff;
        failed = other.failed;
        ready = other.ready;
        adapted = other.adapted;
        adaptationDiff = other.adaptationDiff;
        newStart = other.newStart;
    }

    @Override
    public boolean isFailed() {
        return failed;
    }
    @Override
    public boolean isReady() {
        return ready;
    }

    @Nonnull
    @Override
    public DataContextDiff getDiff() {
        Preconditions.checkState(isReady());
        assert diff != null;
        return diff;
    }

    @Nonnull
    @Override
    public V getNewStart() {
        Preconditions.checkState(adapted);
        assert newStart != null;
        return newStart;
    }
    @Nonnull
    @Override
    public DataContextDiff getAdaptationDiff() {
        Preconditions.checkState(adapted);
        assert adaptationDiff != null;
        return adaptationDiff;
    }
}

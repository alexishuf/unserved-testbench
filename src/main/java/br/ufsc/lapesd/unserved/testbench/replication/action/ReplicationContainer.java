package br.ufsc.lapesd.unserved.testbench.replication.action;

import br.ufsc.lapesd.unserved.testbench.replication.ReplicatedExecution;
import br.ufsc.lapesd.unserved.testbench.replication.model.Replication;
import br.ufsc.lapesd.unserved.testbench.replication.model.ReplicationAction;
import br.ufsc.lapesd.unserved.testbench.util.jena.UncloseableGraph;
import com.google.common.base.Preconditions;
import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.graph.compose.Union;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.sparql.graph.GraphFactory;
import org.apache.jena.sparql.graph.UnmodifiableGraph;
import org.apache.jena.vocabulary.RDF;

import javax.annotation.Nonnull;
import java.util.*;

public class ReplicationContainer implements AutoCloseable {
    @Nonnull
    private final Model model;
    @Nonnull private final Resource realAction;
    @Nonnull private final ReplicatedExecution execution;
    @Nonnull private final ReplicationAction action;
    private final String uuid = UUID.randomUUID().toString();
    private boolean wasForked = false;
    private final HashSet<ReplicationContainer> forks = new HashSet<>();
    private ReplicationContainer parent = null;
    private Progress progress = null;


    private static final Map<String, ReplicationContainer> containerMap
            = Collections.synchronizedMap(new HashMap<>());

    public ReplicationContainer(@Nonnull Model model,
                                @Nonnull Resource realAction,
                                @Nonnull ReplicatedExecution execution) {
        Preconditions.checkArgument(!model.isClosed());
        if (realAction.getModel() != model) {
            Preconditions.checkArgument(model.containsResource(realAction));
            realAction = realAction.isAnon() ? model.createResource(realAction.getId())
                    : model.createResource(realAction.getURI());
        }

        this.model = model;
        this.realAction = realAction;
        this.execution = execution;
        action = model.createResource()
                .addProperty(Replication.wrappedAction, realAction)
                .addProperty(Replication.containerId, uuid, XSDDatatype.XSDstring)
                .addProperty(RDF.type, Replication.ReplicationAction)
                .as(ReplicationAction.class);
        containerMap.put(uuid, this);
    }
    public ReplicationContainer(@Nonnull Resource realAction,
                                @Nonnull ReplicatedExecution execution) {
        this(ModelFactory.createModelForGraph(new Union(GraphFactory.createGraphMem(),
                new UncloseableGraph(new UnmodifiableGraph(realAction.getModel().getGraph())))),
                realAction, execution);
    }

    private ReplicationContainer(ReplicationContainer other) {
        this.model = other.model;
        this.realAction = other.realAction;
        this.execution = other.execution.duplicate();
        this.action = other.action;
    }

    public ReplicationContainer fork() {
        ReplicationContainer fork = new ReplicationContainer(this);
        fork.parent = this;
        wasForked = true;
        forks.add(fork);
        return fork;
    }

    public boolean wasForked() {
        return wasForked;
    }

    public synchronized void join() throws InterruptedException {
        while (!forks.isEmpty()) wait();
    }

    private synchronized void notifyChildClosed(ReplicationContainer child) {
        forks.remove(child);
        notifyAll();
    }


    public static ReplicationContainer fromUUID(String containerId) {
        return containerMap.getOrDefault(containerId, null);
    }

    @Nonnull
    public Resource getRealAction() {
        return realAction;
    }

    @Nonnull
    public ReplicationAction getAction() {
        return action;
    }

    @Nonnull
    public ReplicatedExecution getExecution() {
        return execution;
    }

    public Progress getProgress() {
        return progress;
    }
    public void setProgress(Progress progress) {
        this.progress = progress;
    }

    @Override
    public void close() {
        containerMap.remove(uuid);
        if (parent != null) parent.notifyChildClosed(this);
        /* since model is shared among all fork()s, close on finalizer */
    }
}

package br.ufsc.lapesd.unserved.testbench.components.expressions.evaluator;

import br.ufsc.lapesd.unserved.testbench.components.expressions.*;
import br.ufsc.lapesd.unserved.testbench.components.expressions.evaluator.instance.*;
import org.apache.jena.rdf.model.RDFNode;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.stream.Collectors;

public class ClassExpressionInstanceEvaluator extends AbstractClassExpressionEvaluator<RDFNode> {

    public ClassExpressionInstanceEvaluator(@Nonnull ClassExpression classExpression,
                                            @Nonnull RDFNode argument) {
        super(classExpression, createStep(classExpression), argument);
    }

    private static Step<RDFNode> createStep(ClassExpression expression) {
        //TODO short-circuit steps (ie. do not evaluate siblings as soon as a node evaluated is sufficient for determining falsehood (or truth) of parent
        //TODO optimize evaluation by first evaluating tests that often infer falsehood
        switch (expression.getType()) {
            case Connective:
                List<Step<RDFNode>> children = expression.getChildren().stream()
                        .map(ClassExpressionInstanceEvaluator::createStep)
                        .collect(Collectors.toList());
                return new ConnectiveInstanceStep(children, (Connective)expression);
            case PropertyRestriction:
                PropertyRestriction p = (PropertyRestriction) expression;
                if (p.getRestrictionType().hasRangeClass())
                    return new PropertyRestrictionInstanceStep(p, createStep(p.getRangeClass()));
                else
                    return new PropertyRestrictionInstanceStep(p);
            case Cardinality:
                Cardinality c = (Cardinality) expression;
                if (c.getCardinalityType().isQualified())
                    return new CardinalityInstanceStep(c, createStep(c.getRangeQualification()));
                else
                    return new CardinalityInstanceStep(c);
            case Enumeration:
                return new EnumerationInstanceStep((Enumeration)expression);
            case Atomic:
                return new AtomicInstanceStep((AtomicClassExpression) expression);
        }
        throw new UnsupportedOperationException();
    }
}

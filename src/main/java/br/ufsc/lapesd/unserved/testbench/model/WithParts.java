package br.ufsc.lapesd.unserved.testbench.model;

import org.apache.jena.rdf.model.Resource;

import java.util.List;
import java.util.Set;

/**
 * Something that has unserved:part(s). This has no class in uNSERVED
 * and therefore cannot be used with <code>RDFNOde.as()</code>
 */
public interface WithParts extends Resource {
    List<Part> getParts();
    List<Variable> getVariables();

    Set<Part> getPartsRecursive();
    Set<Variable> getVariablesRecursive();
}

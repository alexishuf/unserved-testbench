package br.ufsc.lapesd.unserved.testbench.pragproof.context;

import br.ufsc.lapesd.unserved.testbench.input.RDFInput;
import br.ufsc.lapesd.unserved.testbench.process.data.DataContext;
import org.apache.jena.rdf.model.Resource;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.List;

/**
 * This is the context data of the Pragmatic Proof algorithm (R, g, H, B) in [1].
 *
 * [1]: http://dx.doi.org/10.1017/S1471068416000016
 */
public interface PragProofContext extends AutoCloseable {
    List<RDFInput> getAllInputs();

    PragProofRules getRules();
    PragProofQuery getQuery();

    /**
     * This is similar to createNonIsolatedCopy() in {@link DataContext}.
     *
     * Creates a new {@link PragProofContext} on which changes will not affect this context. It
     * is allowed for an implementation that changes on this {@link PragProofContext} be
     * visible on the copy context.
     *
     * @return non-isolated copy of this context.
     */
    @Nonnull
    PragProofContext createNonIsolatedCopy();

    @Nonnull Collection<Resource> getInputWantedVariables();

    Resource getWantedVariable(Resource inputResource);

    @Nonnull DataContext getDataContext();

    @Override
    void close();
}

package br.ufsc.lapesd.unserved.testbench.io_composer.conditions.index;

import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.atomic.VariableSpec;
import br.ufsc.lapesd.unserved.testbench.util.TransitiveClosureGetter;
import com.google.common.base.Preconditions;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.OWL2;
import org.apache.jena.vocabulary.RDFS;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.apache.commons.lang3.tuple.ImmutablePair.of;

/**
 * Indexes values by {@link VariableSpec}, allows querying with a {@link VariableSpec}.
 * Querying matches any entry whose both type and representation are in the subClassOf
 * closure of the given key (i.e., Class.isAssignableFrom)..
 *
 * @param <V> indexed value
 */
public class VariableSpecIndex<V> implements ConditionIndex<VariableSpec, V> {
    private final @Nonnull TransitiveIndex<ImmutablePair<VariableSpec, V>> index;
    private final @Nonnull TransitiveClosureGetter cGetter;

    public VariableSpecIndex(@Nonnull TransitiveClosureGetter closureGetter) {
        this(closureGetter, false);
    }

    protected VariableSpecIndex(@Nonnull TransitiveClosureGetter closureGetter,
                                boolean closureOnPut) {
        Preconditions.checkArgument(closureGetter.getPredicate().equals(RDFS.subClassOf));
        this.cGetter = closureGetter;
        this.index = new TransitiveIndex<>(closureGetter, closureOnPut);
        this.index.addTopResource(OWL2.Thing).addTopResource(RDFS.Resource);
    }

    /**
     * Gets index that runs the {@link TransitiveClosureGetter} on put, not on get operations.
     * Functionality of such getter is the same as if getter backward flag where flipped and
     * the closure where done on get. This may impose a memory usage drawback if closures for
     * types are large, typically it is a good idea to use a forward closure getter with this
     * constructor (i.e., a superclass getter).
     *
     * @param closureGetter The closure getter
     * @param <V> value type
     * @return the new index
     */
    @Nonnull
    public static <V> VariableSpecIndex<V>
    withClosureOnPut(@Nonnull TransitiveClosureGetter closureGetter) {
        return new VariableSpecIndex<>(closureGetter, true);
    }

    @Override
    public void put(@Nonnull VariableSpec condition, @Nonnull V value) {
        index.put(condition.getType(), of(condition, value));
    }

    @Override
    public void optimize() { /* pass */ }

    @Override
    public boolean remove(@Nonnull VariableSpec condition) {
        return index.remove(condition.getType());
    }

    @Override
    public boolean remove(@Nonnull VariableSpec condition, @Nonnull V value) {
        return index.remove(condition.getType(), of(condition, value));
    }

    /**
     * Gets all values indexed from a VariableSpec x s.t. x.getType() and x.getRepresentation()
     * are in the subClassOf closure of their corresponding fields in spec. If
     * <code>spec.getRepresentation() == null</code>, then any representation class is accepted.
     *
     * @param spec key
     * @return A distinct and nonnull stream. Will be empty, if there are no matches.
     */
    public @Nonnull Stream<V> getStream(@Nonnull VariableSpec spec) {
        return getNonDistinctStream(spec).distinct();
    }

    public @Nonnull Stream<V> getNonDistinctStream(@Nonnull VariableSpec spec) {
        Resource rep = spec.getRepresentation();
        if (index.isClosureOnPut()) {
            //spec.representation subclassof indexed.representation
            Stream<ImmutablePair<VariableSpec, V>> stream = index.getStream(spec.getType());
            if (rep != null && !rep.equals(OWL2.Thing) && !rep.equals(RDFS.Resource))
                stream = stream.filter(p -> p.left.getRepresentation() == null
                            || cGetter.getClosureSet(p.left.getRepresentation()).contains(rep));
            return stream.map(p -> p.right);
        } else {
            //indexed.representation subclassof spec.representation
            Set<Resource> vReps = rep == null || rep.equals(OWL2.Thing) || rep.equals(RDFS.Resource) ? null
                    : cGetter.getClosureSet(rep);
            return index.getStream(spec.getType())
                    .filter(p -> vReps == null || vReps.contains(p.left.getRepresentation()))
                    .map(ImmutablePair::getRight);
        }
    }

    @Nullable
    public V getFirst(@Nonnull VariableSpec spec) {
        Resource rep = spec.getRepresentation();
        if (index.isClosureOnPut()) {
            Set<ImmutablePair<VariableSpec, V>> set = index.get(spec.getType());
            for (ImmutablePair<VariableSpec, V> pair : set) {
                Resource idxRep = pair.left.getRepresentation();
                if (idxRep == null || idxRep.equals(rep)
                        || cGetter.getClosureSet(idxRep).contains(rep)) {
                    return pair.right;
                }
            }
            return null;
        } else {
            return getNonDistinctStream(spec).findFirst().orElse(null);
        }
    }

    /**
     * Equivalent to <code>getStream(spec).collect(Collectors.toSet())</code>.
     */
    public @Nonnull Set<V> get(@Nonnull VariableSpec spec) {
        return getStream(spec).collect(Collectors.toSet());
    }
}

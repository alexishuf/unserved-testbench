package br.ufsc.lapesd.unserved.testbench.benchmarks.design1;

import br.ufsc.lapesd.unserved.testbench.Algorithm;
import br.ufsc.lapesd.unserved.testbench.AlgorithmFamily;
import br.ufsc.lapesd.unserved.testbench.benchmarks.design1.data.D1ExperimentData;
import br.ufsc.lapesd.unserved.testbench.benchmarks.experiment.ExperimentException;
import br.ufsc.lapesd.unserved.testbench.benchmarks.restdesc.RESTdescPragProofApp;
import com.google.common.base.Preconditions;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RESTdescPPD1Experiment extends BasicD1Experiment {
    private File restdescGoal;
    private File restdescInitial;

    public RESTdescPPD1Experiment(D1ExperimentData experimentData, D1Factors factors) {
        super(experimentData, factors);
        Preconditions.checkArgument(getFactors().getAlgorithm().getFamily()
                == AlgorithmFamily.RESTdescPP);
    }

    @Override
    public void setUp() throws Exception {
        super.setUp();
        restdescGoal = descriptionsData.getWantedFile();
        restdescInitial = descriptionsData.getKnownFile();
    }

    @Override
    protected void run(File workflowFile, File timesFile, List<File> fileList) throws ExperimentException {
        List<String> args = new ArrayList<>(Arrays.asList(RESTdescPragProofApp.class.getName(),
                "--preheat-count", String.valueOf(getFactors().getPreheatCount()),
                "--measure-reasoner-spawn"));
        if (getFactors().getAlgorithm() != Algorithm.RESTdescPPWithUnstableReasoner)
            args.add("--cheat-unstable-reasoner");
        if (getFactors().getExecute()) args.add("--execute");
        args.addAll(Arrays.asList(
                "--workflow-out", workflowFile.getAbsolutePath(),
                "--times-out", timesFile.getAbsolutePath(),
                "--goal", restdescGoal.getAbsolutePath(),
                restdescInitial.getAbsolutePath()));
        runChildJVM(args, fileList);
    }
}

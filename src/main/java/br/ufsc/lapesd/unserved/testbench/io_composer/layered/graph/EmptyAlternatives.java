package br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph;

import java.util.Iterator;
import java.util.NoSuchElementException;

public final class EmptyAlternatives<T extends Alternative<?>> implements Alternatives<T> {
    @Override
    public Iterator<T> iterator() {
        return new Iterator<T>() {
            @Override
            public boolean hasNext() {
                return false;
            }
            @Override
            public T next() {
                throw new NoSuchElementException();
            }
        };
    }

    @Override
    public boolean remove(T object) {
        return false;
    }

    @Override
    public Alternatives<T> duplicate() {
        return this; //immutable
    }
}

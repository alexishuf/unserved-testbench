package br.ufsc.lapesd.unserved.testbench.io_composer.graph;

public class EmptyChangeSet implements ChangeSet {
    @Override
    public void undo() {
        /* pass */
    }
    @Override
    public boolean isEmpty() {
        return true;
    }
}

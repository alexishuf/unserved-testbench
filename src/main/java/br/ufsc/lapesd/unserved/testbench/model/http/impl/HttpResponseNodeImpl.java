package br.ufsc.lapesd.unserved.testbench.model.http.impl;

import br.ufsc.lapesd.unserved.testbench.iri.UnservedX;
import br.ufsc.lapesd.unserved.testbench.model.http.HttpResponseNode;
import br.ufsc.lapesd.unserved.testbench.util.ImplementationByType;
import org.apache.jena.enhanced.EnhGraph;
import org.apache.jena.enhanced.EnhNode;
import org.apache.jena.enhanced.Implementation;
import org.apache.jena.graph.Node;

public class HttpResponseNodeImpl extends AbstractHttpMessageNode implements HttpResponseNode {
    public static Implementation factory
            = new ImplementationByType(UnservedX.HTTP.HttpResponse.asNode()) {
        @Override
        public EnhNode wrap(Node node, EnhGraph eg) {
            return new HttpResponseNodeImpl(node, eg);
        }
    };

    public HttpResponseNodeImpl(Node n, EnhGraph m) {
        super(n, m);
    }
}

package br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service;

import br.ufsc.lapesd.unserved.testbench.benchmarks.wsc08.Wscc;
import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.And;
import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.parsers.SimpleSPINConditionsParser;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.cost.CostInfo;
import br.ufsc.lapesd.unserved.testbench.model.Message;
import br.ufsc.lapesd.unserved.testbench.model.Part;
import br.ufsc.lapesd.unserved.testbench.model.Variable;
import br.ufsc.lapesd.unserved.testbench.util.DangerousArrayListSet;
import org.apache.jena.rdf.model.Statement;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * A node represents a service, which for uNSERVED is a pair of antecedent and consequent messages.
 */
public class ServiceNode extends AbstractNode implements Node {
    private @Nonnull Message antecedent;
    private Message consequent;
    private Set<Variable> inputsCache = null;
    private Set<Variable> outputsCache = null;
    private @Nullable And preconditions;
    private @Nullable And postconditions;
    private @Nonnull CostInfo costInfo;

    public ServiceNode(@Nonnull Message antecedent, Message consequent,
                       @Nonnull List<Node> alternatives,
                       @Nullable And preconditions, @Nullable And postconditions, @Nonnull CostInfo costInfo) {
        super(alternatives);
        this.antecedent = antecedent;
        this.consequent = consequent;
        this.preconditions = preconditions;
        this.postconditions = postconditions;
        this.costInfo = costInfo;
    }

    public ServiceNode(@Nonnull Message antecedent, Message consequent,
                       @Nonnull List<Node> alternatives,
                       @Nullable And preconditions, @Nullable And postconditions,
                       CostInfo costInfo, @Nonnull Set<Variable> inputs) {
        this(antecedent, consequent, alternatives, preconditions, postconditions, costInfo);
        this.inputsCache = inputs;
    }

    public ServiceNode(@Nonnull Message antecedent, Message consequent,
                       @Nonnull List<Node> alternatives,
                       @Nullable And preconditions, @Nullable And postconditions,
                       CostInfo costInfo, @Nonnull Set<Variable> inputs,
                       @Nonnull Set<Variable> outputs) {
        this(antecedent, consequent, alternatives, preconditions, postconditions, costInfo, inputs);
        this.outputsCache = outputs;
    }

    public ServiceNode(ServiceNode other) {
        super(other);
        antecedent = other.getAntecedent();
        consequent = other.getConsequent();
        preconditions = other.getPreConditions();
        postconditions = other.getPostConditions();
        costInfo = other.costInfo;
        inputsCache = other.inputsCache;
        outputsCache = other.outputsCache;
    }

    public ServiceNode(ServiceNode other, @Nonnull List<Node> alternatives) {
        super(other, alternatives);
        antecedent = other.getAntecedent();
        consequent = other.getConsequent();
        preconditions = other.getPreConditions();
        postconditions = other.getPostConditions();
        costInfo = other.costInfo;
        inputsCache = other.inputsCache;
        outputsCache = other.outputsCache;
    }

    @Nonnull
    public Message getAntecedent() {
        return antecedent;
    }

    public Message getConsequent() {
        return consequent;
    }

    @Nonnull
    @Override
    public Set<Variable> getInputs() {
        if (inputsCache == null) {
            inputsCache = antecedent.getParts().stream().map(Part::getVariable)
                    .collect(Collectors.toCollection(DangerousArrayListSet::new));
        }
        return inputsCache;
    }

    @Nonnull
    @Override
    public Set<Variable> getOutputs() {
        if (outputsCache == null) {
            outputsCache = consequent.getPartsRecursive().stream().map(Part::getVariable)
                    .collect(Collectors.toCollection(DangerousArrayListSet::new));
        }
        return outputsCache;
    }

    @Nonnull
    @Override
    public And getPreConditions() {
        if (preconditions == null)
            preconditions = new SimpleSPINConditionsParser().parse(antecedent);
        return preconditions;
    }

    @Nonnull
    @Override
    public And getPostConditions() {
        if (postconditions == null)
            postconditions = new SimpleSPINConditionsParser().parse(consequent);
        return postconditions;
    }

    @Override
    @Nonnull
    public CostInfo getCostInfo() {
        return costInfo;
    }

    @Override
    public Object getId() {
        return this;
    }

    @Override
    public String toString() {
        Statement statement = antecedent.getProperty(Wscc.wscService);
        String costString = costInfo == CostInfo.EMPTY ? "" : "," +costInfo.toString();
        if (statement != null) {
            return String.format("ServiceNode(%s%s)",
                    statement.getLiteral().getLexicalForm().split("#")[1], costString);
        }
        return String.format("ServiceNode(%s => %s%s)", String.valueOf(antecedent),
                String.valueOf(consequent), costString);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ServiceNode that = (ServiceNode) o;

        return antecedent.equals(that.antecedent) &&
                (consequent != null ? consequent.equals(that.consequent) : that.consequent == null);
    }

    @Override
    public int hashCode() {
        int result = antecedent.hashCode();
        result = 31 * result + (consequent != null ? consequent.hashCode() : 0);
        return result;
    }
}

package br.ufsc.lapesd.unserved.testbench.benchmarks.design1;

import br.ufsc.lapesd.unserved.testbench.benchmarks.compose.TimesExperimentResult;
import br.ufsc.lapesd.unserved.testbench.benchmarks.design1.data.D1ExperimentData;
import br.ufsc.lapesd.unserved.testbench.benchmarks.experiment.AbstractExperimentDesignRunner;
import br.ufsc.lapesd.unserved.testbench.benchmarks.experiment.AbstractExperimentLevelsDesignRunner;
import br.ufsc.lapesd.unserved.testbench.benchmarks.experiment.ExperimentDesignLevels;
import br.ufsc.lapesd.unserved.testbench.benchmarks.experiment.ExperimentResult;
import com.google.gson.Gson;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class D1ExperimentRunner extends AbstractExperimentLevelsDesignRunner {
    @Option(name = "--experiment-data", required = true,
            usage = "Directory containing all experiment generated data. If empty, data will be " +
                    "generated")
    private File experimentDataDir = null;

    public D1ExperimentRunner() {
        super(new D1ExperimentFactory());
    }

    public static void main(String[] args) throws Exception {
        D1ExperimentRunner runner = new D1ExperimentRunner();
        new CmdLineParser(runner).parseArgument(args);
        runner.run();
    }

    @Override
    protected void beforeLoop() throws Exception {
        D1ExperimentData data = null;
        if (experimentDataDir.exists()) {
            if (experimentDataDir.isDirectory()) {
                data = D1ExperimentData.load(experimentDataDir); //null if no index
            } else {
                throw new Exception("--experiment-data is a regular file.");
            }
        }
        if (data == null) data = new D1ExperimentData(experimentDataDir);

        List<D1Factors> factorsList = new ArrayList<>();
        IntStream.range(0, design.rows.size()).parallel()
                .mapToObj(i -> (D1Factors) levels.createFactors(design.getFactors(i)))
                .forEachOrdered(factorsList::add);

        long hasData = factorsList.stream().filter(data::hasData).count();
        if (hasData > 0 && hasData != design.rows.size())
            throw new Exception("--experiment-data is not complete. Remove it and try again");
        if (hasData == 0) {
            data.generateData(factorsList);
            data.save(experimentDataDir);
        }
        ((D1ExperimentFactory)experimentFactory).setExperimentData(data);
    }

    @Override
    protected ExperimentDesignLevels loadLevels() throws FileNotFoundException {
        return new Gson().fromJson(new FileReader(levelsJson), D1ExperimentLevels.class);
    }

    @Override
    protected List<String> getResultHeaders() {
        return Arrays.asList("initialization", "output", "composition", "blacklisting",
                "execution", "total");
    }

    @Override
    protected List<String> getResultValues(ExperimentResult result) {
        TimesExperimentResult r = (TimesExperimentResult) result;
        return Stream.of(r.getInitialization(), r.getOutput(), r.getComposition(),
                r.getBlacklisting(), r.getExecution(), r.getTotal()).map(String::valueOf)
                .collect(Collectors.toList());
    }

    public static class Builder extends AbstractExperimentLevelsDesignRunner.Builder {
        public Builder(File designCsv, File levelsJson, File resultObjectsDir) {
            super(designCsv, levelsJson, resultObjectsDir);
        }

        @Override
        public AbstractExperimentDesignRunner build() {
            return setupAbstractRunner(new D1ExperimentRunner());
        }
    }
}

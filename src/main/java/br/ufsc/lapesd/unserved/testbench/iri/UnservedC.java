package br.ufsc.lapesd.unserved.testbench.iri;

import br.ufsc.lapesd.unserved.testbench.util.ResourcesBackground;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.riot.RDFFormat;

import java.util.Collections;

public class UnservedC extends ResourcesBackground {
    public static final String IRI = "https://alexishuf.bitbucket.io/2016/04/unserved/unserved-c.ttl";
    public static final String PREFIX = IRI + "#";
    public static final String PREFIX_SHORT = "unserved-c";

    public UnservedC() {
        super(Collections.singletonList(ImmutablePair.of("unserved/unserved-c.ttl",
                RDFFormat.TURTLE)), true);
    }
    static UnservedC instance = new UnservedC();
    public static UnservedC getInstance() {
        return instance;
    }

    public static final Resource Component = getInstance().getModel().createResource(PREFIX + "Component");

    public static final Resource ActionRunner = getInstance().getModel().createResource(PREFIX + "ActionRunner");
    public static final Resource Renderer = getInstance().getModel().createResource(PREFIX + "Renderer");
    public static final Resource Parser = getInstance().getModel().createResource(PREFIX + "Parser");

    public static final Property actionClass = getInstance().getModel().createProperty(PREFIX + "actionClass");
    public static final Property valueClass = getInstance().getModel().createProperty(PREFIX + "valueClass");
    public static final Property representationClass = getInstance().getModel().createProperty(PREFIX + "representationClass");
}

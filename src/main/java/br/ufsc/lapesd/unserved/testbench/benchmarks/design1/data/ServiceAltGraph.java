package br.ufsc.lapesd.unserved.testbench.benchmarks.design1.data;

import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

public class ServiceAltGraph extends ServiceGraph implements Serializable {
    private Map<Service, Set<Service>> alternatives = new HashMap<>();

    private ServiceAltGraph(ServiceGraph other) {
        super(other);
    }

    public Set<Service> getAlternatives(Service service) {
        return Collections.unmodifiableSet(alternatives.getOrDefault(service,
                Collections.emptySet()));
    }

    public static ServiceAltGraph from(ServiceGraph graph, ServiceAltGraphFactors factors) {
        ServiceAltGraph altGraph = new ServiceAltGraph(graph);
        altGraph.initAlternatives(factors);
        return altGraph;
    }

    private void initAlternatives(ServiceAltGraphFactors factors) {
        getParallelSequences().stream().flatMap(List::stream).forEach(s -> {
            HashSet<Service> set = new HashSet<>();
            for (int i = 0; i < factors.getAlternatives(); i++) {
                Service alt = createNextService();
                set.add(alt);
                s.getOutgoing().forEach(outConn -> connection(replaced(outConn, s, alt))
                        .to(replaced(outConn.getNext(), s, alt)).add());
                s.getIncoming().forEach(inConn -> connection(replaced(inConn.getPrev(), s, alt))
                        .to(replaced(inConn, s, alt)).add());
            }
            alternatives.put(s, set);
        });
    }

    private static Set<Service>
    replaced(Set<Service> services, Service victim, Service replacement) {
        return services.stream().map(s -> Objects.equals(s, victim) ? replacement : s)
                .collect(Collectors.toSet());
    }
}

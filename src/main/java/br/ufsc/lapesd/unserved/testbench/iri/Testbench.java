package br.ufsc.lapesd.unserved.testbench.iri;

public class Testbench {
    public static final String IRI = "https://alexishuf.bitbucket.io/2016/04/unserved/testbench";
    public static final String PREFIX = IRI + "#";
    public static final String PREFIX_SHORT = "testbench";
}

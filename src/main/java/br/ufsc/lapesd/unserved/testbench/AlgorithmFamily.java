package br.ufsc.lapesd.unserved.testbench;

import java.util.Collections;
import java.util.List;

/**
 * Classification for Algorithm values.
 */
public enum AlgorithmFamily {
    /**
     * Algorithms based on Pragmatic Proof that use specifically RESTdesc as input.
     *
     * VERBORGH, R., ARNDT, D., VAN HOECKE, S., DE ROO, J. O. S., MELS, G., STEINER, T., & GABARRO, J. (2016). The pragmatic proof: Hypermedia API composition and execution. Theory and Practice of Logic Programming, FirstView, 1–48. article. http://doi.org/10.1017/S1471068416000016
     */
    RESTdescPP,
    /**
     * Algorithms that reduce service composition to path-finding in a graph.
     *
     * Rodriguez-Mier, P., Pedrinaci, C., Lama, M., & Mucientes, M. (2016). An Integrated Semantic Web Service Discovery and Composition Framework. IEEE Transactions on Services Computing. http://doi.org/10.1109/TSC.2015.2402679
     */
    IOGraphPath;

    public List<AlgorithmFamily> getParents() {
        return Collections.emptyList();
    }
}

package br.ufsc.lapesd.unserved.testbench.reasoner;

/**
 * Creates N3Reasoner instances
 */
public interface N3ReasonerFactory {
    N3Reasoner createReasoner();
}

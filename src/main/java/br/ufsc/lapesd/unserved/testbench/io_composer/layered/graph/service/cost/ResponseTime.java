package br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.cost;

import javax.annotation.Nonnull;

public class ResponseTime implements CostParameter {
    private double milliseconds;
    public ResponseTime(double milliseconds) {
        this.milliseconds = milliseconds;
    }

    public static String NAME = "ResponseTime";
    public static ResponseTime IDENTITY = new ResponseTime(0);

    @Nonnull
    @Override
    public String getName() {
        return NAME;
    }

    public double getMilliseconds() {
        return milliseconds;
    }

    @Override
    public CostParameter aggregateSequence(CostParameter right) {
        return new ResponseTime(milliseconds + ((ResponseTime)right).milliseconds);
    }

    @Override
    public CostParameter aggregateParallel(CostParameter right) {
        return new ResponseTime(Math.max(milliseconds, ((ResponseTime)right).milliseconds));
    }

    @Override
    public int compare(CostParameter right) {
        return Double.compare(milliseconds, ((ResponseTime)right).milliseconds);
    }

    @Override
    public String toString() {
        return String.format("%s(%.3f)", NAME, milliseconds);
    }
}

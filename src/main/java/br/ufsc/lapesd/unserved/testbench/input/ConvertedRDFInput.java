package br.ufsc.lapesd.unserved.testbench.input;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.riot.RDFFormat;

import javax.annotation.Nonnull;
import java.io.IOException;


public class ConvertedRDFInput extends AbstractRDFInput {
    private final RDFInput source;

    public ConvertedRDFInput(RDFInput source, @Nonnull RDFFormat format) {
        super(format);
        this.source = source;
    }

    @Override
    protected Model loadModel() throws IOException {
        return source.getModel();
    }

    @Override
    public void close() throws IOException {
        source.close();
        super.close();
    }
}

package br.ufsc.lapesd.unserved.testbench.io_composer.state;

import javax.annotation.Nonnull;
import java.util.*;

public class ExecutionPath<T> implements Iterable<T> {
    private final List<T> list = new LinkedList<>();

    public ExecutionPath() {
    }
    public ExecutionPath(@Nonnull ExecutionPath<T> other) {
        this(other.asList());
    }
    public ExecutionPath(@Nonnull List<T> list) {
        this.list.addAll(list);
    }

    public void append(ExecutionPath<T> path) {
        path.iterator().forEachRemaining(list::add);
    }

    public void add(T e) {
        list.add(e);
    }

    public void addAll(Collection<T> elements) {
        elements.forEach(this::add);
    }

    public int size() {
        return list.size();
    }

    @Override
    public Iterator<T> iterator() {
        return asList().iterator();
    }

    public ListIterator<T> listIterator() {
        return asList().listIterator();
    }

    public ListIterator<T> listIterator(int position) {
        return asList().listIterator(position);
    }

    public List<T> asList() {
        return Collections.unmodifiableList(list);
    }

    /**
     * Undoes append(path), strictly meaning that either all path elements are removed from
     * this or none, and only a contiguous segment of elements at the end of this list
     * will be removed.
     *
     * @param path path to be removed, if this path ends with it.
     * @return <code>true</code> iff the removal happened.
     */
    public boolean removeTailStrictly(ExecutionPath<T> path) {
        ListIterator<T> it = list.listIterator(list.size()), pIt = path.listIterator(path.size());
        while (pIt.hasPrevious()) {
            if (!it.hasPrevious()) return false;
            if (!it.previous().equals(pIt.previous())) return false;
        }

        it = list.listIterator();
        pIt = path.listIterator();
        while (pIt.hasPrevious() && it.hasPrevious()) {
            if (it.previous().equals(pIt.previous()))
                it.remove();
            else break;
        }
        return true;
    }

    /**
     * Removes a contiguous sequence from the start of this path that is equal to the whole of path.
     * @param path {@link ExecutionPath} fragment to be removed.
     * @return <code>true</code> iff the removal happened.
     */
    public boolean removeHeadStrictly(ExecutionPath<T> path) {
        Iterator<T> it = list.iterator(), pit = path.iterator();
        while (pit.hasNext()) {
            if (!it.hasNext()) return false;
            if (!it.next().equals(pit.next())) return false;
        }

        it = list.iterator();
        pit = path.iterator();
        while (pit.hasNext() && it.hasNext()) {
            if (it.next().equals(pit.next()))
                it.remove();
            else  break;
        }
        return true;
    }

    /**
     * Remove at most count elements from the head of the path
     * @param count Maximum number of elements to remove from the head.
     * @return Actual number of elements removed.
     */
    public int removeHead(int count) {
        int actual = 0;
        for (; actual < count && !list.isEmpty(); actual++)
            list.remove(0);
        return actual;
    }
}

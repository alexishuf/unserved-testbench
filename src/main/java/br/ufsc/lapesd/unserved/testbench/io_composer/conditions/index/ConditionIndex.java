package br.ufsc.lapesd.unserved.testbench.io_composer.conditions.index;

import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.Condition;

import javax.annotation.Nonnull;

/**
 * Indexes conditions using keys that vary according to the {@link Condition} implementation.
 * Null values are not admitted.
 *
 * @param <K> The condition implementation
 */
public interface ConditionIndex<K extends Condition, V> {
    /**
     * Indexes a condition. If already present, the old value is replaced with the given one.
     */
    void put(@Nonnull K condition, @Nonnull V value);

    /**
     * Applies implementation-defined optimizations. This may put the index in a unmodifiable state.
     */
    void optimize();

    /**
     * Removes all values associated to the condition
     *
     * @return true iff the condition was present in the index.
     */
    boolean remove(@Nonnull K condition);

    /**
     * Removes only the association from condition to value.
     * @return true iff, the association was already present.
     */
    boolean remove(@Nonnull K condition, @Nonnull V value);
}

package br.ufsc.lapesd.unserved.testbench.pragproof;

import br.ufsc.lapesd.unserved.testbench.input.RDFInput;
import br.ufsc.lapesd.unserved.testbench.pragproof.context.PragProofContext;
import br.ufsc.lapesd.unserved.testbench.reasoner.N3Reasoner;
import br.ufsc.lapesd.unserved.testbench.reasoner.N3ReasonerException;
import br.ufsc.lapesd.unserved.testbench.reasoner.N3ReasonerFactory;
import org.apache.commons.io.IOUtils;

import javax.annotation.Nonnull;
import java.io.IOException;

public class PragProofProver {
    private final N3ReasonerFactory reasonerFactory;

    protected PragProofProver(N3ReasonerFactory reasonerFactory) {
        this.reasonerFactory = reasonerFactory;
    }

    public String prove(@Nonnull PragProofContext context) throws PragProofProverException {
        N3Reasoner n3 = reasonerFactory.createReasoner();
        try {
            for (RDFInput input : context.getAllInputs()) n3.addInput(input);
            n3.setQueryFile(context.getQuery().getInput());
            n3.setExplainProof(true);

            try {
                String result = IOUtils.toString(n3.run());
                try {
                    n3.waitFor();
                } catch (InterruptedException ignored) {
                }
                return result;
            } catch (IOException | N3ReasonerException e) {
                throw new PragProofProverException("Problem with N3 reasoner", e);
            }
        } catch (PragProofProverException e) {
            throw e;
        } catch (IOException e) {
            throw new PragProofProverException("Problem writing temp ruleFiles", e);
        } catch (Exception e) {
            throw new PragProofProverException("Unhandled Exception", e);
        }
    }

}

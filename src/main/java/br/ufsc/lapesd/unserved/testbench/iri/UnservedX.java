package br.ufsc.lapesd.unserved.testbench.iri;

import br.ufsc.lapesd.unserved.testbench.model.http.HttpNodeFactory;
import br.ufsc.lapesd.unserved.testbench.util.ResourcesBackground;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.riot.RDFFormat;

import java.util.Collections;

public class UnservedX extends ResourcesBackground {
    public static final String IRI = "https://alexishuf.bitbucket.io/2016/04/unserved/unserved-x.ttl";
    public static final String PREFIX = IRI + "#";
    public static final String PREFIX_SHORT = "unservedx";

    public UnservedX() {
        super(Collections.singletonList(ImmutablePair.of("unserved/unserved-x.ttl",
                RDFFormat.TURTLE)), true);
    }

    public static class XSD extends ResourcesBackground {
        public static final String IRI = "https://alexishuf.bitbucket.io/2016/04/unserved/unserved-x/xsd.ttl";
        public static final String PREFIX = IRI + "#";
        public static final String PREFIX_SHORT = "ux-xsd";

        public XSD() {
            super(Collections.singletonList(ImmutablePair.of("unserved/unserved-x" +
                    "/xsd.ttl", RDFFormat.TURTLE)), true);
        }

        private static XSD instance = new XSD();
        public static XSD getInstance() {
            return instance;
        }

        public static final Resource XsdInfoset = getInstance().getModel().createResource(PREFIX + "XsdInfoset");

        public static final Resource XsdFile = getInstance().getModel().createResource(PREFIX + "XsdFile");
        public static final Property xsdType = getInstance().getModel().createProperty(PREFIX + "xsdType");
        public static final Property xsdTypeName = getInstance().getModel().createProperty(PREFIX + "xsdTypeName");

        public static final Property sourceFile = getInstance().getModel().createProperty(PREFIX + "sourceFile");
    }

    public static class WSDL extends ResourcesBackground {
        public static final String IRI = "https://alexishuf.bitbucket.io/2016/04/unserved/unserved-x/wsdl11.ttl";
        public static final String PREFIX = IRI + "#";
        public static final String PREFIX_SHORT = "ux-wsdl";

        private static WSDL instance = new WSDL();
        public static WSDL getInstance() {
            return instance;
        }

        public static final Resource WsdlMessage = getInstance().getModel().createResource(PREFIX + "WsdlMessage");
        public static final Resource WsdlReply = getInstance().getModel().createResource(PREFIX + "WsdlReply");
        public static final Resource WsdlRequest = getInstance().getModel().createResource(PREFIX + "WsdlRequest");
        public static final Resource WsdlFile = getInstance().getModel().createResource(PREFIX + "WsdlFile");

        public static final Property wsdlPortName = getInstance().getModel().createProperty(PREFIX + "wsdlPortName");
        public static final Property wsdlOperationName = getInstance().getModel().createProperty(PREFIX + "wsdlOperationName");
        public static final Property sourceWsdl = getInstance().getModel().createProperty(PREFIX + "sourceWsdl");

        public static final Property wsdlContent = getInstance().getModel().createProperty(PREFIX + "wsdlContent");

        public WSDL() {
            super(Collections.singletonList(ImmutablePair.of("unserved/unserved-x" +
                    "/wsdl11.ttl", RDFFormat.TURTLE)), true);
        }
    }

    public static class HTTP extends ResourcesBackground {
        public static String IRI = "https://alexishuf.bitbucket.io/2016/04/unserved/unserved-x/http.ttl";
        public static String PREFIX = IRI + "#";
        public static String PREFIX_SHORT = "ux-http";

        public HTTP() {
            super(Collections.singletonList(ImmutablePair.of("unserved/unserved-x" +
                    "/http.ttl", RDFFormat.TURTLE)), true);
        }

        private static HTTP instance = new HTTP();
        public static HTTP getInstance() {
            return instance;
        }

        public static final Resource HttpMessage = getInstance().getModel().createResource(PREFIX + "HttpMessage");
        public static final Resource HttpRequest = getInstance().getModel().createResource(PREFIX + "HttpRequest");
        public static final Resource HttpResponse = getInstance().getModel().createResource(PREFIX + "HttpResponse");

        public static final Resource AsHttpProperty = getInstance().getModel().createResource(PREFIX + "AsHttpProperty");
        public static final Resource URITemplateAssignment = getInstance().getModel().createResource(PREFIX + "URITemplateAssignment");
        public static final Property httpProperty = getInstance().getModel().createProperty(PREFIX + "httpProperty");
        public static final Property httpResource = getInstance().getModel().createProperty(PREFIX + "httpResource");
        public static final Property modifier = getInstance().getModel().createProperty(PREFIX + "modifier");

        public static final Property uriTemplate = getInstance().getModel().createProperty(PREFIX + "uriTemplate");

        public static final Property uriTemplateVariable = getInstance().getModel().createProperty(PREFIX + "uriTemplateVariable");

        static {
            HttpNodeFactory.install();
        }
    }

    public static class MediaType extends ResourcesBackground {
        public static final String IRI = "https://alexishuf.bitbucket.io/2016/04/unserved/unserved-x/media-type.ttl";
        public static final String PREFIX = IRI + "#";
        public static final String PREFIX_SHORT = "ux-mt";

        public MediaType() {
            super(Collections.singletonList(ImmutablePair.of("unserved/unserved-x/media-type.ttl", RDFFormat.TURTLE)), true);
        }

        private static MediaType instance = new MediaType();
        public static UnservedX.MediaType getInstance() {
            return instance;
        }

        public static final Resource MediaType = getInstance().getModel().createResource(PREFIX + "MediaType");
        public static final Property acceptMediaType = getInstance().getModel().createProperty(PREFIX + "acceptMediaType");

        public static final Property mediaTypeValue = getInstance().getModel().createProperty(PREFIX + "mediaTypeValue");;
    }

    public static class RDF extends ResourcesBackground {
        public static final String IRI = "https://alexishuf.bitbucket.io/2016/04/unserved/unserved-x/rdf.ttl";
        public static final String PREFIX = IRI + "#";
        public static final String PREFIX_SHORT = "ux-rdf";

        public RDF() {
            super(Collections.singletonList(ImmutablePair.of("unserved/unserved-x/" +
                    "rdf.ttl", RDFFormat.TURTLE)), true);
        }

        private static RDF instance = new RDF();
        public static UnservedX.RDF getInstance() {
            return instance;
        }

        public static final Resource RDF = getInstance().getModel().createResource(PREFIX + "RDF");
        public static final Resource AsProperty = getInstance().getModel().createResource(PREFIX + "AsProperty");

        public static final Property rdfMediaType = getInstance().getModel().createProperty(PREFIX + "rdfMediaType");
        public static final Property property = getInstance().getModel().createProperty(PREFIX + "property");
        public static final Property propertyOf = getInstance().getModel().createProperty(PREFIX + "propertyOf");
    }
}

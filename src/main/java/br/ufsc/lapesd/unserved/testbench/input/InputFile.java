package br.ufsc.lapesd.unserved.testbench.input;

import java.io.File;
import java.io.IOException;

public class InputFile extends AbstractInput {
    private final File file;

    public InputFile(String mediaType, File file) {
        super(mediaType);
        this.file = file;
    }

    @Override
    public File toFile() throws IOException {
        return file;
    }

    @Override
    public void close() throws IOException {
        /* pass */
    }

    @Override
    public String toString() {
        return String.format("InputFile(%s, %s)", getMediaType(), file.getAbsolutePath());
    }
}

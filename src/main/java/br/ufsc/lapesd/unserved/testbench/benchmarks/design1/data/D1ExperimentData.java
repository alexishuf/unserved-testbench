package br.ufsc.lapesd.unserved.testbench.benchmarks.design1.data;

import br.ufsc.lapesd.unserved.testbench.benchmarks.design1.D1Factors;
import br.ufsc.lapesd.unserved.testbench.benchmarks.experiment.Factors;
import com.google.common.base.Preconditions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.*;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class D1ExperimentData {
    private static Logger logger = LoggerFactory.getLogger(D1ExperimentData.class);
    private File dir = null;
    private Index index;

    public D1ExperimentData(File dir) {
        this.dir = dir;
        this.index = new Index();
    }

    public D1ExperimentData(File dir, Index index) {
        this.dir = dir;
        this.index = index;
    }

    /**
     * Get files with service descriptions, known and wanted variables/data for the experiment
     * identified by the given factors.
     * @param factors {@link Factors} {@link Factors} that identify the experiment.
     */
    @Nonnull
    public DescriptionsData getDescriptions(D1Factors factors) {
        Preconditions.checkState(hasData(factors));
        String id = index.descriptionsDataIds.get(factors);
        try {
            return DescriptionsData.load(dir, id);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public boolean hasData(D1Factors factors) {
        return index.descriptionsDataIds.containsKey(factors);
    }

    public void generateData(List<D1Factors> factorsList) throws IOException {
        factorsList = factorsList.stream().distinct().collect(Collectors.toList());
        Preconditions.checkState(factorsList.stream().noneMatch(this::hasData));
        Set<Integer> lengths = factorsList.stream().map(D1Factors::getSolutionLength)
                .collect(Collectors.toSet());
        int maxLength = lengths.stream().max(Integer::compareTo)
                .orElseThrow(IllegalArgumentException::new);
        int maxParallel = factorsList.stream().map(D1Factors::getSolutionParallelSequences)
                .max(Integer::compareTo).orElseThrow(IllegalArgumentException::new);

        Map<ServiceGraphFactors, ServiceGraph> serviceGraphs = new ConcurrentHashMap<>();
        Map<ServiceAltGraphFactors, ServiceAltGraph> serviceAltGraphs = new ConcurrentHashMap<>();
        Map<ServicesIOFactors, ServicesIOs> serviceIOs = new ConcurrentHashMap<>();

        logger.info("Generating ServiceGraphs...");
        factorsList.parallelStream().map(ServiceGraphFactors::new).distinct().forEach(f ->
                serviceGraphs.put(f, ServiceGraph.random(f, maxLength, maxParallel)));
        logger.info("Generating ServiceAltGraph...");
        factorsList.parallelStream().map(ServiceAltGraphFactors::new).distinct().forEach(f -> {
            ServiceGraph graph = serviceGraphs.get(f.getGraphFactors());
            serviceAltGraphs.put(f, ServiceAltGraph.from(graph, f));
        });
        logger.info("Generating ServicesIOs...");
        factorsList.parallelStream().map(ServicesIOFactors::new).distinct().forEach(f -> {
            ServiceAltGraph altGraph = serviceAltGraphs.get(f.getServiceAltGraphFactors());
            serviceIOs.put(f, ServicesIOs.withRandomIOs(altGraph, f, lengths));
        });

        for (D1Factors factors : factorsList) {
            ServicesIOFactors ioFactors = new ServicesIOFactors(factors);
            ServicesIOs ios = serviceIOs.get(ioFactors);
            assert ios != null;
            assert !index.descriptionsDataIds.containsKey(factors);
            String id = UUID.randomUUID().toString();
            DescriptionsData.create(factors, ios, dir, id); //writes to the filesystem
            index.descriptionsDataIds.put(factors, id);
        }
    }

    public void save(File dir) throws IOException {
        Preconditions.checkArgument(!dir.exists() || dir.isDirectory());
        if ((!dir.exists() && !dir.mkdirs()) || !dir.isDirectory())
            throw new IOException("Couldn't mkdir " + dir.getAbsolutePath());
        try (FileOutputStream fileOut = new FileOutputStream(new File(dir, "index"));
             ObjectOutputStream objOut = new ObjectOutputStream(fileOut)) {
            objOut.writeObject(index);
        }
    }

    public static D1ExperimentData load(File dir) throws IOException {
        Index index;
        File file = new File(dir, "index");
        if (!file.exists())
            return null;
        try (FileInputStream fileIn = new FileInputStream(file);
             ObjectInputStream objIn = new ObjectInputStream(fileIn)) {
            index = (Index) objIn.readObject();
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
        return new D1ExperimentData(dir, index);
    }

    private static class Index implements Serializable {
        HashMap<D1Factors, String> descriptionsDataIds = new HashMap<>();
    }
}

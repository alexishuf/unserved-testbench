package br.ufsc.lapesd.unserved.testbench.byte_renderers;

import br.ufsc.lapesd.unserved.testbench.components.ComponentFactory;

public interface RendererFactory extends ComponentFactory {
    Renderer newInstance();
}

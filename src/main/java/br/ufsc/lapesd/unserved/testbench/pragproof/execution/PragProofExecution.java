package br.ufsc.lapesd.unserved.testbench.pragproof.execution;

import br.ufsc.lapesd.unserved.testbench.io_composer.IOComposerTimes;
import br.ufsc.lapesd.unserved.testbench.pragproof.PragProofProver;
import br.ufsc.lapesd.unserved.testbench.pragproof.PragProofProverException;
import br.ufsc.lapesd.unserved.testbench.pragproof.ProofActions;
import br.ufsc.lapesd.unserved.testbench.pragproof.context.PragProofContext;
import br.ufsc.lapesd.unserved.testbench.pragproof.exceptions.NoProofException;
import br.ufsc.lapesd.unserved.testbench.pragproof.exceptions.PragmaticProofException;
import br.ufsc.lapesd.unserved.testbench.process.Interpreter;
import br.ufsc.lapesd.unserved.testbench.process.actions.ActionExecutionException;
import br.ufsc.lapesd.unserved.testbench.process.data.DataContext;
import br.ufsc.lapesd.unserved.testbench.process.data.DataContextDiff;
import br.ufsc.lapesd.unserved.testbench.reasoner.N3ReasonerFactory;
import br.ufsc.lapesd.unserved.testbench.replication.ReplicatedExecution;
import br.ufsc.lapesd.unserved.testbench.replication.ReplicatedExecutionException;
import br.ufsc.lapesd.unserved.testbench.replication.action.ReplicationContainer;
import com.google.common.base.Preconditions;
import org.apache.jena.rdf.model.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class PragProofExecution implements ReplicatedExecution {
    private static Logger logger = LoggerFactory.getLogger(PragProofExecution.class);

    private final PragProofProver prover;
    private final N3ReasonerFactory reasonerFactory;
    private Interpreter interpreter;

    private boolean closed = false;
    private State state = State.AT_TOP;
    private int nPre = 0, nPost = 0;
    private PragProofContext proofContext;
    private ProofActions preInteractions = null, postInteractions = null;

    private DataContextDiff diff = null;
    private Resource action = null;
    private ReplicationContainer actionContainer;

    public enum State {
        AT_TOP,
        AT_BOTTOM,
        FAILED,
        DONE,
        /**
         * The execution is complete with success, however there is no result. This is the
         * case of forked executions, only the forks produce results (if they themselves
         * are not forked).
         */
        DONE_NO_RESULT;

        boolean hasResult() {return this == DONE;}
        boolean isDone() {return this == DONE || this == FAILED || this == DONE_NO_RESULT;}
    }

    public PragProofExecution(Interpreter interpreter, PragProofContext proofContext,
                              PragProofProver prover, N3ReasonerFactory reasonerFactory) {
        this.interpreter = interpreter;
        this.proofContext = proofContext;
        this.prover = prover;
        this.reasonerFactory = reasonerFactory;
    }

    private PragProofExecution(PragProofExecution other) {
        this.interpreter = other.interpreter;
        this.prover = other.prover;
        this.reasonerFactory = other.reasonerFactory;
        this.state = other.state;
        this.nPost = other.nPost;
        this.nPre = other.nPre;
        this.proofContext = other.proofContext.createNonIsolatedCopy();
        Map<PragProofContext, PragProofContext> proofContextMap;
        proofContextMap = Collections.singletonMap(other.proofContext, this.proofContext);
        this.preInteractions = other.preInteractions.duplicate(proofContextMap);
        this.postInteractions = other.postInteractions.duplicate(proofContextMap);
        this.action = other.action;
        if (this.action != null) {
            if (this.action.isAnon()) {
                preInteractions.getModel().createResource(action.getId());
            } else if (action.isURIResource()) {
                preInteractions.getModel().createResource(action.getURI());
            } else {
                assert false : "resource is neither Anon neither URI";
            }
        }
        this.diff = other.diff;
    }

    @Override
    public IOComposerTimes getTimes() {
        return null;
    }

    public PragProofExecution duplicate() {
        return new PragProofExecution(this);
    }

    public State getState() {
        return state;
    }

    @Override
    public void close() {
        if (closed) return;
        closed = true;
        if (proofContext != null)
            proofContext.close();
    }

    public PragProofResult toResult() {
        Preconditions.checkState(!closed && state == State.DONE);
        PragProofResult result = new PragProofResult(proofContext);
        proofContext = null;
        close();
        return result;
    }

    public PragProofContext getProofContext() {
        Preconditions.checkState(!closed);
        return proofContext;
    }

    @Override
    public DataContext getDataContext() {
        Preconditions.checkState(!closed);
        return proofContext.getDataContext();
    }

    public void putDiff(@Nullable DataContextDiff diff) {
        Preconditions.checkState(!closed);
        Preconditions.checkState(this.diff == null);
        this.diff = diff;
    }

    /**
     * Runs the pragmatic proof algorithm until the goal is achieved by continuously
     * getting an action and interpreting it.
     *
     * @throws ReplicatedExecutionException if something goes wrong
     */
    public void resumeInterpretation() throws ReplicatedExecutionException {
        if (!state.isDone()) {
            Resource ppAction;
            try {
                ppAction = yieldAction();
            } catch (PragmaticProofException e) {
                throw new ReplicatedExecutionException(e);
            }
            if (ppAction == null) {
                assert state.isDone();
                if (state.hasResult())
                    interpreter.deliverResult(toResult());
                return;
            }

            try {
                interpreter.interpret(ppAction).waitForDone();
            } catch (ActionExecutionException e) {
                logger.info("Interpreter failed to run PragProofAction", e);
                putDiff(null);
            } catch (InterruptedException e) {
                logger.info("Interpreter interrupted while running PragProofAction", e);
                putDiff(null);
            }
        }
    }

    /**
     * Gets the next action to be executed in the composition. For this implementation of the
     * Pragmatic Proof, this is an action which wraps the real action and which will call
     * resume after the execution of the child. The effect is that submitting this to the
     * interpreter will seem like the whole composition was executed.
     *
     * @return The action or null if the PP algorithm ended or failed (check getState())
     * @throws PragmaticProofException if something goes wrong.
     */
    @Nullable
    public Resource yieldAction() throws PragmaticProofException {
        Preconditions.checkState(!closed && !state.isDone());
        if (state == State.AT_BOTTOM) {
            try {
                if (actionContainer != null && actionContainer.wasForked()) {
                    actionContainer.join();
                    state = State.DONE_NO_RESULT;
                    return null;
                }
            } catch(InterruptedException e) {
                throw new PragmaticProofException(e);
            } finally {
                if (actionContainer != null) actionContainer.close();
            }

            postInteractions = diff == null ? null : getInteractions(diff);
            nPost = postInteractions == null ? nPre : postInteractions.get().size();
            if (postInteractions == null || nPost >= nPre) {
                try {
                    proofContext.getRules().removeInteractionRule(action);
                } catch (IOException e) {
                    throw new PragmaticProofException("Problem while removing a rule", e);
                }
                preInteractions.close();
                preInteractions = null;
            } else {
                preInteractions.close();
                preInteractions = postInteractions;
                diff.apply(proofContext.getDataContext());
            }
            diff = null;
        }
        if (preInteractions == null)
            preInteractions = getInteractions();
        if (preInteractions == null)
            throw new NoProofException();
        if ((nPre = preInteractions.get().size()) == 0) {
            state = State.DONE;
            return null;
        }

        action = selectPlanFragment(preInteractions);
        state = State.AT_BOTTOM;
        actionContainer = new ReplicationContainer(action, this);
        assert diff == null;
        return actionContainer.getAction();
    }

    private Resource selectPlanFragment(ProofActions interactions) {
        List<Resource> list = interactions.topologicallySorted();
        Preconditions.checkArgument(!list.isEmpty(), "Caller should ensure there are interactions");
        return list.get(list.size() - 1);
    }

    private ProofActions getInteractions(@Nonnull DataContextDiff diff)
            throws PragmaticProofException {
        Preconditions.checkState(!closed);
        PragProofContext proofContext = this.proofContext.createNonIsolatedCopy();
        diff.apply(proofContext.getDataContext());
        ProofActions interactions = getInteractions(proofContext);
        if (interactions != null)
            interactions.setProofContext(proofContext);
        return interactions;
    }

    private ProofActions getInteractions() throws PragmaticProofException {
        Preconditions.checkState(!closed);
        return getInteractions(proofContext);
    }

    private ProofActions getInteractions(@Nonnull PragProofContext proofContext)
            throws PragmaticProofException {
        Preconditions.checkNotNull(proofContext);
        try {
            String proof = prover.prove(proofContext);
            return ProofActions.extract(proof, reasonerFactory, proofContext);
        } catch (PragProofProverException e) {
            e.printStackTrace(); //FIXME these may be an IO error or a no-proof error
            return null;
        } catch (Exception e) {
            throw new PragmaticProofException("Couldn't extract ppo:Actions from proof", e);
        }
    }
}

package br.ufsc.lapesd.unserved.testbench.benchmarks.experiment;

import java.io.Serializable;

/**
 * Collection of all collected result variables from a Experiment
 */
public interface ExperimentResult extends Serializable {
}
package br.ufsc.lapesd.unserved.testbench.httpcomponents.extractors;

public class IncompletePartException extends Exception {
    public IncompletePartException() {
        super("Given Part has no sufficient information for extractor selection");
    }
}

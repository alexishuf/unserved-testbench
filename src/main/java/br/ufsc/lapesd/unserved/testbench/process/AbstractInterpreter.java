package br.ufsc.lapesd.unserved.testbench.process;

import com.google.common.base.Preconditions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.Consumer;

public abstract class AbstractInterpreter implements Interpreter {
    private static Logger logger = LoggerFactory.getLogger(AbstractInterpreter.class);
    public LinkedHashMap<Class, Consumer> consumers = new LinkedHashMap<>();

    @Override
    public <T> void setResultConsumer(@Nonnull Class<T> aClass, @Nonnull Consumer<T> consumer) {
        Preconditions.checkNotNull(aClass);
        Preconditions.checkNotNull(consumer);
        consumers.put(aClass, consumer);
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public <T> Consumer<T> getResultConsumer(Class<T> resultClass) {
        Consumer consumer = consumers.get(resultClass);
        if (consumer == null) {
            consumer = consumers.entrySet().stream()
                    .filter(e -> e.getKey().isAssignableFrom(resultClass))
                    .map(Map.Entry::getValue).findFirst().orElse(null);
        }
        return consumer;
    }

    @Override
    @SuppressWarnings("unchecked")
    public boolean deliverResult(Object result) {
        Consumer consumer = getResultConsumer(result.getClass());
        if (consumer != null) {
            logger.debug("deliverResult({}) to consumer={}", result, consumer);
            consumer.accept(result);
            return true;
        } else {
            logger.debug("deliverResult({}) to no consumer.", result);
        }
        return false;
    }
}

package br.ufsc.lapesd.unserved.testbench.benchmarks;

import br.ufsc.lapesd.unserved.testbench.Algorithm;
import br.ufsc.lapesd.unserved.testbench.benchmarks.compose.TimesExperimentResult;
import br.ufsc.lapesd.unserved.testbench.benchmarks.experiment.Experiment;
import br.ufsc.lapesd.unserved.testbench.benchmarks.experiment.Factors;
import br.ufsc.lapesd.unserved.testbench.benchmarks.pragproof_design.PPExperimentFactory;
import br.ufsc.lapesd.unserved.testbench.benchmarks.pragproof_design.PPFactors;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.List;

/**
 * Java rewrite of csv branch in [1], which is a fork from [2]
 *
 * [1]: https://bitbucket.org/alexishuf/unserved-composition-benchmark
 * [2]: https://github.com/RubenVerborgh/RESTdesc-Composition-Benchmark
 */
public class VerborghCompositionBenchmarkApp {
    @Option(name = "--repetitions")
    private int repetitions = 5;
    @Option(name = "--min-length")
    private int minLength = 2;
    @Option(name = "--max-length")
    private int maxLength = 1024;
    @Option(name = "--alternatives")
    private int alternatives = 1;

    @Option(name = "--non-dummy-chain")
    private int nonDummyChain = 32;
    @Option(name = "--non-dummy-alternatives")
    private int nonDummyAlternatives = 1;

    @Option(name = "--preheat-count")
    private int preheatCount = 1;

    @Option(name = "--no-cheat-unstable-reasoner")
    private boolean noCheatUnstableReasoner = false;

    @Option(name = "--csv-out")
    private File csvOut;
    private PrintStream csvOutStream = null;

    @Option(name = "--algorithm", required = true)
    Algorithm algorithm;
    private final PPExperimentFactory experimentFactory;

    public VerborghCompositionBenchmarkApp() {
        experimentFactory = new PPExperimentFactory();
    }

    @Option(name = "--execute")
    private boolean execute = false;

    public static class Builder {
        VerborghCompositionBenchmarkApp app = new VerborghCompositionBenchmarkApp();

        public Builder(Algorithm algorithm) throws IOException {
            app.algorithm = algorithm;
        }
        public Builder repeats(int value) {
            app.repetitions = value;
            return this;
        }
        public Builder chainLength(int value) {
            app.maxLength = value;
            return this;
        }
        public Builder setAlternatives(int value) {
            app.alternatives = value;
            return this;
        }
        public Builder csvOut(File value) {
            app.csvOut = value;
            return this;
        }
        public VerborghCompositionBenchmarkApp build() {
            VerborghCompositionBenchmarkApp ret = this.app;
            app = null;
            return ret;
        }
    }

    public static void main(String[] args) throws Exception {
        VerborghCompositionBenchmarkApp app = new VerborghCompositionBenchmarkApp();
        CmdLineParser parser = new CmdLineParser(app);
        parser.parseArgument(args);
        app.run();
    }

    private Factors simpleFactors(int chain, int ioCount) {
        return new PPFactors.Builder(chain, algorithm).withAlternatives(alternatives)
                .withExecute(execute)
                .withPreheatCount(preheatCount)
                .withIoCount(ioCount).build();
    }
    private Factors dummyFactors(int chain) {
        return new PPFactors.Builder(chain, algorithm).withAlternatives(alternatives)
                .withExecute(execute)
                .withPreheatCount(preheatCount)
                .withNonDummyChain(nonDummyChain)
                .withNonDummyAlternatives(nonDummyAlternatives)
                .withNonDummyIOCount(1)
                .withIoCount(1).build();
    }

    public void run() throws Exception {
        initCsvOut();
        for (int chain = minLength; chain <= maxLength; chain = incLength(chain)) {
            List<Factors> factorsList = Arrays.asList(
                    simpleFactors(chain, 1),
                    simpleFactors(chain, 2),
                    simpleFactors(chain, 3),
                    dummyFactors(chain));
            for (int repetition = 0; repetition < repetitions; repetition++) {
                beginCsvLine(chain);
                for (Factors f : factorsList) run(f);
                endCsvLine();
            }
        }
    }

    private int incLength(int length) {
        return alternatives > 1 ? length + 1 : length * 2;
    }

    private void run(Factors factors) throws Exception {
        System.out.printf("%s: ...\n", factors.toString());
        try(Experiment experiment = experimentFactory.createExperiment(factors)) {
            experiment.setUp();
            appendToCsv((TimesExperimentResult) experiment.run());
        }
    }

    private void initCsvOut() throws IOException {
        if (csvOut == null) return;
        if (csvOutStream != null) return;
        csvOutStream = new PrintStream(new FileOutputStream(csvOut));
        csvOutStream.printf("descriptionCount, " +
                " parseDescriptions," +
                " createCompositionNoParse," +
                " createComposition," +
                " execution, " +

                " parseDescriptionsTwoConditions," +
                " createCompositionTwoConditionsNoParse," +
                " createCompositionTwoConditions," +
                " executionTwoConditions," +

                " parseDescriptionsThreeConditions," +
                " createCompositionThreeConditionsNoParse," +
                " createCompositionThreeConditions," +
                " executionThreeConditions," +

                " parseDescriptionsWithDummies," +
                " createCompositionWithDummiesNoParse," +
                " createCompositionWithDummies," +
                " executionWithDummies\n");
    }

    private void appendToCsv(TimesExperimentResult time) {
        if (csvOutStream != null) {
            csvOutStream.printf(", %.3f, %.3f, %.3f, %.3f",
                    time.getInitialization(), time.getComposition(),
                    time.getInitialization()+time.getComposition()+time.getOutput(),
                    execute ? time.getExecution() : 0);
        }
    }

    private void beginCsvLine(int chainLength) {
        if (csvOutStream != null)
            csvOutStream.printf("%d", chainLength);
    }

    private void endCsvLine() {
        if (csvOutStream != null)
            csvOutStream.printf("\n");
    }

}

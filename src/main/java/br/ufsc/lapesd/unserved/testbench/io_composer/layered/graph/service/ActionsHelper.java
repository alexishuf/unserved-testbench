package br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service;

import br.ufsc.lapesd.unserved.testbench.io_composer.state.MessagePairAction;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class ActionsHelper {
    @Nonnull
    public static Map<IndexedNode, MessagePairAction>
    getMessagePairs(@Nonnull Collection<IndexedNode> nodes) {
        Map<IndexedNode, MessagePairAction> map = new HashMap<>();
        nodes.stream().filter(node -> node.node instanceof ServiceNode)
                .forEachOrdered(n -> map.put(n, new MessagePairAction(
                        ((ServiceNode)n.node).getAntecedent(),
                        ((ServiceNode)n.node).getConsequent(), n.index)));
        return map;
    }

}

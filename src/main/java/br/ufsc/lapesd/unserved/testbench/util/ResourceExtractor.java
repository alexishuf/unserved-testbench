package br.ufsc.lapesd.unserved.testbench.util;

import org.apache.commons.io.IOUtils;

import javax.annotation.Nonnull;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Extracts a resource into a temporary file set to auto delete on exit.
 */
public class ResourceExtractor {
    ClassLoader classLoader;
    private final Pattern suffixPattern = Pattern.compile(".*(\\.[^.]*)$");

    public ResourceExtractor() {
        classLoader = getClass().getClassLoader();
    }
    public ResourceExtractor(ClassLoader classLoader) {
        this.classLoader = classLoader;;
    }

    public File extract(@Nonnull String path) throws IOException {
        File tempFile = null;
        try(InputStream inputStream = classLoader.getResourceAsStream(path)) {
            Matcher matcher = suffixPattern.matcher(path);
            String suffix = matcher.matches() ? matcher.group(1) : "";
            Path tempPath = Files.createTempFile("unserved-testbench-", suffix);

            tempFile = tempPath.toFile();
            tempFile.deleteOnExit();

            try (FileOutputStream outputStream = new FileOutputStream(tempFile)) {
                IOUtils.copy(inputStream, outputStream);
            } catch (FileNotFoundException e) {
                throw new RuntimeException(e);
            }
        }
        return tempFile;
    }
}

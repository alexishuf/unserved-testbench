package br.ufsc.lapesd.unserved.testbench.benchmarks.wsc08;

import br.ufsc.lapesd.unserved.testbench.Algorithm;
import br.ufsc.lapesd.unserved.testbench.benchmarks.experiment.ExperimentDesignLevels;
import br.ufsc.lapesd.unserved.testbench.benchmarks.experiment.Factors;
import com.google.common.base.Preconditions;

import java.io.File;
import java.util.Map;

@SuppressWarnings("unused")
public class WscExperimentDesignLevels implements ExperimentDesignLevels {
    private Values low, high, fallback;
    private transient File baseDir;

    public void setBaseDir(File baseDir) {
        this.baseDir = baseDir;
    }

    @Override
    public Factors createFactors(Map<String, String> levels) {
        File wscConvertedRoot = getFile(levels, "wscConvertedRoot");
        Preconditions.checkArgument(wscConvertedRoot != null);
        File serviceFailures = getFile(levels, "failPoints");
        WscFactors.Builder builder = new WscFactors.Builder(wscConvertedRoot,
                getValue(levels, "algorithm", Algorithm.class))
                .withServiceCompression(getValue(levels, "serviceCompression", Boolean.class))
                .withBackwardClosureOpt(getValue(levels, "backwardClosureOpt", Boolean.class))
                .withPreconditions(getValue(levels, "preconditions", Boolean.class))
                .withOnlyGraph(getValue(levels, "onlyGraph", Boolean.class))
                .withNaiveIO(getValue(levels, "naiveIO", Boolean.class))
                .withCombinedJN(getValue(levels, "combinedJN", Boolean.class))
                .withListSubclasses(getValue(levels, "listSubclasses", Boolean.class))
                .withNaiveTCG(getValue(levels, "naiveTCG", Boolean.class))
                .withDismissOutput(getValue(levels, "dismissOutput", Boolean.class))
                .withPreconditions(getValue(levels, "preconditions", Boolean.class))
                .withChildXms(getValue(levels, "childXms", String.class))
                .withChildXmx(getValue(levels, "childXmx", String.class))
                .withChildJVMOpts(getValue(levels, "childJVMOpts", String.class))
                .withExecute(getValue(levels, "execute", Boolean.class))
                .withProblem(getValue(levels, "problem", Integer.class))
                .withSubDir(getValue(levels, "subdir", String.class))
                .withKnown(getValue(levels, "known", String.class))
                .withWanted(getValue(levels, "wanted", String.class))
                .withPreheatCount(getValue(levels, "preheatCount", Integer.class));
        return builder.withFailPoints(serviceFailures).build();
    }

    private File getFile(Map<String, String> map, String name) {
        String path = getValue(map, name, String.class);
        if (path == null || path.isEmpty()) return null;
        File pathFile = new File(path);
        return pathFile.isAbsolute() || baseDir == null ? pathFile : new File(baseDir, path);
    }
    private <T> T getValue(Map<String, String> map, String name, Class<T> theClass) {
        return getValue(name, map.getOrDefault(name, null), theClass);
    }

    private <T> T getValue(String name, String value, Class<T> theClass) {
        int level = value == null ? 0 : Integer.parseInt(value);
        Values values = level == -1 ? low : (level == 1 ? high : fallback);
        return values.getValue(name, theClass);
    }

    private static class Values {
        private String wscConvertedRoot;
        private int problem;
        private String childXms;
        private String childXmx;
        private boolean execute;
        private boolean serviceCompression;
        private boolean preconditions;
        private String failPoints;
        //TODO add factors controlling failures
        private int preheatCount;
        private Algorithm algorithm;

        private Object getValue(String name) {
            try {
                return Values.class.getDeclaredField(name).get(this);
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            } catch (NoSuchFieldException e) {
                throw new IllegalArgumentException("Unknown name " + name);
            }
        }

        private <T> T getValue(String name, Class<T> theClass) {
            Object o = getValue(name);
            if (o != null)
                Preconditions.checkArgument(theClass.isAssignableFrom(o.getClass()));
            //noinspection unchecked
            return (T)o;
        }
    }
}

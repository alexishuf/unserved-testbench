package br.ufsc.lapesd.unserved.testbench.components.expressions.evaluator;

import br.ufsc.lapesd.unserved.testbench.components.expressions.ClassExpression;

import javax.annotation.Nonnull;

public interface ClassExpressionEvaluator {
    enum Result {
        FALSE, TRUE, INDETERMINATE, UNFINISHED;

        public static Result from(boolean value) {
            return value ? TRUE : FALSE;
        }

        public Result negated() {
            switch (this) {
                case TRUE:
                    return FALSE;
                case FALSE:
                    return TRUE;
                default:
                    return this;
            }
        }
    }

    /**
     * The {@link ClassExpression} being evaluated
     */
    @Nonnull
    ClassExpression getClassExpression();

    /**
     * Last value returned from a evaluateStep() call or UNFINISHED is no such call has
     * been done.
     *
     * @return Current result of the evaluator.
     */
    @Nonnull
    Result getResult();

    /**
     * Executes one evaluation step.
     *
     * Implementations are free to specify the granularity of a step, but overall there should
     * be a balance so that all concurrent {@link ClassExpression} are evaluated in an order
     * that resembles breadth-first.
     *
     * If getResult() != UNFINISHED, calls to this method should perform no additional
     * computation.
     *
     * For any {@link ClassExpression}, a finite calls to this should produce a
     * Result != UNFINISHED. However, keep in mind that INDETERMINATE is a conclusive response
     *
     * @return Result after a step of computation
     */
    @Nonnull
    Result evaluateStep();
}

package br.ufsc.lapesd.unserved.testbench.components.expressions;

import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.OWL2;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;


public class Connective extends AbstractClassExpression {
    public enum ConnectiveType {
        UNION,
        INTERSECTION,
        COMPLEMENT;

        public static ConnectiveType fromProperty(Property property) {
            if (property.equals(OWL2.unionOf))
                return UNION;
            else if (property.equals(OWL2.intersectionOf))
                return INTERSECTION;
            else if (property.equals(OWL2.complementOf))
                return COMPLEMENT;
            return null;
        }
    }
    @Nonnull private final ConnectiveType connectiveType;
    private final Set<ClassExpression> children = new HashSet<>();

    public Connective(@Nonnull ConnectiveType connectiveType,
                      @Nonnull Collection<ClassExpression> children,
                      @Nonnull Set<Resource> names) {
        super(ClassExpression.Type.Connective, names);
        this.connectiveType = connectiveType;
        this.children.addAll(children);
    }

    public @Nonnull ConnectiveType connectiveType() {
        return connectiveType;
    }

    @Override
    public Set<ClassExpression> getChildren() {
        return children;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(connectiveType()).append(getChildren()).hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) return true;
        if (!(o instanceof Connective)) return false;
        Connective rhs = (Connective) o;
        return connectiveType() == rhs.connectiveType() && getChildren().equals(rhs.getChildren());
    }
}

package br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service;

import br.ufsc.lapesd.unserved.testbench.benchmarks.wsc08.Wscc;
import br.ufsc.lapesd.unserved.testbench.iri.HTTP;
import br.ufsc.lapesd.unserved.testbench.model.Variable;
import br.ufsc.lapesd.unserved.testbench.util.SkolemizerMap;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Statement;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class NodeStableComparator implements Comparator<Node> {
    private enum Type {
        Start, End, Service, SingleJump, CombinedJump
    }

    @Nonnull
    private Type getType(@Nonnull Node node) {
        if (node instanceof StartNode) return Type.Start;
        else if (node instanceof EndNode) return Type.End;
        else if (node instanceof ServiceNode) return Type.Service;
        else if (node instanceof SingleJumpNode) return Type.SingleJump;
        else if (node instanceof CombinedJumpNode) return Type.CombinedJump;

        throw new UnsupportedOperationException("Class " + node.getClass()
                + " is not known as a NodeStableComparator.Type");
    }

    @Override
    public int compare(@Nullable Node lhs, @Nullable Node rhs) {
        if (lhs == null) return rhs == null ? 0 : -1;
        else if (rhs == null) return 1;
        //else: both are not null

        Type lhsType = getType(lhs), rhsType = getType(rhs);

        if (lhsType != rhsType) {
            assert !lhs.equals(rhs);
            return lhsType.compareTo(rhsType);
        } else if (lhsType == Type.Start) {
            int diff = compareResources(lhs.getOutputs(), rhs.getOutputs());
            assert diff != 0 || lhs.equals(rhs);
            return diff;
        } else if (lhsType == Type.End) {
            int diff = compareResources(lhs.getInputs(), rhs.getInputs());
            assert diff != 0 || lhs.equals(rhs);
            return diff;
        } else if (lhsType == Type.Service) {
            int diff = getIdString((ServiceNode) lhs).compareTo(getIdString((ServiceNode) rhs));
            assert diff != 0 || lhs.equals(rhs);
            return diff;
        } else if (lhsType == Type.SingleJump) {
            Variable lIn = lhs.getInputs().iterator().next();
            Variable rIn = rhs.getInputs().iterator().next();
            int diff = getIdString(lIn).compareTo(getIdString(rIn));
            assert diff != 0 || lhs.equals(rhs);
            return diff;
        } else if (lhsType == Type.CombinedJump) {
            CombinedJumpNode cLhs = (CombinedJumpNode) lhs;
            CombinedJumpNode cRhs = (CombinedJumpNode) rhs;
            int diff = compare(cLhs.getDelegate(), cRhs.getDelegate());
            assert diff != 0 || lhs.equals(rhs);
            return diff;
        }

        throw new UnsupportedOperationException("Don't know how to handle" + lhsType);
    }

    private int compareResources(@Nullable Collection<? extends RDFNode> left,
                                 @Nullable Collection<? extends RDFNode> right) {
        if (left == null) return right == null ? 0 : -1;
        else if (right == null) return 1;

        List<String> lList = left.stream().map(NodeStableComparator::getIdString).sorted()
                .collect(Collectors.toList());
        List<String> rList = right.stream().map(NodeStableComparator::getIdString).sorted()
                .collect(Collectors.toList());
        int min = Math.min(lList.size(), rList.size());
        for (int i = 0; i < min; i++) {
            int diff = lList.get(i).compareTo(rList.get(i));
            if (diff != 0) return diff;
        }
        return Integer.compare(lList.size(), rList.size());
    }

    private static String getIdString(@Nonnull ServiceNode n) {
        Statement stmt = n.getAntecedent().getProperty(Wscc.wscService);
        if (stmt != null) return stmt.getObject().toString();
        stmt = n.getAntecedent().getProperty(HTTP.requestURI);
        if (stmt != null) return stmt.getObject().toString();
        stmt = n.getAntecedent().getProperty(HTTP.absoluteURI);
        if (stmt != null) return stmt.getObject().toString();
        return getIdString(n.getAntecedent()) + "::" + getIdString(n.getConsequent());
    }

    private static String getIdString(@Nullable RDFNode n) {
        if (n == null) return "[null]";
        else if (n.canAs(Variable.class)) return getIdString(n.as(Variable.class));
        else if (n.isURIResource()) return n.asResource().getURI();
        else if (n.isAnon()) return n.asResource().getId().toString();
        else if (n.isLiteral())
            return n.asLiteral().getLexicalForm() + "^^" + n.asLiteral().getDatatypeURI();
        else
            throw new UnsupportedOperationException("Unknown node type" + n.toString());
    }

    private static String getIdString(@Nullable Variable var) {
        if (var == null) return "[null]";
        if (var.isURIResource() && !var.getURI().startsWith(SkolemizerMap.DEFAULT_PREFIX))
            return var.getURI();
        else
            return getIdString(var.getType()) + "::" + getIdString((RDFNode)var);

    }
}

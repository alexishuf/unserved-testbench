package br.ufsc.lapesd.unserved.testbench.model;

import org.apache.jena.rdf.model.Resource;

/**
 * Enhanced node for unserved:Part.
 */
public interface Part extends Resource {
    Variable getVariable();
    Resource getPartModifier();
}

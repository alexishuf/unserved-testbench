package br.ufsc.lapesd.unserved.testbench.benchmarks.design1;

import br.ufsc.lapesd.unserved.testbench.Algorithm;
import br.ufsc.lapesd.unserved.testbench.benchmarks.experiment.Factors;
import br.ufsc.lapesd.unserved.testbench.benchmarks.pragproof_design.ComposeFactors;
import com.google.common.base.Preconditions;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.annotation.Nonnull;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class D1Factors extends ComposeFactors implements Factors, Serializable {
    private final int serviceCount;
    private final int inputSetsCount;
    private final int inputSetsSize;
    private final int freeOutputCount;
    private final int connectedIOs;
    private final int solutionParallelSequences;
    private final int solutionLength;
    private final int solutionServiceAlternatives;

    public D1Factors(int preheatCount, boolean execute, Algorithm algorithm, int serviceCount,
                     int inputSetsCount, int inputSetsSize, int freeOutputCount, int connectedIOs,
                     int solutionParallelSequences,
                     int solutionLength, int solutionServiceAlternatives) {
        super(preheatCount, execute, algorithm);
        this.serviceCount = serviceCount;
        this.inputSetsCount = inputSetsCount;
        this.inputSetsSize = inputSetsSize;
        this.freeOutputCount = freeOutputCount;
        this.connectedIOs = connectedIOs;
        this.solutionParallelSequences = solutionParallelSequences;
        this.solutionLength = solutionLength;
        this.solutionServiceAlternatives = solutionServiceAlternatives;
    }

    @Override
    public String getValue(@Nonnull String name) {
        switch (name) {
            case "preheatCount": return String.valueOf(preheatCount);
            case "execute": return String.valueOf(execute);
            case "algorithm": return String.valueOf(algorithm);
            case "serviceCount": return String.valueOf(serviceCount);
            case "inputSetsCount": return String.valueOf(inputSetsCount);
            case "inputSetsSize": return String.valueOf(inputSetsSize);
            case "freeOutputCount": return String.valueOf(freeOutputCount);
            case "connectedIOs": return String.valueOf(connectedIOs);
            case "solutionParallelSequences": return String.valueOf(solutionParallelSequences);
            case "solutionLength": return String.valueOf(solutionLength);
            case "solutionServiceAlternatives": return String.valueOf(solutionServiceAlternatives);
        }
        throw new IllegalArgumentException("Unknown factor " + String.valueOf(name));
    }

    @Override
    public List<String> getNames() {
        return Arrays.asList("preheatCount", "execute", "algorithm", "serviceCount",
                "inputSetsCount", "inputSetsSize", "freeOutputCount", "connectedIOs",
                "solutionParallelSequences", "solutionLength", "solutionServiceAlternatives");
    }

    public int getServiceCount() {
        return serviceCount;
    }
    public int getInputSetsCount() {
        return inputSetsCount;
    }
    public int getInputSetsSize() {
        return inputSetsSize;
    }
    public int getFreeOutputCount() {
        return freeOutputCount;
    }
    public int getConnectedIOs() {
        return connectedIOs;
    }
    public int getSolutionParallelSequences() {
        return solutionParallelSequences;
    }
    public int getSolutionLength() {
        return solutionLength;
    }
    public int getSolutionServiceAlternatives() {
        return solutionServiceAlternatives;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) return true;
        if (o == null) return false;
        if (!(o instanceof D1Factors)) return false;
        D1Factors rhs = (D1Factors) o;
        return Objects.equals(getAlgorithm(), rhs.getAlgorithm())
                && getExecute() == rhs.getExecute()
                && getPreheatCount() == rhs.getPreheatCount()
                && getServiceCount() == rhs.getServiceCount()
                && getInputSetsCount() == rhs.getInputSetsCount()
                && getInputSetsSize() == rhs.getInputSetsSize()
                && getFreeOutputCount() == rhs.getFreeOutputCount()
                && getConnectedIOs() == rhs.getConnectedIOs()
                && getSolutionParallelSequences() == rhs.getSolutionParallelSequences()
                && getSolutionLength() == rhs.getSolutionLength()
                && getSolutionServiceAlternatives() == rhs.getSolutionServiceAlternatives();
    }

    @Override
    public String toString() {
        return String.format("D1Factors(IS s/c: %s/%s, O. free/conn. %d/%d, " +
                        "par./alt./length: %d/%d/%d, alg%s: %s)", getInputSetsSize(),
                getInputSetsCount(), getFreeOutputCount(), getConnectedIOs(),
                getSolutionParallelSequences(), getSolutionServiceAlternatives(),
                getSolutionLength(), getExecute() ? "[e]" : "", getAlgorithm().name());
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getAlgorithm()).append(getExecute())
                .append(getPreheatCount()).append(getServiceCount()).append(getInputSetsCount())
                .append(getInputSetsSize()).append(getFreeOutputCount()).append(getConnectedIOs())
                .append(getSolutionParallelSequences()).append(getSolutionLength())
                .append(getSolutionServiceAlternatives()).hashCode();
    }

    public static class Builder {
        private int preheatCount = 2;
        private boolean execute = false;
        private Algorithm algorithm = null;
        private int serviceCount = -1, inputSetsCount = -1, inputSetsSize = -1;
        private int freeOutputCount = -1, connectedIOs = -1;
        private int solutionParallelSequences = 1;
        private int solutionLength = -1;
        private int solutionServiceAlternatives = 1;

        public Builder setPreheatCount(int preheatCount) {
            this.preheatCount = preheatCount;
            return this;
        }
        public Builder setExecute(boolean execute) {
            this.execute = execute;
            return this;
        }
        public Builder setAlgorithm(Algorithm algorithm) {
            this.algorithm = algorithm;
            return this;
        }
        public Builder setServiceCount(int serviceCount) {
            this.serviceCount = serviceCount;
            return this;
        }
        public Builder setInputSetsCount(int inputSetsCount) {
            this.inputSetsCount = inputSetsCount;
            return this;
        }
        public Builder setInputSetsSize(int inputSetsSize) {
            this.inputSetsSize = inputSetsSize;
            return this;
        }
        public Builder setFreeOutputCount(int freeOutputCount) {
            this.freeOutputCount = freeOutputCount;
            return this;
        }
        public Builder setConnectedIOs(int connectedIOs) {
            this.connectedIOs = connectedIOs;
            return this;
        }
        public Builder setSolutionParallelSequences(int solutionParallelSequences) {
            this.solutionParallelSequences = solutionParallelSequences;
            return this;
        }
        public Builder setSolutionLength(int solutionLength) {
            this.solutionLength = solutionLength;
            return this;
        }
        public Builder setSolutionServiceAlternatives(int solutionServiceAlternatives) {
            this.solutionServiceAlternatives = solutionServiceAlternatives;
            return this;
        }

        public D1Factors build() {
            Preconditions.checkState(serviceCount > 0);
            Preconditions.checkState(solutionLength >= 0);
            Preconditions.checkState(algorithm != null);
            Preconditions.checkState(inputSetsCount >= 0);
            Preconditions.checkState(inputSetsSize >= 0);
            Preconditions.checkState(freeOutputCount >= 0);
            Preconditions.checkState(connectedIOs > 0);
            Preconditions.checkState(solutionServiceAlternatives > 0);
            Preconditions.checkState(solutionParallelSequences > 0);
            Preconditions.checkState(solutionLength > 0);
            return new D1Factors(preheatCount, execute, algorithm,
                    serviceCount, inputSetsSize, inputSetsCount, freeOutputCount, connectedIOs,
                    solutionParallelSequences, solutionLength, solutionServiceAlternatives);
        }
    }
}

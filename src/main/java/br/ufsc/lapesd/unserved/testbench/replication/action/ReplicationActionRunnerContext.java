package br.ufsc.lapesd.unserved.testbench.replication.action;

import br.ufsc.lapesd.unserved.testbench.process.AbstractActionContext;
import br.ufsc.lapesd.unserved.testbench.process.ActionRunnersContext;
import br.ufsc.lapesd.unserved.testbench.process.Context;
import br.ufsc.lapesd.unserved.testbench.process.data.DiffingDataContext;
import br.ufsc.lapesd.unserved.testbench.process.model.Receive;

public class ReplicationActionRunnerContext extends AbstractActionContext {
    private final DiffingDataContext dataContext;
    private final ReplicationContainer container;
    private final Context outerContext;

    public ReplicationActionRunnerContext(ActionRunnersContext runners, Receive receive,
                                          DiffingDataContext dataContext,
                                          ReplicationContainer container, Context outerContext) {
        super(runners, receive);
        this.dataContext = dataContext;
        this.container = container;
        this.outerContext = outerContext;
    }

    public DiffingDataContext getDataContext() {
        return dataContext;
    }

    public ReplicationContainer getContainer() {
        return container;
    }

    public Context getOuterContext() {
        return outerContext;
    }

    @Override
    public void close() throws Exception {
        /* pass */
    }
}

package br.ufsc.lapesd.unserved.testbench.pragproof;

import br.ufsc.lapesd.unserved.testbench.composer.ComposerBuilderException;
import br.ufsc.lapesd.unserved.testbench.composer.ComposerException;
import br.ufsc.lapesd.unserved.testbench.composer.Composition;
import br.ufsc.lapesd.unserved.testbench.composer.ReplicatedComposer;
import br.ufsc.lapesd.unserved.testbench.input.RDFInput;
import br.ufsc.lapesd.unserved.testbench.io_composer.IOComposer;
import br.ufsc.lapesd.unserved.testbench.io_composer.IOComposerBuilder;
import br.ufsc.lapesd.unserved.testbench.pragproof.context.PragProofContext;
import br.ufsc.lapesd.unserved.testbench.pragproof.context.impl.DefaultPragProofContext;
import br.ufsc.lapesd.unserved.testbench.pragproof.exceptions.PragmaticProofException;
import br.ufsc.lapesd.unserved.testbench.pragproof.execution.PragProofExecution;
import br.ufsc.lapesd.unserved.testbench.process.CompleteTask;
import br.ufsc.lapesd.unserved.testbench.process.Interpreter;
import br.ufsc.lapesd.unserved.testbench.reasoner.N3ReasonerFactory;
import br.ufsc.lapesd.unserved.testbench.replication.ReplicatedExecutionException;
import com.google.common.base.Preconditions;
import org.apache.jena.rdf.model.Resource;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.IOException;
import java.util.Collection;

/**
 * Implementation of R. Verborgh's pragmatic proof algorithm[1] adapted to use
 * {@link PragProofProver}.
 *
 * [1]: http://doi.org/10.1017/S1471068416000016
 */
public class PragmaticProof implements IOComposer, ReplicatedComposer {
    private boolean closed = false;
    @Nonnull private final PragProofProver prover;
    @Nonnull private final N3ReasonerFactory reasonerFactory;
    @Nonnull private final PragProofContext initialContext;

    public static class Builder implements IOComposerBuilder {
        private DefaultPragProofContext.Builder proofContextBuilder
                = new DefaultPragProofContext.Builder();
        private boolean closed = false;
        private N3ReasonerFactory reasonerFactory = null;

        public Builder(N3ReasonerFactory reasonerFactory) {
            this.reasonerFactory = reasonerFactory;
        }

        @Nonnull
        @Override
        public IOComposerBuilder withRulesFile(@Nonnull File rulesFile) {
            return this;
        }

        @Nonnull
        @Override
        public IOComposerBuilder withInputHasNoSkolemized(boolean ignored) {
            return this;
        }

        @Nonnull
        @Override
        public IOComposerBuilder withBasicReasoning(boolean ignored) {
            return this;
        }

        @Nonnull
        public Builder addInput(@Nonnull RDFInput input) {
            proofContextBuilder.addInput(input);
            return this;
        }

        @Nonnull
        public Builder addInputs(Collection<RDFInput> inputs) {
            proofContextBuilder.addInputs(inputs);
            return this;
        }

        @Nonnull
        public Builder markWantedVariable(@Nonnull Resource variable) {
            proofContextBuilder.markWantedVariable(variable);
            return this;
        }

        public Builder markWantedVariables(Collection<Resource> variables) {
            proofContextBuilder.markWantedVariables(variables);
            return this;
        }

        public PragmaticProof build() throws ComposerBuilderException {
            try {
                PragmaticProof instance = null;
                instance = new PragmaticProof(new PragProofProver(reasonerFactory),
                        proofContextBuilder.build(), reasonerFactory);
                closed = true;
                return instance;
            } catch (IOException e) {
                throw new ComposerBuilderException(e);
            }
        }

        @Override
        public void close() throws ComposerBuilderException {
            if (closed) return;
            closed = true;
            try {
                proofContextBuilder.close();
            } catch (Exception e) {
                throw new ComposerBuilderException(e);
            }
        }
    }

    private PragmaticProof(@Nonnull PragProofProver prover,
                           @Nonnull PragProofContext initialContext,
                           @Nonnull N3ReasonerFactory reasonerFactory) {
        Preconditions.checkNotNull(prover);
        Preconditions.checkNotNull(initialContext);
        Preconditions.checkNotNull(reasonerFactory);

        this.prover = prover;
        this.reasonerFactory = reasonerFactory;
        this.initialContext = initialContext;
    }

    @Override
    public Interpreter.Task run(Interpreter interpreter) throws PragmaticProofException {
        Preconditions.checkState(!closed);
        try (PragProofExecution execution = new PragProofExecution(interpreter, initialContext,
                prover, reasonerFactory)) {
            execution.resumeInterpretation();
        } catch (ReplicatedExecutionException e) {
            if (e.getCause() != null && e.getCause() instanceof PragmaticProofException)
                throw (PragmaticProofException) e.getCause();
        }
        return new CompleteTask();
    }

    @Nonnull
    @Override
    public Composition run() throws ComposerException {
        throw new UnsupportedOperationException();
        //FIXME
    }

    @Override
    public void close() {
        if (closed) return;
        closed = true;
    }
}

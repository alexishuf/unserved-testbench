package br.ufsc.lapesd.unserved.testbench.restdesc_pragproof;

import br.ufsc.lapesd.unserved.testbench.composer.ComposerInput;
import br.ufsc.lapesd.unserved.testbench.input.RDFInput;

import java.io.IOException;
import java.util.List;

public interface RESTdescComposerInput extends ComposerInput {
    List<RDFInput> getRuleInputs();
    void setGoal(RDFInput goal);
    RDFInput getGoal();
    RDFInput initRules() throws IOException;
    RDFInput getRules();
    void forceRules(RDFInput forced);
}

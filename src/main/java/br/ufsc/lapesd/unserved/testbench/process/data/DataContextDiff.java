package br.ufsc.lapesd.unserved.testbench.process.data;


import br.ufsc.lapesd.unserved.testbench.process.data.action.DataContextAction;
import com.google.common.base.Preconditions;
import org.slf4j.Logger;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * A Diff produced by a {@link DiffingDataContext}.
 */
public class DataContextDiff {
    private static Logger logger = org.slf4j.LoggerFactory.getLogger(DataContextDiff.class);
    @Nonnull private final List<DataContextAction> actions;

    public DataContextDiff(@Nonnull List<DataContextAction> actions) {
        Preconditions.checkNotNull(actions);
        this.actions = actions;
    }

    public static DataContextDiff concat(@Nonnull DataContextDiff before, @Nonnull DataContextDiff after) {
        ArrayList<DataContextAction> list = new ArrayList<>(before.actions);
        list.addAll(after.actions);
        return new DataContextDiff(list);
    }

    public boolean isEmpty() {
        return actions.isEmpty();
    }

    public void apply(DataContext target) {
        if (isEmpty()) {
            logger.debug("apply({}) empty diff", target);
        } else {
            logger.debug("BEGIN apply({})", target);
            actions.forEach(a -> a.apply(target));
            logger.debug("END apply({})", target);
        }
    }

    @Nonnull
    public List<DataContextAction> getActions() {
        return Collections.unmodifiableList(actions);
    }

    public String dump() {
        return actions.stream().map(Object::toString).reduce((l, r) -> l + "\n" + r).orElse("");
    }
}

package br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service;


import br.ufsc.lapesd.unserved.testbench.io_composer.impl.IOComposerInput;

import javax.annotation.Nonnull;

/**
 * Base interface of all delegates used by a LayeredGraph builder.
 */
public interface BuilderDelegate {
    /**
     * A {@link BuilderDelegate} is active between the begin() and end() calls.
     * @return true iff active
     */
    boolean isActive();

    /**
     * Sets up the delegate for use on the construction of a graph.
     *
     * @param composerInput The composer's input.
     * @param startNode start {@link Node} of the graph, is not yet be present on layers.
     * @param endNode end {@link Node} of the graph, is not yet present on layers.
     */
    void begin(IOComposerInput composerInput, @Nonnull StartNode startNode,
               @Nonnull EndNode endNode);

    /**
     * Notifies that the graph construction has ended. The delegate may discard resources, but
     * begin() may be called in the future if another graph is contructed with the same builder.
     */
    void end();
}

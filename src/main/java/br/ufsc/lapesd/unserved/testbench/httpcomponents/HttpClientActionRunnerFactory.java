package br.ufsc.lapesd.unserved.testbench.httpcomponents;

import br.ufsc.lapesd.unserved.testbench.components.SimpleActionRunnerFactory;
import br.ufsc.lapesd.unserved.testbench.iri.UnservedP;
import br.ufsc.lapesd.unserved.testbench.iri.UnservedX;
import br.ufsc.lapesd.unserved.testbench.process.actions.ActionRunner;
import br.ufsc.lapesd.unserved.testbench.process.actions.ActionRunnerFactory;
import br.ufsc.lapesd.unserved.testbench.util.jena.UncloseableGraph;
import org.apache.jena.graph.Graph;
import org.apache.jena.graph.compose.MultiUnion;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;

import javax.annotation.Nonnull;
import java.io.InputStream;

public class HttpClientActionRunnerFactory extends SimpleActionRunnerFactory {
    public HttpClientActionRunnerFactory(@Nonnull Class<? extends ActionRunner> implementation,
                                         @Nonnull Class<? extends ActionRunnerFactory> factory,
                                         @Nonnull String turtleResourcePath,
                                         @Nonnull String classURI) {
        super(implementation, factory, createDescription(turtleResourcePath, classURI));
    }

    private static Resource createDescription(String turtleResourcePath, String className) {
        Model myModel = ModelFactory.createDefaultModel();
        ClassLoader loader = HttpClientSendRunner.class.getClassLoader();
        InputStream stream = loader.getResourceAsStream(turtleResourcePath);
        RDFDataMgr.read(myModel, stream, Lang.TURTLE);

        Model union = ModelFactory.createModelForGraph(new MultiUnion(new Graph[]{
                new UncloseableGraph(myModel.getGraph()),
                new UncloseableGraph(new UnservedP().getModel().getGraph()),
                new UncloseableGraph(new UnservedX.HTTP().getModel().getGraph())}));
        Resource resource = union.createResource(className);
        assert  union.containsResource(resource);
        assert resource != null;
        assert resource.listProperties().hasNext();
        return resource;
    }
}

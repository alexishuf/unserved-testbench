package br.ufsc.lapesd.unserved.testbench.io_composer.impl;

import br.ufsc.lapesd.unserved.testbench.composer.ComposerException;
import br.ufsc.lapesd.unserved.testbench.composer.impl.AbstractComposerInput;
import br.ufsc.lapesd.unserved.testbench.input.RDFInput;
import br.ufsc.lapesd.unserved.testbench.io_composer.IOComposer;
import br.ufsc.lapesd.unserved.testbench.iri.Unserved;
import br.ufsc.lapesd.unserved.testbench.model.Variable;
import br.ufsc.lapesd.unserved.testbench.util.Skolemizer;
import br.ufsc.lapesd.unserved.testbench.util.SkolemizerMap;
import br.ufsc.lapesd.unserved.testbench.util.UnservedReasoning;
import com.google.common.base.Preconditions;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.reasoner.rulesys.Rule;

import javax.annotation.Nonnull;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 * Typical input of an {@link IOComposer}
 */
public class IOComposerInputImpl extends AbstractComposerInput implements IOComposerInput {
    private final @Nonnull List<RDFInput> inputs = new ArrayList<>();
    private final @Nonnull HashSet<Resource> wanted = new HashSet<>();
    private final @Nonnull List<Rule> rules = new ArrayList<>();
    private Set<Variable> wantedAsVars = null;
    private Model union = null;
    private @Nonnull SkolemizerMap skolemizerMap = new SkolemizerMap();
    private @Nonnull WeakReference<Set<Resource>> knownAsResources = new WeakReference<>(null);
    private Set<Variable> known = null;

    public IOComposerInputImpl() {
        super(null);
    }

    @Override
    public List<RDFInput> getInputs() {
        Preconditions.checkState(!closed);
        return inputs;
    }
    @Override
    public void addInput(RDFInput input) {
        Preconditions.checkState(!closed);
        inputs.add(input);
        invalidateUnion();
    }

    @Override
    public void addRulesFile(File file) throws IOException {
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            rules.addAll(Rule.parseRules(Rule.rulesParserFromReader(reader)));
        }
    }

    @Nonnull
    @Override
    public List<Rule> customRules() {
        return rules;
    }

    @Nonnull
    @Override
    public Set<Resource> getWanted() {
        Preconditions.checkState(!closed);
        return wanted;
    }
    @Nonnull
    @Override
    public Set<Variable> getWantedAsVariables() {
        Preconditions.checkState(!closed);
        if (wantedAsVars == null) {
            Model nonFinalUnion = null;
            nonFinalUnion = getSkolemizedUnion();
            final Model u = nonFinalUnion;
            wantedAsVars = wanted.stream().filter(u::containsResource)
                    .map(v -> v.isURIResource() ? u.createResource(v.getURI())
                            : u.createResource(v.getId()))
                    .filter(v -> v.canAs(Variable.class)).map(v -> v.as(Variable.class))
                    .collect(Collectors.toSet());
        }
        return wantedAsVars;
    }
    @Override
    public void addWanted(@Nonnull Resource resource) {
        Preconditions.checkState(!closed);
        wanted.add(resource);
        wantedAsVars = null;
    }

    @Nonnull
    @Override
    public Set<Resource> getKnown() {
        Preconditions.checkState(!closed);
        Set<Resource> set = knownAsResources.get();
        if (set == null) {
            set = new HashSet<>(getKnownAsVariables());
            knownAsResources = new WeakReference<>(set);
        }
        return set;
    }

    @Nonnull
    @Override
    public Set<Variable> getKnownAsVariables() {
        Preconditions.checkState(!closed);
        if (known == null) {
            final Model u = getSkolemizedUnion();
            known = new HashSet<>();
            Consumer<Resource> adder = r -> known.add(r.as(Variable.class));
            u.listSubjectsWithProperty(Unserved.literalValue).forEachRemaining(adder);
            u.listSubjectsWithProperty(Unserved.resourceValue).forEachRemaining(adder);
        }
        return known;
    }

    private void invalidateUnion() {
        if (union != null) {
            union.close();
            union = null;
            known = null;
            knownAsResources.clear();
        }
    }


    @Override
    @Nonnull
    public Model initSkolemizedUnion(boolean basicReasoning, boolean inputHasNoSkolemized) throws IOException {
        Preconditions.checkState(!closed);
        if (union == null) {
            union = ModelFactory.createDefaultModel();
            ArrayList<Model> list = new ArrayList<>();
            for (RDFInput input : inputs) list.add(input.getModel());
            skolemizerMap.setCheckSkolem(!inputHasNoSkolemized);
            new Skolemizer(() -> skolemizerMap).skolemizeModels(union, list);
            if (basicReasoning)
                union = UnservedReasoning.wrap(union);
        }
        return union;
    }

    @Override
    @Nonnull
    public Model getSkolemizedUnion() {
        Preconditions.checkState(!closed && union != null);
        return union;
    }

    @Nonnull
    @Override
    public SkolemizerMap getSkolemizerMap() {
        return skolemizerMap;
    }

    @Override
    public void closeImpl() throws ComposerException {
        ArrayList<RDFInput> errors = new ArrayList<>(inputs.size());
        for (RDFInput input : inputs) {
            try {
                input.close();
            } catch (IOException e) {
                errors.add(input);
            }
        }
        if (!errors.isEmpty()) {
            throw new ComposerException(errors.size() + " errors on close(): "
                    + errors.stream().map(RDFInput::toString).reduce((l, r) -> l + ", " + r)
                    .orElse(""));
        }
    }
}

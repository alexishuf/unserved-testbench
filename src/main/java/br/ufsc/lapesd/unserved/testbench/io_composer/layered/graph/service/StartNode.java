package br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service;

import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.And;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.cost.CostInfo;
import br.ufsc.lapesd.unserved.testbench.model.Variable;

import javax.annotation.Nonnull;
import java.util.*;

public class StartNode extends AbstractNode implements Node {
    private final @Nonnull HashSet<Variable> known = new HashSet<>();
    private final @Nonnull And conditions;

    public StartNode(Collection<Variable> known, @Nonnull And conditions) {
        super(new ArrayList<>());
        this.known.addAll(known);
        this.conditions = conditions;
    }

    protected StartNode(StartNode other) {
        super(other);
        this.known.addAll(other.known);
        this.conditions = other.conditions; //Conditions are immutable
    }

    @Nonnull
    @Override
    public Set<Variable> getInputs() {
        return Collections.emptySet();
    }

    @Nonnull
    @Override
    public Set<Variable> getOutputs() {
        return known;
    }

    @Nonnull
    @Override
    public And getPreConditions() {
        return And.empty();
    }

    @Nonnull
    @Override
    public And getPostConditions() {
        return conditions;
    }

    @Override
    public Object getId() {
        return this;
    }

    @Nonnull
    @Override
    public CostInfo getCostInfo() {
        return CostInfo.EMPTY;
    }

    @Override
    public String toString() {
        return String.format("StartNode(%s)", known.stream().map(Object::toString)
                .reduce((l, r) -> l + ", " + r).orElse(""));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        StartNode startNode = (StartNode) o;

        return known.equals(startNode.known);
    }

    @Override
    public int hashCode() {
        return known.hashCode();
    }
}

package br.ufsc.lapesd.unserved.testbench.io_composer.conditions;

import javax.annotation.Nonnull;

/**
 * A class that evaluates conditions.
 *
 * Not all Condition implementations need to be supported by the evaluator.
 */
public interface ConditionEvaluator {
    /**
     * Evaluates the given condition of the given type.
     *
     * @param <T> The condition type
     * @param condition the condition to be evaluated
     * @return true if the condition is true in the context of this evaluator.
     * @throws IllegalArgumentException iff <code>canEvaluate(condition) == false</code>.
     */
    <T extends Condition> boolean evaluate(@Nonnull T condition)
            throws IllegalArgumentException ;

    /**
     * Checks if the particular condition implementation is supported.
     *
     * @param condition instance to check for
     * @param <T> instance static type
     * @return true if evaluate() has an implementation for this condition.
     */
    <T extends Condition> boolean canEvaluate(@Nonnull T condition);
}

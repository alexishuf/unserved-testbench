package br.ufsc.lapesd.unserved.testbench.benchmarks;

import br.ufsc.lapesd.unserved.testbench.components.ComponentRegistrationException;
import br.ufsc.lapesd.unserved.testbench.components.ComponentRegistry;

import javax.annotation.Nonnull;

public interface ComponentRegistrySetup {
    void setArgs(@Nonnull String args);
    void setup(@Nonnull ComponentRegistry registry) throws ComponentRegistrationException;
}

package br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.cost;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.function.BiFunction;

import static java.util.Collections.synchronizedMap;

public class CostInfo {
    private static Map<Class<?>, String> parameterNames = synchronizedMap(new HashMap<>());
    public static CostInfo EMPTY = new CostInfo();

    private @Nonnull final Map<String, CostParameter> parameters;

    public CostInfo(@Nonnull Collection<CostParameter> parameters) {
        this.parameters = new HashMap<>(parameters.size() + 1, 1f);
        parameters.forEach(p -> this.parameters.put(p.getName(), p));
    }

    public CostInfo(CostParameter... parameters) {
        this.parameters = new HashMap<>(parameters.length + 1, 1f);
        for (CostParameter parameter : parameters)
            this.parameters.put(parameter.getName(), parameter);
    }

    public CostInfo(@Nonnull Map<String, CostParameter> parameters) {
        this.parameters = parameters;
    }

    public @Nonnull <T> T get(@Nonnull Class<T> clazz) throws NoSuchElementException {
        String name = parameterNames.computeIfAbsent(clazz, c -> {
            try {
                Field f = clazz.getField("NAME");
                if ((f.getModifiers() & Modifier.STATIC) != 0 && f.getType().equals(String.class))
                    return (String) f.get(null);
            } catch (IllegalAccessException | NoSuchFieldException ignored) { }
            return null;
        });
        if (name == null)
            throw new IllegalArgumentException("Class " + clazz.getName() + " has no \"public static String NAME\" field.");
        //noinspection unchecked
        return (T)get(name);
    }

    public CostParameter getOrElse(@Nonnull String name, @Nullable CostParameter fallback) {
        return parameters.getOrDefault(name, fallback);
    }

    public @Nonnull CostParameter get(@Nonnull String name) throws NoSuchElementException {
        CostParameter param = getOrElse(name, null);
        if (param == null) throw new NoSuchElementException(name);
        return param;
    }

    public Collection<CostParameter> parameters() {
        return parameters.values();
    }

    public @Nonnull CostInfo with(@Nonnull CostParameter parameter) {
        HashMap<String, CostParameter> copy = new HashMap<>(parameters);
        copy.put(parameter.getName(), parameter);
        return new CostInfo(copy);
    }

    public CostInfo aggregateSequence(CostInfo right) {
        return aggregate(right, CostParameter::aggregateSequence);
    }

    public CostInfo aggregateParallel(CostInfo right) {
        return aggregate(right, CostParameter::aggregateParallel);
    }

    protected @Nonnull CostInfo
    aggregate(@Nonnull CostInfo right,
              @Nonnull BiFunction<CostParameter, CostParameter, CostParameter> fn) {
        Map<String, CostParameter> map = new HashMap<>(parameters);
        for (CostParameter theirs : right.parameters()) {
            String name = theirs.getName();
            CostParameter mine = map.get(name);
            map.put(name, mine == null ? theirs : fn.apply(mine, theirs));
        }
        for (CostParameter mine : this.parameters()) {
            String name = mine.getName();
            if (!map.containsKey(name)) {
                CostParameter their = map.get(name);
                map.put(name, their == null ? mine : fn.apply(their, mine));
            }
        }
        return new CostInfo(map);
    }

    @Override
    public String toString() {
        return "CostInfo("+parameters().stream().map(Object::toString)
                .reduce((l, r) -> l + "," + r).orElse("") + ")";
    }
}

package br.ufsc.lapesd.unserved.testbench.benchmarks.design1;

import br.ufsc.lapesd.unserved.testbench.AlgorithmFamily;
import br.ufsc.lapesd.unserved.testbench.benchmarks.design1.data.D1ExperimentData;
import br.ufsc.lapesd.unserved.testbench.benchmarks.experiment.Experiment;
import br.ufsc.lapesd.unserved.testbench.benchmarks.experiment.ExperimentFactory;
import br.ufsc.lapesd.unserved.testbench.benchmarks.experiment.Factors;
import com.google.common.base.Preconditions;

public class D1ExperimentFactory implements ExperimentFactory {
    private D1ExperimentData experimentData;

    public D1ExperimentFactory() {
        this(null);
    }
    public D1ExperimentFactory(D1ExperimentData experimentData) {
        this.experimentData = experimentData;
    }

    public void setExperimentData(D1ExperimentData experimentData) {
        this.experimentData = experimentData;
    }

    @Override
    public Experiment createExperiment(Factors factors) {
        Preconditions.checkState(experimentData != null);
        D1Factors f = (D1Factors) factors;
        if (f.getAlgorithm().getFamily() == AlgorithmFamily.RESTdescPP) {
            return new RESTdescPPD1Experiment(experimentData, f);
        } else if (f.getAlgorithm().getFamily() == AlgorithmFamily.IOGraphPath) {
            return new ComposeWorkflowAppD1Experiment(experimentData, f);
        }
        throw new UnsupportedOperationException();
    }
}

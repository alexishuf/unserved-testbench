package br.ufsc.lapesd.unserved.testbench.model.http.impl;

import br.ufsc.lapesd.unserved.testbench.model.Message;
import br.ufsc.lapesd.unserved.testbench.model.Part;
import br.ufsc.lapesd.unserved.testbench.model.Variable;
import br.ufsc.lapesd.unserved.testbench.model.When;
import br.ufsc.lapesd.unserved.testbench.model.http.AsHttpProperty;
import br.ufsc.lapesd.unserved.testbench.model.http.HttpMessageNode;
import br.ufsc.lapesd.unserved.testbench.model.http.modifiers.AsHttpPropertyModifierHandler;
import br.ufsc.lapesd.unserved.testbench.model.http.modifiers.AsHttpPropertyModifierHandlerFactory;
import br.ufsc.lapesd.unserved.testbench.model.http.modifiers.DefaultAsHttpPropertyModifierHandler;
import br.ufsc.lapesd.unserved.testbench.model.impl.MessageImpl;
import br.ufsc.lapesd.unserved.testbench.model.impl.WithPartsImpl;
import br.ufsc.lapesd.unserved.testbench.util.RDFListHelper;
import org.apache.jena.enhanced.EnhGraph;
import org.apache.jena.graph.Node;
import org.apache.jena.rdf.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public abstract class AbstractHttpMessageNode extends WithPartsImpl implements HttpMessageNode {
    private static Logger logger = LoggerFactory.getLogger(AbstractHttpMessageNode.class);
    private AsHttpPropertyModifierHandlerFactory handlerFactory = DefaultAsHttpPropertyModifierHandler::new;

    private MessageImpl asMessage;
    protected AbstractHttpMessageNode(Node n, EnhGraph m) {
        super(n, m);
        asMessage = (MessageImpl) MessageImpl.factory.wrap(n, m);
    }

    @Override
    public AsHttpPropertyModifierHandlerFactory getAsHttpPropertyModifierHandlerFactory() {
        return handlerFactory;
    }

    @Override
    public void setAsHttpPropertyModifierHandler(AsHttpPropertyModifierHandlerFactory factory) {
        handlerFactory = factory;
    }

    @Override
    public RDFNode getHttpProperty(Resource resource, Property property) {
        Statement statement = getProperty(property);
        if (statement != null) {
            return statement.getObject();
        } else {
            try (AsHttpPropertyModifierHandler modifierHandler
                         = handlerFactory == null ? null : handlerFactory.newInstance()) {
                RDFNode value = processParts(resource, property, modifierHandler);
                if (value != null) return value;
            }
        }

        return null;
    }

    private RDFNode processParts(Resource resource, Property property, AsHttpPropertyModifierHandler modifierHandler) {
        for (Part part : getParts()) {
            if (!part.getPartModifier().canAs(AsHttpProperty.class)) continue;
            AsHttpProperty partModifier = part.getPartModifier().as(AsHttpProperty.class);
            if (!partModifier.getHttpProperty().equals(property)) continue;

            Resource httpResource = partModifier.getHttpResource();
            if (httpResource == null) httpResource = this; //default to the message
            if (!httpResource.equals(resource)) continue;

            RDFNode value = getVariableValue(part);
            if (value == null) continue;
            Resource modifier = partModifier.getModifier();
            if (modifier == null) {
                return value;
            } else {
                if (modifierHandler == null) {
                    logger.error("No modifier handler, will ignore modifier {} on " +
                            "{}.getHttpProperty({}, {})", modifier, this, resource, property);
                    continue;
                }
                modifierHandler.handle(modifier, part.getVariable());
                if (modifierHandler.isComplete())
                    return modifierHandler.getResultNode(resource.getModel());
            }
        }

        /* maybe the handler could not determine it was complete, so we have to notify that
           by asking for a node */
        if (modifierHandler != null && modifierHandler.canGet()) {
            assert !modifierHandler.isComplete();
            return modifierHandler.getResultNode(resource.getModel());
        }
        return null;
    }

    private RDFNode getVariableValue(Part part) {
        RDFNode value = null;
        Literal literalValue = part.getVariable().getLiteralValue();
        if (literalValue != null) value = literalValue;
        Resource resourceValue = part.getVariable().getResourceValue();
        if (resourceValue != null) value = resourceValue;
        return value;
    }

    @Override
    public List<Resource> getHttpPropertyList(Property property) {
        Statement stmt = getProperty(property);
        return stmt != null && stmt.getObject().isResource()
                ? RDFListHelper.collect(stmt.getResource()) : new ArrayList<>();
    }

    @Override
    public Variable getFrom() {
        return asMessage.getFrom();
    }

    @Override
    public Variable getTo() {
        return asMessage.getTo();
    }

    @Override
    public When getWhen() {
        return asMessage.getWhen();
    }

    @Override
    public List<Part> getParts() {
        return asMessage.getParts();
    }

    @Override
    public List<Variable> getVariables() {
        return asMessage.getVariables();
    }

    @Override
    public Stream<Message> getConsequent() {
        return asMessage.getConsequent();
    }
}

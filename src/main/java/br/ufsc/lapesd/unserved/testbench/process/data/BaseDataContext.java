package br.ufsc.lapesd.unserved.testbench.process.data;

import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.IndexedVariable;
import br.ufsc.lapesd.unserved.testbench.model.Variable;
import com.google.common.base.Preconditions;
import org.apache.jena.rdf.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * Utility to make implementation of {@link DataContext} less verbose
 */
public abstract class BaseDataContext implements DataContext {
    private static Logger logger = LoggerFactory.getLogger(BaseDataContext.class);

    private boolean closed = false;

    protected Resource baseResolveAnon(Resource anon) {
        return anon;
    }
    abstract protected Model getModifiableModel();
    abstract protected void baseAdd(@Nonnull Statement statement);
    abstract protected void baseRemove(@Nonnull Statement statement);
    abstract protected void baseRemoveAll(@Nullable Resource subject, @Nullable Property predicate,
                                          @Nullable RDFNode object);
    abstract protected void baseClose();

    protected static Statement stmt(Resource subj, Property predicate, RDFNode object) {
        return ResourceFactory.createStatement(subj, predicate, object);
    }

    @Nullable
    @Override
    public Resource getResource(@Nullable Resource resource) {
        Preconditions.checkState(!closed, "context is closed");

        if (resource == null) return null;
        if (resource.isAnon())
            resource = baseResolveAnon(resource);
        Preconditions.checkArgument(!resource.isAnon(), "Anon resources are not supported");
        if (resource.getModel() == getUnmodifiableModel())
            return resource;

        return getResource(resource.getURI());
    }

    @Nullable
    protected Resource getModifiableResource(@Nullable Resource resource) {
        Preconditions.checkState(!closed, "context is closed");

        Resource local = getResource(resource);
        if (local == null) return null;
        else if (local.isURIResource())
            return getModifiableModel().createResource(local.getURI());
        else
            return getModifiableModel().createResource(local.getId());

    }

    @Nullable
    @Override
    public Resource getResource(@Nonnull String uri) {
        Preconditions.checkState(!closed, "context is closed");
        Preconditions.checkNotNull(uri);

        Model model = getUnmodifiableModel();
        if (!model.containsResource(ResourceFactory.createResource(uri)))
            return null;
        return model.createResource(uri);
    }

    @Override
    public void addStatement(@Nonnull Statement statement) {
        Preconditions.checkState(!closed, "context is closed");
        baseAdd(statement);
    }

    @Override
    public void unsetVariable(@Nonnull Variable var) {
        Preconditions.checkState(!closed, "context is closed");
        Preconditions.checkNotNull(var);

        int index = var instanceof IndexedVariable ? ((IndexedVariable)var).index : 0;
        Resource res = getModifiableResource(var);
        if (res == null) {
            logger.warn("Unknown variable {}", var);
            return;
        }
        Variable local = res.as(Variable.class);
        local.unset(index);
//        baseRemoveAll(var, Unserved.resourceValue, null);
//        baseRemoveAll(var, Unserved.literalValue, null);
//        baseRemoveAll(var, Unserved.valueRepresentation, null);
//        baseRemove(stmt(var, RDF.type, Unserved.LiteralBoundVariable));
//        baseRemove(stmt(var, RDF.type, Unserved.ResourceBoundVariable));
//        baseRemove(stmt(var, RDF.type, Unserved.ValueBoundVariable));
        logger.debug("unsetVariable({})", var.toString());
    }

    @Override
    public void setVariable(@Nonnull Variable var, @Nullable Resource representation,
                            @Nonnull Literal literal) {
        Preconditions.checkState(!closed, "context is closed");
        Preconditions.checkNotNull(var);

        int index = var instanceof IndexedVariable ? ((IndexedVariable)var).index : 0;
        Resource res = getModifiableResource(var);
        if (res == null) {
            logger.warn("Unknown variable {}", var);
            return;
        }
        Variable local = res.as(Variable.class);
        local.set(index, representation, literal);
//        Preconditions.checkState(!closed, "context is closed");
//        Preconditions.checkNotNull(var);
//        Preconditions.checkNotNull(literal);
//
//        unsetVariable(var);
//        if (representation != null)
//            baseAdd(stmt(var, Unserved.valueRepresentation, representation));
//        baseAdd(stmt(var, Unserved.literalValue, literal));
//        baseAdd(stmt(var, RDF.type, Unserved.LiteralBoundVariable));
//        baseAdd(stmt(var, RDF.type, Unserved.ValueBoundVariable));
        logger.debug("setVariable({}, {})", var.toString(), literal.toString());
    }

    @Override
    public void setVariable(@Nonnull Variable var, @Nullable Resource representation,
                            @Nonnull Resource resource) {
        Preconditions.checkState(!closed, "context is closed");
        Preconditions.checkNotNull(var);

        int index = var instanceof IndexedVariable ? ((IndexedVariable)var).index : 0;
        Resource res = getModifiableResource(var);
        if (res == null) {
            logger.warn("Unknown variable {}", var);
            return;
        }
        Variable local = res.as(Variable.class);
        local.set(index, representation, resource);
//        Preconditions.checkState(!closed, "context is closed");
//        Preconditions.checkNotNull(var);
//        Preconditions.checkNotNull(resource);
//
//        unsetVariable(var);
//        if (representation != null)
//            baseAdd(stmt(var, Unserved.valueRepresentation, representation));
//        baseAdd(stmt(var, Unserved.resourceValue, resource));
//        baseAdd(stmt(var, RDF.type, Unserved.ResourceBoundVariable));
//        baseAdd(stmt(var, RDF.type, Unserved.ValueBoundVariable));
        logger.debug("setVariable({}, {})", var.toString(), resource.toString());
    }

    @Override
    public void close() {
        if (closed) return;
        closed = true;
        baseClose();
    }
}

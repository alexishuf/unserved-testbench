package br.ufsc.lapesd.unserved.testbench.util;

import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.vocabulary.RDF;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;

public class RDFListHelper {
    public static @Nullable List<Resource> collect(@Nullable Resource node) {
        if (node == null) return null;
        List<Resource> list = new ArrayList<>();
        while (!node.equals(RDF.nil)) {
            Statement stmt = node.getProperty(RDF.first);
            if (stmt != null && stmt.getObject().isResource())
                list.add(stmt.getResource());
            stmt = node.getProperty(RDF.rest);
            node = stmt != null && stmt.getObject().isResource() ? stmt.getResource() : RDF.nil;
        }
        return list;
    }
}

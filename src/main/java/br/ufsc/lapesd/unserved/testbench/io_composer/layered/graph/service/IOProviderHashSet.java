package br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service;

import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.Condition;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.lazy.EquivalenceSets;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.lazy.SetNode;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.discovery.NodeOutput;
import br.ufsc.lapesd.unserved.testbench.model.Variable;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.SetMultimap;
import org.apache.commons.lang3.tuple.ImmutablePair;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class IOProviderHashSet implements ProviderSet {
    private int unsatisfied = 0;
    private final SetMultimap<Variable, Node> providers = HashMultimap.create();
    private final SetMultimap<ImmutablePair<Variable, Node>, Variable> outputs
            = HashMultimap.create();

    public IOProviderHashSet addTarget(@Nonnull Variable target) {
        if (providers.containsKey(target))
            return this; //nothing to do: either a null or real Nodes
        providers.put(target, null);
        ++unsatisfied;
        return this;
    }

    public IOProviderHashSet add(@Nonnull Variable target, @Nonnull Node node,
                                 @Nonnull Variable output) {
        if (providers.remove(target, null))
            --unsatisfied;
        providers.put(target, node);
        outputs.put(ImmutablePair.of(target, node), output);
        return this;
    }

    public IOProviderHashSet without(@Nonnull FilterMap f) {
        IOProviderHashSet copy = new IOProviderHashSet();
        getTargets().stream().map(f::mapTarget).filter(Objects::nonNull).forEach(copy::addTarget);
        getAssignmentsStream().map(f::mapAssignment).filter(Objects::nonNull)
                .forEach(a -> copy.add(a.target, a.node, a.output));
        return copy;
    }

    public IOProviderHashSet addAll(@Nonnull ProviderSet other) {
        other.getTargets().forEach(this::addTarget);
        for (Variable target : other.getTargets()) {
            for (Node provider : other.getProviders(target)) {
                for (Variable out : other.getOutputs(target, provider)) {
                    add(target, provider, out);
                }
            }
        }
        return this;
    }

    @Nonnull
    @Override
    public Stream<Node> stream() {
        return providers.values().stream().distinct();
    }

    @Override
    public boolean contains(Node node) {
        return providers.containsValue(node);
    }
    @Nonnull
    @Override
    public Iterator<Node> iterator() {
        return stream().iterator();
    }

    @Override
    public void forEach(Consumer<? super Node> consumer) {
        stream().forEach(consumer);
    }

    @Nonnull
    @Override
    public Set<Variable> getUnsatisfiedTargets() {
        return providers.keySet().stream().filter(t -> providers.containsEntry(t, null))
                .collect(Collectors.toSet());
    }

    @Override
    public boolean isSatisfied() {
        return unsatisfied == 0;
    }

    @Nonnull
    @Override
    public Stream<PrecondMap> getPreconditionMappings(EquivalenceSets successorEqSets) {
        return Stream.empty(); //we have no preconditions
    }

    @Nonnull
    @Override
    public EquivalenceSets getEquivalenceSets(@Nonnull PrecondMap map,
                                              @Nullable EquivalenceSets parent) {
        return new EquivalenceSets(); //no conditions to work on
    }

    @Override
    public void bindConstants(@Nonnull Map<Variable, NodeOutput> fullMap, SetNode successor, @Nonnull EquivalenceSets equivalenceSets, @Nonnull Set<IndexedNode> nodes) {
        /* pass -- no conditions */
    }

    @Override
    public boolean satisfyPreconditions(Collection<Node> providers) {
        return true; //we have no preconditions
    }

    @Nonnull
    @Override
    public Set<Condition> getUnsatisfiedConditions() {
        return Collections.emptySet();
    }

    @Nonnull
    @Override
    public Set<Variable> getTargets() {
        return Collections.unmodifiableSet(providers.keySet());
    }

    @Nonnull
    @Override
    public Set<Condition> getConditions() {
        return Collections.emptySet();
    }

    @Nonnull
    @Override
    public Set<Node> getProviders(@Nonnull Variable variable) {
        return providers.get(variable);
    }

    @Nonnull
    @Override
    public Variable getOutput(@Nonnull Variable target, @Nonnull Node node) {
        Set<Variable> set = outputs.get(ImmutablePair.of(target, node));
        assert set.isEmpty() || getProviders(target).contains(node);
        if (set.isEmpty()) throw new NoSuchElementException();
        return set.iterator().next();
    }

    @Override
    public Set<Variable> getOutputs(@Nonnull Variable target, @Nonnull Node provider) {
        return outputs.get(ImmutablePair.of(target, provider));
    }

    @Nonnull
    @Override
    public Set<Node> getProviders(@Nonnull Condition condition) {
        return Collections.emptySet();
    }

    @Nonnull
    @Override
    public Condition getPostcondition(@Nonnull Condition precondition, @Nonnull Node provider) {
        throw new NoSuchElementException();
    }

    @Nonnull
    @Override
    public Set<Condition> getPostconditions(@Nonnull Condition precondition, @Nonnull Node provider) {
        return Collections.emptySet();
    }

    @Nonnull
    @Override
    public Stream<Assignment> getAssignmentsStream() {
        return providers.entries().stream().map(e
                -> new Assignment(e.getKey(), e.getValue(), getOutput(e.getKey(), e.getValue())));
    }

    @Nonnull
    public IOProviderHashSet replace(@Nonnull Node old, @Nonnull CombinedJumpNode replacement) {
        List<Variable> targets = new ArrayList<>();
        for (Variable tgt : new ArrayList<>(providers.keySet())) {
            Set<Node> set = providers.get(tgt);
            if (set.contains(old)) {
                targets.add(tgt);
                set.add(replacement);
                set.remove(old);
            }
        }
        for (Variable target : targets) {
            outputs.removeAll(ImmutablePair.of(target, old));
            outputs.put(ImmutablePair.of(target, replacement), target);
        }
        return this;
    }
}

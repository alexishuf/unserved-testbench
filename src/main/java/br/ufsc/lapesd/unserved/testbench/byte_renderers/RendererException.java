package br.ufsc.lapesd.unserved.testbench.byte_renderers;

public class RendererException extends Exception {
    public RendererException(String s) {
        super(s);
    }

    public RendererException(String s, Throwable throwable) {
        super(s, throwable);
    }
}

package br.ufsc.lapesd.unserved.testbench.io_composer.conditions;

import br.ufsc.lapesd.unserved.testbench.model.Variable;

import javax.annotation.Nonnull;
import java.util.*;

public class ConditionList implements Condition {
    private final @Nonnull List<Condition> children;
    private final @Nonnull String op;

    public ConditionList(@Nonnull String op, Condition... conditions) {
        this.op = op;
        children = Collections.unmodifiableList(Arrays.asList(conditions));

    }
    public ConditionList(@Nonnull String op, @Nonnull Collection<Condition> collection) {
        this.op = op;
        this.children = Collections.unmodifiableList(new ArrayList<>(collection));
    }
    public ConditionList(@Nonnull String op, @Nonnull Collection<Condition> collection1,
                         @Nonnull Collection<Condition> collection2) {
        this.op = op;
        ArrayList<Condition> list = new ArrayList<>(collection1.size() + collection2.size());
        list.addAll(collection1);
        list.addAll(collection2);
        this.children = Collections.unmodifiableList(list);
    }


    @Nonnull
    @Override
    public Condition replacing(@Nonnull Map<Variable, Variable> current2replacement) {
        List<Condition> replaced = new ArrayList<>(children.size());
        for (Condition child : children) replaced.add(child.replacing(current2replacement));
        return new ConditionList(op, replaced);
    }

    public boolean isEmpty() { return  children.isEmpty(); }

    public @Nonnull List<Condition> getMembers() {
        return children;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ConditionList conditionList = (ConditionList) o;
        return Objects.equals(children, conditionList.children);
    }

    @Override
    public int hashCode() {
        return Objects.hash(children);
    }

    @Override
    public String toString() {
        return "(" + children.stream().map(Object::toString)
                .reduce((a, b) -> a + " " + op + " " + b).orElse("") + ")";
    }
}

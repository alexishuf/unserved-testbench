package br.ufsc.lapesd.unserved.testbench.representations;

import com.google.common.reflect.ClassPath;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.vocabulary.RDF;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class DefaultRepresentationUpgrader implements RepresentationUpgrader {
    private static Logger logger = LoggerFactory.getLogger(DefaultRepresentationUpgrader.class);
    private final Map<String, RepresentationUpgrader> upgraders;
    private static DefaultRepresentationUpgrader instance = new DefaultRepresentationUpgrader();

    public static DefaultRepresentationUpgrader getInstance() {
        return instance;
    }

    private DefaultRepresentationUpgrader() {
        this(scanUpgraders());
    }

    public DefaultRepresentationUpgrader(Map<String, RepresentationUpgrader> upgraders) {
        this.upgraders = upgraders;
    }

    @Override
    public boolean upgradeable(Resource representation) {
        StmtIterator it = representation.listProperties(RDF.type);
        while (it.hasNext()) {
            Statement statement = it.next();
            if (!statement.getObject().isResource() || statement.getResource().isAnon()) continue;
            String type = statement.getResource().getURI();
            RepresentationUpgrader upgrader = upgraders.get(type);
            if (upgrader != null && upgrader.upgradeable(representation))
                return true;
        }
        return false;
    }

    @Override
    public void upgrade(Resource representation) {
        StmtIterator it = representation.listProperties(RDF.type);
        List<RepresentationUpgrader> selected = new ArrayList<>();
        while (it.hasNext()) {
            Statement statement = it.next();
            if (!statement.getObject().isResource() || statement.getResource().isAnon()) continue;
            String type = statement.getResource().getURI();
            RepresentationUpgrader upgrader = upgraders.get(type);
            if (upgrader != null && upgrader.upgradeable(representation))
                selected.add(upgrader);
        }
        selected.forEach(u -> u.upgrade(representation));
    }

    private static Map<String, RepresentationUpgrader> scanUpgraders() {
        HashMap<String, RepresentationUpgrader> map = new HashMap<>();
        List<Class<? extends RepresentationUpgrader>> list;
        try {
            //noinspection unchecked
            list = ClassPath.from(DefaultRepresentationUpgrader.class.getClassLoader())
            .getTopLevelClassesRecursive("br.ufsc.lapesd.unserved.testbench.representations")
            .stream().map(ClassPath.ClassInfo::load)
            .filter(RepresentationUpgrader.class::isAssignableFrom)
            .map(c -> (Class<? extends RepresentationUpgrader>) c)
            .collect(Collectors.toList());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        for (Class<? extends RepresentationUpgrader> aClass : list) {
            UpgradesRepresentation annotation = aClass.getAnnotation(UpgradesRepresentation.class);
            if (annotation == null) continue;
            try {
                Constructor<? extends RepresentationUpgrader> constructor = aClass.getConstructor();
                RepresentationUpgrader upgrader = constructor.newInstance();
                map.put(annotation.value(), upgrader);
            } catch (ReflectiveOperationException e) {
                logger.error("Problem registering {}.", aClass.getName(), e);
            }
        }

        return map;
    }
}

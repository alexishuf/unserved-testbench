package br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.discovery;

import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.ConditionEvaluator;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.ProviderSetsSupplier;

import javax.annotation.Nonnull;

public interface LayersState {
    @Nonnull ConditionEvaluator getEvaluator();
    @Nonnull ConditionEvaluator getVariableSpecsEvaluator();
    @Nonnull ProviderSetsSupplier createProviderSetsSupplier();
    @Nonnull NodesSupplier<? extends LayersState> createNodesSupplier();
}

package br.ufsc.lapesd.unserved.testbench.components.expressions.evaluator.instance;

import br.ufsc.lapesd.unserved.testbench.components.expressions.Cardinality;
import br.ufsc.lapesd.unserved.testbench.components.expressions.Cardinality.CardinalityType;
import br.ufsc.lapesd.unserved.testbench.components.expressions.evaluator.ClassExpressionEvaluator.Result;
import br.ufsc.lapesd.unserved.testbench.components.expressions.evaluator.Step;
import br.ufsc.lapesd.unserved.testbench.util.ModelStreams;
import com.google.common.base.Preconditions;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;

import javax.annotation.Nonnull;
import java.util.Collections;
import java.util.stream.Stream;

public class CardinalityInstanceStep extends Step<RDFNode> {
    @Nonnull private final Cardinality expr;
    private final Step<RDFNode> rangeStep;

    public CardinalityInstanceStep(@Nonnull Cardinality expr) {
        super(Collections.emptySet());
        Preconditions.checkArgument(!expr.getCardinalityType().isQualified());
        this.expr = expr;
        this.rangeStep = null;
    }

    public CardinalityInstanceStep(@Nonnull Cardinality expr, @Nonnull Step<RDFNode> rangeStep) {
        super(Collections.emptySet());
        Preconditions.checkArgument(expr.getCardinalityType().isQualified());
        this.expr = expr;
        this.rangeStep = rangeStep;
    }

    @Override
    protected Result performEvaluate(RDFNode node) {
        if (!node.isResource()) return Result.FALSE;
        Resource ind = node.asResource();
        if (Helper.isA(ind, expr.getNames()))
            return Result.TRUE;

        Stream<Resource> stream;
        stream = ModelStreams.objectsOfProperty(ind, expr.getOnProperty(), Resource.class);
        CardinalityType t = expr.getCardinalityType();
        if (t.isQualified()) {
            stream = stream.filter(r -> rangeStep.evaluate(r) == Result.TRUE);
        }

        switch (t) {
            case MIN_CARDINALITY:
            case MIN_QUALIFIED_CARDINALITY:
                return stream.count() >= expr.getValue() ? Result.TRUE : Result.INDETERMINATE;
            case MAX_CARDINALITY:
            case MAX_QUALIFIED_CARDINALITY:
                return stream.count() > expr.getValue() ? Result.FALSE : Result.INDETERMINATE;
            case EXACT_CARDINALITY:
            case EXACT_QUALIFIED_CARDINALITY:
                return Result.INDETERMINATE;
        }
        throw new UnsupportedOperationException();
    }

}

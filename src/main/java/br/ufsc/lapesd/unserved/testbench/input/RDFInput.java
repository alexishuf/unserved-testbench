package br.ufsc.lapesd.unserved.testbench.input;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFFormat;

import java.io.IOException;

public interface RDFInput extends Input {
    RDFFormat N3 = new RDFFormat(Lang.N3);

    RDFFormat getFormat();
    default Lang getLang() {
        return getFormat().getLang();
    }
    @Override
    default String getMediaType() { return getLang().getContentType().getContentType(); }

    Model getModel() throws IOException;

    /**
     * Parses the RDF into a model, closes the RDFInput object and gives ownership of the model
     * to the caller.
     *
     * @return same as getModel(), but ownership is transfered to the caller
     * @throws IOException same as getModel()
     */
    Model takeModel() throws IOException;
}

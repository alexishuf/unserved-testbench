package br.ufsc.lapesd.unserved.testbench.pragproof.context.impl;

import br.ufsc.lapesd.unserved.testbench.input.RDFInput;
import br.ufsc.lapesd.unserved.testbench.input.RDFInputFile;
import br.ufsc.lapesd.unserved.testbench.pragproof.context.PragProofQuery;

import java.io.IOException;

public class DefaultPragProofQuery implements PragProofQuery {
    private RDFInput input;

    public DefaultPragProofQuery(RDFInput input) {
        this.input = input;
    }

    @Override
    public RDFInput getInput() {
        return input;
    }

    @Override
    public void close() throws Exception {
        input.close();
    }

    @Override
    public PragProofQuery createNonIsolatedCopy() {
        try {
            return new DefaultPragProofQuery(new RDFInputFile(input));
        } catch (IOException e) {
            throw new RuntimeException("Failed to copy RDFInput.", e);
        }
    }
}

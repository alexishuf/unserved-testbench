package br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.discovery;

import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.And;
import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.atomic.VariableSpec;
import br.ufsc.lapesd.unserved.testbench.io_composer.impl.IOComposerInput;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.EndNode;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.Node;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.ServiceNode;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.StartNode;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.cost.CostInfoParser;
import br.ufsc.lapesd.unserved.testbench.iri.Unserved;
import br.ufsc.lapesd.unserved.testbench.model.Message;
import br.ufsc.lapesd.unserved.testbench.model.Part;
import br.ufsc.lapesd.unserved.testbench.model.Variable;
import br.ufsc.lapesd.unserved.testbench.util.BFSBGPIterator;
import br.ufsc.lapesd.unserved.testbench.util.DangerousArrayListSet;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.RDFS;

import javax.annotation.Nonnull;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Maintains a index of services by their input variable types and representations.
 */
public class InputIndexedNodesSupplier implements NodesSupplier<IOLayersState> {
    private static final CostInfoParser costInfoParser = new CostInfoParser();
    private Map<Resource, List<MessagePair> > index = null;
    private boolean subclassMatchingEnabled = true;

    @Override
    public boolean isActive() {
        return index != null;
    }

    @Override
    public void begin(IOComposerInput composerInput,
                      @Nonnull StartNode startNode, @Nonnull EndNode endNode) {
        assert index == null;
        index = new HashMap<>();
        Model m = composerInput.getSkolemizedUnion();

        for (Resource r : m.listSubjectsWithProperty(Unserved.reactionTo).toList()) {
            Resource antRes = r.getPropertyResourceValue(Unserved.reactionTo);
            if (!antRes.canAs(Message.class)) continue;

            Message ant = antRes.as(Message.class);
            Set<Variable> inputs = getInputs(ant);
            List<Resource> consList = m.listSubjectsWithProperty(Unserved.when, r).toList();
            if (consList.isEmpty())
                continue;
            /* {?x in consList} => {?x u:when ?y. ?y u:reactionTo ?antRes} */

            List<MessagePair> pairs = new ArrayList<>(consList.size());
            for (Resource consRes : consList) {
                if (!consRes.canAs(Message.class)) continue;
                pairs.add(new MessagePair(inputs, ant, consRes.as(Message.class)));
            }
            for (Resource type : pairs.get(0).inputTs) {
                List<MessagePair> list = index.getOrDefault(type, null);
                if (list == null) index.put(type, list = new ArrayList<>(pairs.size()));
                list.addAll(pairs);
            }
        }
    }

    @Override
    public void end() {
        index = null;
    }

    @Nonnull
    @Override
    public Set<Node> getNodes(@Nonnull IOLayersState state,
                                 @Nonnull Set<Message> blacklist) {
        //@Nonnull Set<Variable> known, @Nonnull Set<Variable> newlyKnown,
        assert index != null;
        Set<Resource> newlyKnownTypes = state.getNewlyKnown().stream().map(VariableSpec::getType)
                .collect(Collectors.toSet());
        Set<Resource> knownTypes = Stream.concat(newlyKnownTypes.stream(),
                state.getKnown().stream().map(VariableSpec::getType).filter(Objects::nonNull)
        ).map(this::getWithSuperClasses).flatMap(Set::stream).collect(Collectors.toSet());

        Set<Node> set = newlyKnownTypes.stream().map(this::getWithSuperClasses)
                .flatMap(Set::stream).distinct()
                .map(type -> index.getOrDefault(type, Collections.emptyList()))
                .flatMap(List::stream).distinct()
                .filter(p -> !blacklist.contains(p.consequent) && knownTypes.containsAll(p.inputTs))
                .map(p -> (Node) p.createServiceNode()).collect(Collectors.toSet());
        state.advance(set);
        return set;
    }

    public boolean isSubclassMatchingEnabled() {
        return subclassMatchingEnabled;
    }
    public void setSubclassMatchingEnabled(boolean subclassMatchingEnabled) {
        this.subclassMatchingEnabled = subclassMatchingEnabled;
    }

    protected Set<Resource> getWithSuperClasses(Resource type) {
        return !subclassMatchingEnabled ? Collections.singleton(type) : BFSBGPIterator.from(type)
                .withInitial().forward(RDFS.subClassOf).toCollection(HashSet::new);
    }

    private Set<Variable> getInputs(Message message) {
        return message.getParts().stream().map(Part::getVariable)
                .collect(Collectors.toCollection(DangerousArrayListSet::new));
    }

    private static final class MessagePair {
        public final @Nonnull Set<Variable> inputs;
        public final @Nonnull Set<Resource> inputTs;
        public final @Nonnull Message antecedent;
        public final @Nonnull Message consequent;

        public MessagePair(@Nonnull Set<Variable> inputs, @Nonnull Message antecedent,
                           @Nonnull Message consequent) {
            this.inputs = inputs;
            this.inputTs = inputs.stream().map(Variable::getType).collect(Collectors.toSet());
            this.antecedent = antecedent;
            this.consequent = consequent;
        }

        @Nonnull
        public ServiceNode createServiceNode() {
            return new ServiceNode(antecedent, consequent, new ArrayList<>(),
                    And.empty(), And.empty(), costInfoParser.parse(consequent), inputs);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            MessagePair that = (MessagePair) o;

            return antecedent.equals(that.antecedent) && consequent.equals(that.consequent);
        }

        @Override
        public int hashCode() {
            int result = antecedent.hashCode();
            result = 31 * result + consequent.hashCode();
            return result;
        }
    }
}

package br.ufsc.lapesd.unserved.testbench.components.expressions;

import org.apache.jena.rdf.model.Resource;

import java.util.Set;

public interface ClassExpression {
    enum Type {
        /**
         * owl:unionOf, owl:intersectionOf and owl:complementOf
         */
        Connective,
        /**
         * owl:someValuesFrom, owl:allValuesFrom, owl:hasValue and owl:hasSelf
         */
        PropertyRestriction,
        /**
         * owl:minCardinality, owl:maxCardinality, owl:cardinality and *Qualified variants
         */
        Cardinality,
        /**
         * owl:oneOf
         */
        Enumeration,
        /**
         * Not a expression, simply a class
         */
        Atomic,
    }

    Type getType();
    Set<ClassExpression> getChildren();
    Set<Resource> getNames();
}

package br.ufsc.lapesd.unserved.testbench.benchmarks.design1;

import br.ufsc.lapesd.unserved.testbench.Algorithm;
import br.ufsc.lapesd.unserved.testbench.benchmarks.experiment.ExperimentDesignLevels;
import br.ufsc.lapesd.unserved.testbench.benchmarks.experiment.Factors;
import com.google.common.base.Preconditions;

import java.util.Map;

public class D1ExperimentLevels implements ExperimentDesignLevels {
    private Values low, high, fallback = null;

    @Override
    public Factors createFactors(Map<String, String> levels) {
        return new D1Factors.Builder()
                .setPreheatCount(getValue(levels, "preheatCount", Integer.class))
                .setServiceCount(getValue(levels, "serviceCount", Integer.class))
                .setSolutionLength(getValue(levels, "solutionLength", Integer.class))
                .setSolutionParallelSequences(getValue(levels, "solutionParallelSequences", Integer.class))
                .setSolutionServiceAlternatives(getValue(levels, "solutionServiceAlternatives", Integer.class))
                .setInputSetsCount(getValue(levels, "inputSetsCount", Integer.class))
                .setInputSetsSize(getValue(levels, "inputSetsSize", Integer.class))
                .setFreeOutputCount(getValue(levels, "freeOutputCount", Integer.class))
                .setConnectedIOs(getValue(levels, "connectedIOs", Integer.class))
                .setExecute(getValue(levels, "execute", Boolean.class))
                .setAlgorithm(getValue(levels, "algorithm", Algorithm.class))
                .build();
    }

    private <T> T getValue(Map<String, String> map, String name, Class<T> theClass) {
        return getValue(name, map.getOrDefault(name, null), theClass);
    }
    private <T> T getValue(String name, String value, Class<T> theClass) {
        int level = value == null ? 0 : Integer.parseInt(value);
        Values values = level == -1 ? low : (level == 1 ? high : fallback);
        return values.getValue(name, theClass);
    }

    private static class Values {
        private int serviceCount, solutionParallelSequences, solutionLength,
                solutionServiceAlternatives, preheatCount, inputSetsCount, inputSetsSize,
                freeOutputCount, connectedIOs;
        private boolean execute;
        private Algorithm algorithm;

        private Object getValue(String name) {
            try {
                return Values.class.getDeclaredField(name).get(this);
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            } catch (NoSuchFieldException e) {
                throw new IllegalArgumentException("Unknown name " + name);
            }
        }

        private <T> T getValue(String name, Class<T> theClass) {
            Object o = getValue(name);
            if (o != null)
                Preconditions.checkArgument(theClass.isAssignableFrom(o.getClass()));
            //noinspection unchecked
            return (T)o;
        }
    }
}

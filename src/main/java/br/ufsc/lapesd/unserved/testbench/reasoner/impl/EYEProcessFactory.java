package br.ufsc.lapesd.unserved.testbench.reasoner.impl;

import br.ufsc.lapesd.unserved.testbench.reasoner.N3Reasoner;
import br.ufsc.lapesd.unserved.testbench.reasoner.N3ReasonerException;
import br.ufsc.lapesd.unserved.testbench.reasoner.N3ReasonerFactory;
import org.apache.jena.ext.com.google.common.base.Preconditions;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.Arrays;

/**
 * Factory for EYEProcess N3 reasoner.
 */
public class EYEProcessFactory implements N3ReasonerFactory {
    private String path;

    public EYEProcessFactory(@Nonnull String path) {
        Preconditions.checkNotNull(path);
        this.path = path;

    }

    /**
     * Constructor guessing the path to the eye executable.
     *
     * @throws N3ReasonerException If a valid path could not be guessed.
     */
    public EYEProcessFactory() throws N3ReasonerException {
        path = guessPath();
        if (path == null)
            throw new N3ReasonerException("Couldn't guess a path for the eye reasoner.");
    }

    private static String guessPath() {
        return Arrays.asList("eye.sh", "eye", "eye.bat", "/opt/eye/bin/eye.sh",
                "/opt/eye/bin/eye").stream()
                .filter(EYEProcessFactory::tryEYE).findFirst().orElse(null);
    }

    private static boolean tryEYE(String binaryName) {
        try {
            Process process = new ProcessBuilder().command(binaryName, "--version").start();
            process.waitFor();
        } catch (IOException | InterruptedException e) {
            return false;
        }
        return true;
    }

    @Override
    public N3Reasoner createReasoner() {
        return new EYEProcess(path);
    }
}

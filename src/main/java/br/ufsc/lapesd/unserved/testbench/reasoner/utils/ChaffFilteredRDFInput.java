package br.ufsc.lapesd.unserved.testbench.reasoner.utils;

import br.ufsc.lapesd.unserved.testbench.input.AbstractRDFInput;
import br.ufsc.lapesd.unserved.testbench.input.ClosedException;
import br.ufsc.lapesd.unserved.testbench.input.RDFInput;
import org.apache.jena.rdf.model.*;
import org.apache.jena.riot.RDFFormat;
import org.apache.jena.vocabulary.RDF;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.HashSet;

public class ChaffFilteredRDFInput extends AbstractRDFInput {
    private final RDFInput input;
    private static Resource Chaff =
            ResourceFactory.createResource("http://www.w3.org/2000/10/swap/log#Chaff");

    public ChaffFilteredRDFInput(@Nonnull RDFInput input, @Nonnull RDFFormat format) {
        super(format);
        this.input = input;
    }
    public ChaffFilteredRDFInput(@Nonnull RDFInput input) {
        super(input.getFormat());
        this.input = input;
    }

    @Override
    protected Model loadModel() throws IOException {
        if (closed) throw new ClosedException();
        try {
            Model inputModel = input.getModel();
            HashSet<Resource> chaffs = new HashSet<>();
            inputModel.listStatements(null, RDF.type, Chaff)
                    .forEachRemaining(s -> chaffs.add(s.getSubject()));


            Model model = ModelFactory.createDefaultModel();
            model.setNsPrefixes(inputModel.getNsPrefixMap());
            inputModel.listStatements().forEachRemaining(s -> {
                if (chaffs.contains(s.getSubject()))
                    return;
                RDFNode object = s.getObject();
                if (object.isResource() && chaffs.contains(object.asResource()))
                    return;
                model.add(s);
            });

            return model;
        } finally {
            input.close();
        }

    }
}

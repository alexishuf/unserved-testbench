package br.ufsc.lapesd.unserved.testbench.process.model;

import java.util.List;

/**
 * Enhanced node interface for unserved-p:Sequence
 */
public interface Sequence extends Action {
    List<List<Action>> getMembersLists();
    List<Action> getMembers();
}

package br.ufsc.lapesd.unserved.testbench.util.jena;

import org.apache.jena.datatypes.RDFDatatype;
import org.apache.jena.graph.Graph;
import org.apache.jena.graph.Node;
import org.apache.jena.graph.Triple;
import org.apache.jena.rdf.model.*;
import org.apache.jena.shared.Command;
import org.apache.jena.shared.Lock;
import org.apache.jena.shared.PrefixMapping;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

/**
 * Indirection helper to that delegates all operations to another model, which can be changed.
 */
public class ModelReference implements Model {
    private Supplier<Model> refereeAccessor;
    private GraphReference graphReference;

    public ModelReference(Supplier<Model> refereeAccessor) {
        this.refereeAccessor = refereeAccessor;
        this.graphReference = new GraphReference(() -> getReferee().getGraph());
    }

    public Model getReferee() {
        return getRefereeAccessor().get();
    }

    public void setReferee(Model referee) {
        setRefereeAccessor(() -> referee);
    }

    public Supplier<Model> getRefereeAccessor() {
        return refereeAccessor;
    }

    public void setRefereeAccessor(Supplier<Model> refereeAccessor) {
        this.refereeAccessor = refereeAccessor;
    }

    private RDFNode wrapNode(RDFNode node) {
        return new RDFNodeModelReference(node, this);
    }
    private <T extends RDFNode> T wrapNodeAs(RDFNode node, Class<T> clazz) {
        return wrapNode(node).as(clazz);
    }
    private Resource wrapResource(Resource resource) {
        return wrapNodeAs(resource, Resource.class);
    }
    private Literal wrapLiteral(Literal resource) {
        return wrapNodeAs(resource, Literal.class);
    }
    private Property wrapProperty(Property resource) {
        return wrapNodeAs(resource, Property.class);
    }
    private Statement wrapStatement(Statement statement) {
        return new StatementReference(this, this::wrapNode, statement);
    }

    @Override
    public long size() {
        return getReferee().size();
    }

    @Override
    public boolean isEmpty() {
        return getReferee().isEmpty();
    }

    @Override
    public ResIterator listSubjects() {
        return new MapFilterResIterator<>(this::wrapResource, getReferee().listSubjects());
    }

    @Override
    public NsIterator listNameSpaces() {
        return getReferee().listNameSpaces();
    }

    @Override
    public Resource getResource(String uri) {
        return wrapResource(getReferee().getResource(uri));
    }

    @Override
    public Property getProperty(String nameSpace, String localName) {
        return wrapProperty(getReferee().getProperty(nameSpace, localName));
    }

    @Override
    public Resource createResource() {
        return wrapResource(getReferee().createResource());
    }

    @Override
    public Resource createResource(AnonId id) {
        return wrapResource(getReferee().createResource(id));
    }

    @Override
    public Resource createResource(String uri) {
        return wrapResource(getReferee().createResource(uri));
    }

    @Override
    public Property createProperty(String nameSpace, String localName) {
        return wrapProperty(getReferee().createProperty(nameSpace, localName));
    }

    @Override
    public Literal createLiteral(String v, String language) {
        return wrapLiteral(getReferee().createLiteral(v, language));
    }

    @Override
    public Literal createLiteral(String v, boolean wellFormed) {
        return wrapLiteral(getReferee().createLiteral(v, wellFormed));
    }

    @Override
    public Literal createTypedLiteral(String lex, RDFDatatype dtype) {
        return wrapLiteral(getReferee().createTypedLiteral(lex, dtype));
    }

    @Override
    public Literal createTypedLiteral(Object value, RDFDatatype dtype) {
        return wrapLiteral(getReferee().createTypedLiteral(value, dtype));
    }

    @Override
    public Literal createTypedLiteral(Object value) {
        return wrapLiteral(getReferee().createTypedLiteral(value));
    }

    @Override
    public Statement createStatement(Resource s, Property p, RDFNode o) {
        return wrapStatement(getReferee().createStatement(s, p, o));
    }

    @Override
    public RDFList createList() {
        return wrapNodeAs(getReferee().createList(), RDFList.class);
    }

    @Override
    public RDFList createList(Iterator<? extends RDFNode> members) {
        return wrapNodeAs(getReferee().createList(members), RDFList.class);
    }

    @Override
    public RDFList createList(RDFNode[] members) {
        return wrapNodeAs(getReferee().createList(members), RDFList.class);
    }

    @Override
    public Model add(Statement s) {
        getReferee().add(s);
        return this;
    }

    @Override
    public Model add(Statement[] statements) {
        getReferee().add(statements);
        return this;
    }

    @Override
    public Model remove(Statement[] statements) {
        getReferee().remove(statements);
        return this;
    }

    @Override
    public Model add(List<Statement> statements) {
        getReferee().add(statements);
        return this;
    }

    @Override
    public Model remove(List<Statement> statements) {
        getReferee().remove(statements);
        return this;
    }

    @Override
    public Model add(StmtIterator iter) {
        getReferee().add(iter);
        return this;
    }

    @Override
    public Model add(Model m) {
        getReferee().add(m);
        return this;
    }

    @Override
    public Model read(String url) {
        getReferee().read(url);
        return this;
    }

    @Override
    public Model read(InputStream in, String base) {
        getReferee().read(in, base);
        return this;
    }

    @Override
    public Model read(InputStream in, String base, String lang) {
        getReferee().read(in, base, lang);
        return this;
    }

    @Override
    public Model read(Reader reader, String base) {
        getReferee().read(reader, base);
        return this;
    }

    @Override
    public Model read(String url, String lang) {
        getReferee().read(url, lang);
        return this;
    }

    @Override
    public Model read(Reader reader, String base, String lang) {
        getReferee().read(reader, base, lang);
        return this;
    }

    @Override
    public Model read(String url, String base, String lang) {
        getReferee().read(url, base, lang);
        return this;
    }

    @Override
    public Model write(Writer writer) {
        getReferee().write(writer);
        return this;
    }

    @Override
    public Model write(Writer writer, String lang) {
        getReferee().write(writer, lang);
        return this;
    }

    @Override
    public Model write(Writer writer, String lang, String base) {
        getReferee().write(writer, lang, base);
        return this;
    }

    @Override
    public Model write(OutputStream out) {
        getReferee().write(out);
        return this;
    }

    @Override
    public Model write(OutputStream out, String lang) {
        getReferee().write(out, lang);
        return this;
    }

    @Override
    public Model write(OutputStream out, String lang, String base) {
        getReferee().write(out, lang, base);
        return this;
    }

    @Override
    public Model remove(Statement s) {
        getReferee().remove(s);
        return this;
    }

    @Override
    public Statement getRequiredProperty(Resource s, Property p) {
        return wrapStatement(getReferee().getRequiredProperty(s, p));
    }

    @Override
    public Statement getProperty(Resource s, Property p) {
        return wrapStatement(getReferee().getProperty(s, p));
    }

    @Override
    public ResIterator listSubjectsWithProperty(Property p) {
        return new MapFilterResIterator<>(this::wrapResource, getReferee().listSubjectsWithProperty(p));
    }

    @Override
    public ResIterator listResourcesWithProperty(Property p) {
        return new MapFilterResIterator<>(this::wrapResource, getReferee().listResourcesWithProperty(p));
    }

    @Override
    public ResIterator listSubjectsWithProperty(Property p, RDFNode o) {
        return new MapFilterResIterator<>(this::wrapResource, getReferee().listSubjectsWithProperty(p, o));
    }

    @Override
    public ResIterator listResourcesWithProperty(Property p, RDFNode o) {
        return new MapFilterResIterator<>(this::wrapResource, getReferee().listResourcesWithProperty(p, o));
    }

    @Override
    public NodeIterator listObjects() {
        return new MapFilterNodeIterator<>(this::wrapNode, getReferee().listObjects());
    }

    @Override
    public NodeIterator listObjectsOfProperty(Property p) {
        return new MapFilterNodeIterator<>(this::wrapNode, getReferee().listObjectsOfProperty(p));
    }

    @Override
    public NodeIterator listObjectsOfProperty(Resource s, Property p) {
        return new MapFilterNodeIterator<>(this::wrapNode, getReferee().listObjectsOfProperty(s, p));
    }

    @Override
    public boolean contains(Resource s, Property p) {
        return getReferee().contains(s, p);
    }

    @Override
    public boolean containsResource(RDFNode r) {
        return getReferee().containsResource(r);
    }

    @Override
    public boolean contains(Resource s, Property p, RDFNode o) {
        return getReferee().contains(s, p, o);
    }

    @Override
    public boolean contains(Statement s) {
        return getReferee().contains(s);
    }

    @Override
    public boolean containsAny(StmtIterator iter) {
        return getReferee().containsAny(iter);
    }

    @Override
    public boolean containsAll(StmtIterator iter) {
        return getReferee().containsAll(iter);
    }

    @Override
    public boolean containsAny(Model model) {
        return getReferee().containsAny(model);
    }

    @Override
    public boolean containsAll(Model model) {
        return getReferee().containsAll(model);
    }

    @Override
    public boolean isReified(Statement s) {
        return getReferee().isReified(s);
    }

    @Override
    public Resource getAnyReifiedStatement(Statement s) {
        return wrapResource(getReferee().getAnyReifiedStatement(s));
    }

    @Override
    public void removeAllReifications(Statement s) {
        getReferee().removeAllReifications(s);
    }

    @Override
    public void removeReification(ReifiedStatement rs) {
        getReferee().removeReification(rs);
    }

    @Override
    public StmtIterator listStatements() {
        return new MapFilterStmtIterator<>(this::wrapStatement, getReferee().listStatements());
    }

    @Override
    public StmtIterator listStatements(Selector s) {
        return new MapFilterStmtIterator<>(this::wrapStatement, getReferee().listStatements(s));
    }

    @Override
    public StmtIterator listStatements(Resource s, Property p, RDFNode o) {
        return new MapFilterStmtIterator<>(this::wrapStatement, getReferee().listStatements(s, p, o));
    }

    @Override
    public ReifiedStatement createReifiedStatement(Statement s) {
        return wrapNodeAs(getReferee().createReifiedStatement(s), ReifiedStatement.class);
    }

    @Override
    public ReifiedStatement createReifiedStatement(String uri, Statement s) {
        return wrapNodeAs(getReferee().createReifiedStatement(uri, s), ReifiedStatement.class);
    }

    @Override
    public RSIterator listReifiedStatements() {
        return new MapFilterRSIterator<>(s -> wrapNodeAs(s, ReifiedStatement.class), getReferee().listReifiedStatements());
    }

    @Override
    public RSIterator listReifiedStatements(Statement st) {
        return new MapFilterRSIterator<>(s -> wrapNodeAs(s, ReifiedStatement.class), getReferee().listReifiedStatements(st));
    }

    @Override
    public Model query(Selector s) {
        getReferee().query(s);
        return this;
    }

    @Override
    public Model union(Model model) {
        getReferee().union(model);
        return this;
    }

    @Override
    public Model intersection(Model model) {
        getReferee().intersection(model);
        return this;
    }

    @Override
    public Model difference(Model model) {
        getReferee().difference(model);
        return this;
    }

    @SuppressWarnings("EqualsWhichDoesntCheckParameterClass")
    @Override
    public boolean equals(Object m) {
        return getReferee().equals(m);
    }

    @Override
    public Model begin() {
        getReferee().begin();
        return this;
    }

    @Override
    public Model abort() {
        getReferee().abort();
        return this;
    }

    @Override
    public Model commit() {
        getReferee().commit();
        return this;
    }

    @Override
    public Object executeInTransaction(Command cmd) {
        return getReferee().executeInTransaction(cmd);
    }

    @Override
    public boolean independent() {
        return getReferee().independent();
    }

    @Override
    public boolean supportsTransactions() {
        return getReferee().supportsTransactions();
    }

    @Override
    public boolean supportsSetOperations() {
        return getReferee().supportsSetOperations();
    }

    @Override
    public boolean isIsomorphicWith(Model g) {
        return getReferee().isIsomorphicWith(g);
    }

    @Override
    public void close() {
        getReferee().close();
    }

    @Override
    public Lock getLock() {
        return getReferee().getLock();
    }

    @Override
    public Model register(ModelChangedListener listener) {
        getReferee().register(listener);
        return this;
    }

    @Override
    public Model unregister(ModelChangedListener listener) {
        getReferee().unregister(listener);
        return this;
    }

    @Override
    public Model notifyEvent(Object e) {
        getReferee().notifyEvent(e);
        return this;
    }

    @Override
    public Model removeAll() {
        getReferee().removeAll();
        return this;
    }

    @Override
    public Model removeAll(Resource s, Property p, RDFNode r) {
        getReferee().removeAll(s, p, r);
        return this;
    }

    @Override
    public boolean isClosed() {
        return getReferee().isClosed();
    }

    @Override
    @Deprecated
    public Resource getResource(String uri, ResourceF f) {
        return wrapResource(getReferee().getResource(uri, f));
    }

    @Override
    public Property getProperty(String uri) {
        return wrapProperty(getReferee().getProperty(uri));
    }

    @Override
    public Bag getBag(String uri) {
        return wrapNodeAs(getReferee().getBag(uri), Bag.class);
    }

    @Override
    public Bag getBag(Resource r) {
        return wrapNodeAs(getReferee().getBag(r), Bag.class);
    }

    @Override
    public Alt getAlt(String uri) {
        return wrapNodeAs(getReferee().getAlt(uri), Alt.class);
    }

    @Override
    public Alt getAlt(Resource r) {
        return wrapNodeAs(getReferee().getAlt(r), Alt.class);
    }

    @Override
    public Seq getSeq(String uri) {
        return wrapNodeAs(getReferee().getSeq(uri), Seq.class);
    }

    @Override
    public Seq getSeq(Resource r) {
        return wrapNodeAs(getReferee().getSeq(r), Seq.class);
    }

    @Override
    public Resource createResource(Resource type) {
        return wrapResource(getReferee().createResource(type));
    }

    @Override
    public RDFNode getRDFNode(Node n) {
        return wrapNode(getReferee().getRDFNode(n));
    }

    @Override
    public Resource createResource(String uri, Resource type) {
        return wrapResource(getReferee().createResource(uri, type));
    }

    @Override
    @Deprecated
    public Resource createResource(ResourceF f) {
        return wrapResource(getReferee().createResource(f));
    }

    @Override
    @Deprecated
    public Resource createResource(String uri, ResourceF f) {
        return wrapResource(getReferee().createResource(uri, f));
    }

    @Override
    public Property createProperty(String uri) {
        return  wrapProperty(getReferee().createProperty(uri));
    }

    @Override
    public Literal createLiteral(String v) {
        return wrapLiteral(getReferee().createLiteral(v));
    }

    @Override
    public Literal createTypedLiteral(boolean v) {
        return wrapLiteral(getReferee().createTypedLiteral(v));
    }

    @Override
    public Literal createTypedLiteral(int v) {
        return wrapLiteral(getReferee().createTypedLiteral(v));
    }

    @Override
    public Literal createTypedLiteral(long v) {
        return wrapLiteral(getReferee().createTypedLiteral(v));
    }

    @Override
    public Literal createTypedLiteral(Calendar d) {
        return wrapLiteral(getReferee().createTypedLiteral(d));
    }

    @Override
    public Literal createTypedLiteral(char v) {
        return wrapLiteral(getReferee().createTypedLiteral(v));
    }

    @Override
    public Literal createTypedLiteral(float v) {
        return wrapLiteral(getReferee().createTypedLiteral(v));
    }

    @Override
    public Literal createTypedLiteral(double v) {
        return wrapLiteral(getReferee().createTypedLiteral(v));
    }

    @Override
    public Literal createTypedLiteral(String v) {
        return wrapLiteral(getReferee().createTypedLiteral(v));
    }

    @Override
    public Literal createTypedLiteral(String lex, String typeURI) {
        return wrapLiteral(getReferee().createTypedLiteral(lex, typeURI));
    }

    @Override
    public Literal createTypedLiteral(Object value, String typeURI) {
        return wrapLiteral(getReferee().createTypedLiteral(value, typeURI));
    }

    @Override
    public Statement createLiteralStatement(Resource s, Property p, boolean o) {
        return wrapStatement(getReferee().createLiteralStatement(s, p, o));
    }

    @Override
    public Statement createLiteralStatement(Resource s, Property p, float o) {
        return wrapStatement(getReferee().createLiteralStatement(s, p, o));
    }

    @Override
    public Statement createLiteralStatement(Resource s, Property p, double o) {
        return wrapStatement(getReferee().createLiteralStatement(s, p, o));
    }

    @Override
    public Statement createLiteralStatement(Resource s, Property p, long o) {
        return wrapStatement(getReferee().createLiteralStatement(s, p, o));
    }

    @Override
    public Statement createLiteralStatement(Resource s, Property p, int o) {
        return wrapStatement(getReferee().createLiteralStatement(s, p, o));
    }

    @Override
    public Statement createLiteralStatement(Resource s, Property p, char o) {
        return wrapStatement(getReferee().createLiteralStatement(s, p, o));
    }

    @Override
    public Statement createLiteralStatement(Resource s, Property p, Object o) {
        return wrapStatement(getReferee().createLiteralStatement(s, p, o));
    }

    @Override
    public Statement createStatement(Resource s, Property p, String o) {
        return wrapStatement(getReferee().createStatement(s, p, o));
    }

    @Override
    public Statement createStatement(Resource s, Property p, String o, String l) {
        return wrapStatement(getReferee().createStatement(s, p, o, l));
    }

    @Override
    public Statement createStatement(Resource s, Property p, String o, boolean wellFormed) {
        return wrapStatement(getReferee().createStatement(s, p, o, wellFormed));
    }

    @Override
    public Statement createStatement(Resource s, Property p, String o, String l, boolean wellFormed) {
        return wrapStatement(getReferee().createStatement(s, p, o, l, wellFormed));
    }

    @Override
    public Bag createBag() {
        return wrapNodeAs(getReferee().createBag(), Bag.class);
    }

    @Override
    public Bag createBag(String uri) {
        return wrapNodeAs(getReferee().createBag(uri), Bag.class);
    }

    @Override
    public Alt createAlt() {
        return wrapNodeAs(getReferee().createAlt(), Alt.class);
    }

    @Override
    public Alt createAlt(String uri) {
        return wrapNodeAs(getReferee().createAlt(uri), Alt.class);
    }

    @Override
    public Seq createSeq() {
        return wrapNodeAs(getReferee().createSeq(), Seq.class);
    }

    @Override
    public Seq createSeq(String uri) {
        return wrapNodeAs(getReferee().createSeq(uri), Seq.class);
    }

    @Override
    public Model add(Resource s, Property p, RDFNode o) {
        getReferee().add(s, p, o);
        return this;
    }

    @Override
    public Model addLiteral(Resource s, Property p, boolean o) {
        getReferee().addLiteral(s, p, o);
        return this;
    }

    @Override
    public Model addLiteral(Resource s, Property p, long o) {
        getReferee().addLiteral(s, p, o);
        return this;
    }

    @Override
    public Model addLiteral(Resource s, Property p, int o) {
        getReferee().addLiteral(s, p, o);
        return this;
    }

    @Override
    public Model addLiteral(Resource s, Property p, char o) {
        getReferee().addLiteral(s, p, o);
        return this;
    }

    @Override
    public Model addLiteral(Resource s, Property p, float o) {
        getReferee().addLiteral(s, p, o);
        return this;
    }

    @Override
    public Model addLiteral(Resource s, Property p, double o) {
        getReferee().addLiteral(s, p, o);
        return this;
    }

    @Override
    @Deprecated
    public Model addLiteral(Resource s, Property p, Object o) {
        getReferee().addLiteral(s, p, o);
        return this;
    }

    @Override
    public Model addLiteral(Resource s, Property p, Literal o) {
        getReferee().addLiteral(s, p, o);
        return this;
    }

    @Override
    public Model add(Resource s, Property p, String o) {
        getReferee().add(s, p, o);
        return this;
    }

    @Override
    public Model add(Resource s, Property p, String lex, RDFDatatype datatype) {
        getReferee().add(s, p, lex, datatype);
        return this;
    }

    @Override
    public Model add(Resource s, Property p, String o, boolean wellFormed) {
        getReferee().add(s, p, o, wellFormed);
        return this;
    }

    @Override
    public Model add(Resource s, Property p, String o, String l) {
        getReferee().add(s, p, o, l);
        return this;
    }

    @Override
    public Model remove(Resource s, Property p, RDFNode o) {
        getReferee().remove(s, p, o);
        return this;
    }

    @Override
    public Model remove(StmtIterator iter) {
        getReferee().remove(iter);
        return this;
    }

    @Override
    public Model remove(Model m) {
        getReferee().remove(m);
        return this;
    }

    @Override
    public StmtIterator listLiteralStatements(Resource subject, Property predicate, boolean object) {
        return new MapFilterStmtIterator<>(this::wrapStatement, getReferee().listLiteralStatements(subject, predicate, object));
    }

    @Override
    public StmtIterator listLiteralStatements(Resource subject, Property predicate, char object) {
        return new MapFilterStmtIterator<>(this::wrapStatement, getReferee().listLiteralStatements(subject, predicate, object));
    }

    @Override
    public StmtIterator listLiteralStatements(Resource subject, Property predicate, long object) {
        return new MapFilterStmtIterator<>(this::wrapStatement, getReferee().listLiteralStatements(subject, predicate, object));
    }

    @Override
    public StmtIterator listLiteralStatements(Resource subject, Property predicate, float object) {
        return new MapFilterStmtIterator<>(this::wrapStatement, getReferee().listLiteralStatements(subject, predicate, object));
    }

    @Override
    public StmtIterator listLiteralStatements(Resource subject, Property predicate, double object) {
        return new MapFilterStmtIterator<>(this::wrapStatement, getReferee().listLiteralStatements(subject, predicate, object));
    }

    @Override
    public StmtIterator listStatements(Resource subject, Property predicate, String object) {
        return new MapFilterStmtIterator<>(this::wrapStatement, getReferee().listStatements(subject, predicate, object));
    }

    @Override
    public StmtIterator listStatements(Resource subject, Property predicate, String object, String lang) {
        return new MapFilterStmtIterator<>(this::wrapStatement, getReferee().listStatements(subject, predicate, object, lang));
    }

    @Override
    public ResIterator listResourcesWithProperty(Property p, boolean o) {
        return new MapFilterResIterator<>(this::wrapResource, getReferee().listResourcesWithProperty(p, o));
    }

    @Override
    public ResIterator listResourcesWithProperty(Property p, long o) {
        return new MapFilterResIterator<>(this::wrapResource, getReferee().listResourcesWithProperty(p, o));
    }

    @Override
    public ResIterator listResourcesWithProperty(Property p, char o) {
        return new MapFilterResIterator<>(this::wrapResource, getReferee().listResourcesWithProperty(p, o));
    }

    @Override
    public ResIterator listResourcesWithProperty(Property p, float o) {
        return new MapFilterResIterator<>(this::wrapResource, getReferee().listResourcesWithProperty(p, o));
    }

    @Override
    public ResIterator listResourcesWithProperty(Property p, double o) {
        return new MapFilterResIterator<>(this::wrapResource, getReferee().listResourcesWithProperty(p, o));
    }

    @Override
    public ResIterator listResourcesWithProperty(Property p, Object o) {
        return new MapFilterResIterator<>(this::wrapResource, getReferee().listResourcesWithProperty(p, o));
    }

    @Override
    public ResIterator listSubjectsWithProperty(Property p, String o) {
        return new MapFilterResIterator<>(this::wrapResource, getReferee().listSubjectsWithProperty(p, o));
    }

    @Override
    public ResIterator listSubjectsWithProperty(Property p, String o, String l) {
        return new MapFilterResIterator<>(this::wrapResource, getReferee().listSubjectsWithProperty(p, o, l));
    }

    @Override
    public boolean containsLiteral(Resource s, Property p, boolean o) {
        return getReferee().containsLiteral(s, p, o);
    }

    @Override
    public boolean containsLiteral(Resource s, Property p, long o) {
        return getReferee().containsLiteral(s, p, o);
    }

    @Override
    public boolean containsLiteral(Resource s, Property p, int o) {
        return getReferee().containsLiteral(s, p, o);
    }

    @Override
    public boolean containsLiteral(Resource s, Property p, char o) {
        return getReferee().containsLiteral(s, p, o);
    }

    @Override
    public boolean containsLiteral(Resource s, Property p, float o) {
        return getReferee().containsLiteral(s, p, o);
    }

    @Override
    public boolean containsLiteral(Resource s, Property p, double o) {
        return getReferee().containsLiteral(s, p, o);
    }

    @Override
    public boolean containsLiteral(Resource s, Property p, Object o) {
        return getReferee().containsLiteral(s, p, o);
    }

    @Override
    public boolean contains(Resource s, Property p, String o) {
        return getReferee().contains(s, p, o);
    }

    @Override
    public boolean contains(Resource s, Property p, String o, String l) {
        return getReferee().contains(s, p, o, l);
    }

    @Override
    public Statement asStatement(Triple t) {
        return wrapStatement(getReferee().asStatement(t));
    }

    @Override
    public Graph getGraph() {
        return graphReference;
    }

    @Override
    public RDFNode asRDFNode(Node n) {
        return wrapNode(getReferee().asRDFNode(n));
    }

    @Override
    public Resource wrapAsResource(Node n) {
        return wrapResource(getReferee().wrapAsResource(n));
    }

    @Override
    public RDFReader getReader() {
        return getReferee().getReader();
    }

    @Override
    public RDFReader getReader(String lang) {
        return getReferee().getReader(lang);
    }

    @Override
    @Deprecated
    public String setReaderClassName(String lang, String className) {
        return getReferee().setReaderClassName(lang, className);
    }

    @Override
    @Deprecated
    public void resetRDFReaderF() {
        getReferee().resetRDFReaderF();
    }

    @Override
    @Deprecated
    public String removeReader(String lang) throws IllegalArgumentException {
        return getReferee().removeReader(lang);
    }

    @Override
    public RDFWriter getWriter() {
        return getReferee().getWriter();
    }

    @Override
    public RDFWriter getWriter(String lang) {
        return getReferee().getWriter(lang);
    }

    @Override
    @Deprecated
    public String setWriterClassName(String lang, String className) {
        return getReferee().setWriterClassName(lang, className);
    }

    @Override
    @Deprecated
    public void resetRDFWriterF() {
        getReferee().resetRDFWriterF();
    }

    @Override
    @Deprecated
    public String removeWriter(String lang) throws IllegalArgumentException {
        return getReferee().removeWriter(lang);
    }

    @Override
    public PrefixMapping setNsPrefix(String prefix, String uri) {
        return getReferee().setNsPrefix(prefix, uri);
    }

    @Override
    public PrefixMapping removeNsPrefix(String prefix) {
        return getReferee().removeNsPrefix(prefix);
    }

    @Override
    public PrefixMapping setNsPrefixes(PrefixMapping other) {
        return getReferee().setNsPrefixes(other);
    }

    @Override
    public PrefixMapping setNsPrefixes(Map<String, String> map) {
        return getReferee().setNsPrefixes(map);
    }

    @Override
    public PrefixMapping withDefaultMappings(PrefixMapping map) {
        return getReferee().withDefaultMappings(map);
    }

    @Override
    public String getNsPrefixURI(String prefix) {
        return getReferee().getNsPrefixURI(prefix);
    }

    @Override
    public String getNsURIPrefix(String uri) {
        return getReferee().getNsURIPrefix(uri);
    }

    @Override
    public Map<String, String> getNsPrefixMap() {
        return getReferee().getNsPrefixMap();
    }

    @Override
    public String expandPrefix(String prefixed) {
        return getReferee().expandPrefix(prefixed);
    }

    @Override
    public String shortForm(String uri) {
        return getReferee().shortForm(uri);
    }

    @Override
    public String qnameFor(String uri) {
        return getReferee().qnameFor(uri);
    }

    @Override
    public PrefixMapping lock() {
        return getReferee().lock();
    }

    @Override
    public boolean samePrefixMappingAs(PrefixMapping other) {
        return getReferee().samePrefixMappingAs(other);
    }

    @Override
    public void enterCriticalSection(boolean readLockRequested) {
        getReferee().enterCriticalSection(readLockRequested);
    }

    @Override
    public void leaveCriticalSection() {
        getReferee().leaveCriticalSection();
    }

    /* jena 3.1.1 new methods */

    @Override
    public Statement getRequiredProperty(Resource resource, Property property, String s) {
        return getReferee().getRequiredProperty(resource, property, s);
    }

    @Override
    public Statement getProperty(Resource resource, Property property, String s) {
        return getReferee().getProperty(resource, property, s);
    }

    @Override
    public void executeInTxn(Runnable runnable) {
        getReferee().executeInTxn(runnable);
    }

    @Override
    public <T> T calculateInTxn(Supplier<T> supplier) {
        return getReferee().calculateInTxn(supplier);
    }

    @Override
    public StmtIterator listLiteralStatements(Resource resource, Property property, int i) {
        return getReferee().listLiteralStatements(resource, property, i);
    }

    @Override
    public PrefixMapping clearNsPrefixMap() {
        return getReferee().clearNsPrefixMap();
    }

    @Override
    public int numPrefixes() {
        return getReferee().numPrefixes();
    }
}

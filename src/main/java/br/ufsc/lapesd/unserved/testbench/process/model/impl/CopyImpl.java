package br.ufsc.lapesd.unserved.testbench.process.model.impl;

import br.ufsc.lapesd.unserved.testbench.iri.UnservedP;
import br.ufsc.lapesd.unserved.testbench.model.Variable;
import br.ufsc.lapesd.unserved.testbench.process.model.Copy;
import br.ufsc.lapesd.unserved.testbench.util.ImplementationByType;
import org.apache.jena.enhanced.EnhGraph;
import org.apache.jena.enhanced.EnhNode;
import org.apache.jena.enhanced.Implementation;
import org.apache.jena.graph.Node;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.impl.ResourceImpl;

import java.util.Arrays;

public class CopyImpl  extends ResourceImpl implements Copy {
    public static Implementation factory = new ImplementationByType(UnservedP.Copy.asNode(),
            Arrays.asList(UnservedP.copyFrom.asNode(), UnservedP.copyTo.asNode())) {
        @Override
        public EnhNode wrap(Node node, EnhGraph eg) {
            return new CopyImpl(node, eg);
        }
    };

    public CopyImpl(Node n, EnhGraph m) {
        super(n, m);
    }

    @Override
    public Variable getFrom() {
        Statement statement = getProperty(UnservedP.copyFrom);
        return statement.getResource().as(Variable.class);
    }

    @Override
    public Variable getTo() {
        Statement statement = getProperty(UnservedP.copyTo);
        return statement.getResource().as(Variable.class);
    }

    @Override
    public int getFromIndex() {
        Statement statement = getProperty(UnservedP.copyFromIndex);
        return statement == null ? 0 : statement.getObject().asLiteral().getInt();
    }

    @Override
    public int getToIndex() {
        Statement statement = getProperty(UnservedP.copyToIndex);
        return statement == null ? 0 : statement.getObject().asLiteral().getInt();
    }
}

package br.ufsc.lapesd.unserved.testbench.msm;

import br.ufsc.lapesd.unserved.testbench.util.ResourcesBackground;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;
import org.apache.jena.riot.RDFFormat;

import java.util.Collections;

public class MSM extends ResourcesBackground {
    public MSM() {
        super(Collections.singletonList(ImmutablePair.of("msm.ttl", RDFFormat.TURTLE)), true);
    }

    public static String IRI = "http://iserve.kmi.open.ac.uk/ns/msm";
    public static String PREFIX = IRI + "#";

    public static Resource MessageContent = ResourceFactory.createResource(PREFIX + "MessageContent");
    public static Resource MessagePart = ResourceFactory.createResource(PREFIX + "MessagePart");
    public static Resource Operation = ResourceFactory.createResource(PREFIX + "Operation ");
    public static Resource Service = ResourceFactory.createResource(PREFIX + "Service");

    public static Property hasInput = ResourceFactory.createProperty(PREFIX + "hasInput");
    public static Property hasMandatoryPart = ResourceFactory.createProperty(PREFIX + "hasMandatoryPart");
    public static Property hasName = ResourceFactory.createProperty(PREFIX + "hasName");
    public static Property hasOperation = ResourceFactory.createProperty(PREFIX + "hasOperation");
    public static Property hasOptionalPart = ResourceFactory.createProperty(PREFIX + "hasOptionalPart");
    public static Property hasOutput = ResourceFactory.createProperty(PREFIX + "hasOutput");
    public static Property hasFault = ResourceFactory.createProperty(PREFIX + "hasFault");
    public static Property hasInputFault = ResourceFactory.createProperty(PREFIX + "hasInputFault");
    public static Property hasOutputFault = ResourceFactory.createProperty(PREFIX + "hasOutputFault");
    public static Property hasPart = ResourceFactory.createProperty(PREFIX + "hasPart");
    public static Property hasPartTransitive = ResourceFactory.createProperty(PREFIX + "hasPartTransitive");
    public static Property isGroundedIn = ResourceFactory.createProperty(PREFIX + "isGroundedIn");
}

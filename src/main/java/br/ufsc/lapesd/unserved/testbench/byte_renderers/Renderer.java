package br.ufsc.lapesd.unserved.testbench.byte_renderers;

import br.ufsc.lapesd.unserved.testbench.model.Variable;

/**
 * This type of component provides conversion from RDF nodes to unicode strings that
 * represent the same data represented by that node.
 *
 * This formatting may be trivial, such as for xsd:string, simple as is the case of
 * xsd:base64Binary (the base64 is decoded) or complicated for some resources,
 * such as unserved:RDF.
 */
public interface Renderer {
    Render format(Variable variable) throws RendererException;
}

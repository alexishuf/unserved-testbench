package br.ufsc.lapesd.unserved.testbench.benchmarks.wsc08;

import br.ufsc.lapesd.unserved.testbench.io_composer.state.MessagePairAction;
import br.ufsc.lapesd.unserved.testbench.iri.Unserved;
import br.ufsc.lapesd.unserved.testbench.model.Message;
import br.ufsc.lapesd.unserved.testbench.model.Variable;
import com.google.common.base.Preconditions;
import org.apache.commons.io.FileUtils;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.rdf.model.*;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.vocabulary.OWL;
import org.apache.jena.vocabulary.OWL2;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.*;
import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.google.common.base.Preconditions.checkArgument;
import static java.lang.String.format;
import static java.util.Comparator.comparing;

/**
 * Converts a WSC problem into a Pragmatic Proof problem
 */
public class PragmaticProofConverter {
    private static final Logger logger = LoggerFactory.getLogger(PragmaticProofConverter.class);

    @Option(name = "--help", aliases = {"-h"}, help = true, usage = "Shows help")
    private boolean help = false;

    @Option(name = "--without-http", aliases = {"-H"}, usage = "If given, blank nodes " +
            "describing HTTP request and response message will not be added to " +
            "consequent of services")
    private boolean withoutHttp = false;

    @Option(name = "--omit-body", aliases = {"-B"}, usage = "If given, without --without-http, " +
            "this prevent the generation of an additional output to represent the HTTP reply body",
            forbids = {"--without-http"})
    private boolean omitBody = false;

    @Option(name = "--shadow", aliases = {"-p"}, usage = "Which shadowing strategy to use (e.g., " +
            "generate preconditions, or just shadow IOs)")
    private ShadowType shadowStrategy = ShadowType.NONE;

    @Option(name = "--in", aliases = {"-i"}, usage = "Path to the root of WSC converted problems " +
            "(containing sub dirs 01, 02, ...) or to a particular WSC problem/subdir")
    private File cmdInDir;

    @Option(name = "--out", aliases = {"-o"}, usage = "Path to the destination of converted " +
            "problems. Structure will mirror that of --in")
    private File cmdOutDir;

    public static void main(String[] args)  throws Exception {
        Unserved.init();
        PragmaticProofConverter app = new PragmaticProofConverter();
        CmdLineParser parser = new CmdLineParser(app);
        parser.parseArgument(args);
        if (app.help) parser.printUsage(System.out);
        else app.run();
    }

    private void run() throws IOException {
        Preconditions.checkState(cmdInDir != null);
        checkArgument(cmdInDir.exists());
        checkArgument(cmdInDir.isDirectory());
        Preconditions.checkState(cmdOutDir != null);
        checkArgument(!cmdOutDir.exists() || cmdOutDir.isDirectory());

        File[] subDirs = cmdInDir.listFiles(File::isDirectory);
        if (subDirs == null || subDirs.length == 0) {
            logger.info("Converting {} to {}...", cmdInDir, cmdOutDir);
            try {
                new Converter(cmdInDir, cmdOutDir, withoutHttp, omitBody, shadowStrategy).run();
                logger.info("Converted {} to {}.", cmdInDir, cmdOutDir);
            } catch (Exception e) {
                logger.error("Conversion of {} to {} FAILED.", cmdInDir, cmdOutDir);
                throw e;
            }
        } else {
            List<File> subDirsList = Arrays.stream(subDirs).sorted(comparing(File::getName))
                    .collect(Collectors.toList());
            for (File subDir : subDirsList) {
                File outSubDir = new File(cmdOutDir, subDir.getName());
                if (!outSubDir.exists() && !outSubDir.mkdirs()) {
                    logger.error("Could not mkdirs {}", outSubDir);
                    throw new IOException("Could not mkdirs " + outSubDir.getAbsoluteFile());
                }
                try {
                    new Converter(subDir, outSubDir, withoutHttp, omitBody, shadowStrategy).run();
                    logger.info("Converted {} to {}.", subDir, outSubDir);
                } catch (Exception e) {
                    logger.error("Conversion of {} to {} FAILED.", subDir, outSubDir);
                    throw e;
                }
            }
        }
    }

    public void run(@Nonnull File wscDir, @Nonnull File outDir) throws IOException {
        this.cmdInDir = wscDir;
        this.cmdOutDir = outDir;
        run();
    }

    enum ShadowType {
        NONE,
        DUP_IOS,
        SINGLE_CONJUNCTION,
        SHADOW_CONJUNCTION;
    }

    private static class Converter {
        static final String wscURI = "http://www.ws-challenge.org/WSC08Services/#";
        @Nonnull final File inDir;
        @Nonnull final File outDir;
        final boolean withoutHttp;
        final boolean omitBody;
        @Nonnull final ShadowType shadowType;
        @Nonnull final String taxURI;
        @Nonnull private final String problem;

        private long id = 1;

        public Converter(@Nonnull File inDir, @Nonnull File outDir, boolean withoutHttp,
                         boolean omitBody, @Nonnull ShadowType shadowType) {
            this.withoutHttp = withoutHttp;
            this.omitBody = omitBody;
            this.shadowType = shadowType;
            checkArgument(inDir.exists() && inDir.isDirectory());
            checkArgument(outDir.exists() && outDir.isDirectory());
            this.inDir = inDir;
            this.outDir = outDir;
            this.problem = inDir.getName();
            checkArgument(problem.matches("^[0-9]+$"),
                    "Expected problem name " + inDir.getName() + "to be an integer");
            this.taxURI = format("http://localhost/wsc/%s/taxonomy.owl#", inDir.getName());
        }

        public void run() throws IOException {
            File inTaxonomy = new File(inDir, "taxonomy.ttl");
            File inServices = new File(inDir, "services.ttl");
            File inKnown = new File(inDir, "known.ttl");
            File inWanted = new File(inDir, "wanted.ttl");

            checkArgument(inTaxonomy.exists() && inTaxonomy.isFile());
            checkArgument(inServices.exists() && inServices.isFile());
            checkArgument(inKnown.exists() && inKnown.isFile());
            checkArgument(inWanted.exists() && inWanted.isFile());

            if (shadowType == ShadowType.NONE) {
                FileUtils.copyFile(inTaxonomy, new File(outDir, "taxonomy.ttl"));
            } else {
                shadowTaxonomy(inTaxonomy, new File(outDir, "taxonomy.ttl"));
            }

            convertServices(inServices, new File(outDir, "descriptions.n3"));
            convertKnown(inKnown, new File(outDir, "initial.n3"));
            convertWanted(inWanted, new File(outDir, "goal.n3"));
            writeInfRules(new File(outDir, "inf.n3"));
        }

        private void shadowTaxonomy(@Nonnull File inFile,
                                    @Nonnull File outFile) throws IOException {
            checkArgument(inFile.exists() && inFile.canRead());
            checkArgument(!outFile.exists() || (outFile.isFile() && outFile.canWrite()));

            Model m = ModelFactory.createDefaultModel();
            try (FileInputStream in = new FileInputStream(inFile)) {
                RDFDataMgr.read(m, in, Lang.TURTLE);
            }
            try (PrintStream out = new PrintStream(new FileOutputStream(outFile))) {
                out.println("@prefix owl: <" + OWL.NS + ">.");
                out.println("@prefix rdfs: <" + RDFS.getURI() + ">.");
                out.println(getPrefixesPreamble());

                ResIterator typeIt = m.listSubjectsWithProperty(RDF.type);
                while (typeIt.hasNext()) {
                    Resource type = typeIt.next();
                    String localName = type.getLocalName();
                    Consumer<String> gen = tax -> {
                        out.printf("%1$s%2$s a owl:Class", tax, localName);
                        String str = getSuperClassesString(tax, type);
                        if (str == null) out.println(".");
                        else out.printf(";\n    rdfs:subClassOf %s.\n", str);

                    };
                    gen.accept("tax:");
                    gen.accept("tax-1:");

                    if (shadowType == ShadowType.SHADOW_CONJUNCTION) {
                        out.printf("ex:pred-%s a owl:ObjectProperty", localName);
                        String list = getSuperClassesString("ex:pred-", type);
                        if (list == null) out.println(".");
                        else out.printf(";\n    rdfs:subPropertyOf %s.\n", list);
                    }
                }
            }
        }

        private static  @Nullable String getSuperClassesString(@Nonnull String prefix,
                                                               @Nonnull Resource aClass) {
            return aClass.getModel().listObjectsOfProperty(aClass, RDFS.subClassOf).toList()
                    .stream()
                    .filter(RDFNode::isResource)
                    .map(o -> o.asResource().getLocalName())
                    .map(lName -> prefix + lName)
                    .reduce((l, r) -> l + ", " + r)
                    .orElse(null);
        }

        private void writeInfRules(@Nonnull File file) throws IOException {
            try (PrintStream out = new PrintStream(new FileOutputStream(file))) {
                out.print("@prefix rdfs: <" + RDFS.getURI() + ">.\n" +
                        "@prefix owl: <" + OWL2.NS + ">.\n" +
                        "\n" +
                        "{?x rdfs:subClassOf ?y. ?z a ?x.} => {?z a ?y.}.\n" +
                        "{?x rdfs:subClassOf ?y. ?y rdfs:subClassOf ?z.} \n" +
                        "    => {?x rdfs:subClassOf ?z.}.\n");
                if (shadowType == ShadowType.SHADOW_CONJUNCTION) {
                    out.print("{?x rdfs:subPropertyOf ?y. ?z a ?x.} => {?z a ?y.}.\n" +
                            "{?x a owl:ObjectProperty.} => {?x rdfs:subPropertyOf ?x.}.\n" +
                            "{?x rdfs:subPropertyOf ?y. ?y rdfs:subPropertyOf ?z.} \n" +
                            "    => {?x rdfs:subPropertyOf ?z.}.\n");
                }
            }
        }

        private void convertKnown(@Nonnull File inFile, @Nonnull File outFile) throws IOException {
            checkArgument(inFile.exists() && inFile.isFile() && inFile.canRead());
            checkArgument(!outFile.exists() || (outFile.isFile() && outFile.canWrite()));

            Model m = ModelFactory.createDefaultModel();
            try (FileInputStream in = new FileInputStream(inFile)) {
                RDFDataMgr.read(m, in, Lang.TURTLE);
            }
            try (PrintStream out = new PrintStream(new FileOutputStream(outFile))) {
                out.println(getPrefixesPreamble());
                m.listSubjectsWithProperty(RDF.type, Unserved.Variable)
                        .forEachRemaining(r -> out.println(getValueTriple(r.as(Variable.class))));
            }
        }

        private void convertWanted(@Nonnull File inFile, @Nonnull File outFile) throws IOException {
            checkArgument(inFile.exists() && inFile.isFile() && inFile.canRead());
            checkArgument(!outFile.exists() || (outFile.isFile() && outFile.canWrite()));

            Model m = ModelFactory.createDefaultModel();
            try (FileInputStream in = new FileInputStream(inFile)) {
                RDFDataMgr.read(m, in, Lang.TURTLE);
            }
            try (PrintStream out = new PrintStream(new FileOutputStream(outFile))) {
                out.println(getPrefixesPreamble());
                StringBuilder b = new StringBuilder();
                m.listSubjectsWithProperty(RDF.type, Unserved.Variable).forEachRemaining(r ->
                        b.append(getVarTriple("w", r.as(Variable.class))).append('\n'));
                out.printf("{\n%s\n} => {\n%s\n}.\n", b.toString(), b.toString());
            }
        }

        private void convertServices(@Nonnull File inFile, @Nonnull File outFile) throws IOException {
            checkArgument(inFile.exists() && inFile.isFile() && inFile.canRead());
            checkArgument(!outFile.exists() || (outFile.isFile() && outFile.canWrite()));

            Model m = ModelFactory.createDefaultModel();
            try (FileInputStream in = new FileInputStream(inFile)) {
                RDFDataMgr.read(m, in, Lang.TURTLE);
            }
            try (PrintStream out = new PrintStream(new FileOutputStream(outFile))) {
                out.println(getPrefixesPreamble());
                List<MessagePairAction> list = new ArrayList<>();
                try (QueryExecution ex = QueryExecutionFactory.create(QueryFactory.create(
                        "PREFIX u: <" + Unserved.PREFIX + ">\n" +
                                "SELECT ?rep ?req WHERE {?rep u:when/u:reactionTo ?req.}"), m)) {
                    ex.execSelect().forEachRemaining(s -> list.add(new MessagePairAction(
                            s.getResource("req").as(Message.class),
                            s.getResource("rep").as(Message.class))));
                }
                for (MessagePairAction p : list) {
                    out.println("{\n");

                    List<Long> inIds = new ArrayList<>(), outIds = new ArrayList<>();
                    assert p.getAntecedent().getVariables().size()
                            == p.getAntecedent().getVariablesRecursive().size();
                    out.println(getVarTriples("i", p.getAntecedent().getVariables(), inIds));
                    assert !inIds.isEmpty();
                    assert  inIds.size() == p.getAntecedent().getVariables().size();

                    out.println("} => {\n");

                    List<Variable> l = p.getConsequent().getVariables();
                    assert l.size() == 1;
                    long bodyId = omitBody ? -1 : id++;
                    String outTriples = getVarTriples("o", l.get(0).getVariables(), outIds);
                    assert !outIds.isEmpty() && outIds.size() == l.get(0).getVariables().size();
                    if (omitBody) bodyId = outIds.get(0);
                    if (!withoutHttp) {
                        out.printf("_:request http:methodName \"GET\";\n" +
                                      "  http:requestURI ?i%d;\n" +
                                      "  http:resp [ http:body ?o%d ].\n", inIds.get(0), bodyId);
                        if (!omitBody) {
                            for (Long outId : outIds) {
                                if (shadowType == ShadowType.DUP_IOS || shadowType == ShadowType.SINGLE_CONJUNCTION) {
                                    out.printf("?o%d wsc:hasOutputMember ?o%d.\n", bodyId, outId);
                                    out.printf("?o%d wsc:hasOutputMember ?o1-%d.\n", bodyId, outId);
                                } else if (shadowType == ShadowType.NONE) {
                                    out.printf("?o%d wsc:hasOutputMember ?o%d.\n", bodyId, outId);
                                } else {
                                    throw new UnsupportedOperationException();
                                }
                            }
                        }
                    }
                    out.println(outTriples);

                    out.println("}.\n");
                }
            }
        }

        @SuppressWarnings("SameParameterValue")
        @Nonnull String getVarTriple(@Nonnull String prefix, @Nonnull Variable var) {
            return getVarTriple(prefix, var, null);
        }

        @Nonnull String getVarTriple(@Nonnull String prefix, @Nonnull Variable var,
                                     @Nullable List<Long> ids) {
            Resource type = var.getType();
            assert Objects.equals(type.getNameSpace(), taxURI);
            long assigned = id++;
            if (ids != null) ids.add(assigned);
            if (shadowType == ShadowType.NONE) {
                return format("?%s%d a tax:%s.", prefix, assigned, type.getLocalName());
            } else if (shadowType == ShadowType.DUP_IOS) {
                return format("?%1$s%2$d a tax:%3$s.\n" +
                                     "?%1$s1-%2$d a tax-1:%3$s.",
                        prefix, assigned, type.getLocalName());
            } else if (shadowType == ShadowType.SINGLE_CONJUNCTION) {
                return format("?%1$s%2$d a tax:%3$s.\n" +
                                     "?%1$s1-%2$d a tax-1:%3$s.\n" +
                                     "?%1$s%2$d ex:predicate ?%1$s1-%2$d.",
                        prefix, assigned, type.getLocalName());
            } else if (shadowType == ShadowType.SHADOW_CONJUNCTION) {
                return format("?%1$s%2$d a tax:%3$s.\n" +
                                "?%1$s1-%2$d a tax-1:%3$s.\n" +
                                "?%1$s%2$d ?%1$sp-%2$d ?%1$s1-%2$d.\n" +
                                "?%1$sp-%2$d rdfs:subPropertyOf ex:pred-%3$s.\n",
                        prefix, assigned, type.getLocalName());
            } else {
                throw new UnsupportedOperationException();
            }
        }

        @Nonnull String getVarTriples(@Nonnull String prefix,
                                      @Nonnull Collection<Variable> vars,
                                      @Nullable List<Long> ids) {
            StringBuilder b = new StringBuilder();
            vars.forEach(v -> b.append(getVarTriple(prefix, v, ids)).append('\n'));
            return b.toString();
        }

        @Nonnull String getValueTriple(@Nonnull Variable var) {
            Resource type = var.getType();
            assert Objects.equals(type.getNameSpace(), wscURI);
            assert var.isURIResource();
            assert Objects.equals(var.getNameSpace(), wscURI);
            if (shadowType == ShadowType.NONE) {
                return format("wsc:%s a tax:%s.", var.getLocalName(), type.getLocalName());
            } else if (shadowType == ShadowType.DUP_IOS) {
                return format("wsc:%1$s a tax:%2$s.\n" +
                                     "wsc-1:%1$s a tax-1:%2$s.",
                        var.getLocalName(), type.getLocalName());
            } else if (shadowType == ShadowType.SINGLE_CONJUNCTION) {
                return format("wsc:%1$s a tax:%2$s.\n" +
                                     "wsc:%1$s ex:predicate wsc-1:%1$s.\n" +
                                     "wsc-1:%1$s a tax-1:%2$s.\n",
                        var.getLocalName(), type.getLocalName());
            } else if (shadowType == ShadowType.SHADOW_CONJUNCTION) {
                return format("wsc:%1$s a tax:%2$s.\n" +
                                "wsc:%1$s ex:pred-%2$s wsc-1:%1$s.\n" +
                                "wsc-1:%1$s a tax-1:%2$s.\n",
                        var.getLocalName(), type.getLocalName());
            } else {
                throw new UnsupportedOperationException();
            }

        }

        @Nonnull String getPrefixesPreamble() {
            List<String> lines = Stream.of("@prefix wsc: <" + wscURI + ">.",
                    "@prefix tax: <" + taxURI + ">.",
                    "@prefix http: <http://www.w3.org/2011/http#>.").collect(Collectors.toList());
            if (shadowType != ShadowType.NONE) {
                String tax1URI = taxURI.replaceAll("taxonomy\\.owl", "taxonomy-1.owl");
                lines.add("@prefix tax-1: <" + tax1URI + ">.");
                lines.add("@prefix wsc-1: <" + wscURI.replaceAll("#$", "1#") + ">.");
                if (shadowType == ShadowType.SINGLE_CONJUNCTION
                        || shadowType == ShadowType.SHADOW_CONJUNCTION) {
                    lines.add("@prefix ex: <http://example.org/ns#>.");
                }
                if (shadowType == ShadowType.SHADOW_CONJUNCTION) {
                    lines.add("@prefix rdfs: <" + RDFS.getURI() + ">.");
                }
            }
            return lines.stream().reduce((l, r) -> l + "\n" + r).orElse("");
        }

    }
}

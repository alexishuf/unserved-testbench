package br.ufsc.lapesd.unserved.testbench.benchmarks.design1.data;

import com.google.common.base.Preconditions;
import es.usc.citius.hipster.algorithm.AStar;
import es.usc.citius.hipster.algorithm.Hipster;
import es.usc.citius.hipster.model.Transition;
import es.usc.citius.hipster.model.impl.WeightedNode;
import es.usc.citius.hipster.model.problem.ProblemBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ServiceGraph implements Serializable {
    private static Logger logger = LoggerFactory.getLogger(ServiceGraph.class);

    private List<Service> services = new ArrayList<>();
    private HashMap<Service, Set<IncomingConnector>> incomingConnectors = new HashMap<>();
    private HashMap<Service, Set<OutgoingConnector>> outgoingConnectors = new HashMap<>();
    private List<List<Service>> parallelSequences = new ArrayList<>();
    private Service first, last;

    private ServiceGraph() {
    }
    protected ServiceGraph(ServiceGraph other) {
        other.getServices().stream().map(Service::getId).map(Service::new).sorted()
                .forEachOrdered(services::add);
        parallelSequences = other.getParallelSequences().stream()
                .map(seq -> seq.stream().map(this::convert).collect(Collectors.toList()))
                .collect(Collectors.toList());
        first = new Service(other.getFirst().getId());
        last = new Service(other.getLast().getId());
        other.getEdges().forEach(pair ->
                connection(pair.getLeft().stream().map(this::convert))
                        .to(pair.getRight().stream().map(this::convert))
                        .add());
    }

    private Service convert(Service other) {
        return new Service(other.getId());
    }

    public List<List<Service>> getParallelSequences() {
        return parallelSequences;
    }

    public Service getFirst() {
        return first;
    }

    public Service getLast() {
        return last;
    }

    public Collection<Service> getServices() {
        return services;
    }

    public Collection<ImmutablePair<Set<Service>, Set<Service>>> getEdges() {
        return outgoingConnectors.values().stream().flatMap(Set::stream).distinct()
                .map(outConn -> ImmutablePair.<Set<Service>, Set<Service>>
                        of(outConn, outConn.getNext()))
                .collect(Collectors.toList());
    }

    public static ServiceGraph random(ServiceGraphFactors factors, int maxLength, int maxParallel) {
        ServiceGraph graph = new ServiceGraph();
        graph.initRandom(factors, maxLength, maxParallel);
        return graph;
    }

    private void initRandom(ServiceGraphFactors factors, int maxLength, int maxParallel) {
        Preconditions.checkArgument(factors.getInputSetsSize() > 0
                || factors.getInputSetsCount() == 0);

        initServicesOfInterest(maxLength, maxParallel);
        for (int i = services.size(); i < factors.getServiceCount(); i++)
            createNextService();

        LinkedList<Service> queue = new LinkedList<>(services);
        LinkedList<Service> candidates = new LinkedList<>(services);
        queue.removeAll(getFirstsInSequences()); //no incoming edges into the firsts of a sequence
        candidates.remove(first); //no outgoing edge from first

        Collections.shuffle(queue);
        Collections.shuffle(candidates);
        int setsTried = 0, setsFailed = 0;
        while (!queue.isEmpty()) {
            Service service = queue.remove();
            for (int i = 0; i < factors.getInputSetsCount(); i++) {
                ++setsTried;
                if (!initRandomConnectSet(service, candidates, factors.getInputSetsSize()))
                    ++setsFailed;
            }
        }
        if (setsFailed> 0)
            logger.info("Failed to create {} sets out of {}.", setsFailed, setsTried);
        else
            logger.info("Created {} edges", setsTried);
    }

    private boolean initRandomConnectSet(Service service, LinkedList<Service> candidates,
                                         int setSize) {
        List<Service> failed = new ArrayList<>(candidates.size());
        List<Service> set = new ArrayList<>(setSize);
        try {
            while (true) {
                set.clear();
                for (int i = 0; !candidates.isEmpty() && i < setSize; i++)
                    set.add(candidates.remove());
                if (set.isEmpty())
                    return false;
                Connection connection = connection(set).to(service).add();
                boolean solutionOk = checkSolution();
                boolean assignable = solutionOk && /* avoid expensive computation */
                        ServicesIOs.canAssignRandomIOs(service.getIncoming().stream()
                        .map(IncomingConnector::getPrev).collect(Collectors.toSet()), setSize);
                if (solutionOk && !assignable) {
                    assignable = ServicesIOs.canAssignRandomIOs(service.getIncoming().stream()
                            .map(IncomingConnector::getPrev).collect(Collectors.toSet()), setSize);
                }
                if (solutionOk && assignable) {
                    return true;
                } else {
                    connection.remove();
                    failed.addAll(set);
                }
            }
        } finally {
            candidates.addAll(failed);
            candidates.addAll(set);
        }
    }

    private boolean checkSolution() {
        AStar<Void, Set<Service>, Double, WeightedNode<Void, Set<Service>, Double>> dijkstra;
        Set<Service> firsts = getFirstsInSequences();
        dijkstra = Hipster.createDijkstra(ProblemBuilder.create()
                .initialState(firsts).defineProblemWithoutActions()
                .useTransitionFunction(this::checkSolutionTransition)
                .useCostFunction(t -> 1.0).build());
        Set<Service> invoked; //set of services used from first to last.
        invoked = dijkstra.search(this::checkSolutionGoalPredicate).getGoalNode().state();
        /* the path was optimal. if all services in parallelSequences were used, then the
         * optimal path remains the originally intended. */
        List<Service> servicesOfInterest = parallelSequences.stream().flatMap(List::stream)
                .collect(Collectors.toList());
        servicesOfInterest.add(last);
        return invoked.containsAll(servicesOfInterest);
    }

    private Set<Service> getFirstsInSequences() {
        return first.getOutgoing().stream().map(OutgoingConnector::getNext)
                    .flatMap(Set::stream).collect(Collectors.toSet());
    }

    private boolean checkSolutionGoalPredicate(WeightedNode<Void, Set<Service>, Double> node) {
        return node.state().contains(last);
    }

    private Iterable<Transition<Void, Set<Service>>> checkSolutionTransition(Set<Service> svcSet) {
        return svcSet.stream().map(s -> outgoingConnectors.getOrDefault(s, Collections.emptySet()))
                .flatMap(Set::stream) // overall set of out-connectors
                .filter(svcSet::containsAll) //only out-connectors that can be satisfied
                .map(OutgoingConnector::getNext) //out-conn enables invoking the in-connector
                .filter(inConn -> !svcSet.containsAll(inConn)) //only in-conn with new services
                .map(inConn -> {
                    Set<Service> set = new HashSet<>(svcSet); //retain the current invoked services
                    set.addAll(inConn); //add the newly invokable services
                    return set;
                }).map(set -> Transition.create(svcSet, set)).collect(Collectors.toList());
    }

    private void initServicesOfInterest(int maxLength, int maxParallel) {
        parallelSequences = new ArrayList<>();
        first = createNextService();
        Set<Service> firsts = new HashSet<>(), lasts = new HashSet<>();
        for (int i = 0; i < maxParallel; i++) {
            List<Service> sequence = new ArrayList<>();
            Service previous = createNextService();
            sequence.add(previous);
            firsts.add(previous);
            for (int j = 1; j < maxLength; j++) {
                Service next = createNextService();
                connection(previous).to(next).add();
                previous = next;
                sequence.add(previous);
            }
            lasts.add(previous);
            parallelSequences.add(sequence);
        }
        last = createNextService();
        firsts.forEach(service -> connection(first).to(service).add());
        connection(lasts).to(last).add();
    }

    protected Service createNextService() {
        Service s = new Service(services.size());
        services.add(s);
        return s;
    }

    protected ConnectHelper connection(Service... services) {
        return connection(Arrays.asList(services));
    }
    protected ConnectHelper connection(Collection<Service> services) {
        return connection(services.stream());
    }
    protected ConnectHelper connection(Stream<Service> services) {
        return new ConnectHelper(services);
    }

    private static  <T>
    void addConnector(Map<Service, Set<T>> map, Set<Service> services, T connector) {
        services.forEach(service -> {
            Set<T> set = map.getOrDefault(service, null);
            if (set == null) {
                set = new HashSet<>();
                map.put(service, set);
            }
            set.add(connector);
        });
    }

    private static <T>
    void removeConnector(Map<Service, Set<T>> map, Set<Service> services, T connector) {
        services.forEach(service -> {
            Set<T> set = map.getOrDefault(service, null);
            if (set != null) {
                set.remove(connector);
                if (set.isEmpty())
                    map.remove(service, set);
            }
        });
    }


    protected class ConnectHelper {
        private final Collection<Service> initial;

        private ConnectHelper(Stream<Service> services) {
            this.initial = services.collect(Collectors.toSet());
        }

        public Connection to(Service... services) {
            return to(Arrays.asList(services));
        }
        public Connection to(Collection<Service> services) {
            return to(services.stream());
        }
        public Connection to(Stream<Service> services) {
            return new Connection(new OutgoingConnector(initial),
                    new IncomingConnector(services.collect(Collectors.toSet())));
        }
    }

    protected class Connection {
        private OutgoingConnector out;
        private IncomingConnector in;

        public Connection(OutgoingConnector out, IncomingConnector in) {
            out.setNext(in.setPrev(out));
            this.out = out;
            this.in = in;
        }

        public Connection add() {
            addConnector(outgoingConnectors, out, out);
            addConnector(incomingConnectors, in, in);
            return this;
        }

        public Connection remove() {
            removeConnector(outgoingConnectors, out, out);
            removeConnector(incomingConnectors, in, in);
            return this;
        }
    }

    public class Service implements Serializable, Comparable<Service> {
        private final int id;
        public Service(int id) {
            this.id = id;
        }

        public int getId() {
            return id;
        }

        public ServiceGraph getServiceGraph() {
            return ServiceGraph.this;
        }

        protected Set<IncomingConnector> getIncoming() {
            return incomingConnectors.getOrDefault(this, Collections.emptySet());
        }
        protected Set<OutgoingConnector> getOutgoing() {
            return outgoingConnectors.getOrDefault(this, Collections.emptySet());
        }

        @Override
        public String toString() {
            return String.format("Service(%d)", getId());
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null) return false;
            if (!(o instanceof Service)) return false;
            Service rhs = (Service) o;
            return getServiceGraph() == rhs.getServiceGraph() &&  getId() == rhs.getId();
        }

        @Override
        public int hashCode() {
            return new HashCodeBuilder().append(getServiceGraph()).append(getId()).hashCode();
        }

        @Override
        public int compareTo(@Nonnull Service rhs) {
            return Integer.compare(getId(), rhs.getId());
        }
    }

    protected static class IncomingConnector extends HashSet<Service> implements Serializable {
        private OutgoingConnector prev = null;
        public IncomingConnector(Collection<? extends Service> collection) {
            super(collection);
        }
        private IncomingConnector setPrev(OutgoingConnector prev) {
            this.prev = prev;
            return this;
        }
        public OutgoingConnector getPrev() {
            return prev;
        }

        @Override
        public boolean equals(Object o) {
            return this == o;
        }

        @Override
        public int hashCode() {
            return System.identityHashCode(this);
        }
    }

    protected static class OutgoingConnector extends HashSet<Service> implements Serializable {
        private IncomingConnector next = null;
        public OutgoingConnector(Collection<? extends Service> collection) {
            super(collection);
        }
        private OutgoingConnector setNext(IncomingConnector next) {
            this.next = next;
            return this;
        }
        public IncomingConnector getNext() {
            return next;
        }

        @Override
        public boolean equals(Object o) {
            return this == o;
        }

        @Override
        public int hashCode() {
            return System.identityHashCode(this);
        }
    }
}

package br.ufsc.lapesd.unserved.testbench.httpcomponents.extractors;

import br.ufsc.lapesd.unserved.testbench.model.Part;
import br.ufsc.lapesd.unserved.testbench.model.http.AsHttpProperty;
import com.google.common.base.Preconditions;
import com.google.common.reflect.ClassPath;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class DefaultHttpResponseExtractorRepository implements HttpResponseExtractorRepository {
    public Map<Property, HttpResponseExtractor> extractors = new HashMap<>();

    public DefaultHttpResponseExtractorRepository(Map<Property, HttpResponseExtractor> extractors) {
        this.extractors = extractors;
    }

    private DefaultHttpResponseExtractorRepository() {
        this(scanDefaultExtractors());
    }

    private static Map<Property, HttpResponseExtractor> scanDefaultExtractors() {
        Map<Property, HttpResponseExtractor> map = new HashMap<>();
        try {
            @SuppressWarnings("unchecked") List<? extends Class<? extends HttpResponseExtractor>> classes
                    = ClassPath.from(DefaultHttpResponseExtractorRepository.class.getClassLoader())
                    .getTopLevelClasses("br.ufsc.lapesd.unserved.testbench.httpcomponents.extractors")
                    .stream().map(ClassPath.ClassInfo::load)
                    .filter(HttpResponseExtractor.class::isAssignableFrom)
                    .map(c -> (Class<? extends HttpResponseExtractor>)c)
                    .collect(Collectors.toList());
            for (Class<? extends HttpResponseExtractor> clazz : classes) {
                Constructor<? extends HttpResponseExtractor> constructor = null;
                try {
                    ExtractsProperties annotation = clazz.getAnnotation(ExtractsProperties.class);
                    if (annotation == null) continue;
                    constructor = clazz.getConstructor();
                    HttpResponseExtractor extractor = constructor.newInstance();
                    Arrays.stream(annotation.value()).map(ResourceFactory::createProperty)
                            .forEach(p -> map.put(p, extractor));
                } catch (ReflectiveOperationException ignored) { }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return map;
    }

    private static final DefaultHttpResponseExtractorRepository instance
            = new DefaultHttpResponseExtractorRepository();
    public static DefaultHttpResponseExtractorRepository getInstance() {
        return instance;
    }

    @Override
    public @Nonnull HttpResponseExtractor getExtractor(Part part)
            throws NoExtractorException, IncompletePartException {
        Resource partModifier = part.getPartModifier();
        if (partModifier == null) throw new IncompletePartException();
        Preconditions.checkArgument(partModifier.canAs(AsHttpProperty.class));
        AsHttpProperty asHttpProp = partModifier.as(AsHttpProperty.class);
        Property property = asHttpProp.getHttpProperty();
        if (property == null) throw new IncompletePartException();

        HttpResponseExtractor extractor = extractors.getOrDefault(property, null);
        if (extractor == null) throw new NoExtractorException();
        return extractor;
    }
}

package br.ufsc.lapesd.unserved.testbench.benchmarks.design1.data;

import br.ufsc.lapesd.unserved.testbench.benchmarks.design1.D1Factors;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.util.Objects;

public class ServiceAltGraphFactors {
    private ServiceGraphFactors graphFactors;
    private int alternatives;

    public ServiceAltGraphFactors(D1Factors factors) {
        this.graphFactors = new ServiceGraphFactors(factors);
        this.alternatives = factors.getSolutionServiceAlternatives();
    }

    public ServiceGraphFactors getGraphFactors() {
        return graphFactors;
    }

    public int getAlternatives() {
        return alternatives;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) return true;
        if (o == null) return false;
        if (!(o instanceof ServiceAltGraphFactors)) return false;
        ServiceAltGraphFactors rhs = (ServiceAltGraphFactors) o;
        return Objects.equals(getGraphFactors(), rhs.getGraphFactors())
                && getAlternatives() == rhs.getAlternatives();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getGraphFactors()).append(getAlternatives()).hashCode();
    }

    @Override
    public String toString() {
        return String.format("ServiceAltGraphFactors(%d, %s)", getAlternatives(),
                getGraphFactors());
    }
}

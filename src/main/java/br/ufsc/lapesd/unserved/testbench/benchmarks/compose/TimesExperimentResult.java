package br.ufsc.lapesd.unserved.testbench.benchmarks.compose;


import br.ufsc.lapesd.unserved.testbench.benchmarks.experiment.ExperimentResult;

import java.util.*;

public class TimesExperimentResult implements ExperimentResult {
    private double initialization;
    private double output;
    private double composition;
    private ArrayList<Double> compositionTimes = new ArrayList<>();
    private double blacklisting;
    private ArrayList<Double> blacklistingTimes = new ArrayList<>();
    private ArrayList<Double> reasonerSpawnTimes = new ArrayList<>();
    private double execution = -1;
    private double total;
    private Map<String, Double> totals = new HashMap<>();
    private Map<String, List<Double>> times = new HashMap<>();
    private Map<String, List<Long>> mems = new HashMap<>();

    @Override
    public String toString() {
        return String.format("{init: %.3f, output: %.3f, composition: %.3f,%s total: %.3f}",
                initialization, output, composition,
                execution >= 0 ? String.format("execution: %.3f,", execution) : "",
                total);
    }


    public double getInitialization() {
        return initialization;
    }
    public double getOutput() {
        return output;
    }
    public double getComposition() {
        return composition;
    }
    public List<Double> getCompositionTimes() {
        return Collections.unmodifiableList(compositionTimes);
    }
    public double getBlacklisting() {
        return blacklisting;
    }
    public List<Double> getBlacklistingTimes() {
        return Collections.unmodifiableList(blacklistingTimes);
    }
    public List<Double> getReasonerSpawnTimes() {
        return Collections.unmodifiableList(reasonerSpawnTimes);
    }
    public double getTotal(String name) {
        if (!totals.containsKey(name)) throw new NoSuchElementException(name);
        return totals.get(name);
    }
    public double getTotal(String name, double fallback) {
        if (!totals.containsKey(name)) return fallback;
        return totals.get(name);
    }
    public double getTotalOrElse(String name, double value) {
        if (!totals.containsKey(name)) return value;
        return totals.get(name);
    }
    public List<Double> getTimes(String name) {
        if (!times.containsKey(name)) throw new NoSuchElementException(name);
        return times.get(name);
    }
    public List<Double> getTimesOrEmpty(String name) {
        return times.getOrDefault(name, Collections.emptyList());
    }
    public Set<String> getTimesNames() {
        return times.keySet();
    }

    public long getMaxMem(String name) {
        return mems.getOrDefault(name, Collections.emptyList()).stream().max(Long::compareTo)
                .orElse(0L);
    }
    public List<Long> getMems(String name) {
        if (!mems.containsKey(name)) throw new NoSuchElementException(name);
        return mems.get(name);
    }
    public List<Long> getMemsOrEmpty(String name) {
        return mems.getOrDefault(name, Collections.emptyList());
    }
    public Set<String> getMemNames() {
        return mems.keySet();
    }

    public double getExecution() {
        return execution;
    }

    public double getTotal() {
        return total;
    }

    public void setInitialization(double initialization) {
        this.initialization = initialization;
    }

    public void setOutput(double output) {
        this.output = output;
    }

    public void setComposition(double composition) {
        this.composition = composition;
    }

    public void addCompositionTime(double time) {
        this.compositionTimes.add(time);
    }

    public void setBlacklisting(double blacklisting) {
        this.blacklisting = blacklisting;
    }

    public void addBlacklistingTime(double time) {
        this.blacklistingTimes.add(time);
    }

    public void addReasonerSpawnTime(double time) {
        this.reasonerSpawnTimes.add(time);
    }

    public void setExecution(double execution) {
        this.execution = execution;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public void addTime(String name, double time) {
        times.computeIfAbsent(name, k -> new ArrayList<>()).add(time);
        totals.put(name, totals.getOrDefault(name, 0.0) + time);
    }

    public void addMem(String name, long bytes) {
        this.mems.computeIfAbsent(name, k -> new ArrayList<>()).add(bytes);
    }
}

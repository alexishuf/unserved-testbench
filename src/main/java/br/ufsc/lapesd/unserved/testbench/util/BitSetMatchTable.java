package br.ufsc.lapesd.unserved.testbench.util;

import com.google.common.base.Preconditions;
import org.apache.jena.rdf.model.Resource;

import javax.annotation.Nonnull;
import java.util.*;

@SuppressWarnings("RedundantConditionalExpression")
public class BitSetMatchTable implements MatchTable {
    private HashMap<Resource, Integer> index;
    private BitSet data;

    public BitSetMatchTable(HashMap<Resource, Integer> index) {
        this.index = index;
        int s = index.size();
        data = new BitSet((s-1)*s/2);
    }

    @Override
    @Nonnull
    public Relation get(@Nonnull Resource left, @Nonnull Resource right) {
        int leftIdx = index.getOrDefault(left, -1);
        int rightIdx = index.getOrDefault(right, -1);

        if (leftIdx < 0 || rightIdx < 0) return Relation.UNKNOWN;
        else if (leftIdx == rightIdx) return Relation.SAME;
        else return read(leftIdx, rightIdx);
    }

    @Override
    public void set(@Nonnull Resource left, @Nonnull Resource right, @Nonnull Relation relation) {
        if (left.equals(right))
            Preconditions.checkArgument(relation == Relation.SAME);
        else
            Preconditions.checkArgument(relation != Relation.SAME);
        int leftIdx = index.getOrDefault(left, -1), rightIdx = index.getOrDefault(right, -1);
        Preconditions.checkArgument(leftIdx >= 0);
        Preconditions.checkArgument(rightIdx >= 0);

        write(leftIdx, rightIdx, relation);
    }

    @Override
    public boolean contains(@Nonnull Resource resource) {
        return index.containsKey(resource);
    }

    @Override
    public Iterator<Resource> rightIterator(@Nonnull Resource left, @Nonnull Relation relation) {
        if (!contains(left))
            return Collections.<Resource>emptyList().iterator();
        if (relation == Relation.SAME) return Collections.singleton(left).iterator();

        return new Iterator<Resource>() {
            Iterator<Resource> others = index.keySet().iterator();
            Resource right = null;

            @Override
            public boolean hasNext() {
                if (right != null) return true;

                while (others.hasNext()) {
                    Resource other = others.next();
                    if (other.equals(left)) continue;
                    if (get(left, other) == relation) {
                        right = other;
                        return true;
                    }
                }
                return false;
            }

            @Override
            public Resource next() {
                Preconditions.checkState(hasNext());
                Resource resource = this.right;
                this.right = null;
                return resource;
            }
        };
    }

    @Override
    public Iterator<Resource> closureIterator(@Nonnull Resource left, @Nonnull Relation relation) {
        if (!contains(left)) return Collections.singleton(left).iterator();
        if (relation == Relation.SAME) return Collections.singleton(left).iterator();

        Queue<Resource> queue = new LinkedList<>();
        queue.add(left);
        Set<Resource> visited = new HashSet<>();

        return new Iterator<Resource>() {
            Resource right = null;

            @Override
            public boolean hasNext() {
                if (right != null) return true;
                while (!queue.isEmpty()) {
                    Resource resource = queue.remove();
                    if (!visited.contains(resource)) {
                        right = resource;
                        return true;
                    }
                }
                return false;
            }

            @Override
            public Resource next() {
                Preconditions.checkState(hasNext());
                visited.add(right);
                rightStream(right, relation).forEach(queue::add);

                Resource resource = this.right;
                this.right = null;
                return resource;
            }
        };
    }

    private Relation flipIf(Relation relation, boolean flip) {
        if (flip && relation == Relation.SUPERCLASS) return Relation.SUBCLASS;
        else if (flip && relation == Relation.SUBCLASS) return Relation.SUPERCLASS;
        else return relation;
    }

    private Relation read(int leftIdx, int rightIdx) {
        int row = Math.min(leftIdx, rightIdx);
        int col = Math.max(leftIdx, rightIdx)-row-1;

        // s=5{abcde} aaaabbbccd
        // match(c, e): li=2,ri=4  r=2,c=4-2-1=1
        // i = \sum_{1}^{s-1}{i} - \sum_{1}^{s-row-1}
        // i = (s-1)*s/2 - (s-row-1)*(s-row)/2
        final int s = index.size();
        int msbIdx = ( (s-1)*s - (s-row-1)*(s-row) ) + col*2;
        int value = (data.get(msbIdx) ? 2 : 0) + (data.get(msbIdx+1) ? 1 : 0);

        return flipIf(Relation.values()[value], row == rightIdx);
    }

    private void write(int leftIdx, int rightIdx, Relation relation) {
        int row = Math.min(leftIdx, rightIdx);
        int col = Math.max(leftIdx, rightIdx)-row-1;
        relation = flipIf(relation, row == rightIdx);

        final int s = index.size();
        int msbIdx = ( (s-1)*s - (s-row-1)*(s-row) ) + col*2;
        Relation[] values = Relation.values();
        for (int i = 0; i < values.length; i++) {
            if (values[i] == relation) {
                data.set(msbIdx,   (i & 0x2) != 0 ? true : false);
                data.set(msbIdx+1, (i & 0x1) != 0 ? true : false);
                break;
            }
        }
    }

    public static final class Builder {
        private HashMap<Resource, Integer> index = new HashMap<>();

        public Builder add(Resource resource) {
            index.putIfAbsent(resource, index.size());
            return this;
        }
        public Builder addAll(Collection<Resource> resources) {
            resources.forEach(this::add);
            return this;
        }

        public MatchTable build() {
            return new BitSetMatchTable(index);
        }
    }
}

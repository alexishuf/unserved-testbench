package br.ufsc.lapesd.unserved.testbench.util;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Helper for mapping blank nodes to a named Resource.
 */
public class SkolemizerMap {
    public static String DEFAULT_PREFIX = "https://alexishuf.bitbucket.io/2016/04/unserved/skolem#";

    private HashMap<Resource, Long> map = new HashMap<>();
    private String prefix = DEFAULT_PREFIX;
    private List<Model> models = new ArrayList<>();
    private long nextId = 1;
    private boolean checkSkolem = true;

    public SkolemizerMap() {}
    public SkolemizerMap(SkolemizerMap other) {
        map.putAll(other.map);
        prefix = other.getPrefix();
        models.addAll(other.models);
        nextId = other.nextId;
    }
    public SkolemizerMap(String prefix) {
        this.prefix = prefix;
    }

    /**
     * get() will check in any model added through this method if a generated skolemized
     * Resource already exists.
     *
     * @return The same {@link SkolemizerMap} instance.
     */
    public SkolemizerMap addModel(Model model) {
        models.add(model);
        return this;
    }

    @Nullable
    public Resource resolve(@Nullable Resource resource) {
        if (resource == null || !resource.isAnon()) return resource;
        long id = map.getOrDefault(resource, -1L);
        return id < 0 ? null : ResourceFactory.createResource(prefix + id);
    }

    public Resource get(Resource resource) {
        if (!resource.isAnon()) return resource;
        long id = map.getOrDefault(resource, -1L);
        if (id < 0) {
            id = nextId;
            if (checkSkolem) {
                while (isUsed(id)) ++id;
            }
            nextId = id+1;
        }
        map.put(resource, id);
        return ResourceFactory.createResource(prefix + id);
    }

    private boolean isUsed(long id) {
        if (models.isEmpty()) return false;
        Resource resource = ResourceFactory.createResource(prefix + id);
        return models.stream().filter(m -> m.containsResource(resource)).findAny().isPresent();
    }

    public String getPrefix() {
        return prefix;
    }

    public void setCheckSkolem(boolean checkSkolem) {
        this.checkSkolem = checkSkolem;
    }
}

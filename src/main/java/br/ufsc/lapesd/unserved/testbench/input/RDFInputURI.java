package br.ufsc.lapesd.unserved.testbench.input;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFFormat;

import javax.annotation.Nonnull;
import java.io.IOException;

public class RDFInputURI extends AbstractRDFInput {
    private final String uri;

    public RDFInputURI(String uri, @Nonnull RDFFormat format) {
        super(format);
        this.uri = uri;
    }

    @Override
    public String toURI() throws IOException {
        if (closed) throw new ClosedException();
        return uri;
    }

    @Override
    protected Model loadModel() throws IOException {
        Model model = ModelFactory.createDefaultModel();
        RDFDataMgr.read(model, uri, uri, getFormat().getLang());
        return model;
    }

    @Override
    public String getBaseURI() {
        try {
            return toURI();
        } catch (IOException e) {
            throw new RuntimeException("Unexpected exception", e);
        }
    }

    @Override
    public String toString() {
        return "RDFInputURI(" + getLang().getName()  + "," + uri + ")";
    }
}

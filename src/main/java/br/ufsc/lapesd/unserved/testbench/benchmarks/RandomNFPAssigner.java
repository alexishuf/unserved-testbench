package br.ufsc.lapesd.unserved.testbench.benchmarks;

import br.ufsc.lapesd.unserved.testbench.iri.Unserved;
import br.ufsc.lapesd.unserved.testbench.iri.UnservedN;
import com.google.common.base.Preconditions;
import com.google.gson.Gson;
import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.vocabulary.RDF;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;

public class RandomNFPAssigner {
    private Config cfg;
    private transient Random random;

    public static class Config {
        public @Nullable Long seed;
        public List<Generator> responseTimeGenerators;
        public List<Generator> throughputGenerators;
        public static class Generator {
            public double mean;
            public double sd;
        }
    }

    public RandomNFPAssigner(@Nonnull Config cfg) {
        this.cfg = cfg;
        this.random = cfg.seed == null ? new Random() : new Random(cfg.seed);
    }

    public static @Nonnull RandomNFPAssigner fromFile(@Nonnull File file) throws IOException {
        try (FileReader reader = new FileReader(file)) {
            return new RandomNFPAssigner(new Gson().fromJson(reader, Config.class));
        }
    }

    public @Nonnull Config getConfig() {
        return cfg;
    }

    public void appendTo(@Nonnull Resource resource) {
        Preconditions.checkArgument(resource.getModel() != null);
        Config.Generator rtGen = getResponseTimeGenerator();
        Config.Generator tpGen = getThroughputGenerator();

        Set<Resource> types = resource.listProperties(Unserved.nfp).toList().stream()
                .flatMap(p -> p.getResource().listProperties(RDF.type).toList().stream())
                .map(Statement::getResource).collect(Collectors.toSet());
        if (!types.contains(UnservedN.ResponseTime) && rtGen != null) {
            resource.addProperty(Unserved.nfp, resource.getModel().createResource()
                    .addProperty(RDF.type, UnservedN.ResponseTime)
                    .addProperty(UnservedN.value, getLiteral(rtGen)));
        }
        if (!types.contains(UnservedN.Throughput) && tpGen != null) {
            resource.addProperty(Unserved.nfp, resource.getModel().createResource()
                    .addProperty(RDF.type, UnservedN.Throughput)
                    .addProperty(UnservedN.value, getLiteral(tpGen)));
        }
    }

    private Config.Generator getResponseTimeGenerator() {
        if (cfg.responseTimeGenerators == null || cfg.responseTimeGenerators.isEmpty()) return null;
        return cfg.responseTimeGenerators.get(random.nextInt(cfg.responseTimeGenerators.size()));
    }
    private Config.Generator getThroughputGenerator() {
        if (cfg.throughputGenerators == null || cfg.throughputGenerators.isEmpty()) return null;
        return cfg.throughputGenerators.get(random.nextInt(cfg.throughputGenerators.size()));
    }

    private Literal getLiteral(Config.Generator generator) {
        double value = random.nextGaussian() * generator.sd + generator.mean;
        return ResourceFactory.createTypedLiteral(String.format("%.3f", value), XSDDatatype.XSDdouble);
    }
}

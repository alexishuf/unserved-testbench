package br.ufsc.lapesd.unserved.testbench.model.impl;

import br.ufsc.lapesd.unserved.testbench.iri.Unserved;
import br.ufsc.lapesd.unserved.testbench.model.Message;
import br.ufsc.lapesd.unserved.testbench.model.UnservedFactory;
import br.ufsc.lapesd.unserved.testbench.model.Variable;
import br.ufsc.lapesd.unserved.testbench.model.When;
import br.ufsc.lapesd.unserved.testbench.util.BNodeShim;
import br.ufsc.lapesd.unserved.testbench.util.ImplementationByType;
import br.ufsc.lapesd.unserved.testbench.util.ResultSetStream;
import org.apache.jena.enhanced.EnhGraph;
import org.apache.jena.enhanced.EnhNode;
import org.apache.jena.enhanced.Implementation;
import org.apache.jena.graph.Node;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.rdf.model.Statement;

import java.util.Arrays;
import java.util.stream.Stream;

public class MessageImpl extends WithPartsImpl implements Message {
    public static Implementation factory = new ImplementationByType(Unserved.Message.asNode(),
            Arrays.asList(Unserved.when.asNode(), Unserved.from.asNode(), Unserved.to.asNode())) {
        @Override
        public EnhNode wrap(Node node, EnhGraph eg) {
            return new MessageImpl(node, eg);
        }
    };

    public MessageImpl(Node n, EnhGraph m) {
        super(n, m);
    }

    @Override
    public Variable getFrom() {
        Statement statement = getProperty(Unserved.from);
        return statement == null ? null : statement.getResource().as(Variable.class);
    }

    @Override
    public Variable getTo() {
        Statement statement = getProperty(Unserved.to);
        return statement == null ? null : statement.getResource().as(Variable.class);
    }

    @Override
    public When getWhen() {
        Statement statement = getProperty(Unserved.when);
        return statement == null ? null : UnservedFactory.asWhen(statement.getResource());
    }

    @Override
    public Stream<Message> getConsequent() {
        QueryExecution ex = QueryExecutionFactory.create(
                QueryFactory.create(String.format("PREFIX u: <" + Unserved.PREFIX + ">\n" +
                        "SELECT ?msg WHERE {\n" +
                        "  ?msg a u:Message.\n" +
                        "  ?msg u:when/u:reactionTo %1$s.\n" +
                        "}\n", BNodeShim.sparqlResource(this))), getModel());
        return ResultSetStream.toStream(ex, ex.execSelect(), "msg", Message.class);
    }
}

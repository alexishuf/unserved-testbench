package br.ufsc.lapesd.unserved.testbench.benchmarks.design1.data;

import br.ufsc.lapesd.unserved.testbench.benchmarks.design1.D1Factors;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.Objects;

public class ServicesIOFactors implements Serializable {
    private ServiceAltGraphFactors serviceAltGraphFactors;
    private int freeOutputs, connectedIOs;

    public ServicesIOFactors(D1Factors factors) {
        this.freeOutputs = factors.getFreeOutputCount();
        this.connectedIOs = factors.getConnectedIOs();
        this.serviceAltGraphFactors = new ServiceAltGraphFactors(factors);
    }

    public ServiceAltGraphFactors getServiceAltGraphFactors() {
        return serviceAltGraphFactors;
    }
    public int getFreeOutputs() {
        return freeOutputs;
    }
    public int getConnectedIOs() {
        return connectedIOs;
    }

    @Override
    public String toString() {
        return String.format("ServiceIOGraphFactors(%d, %d, %s)", getFreeOutputs(),
                getConnectedIOs(), getServiceAltGraphFactors());
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) return true;
        if (o == null) return false;
        if (!(o instanceof ServicesIOFactors)) return false;
        ServicesIOFactors rhs = (ServicesIOFactors) o;
        return getFreeOutputs() == rhs.getFreeOutputs()
                && getConnectedIOs() == rhs.getConnectedIOs()
                && Objects.equals(getServiceAltGraphFactors(), rhs.getServiceAltGraphFactors());
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getFreeOutputs()).append(getConnectedIOs())
                .append(getServiceAltGraphFactors()).hashCode();
    }
}

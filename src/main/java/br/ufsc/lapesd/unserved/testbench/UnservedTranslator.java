package br.ufsc.lapesd.unserved.testbench;

import br.ufsc.lapesd.unserved.testbench.input.Input;
import br.ufsc.lapesd.unserved.testbench.input.RDFInput;

import java.util.Collections;
import java.util.List;

public interface UnservedTranslator {
    RDFInput compile(List<Input> descriptions) throws UnservedTranslatorException;
    default RDFInput compile(Input description) throws UnservedTranslatorException {
        return compile(Collections.singletonList(description));
    }
}

package br.ufsc.lapesd.unserved.testbench.composer.impl;

import br.ufsc.lapesd.unserved.testbench.composer.Composition;
import br.ufsc.lapesd.unserved.testbench.process.data.DataContext;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;

import javax.annotation.Nonnull;

public class NullComposition implements Composition {
    @Override
    public boolean isNull() {
        return true;
    }

    @Nonnull
    @Override
    public Resource getWorkflowRoot() {
        throw new IllegalStateException("Null composition");
    }

    @Nonnull
    @Override
    public DataContext createDataContext() {
        throw new IllegalStateException("Null composition");
    }

    @Override
    public void close() {
        /* pass */
    }

    @Override
    public Model takeModel() {
        throw new IllegalStateException("Null composition");
    }
}

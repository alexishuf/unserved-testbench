package br.ufsc.lapesd.unserved.testbench.model.impl;

import br.ufsc.lapesd.unserved.testbench.iri.Unserved;
import br.ufsc.lapesd.unserved.testbench.model.ValuedCardinality;
import br.ufsc.lapesd.unserved.testbench.util.ImplementationByType;
import org.apache.jena.enhanced.EnhGraph;
import org.apache.jena.enhanced.EnhNode;
import org.apache.jena.enhanced.Implementation;
import org.apache.jena.graph.Node;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.impl.ResourceImpl;

import java.util.Collections;
import java.util.NoSuchElementException;

public class ValuedCardinalityImpl extends ResourceImpl implements ValuedCardinality {
    public static Implementation factory = new ImplementationByType(Unserved.ValuedCardinality.asNode(),
            Collections.singletonList(Unserved.cardinalityValue.asNode())) {
        @Override
        public EnhNode wrap(Node node, EnhGraph eg) {
            return new ValuedCardinalityImpl(node, eg);
        }
    };

    public ValuedCardinalityImpl(Node n, EnhGraph m) {
        super(n, m);
    }

    @Override
    public long getCardinalityValue() {
        Statement statement = getProperty(Unserved.cardinalityValue);
        if (statement == null) throw new NoSuchElementException();
        return statement.getLiteral().getLong();
    }
}

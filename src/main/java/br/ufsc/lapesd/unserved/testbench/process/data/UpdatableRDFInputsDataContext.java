package br.ufsc.lapesd.unserved.testbench.process.data;

import br.ufsc.lapesd.unserved.testbench.input.RDFInput;
import br.ufsc.lapesd.unserved.testbench.util.UpdatableRDFInputs;
import com.google.common.base.Preconditions;
import org.apache.jena.rdf.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collection;

public class UpdatableRDFInputsDataContext extends BaseDataContext {
    private static Logger logger = LoggerFactory.getLogger(UpdatableRDFInputsDataContext.class);
    private final UpdatableRDFInputs inputs;

    public UpdatableRDFInputsDataContext(@Nonnull UpdatableRDFInputs inputs) {
        Preconditions.checkNotNull(inputs);
        this.inputs = inputs;
    }

    @Override
    protected Model getModifiableModel() {
        return inputs.getModel();
    }

    @Override
    protected void baseAdd(@Nonnull Statement statement) {
        inputs.add(statement);
    }

    @Override
    protected void baseRemove(@Nonnull Statement statement) {
        inputs.remove(statement);
    }

    @Override
    protected void baseRemoveAll(@Nullable Resource subject, @Nullable Property predicate,
                                 @Nullable RDFNode object) {
        inputs.removeAll(subject, predicate, object);
    }

    @Override
    protected void baseClose() {
        try {
            inputs.close();
        } catch (Exception e) {
            logger.error("Ops", e);
        }
    }

    /**
     * Returns a Model with the current prover inputs. Note that this model may
     * become out-of-sync with the actual inputs as soon as the mutators
     * {set,unset}Variable are called.
     *
     * @return unmodifiable model representing the current inputs.
     */
    @Nonnull
    @Override
    public Model getUnmodifiableModel() {
        return inputs.getModel();
    }

    @Nonnull
    @Override
    public DataContext createNonIsolatedCopy() {
        return new UpdatableRDFInputsDataContext(new UpdatableRDFInputs(inputs));
    }

    public UpdatableRDFInputs getUpdatableInputs() {
        return inputs;
    }

    @Nonnull
    @Override
    public Collection<RDFInput> getInputs() {
        return inputs.getLayers();
    }
}

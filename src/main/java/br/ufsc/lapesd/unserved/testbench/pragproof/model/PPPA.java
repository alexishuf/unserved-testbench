package br.ufsc.lapesd.unserved.testbench.pragproof.model;

import br.ufsc.lapesd.unserved.testbench.BackgroundProvider;
import br.ufsc.lapesd.unserved.testbench.input.RDFInput;
import br.ufsc.lapesd.unserved.testbench.util.ResourcesBackground;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;

import java.util.Collections;

@BackgroundProvider(vocab = "https://alexishuf.bitbucket.io/2016/04/pragproof-proof-actions.n3#")
public class PPPA extends ResourcesBackground {
    public PPPA() {
        super(Collections.singletonList(ImmutablePair.of("pragproof-proof-actions.n3",
                RDFInput.N3)), false);
    }

    public static String IRI = "https://alexishuf.bitbucket.io/2016/04/pragproof-proof-actions.n3";
    public static String PREFIX = IRI + "#";

    public static Resource Input = ResourceFactory.createResource(PREFIX + "Input");

    public static Property source = ResourceFactory.createProperty(PREFIX + "source");
    public static Property depends = ResourceFactory.createProperty(PREFIX + "depends");
    public static Property transitiveEvidence = ResourceFactory.createProperty(PREFIX + "transitiveEvidence");
}

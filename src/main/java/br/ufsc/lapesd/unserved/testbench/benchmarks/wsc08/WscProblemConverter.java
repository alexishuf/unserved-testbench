package br.ufsc.lapesd.unserved.testbench.benchmarks.wsc08;

import br.ufsc.lapesd.unserved.testbench.benchmarks.RandomNFPAssigner;
import br.ufsc.lapesd.unserved.testbench.iri.HTTP;
import br.ufsc.lapesd.unserved.testbench.iri.Unserved;
import br.ufsc.lapesd.unserved.testbench.iri.UnservedX;
import com.google.common.base.Preconditions;
import org.apache.commons.io.FileUtils;
import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.rdf.model.*;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFFormat;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import static br.ufsc.lapesd.unserved.testbench.benchmarks.wsc08.Wscc.wscService;
import static br.ufsc.lapesd.unserved.testbench.iri.Unserved.*;
import static java.lang.String.format;

/**
 * This class converts a WSC08 problem into uNSERVED equivalent constructs.
 */
public class WscProblemConverter {
    private static final String svcNs = "http://www.ws-challenge.org/WSC08Services/#";
    private static final Property genericMember =
            ResourceFactory.createProperty("http://example.org/#part");

    public static Result load(@Nonnull File convertedRoot, boolean checkFiles) throws IOException {
        Preconditions.checkArgument(convertedRoot.getCanonicalFile().isDirectory());
        File taxonomyTTL = new File(convertedRoot, "taxonomy.ttl");
        File unservedTTL = new File(convertedRoot, "services.ttl");
        File knownTTL = new File(convertedRoot, "known.ttl");
        File wantedTTL = new File(convertedRoot, "wanted.ttl");

        if (checkFiles) {
            Model m = ModelFactory.createDefaultModel();
            try (FileInputStream in = new FileInputStream(taxonomyTTL)) {
                RDFDataMgr.read(m, in, Lang.TURTLE);
            }
            m.removeAll();
            try (FileInputStream in = new FileInputStream(unservedTTL)) {
                RDFDataMgr.read(m, in, Lang.TURTLE);
            }
            m.removeAll();
            try (FileInputStream in = new FileInputStream(knownTTL)) {
                RDFDataMgr.read(m, in, Lang.TURTLE);
            }
            m.removeAll();
            try (FileInputStream in = new FileInputStream(wantedTTL)) {
                RDFDataMgr.read(m, in, Lang.TURTLE);
            }
        }

        return new Result(taxonomyTTL, unservedTTL, knownTTL, wantedTTL, null);
    }

    public static Result convert(@Nonnull File wscProblemDir, @Nonnull File outDir,
                                 @Nullable RandomNFPAssigner nfpAssigner)
            throws IOException {
        Preconditions.checkArgument(wscProblemDir.isDirectory());
        File taxonomyTTL = null, unservedTTL = null, knownTTL = null, wantedTTL = null;
        Result result = null;
        try {
            Map<String, String> instanceTypes = getInstanceTypes(wscProblemDir);
            taxonomyTTL = convertTaxonomy(wscProblemDir, outDir);
            unservedTTL = convertServices(wscProblemDir, outDir, instanceTypes, nfpAssigner);
            knownTTL = createKnownVariables(wscProblemDir, outDir, instanceTypes);
            wantedTTL = createWantedVariables(wscProblemDir, outDir, instanceTypes);
            result = new Result(taxonomyTTL, unservedTTL, knownTTL, wantedTTL, null);
        } catch (WscProblemConverterException | SAXException | ParserConfigurationException
                | XPathExpressionException e) {
            throw new IOException(e); //i hate xml
        } finally {
            if (result == null) {
                if (taxonomyTTL != null) FileUtils.deleteQuietly(taxonomyTTL);
                if (unservedTTL != null) FileUtils.deleteQuietly(unservedTTL);
                if (knownTTL != null) FileUtils.deleteQuietly(knownTTL);
                if (wantedTTL != null) FileUtils.deleteQuietly(wantedTTL);
            }
        }
        return result;

    }

    private static String getTaxNs(File wscProblemDir) {
        return format("http://localhost/wsc/%s/taxonomy.owl#", wscProblemDir.getName());
    }

    private static class ProtoService {

        @Nonnull String name;
        List<Resource> inputTypes = null, outputTypes = null;
        public ProtoService(@Nonnull String name) {
            this.name = name;
        }

        public org.apache.jena.rdf.model.Resource createUnserved(Model m) {
            Literal wscServiceUri = ResourceFactory.createTypedLiteral(svcNs + name,
                    XSDDatatype.XSDanyURI);
            Resource from = m.createResource(Unserved.Variable);
            Resource to = m.createResource(Unserved.Variable);
            Resource req = m.createResource(Message)
                    .addProperty(wscService, wscServiceUri)
                    .addProperty(RDF.type, UnservedX.HTTP.HttpRequest)
                    .addProperty(RDF.type, HTTP.Request)
                    .addProperty(Unserved.from, from).addProperty(Unserved.to, to)
                    .addProperty(when, spontaneous)
                    .addProperty(HTTP.mthd, HTTP.Methods.POST)
                    .addProperty(HTTP.requestURI, ResourceFactory.createTypedLiteral("http://example.org/" + name, XSDDatatype.XSDanyURI));
            Resource reqBody = m.createResource(Variable)
                    .addProperty(type, m.createResource(RDFS.Class))
                    .addProperty(representation, Unserved.Resource);
            m.createResource(Binding).addProperty(RDF.type, PropertyBinding)
                    .addProperty(bindingResource, req).addProperty(bindingVariable, reqBody)
                    .addProperty(bindingProperty, HTTP.body);
            Resource res = m.createResource(Message)
                    .addProperty(wscService, wscServiceUri)
                    .addProperty(RDF.type, UnservedX.HTTP.HttpResponse)
                    .addProperty(RDF.type, HTTP.Response)
                    .addProperty(Unserved.from, to).addProperty(Unserved.to, from)
                    .addProperty(when, m.createResource(Reaction)
                            .addProperty(reactionTo, req)
                            .addProperty(reactionCardinality, one));
            Resource resBody = m.createResource(Variable)
                    .addProperty(type, m.createResource(RDFS.Class))
                    .addProperty(representation, createRDFRepresentation(m, "text/turtle"));
            res.addProperty(part, m.createResource(Part)
                    .addProperty(variable, resBody)
                    .addProperty(partModifier, m.createResource(UnservedX.HTTP.AsHttpProperty)
                            .addProperty(UnservedX.HTTP.httpProperty, HTTP.body)
                            .addProperty(UnservedX.HTTP.httpResource, res)));

            addParts(inputTypes, m, req, reqBody);
            addParts(outputTypes, m, resBody, resBody);
            return res;
        }

        private static void addParts(Collection<Resource> types, Model m, Resource partOf,
                                     Resource propertyOf) {
            types.forEach(type -> partOf.addProperty(part, m.createResource(Part)
                    .addProperty(variable, m.createResource(Variable)
                            .addProperty(representation, Resource)
                            .addProperty(Unserved.type, type))
                    .addProperty(partModifier, m.createResource(Part)
                            .addProperty(RDF.type, UnservedX.RDF.AsProperty)
                            .addProperty(UnservedX.RDF.property, genericMember)
                            .addProperty(UnservedX.RDF.propertyOf, propertyOf))));
        }

    }
    private static List<ProtoService> getProtoServices(@Nonnull Map<String, String> instanceTypes,
                                                       @Nonnull File wscProblemDir)
            throws ParserConfigurationException, IOException, SAXException,
            XPathExpressionException, WscProblemConverterException {
        File servicesXml = new File(wscProblemDir, "services.xml");
        Preconditions.checkArgument(servicesXml.exists() && servicesXml.canRead());

        DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
        Document doc = docBuilder.parse(servicesXml);
        XPath xp = XPathFactory.newInstance().newXPath();
        NodeList services;
        services = (NodeList) xp.evaluate("/services/service", doc, XPathConstants.NODESET);

        List<ProtoService> list = new ArrayList<>();
        for (int i = 0; i < services.getLength(); i++) {
            Node svcNode = services.item(i);
            ProtoService ps = new ProtoService(svcNode.getAttributes().getNamedItem("name")
                    .getNodeValue());
            ps.inputTypes = getIOTypes(instanceTypes, xp, svcNode, ps.name, "inputs");
            ps.outputTypes = getIOTypes(instanceTypes, xp, svcNode, ps.name, "outputs");
            list.add(ps);
        }
        return list;
    }

    private static List<Resource> getIOTypes(@Nonnull Map<String, String> instanceTypes, XPath xp,
                                             Node svcNode, String serviceName, String tagName) throws XPathExpressionException, WscProblemConverterException {
        List<Resource> list = s(xp.evaluate(tagName + "/instance", svcNode,
                XPathConstants.NODESET)).stream()
                .map(node -> node.getAttributes().getNamedItem("name").getNodeValue())
                .map(instanceTypes::get).map(ResourceFactory::createResource)
                .collect(Collectors.toList());
        if (!list.stream().allMatch(Objects::nonNull)) {
            throw new WscProblemConverterException("Some " + tagName + "s of " + serviceName
                    + " have no type!");
        }
        return list;
    }

    private static File convertServices(@Nonnull File wscProblemDir, @Nonnull File outDir,
                                        @Nonnull Map<String, String> instanceTypes,
                                        @Nullable RandomNFPAssigner assigner)
            throws ParserConfigurationException, SAXException, XPathExpressionException,
            IOException, WscProblemConverterException {
        Model model = null;
        File outFile = new File(outDir, "services.ttl");
        try (FileOutputStream out = new FileOutputStream(outFile)) {
            model = ModelFactory.createDefaultModel();
            model.setNsPrefix("u", Unserved.PREFIX);
            model.setNsPrefix("tax", getTaxNs(wscProblemDir));
            model.setNsPrefix("ux-rdf", UnservedX.RDF.PREFIX);
            model.setNsPrefix("ux-mt", UnservedX.MediaType.PREFIX);
            model.setNsPrefix("wscc", Wscc.PREFIX);
            for (ProtoService ps : getProtoServices(instanceTypes, wscProblemDir)) {
                org.apache.jena.rdf.model.Resource consequent = ps.createUnserved(model);
                if (assigner != null) assigner.appendTo(consequent);
            }
            /* WARNING: pretty turtle looses reactionTo triples and the whole tree of nodes
             * reachable from the triple's object  */
            RDFDataMgr.write(out, model, RDFFormat.TURTLE_FLAT);
        } finally {
            if (model != null)
                model.close();
        }
        return outFile;
    }

    private static Map<String, String> getInstanceTypes(File wscProblemDir) throws
            ParserConfigurationException, XPathExpressionException, IOException, SAXException {
        Map<String, String> map = new HashMap<>();
        String taxNs = getTaxNs(wscProblemDir);
        File taxFile = new File(wscProblemDir, "taxonomy.xml");
        Preconditions.checkArgument(taxFile.exists() && taxFile.canRead());

        Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder()
                .parse(taxFile);
        XPath xp = XPathFactory.newInstance().newXPath();

        NodeList nodes = (NodeList) xp.evaluate("//instance", doc, XPathConstants.NODESET);
        for (int i = 0; i < nodes.getLength(); i++) {
            Node parent = nodes.item(i).getParentNode();
            String conceptName = parent.getAttributes().getNamedItem("name").getNodeValue();
            String name = nodes.item(i).getAttributes().getNamedItem("name").getNodeValue();
            map.put(name, taxNs + conceptName);
        }
        return map;
    }

    private static List<Node> s(Object nodeListAsObject) {
        NodeList nodeList = (NodeList) nodeListAsObject;
        List<Node> set = new ArrayList<>(nodeList.getLength());
        for (int i = 0; i < nodeList.getLength(); i++) set.add(nodeList.item(i));
        return set;
    }

    private static File convertTaxonomy(File wscProblemDir, File outDir) throws IOException {
        File file = new File(wscProblemDir, "taxonomy.owl");
        Preconditions.checkArgument(file.exists() && file.canRead());
        Model m = ModelFactory.createDefaultModel();
        File outFile = new File(outDir, "taxonomy.ttl");
        try (FileInputStream in = new FileInputStream(file);
             FileOutputStream out = new FileOutputStream(outFile)) {
            RDFDataMgr.read(m, in, Lang.RDFXML);
            m.setNsPrefix("tax", format("http://localhost/wsc/%s/taxonomy.owl#",
                    wscProblemDir.getName()));
            RDFDataMgr.write(out, m, Lang.TURTLE);
        }
        return outFile;
    }

    private static File createKnownVariables(File wscProblemDir, File outDir,
                                             Map<String, String> instanceTypes) throws ParserConfigurationException, IOException, SAXException, XPathExpressionException {
        Model model = createProblemVariables(wscProblemDir, instanceTypes,
                "provided", true);
        File file = new File(outDir, "known.ttl");
        try (FileOutputStream out = new FileOutputStream(file)) {
            RDFDataMgr.write(out, model, Lang.TURTLE);
        } finally {
            model.close();
        }
        return file;
    }

    private static File createWantedVariables(File wscProblemDir, File outDir,
                                              Map<String, String> instanceTypes) throws ParserConfigurationException, SAXException, XPathExpressionException, IOException {
        Model model = createProblemVariables(wscProblemDir, instanceTypes,
                "wanted", false);
        model.listSubjectsWithProperty(RDF.type, Unserved.Variable).toList()
                .forEach(r -> r.addProperty(RDF.type, Unserved.Wanted));
        File file = new File(outDir, "wanted.ttl");
        try (FileOutputStream out = new FileOutputStream(file)) {
            RDFDataMgr.write(out, model, Lang.TURTLE);
        } finally {
            model.close();
        }
        return file;
    }

    private static Model createProblemVariables(File wscProblemDir,
                                                Map<String, String> instanceTypes, String tagName,
                                                boolean bound)
            throws ParserConfigurationException, IOException, SAXException,
            XPathExpressionException {
        File problemXml = new File(wscProblemDir, "problem.xml");
        Preconditions.checkArgument(problemXml.exists() && problemXml.canRead());

        Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(problemXml);
        XPath xp = XPathFactory.newInstance().newXPath();
        NodeList nodes = (NodeList) xp.evaluate("/problemStructure/task/" + tagName
                        + "/instance", doc, XPathConstants.NODESET);

        Model model = ModelFactory.createDefaultModel();
        model.setNsPrefix("u", Unserved.PREFIX);
        model.setNsPrefix("tax", getTaxNs(wscProblemDir));
        model.setNsPrefix("wsc", svcNs);
        for (int i = 0; i < nodes.getLength(); i++) {
            String instName = nodes.item(i).getAttributes().getNamedItem("name").getNodeValue();
            String typeName = instanceTypes.get(instName);
            Resource type = ResourceFactory.createResource(typeName);
            Resource var = model.createResource(svcNs + instName)
                    .addProperty(RDF.type, Unserved.Variable)
//                        .addProperty(Unserved.representation, createRDFRepresentation(model, "text/turtle"))
                    .addProperty(representation, Unserved.Resource)
                    .addProperty(Unserved.type, type);
            if (bound) {
                var.addProperty(RDF.type, ResourceBoundVariable)
                        .addProperty(RDF.type, ValueBoundVariable)
                        .addProperty(resourceValue, model.createResource(type));
//                    Model representationModel = ModelFactory.createDefaultModel();
//                    representationModel.createResource(type);
//                    ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
//                    RDFDataMgr.write(byteOut, representationModel, RDFFormat.TURTLE);
//                    String base64 = Base64.getEncoder().encodeToString(byteOut.toByteArray());
//
//                    var.addProperty(RDF.type, Unserved.ResourceBoundVariable)
//                            .addProperty(RDF.type, Unserved.ValueBoundVariable)
//                            .addProperty(valueRepresentation,
//                                    createRDFRepresentation(model, "text/turtle"))
//                            .addProperty(Unserved.literalValue,
//                                    ResourceFactory.createTypedLiteral(base64,
//                                            XSDDatatype.XSDanyURI));
            }
        }

        return model;
    }

    private static Resource createRDFRepresentation(Model model, String mediaType) {
        return model.createResource(UnservedX.MediaType.MediaType)
                .addProperty(UnservedX.MediaType.mediaTypeValue,
                        ResourceFactory.createTypedLiteral(mediaType, XSDDatatype.XSDstring))
                .addProperty(UnservedX.RDF.rdfMediaType,
                        ResourceFactory.createResource("http://w3id.org/spar/mediatype/" + mediaType));
    }

    public static class Result {
        @Nonnull public final File taxonomyTTL;
        @Nonnull public final File servicesTTL;
        @Nonnull public final File knownTTL;
        @Nonnull public final File wantedTTL;
        @Nullable public final File rules;

        public Result(@Nonnull File taxonomyTTL, @Nonnull File servicesTTL,
                       @Nonnull File knownTTL, @Nonnull File wantedTTL,
                       @Nullable File rules) {
            this.taxonomyTTL = taxonomyTTL;
            this.servicesTTL = servicesTTL;
            this.knownTTL = knownTTL;
            this.wantedTTL = wantedTTL;
            this.rules = rules;
        }

        @Nonnull
        public File getTaxonomyTTL() {
            return taxonomyTTL;
        }
        @Nonnull
        public File getServicesTTL() {
            return servicesTTL;
        }
        @Nonnull
        public File getKnownTTL() {
            return knownTTL;
        }
        @Nonnull
        public File getWantedTTL() {
            return wantedTTL;
        }
        @Nullable
        public File getRules() {
            return rules;
        }
    }

}

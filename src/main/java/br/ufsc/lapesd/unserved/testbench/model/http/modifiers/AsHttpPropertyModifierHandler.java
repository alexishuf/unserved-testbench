package br.ufsc.lapesd.unserved.testbench.model.http.modifiers;

import br.ufsc.lapesd.unserved.testbench.model.Variable;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;

import javax.annotation.Nonnull;

public interface AsHttpPropertyModifierHandler extends AutoCloseable {
    void handle(Resource modifier, Variable variable);

    /**
     * A handler is complete when it has determined that all information has been provided through
     * handle()
     *
     * @return true iff complete.
     */
    boolean isComplete();

    /**
     * For handlers that are not able to determine if they are complete, this method will return
     * true when the handler has enough information to construct a result node.
     *
     * If the handler implementation of isComplete() is not always false, this should be true
     * only when isComplete() also is.
     *
     * @return true if getResultNode() can be called.
     */
    boolean canGet();

    /**
     * Gets the value after processing by the modifier.
     *
     * @param model Model where to create the node and related resources.
     * @exception IllegalStateException if <code>canGet() == false</code>.
     * @exception NullPointerException if <code>model == null</code>
     * @return RDFNode with the represented value.
     */
    RDFNode getResultNode(@Nonnull Model model);

    @Override
    void close();
}

package br.ufsc.lapesd.unserved.testbench.input;

public class ClosedException extends RuntimeException {
    public ClosedException() {
        super("Object is already closed.");
    }
}

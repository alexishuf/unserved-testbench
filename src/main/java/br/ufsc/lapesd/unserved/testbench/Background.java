package br.ufsc.lapesd.unserved.testbench;

import br.ufsc.lapesd.unserved.testbench.input.RDFInput;
import org.apache.jena.rdf.model.Model;

import java.util.List;

/**
 * Gets an RDF model with knowledge related to a particular domain.
 *
 * Example implementation: Hydra vocabulary and transformation to uNSERVED rules.
 */
//TODO reuse instances and provide a cached Rules (bindSchema takes a long time to generate them)
public interface Background extends AutoCloseable {
    Model getModel();
    List<RDFInput> getInputs();
    boolean isSchema();
}

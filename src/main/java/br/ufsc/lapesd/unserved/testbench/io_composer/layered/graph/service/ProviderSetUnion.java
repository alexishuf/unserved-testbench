package br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service;

import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.Condition;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.lazy.EquivalenceSets;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.lazy.SetNode;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.discovery.NodeOutput;
import br.ufsc.lapesd.unserved.testbench.model.Variable;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.*;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toSet;

public class ProviderSetUnion implements ProviderSet {
    protected final @Nonnull List<ProviderSet> l = new ArrayList<>();

    public ProviderSetUnion add(@Nonnull ProviderSet ps) {
        assert ps.getTargets().stream()
                .allMatch(t -> l.stream().noneMatch(old -> old.getTargets().contains(t)));
        l.add(ps);
        return this;
    }

    @Override
    public IOProviderHashSet without(@Nonnull FilterMap f) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean isSatisfied() {
        return l.stream().allMatch(ProviderSet::isSatisfied);
    }

    @Nonnull
    @Override
    public Stream<PrecondMap> getPreconditionMappings(EquivalenceSets successorEqSets) {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public EquivalenceSets getEquivalenceSets(@Nonnull PrecondMap map,
                                              @Nullable EquivalenceSets parent) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void bindConstants(@Nonnull Map<Variable, NodeOutput> fullMap, SetNode successor, @Nonnull EquivalenceSets equivalenceSets, @Nonnull Set<IndexedNode> nodes) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean satisfyPreconditions(Collection<Node> providers) {
        return l.stream().allMatch(ps -> ps.satisfyPreconditions(providers));
    }

    @Nonnull
    @Override
    public Stream<Node> stream() {
        return l.stream().flatMap(ProviderSet::stream).distinct();
    }

    @Override
    public boolean contains(Node node) {
        return l.stream().anyMatch(ps -> ps.contains(node));
    }

    @Nonnull
    @Override
    public Set<Variable> getTargets() {
        return l.stream().flatMap(ps -> ps.getTargets().stream()).collect(toSet());
    }

    @Nonnull
    @Override
    public Set<Condition> getConditions() {
        return l.stream().flatMap(ps -> ps.getConditions().stream()).collect(toSet());
    }

    @Nonnull
    @Override
    public Set<Variable> getUnsatisfiedTargets() {
        return l.stream().flatMap(ps -> ps.getUnsatisfiedTargets().stream()).collect(toSet());
    }

    @Nonnull
    @Override
    public Set<Condition> getUnsatisfiedConditions() {
        return l.stream().flatMap(ps -> ps.getUnsatisfiedConditions().stream()).collect(toSet());
    }

    @Nonnull
    @Override
    public Set<Node> getProviders(@Nonnull Variable target) {
        return l.stream().flatMap(ps -> ps.getProviders(target).stream()).collect(toSet());
    }

    @Nonnull
    @Override
    public Variable getOutput(@Nonnull Variable target, @Nonnull Node provider) {
        return l.stream().filter(ps -> ps.getProviders(target).contains(provider)).findFirst()
                .orElseThrow(NoSuchElementException::new).getOutput(target, provider);
    }

    @Override
    public Set<Variable> getOutputs(@Nonnull Variable target, @Nonnull Node provider) {
        return l.stream().flatMap(ps -> ps.getOutputs(target, provider).stream()).collect(toSet());
    }

    @Nonnull
    @Override
    public Set<Node> getProviders(@Nonnull Condition condition) {
        return l.stream().flatMap(ps -> ps.getProviders(condition).stream()).collect(toSet());
    }

    @Nonnull
    @Override
    public Condition getPostcondition(@Nonnull Condition precondition, @Nonnull Node provider) {
        return l.stream().filter(ps -> ps.getProviders(precondition).contains(provider)).findFirst()
                .orElseThrow(NoSuchElementException::new).getPostcondition(precondition, provider);
    }

    @Nonnull
    @Override
    public Set<Condition> getPostconditions(@Nonnull Condition precondition, @Nonnull Node provider) {
        return l.stream().flatMap(ps -> ps.getPostconditions(precondition, provider).stream())
                .collect(toSet());
    }

    @Nonnull
    @Override
    public Stream<Assignment> getAssignmentsStream() {
        return l.stream().flatMap(ProviderSet::getAssignmentsStream);
    }

    @Nonnull
    @Override
    public Iterator<Node> iterator() {
        return stream().iterator();
    }
}

package br.ufsc.lapesd.unserved.testbench.process.model;

import br.ufsc.lapesd.unserved.testbench.model.Variable;

/**
 * Enhanced node interface for unserved-p:Copy
 */
public interface Copy  extends Action {
    Variable getFrom();
    Variable getTo();
    int getFromIndex();
    int getToIndex();
}

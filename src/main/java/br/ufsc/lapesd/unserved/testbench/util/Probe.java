package br.ufsc.lapesd.unserved.testbench.util;

import com.google.common.base.Preconditions;
import com.google.common.base.Stopwatch;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;

import java.io.PrintStream;
import java.util.HashMap;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;

public class Probe {
    public String name;
    Stopwatch w;
    double acc = 0;
    boolean saved = false;
    boolean cumulative = false;

    public static ArrayListMultimap<String, Double> values = ArrayListMultimap.create();
    public static HashMap<String, Probe> global = new HashMap<>();

    public Probe(String name, boolean cumulative) {
        this.name = name;
        this.cumulative = cumulative;
        w = Stopwatch.createStarted();
    }
    public Probe(String name) {
        this(name, false);
    }

    public void pause() {
        if (!w.isRunning()) return;
        acc += w.elapsed(TimeUnit.MICROSECONDS)/1000.0;
        w.reset();
    }

    public void resume() {
        Preconditions.checkState(!w.isRunning());
        w.start();
    }

    public void save() {
        Preconditions.checkState(!saved);
        pause();
        acc += values.get(name).stream().reduce((l, r) -> l + r).orElse(0.0);
        values.get(name).clear();
        values.put(name, acc);
        saved = true;
    }

    public static void reset() {
        values.clear();
    }
    public static void dump(PrintStream out) {
        values.entries().forEach(e -> out.printf("%s %.3f\n", e.getKey(), e.getValue()));
    }
    public static void dump(PrintStream out, ListMultimap<String, Double> left, ListMultimap<String, Double> right) {
        TreeSet<String> keys = new TreeSet<>(left.keySet());
        keys.addAll(right.keySet());
        keys.forEach(k -> {
            double lval = left.get(k).stream().reduce((l, r) -> l + r).orElse(Double.NaN);
            double rval = right.get(k).stream().reduce((l, r) -> l + r).orElse(Double.NaN);
            out.printf("%s %.3f   %.3f\n", k, lval, rval);
        });
    }
    public static ListMultimap<String, Double> snapshot() {
        return ArrayListMultimap.create(values);
    }

    public static Probe cumulative(String name) {
        return new Probe(name, true);
    }
    public static Probe createGlobal(String name) {
        Probe probe = new Probe(name, true);
        global.put(name, probe);
        return probe;
    }
    public static Probe getGlobal(String name) {
        return global.get(name);
    }
}

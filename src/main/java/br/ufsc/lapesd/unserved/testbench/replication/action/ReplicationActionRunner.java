package br.ufsc.lapesd.unserved.testbench.replication.action;

import br.ufsc.lapesd.unserved.testbench.components.ComponentRegistrationException;
import br.ufsc.lapesd.unserved.testbench.components.DefaultComponentFactory;
import br.ufsc.lapesd.unserved.testbench.components.SimpleActionRunnerFactory;
import br.ufsc.lapesd.unserved.testbench.components.WrappingComponentRegistry;
import br.ufsc.lapesd.unserved.testbench.iri.UnservedP;
import br.ufsc.lapesd.unserved.testbench.process.Context;
import br.ufsc.lapesd.unserved.testbench.process.OverriddenContext;
import br.ufsc.lapesd.unserved.testbench.process.actions.ActionExecutionException;
import br.ufsc.lapesd.unserved.testbench.process.actions.ActionRunner;
import br.ufsc.lapesd.unserved.testbench.process.data.DiffingDataContext;
import br.ufsc.lapesd.unserved.testbench.process.model.Receive;
import br.ufsc.lapesd.unserved.testbench.replication.ReplicatedExecutionException;
import br.ufsc.lapesd.unserved.testbench.replication.model.Replication;
import br.ufsc.lapesd.unserved.testbench.replication.model.ReplicationAction;
import com.google.common.base.Preconditions;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.vocabulary.RDF;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;

public class ReplicationActionRunner implements ActionRunner {

    @DefaultComponentFactory
    public static class Factory extends SimpleActionRunnerFactory {
        public Factory() {
            super(ReplicationActionRunner.class, Factory.class, Replication.ReplicationAction);
        }
    }

    @Override
    public void run(Resource action, Context outerContext) throws ActionExecutionException {
        Preconditions.checkArgument(action.canAs(ReplicationAction.class));
        ReplicationAction ppAction = action.as(ReplicationAction.class);
        ReplicationContainer container = ReplicationContainer.fromUUID(ppAction.getContainerId());
        if (container == null)
            throw new ActionExecutionException("Stale ReplicationAction");

        ActionRunner runner;
        runner = outerContext.componentRegistry().createActionRunner(container.getRealAction());

        try (DiffingDataContext dataContext = new DiffingDataContext(outerContext.data());
             Context myContext = new OverriddenContext(outerContext).setDataContext(dataContext)
                     .setComponentRegistry(getComponentRegistry(outerContext))) {
            Receive receive = findReceive(ppAction.getWrappedAction());
            if (receive != null) {
                new ReplicationActionRunnerContext(myContext.runners(), receive, dataContext,
                        container, outerContext);
            }
            runner.run(container.getRealAction(), myContext);
            if (receive == null) {
                container.getExecution().putDiff(dataContext.getDiff());
                container.getExecution().resumeInterpretation(myContext.interpreter());
            }
        } catch (ReplicatedExecutionException e) {
            throw new ReplicationActionRunnerResumeException(e);
        }
    }

    private WrappingComponentRegistry getComponentRegistry(Context outerContext) throws ActionExecutionException {
        WrappingComponentRegistry componentRegistry = new WrappingComponentRegistry(outerContext.componentRegistry());
        try {
            componentRegistry.registerComponent(ReplicationReceiveRunner.Factory.class);
        } catch (ComponentRegistrationException e) {
            throw new ActionExecutionException(e);
        }
        return componentRegistry;
    }

    private Receive findReceive(Resource realAction) {
        HashSet<Resource> visited = new HashSet<>();
        Queue<Resource> queue = new LinkedList<>();
        queue.add(realAction);
        while (!queue.isEmpty()) {
            Resource resource = queue.remove();
            if (resource.hasProperty(RDF.type, UnservedP.Receive))
                return resource.as(Receive.class);
            if (visited.contains(resource)) continue;
            visited.add(resource);

            //TODO not all properties should be followed, this is wasteful
            StmtIterator it = resource.listProperties();
            while (it.hasNext()) {
                Statement statement = it.next();
                if (statement.getObject().isResource())
                    queue.add(statement.getResource());
            }
        }
        return null;
    }
}

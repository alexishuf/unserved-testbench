package br.ufsc.lapesd.unserved.testbench.restdesc_pragproof.impl;

import br.ufsc.lapesd.unserved.testbench.composer.ComposerException;
import br.ufsc.lapesd.unserved.testbench.composer.impl.AbstractComposerInput;
import br.ufsc.lapesd.unserved.testbench.input.RDFInput;
import br.ufsc.lapesd.unserved.testbench.input.RDFInputFile;
import br.ufsc.lapesd.unserved.testbench.restdesc_pragproof.RESTdescComposerInput;
import br.ufsc.lapesd.unserved.testbench.util.Skolemizer;
import br.ufsc.lapesd.unserved.testbench.util.SkolemizerMap;
import com.google.common.base.Preconditions;
import org.apache.commons.io.IOUtils;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.reasoner.rulesys.Rule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static java.util.stream.Stream.concat;
import static java.util.stream.Stream.of;

public class RESTdescComposerInputImpl extends AbstractComposerInput implements RESTdescComposerInput {
    private Logger logger = LoggerFactory.getLogger(RESTdescComposerInputImpl.class);

    private boolean parentInputs = false;
    private boolean parentRuleInputs = false;
    private final List<RDFInput> inputs = new ArrayList<>();
    private final List<RDFInput> ruleInputs = new ArrayList<>();
    private RDFInput goal = null;
    private RDFInputFile rulesFile = null;
    private Model union = null;
    private SkolemizerMap skolemizerMap = null;
    private RDFInput forcedRules = null;

    public RESTdescComposerInputImpl(RESTdescComposerInput parent) {
        super(parent);
        inputs.addAll(parent.getInputs());
        ruleInputs.addAll(parent.getRuleInputs());
        parentInputs = true;
        parentRuleInputs = true;
    }

    public RESTdescComposerInputImpl() {
        super(null);
        skolemizerMap = new SkolemizerMap();
    }

    @Override
    public List<RDFInput> getInputs() {
        Preconditions.checkState(!closed);
        return inputs;
    }

    @Override
    public List<RDFInput> getRuleInputs() {
        return ruleInputs;
    }

    @Override
    public void addInput(RDFInput input) {
        Preconditions.checkState(!closed);
        if (input.getFormat().equals(RDFInput.N3)) {
            ruleInputs.add(input);
            invalidateRulesFile();
        } else {
            inputs.add(input);
            invalidateUnion();
        }
    }

    @Override
    public void addRulesFile(File file) {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public List<Rule> customRules() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void setGoal(RDFInput goal) {
        this.goal = goal;
    }

    @Override
    public RDFInput getGoal() {
        if (goal == null && parent != null) return getParent().getGoal();
        return goal;
    }

    @Nonnull
    @Override
    public Model initSkolemizedUnion(boolean basicReasoning, boolean inputHasNoSkolemized) throws IOException {
        Preconditions.checkState(!closed);
        if (union == null) {
            union = ModelFactory.createDefaultModel();
            ArrayList<Model> list = new ArrayList<>();
            for (RDFInput input : inputs) list.add(input.getModel());
            new Skolemizer(() -> skolemizerMap).skolemizeModels(union, list);
        }
        return union;
    }

    private void invalidateUnion() {
        parentInputs = false;
        if (parent != null)
            skolemizerMap = new SkolemizerMap(parent.getSkolemizerMap());
        if (union != null) {
            union.close();
            union = null;
        }
    }

    @Override
    public RDFInput initRules() throws IOException {
        if (rulesFile == null) {
            File outFile = Files.createTempFile("RESTdescComposerInput-rules", ".n3").toFile();
            try (FileOutputStream out = new FileOutputStream(outFile)) {
                for (RDFInput input : ruleInputs) {
                    try (FileInputStream in = new FileInputStream(input.toFile())) {
                        IOUtils.copy(in, out);
                    }
                }
            }
            rulesFile = RDFInputFile.createOwningFile(outFile, RDFInput.N3);
        }
        return rulesFile;
    }

    @Override
    public RDFInput getRules() {
        if (forcedRules != null) return forcedRules;
        if (getParent() != null && parentRuleInputs) return getParent().getRules();
        return rulesFile;
    }

    @Override
    public void forceRules(RDFInput forced) {
        if (forcedRules != null) {
            try {
                forcedRules.close();
            } catch (IOException e) {
                logger.error("Error closing old forcedRules {}", forcedRules, e);
            }
        }
        forcedRules = forced;
    }

    private RESTdescComposerInput getParent() {
        return (RESTdescComposerInput)parent;
    }

    private void invalidateRulesFile() {
        Preconditions.checkState(forcedRules == null);
        try {
            parentRuleInputs = false;
            if (rulesFile != null) {
                rulesFile.close();
                rulesFile = null;
            }
        } catch (IOException e) {
            logger.error("Swallowing exception on invalidateRulesFile.", e);
        }
    }

    @Nonnull
    @Override
    public Model getSkolemizedUnion() {
        if (getParent() != null && parentInputs) return getParent().getSkolemizedUnion();
        return union;
    }

    @Override
    public SkolemizerMap getSkolemizerMap() {
        if (skolemizerMap == null && parent != null) return parent.getSkolemizerMap();
        return skolemizerMap;
    }

    @Override
    protected void closeImpl() throws ComposerException {
        ArrayList<RDFInput> errors = new ArrayList<>(inputs.size());
        Set<RDFInput> blacklist = new HashSet<>();
        if (parent != null) {
            blacklist.addAll(getParent().getInputs());
            blacklist.addAll(getParent().getRuleInputs());
            blacklist.add(getParent().getRules());
        }
        concat(concat(inputs.stream(), ruleInputs.stream()), of(rulesFile, forcedRules))
                .filter(i -> i != null)
                .filter(i -> !blacklist.contains(i))
                .forEach(input -> {
                    try {
                        if (input != null)
                            input.close();
                    } catch (IOException e) {
                        errors.add(input);
                    }
                });
        if (!errors.isEmpty()) {
            throw new ComposerException(errors.size() + " errors on close(): "
                    + errors.stream().map(RDFInput::toString).reduce((l, r) -> l + ", " + r)
                    .orElse(""));
        }
    }
}

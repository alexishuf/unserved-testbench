package br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ServiceLayer {
    private final List<Node> nodes = new ArrayList<>();

    public ServiceLayer(Collection<Node> nodes) {
        this.nodes.addAll(nodes);
    }

    @Nonnull
    public List<Node> getNodes() {
        return nodes;
    }

    public boolean replaceNode(@Nonnull Node old, @Nonnull Node replacement) {
        boolean contained = nodes.remove(old);
        nodes.add(replacement);
        return contained;
    }
}

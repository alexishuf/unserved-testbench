package br.ufsc.lapesd.unserved.testbench.io_composer.conditions.parsers;

import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.And;
import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.Condition;
import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.Or;
import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.atomic.TriplePattern;
import br.ufsc.lapesd.unserved.testbench.model.Variable;
import com.google.common.base.Preconditions;
import org.apache.jena.rdf.model.RDFList;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.StmtIterator;
import org.spinrdf.model.Element;
import org.spinrdf.model.Union;
import org.spinrdf.vocabulary.SP;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static br.ufsc.lapesd.unserved.testbench.iri.Unserved.condition;
import static java.util.stream.Stream.concat;

public class SimpleSPINConditionsParser implements ConditionsParser {
    /**
     * Parses SPIN conditions. Supports only:
     *
     * 1. RDF lists
     * 2. sp:Union
     * 3. sp:TriplePattern
     *
     * @throws ConditionParsingException if an unsupported construct is used.
     */
    @Nonnull
    @Override
    public And parse(@Nonnull Resource resource) throws ConditionParsingException {
        Preconditions.checkArgument(resource.getModel() != null);
        ArrayList<Condition> list = new ArrayList<>();
        for (StmtIterator it = resource.listProperties(condition); it.hasNext(); )
            list.add(parseElement(it.next().getResource()));
        return And.of(list);
    }

    @Nonnull
    public Condition parseElement(@Nonnull Resource r) throws ConditionParsingException {
        if (r.canAs(RDFList.class)) {
            return And.of(r.as(RDFList.class).asJavaList().stream().filter(RDFNode::isResource)
                    .map(n -> parseElement(n.asResource())).collect(Collectors.toList()));
        } else if (TriplePattern.isTriplePattern(r)) {
            return TriplePattern.fromSPIN(r);
        } else if (r.canAs(org.spinrdf.model.Union.class)) {
            List<Element> es = r.as(Union.class).getElements();
            return new Or(es.stream().map(this::parseElement).collect(Collectors.toList()));
        }
        throw new ConditionParsingException("Unsupported SPIN element type. Only RDF Lists, " +
                "sp:Union and sp:TriplePattern are supported", r);
    }

    public And parseForVariables(Set<Variable> variables) {
        SimpleSPINConditionsParser parser = new SimpleSPINConditionsParser();
        return And.of(variables.stream().flatMap(v -> concat(
                v.getModel().listSubjectsWithProperty(SP.subject, v).toList().stream(),
                concat(v.getModel().listSubjectsWithProperty(SP.predicate, v).toList().stream(),
                        v.getModel().listSubjectsWithProperty(SP.object, v).toList().stream()))
        ).distinct().map(parser::parseElement).collect(Collectors.toList()));
    }

}

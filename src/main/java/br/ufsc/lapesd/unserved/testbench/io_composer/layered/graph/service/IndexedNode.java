package br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service;

import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.And;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.cost.CostInfo;
import br.ufsc.lapesd.unserved.testbench.model.Variable;
import com.google.common.base.Preconditions;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.*;

public class IndexedNode implements Node {
    public final Node node;
    public final int index;
    private Map<IndexedVariable, Variable> forcedBindings = new HashMap<>();
    private Set<Variable> inputsCache = null, outputsCache = null;
    private And preconditionsCache = null, postconditionsCache = null;


    private IndexedNode(Node node) {
        this(node, 0);
    }

    private IndexedNode(Node node, int index) {
        assert index >= 0;
        this.node = node;
        this.index = index;
    }

    public static IndexedNode wrap(Node node) {
        return wrap(node, 0);
    }

    public static IndexedNode wrap(Node node, int index) {
        if (node instanceof IndexedNode) {
            Preconditions.checkArgument(((IndexedNode) node).index == index);
            return ((IndexedNode)node);
        } else {
            return new IndexedNode(node, index);
        }
    }

    public Node getNode() {
        return node;
    }

    public int getIndex() {
        return index;
    }

    public @Nonnull Map<IndexedVariable, Variable> getForcedBindings() {
        return forcedBindings;
    }

    public @Nullable Variable boundTo(@Nonnull Variable input) {
        if (input instanceof IndexedVariable) {
            return forcedBindings.getOrDefault(input, null);
        } else {
            return forcedBindings.getOrDefault(new IndexedVariable(input, index), null);
        }
    }

    void bindInput(@Nonnull Variable input, @Nonnull Variable source) {
        Preconditions.checkArgument(source.isValueBound());
        if (input instanceof IndexedVariable) {
            Preconditions.checkArgument(getInputs().contains(input));
            forcedBindings.put((IndexedVariable)input, source);
        } else {
            Preconditions.checkArgument(node.getInputs().contains(input));
            forcedBindings.put(new IndexedVariable(input, index), source);
        }
    }

    public boolean isA(Class<? extends Node> clazz) {
        return clazz.isAssignableFrom(node.getClass());
    }

    public <T> T as(Class<T> clazz) {
        //noinspection unchecked
        return (T)node;
    }

    @Override
    @Nonnull
    public Set<Variable> getInputs() {
        assert index >= 0;
        if (index > 0 && inputsCache == null) {
            inputsCache = new HashSet<>();
            for (Variable in : node.getInputs()) inputsCache.add(new IndexedVariable(in, index));
        }
        return index == 0 ? node.getInputs() : inputsCache;
    }

    @Override
    @Nonnull
    public Set<Variable> getOutputs() {
        assert index >= 0;
        if (index > 0 && outputsCache == null) {
            outputsCache = new HashSet<>();
            for (Variable in : node.getInputs()) outputsCache.add(new IndexedVariable(in, index));
        }
        return index == 0 ? node.getInputs() : outputsCache;
    }

    @Override
    public Object getId() {
        return node.getId();
    }

    @Override
    @Nonnull
    public And getPreConditions() {
        assert index >= 0;
        if (index > 0 && preconditionsCache == null) {
            Map<Variable, Variable> map = new HashMap<>();
            for (Variable var : node.getInputs()) map.put(var, new IndexedVariable(var, index));
            preconditionsCache = (And)node.getPreConditions().replacing(map);
        }
        return index == 0 ? node.getPreConditions() : preconditionsCache;
    }

    @Override
    @Nonnull
    public And getPostConditions() {
        assert index >= 0;
        if (index > 0 && postconditionsCache == null) {
            Map<Variable, Variable> map = new HashMap<>();
            for (Variable var : node.getInputs()) map.put(var, new IndexedVariable(var, index));
            postconditionsCache = (And)node.getPostConditions().replacing(map);
        }
        return index == 0 ? node.getPostConditions() : postconditionsCache;
    }

    @Override
    @Nonnull
    public CostInfo getCostInfo() {
        return node.getCostInfo();
    }

    

    @Override
    public List<Node> getAlternatives() {
        return node.getAlternatives();
    }

    public @Nonnull List<IndexedNode> getIndexedAlternatives() {
        List<Node> alts = node.getAlternatives();
        List<IndexedNode> result = new ArrayList<>(alts.size());
        for (Node alt : alts) {
            result.add(alt instanceof IndexedNode ? (IndexedNode)alt
                                                  : new IndexedNode(alt, index));
        }
        return result;
    }

    @Override
    public String toString() {
        return String.format("%s[%d]", node, index);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IndexedNode that = (IndexedNode) o;
        return index == that.index &&
                Objects.equals(node, that.node);
    }

    @Override
    public int hashCode() {
        return Objects.hash(node, index);
    }
}

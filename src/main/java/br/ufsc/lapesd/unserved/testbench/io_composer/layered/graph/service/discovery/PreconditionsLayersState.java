package br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.discovery;

import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.*;
import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.atomic.TriplePattern;
import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.atomic.VariableSpec;
import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.index.TransitiveIndex;
import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.index.UnservedTriplePatternIndex;
import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.index.VariableSpecIndex;
import br.ufsc.lapesd.unserved.testbench.io_composer.impl.IOComposerInput;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.*;
import br.ufsc.lapesd.unserved.testbench.model.Part;
import br.ufsc.lapesd.unserved.testbench.model.Variable;
import br.ufsc.lapesd.unserved.testbench.util.TransitiveClosureGetter;
import com.google.common.base.Preconditions;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.RDFS;
import org.spinrdf.vocabulary.SP;

import javax.annotation.Nonnull;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static br.ufsc.lapesd.unserved.testbench.iri.UnservedX.RDF;

public class PreconditionsLayersState extends IOLayersState {
    private @Nonnull VariableSpecIndex<NodeOutput> vsIndex;
    private @Nonnull UnservedTriplePatternIndex<NodeTriplePattern> tpIndex;
    private @Nonnull Set<Node> current = new HashSet<>();
    private @Nonnull MyConditionEvaluator myConditionEvaluator = new MyConditionEvaluator();
    private @Nonnull MyVariableSpecsEvaluator myVariableSpecsEvaluator = new MyVariableSpecsEvaluator();
    private final @Nonnull TransitiveClosureGetter superClassGetter;
    private final @Nonnull TransitiveClosureGetter superPropGetter;

    public PreconditionsLayersState(@Nonnull TransitiveClosureGetter superClassGetter) {
        super();
        Preconditions.checkArgument(superClassGetter.getPredicate().equals(RDFS.subClassOf));
        Preconditions.checkArgument(!superClassGetter.isBackward());
        this.superClassGetter = superClassGetter;
        this.superPropGetter = superClassGetter.createFor(RDFS.subPropertyOf, false);

        vsIndex = VariableSpecIndex.withClosureOnPut(superClassGetter);
        tpIndex = new UnservedTriplePatternIndex<>(
                () -> VariableSpecIndex.withClosureOnPut(superClassGetter),
                () -> TransitiveIndex.withClosureOnPut(superClassGetter),
                () -> TransitiveIndex.withClosureOnPut(superPropGetter));
    }

    @Override
    public boolean supportsSPINConditions() {
        return true;
    }

    @Nonnull
    @Override
    public ConditionEvaluator getEvaluator() {
        return myConditionEvaluator;
    }

    @Nonnull
    @Override
    public ConditionEvaluator getVariableSpecsEvaluator() {
        return myVariableSpecsEvaluator;
    }

    private class MyVariableSpecsEvaluator implements ConditionEvaluator {
        @Override
        public <T extends Condition> boolean evaluate(@Nonnull T condition)
                throws IllegalArgumentException {
            Preconditions.checkArgument(condition instanceof VariableSpec);
            return vsIndex.getFirst(((VariableSpec) condition)) != null;
        }

        @Override
        public <T extends Condition> boolean canEvaluate(@Nonnull T condition) {
            return condition instanceof VariableSpec;
        }
    }

    private class MyConditionEvaluator extends BaseConditionEvaluator {
        public MyConditionEvaluator() {
            this.add(TriplePattern.class, this::evaluateTriplePattern);
            this.add(VariableSpec.class, this::evaluateVariableSpec);
        }

        public boolean evaluateTriplePattern(@Nonnull TriplePattern tp) {
            return tpIndex.getStream(tp).findAny().isPresent();
        }
        public boolean evaluateVariableSpec(@Nonnull VariableSpec spec) {
            return vsIndex.getFirst(spec) != null;
        }
    }

    @Override
    public void advance(@Nonnull Collection<Node> nodes) {
        super.advance(nodes);
        current.clear();
        for (Node n : nodes) {
            current.add(n);
            n.getOutputs().forEach(o -> vsIndex.put(o.asSpec(), new NodeOutput(n, o)));
            getOutputTriplePatterns(n).forEach(tp -> tpIndex.put(tp, new NodeTriplePattern(n, tp)));
        }
    }

    static List<TriplePattern> getTriplePatterns(@Nonnull And and) {
        List<TriplePattern> list = new ArrayList<>();
        ConditionTraverser.visitPreOrder(and, new ConditionVisitor() {
            @Override
            public boolean visitTriplePattern(@Nonnull TriplePattern cond) {
                list.add(cond);
                return true;
            }
        });
        return list;
    }

    static List<TriplePattern> getOutputTriplePatterns(@Nonnull Node node) {
        List<TriplePattern> list = getTriplePatterns(node.getPostConditions());
        /* part relations also imply sp:TriplePatterns when there is a ux-rdf:property */
        addTriplePatternsFromParts(list, node.getOutputs());
        return list;
    }

    static private void addTriplePatternsFromParts(List<TriplePattern> list, Set<Variable> outputs) {
        /* Important: for StartNode and EndNode, getOutputs does not use getPartsRecursive(). */
        Set<Variable> visited = new HashSet<>();
        Stack<Variable> stack = new Stack<>();
        outputs.forEach(stack::push);
        while (!stack.isEmpty()) {
            Variable var = stack.pop();
            if (visited.contains(var)) continue;
            visited.add(var);
            for (Part part : var.getParts()) {
                Variable partVariable = part.getVariable();
                stack.push(partVariable);

                Resource pm = part.getPartModifier();
                if (pm == null) continue;
                Resource property = pm.getPropertyResourceValue(RDF.property);
                Resource propertyOf = pm.getPropertyResourceValue(RDF.propertyOf);
                if (property != null && propertyOf.equals(var))
                    list.add(new TriplePattern(var, property, partVariable));
            }
        }
    }

    @Override
    @Nonnull
    public ProviderSetsSupplier createProviderSetsSupplier() {
        return new ProviderSetsSupplier();
    }

    @Nonnull
    @Override
    public NodesSupplier<? extends PreconditionsLayersState> createNodesSupplier() {
        return new PreconditionIndexedNodesSupplier(superClassGetter);
    }

    /**
     * An implementation of {@link AutoFixProviderSetsSupplier} that reuses data structures
     * internal to the {@link PreconditionsLayersState} instance.
     */
    public class ProviderSetsSupplier implements AutoFixProviderSetsSupplier {
        @Nonnull
        @Override
        public Map<Node, ProviderSet> get(@Nonnull Collection<Node> targetNodes) {
            Map<VariableSpec, Set<NodeOutput>> vsAlts = targetNodes.stream()
                    .flatMap(n -> n.getInputs().stream().map(VariableSpec::new))
                    .distinct().collect(Collectors.toMap(Function.identity(), vsIndex::get));
            Map<Node, List<TriplePattern>> patterns = targetNodes.stream()
                    .collect(Collectors.toMap(Function.identity(),
                            n -> getTriplePatterns(n.getPreConditions())));
            Map<TriplePattern, Set<NodeTriplePattern>> tpAlts = targetNodes.stream()
                    .flatMap(n -> patterns.get(n).stream())
                    .collect(Collectors.toMap(Function.identity(), tpIndex::get));
            return targetNodes.stream()
                    .collect(Collectors.toMap(Function.identity(),
                            n -> getProviderSet(n, patterns.get(n), vsAlts, tpAlts)));
        }

        @Nonnull ProviderSet
        getProviderSet(@Nonnull Node node,
                       List<TriplePattern> triplePatterns,
                       @Nonnull Map<VariableSpec, Set<NodeOutput>> vsAlts,
                       @Nonnull Map<TriplePattern, Set<NodeTriplePattern>> tpAlts) {
            ConditionProviderHashSet ps = new ConditionProviderHashSet();
            for (Variable i : node.getInputs()) {
                for (NodeOutput no : vsAlts.get(i.asSpec()))
                    if (!current.contains(no.node)) ps.add(i, no.node, no.output);
            }
            for (TriplePattern tp : triplePatterns) {
                ps.addCondition(tp);
                for (NodeTriplePattern no : tpAlts.get(tp))
                    if (!current.contains(no.node)) ps.add(tp, no.node, no.triplePattern);
            }
            return ps;
        }

        @Override
        public void update(@Nonnull Collection<Node> nodes) {
            assert nodes.stream().flatMap(n -> n.getOutputs().stream()).map(VariableSpec::new)
                    .allMatch(s -> vsIndex.getFirst(s) != null);
            nodes.forEach(current::remove);
        }

        @Override
        public boolean isActive() {
            return true;
        }
        @Override
        public void begin(IOComposerInput composerInput,
                          @Nonnull StartNode startNode, @Nonnull EndNode endNode) {
            /* no op */
        }
        @Override
        public void end() {
            /* no op */
        }
    }

    static {
        SP.getModel();
    }
}

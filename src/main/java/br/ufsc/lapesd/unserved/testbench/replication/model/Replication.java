package br.ufsc.lapesd.unserved.testbench.replication.model;

import br.ufsc.lapesd.unserved.testbench.replication.model.impl.ReplicationActionImpl;
import br.ufsc.lapesd.unserved.testbench.util.ResourcesBackground;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.jena.enhanced.BuiltinPersonalities;
import org.apache.jena.enhanced.Personality;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.riot.RDFFormat;

import java.util.Collections;

public class Replication extends ResourcesBackground {
    public Replication() {
        super(Collections.singletonList(ImmutablePair.of("replication.ttl", RDFFormat.TURTLE)),
                true);
    }

    public static String IRI = "https://alexishuf.bitbucket.io/2016/04/replication.ttl";
    public static String PREFIX = IRI + "#";
    public static String PREFIX_SHORT = "utb-rep";

    private static Replication instance = new Replication();
    public static Replication getInstance() {
        return instance;
    }

    private static void install(Personality<RDFNode> p) {
        p.add(ReplicationAction.class, ReplicationActionImpl.factory);
    }

    private static Resource r(String localName) {
        return getInstance().getModel().createResource(PREFIX + localName);
    }
    private static Property p(String localName) {
        return getInstance().getModel().createProperty(PREFIX + localName);
    }

    public static final Resource ReplicationAction = r("ReplicationAction");
    public static final Property wrappedAction = p("wrappedAction");
    public static final Property containerId = p("containerId");

    static {
        install(BuiltinPersonalities.model);
    }
}

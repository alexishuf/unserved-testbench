package br.ufsc.lapesd.unserved.testbench.io_composer.replicated;

import br.ufsc.lapesd.unserved.testbench.composer.ComposerException;
import br.ufsc.lapesd.unserved.testbench.composer.Composition;
import br.ufsc.lapesd.unserved.testbench.composer.ReplicatedComposer;
import br.ufsc.lapesd.unserved.testbench.composer.impl.NullComposition;
import br.ufsc.lapesd.unserved.testbench.composer.impl.ReplicationComposition;
import br.ufsc.lapesd.unserved.testbench.io_composer.IOComposer;
import br.ufsc.lapesd.unserved.testbench.io_composer.IOCompositionResult;
import br.ufsc.lapesd.unserved.testbench.io_composer.impl.DefaultIOComposerTimes;
import br.ufsc.lapesd.unserved.testbench.io_composer.impl.DefaultIOCompositionResult;
import br.ufsc.lapesd.unserved.testbench.io_composer.impl.IOComposerInputDelta;
import br.ufsc.lapesd.unserved.testbench.io_composer.impl.IOComposerInputReference;
import br.ufsc.lapesd.unserved.testbench.process.Interpreter;
import br.ufsc.lapesd.unserved.testbench.process.actions.ActionExecutionException;
import br.ufsc.lapesd.unserved.testbench.process.data.DataContext;
import br.ufsc.lapesd.unserved.testbench.process.data.DataContextDiff;
import br.ufsc.lapesd.unserved.testbench.process.data.SimpleDataContext;
import br.ufsc.lapesd.unserved.testbench.replication.ReplicatedExecution;
import br.ufsc.lapesd.unserved.testbench.replication.ReplicatedExecutionException;
import br.ufsc.lapesd.unserved.testbench.util.jena.UncloseableGraph;
import com.google.common.base.Preconditions;
import com.google.common.base.Stopwatch;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.util.concurrent.TimeUnit;

public abstract class AbstractExecution implements ReplicatedExecution {
    private static Logger logger = LoggerFactory.getLogger(AbstractExecution.class);

    @Nonnull
    protected IOComposerInputDelta composerInput;
    @Nonnull
    protected Interpreter interpreter;
    protected boolean closed = false;
    protected DataContext dataContext = null;
    protected DataContextDiff diff = null;
    protected Composition lastComp = null;
    @Nonnull
    protected DefaultIOComposerTimes times = new DefaultIOComposerTimes();

    public AbstractExecution(@Nonnull IOComposerInputDelta composerInput,
                             @Nonnull Interpreter interpreter,
                             DataContextDiff diff,
                             Composition lastComp) {
        this.composerInput = composerInput;
        this.interpreter = interpreter;
        this.diff = diff;
        this.lastComp = lastComp;
    }

    @Override
    @Nonnull
    public DefaultIOComposerTimes getTimes() {
        return times;
    }

    @Override
    public abstract AbstractExecution duplicate();
    protected abstract ReplicatedComposer getAlgorithm();
    protected abstract void blacklistOperation(@Nonnull Composition composition);

    @Override
    public DataContext getDataContext() {
        Preconditions.checkState(!closed);
        if (dataContext == null) {
            Model model = ModelFactory.createModelForGraph(new UncloseableGraph(
                    composerInput.getSkolemizedUnion().getGraph()));
            dataContext = new SimpleDataContext(model, r -> {
                Resource resolved = composerInput.getSkolemizerMap().resolve(r);
                return resolved == null ? r : resolved;
            });
        }
        return dataContext;
    }

    @Override
    public void putDiff(DataContextDiff diff) {
        Preconditions.checkState(!closed);
        Preconditions.checkState(this.diff == null);
        this.diff = diff;
    }

    @Override
    public void resumeInterpretation() throws ReplicatedExecutionException {
        Preconditions.checkState(!closed);
        boolean isLast = lastComp != null && lastComp instanceof ReplicationComposition
                && ((ReplicationComposition) lastComp).isLast();
        Composition comp = null;
        AbstractExecution ex = duplicate();
        try {
            if (diff != null)
                ex.composerInput.apply(diff);
            if (isLast) {
                comp = new NullComposition();
            } else {
                try {
                    comp = timedRun(ex);
                } catch (ComposerException e) {
                    try {
                        ex.close();
                    } catch (ComposerException e2) {
                        throw new ReplicatedExecutionException("Can't close duplicate", e2);
                    }
                    ex = this;
                    logger.debug("Re-planning failed with diff, blacklisting", e);
                    blacklistOperation(lastComp);
                    try {
                        comp = timedRun(this);
                    } catch (ComposerException e2) {
                        throw new ReplicatedExecutionException("Cant' plan further", e2);
                    }
                }
            }

            try (DataContext dataContext = comp.isNull() ? null : comp.createDataContext()) {
                if (comp.isNull()) {
                    interpreter.deliverResult(ex.createResult());
                } else {
                    assert dataContext != null;
                    ex.diff = null;
                    ex.lastComp = comp;
                    interpreter.interpret(dataContext, comp.getWorkflowRoot()).waitForDone();
                    assert comp instanceof ReplicationComposition;
                    ReplicationComposition repComp = (ReplicationComposition) comp;
                    if (repComp.getContainer().wasForked()) {
                        repComp.getContainer().join();
                    }
                }
            } catch (ActionExecutionException | InterruptedException e) {
                throw new ReplicatedExecutionException(e);
            }
        } finally {
            if (comp != null)
                comp.close();
            try {
                if (ex != this)
                    ex.close();
            } catch (ComposerException e) {
                throw new ReplicatedExecutionException("Can't close duplicate execution (end)", e);
            }
        }
    }

    private Composition timedRun(AbstractExecution ex) {
        Stopwatch watch = Stopwatch.createStarted();
        try {
            return ex.getAlgorithm().run();
        } finally {
            times.addTime(IOComposer.planningTime, watch.elapsed(TimeUnit.NANOSECONDS),
                    TimeUnit.NANOSECONDS);
        }
    }

    private IOCompositionResult createResult() {
        return new DefaultIOCompositionResult(new IOComposerInputReference(composerInput), times);
    }

    @Override
    public void close() {
        if (!closed) {
            if (dataContext != null) dataContext.close();
            composerInput.close();
            closed = true;
        }
    }


}

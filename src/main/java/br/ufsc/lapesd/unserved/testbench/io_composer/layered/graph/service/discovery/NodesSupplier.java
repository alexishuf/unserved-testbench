package br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.discovery;

import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.BuilderDelegate;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.Node;
import br.ufsc.lapesd.unserved.testbench.model.Message;

import javax.annotation.Nonnull;
import java.util.Set;

/**
 * Supplies lists of nodes that constitute a layer.
 * @param <State> Class with info pertaining to each layer, this is specific to each implementation
 */
public interface NodesSupplier<State extends LayersState> extends BuilderDelegate {

    /**
     * Get all {@link Node} instances to be considered in the next layer. This should follow the
     * constraints defined in section 4.1 of doi:10.4018/jwsr.2012040101.
     *
     * @param state implementation-specific state that is kept as layers are generated
     * @param blacklist Set of consequent messages that should not be returned in produced Nodes.
     * @return Set of nodes, may be empty.
     */
    @Nonnull
    Set<Node> getNodes(@Nonnull State state, @Nonnull Set<Message> blacklist);
}

package br.ufsc.lapesd.unserved.testbench.process.model.impl;

import br.ufsc.lapesd.unserved.testbench.iri.UnservedP;
import br.ufsc.lapesd.unserved.testbench.model.Message;
import br.ufsc.lapesd.unserved.testbench.process.model.Receive;
import br.ufsc.lapesd.unserved.testbench.util.ImplementationByType;
import org.apache.jena.enhanced.EnhGraph;
import org.apache.jena.enhanced.EnhNode;
import org.apache.jena.enhanced.Implementation;
import org.apache.jena.graph.Node;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.impl.ResourceImpl;

public class ReceiveImpl extends ResourceImpl implements Receive {
    public static Implementation factory = new ImplementationByType(UnservedP.Receive.asNode()) {
        @Override
        public EnhNode wrap(Node node, EnhGraph eg) {
            return new ReceiveImpl(node, eg);
        }
    };

    public ReceiveImpl(Node n, EnhGraph m) {
        super(n, m);
    }

    @Override
    public Message getMessage() {
        Statement statement = getProperty(UnservedP.message);
        return statement == null ? null : statement.getResource().as(Message.class);
    }

    @Override
    public int getInvocationIndex() {
        Statement statement = getProperty(UnservedP.invocationIndex);
        return statement == null ? 0 : statement.getInt();
    }
}

package br.ufsc.lapesd.unserved.testbench.io_composer.layered;

import br.ufsc.lapesd.unserved.testbench.components.ComponentRegistry;
import br.ufsc.lapesd.unserved.testbench.components.DefaultComponentRegistry;
import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.atomic.VariableSpec;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.AbstractLayeredGraphValidator;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.lazy.SetNode;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.lazy.SetNodeEdge;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.IndexedNode;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.ServiceNode;
import br.ufsc.lapesd.unserved.testbench.io_composer.state.*;
import br.ufsc.lapesd.unserved.testbench.iri.Unserved;
import br.ufsc.lapesd.unserved.testbench.iri.UnservedP;
import br.ufsc.lapesd.unserved.testbench.model.Variable;
import br.ufsc.lapesd.unserved.testbench.process.SimpleContext;
import br.ufsc.lapesd.unserved.testbench.process.UnservedPInterpreter;
import br.ufsc.lapesd.unserved.testbench.process.actions.ActionExecutionException;
import br.ufsc.lapesd.unserved.testbench.process.actions.CopyRunner;
import br.ufsc.lapesd.unserved.testbench.process.data.DataContext;
import br.ufsc.lapesd.unserved.testbench.process.data.DataContextDiff;
import br.ufsc.lapesd.unserved.testbench.process.data.DiffingDataContext;
import br.ufsc.lapesd.unserved.testbench.util.NaiveTransitiveClosureGetter;
import com.google.common.base.Preconditions;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.OWL2;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public abstract class PathDiffValidator extends AbstractLayeredGraphValidator<Action, SetNode, SetNodeEdge> {
    private final TRVariablesMap remainingTRs;
    private final ExecutionPath<Action> executedPath;
    private final NaiveTransitiveClosureGetter superClassGetter = new NaiveTransitiveClosureGetter(RDFS.subClassOf, false);
    private Set<IndexedNode> failedNodes;
    private final @Nonnull Set<Variable> wanted;
    private final @Nonnull Set<Variable> pending;
    /**
     * Map from Variable instances that failed to be bound to the most recently bound
     * {@link Variable} that is compatible with the key. If there is no such bound variable,
     * a null must be present on the map.
     */
    protected final Map<Variable, Variable> autoFix;

    public PathDiffValidator(@Nonnull SetNodeEdge edge, ExecutionPath<Action> actions,
                             @Nonnull DataContext dataContext) {
        super(edge, dataContext);
        remainingTRs = new TRVariablesMap();
        outputs(actions).stream().map(v -> ImmutablePair.of(new VariableSpec(v), v))
                .forEach(p -> remainingTRs.add(p.left, p.right));
        failedNodes = null;
        executedPath = new ExecutionPath<>();
        wanted = copyTargets(actions);
        pending = new HashSet<>(wanted);
        autoFix = new HashMap<>();
    }

    protected PathDiffValidator(@Nonnull PathDiffValidator other) {
        super(other);
        remainingTRs = new TRVariablesMap(other.remainingTRs);
        executedPath = new ExecutionPath<>(other.executedPath);
        failedNodes = other.failedNodes == null ? null : new HashSet<>(other.failedNodes);
        wanted = new HashSet<>(other.wanted);
        pending = new HashSet<>(other.pending);
        autoFix = new HashMap<>();
        autoFix.putAll(other.autoFix);
    }

    @Override
    public boolean validateActions(@Nonnull DataContextDiff diff,
                                   @Nonnull ExecutionPath<Action> path) {
        Preconditions.checkState(!isReady());
        Preconditions.checkState(!isFailed());
        executedPath.append(path);

        /* bookkeeping based on invoked services */
        outputs(path).stream().map(v -> ImmutablePair.of(new VariableSpec(v), v))
                .forEach(p -> remainingTRs.remove(p.left, p.right));

        Set<Variable> failed;
        /* working DataContext */
        try (DiffingDataContext temp = new DiffingDataContext(dataContext)) {
            if (this.diff != null) this.diff.apply(temp);
            diff.apply(temp);

            Set<Variable> written = written(path);
            Set<Variable> bound = bound(temp, written);
            pending.removeAll(bound);
            bound.forEach(autoFix::remove);
            TRVariablesMap boundMap = boundMap(bound);
            failed = failed(temp, wanted);
            failed = updateAutoFix(failed, temp, boundMap);

            this.diff = temp.getDiff();
        }

        boolean fatal = failed.stream().map(VariableSpec::new)
                .map(remainingTRs::getVariables).anyMatch(Set::isEmpty);

        if (!fatal && pending.isEmpty()) {
            applyAutoFix(); //no-op if autoFix is empty
            ready = true;
        }
        if (fatal) {
            Set<Variable> hardFails = autoFix.keySet().stream().filter(k -> autoFix.get(k) == null)
                    .collect(Collectors.toSet());
            failedNodes = computeFailedNodes(hardFails, executedPath);
            this.failed = true;
        }

        return !fatal;
    }

    private Set<IndexedNode> computeFailedNodes(Set<Variable> failed, ExecutionPath<Action> path) {
        HashMap<Variable, List<Action>> sourceMap = new HashMap<>();
        for (Action a : path.asList()) {
            Set<Variable> outputs = actionOutputs(a);
            for (Variable output : outputs) {
                List<Action> list = sourceMap.getOrDefault(output, null);
                if (list == null) sourceMap.put(output, list = new ArrayList<>());
                list.add(a);
            }
        }

        Map<MessagePairAction, IndexedNode> index = new HashMap<>();
        edge.getFrom().getNodes().stream().filter(n -> n.node instanceof ServiceNode)
                .forEach(n -> index.put(new MessagePairAction(
                        ((ServiceNode)n.node).getAntecedent(),
                        ((ServiceNode)n.node).getConsequent(), n.index), n));

        return failed.stream().map(v -> dfsFailedMessagePairs(v, sourceMap))
                .flatMap(Set::stream).distinct()
                .map(p -> index.get(p.withoutCopies()))
                .collect(Collectors.toSet());
    }

    private Set<MessagePairAction> dfsFailedMessagePairs(Variable var,
                                                         HashMap<Variable, List<Action>> sourceMap) {
        Set<MessagePairAction> failedActions = new HashSet<>();
        Set<Action> visited = new HashSet<>();
        Stack<Variable> stack = new Stack<>();
        stack.push(var);
        while (!stack.isEmpty()) {
            Variable current = stack.pop();
            List<Action> sources = sourceMap.getOrDefault(current, null);
            if (sources == null) continue;
            sources = sources.stream().filter(a -> !visited.contains(a))
                    .collect(Collectors.toList());
            visited.addAll(sources);
            if (sources.isEmpty()) continue;

            for (Action a : sources) {
                if (a instanceof MessagePairAction) {
                    failedActions.add((MessagePairAction) a);
                } else if (a instanceof Copy) {
                    stack.add(((Copy)a).getFrom());
                } else if (a instanceof Copies) {
                    ((Copies)a).asList().stream().filter(c -> c.getTo().equals(current))
                            .map(Copy::getFrom).forEach(stack::add);
                }
            }
        }
        return failedActions;
    }

    /**
     * Set of nodes that had at least one failed variable that could not be auto-fixed.
     */
    protected Set<IndexedNode> getFailedNodes() {
        Preconditions.checkState(failedNodes != null);
        return failedNodes;
    }

    private void applyAutoFix() {
        Preconditions.checkState(autoFix.keySet().stream().map(autoFix::get)
                .allMatch(Objects::nonNull));
        ComponentRegistry componentRegistry = DefaultComponentRegistry.getInstance();
        UnservedPInterpreter interpreter = new UnservedPInterpreter(componentRegistry);
        Model actionModel = null;

        try (DiffingDataContext dataContext = new DiffingDataContext(this.dataContext);
             SimpleContext context = new SimpleContext(dataContext, componentRegistry, interpreter)) {
            diff.apply(dataContext);
            actionModel = ModelFactory.createDefaultModel();
            Resource copy = actionModel.createResource().addProperty(RDF.type, UnservedP.Copy)
                    .addProperty(RDF.type, UnservedP.Action);

            for (Map.Entry<Variable, Variable> e : autoFix.entrySet()) {
                Variable from = dataContext.getResource(e.getKey(), Variable.class);
                Variable to = dataContext.getResource(e.getValue(), Variable.class);
                copy.removeAll(UnservedP.copyFrom).addProperty(UnservedP.copyFrom, from)
                        .removeAll(UnservedP.copyTo).addProperty(UnservedP.copyTo, to);
                new CopyRunner().run(copy, context);
            }
        } catch (ActionExecutionException e) {
            throw new RuntimeException(e); //unexpected
        } finally {
            if (actionModel != null) actionModel.close();
        }
    }

    private Set<Variable> updateAutoFix(@Nonnull Set<Variable> failed, @Nonnull DataContext context,
                                        @Nonnull TRVariablesMap bound) {
        Set<Variable> stillFailed = new HashSet<>();
        for (Variable f : failed) {
            VariableSpec tr = new VariableSpec(f);
            Variable b = bound.getVariables(tr).stream().findFirst().orElse(null);
            if (b == null && !autoFix.containsKey(f)) {
                b = findBoundVariable(context, tr);
            }
            assert !Objects.equals(f, b) : f + " is both failed and bound";
            autoFix.put(f, b);
            if (b == null) stillFailed.add(f);
        }
        return stillFailed;
    }

    @Nullable
    private Variable findBoundVariable(@Nonnull DataContext context,
                                       @Nonnull VariableSpec tr) {
        return context.getUnmodifiableModel().listSubjectsWithProperty(RDF.type,
                Unserved.ValueBoundVariable).toList().stream()
                .filter(v -> v.canAs(Variable.class))
                .map(v -> v.as(Variable.class))
                .filter(v -> {
                    Resource type = v.getPropertyResourceValue(Unserved.type);
                    return type != null && type.equals(tr.getType());
                }).filter(v -> Objects.equals(v.getPropertyResourceValue(Unserved.
                                valueRepresentation), tr.getRepresentation()))
                .findFirst().orElse(null);
    }

    private Set<Variable> failed(@Nonnull DataContext after,  @Nonnull Set<Variable> written) {
        Set<VariableSpec> wanted = written.stream().map(VariableSpec::new)
                .collect(Collectors.toSet());
        /* unbound variables are on type of failure */
        Set<Variable> failed = written.stream().filter(v -> {
            Variable var = after.getResource(v, Variable.class);
            assert var != null;
            return !var.isValueBound();
        }).collect(Collectors.toSet());
        /* variables with bad representation are also a failure */
        written.stream().filter(v -> !failed.contains(v)).filter(v -> {
            VariableSpec expected = v.asSpec();
            Variable var = after.getResource(v, Variable.class);
            assert var != null;
            VariableSpec actual = new VariableSpec(var);
            return !checkSubclass(expected.getType(), actual.getType())
                    || !checkSubclass(expected.getType(), actual.getType());
        }).forEach(failed::add);
        return failed;
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    private boolean checkSubclass(Resource expected, Resource actual) {
        return Stream.concat(superClassGetter.getClosureStream(actual),
                Stream.of(OWL2.Thing, RDFS.Resource)).anyMatch(r -> r.equals(expected));
    }

    private Set<Variable> bound(@Nonnull DataContext after, @Nonnull Set<Variable> written) {
        return written.stream().map(v -> {
            Variable inAfter = after.getResource(v, Variable.class);
            return inAfter != null  ? v.enhance(inAfter) : null;
        }).filter(Objects::nonNull).filter(Variable::isValueBound).collect(Collectors.toSet());
    }

    private TRVariablesMap boundMap(Set<Variable> bound) {
        TRVariablesMap map = new TRVariablesMap();
        bound.stream().map(v -> ImmutablePair.of(new VariableSpec(v), v))
                .forEachOrdered(p -> map.add(p.left, p.right));
        return map;
    }

    private Set<Variable> written(ExecutionPath<Action> path) {
        return path.asList().stream().map(PathDiffValidator::actionOutputs).flatMap(Set::stream)
                .collect(Collectors.toSet());
    }

    private static Set<Variable> actionOutputs(Action action) {
        Set<Variable> set = actionCopyTargets(action);
        if (action instanceof MessagePairAction)
            set.addAll(((MessagePairAction)action).getConsequent().getVariablesRecursive());
        return set;
    }

    private static Set<Variable> actionCopyTargets(Action action) {
        Set<Variable> list = new HashSet<>();
        if (action instanceof MessagePairAction) {
            MessagePairAction mp = (MessagePairAction) action;
            list.addAll(mp.getAssignments().getTargets());
        } else if (action instanceof Copies) {
            list.addAll(((Copies) action).getTargets());
        } else if (action instanceof Copy) {
            list.add(((Copy) action).getTo());
        } else {
            throw new IllegalArgumentException("Unknown Action implementation");
        }
        return list;
    }

    private Set<Variable> copyTargets(ExecutionPath<Action> path) {
        return path.asList().stream().map(PathDiffValidator::actionCopyTargets)
                .flatMap(Set::stream).collect(Collectors.toSet());
    }

    private Set<Variable> outputs(ExecutionPath<Action> path) {
        return path.asList().stream().map(PathDiffValidator::actionOutputs)
                .flatMap(Set::stream).collect(Collectors.toSet());
    }

    private static final class TRVariablesMap {
        private HashMap<VariableSpec, Set<Variable>> map = new HashMap<>();

        public TRVariablesMap() {
        }

        public TRVariablesMap(TRVariablesMap other) {
            map.putAll(other.map);
        }

        public Set<Variable> getVariables(VariableSpec tr) {
            return map.getOrDefault(tr, Collections.emptySet());
        }

        public void add(VariableSpec tr, Variable var) {
            Set<Variable> set = map.getOrDefault(tr, null);
            if (set == null) map.put(tr, set = new LinkedHashSet<>());
            set.add(var);
        }

        public void remove(VariableSpec tr, Variable var) {
            Set<Variable> set = map.getOrDefault(tr, null);
            if (set != null) {
                set.remove(var);
                if (set.isEmpty()) map.remove(tr);
            }
        }
    }

}

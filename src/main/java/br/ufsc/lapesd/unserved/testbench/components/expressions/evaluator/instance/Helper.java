package br.ufsc.lapesd.unserved.testbench.components.expressions.evaluator.instance;

import br.ufsc.lapesd.unserved.testbench.util.BFSBGPIterator;
import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;

import java.util.Collection;

class Helper {
    static boolean isA(Resource resource, Resource aClass) {
        return resource.hasProperty(RDF.type, aClass) ||
                BFSBGPIterator.from(resource, RDF.type).forward(RDFS.subClassOf)
                        .toStream().anyMatch(c -> c.equals(aClass));
    }

    static boolean isA(Literal literal, Resource datatype) {
        String uri = literal.getDatatypeURI();
        return datatype.isURIResource() && uri.equals(datatype.getURI()) ||
                BFSBGPIterator.from(datatype).backward(RDFS.subClassOf).toStream()
                        .filter(Resource::isURIResource).map(Resource::getURI)
                        .anyMatch(u -> u.equals(uri));
    }

    static boolean isA(RDFNode node, Resource aClass) {
        return node.isResource() ? isA(node.asResource(), aClass) : isA(node.asLiteral(), aClass);
    }

    static boolean isA(RDFNode node, Collection<Resource> classes) {
        return classes.stream().anyMatch(c -> isA(node, c));
    }
}

package br.ufsc.lapesd.unserved.testbench.io_composer.conditions.parsers;

import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.And;
import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.atomic.VariableSpec;
import br.ufsc.lapesd.unserved.testbench.iri.Unserved;
import br.ufsc.lapesd.unserved.testbench.model.Message;
import br.ufsc.lapesd.unserved.testbench.model.Variable;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.StmtIterator;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;

/**
 * Parser for {@link VariableSpec} instances. Extracts  the variables tha are part of a
 * given {@link Message} or {@link Variable}.
 */
public class VariableSpecParser implements ConditionsParser {
    @Nonnull
    @Override
    public And parse(@Nonnull Resource resource) throws IllegalArgumentException {
        return new And(Collections.unmodifiableCollection(parseList(resource)));
    }

    @Nonnull
    public List<VariableSpec> parseList(@Nonnull Resource resource) throws ConditionParsingException {
        List<VariableSpec> list = new ArrayList<>();
        parseList(list, resource);
        return list;
    }

    public void parseLists(@Nonnull Consumer<Variable> variables,
                           @Nonnull Consumer<VariableSpec> specs, @Nonnull Resource resource) {
        for (StmtIterator it = resource.listProperties(Unserved.part); it.hasNext(); ) {
            Resource part = it.next().getResource();
            Variable var = part.getPropertyResourceValue(Unserved.variable).as(Variable.class);
            variables.accept(var);
            specs.accept(var.asSpec());
        }
    }

    private void parseList(@Nonnull List<VariableSpec> list,
                           @Nonnull Resource resource) throws ConditionParsingException {
        parseLists(v -> {}, list::add, resource);
    }
}

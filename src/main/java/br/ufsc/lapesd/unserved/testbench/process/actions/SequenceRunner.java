package br.ufsc.lapesd.unserved.testbench.process.actions;

import br.ufsc.lapesd.unserved.testbench.components.DefaultComponentFactory;
import br.ufsc.lapesd.unserved.testbench.components.SimpleActionRunnerFactory;
import br.ufsc.lapesd.unserved.testbench.iri.UnservedP;
import br.ufsc.lapesd.unserved.testbench.process.Context;
import br.ufsc.lapesd.unserved.testbench.process.model.Action;
import br.ufsc.lapesd.unserved.testbench.process.model.Sequence;
import com.google.common.base.Preconditions;
import org.apache.jena.rdf.model.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class SequenceRunner implements ActionRunner {
    private static Logger logger = LoggerFactory.getLogger(SequenceRunner.class);

    @DefaultComponentFactory
    public static class Factory extends SimpleActionRunnerFactory {
        public Factory() {
            super(SequenceRunner.class, SequenceRunner.Factory.class,
                    new UnservedP().getModel().createResource(UnservedP.Sequence.getURI()));
        }
    }

    @Override
    public void run(Resource action, Context context) throws ActionExecutionException {
        Preconditions.checkArgument(action.canAs(Sequence.class));
        Sequence sequence = action.as(Sequence.class);

        List<List<Action>> lists = sequence.getMembersLists();
        logger.debug("Starting {} lists from {}", lists.size(), sequence);
        for (List<Action> list : lists) {
            logger.debug("Starting a List with {} members from {}", list.size(), sequence);
            for (Action member : list) {
                ActionRunner runner = context.componentRegistry().createActionRunner(member);
                runner.run(member, context);
            }
        }
        logger.debug("Completed {}", sequence);
    }
}

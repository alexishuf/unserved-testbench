package br.ufsc.lapesd.unserved.testbench.iri;

import br.ufsc.lapesd.unserved.testbench.util.ResourcesBackground;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.riot.RDFFormat;

import java.util.Collections;

public class UnservedN extends ResourcesBackground {
    public static final String IRI = "https://alexishuf.bitbucket.io/2016/04/unserved/unserved-n.ttl";
    public static final String PREFIX = IRI + "#";
    public static final String PREFIX_SHORT = "unserved-n";

    public UnservedN() {
        super(Collections.singletonList(ImmutablePair.of("unserved/unserved-n.ttl",
                RDFFormat.TURTLE)), true);
    }

    private static UnservedN instance = new UnservedN();
    public static UnservedN getInstance() {
        return instance;
    }

    private static Resource r(String action) {
        return getInstance().getModel().createResource(PREFIX + action);
    }
    private static Property p(String copyFrom) {
        return getInstance().getModel().createProperty(PREFIX + copyFrom);
    }

    public static final Resource NFP = r("NFP");
    public static final Resource ResponseTime = r("ResponseTime");
    public static final Resource Throughput = r("Throughput");

    public static final Property value = p("value");

    static {
        init();
    }

    public static void init() { }
}

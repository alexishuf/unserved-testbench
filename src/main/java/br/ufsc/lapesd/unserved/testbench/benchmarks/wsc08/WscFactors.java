package br.ufsc.lapesd.unserved.testbench.benchmarks.wsc08;

import br.ufsc.lapesd.unserved.testbench.Algorithm;
import br.ufsc.lapesd.unserved.testbench.benchmarks.experiment.Factors;
import com.google.common.base.Preconditions;

import javax.annotation.Nonnull;
import java.io.File;
import java.lang.reflect.Field;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class WscFactors implements Factors {
    @Nonnull private final File wscConvertedRoot;
    private final int problem;
    private final String subDir;
    private final String known;
    private final String wanted;
    private final int preheatCount;
    private final String childXms;
    private final String childXmx;
    private final String childJVMOpts;
    private final boolean execute;
    private final boolean serviceCompression;
    private final boolean backwardClosureOpt;
    private final boolean preconditions;
    private final boolean onlyGraph;
    private final boolean naiveIO;
    private final boolean combinedJN;
    private final boolean listSubclasses;
    private final boolean naiveTCG;
    private final boolean dismissOutput;
    private final File failPoints;
    @Nonnull private final Algorithm algorithm;

    public WscFactors(@Nonnull File wscConvertedRoot, int problem, String subDir, String known,
                      String wanted, int preheatCount, String childXms, String childXmx,
                      String childJVMOpts, boolean execute, boolean serviceCompression,
                      boolean backwardClosureOpt, boolean preconditions, boolean onlyGraph,
                      boolean naiveIO, boolean combinedJN, boolean listSubclasses,
                      boolean naiveTCG, boolean dismissOutput, File failPoints,
                      @Nonnull Algorithm algorithm) {
        Preconditions.checkArgument(problem > 0 || subDir != null);
        this.wscConvertedRoot = wscConvertedRoot;
        this.problem = problem;
        this.subDir = subDir;
        this.known = known;
        this.wanted = wanted;
        this.preheatCount = preheatCount;
        this.childXms = childXms;
        this.childXmx = childXmx;
        this.childJVMOpts = childJVMOpts;
        this.execute = execute;
        this.serviceCompression = serviceCompression;
        this.backwardClosureOpt = backwardClosureOpt;
        this.preconditions = preconditions;
        this.onlyGraph = onlyGraph;
        this.naiveIO = naiveIO;
        this.combinedJN = combinedJN;
        this.listSubclasses = listSubclasses;
        this.naiveTCG = naiveTCG;
        this.dismissOutput = dismissOutput;
        this.failPoints = failPoints;
        this.algorithm = algorithm;
    }

    @Override
    public String getValue(String name) {
        try {
            return String.valueOf(WscFactors.class.getDeclaredField(name).get(this));
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        } catch (NoSuchFieldException e) {
            throw new IllegalArgumentException("Unknown name " + name);
        }
    }

    @Override
    public List<String> getNames() {
        return Stream.of(WscFactors.class.getDeclaredFields()).map(Field::getName)
                .collect(Collectors.toList());
    }

    @Nonnull
    public File getWscConvertedRoot() {
        return wscConvertedRoot;
    }
    public int getProblem() {
        return problem;
    }
    public String getSubDir() {
        return subDir;
    }
    public String getKnown() {
        return known;
    }
    public String getWanted() {
        return wanted;
    }
    public int getPreheatCount() {
        return preheatCount;
    }
    public String getChildXms() {
        return childXms;
    }
    public String getChildXmx() {
        return childXmx;
    }
    public String getChildJVMOpts() {
        return childJVMOpts;
    }
    public boolean getExecute() {
        return execute;
    }
    public boolean getServiceCompression() {
        return serviceCompression;
    }
    public boolean getBackwardClosureOpt() {
        return backwardClosureOpt;
    }
    public boolean getPreconditions() {
        return preconditions;
    }
    public boolean getOnlyGraph() {
        return onlyGraph;
    }
    public boolean getNaiveIO() {
        return naiveIO;
    }
    public boolean getCombinedJN() {
        return combinedJN;
    }
    public boolean getListSubclasses() {
        return listSubclasses;
    }
    public boolean getNaiveTCG() {
        return naiveTCG;
    }
    public boolean getDismissOutput() {
        return dismissOutput;
    }
    public File getFailPoints() {
        return failPoints;
    }

    @Nonnull
    public Algorithm getAlgorithm() {
        return algorithm;
    }

    @Override
    public String toString() {
        String flags = String.format("%s%s%s%s%s%s%s%s%s", serviceCompression ? "c" : "",
                backwardClosureOpt ? "b" : "", execute ? "e" : "", preconditions ? "p" : "",
                onlyGraph ? "g" : "", naiveIO ? "n" : "", combinedJN ? "j" : "",
                listSubclasses ? "s" : "", naiveTCG ? "t" : "");
        return String.format("WSC08-%s with %s[%s]%s",
                (subDir != null ? subDir : String.valueOf(problem)), algorithm,
                flags, failPoints == null ? "" : ", failures=" + failPoints);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        WscFactors that = (WscFactors) o;

        if (problem != that.problem) return false;
        if (!subDir.equals(that.subDir)) return false;
        if (preheatCount != that.preheatCount) return false;
        if (execute != that.execute) return false;
        if (serviceCompression != that.serviceCompression) return false;
        if (backwardClosureOpt != that.backwardClosureOpt) return false;
        if (preconditions != that.preconditions) return false;
        if (onlyGraph != that.onlyGraph) return false;
        if (naiveIO != that.naiveIO) return false;
        if (combinedJN != that.combinedJN) return false;
        if (listSubclasses != that.listSubclasses) return false;
        if (naiveTCG != that.naiveTCG) return false;
        if (dismissOutput != that.dismissOutput) return false;
        if (!wscConvertedRoot.equals(that.wscConvertedRoot)) return false;
        if (failPoints != null ? !failPoints.equals(that.failPoints) : that.failPoints != null)
            return false;
        return algorithm == that.algorithm;
    }

    @Override
    public int hashCode() {
        int result = wscConvertedRoot.hashCode();
        result = 31 * result + problem;
        result = 31 * result + subDir.hashCode();
        result = 31 * result + preheatCount;
        result = 31 * result + (execute ? 1 : 0);
        result = 31 * result + (serviceCompression ? 1 : 0);
        result = 31 * result + (backwardClosureOpt ? 1 : 0);
        result = 31 * result + (preconditions ? 1 : 0);
        result = 31 * result + (onlyGraph ? 1 : 0);
        result = 31 * result + (naiveIO ? 1 : 0);
        result = 31 * result + (combinedJN ? 1 : 0);
        result = 31 * result + (listSubclasses ? 1 : 0);
        result = 31 * result + (naiveTCG ? 1 : 0);
        result = 31 * result + (dismissOutput ? 1 : 0);
        result = 31 * result + (failPoints != null ? failPoints.hashCode() : 0);
        result = 31 * result + algorithm.hashCode();
        return result;
    }

    public static class Builder {
        private @Nonnull File wscConvertedRoot;
        private int problem = -1;
        private String subDir = null;
        private String wanted = null;
        private String known = null;
        private int preheatCount = 0;
        private String childXms = null;
        private String childXmx = null;
        private String childJVMOpts = null;
        private boolean execute = false;
        private boolean serviceCompression = false;
        private boolean backwardClosureOpt = false;
        private boolean preconditions = false;
        private boolean onlyGraph = false;
        private boolean naiveIO = false;
        private boolean combinedJN = false;
        private boolean listSubclasses = false;
        private boolean naiveTCG = false;
        private boolean dismissOutput = false;
        private File failPoints = null;
        private @Nonnull Algorithm algorithm;

        public Builder(@Nonnull File wscConvertedRoot, @Nonnull Algorithm algorithm) {
            this.wscConvertedRoot = wscConvertedRoot;
            this.algorithm = algorithm;
        }

        public Builder withWscConvertedRoot(@Nonnull File wscRoot) {
            this.wscConvertedRoot = wscRoot;
            return this;
        }
        public Builder withProblem(int problem) {
            this.problem = problem;
            return this;
        }
        public Builder withSubDir(String subDir) {
            this.subDir = subDir;
            return this;
        }
        public Builder withKnown(String known) {
            this.known = known;
            return this;
        }
        public Builder withWanted(String wanted) {
            this.wanted = wanted;
            return this;
        }
        public Builder withPreheatCount(int preheatCount) {
            this.preheatCount = preheatCount;
            return this;
        }
        public Builder withChildXms(String childXms) {
            this.childXms = childXms;
            return this;
        }
        public Builder withChildXmx(String childXmx) {
            this.childXmx = childXmx;
            return this;
        }
        public Builder withChildJVMOpts(String childJVMOpts) {
            this.childJVMOpts = childJVMOpts;
            return this;
        }
        public Builder withExecute(boolean execute) {
            this.execute = execute;
            return this;
        }
        public Builder withServiceCompression(boolean serviceCompression) {
            this.serviceCompression = serviceCompression;
            return this;
        }
        public Builder withBackwardClosureOpt(boolean backwardClosureOpt) {
            this.backwardClosureOpt = backwardClosureOpt;
            return this;
        }
        public Builder withPreconditions(boolean preconditions) {
            this.preconditions = preconditions;
            return this;
        }
        public Builder withOnlyGraph(boolean onlyGraph) {
            this.onlyGraph = onlyGraph;
            return this;
        }
        public Builder withNaiveIO(boolean naiveIO) {
            this.naiveIO = naiveIO;
            return this;
        }
        public Builder withCombinedJN(boolean combinedJN) {
            this.combinedJN = combinedJN;
            return this;
        }
        public Builder withListSubclasses(boolean listSubclasses) {
            this.listSubclasses = listSubclasses;
            return this;
        }
        public Builder withNaiveTCG(boolean naiveTCG) {
            this.naiveTCG = naiveTCG;
            return this;
        }
        public Builder withDismissOutput(boolean dismissOutput) {
            this.dismissOutput = dismissOutput;
            return this;
        }
        public Builder withFailPoints(File serviceFailures) {
            this.failPoints = serviceFailures;
            return this;
        }
        public Builder withAlgorithm(@Nonnull Algorithm algorithm) {
            this.algorithm = algorithm;
            return this;
        }

        public WscFactors build() {
            return new WscFactors(wscConvertedRoot, problem, subDir, known, wanted, preheatCount,
                    childXms, childXmx, childJVMOpts, execute, serviceCompression,
                    backwardClosureOpt, preconditions, onlyGraph, naiveIO, combinedJN,
                    listSubclasses, naiveTCG, dismissOutput, failPoints, algorithm);
        }
    }
}

package br.ufsc.lapesd.unserved.testbench.benchmarks.experiment;

public class ExperimentException extends Exception {
    public ExperimentException() {
    }

    public ExperimentException(String s) {
        super(s);
    }

    public ExperimentException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public ExperimentException(Throwable throwable) {
        super(throwable);
    }
}

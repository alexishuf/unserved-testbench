package br.ufsc.lapesd.unserved.testbench.io_composer.backward;

import br.ufsc.lapesd.unserved.testbench.io_composer.graph.CompositionWorkingGraphBuilder;
import br.ufsc.lapesd.unserved.testbench.io_composer.graph.GraphAccessor;
import br.ufsc.lapesd.unserved.testbench.io_composer.impl.IOComposerInput;
import br.ufsc.lapesd.unserved.testbench.io_composer.state.Action;
import br.ufsc.lapesd.unserved.testbench.io_composer.state.State;
import es.usc.citius.hipster.model.Transition;

import javax.annotation.Nonnull;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Queue;
import java.util.function.Function;

public class BackwardCompositionWorkingGraphFromAccessor implements CompositionWorkingGraphBuilder<State, Double, Action> {
    private Function<IOComposerInput, GraphAccessor<State, Double, Transition<Void, State>>> graphAccessorFactory = NaiveBackwardGraphAccessor::new;

    public void setGraphAccessorFactory(Function<IOComposerInput, GraphAccessor<State, Double, Transition<Void, State>>> graphAccessorFactory) {
        this.graphAccessorFactory = graphAccessorFactory;
    }

    @Override
    public Result<State, Double, Action> build(@Nonnull IOComposerInput composerInput) {
        GraphAccessor<State, Double, Transition<Void, State>> accessor;
        accessor = graphAccessorFactory.apply(composerInput);

        BackwardCompositionWorkingGraph g = new BackwardCompositionWorkingGraph();
        State initial = accessor.getInitial();
        g.add(initial);
        State goal = addClosure(g, initial, accessor);
        if (goal == null)
            goal = State.initial(Collections.emptySet()); // there will be no path
        return new Result<State, Double, Action>(g, initial, goal);
    }

    private State addClosure(@Nonnull BackwardCompositionWorkingGraph g, @Nonnull State initial,
                             @Nonnull GraphAccessor<State, Double, Transition<Void, State>> accessor) {
        Queue<State> queue = new LinkedList<>();
        queue.add(initial);
        State goal = null;
        while (!queue.isEmpty()) {
            State state = queue.remove();
            for (Transition<Void, State> trans : accessor.getTransitions(state)) {
                State next = trans.getState();
                if (!g.add(next)) continue;

                queue.add(next);
                if (accessor.isGoal(next)) {
                    assert goal == null || goal.equals(next);
                    goal = next;
                }

                double cost = next.getCostFromInitial() - state.getCostFromInitial();
                if (accessor.isGoal(next) && cost < 0) cost = 0;
                assert cost >= 0;
                g.connect(state, next, cost);
            }
        }
        return goal;
    }
}

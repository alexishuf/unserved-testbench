package br.ufsc.lapesd.unserved.testbench;

import com.google.common.reflect.ClassPath;
import org.apache.jena.graph.compose.MultiUnion;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 */
public class BackgroundUtils {
    public static Background get(String vocab) {
        List<Class<?>> list = new ArrayList<>();
        try {
            ClassLoader loader = BackgroundUtils.class.getClassLoader();
            list = ClassPath.from(loader)
                    .getTopLevelClassesRecursive("br.ufsc.lapesd.unserved")
                    .stream().map(ClassPath.ClassInfo::load).collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (Class<?> aClass : list) {
            BackgroundProvider annotation = aClass.getAnnotation(BackgroundProvider.class);
            if (annotation == null || !annotation.vocab().equals(vocab)) continue;
            if (!Background.class.isAssignableFrom(aClass)) continue;
            try {
                return (Background)aClass.newInstance();
            } catch (InstantiationException | IllegalAccessException ignored) {
            }
        }

        return null;
    }

    public static Model unionWithModel(Model model, Iterable<Background> backgrounds) {
        MultiUnion union = new MultiUnion();
        union.addGraph(model.getGraph());
        for (Background bg : backgrounds) union.addGraph(bg.getModel().getGraph());
        return ModelFactory.createModelForGraph(union);
    }
}

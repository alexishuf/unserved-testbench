package br.ufsc.lapesd.unserved.testbench.components.expressions;

import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.jena.rdf.model.Resource;

import java.util.Collections;
import java.util.Set;

public class AtomicClassExpression extends AbstractClassExpression {
    private final Resource resource;

    public AtomicClassExpression(Resource resource) {
        super(Type.Atomic, Collections.singleton(resource));
        this.resource = resource;
    }

    public Resource getResource() {
        return resource;
    }

    @Override
    public Set<ClassExpression> getChildren() {
        return Collections.emptySet();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getResource()).hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof AtomicClassExpression)) return false;
        AtomicClassExpression rhs = (AtomicClassExpression) o;
        return getResource().equals(rhs.getResource());
    }
}

package br.ufsc.lapesd.unserved.testbench.components;

import com.google.common.reflect.ClassPath;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

class DefaultComponentFactoryHelper {
    private static Logger logger = LoggerFactory.getLogger(DefaultComponentFactoryHelper.class);

    static void scan(ClassLoader classLoader, ComponentRegistry registry, String... packages) {
        List<Class<?>> classes = new ArrayList<>();
        try {
            for (String pkg : packages) getClasses(classLoader, classes, pkg);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        for (Class<?> aClass : classes) {
            if (!ComponentFactory.class.isAssignableFrom(aClass)) continue;
            if (!aClass.isAnnotationPresent(DefaultComponentFactory.class)) continue;
            @SuppressWarnings("unchecked") Class<? extends ComponentFactory> implementation =
                    (Class<? extends ComponentFactory>) aClass;
            try {
                registry.registerComponent(implementation);
            } catch (ComponentRegistrationException e) {
                logger.warn("Ignoring registration exception for {}", implementation.getName(), e);
            }
        }
    }

    static void scan(ClassLoader classLoader, ComponentRegistry registry) {
        scan(classLoader, registry, "br.ufsc.lapesd.unserved.testbench");
    }

    private static void getClasses(ClassLoader classLoader, List<Class<?>> output, String pkg)
            throws IOException {
        List<Class<?>> list = ClassPath.from(classLoader)
                .getTopLevelClassesRecursive(pkg)
                .stream().map(ClassPath.ClassInfo::load).collect(Collectors.toList());
        output.addAll(expandInnerClasses(list));
    }

    private static List<Class<?>> expandInnerClasses(List<Class<?>> classes) {
        List<Class<?>> list = new ArrayList<>(classes.size()*2);
        Queue<Class<?>> queue = new LinkedList<>();
        queue.addAll(classes);
        while (!queue.isEmpty()) {
            Class<?> aClass = queue.remove();
            list.add(aClass);
            Collections.addAll(queue, aClass.getDeclaredClasses());
        }
        return list;
    }
}

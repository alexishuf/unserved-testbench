package br.ufsc.lapesd.unserved.testbench.util;

import com.google.common.base.Preconditions;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;

import javax.annotation.Nonnull;

public class ResourceWithModel<T extends Resource> implements AutoCloseable {
    private final T resource;

    public ResourceWithModel(@Nonnull T resource) {
        this.resource = resource;
        Preconditions.checkNotNull(resource.getModel());
    }

    public T getResource() {
        return resource;
    }

    public Model getModel() {
        return resource.getModel();
    }

    public void close() {
        resource.getModel().close();
    }
}

package br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.lazy;

import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.IndexedNode;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.Node;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.cost.CostInfo;
import br.ufsc.lapesd.unserved.testbench.io_composer.state.MessagePairAction;
import br.ufsc.lapesd.unserved.testbench.model.Variable;

import javax.annotation.Nonnull;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

public class EmptySetNode  implements SetNode {
    private final int layer;
    EquivalenceSets empty;

    public EmptySetNode(int layer) {
        this.layer = layer;
    }

    public int getLayer() {
        return layer;
    }

    @Nonnull
    @Override
    public EquivalenceSets getEquivalenceSets() {
        return empty;
    }

    @Nonnull
    @Override
    public Set<IndexedNode> getNodes() {
        return Collections.emptySet();
    }

    @Nonnull
    @Override
    public Stream<Node> streamPlainNodes() {
        return Stream.empty();
    }

    @Nonnull
    @Override
    public List<MessagePairAction> getMessagePairs() {
        return Collections.emptyList();
    }

    @Nonnull
    @Override
    public Set<Variable> getInputs() {
        return Collections.emptySet();
    }

    @Nonnull
    @Override
    public Set<Variable> getOutputs() {
        return Collections.emptySet();
    }

    @Nonnull
    @Override
    public List<SetNode> getAlternatives() {
        return Collections.emptyList();
    }

    @Nonnull
    @Override
    public CostInfo getCostInfo() {
        return CostInfo.EMPTY;
    }

    @Override
    public String toString() {
        return String.format("EmptySetNode@%h(%d)", this, layer);
    }
}

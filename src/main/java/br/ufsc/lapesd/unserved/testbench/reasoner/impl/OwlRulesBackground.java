package br.ufsc.lapesd.unserved.testbench.reasoner.impl;

import br.ufsc.lapesd.unserved.testbench.input.RDFInput;
import br.ufsc.lapesd.unserved.testbench.util.ResourcesBackground;
import org.apache.commons.lang3.tuple.ImmutablePair;

import java.util.Collections;

public class OwlRulesBackground extends ResourcesBackground {
    public OwlRulesBackground() {
        super(Collections.singletonList(ImmutablePair.of("owl-rules.n3", RDFInput.N3)), true);
    }
}

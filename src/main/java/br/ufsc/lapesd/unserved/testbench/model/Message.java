package br.ufsc.lapesd.unserved.testbench.model;

import java.util.stream.Stream;

/**
 * Enhanced node for a unserved:Message
 */
public interface Message extends WithParts {
    Variable getFrom();
    Variable getTo();
    When getWhen();
    Stream<Message> getConsequent();
}

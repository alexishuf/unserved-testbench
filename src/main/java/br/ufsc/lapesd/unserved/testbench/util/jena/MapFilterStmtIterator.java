package br.ufsc.lapesd.unserved.testbench.util.jena;

import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.util.iterator.ExtendedIterator;
import org.apache.jena.util.iterator.MapFilter;
import org.apache.jena.util.iterator.MapFilterIterator;

import java.util.NoSuchElementException;

public class MapFilterStmtIterator<T> extends MapFilterIterator<T, Statement> implements StmtIterator {
    /**
     * Creates a sub-Iterator.
     *
     * @param fl An object is included if it is accepted by this Filter.
     * @param e  The parent Iterator.
     */
    public MapFilterStmtIterator(MapFilter<T, Statement> fl, ExtendedIterator<T> e) {
        super(fl, e);
    }

    @Override
    public Statement nextStatement() throws NoSuchElementException {
        return next();
    }
}

package br.ufsc.lapesd.unserved.testbench.httpcomponents;

import br.ufsc.lapesd.unserved.testbench.httpcomponents.extractors.*;
import br.ufsc.lapesd.unserved.testbench.iri.Testbench;
import br.ufsc.lapesd.unserved.testbench.model.Message;
import br.ufsc.lapesd.unserved.testbench.model.Part;
import br.ufsc.lapesd.unserved.testbench.model.Reaction;
import br.ufsc.lapesd.unserved.testbench.model.When;
import br.ufsc.lapesd.unserved.testbench.model.http.HttpResponseNode;
import br.ufsc.lapesd.unserved.testbench.process.Context;
import br.ufsc.lapesd.unserved.testbench.process.actions.ActionExecutionException;
import br.ufsc.lapesd.unserved.testbench.process.actions.ActionRunner;
import br.ufsc.lapesd.unserved.testbench.process.model.Receive;
import com.google.common.base.Preconditions;
import org.apache.http.HttpResponse;
import org.apache.jena.rdf.model.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class HttpClientReceiveRunner implements ActionRunner {
    private static Logger logger = LoggerFactory.getLogger(HttpClientReceiveRunner.class);

    public static class Factory extends HttpClientActionRunnerFactory {
        public Factory() {
            super(HttpClientReceiveRunner.class, HttpClientReceiveRunner.Factory.class,
                    "httpcomponents/receive-runner.ttl",
                    Testbench.PREFIX + "HttpClientReceiveRunner-Class");
        }
    }

    @Override
    public void run(Resource action, Context context) throws ActionExecutionException {
        Preconditions.checkArgument(action.canAs(Receive.class));
        Message msg = action.as(Receive.class).getMessage();

        When when = msg.getWhen();
        Preconditions.checkArgument(when.canAs(Reaction.class));
        Reaction reaction = when.as(Reaction.class);
        Message req = reaction.getReactionTo();

        Preconditions.checkArgument(msg.canAs(HttpResponseNode.class));
        HttpResponseNode resp = msg.as(HttpResponseNode.class);

        Collection<HttpClientSendReceiveContext> httpContexts;
        httpContexts = context.runners().getActionContexts(req,
                HttpClientSendReceiveContext.class);
        if (httpContexts.isEmpty()) {
            throw new ActionExecutionException(String.format("No HttpClientSendReceiveContext " +
                    "for %s - %s", req, resp));
        }
        if (httpContexts.size() > 1) {
            throw new ActionExecutionException(String.format(
                    "Multiple HttpClientSendReceiveContext instances for %s - %s", req, resp));
        }
        HttpClientSendReceiveContext httpContext = httpContexts.iterator().next();
        try {
            extract(httpContext.getHttpResponse(), msg, context);
        } catch (InterruptedException | ExecutionException e) {
            throw new ActionExecutionException(e);
        }
        try {
            httpContext.close();
        } catch (Exception e) {
            logger.error("Failed to close httpContext, ignoring.", e);
        }
    }

    private void extract(HttpResponse response, Message message, Context context)
            throws ActionExecutionException {
        HttpResponseExtractorRepository repository = getExtractorRepository(context);
        List<HttpResponseExtraction> extractions = new ArrayList<>();

        for (Part part : message.getParts()) {
            HttpResponseExtractor extractor;
            try {
                extractor = repository.getExtractor(part);
            } catch (IncompletePartException |NoExtractorException e) {
                logger.warn("No extractor for part {}. Ignoring.", part, e);
                continue;
            }
            try {
                extractions.add(extractor.extract(response, part, context));
            } catch (ExtractionException e) {
                throw new ActionExecutionException(String.format("Extractor %s failed to extract " +
                        "part %s from HttpResponse", extractor.getClass().getName(), part), e);
            }
        }

        extractions.forEach(e -> e.apply(context));
    }

    private HttpResponseExtractorRepository getExtractorRepository(Context context) {
        HttpResponseExtractorRepository extractorRepository;
        extractorRepository = (HttpResponseExtractorRepository)context.globalContext()
                .get(HttpResponseExtractorRepository.CONFIG_ID);
        if (extractorRepository == null)
            extractorRepository = DefaultHttpResponseExtractorRepository.getInstance();
        return extractorRepository;
    }
}

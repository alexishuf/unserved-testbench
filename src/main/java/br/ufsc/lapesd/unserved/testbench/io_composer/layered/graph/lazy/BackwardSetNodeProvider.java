package br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.lazy;

import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.LayeredServiceGraph;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.ProviderSet;

import javax.annotation.Nonnull;
import java.util.Set;

public interface BackwardSetNodeProvider {
    void setup(@Nonnull LayeredServiceGraph serviceGraph, @Nonnull SetNode start,
               @Nonnull SetNode end);
    /**
     * Create a list of distinct {@link SetNode} instances combining at most one
     * {@link ProviderSet} from each list in setLists. The implementation may apply optimizations
     * to reduce the number of combinations.
     *
     * @param successor Successor of all generated edges.
     * @return Edges from the created combinations to successor. May be empty.
     */
    @Nonnull
    Set<SetNodeEdge> getPredecessors(@Nonnull SetNode successor);
}

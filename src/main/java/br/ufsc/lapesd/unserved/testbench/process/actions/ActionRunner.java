package br.ufsc.lapesd.unserved.testbench.process.actions;

import br.ufsc.lapesd.unserved.testbench.components.Component;
import br.ufsc.lapesd.unserved.testbench.process.Context;
import org.apache.jena.rdf.model.Resource;

public interface ActionRunner extends Component {
    void run(Resource action, Context context) throws ActionExecutionException;
}

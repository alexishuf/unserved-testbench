package br.ufsc.lapesd.unserved.testbench.model.impl;

import br.ufsc.lapesd.unserved.testbench.iri.Unserved;
import br.ufsc.lapesd.unserved.testbench.model.Part;
import br.ufsc.lapesd.unserved.testbench.model.Variable;
import br.ufsc.lapesd.unserved.testbench.util.ImplementationByType;
import org.apache.jena.enhanced.EnhGraph;
import org.apache.jena.enhanced.EnhNode;
import org.apache.jena.enhanced.Implementation;
import org.apache.jena.graph.Node;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.impl.ResourceImpl;

import java.util.Arrays;

public class PartImpl extends ResourceImpl implements Part {
    public static Implementation factory = new ImplementationByType(Unserved.Part.asNode(),
            Arrays.asList(Unserved.variable.asNode(), Unserved.partModifier.asNode())) {
        @Override
        public EnhNode wrap(Node node, EnhGraph eg) {
            return new PartImpl(node, eg);
        }
    };

    public PartImpl(Node n, EnhGraph m) {
        super(n, m);
    }

    @Override
    public Variable getVariable() {
        Statement statement = getProperty(Unserved.variable);
        return statement == null ? null : statement.getResource().as(Variable.class);
    }

    @Override
    public Resource getPartModifier() {
        Statement statement = getProperty(Unserved.partModifier);
        return statement == null ? null : statement.getResource();
    }
}

package br.ufsc.lapesd.unserved.testbench.util;

import org.apache.jena.enhanced.EnhGraph;
import org.apache.jena.enhanced.Implementation;
import org.apache.jena.graph.Graph;
import org.apache.jena.graph.Node;
import org.apache.jena.vocabulary.RDF;

import java.util.Collections;
import java.util.List;

/**
 * Abstract Implementation that implements canWrap with a RDF.type check. Idea stolen
 * from SPIN 2.0 (org.topbraid.spin.util.ImplementationByType)
 */
public abstract class ImplementationByType extends Implementation {
    protected final Node type;
    protected final List<Node> predicates;

    protected ImplementationByType(Node type, List<Node> predicates) {
        this.type = type;
        this.predicates = predicates;
    }
    protected ImplementationByType(Node type) {
        this(type, Collections.emptyList());
    }

    @Override
    public boolean canWrap(Node node, EnhGraph eg) {
        Graph g = eg.asGraph();
        return g.contains(node, RDF.type.asNode(), type) || predicates.stream()
                .filter(p -> g.find(node, p, null).hasNext()).findAny().isPresent();
    }
}

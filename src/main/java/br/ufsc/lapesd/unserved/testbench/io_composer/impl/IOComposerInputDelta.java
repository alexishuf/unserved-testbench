package br.ufsc.lapesd.unserved.testbench.io_composer.impl;

import br.ufsc.lapesd.unserved.testbench.composer.ComposerException;
import br.ufsc.lapesd.unserved.testbench.composer.impl.AbstractComposerInput;
import br.ufsc.lapesd.unserved.testbench.input.RDFInput;
import br.ufsc.lapesd.unserved.testbench.model.Variable;
import br.ufsc.lapesd.unserved.testbench.process.data.DataContextDiff;
import br.ufsc.lapesd.unserved.testbench.process.data.SimpleDataContext;
import br.ufsc.lapesd.unserved.testbench.util.SkolemizerMap;
import br.ufsc.lapesd.unserved.testbench.util.jena.UncloseableGraph;
import org.apache.jena.graph.compose.Delta;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.reasoner.rulesys.Rule;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Set;

public class IOComposerInputDelta extends AbstractComposerInput implements IOComposerInput {
    private Delta delta;
    private Model model;

    public IOComposerInputDelta(@Nonnull IOComposerInput parent) {
        super(parent);
        delta = new Delta(new UncloseableGraph(parent.getSkolemizedUnion().getGraph()));
        model = ModelFactory.createModelForGraph(delta);
    }

    @Override
    public List<RDFInput> getInputs() {
        return getParent().getInputs();
    }

    @Override
    public void addInput(RDFInput input) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void addRulesFile(File file) {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public List<Rule> customRules() {
        return getParent().customRules();
    }

    @Nonnull
    @Override
    public Set<Resource> getWanted() {
        return getParent().getWanted();
    }

    @Nonnull
    @Override
    public Set<Resource> getKnown() {
        return getParent().getKnown();
    }

    private IOComposerInput getParent() {
        return (IOComposerInput)parent;
    }

    @Nonnull
    @Override
    public Set<Variable> getWantedAsVariables() {
        return getParent().getWantedAsVariables();
    }

    @Nonnull
    @Override
    public Set<Variable> getKnownAsVariables() {
        return getParent().getKnownAsVariables();
    }

    @Override
    public void addWanted(@Nonnull Resource resource) {
        throw new UnsupportedOperationException();
    }

    @Override
    @Nonnull
    public Model initSkolemizedUnion(boolean basicReasoning, boolean inputHasNoSkolemized) throws IOException {
        return getSkolemizedUnion();
    }

    @Override
    @Nonnull
    public Model getSkolemizedUnion() {
        return model;
    }

    public void apply(@Nonnull DataContextDiff diff) {
        Model model = ModelFactory.createModelForGraph(new UncloseableGraph(delta));
        try (SimpleDataContext dataContext = new SimpleDataContext(model,
                r -> {
                    Resource resolved = getSkolemizerMap().resolve(r);
                    return resolved == null ? r : resolved;
                })) {
            diff.apply(dataContext);
        }
    }

    @Override
    public SkolemizerMap getSkolemizerMap() {
        return getParent().getSkolemizerMap();
    }

    @Override
    protected void closeImpl() throws ComposerException {
        model.close();
    }
}

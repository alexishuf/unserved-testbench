package br.ufsc.lapesd.unserved.testbench.msm;

import br.ufsc.lapesd.unserved.testbench.util.ResourcesBackground;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;
import org.apache.jena.riot.RDFFormat;

import java.util.Collections;

public class MSMRules extends ResourcesBackground {
    public MSMRules() {
        super(Collections.singletonList(ImmutablePair.of("unserved/msm/rules.n3",
                RDFFormat.TURTLE)), true);
    }

    public static String IRI = "https://alexishuf.bitbucket.io/2016/04/unserved/msm/rules.n3";
    public static String PREFIX = IRI + "#";

    public static Resource Input = ResourceFactory.createResource(PREFIX + "Input");
}

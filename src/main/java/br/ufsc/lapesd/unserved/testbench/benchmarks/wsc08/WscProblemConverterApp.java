package br.ufsc.lapesd.unserved.testbench.benchmarks.wsc08;

import br.ufsc.lapesd.unserved.testbench.benchmarks.RandomNFPAssigner;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class WscProblemConverterApp {
    @Option(name = "--wsc-root", required = true)
    private File wscRoot = null;

    @Option(name = "--out-root", required = true)
    private File outRoot = null;

    @Option(name = "--nfp-assigner-cfg", usage = "A JSON file with a " +
            "RandomNFPAssigner.Config instance")
    private File nfpAssignerConfig = null;

    public static void main(String[] args) throws Exception {
        WscProblemConverterApp app = new WscProblemConverterApp();
        new CmdLineParser(app).parseArgument(args);
        app.run();
    }

    private void run() throws IOException {
        RandomNFPAssigner nfpAssigner = nfpAssignerConfig == null ? null
                : RandomNFPAssigner.fromFile(nfpAssignerConfig);
        String[] names = wscRoot.list();
        if (names == null) throw new IOException("Couldn't get dir list from " + wscRoot);
        List<File> dirs = Stream.of(names).filter(name -> name.matches("[0-9]+"))
                .map(name -> new File(wscRoot, name)).sorted().collect(Collectors.toList());
        for (File dir : dirs) {
            System.out.printf("Converting problem %s (out of %d)...\n", dir.getName(), dirs.size());
            File outDir = new File(outRoot, dir.getName());
            if (outDir.exists() && !outDir.isDirectory())
                throw new IOException(outDir.toString() + "exists but is not a directory");
            if ((!outDir.exists() && !outDir.mkdirs()))
                throw new IOException("Could not mkdir " + outDir.toString());
            WscProblemConverter.convert(dir, outDir, nfpAssigner);
        }
    }

}

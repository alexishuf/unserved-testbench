package br.ufsc.lapesd.unserved.testbench.httpcomponents.extractors;

import br.ufsc.lapesd.unserved.testbench.iri.HTTP;
import br.ufsc.lapesd.unserved.testbench.iri.UnservedX;
import br.ufsc.lapesd.unserved.testbench.model.Part;
import br.ufsc.lapesd.unserved.testbench.model.Variable;
import br.ufsc.lapesd.unserved.testbench.model.http.AsHttpProperty;
import br.ufsc.lapesd.unserved.testbench.process.Context;
import br.ufsc.lapesd.unserved.testbench.representations.DefaultRepresentationUpgrader;
import com.google.common.base.Preconditions;
import org.apache.commons.io.IOUtils;
import org.apache.http.HeaderElement;
import org.apache.http.HttpResponse;
import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;
import org.apache.jena.vocabulary.RDF;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Base64;

@ExtractsProperties({"http://www.w3.org/2011/http#body"})
class BodyExtractor implements HttpResponseExtractor {
    private static Logger logger = LoggerFactory.getLogger(BodyExtractor.class);

    public BodyExtractor() {
    }

    @Override
    public HttpResponseExtraction extract(HttpResponse response, Part part, Context context) throws ExtractionException {
        Preconditions.checkNotNull(part);
        Preconditions.checkArgument(part.getPartModifier().canAs(AsHttpProperty.class));
        AsHttpProperty partModifier = part.getPartModifier().as(AsHttpProperty.class);
        Preconditions.checkArgument(partModifier.getHttpProperty().equals(HTTP.body));

        if (partModifier.getModifier() != null)
            throw new UnsupportedOperationException("Modifiers are not supported");

        int statusCode = response.getStatusLine().getStatusCode();
        if (statusCode < 200 || statusCode >= 300)
            throw new ExtractionException("Not OK response code " + statusCode);
        if (response.getEntity().isChunked())
            throw new ExtractionException("Chunked transfer-encoding not supported.");

        HeaderElement[] elements = response.getEntity().getContentType().getElements();
        if (elements.length != 1)
            throw new ExtractionException("Multiple media types");

        String mediaType = elements[0].getName();
        //FIXME check if mediaTypes are compatible
        logger.warn("Representation compatibility not being checked");

        String base64 = getBodyBase64(response);
        return createExtraction(part.getVariable(), mediaType, base64);
    }

    private HttpResponseExtraction createExtraction(Variable variable,
                                                    String mediaType, String base64) {
        Model model = ModelFactory.createDefaultModel();

        Resource representation = model.createResource();
        model.add(representation, RDF.type, UnservedX.MediaType.MediaType);
        model.add(representation, UnservedX.MediaType.mediaTypeValue,
                ResourceFactory.createTypedLiteral(mediaType, XSDDatatype.XSDstring));
        DefaultRepresentationUpgrader.getInstance().upgrade(representation);

        return new SetVariableHttpResponseExtraction(model, variable, representation,
                ResourceFactory.createTypedLiteral(base64, XSDDatatype.XSDbase64Binary));
    }

    private String getBodyBase64(HttpResponse response) throws ExtractionException {
        String base64;
        try {
            byte[] bytes;
            bytes = IOUtils.toByteArray(response.getEntity().getContent());
            base64 = new String(Base64.getEncoder().encode(bytes));
        } catch (IOException e) {
            throw new ExtractionException("Problem getting HTTP entity content", e);
        }
        return base64;
    }
}

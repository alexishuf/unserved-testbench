package br.ufsc.lapesd.unserved.testbench.pragproof.context.impl;

import br.ufsc.lapesd.unserved.testbench.input.NonClosingRDFInput;
import br.ufsc.lapesd.unserved.testbench.input.RDFInput;
import br.ufsc.lapesd.unserved.testbench.input.RDFInputModel;
import br.ufsc.lapesd.unserved.testbench.iri.Unserved;
import br.ufsc.lapesd.unserved.testbench.pragproof.context.PragProofContext;
import br.ufsc.lapesd.unserved.testbench.pragproof.context.PragProofQuery;
import br.ufsc.lapesd.unserved.testbench.pragproof.context.PragProofRules;
import br.ufsc.lapesd.unserved.testbench.pragproof.context.PragProofRulesFactory;
import br.ufsc.lapesd.unserved.testbench.pragproof.model.PPR;
import br.ufsc.lapesd.unserved.testbench.process.data.DataContext;
import br.ufsc.lapesd.unserved.testbench.process.data.UpdatableRDFInputsDataContext;
import br.ufsc.lapesd.unserved.testbench.reasoner.impl.OwlRulesBackground;
import br.ufsc.lapesd.unserved.testbench.util.Skolemizer;
import br.ufsc.lapesd.unserved.testbench.util.SkolemizerMap;
import br.ufsc.lapesd.unserved.testbench.util.UpdatableRDFInputs;
import com.google.common.base.Preconditions;
import org.apache.jena.rdf.model.*;
import org.apache.jena.riot.RDFFormat;
import org.apache.jena.vocabulary.RDF;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Stream.concat;


public class DefaultPragProofContext implements PragProofContext {
    private Logger logger = LoggerFactory.getLogger(DefaultPragProofContext.class);

    private boolean closed = false;
    private PragProofRules rules;
    private final PragProofQuery query;
    private final DataContext dataContext;
    private final Unserved unservedBackground = new Unserved();
    private final OwlRulesBackground owlRules = new OwlRulesBackground();
    private final PPR dfRules = new PPR();
    private final Map<Resource, Resource> skolemizedWantedVariables;


    public static class Builder implements AutoCloseable {
        private List<RDFInput> inputs = new ArrayList<>();
        private List<Resource> wantedVariables = new ArrayList<>();
        private boolean closed = false;
        private PragProofRulesFactory rulesFactory = new DefaultPragProofRulesFactory();

        public Builder addInput(RDFInput input) {
            inputs.add(input);
            return this;
        }

        public Builder addInputs(Collection<RDFInput> inputs) {
            this.inputs.addAll(inputs);
            return this;
        }

        public Builder setRulesFactory(PragProofRulesFactory rulesFactory) {
            this.rulesFactory = rulesFactory;
            return this;
        }

        public Builder markWantedVariable(Resource variable) {
            wantedVariables.add(variable);
            return this;
        }

        public Builder markWantedVariables(Collection<Resource> variables) {
            wantedVariables.addAll(variables);
            return this;
        }


        public DefaultPragProofContext build() throws IOException {
            Preconditions.checkState(!closed, "Builder is closed");

            HashMap<Resource, Resource> skolemizedWantedVars = new HashMap<>();
            Model skolemizedInput = skolemizeInputs(inputs, wantedVariables, skolemizedWantedVars);
            PragProofRules rules = rulesFactory.createRules(new NonClosingRDFInput(new RDFInputModel(skolemizedInput, RDFFormat.TURTLE)));

            Model variablesModel = ModelFactory.createDefaultModel();
            splitVariables(skolemizedInput, variablesModel);
            UpdatableRDFInputs updatable = new UpdatableRDFInputs(
                    Arrays.asList(skolemizedInput, variablesModel), RDFFormat.TURTLE);
            UpdatableRDFInputsDataContext dataContext;
            dataContext = new UpdatableRDFInputsDataContext(updatable);

            PragProofQuery query = PragProofWantedVariablesQuery.createQuery(wantedVariables);

            closed = true;
            return new DefaultPragProofContext(rules, query, dataContext, skolemizedWantedVars);
        }

        private void splitVariables(Model model, Model variables) {
            //move ?var ?p ?o to variables
            ResIterator it = model.listSubjectsWithProperty(RDF.type, Unserved.Variable);
            while (it.hasNext()) {
                model.listStatements(it.next(), null, (RDFNode) null)
                        .forEachRemaining(variables::add);
            }
            variables.listStatements().forEachRemaining(model::remove);
        }

        private Model skolemizeInputs(List<RDFInput> inputs,
                                               Collection<Resource> resourcesOfInterest,
                                               Map<Resource, Resource> skolemizedNames)
                throws IOException {
            Model union = ModelFactory.createDefaultModel();
            Skolemizer skolemizer = new Skolemizer();

            try {
                List<Model> models = new ArrayList<>();
                for (RDFInput input : inputs) models.add(input.getModel());

                SkolemizerMap map = skolemizer.skolemizeModels(union, models);
                for (Resource r : resourcesOfInterest) skolemizedNames.put(r, map.get(r));
            } finally {
                for (RDFInput input : inputs) {
                    input.close();
                }
            }

            return union;
        }

        @Override
        public void close() throws Exception {
            if (closed) return;
            closed = true;
            for (RDFInput input : inputs) input.close();
        }
    }

    public DefaultPragProofContext(PragProofRules rules, PragProofQuery queryFile,
                                   DataContext dataContext,
                                   Map<Resource, Resource> skolemizedWantedVariables) {
        this.rules = rules;
        this.query= queryFile;
        this.dataContext = dataContext;
        this.skolemizedWantedVariables = skolemizedWantedVariables;
    }

    @Override
    public List<RDFInput> getAllInputs() {
        return concat(rules.getInputs().stream(),
                concat(owlRules.getInputs().stream(),
                  concat(unservedBackground.getInputs().stream(),
                   concat(dfRules.getInputs().stream(),
                          dataContext.getInputs().stream()
               )))).collect(Collectors.toList());
    }

    @Override
    public PragProofRules getRules() {
        return rules;
    }

    @Override
    public PragProofQuery getQuery() {
        return query;
    }

    @Nonnull
    @Override
    public Collection<Resource> getInputWantedVariables() {
        return skolemizedWantedVariables.keySet();
    }

    @Override
    public Resource getWantedVariable(Resource inputResource) {
        Resource skolemized = skolemizedWantedVariables.get(inputResource);
        if (skolemized == null) {
            throw new NoSuchElementException(inputResource.toString()
                    + " is not known as an wanted variable");
        }
        assert !skolemized.isAnon();
        return dataContext.getUnmodifiableModel().createResource(skolemized.getURI());
    }

    @Nonnull
    @Override
    public DataContext getDataContext() {
        return dataContext;
    }

    @Nonnull
    @Override
    public PragProofContext createNonIsolatedCopy() {
        return new DefaultPragProofContext(rules.createNonIsolatedCopy(),
                query.createNonIsolatedCopy(), dataContext.createNonIsolatedCopy(),
                skolemizedWantedVariables);
    }

    @Override
    public void close() {
        if (closed) return;
        closed = true;

        try {
            rules.close();
            query.close();
            dataContext.close();
            unservedBackground.close();
            owlRules.close();
            dfRules.close();
        } catch (Exception e) {
            logger.error("Ops", e);
        }
    }
}

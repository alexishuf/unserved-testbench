package br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.cost;

import br.ufsc.lapesd.unserved.testbench.iri.UnservedN;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.vocabulary.RDF;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.List;

import static br.ufsc.lapesd.unserved.testbench.iri.Unserved.nfp;

public class CostInfoParser {
    @Nonnull
    public CostInfo parse(@Nonnull Resource resource, boolean strict) {
        List<CostParameter> parameters = new ArrayList<>();
        for (StmtIterator it = resource.listProperties(nfp); it.hasNext(); ) {
            Resource nfp = it.next().getResource();
            if (nfp.hasProperty(RDF.type, UnservedN.ResponseTime)) {
                parameters.add(new ResponseTime(getDoubleValue(nfp)));
            } else if (nfp.hasProperty(RDF.type, UnservedN.Throughput)) {
                parameters.add(new Throughput(getDoubleValue(nfp)));
            } else if (strict) {
                throw new IllegalArgumentException(String.format("Resource %s has a unknown " +
                        "NFP. rdf:types %s", resource,
                        nfp.listProperties(RDF.type).toList().toString()));
            }
        }
        return parameters.isEmpty() ? CostInfo.EMPTY : new CostInfo(parameters);
    }

    @Nonnull
    public CostInfo parse(@Nonnull Resource resource) {
        return parse(resource, false);
    }

    @Nonnull
    public CostInfo parseStrictly(@Nonnull Resource resource) {
        return parse(resource, true);
    }

    private double getDoubleValue(Resource nfp) {
        return Double.parseDouble(nfp.getRequiredProperty(UnservedN.value).getLiteral().getLexicalForm());
    }
}

package br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.List;

public abstract class AbstractNode implements Node {
    private final @Nonnull List<Node> alternatives;

    protected AbstractNode(@Nonnull List<Node> alternatives) {
        this.alternatives = alternatives;
    }

    protected AbstractNode(@Nonnull AbstractNode other) {
        alternatives = new ArrayList<>(other.alternatives);
    }
    protected AbstractNode(@Nonnull AbstractNode other, @Nonnull List<Node> alternatives) {
        this.alternatives = alternatives;
    }

    @Nonnull
    public List<Node> getAlternatives() {
        return alternatives;
    }
}

package br.ufsc.lapesd.unserved.testbench;

import br.ufsc.lapesd.unserved.testbench.benchmarks.restdesc.RESTdescDescriptionGenerator;
import br.ufsc.lapesd.unserved.testbench.io_composer.IOComposerBuilder;
import br.ufsc.lapesd.unserved.testbench.io_composer.backward.*;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.BackwardAStarLayeredIOComposer;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.DStarLiteLayeredIOComposer;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.GreedyBackwardLayeredIOComposer;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.LayeredServiceSetGraphIOComposer;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.lazy.NonEquivalentBackwardSetNodeProvider;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.lazy.SingleLeastRTBackwardSetNodeProvider;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.lazy.SingleLeastServicesBackwardSetNodeProvider;

public enum Algorithm {
    BackwardAStar {
        @Override
        public IOComposerBuilder createIOComposerBuilder() {
            return new BackwardIOAStar.Builder();
        }
    },
    OutputIndexedBackwardAStar {
        @Override
        public IOComposerBuilder createIOComposerBuilder() {
            return new BackwardIOAStar.Builder()
                    .setAccessorFactory(OutputIndexedBackwardGraphAccessor::new);
        }
    },
    OutputIndexedReplicatedBackwardAStar {
        @Override
        public IOComposerBuilder createIOComposerBuilder() {
            return new ReplicatedBackwardIOAStar.Builder()
                    .setAccessorFactory(OutputIndexedBackwardGraphAccessor::new);
        }
    },
    FromOutputIndexedReplicatedBackwardIOADStar {
        @Override
        public IOComposerBuilder createIOComposerBuilder() {
            BackwardCompositionWorkingGraphFromAccessor gb;
            gb = new BackwardCompositionWorkingGraphFromAccessor();
            gb.setGraphAccessorFactory(OutputIndexedBackwardGraphAccessor::new);
            return new ReplicatedBackwardIOADStar.Builder().setGraphBuilder(gb);
        }
    },
    BackwardAStarLayeredIO {
        @Override
        public IOComposerBuilder createIOComposerBuilder() {
            return new BackwardAStarLayeredIOComposer.Builder();
        }
    },
    BackwardAStarLayeredIORT {
        @Override
        public IOComposerBuilder createIOComposerBuilder() {
            return new BackwardAStarLayeredIOComposer.Builder()
                    .withCost(new LayeredServiceSetGraphIOComposer.RTCost(100));
        }
    },
    DStarLiteLayeredIO {
        @Override
        public IOComposerBuilder createIOComposerBuilder() {
            return new DStarLiteLayeredIOComposer.Builder();
        }
    },
    DStarLiteLayeredIORT {
        @Override
        public IOComposerBuilder createIOComposerBuilder() {
            return new DStarLiteLayeredIOComposer.Builder()
                    .withCost(new LayeredServiceSetGraphIOComposer.RTCost(100));
        }
    },
    GreedyBackwardLayeredIO {
        @Override
        public IOComposerBuilder createIOComposerBuilder() {
            return new GreedyBackwardLayeredIOComposer.Builder().
                    setBackwardSetNodeProvider(new NonEquivalentBackwardSetNodeProvider());
        }
    },
    GreedyBackwardLayeredIONoAdapt {
        @Override
        public IOComposerBuilder createIOComposerBuilder() {
            return new GreedyBackwardLayeredIOComposer.Builder()
                    .setBackwardSetNodeProvider(new SingleLeastServicesBackwardSetNodeProvider());
        }
    },
    GreedyBackwardLayeredIORT {
        @Override
        public IOComposerBuilder createIOComposerBuilder() {
            return new GreedyBackwardLayeredIOComposer.Builder()
                    .withCost(new LayeredServiceSetGraphIOComposer.RTCost(100))
                    .setBackwardSetNodeProvider(new NonEquivalentBackwardSetNodeProvider());
        }
    },
    GreedyBackwardLayeredIORTNoAdapt {
        @Override
        public IOComposerBuilder createIOComposerBuilder() {
            return new GreedyBackwardLayeredIOComposer.Builder()
                    .withCost(new LayeredServiceSetGraphIOComposer.RTCost(100))
                    .setBackwardSetNodeProvider(new SingleLeastRTBackwardSetNodeProvider());
        }
    },
    RESTdescPP,
    /**
     * RESTdescPragProof with setCheatUnstableReasoner(false)
     */
    RESTdescPPWithUnstableReasoner,
    /**
     * Same algorithm as RESTdesc, however uses --use-existentials option of
     * {@link RESTdescDescriptionGenerator}.
     */
    RESTdescPPExistentials;

    public IOComposerBuilder createIOComposerBuilder() {
        throw new UnsupportedOperationException();
    }

    public boolean isReplicated() {
        switch (this) {
            case BackwardAStar:
            case OutputIndexedBackwardAStar:
                return false;
            case OutputIndexedReplicatedBackwardAStar:
            case FromOutputIndexedReplicatedBackwardIOADStar:
            case BackwardAStarLayeredIO:
            case DStarLiteLayeredIO:
            case GreedyBackwardLayeredIO:
            case GreedyBackwardLayeredIONoAdapt:
            case GreedyBackwardLayeredIORT:
            case GreedyBackwardLayeredIORTNoAdapt:
            case RESTdescPP:
                return true;
        }
        throw new UnsupportedOperationException();
    }

    public AlgorithmFamily getFamily() {
        switch (this) {
            case BackwardAStar:
            case OutputIndexedBackwardAStar:
            case OutputIndexedReplicatedBackwardAStar:
            case FromOutputIndexedReplicatedBackwardIOADStar:
            case BackwardAStarLayeredIO:
            case BackwardAStarLayeredIORT:
            case DStarLiteLayeredIO:
            case DStarLiteLayeredIORT:
            case GreedyBackwardLayeredIO:
            case GreedyBackwardLayeredIORT:
            case GreedyBackwardLayeredIORTNoAdapt:
            case GreedyBackwardLayeredIONoAdapt:
                return AlgorithmFamily.IOGraphPath;
            case RESTdescPP:
            case RESTdescPPWithUnstableReasoner:
            case RESTdescPPExistentials:
                return AlgorithmFamily.RESTdescPP;
            default:
                throw new UnsupportedOperationException();
        }
    }


}

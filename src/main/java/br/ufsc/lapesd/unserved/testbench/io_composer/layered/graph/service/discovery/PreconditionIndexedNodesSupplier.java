package br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.discovery;

import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.*;
import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.atomic.TriplePattern;
import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.atomic.VariableSpec;
import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.index.TransitiveIndex;
import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.index.UnservedTriplePatternIndex;
import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.index.VariableSpecIndex;
import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.parsers.SimpleSPINConditionsParser;
import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.parsers.VariableSpecParser;
import br.ufsc.lapesd.unserved.testbench.io_composer.impl.IOComposerInput;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.EndNode;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.Node;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.ServiceNode;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.StartNode;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.cost.CostInfo;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.cost.CostInfoParser;
import br.ufsc.lapesd.unserved.testbench.iri.Unserved;
import br.ufsc.lapesd.unserved.testbench.model.Message;
import br.ufsc.lapesd.unserved.testbench.model.Variable;
import br.ufsc.lapesd.unserved.testbench.util.DangerousArrayListSet;
import br.ufsc.lapesd.unserved.testbench.util.FixedCacheTransitiveClosureGetter;
import br.ufsc.lapesd.unserved.testbench.util.TransitiveClosureGetter;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.vocabulary.RDFS;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Maintains a index of services by their pre-{@link Condition}s.
 */
public class PreconditionIndexedNodesSupplier implements NodesSupplier<PreconditionsLayersState> {
    private static final VariableSpecParser specsParser = new VariableSpecParser();
    private static final SimpleSPINConditionsParser tpParser = new SimpleSPINConditionsParser();
    private static final CostInfoParser costInfoParser = new CostInfoParser();
    private VariableSpecIndex<MessagePair> vsIndex = null;
    private UnservedTriplePatternIndex<MessagePair> tpIndex = null;
    private TransitiveClosureGetter superClassGetter;

    public PreconditionIndexedNodesSupplier() {
        superClassGetter = null;
    }

    public PreconditionIndexedNodesSupplier(@Nonnull TransitiveClosureGetter superClassGetter) {
        this.superClassGetter = superClassGetter;
    }

    @Override
    public boolean isActive() {
        return tpIndex != null;
    }

    @Override
    public void begin(IOComposerInput composerInput,
                      @Nonnull StartNode startNode, @Nonnull EndNode endNode) {
        assert !isActive();
        Model m = composerInput.getSkolemizedUnion();
        if (superClassGetter == null) {
            superClassGetter = FixedCacheTransitiveClosureGetter.forward(RDFS.subClassOf)
                    .visitAll(m).build();
        }
        vsIndex = new VariableSpecIndex<>(superClassGetter);
        tpIndex = new UnservedTriplePatternIndex<>(
                () -> new VariableSpecIndex<>(superClassGetter),
                () -> new TransitiveIndex<>(superClassGetter),
                () -> new TransitiveIndex<>(superClassGetter.createFor(RDFS.subPropertyOf, false)));

        m.listSubjectsWithProperty(Unserved.reactionTo).forEachRemaining(when -> {
            Message ant = when.getPropertyResourceValue(Unserved.reactionTo).as(Message.class);
            DangerousArrayListSet<Variable> vars = new DangerousArrayListSet<>();
            List<VariableSpec> specs = new ArrayList<>();
            specsParser.parseLists(vars::add, specs::add, ant);
            And preconditions = tpParser.parse(ant);
            m.listSubjectsWithProperty(Unserved.when, when).toList().stream()
                    .map(r -> r.as(Message.class)).forEach(cons ->
            {
                MessagePair mp = new MessagePair(ant, cons, vars, specs, preconditions);
                specs.forEach(spec -> vsIndex.put(spec, mp));
                ConditionTraverser.visit(preconditions, new ConditionVisitor() {
                    @Override
                    public boolean visitTriplePattern(@Nonnull TriplePattern cond) {
                        tpIndex.put(cond, mp);
                        return true;
                    }
                });
            });
        });

    }

    @Override
    public void end() {
        vsIndex = null;
        tpIndex = null;
    }

    @Nonnull
    @Override
    public Set<Node> getNodes(@Nonnull PreconditionsLayersState state,
                              @Nonnull Set<Message> blacklist) {
        ConditionEvaluator vsEvaluator = state.getVariableSpecsEvaluator();
        Set<Node> set = state.getNewlyKnown().stream().flatMap(s -> vsIndex.get(s).stream()).distinct()
                .filter(mp -> !blacklist.contains(mp.consequent) && mp.verify(vsEvaluator))
                .map(mp -> (Node) mp.createServiceNode())
                .collect(Collectors.toSet());
        state.advance(set);
        return set;
    }

    private static final class MessagePair {
        private final @Nonnull Message antecedent;
        private final @Nonnull Message consequent;
        private final @Nonnull Set<Variable> inputVariables;
        private final @Nonnull List<VariableSpec> inputVariableSpecs;
        private final @Nonnull And preconditions;

        public MessagePair(@Nonnull Message antecedent, @Nonnull Message consequent,
                           @Nonnull Set<Variable> inputVariables,
                           @Nonnull List<VariableSpec> inputVariableSpecs,
                           @Nonnull And preconditions) {
            this.antecedent = antecedent;
            this.consequent = consequent;
            this.inputVariables = inputVariables;
            this.inputVariableSpecs = inputVariableSpecs;
            this.preconditions = preconditions;
        }

        public ServiceNode createServiceNode() {
            CostInfo costInfo = costInfoParser.parse(consequent);
            return new ServiceNode(antecedent, consequent, new ArrayList<>(),
                    preconditions, null, costInfo, inputVariables);
        }

        public boolean verify(ConditionEvaluator vsEvaluator) {
            for (VariableSpec vs : inputVariableSpecs) {
                if (!vsEvaluator.evaluate(vs)) return false;
            }
            return true;
        }

        @Override
        public String toString() {
            return "MessagePair(" + antecedent + " -> " + consequent + ")";
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            MessagePair that = (MessagePair) o;
            return Objects.equals(antecedent, that.antecedent) &&
                    Objects.equals(consequent, that.consequent) &&
                    Objects.equals(inputVariableSpecs, that.inputVariableSpecs) &&
                    Objects.equals(preconditions, that.preconditions);
        }

        @Override
        public int hashCode() {
            return Objects.hash(antecedent, consequent, inputVariableSpecs, preconditions);
        }
    }
}

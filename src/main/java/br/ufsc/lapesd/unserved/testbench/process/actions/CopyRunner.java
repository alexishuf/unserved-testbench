package br.ufsc.lapesd.unserved.testbench.process.actions;

import br.ufsc.lapesd.unserved.testbench.components.DefaultComponentFactory;
import br.ufsc.lapesd.unserved.testbench.components.SimpleActionRunnerFactory;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.IndexedVariable;
import br.ufsc.lapesd.unserved.testbench.iri.UnservedP;
import br.ufsc.lapesd.unserved.testbench.model.Variable;
import br.ufsc.lapesd.unserved.testbench.process.Context;
import br.ufsc.lapesd.unserved.testbench.process.model.Copy;
import com.google.common.base.Preconditions;
import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CopyRunner implements ActionRunner {
    private static Logger logger = LoggerFactory.getLogger(CopyRunner.class);

    @DefaultComponentFactory
    public static class Factory extends SimpleActionRunnerFactory {
        public Factory() {
            super(CopyRunner.class, CopyRunner.Factory.class,
                    new UnservedP().getModel().createResource(UnservedP.Copy.getURI()));
        }
    }


    @Override
    public void run(Resource resource, Context c) throws ActionExecutionException {
        Preconditions.checkArgument(resource.canAs(Copy.class));
        Copy copy = resource.as(Copy.class);
        Variable rawTo = c.data().getResource(copy.getTo(), Variable.class);
        Variable rawFrom = c.data().getResource(copy.getFrom(), Variable.class);
        Preconditions.checkArgument(rawFrom != null && rawTo != null);
        IndexedVariable from = new IndexedVariable(rawFrom, copy.getFromIndex());
        IndexedVariable to = new IndexedVariable(rawTo, copy.getToIndex());
        if (from.equals(to)) {
            logger.warn("Ignoring recursive Copying of {}.", from);
            return;
        }

        c.data().unsetVariable(to);
        Resource valueRepresentation = from.getValueRepresentation();
        Resource resourceValue = from.getResourceValue();
        if (resourceValue != null) {
            c.data().setVariable(to, valueRepresentation, resourceValue);
            logger.debug("Copied a resource from {} to {}", from, to);
        }
        Literal literalValue = from.getLiteralValue();
        if (literalValue != null && resourceValue == null) {
            c.data().setVariable(to, valueRepresentation, literalValue);
            logger.debug("Copied {} from {} to {}", literalValue, from, to);
        } else if (literalValue != null) {
            logger.warn("{} has both a literal resource values. Will copy only the resource.", from);
        } else if (resourceValue == null) {
            logger.debug("Copied unset variable {} to {}", from, to);
        }
    }
}

package br.ufsc.lapesd.unserved.testbench.composer;

import br.ufsc.lapesd.unserved.testbench.input.RDFInput;
import br.ufsc.lapesd.unserved.testbench.io_composer.IOComposer;

import javax.annotation.Nonnull;
import java.util.Collection;

public interface ComposerBuilder extends AutoCloseable {
    /**
     * Adds RDF Input to the algorithm. Ownership is given to the Builder, which may
     * transfer it to the built {@link IOComposer}.
     *
     * @param input RDF Input
     * @return the builder
     */
    @Nonnull
    ComposerBuilder addInput(@Nonnull RDFInput input);

    @Nonnull
    default ComposerBuilder addInputs(Collection<RDFInput> inputs) {
        inputs.forEach(this::addInput);
        return this;
    }

    /**
     * Create a {@link IOComposer} object. This will <code>close()</code> the builder.
     *
     * @return a newly created composer
     */
    Composer build() throws ComposerBuilderException;

    /**
     * Releases all resources held by the builder.
     */
    void close() throws ComposerBuilderException;
}

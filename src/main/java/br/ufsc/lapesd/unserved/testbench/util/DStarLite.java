package br.ufsc.lapesd.unserved.testbench.util;

import com.google.common.base.Preconditions;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.*;
import java.util.function.BiFunction;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * Implementation of the D*-Lite algorithm [1]. More specifically, the "Final version" (Figuire 5).
 * In addition to the paper there is a technical report [2].
 *
 * [1]: Koenig, S., & Likhachev, M. (2002). Improved fast replanning for robot navigation in unknown terrain. Robotics and Automation, 2002. Proceedings. ICRA ’02. IEEE International Conference on. https://smartech.gatech.edu/bitstream/handle/1853/21546/icra2002-replanning.pdf
 * [2]: Koenig, S., & Likhachev, M. (n.d.). Improved Fast Replanning for Robot Navigation in Unknown Terrain. http://www.cs.cmu.edu/afs/cs/Web/People/motionplanning/papers/sbp_papers/integrated3/koenig_dstarlite_techreport.pdf
 */
public class DStarLite<State, Cost> {
    @Nonnull private final Algebra<Cost> costAlgebra;
    @Nonnull private final Cost infinite;
    @Nonnull private final GraphAccessor<State, Cost> graphAccessor;
    @Nonnull private final Supplier<PriorityQueue<Info>> queueSupplier;
    @Nonnull private final PriorityQueue<Info> open;
//    @Nonnull private final HashSet<State> closed = new HashSet<>();
    @Nonnull private Cost kM;
    @Nonnull private State goal;
    private State start;
    private boolean multiResults = true;
    @Nonnull private final Map<State, Info> nodeMap = new HashMap<>();
//    @Nonnull private final Set<State> notUpdated = new HashSet<>();
    @Nonnull private final BiFunction<State, State, Cost> h;
    @Nonnull private final Set<State> changed = new HashSet<>();
    private Result<State> result = null;
    private boolean running = false, initialized = false;
//    private HashMap<State, Integer> stateAbbrv = new HashMap<>();

    public static BuilderWithCost<Double> withDoubleCost() {
        return createBuilder(new Algebra<Double>() {
            @Override
            public Double zero() {
                return 0.0;
            }
            @Override
            public Double infinite() {
                return Double.POSITIVE_INFINITY;
            }
            @Override
            public int compare(Double left, Double right) {
                return Double.compare(left, right);
            }
            @Override
            public Double add(Double left, Double right) {
                return left + right;
            }
        });
    }

    public static <Cost> BuilderWithCost<Cost>
    createBuilder(@Nonnull Algebra<Cost> costAlgebra) {
        return new BuilderWithCost<>(costAlgebra);
    }

    public DStarLite(@Nonnull Algebra<Cost> costAlgebra,
                     @Nonnull GraphAccessor<State, Cost> graphAccessor,
                     @Nonnull Supplier<PriorityQueue<Info>> queueSupplier,
                     @Nonnull State goal,
                     @Nonnull BiFunction<State, State, Cost> h) {
        Preconditions.checkNotNull(costAlgebra);
        Preconditions.checkNotNull(graphAccessor);
        Preconditions.checkNotNull(queueSupplier);
        this.queueSupplier = queueSupplier;
        this.costAlgebra = costAlgebra;
        this.infinite = costAlgebra.infinite();
        this.graphAccessor = graphAccessor;
        this.open = queueSupplier.get();
        Preconditions.checkArgument(this.open != null);
        this.goal = goal;
        this.h = h;
        this.kM = costAlgebra.zero();
    }

    /**
     * Duplicates the D* Lite algorithm to operate on another graph which is isomorphic to the
     * graph accessed through this instance's accessor.
     *
     * This operation has no side effect on this object. And no operation on the returned
     * duplicate will affect this instance.
     *
     * @param accessor {@link GraphAccessor} to the new graph.
     * @param h Heuristic function for the new graph. This must be consistent with this instance
     *          own heuristic. That is, <code>h.apply(translator.apply(x), translator.apply(y))
     *          == this.h.apply(x, y)</code> for any x, y. This condition is not verified.
     *
     * @throws IllegalStateException if called reenters or called concurrently with
     *                               <code>runFrom()</code>
     * @throws IllegalArgumentException if <code>translator</code> returns a null for a non-null
     *                                  argument, or if it returns the same state for two
     *                                  different states of this algorithm's graph.
     *
     * @return A duplicate of this D* Lite algorithm, at the same state, but which will perform
     * further search on the new graph.
     */
    public DStarLite<State, Cost> duplicateTo(@Nonnull GraphAccessor<State, Cost> accessor,
                                              @Nonnull BiFunction<State, State, Cost> h) {
        Preconditions.checkState(!running);
        DStarLite<State, Cost> dup = new DStarLite<>(costAlgebra, accessor, queueSupplier, goal, h);
        dup.start = start;
        dup.kM = kM;
        dup.multiResults = multiResults;

        for (Map.Entry<State, Info> e : nodeMap.entrySet()) {
            Info i = e.getValue();
            assert e.getKey().equals(i.state);
            List<State> best = i.best == null ? null : new ArrayList<>(i.best);
            dup.nodeMap.put(e.getKey(), dup.newInfo(e.getKey(), i.g, i.rhs, i.key, best));
        }
        for (Info info : open) dup.open.add(dup.getInfo(info.state));
//        dup.closed.addAll(closed.stream().map(t).collect(Collectors.toList()));
        dup.changed.addAll(changed);
        if (result != null) {
            dup.result = new Result<>(result.getPaths().stream()
                    .map(ArrayList::new)
                    .collect(Collectors.toList()));
        }
        dup.initialized = initialized;
//        dup.notUpdated.addAll(notUpdated.stream().map(translator).collect(Collectors.toSet()));
//        stateAbbrv.entrySet().stream()
//                .map(e -> ImmutablePair.of(translator.apply(e.getKey()), e.getValue()))
//                .forEach(p -> dup.stateAbbrv.put(p.getKey(), p.getValue()));


        return dup;
    }

    public void setMultiResults(boolean multiResults) {
        this.multiResults = multiResults;
    }

    public boolean getMultiResults() {
        return multiResults;
    }

    public DStarLite<State, Cost> runFrom(State start) {
        try {
            running = true;
            State lastStart = this.start != null ? this.start : start;
            this.start = start;
            if (!initialized) {
                open.add(getInfo(goal).setRhs(costAlgebra.zero(), null).updateKey());
                initialized = true;
            } else {
                kM = costAlgebra.add(kM, h.apply(lastStart, start));
//                Stopwatch watch = Stopwatch.createStarted();
                changed.forEach(this::updateVertex);
//                System.err.printf("updates: %.3fms\n", watch.elapsed(TimeUnit.MICROSECONDS)/1000.0);
            }
            changed.clear();
//            Stopwatch watch = Stopwatch.createStarted();
            computeShortestPath();
//            System.err.printf("path: %.3fms\n", watch.elapsed(TimeUnit.MICROSECONDS)/1000.0);
//            watch.reset().start();
            result = createResult(multiResults);
//            System.err.printf("result: %.3fms\n", watch.elapsed(TimeUnit.MICROSECONDS)/1000.0);
            return this;
        } finally {
            running = false;
        }
    }

    /**
     * Notifies that for each state in the collection, at least one edge leaving it was changed,
     * added or removed from the graph. This method cannot be called concurrently with or
     * re-entered from runFrom().
     *
     * @param collection set of sources of edges which where changed, added or removed.
     * @throws IllegalStateException if runFrom() is running.
     * @return The own {@link DStarLite} object.
     */
    public DStarLite<State, Cost> notifyChangedEdgeSources(Collection<State> collection) {
        Preconditions.checkState(!running, "Changing the graph while searching is not supported!");
        changed.addAll(collection);
        return this;
    }

//    /**
//     * Notify, that predecessor, now has additional successor nodes. This nodes will be observed
//     * on subsequent <code>successor()</code> (and <code>predecessor()</code>) calls on the
//     * accessor, even if this notification is ignored.
//     *
//     * A {@link Supplier} is used to deliver the new nodes to allow lazy computation (as not all
//     * notifications may interest the {@link DStarLite} object).
//     *
//     * Concurrent access to this method will cause undefined behaviour as {@link DStarLite} is
//     * not thread-safe. Re-entrant calls are encouraged. If the graph is changing as it is
//     * accessed (for example a lazy graph constructed backwards), this will be more efficient than
//     * a tight loop with <code>notifyChangedEdgeSources()</code>.
//     *
//     * This MUST either be called reentrantly from <code>runFrom()</code>, or the next
//     * <code>runFrom()</code> must use the same start node as the previous. If this is not the
//     * case, consider <code>notifyChangedEdgeSources()</code>. Failure to obey this will yield
//     * incorrect results
//     *
//     * @param predecessor Node with new successors
//     * @param newSuccessorsSupplier A supplier to the collection of newly added successors
//     * @return <code>this</code>
//     */
//    public DStarLite<State, Cost>
//    notifyNewSuccessors(@Nonnull State predecessor,
//                        @Nonnull Supplier<Collection<State>> newSuccessorsSupplier) {
//        Info info = getInfo(predecessor);
//        if (isSuccessorsReentrantChangeMeaningful(info)) {
//            /* compute rhs among the new successors, defaulting to info.rhs */
//            Cost rhs = Stream.concat(Stream.of(info.rhs), newSuccessorsSupplier.get().stream()
//                    .map(this::getInfo).map(si
//                            -> costAlgebra.add(graphAccessor.cost(predecessor, si.state), si.g)))
//                    .min(costAlgebra::compare).orElse(info.rhs);
//
//            /* if we got a better rhs, use it */
//            if (costAlgebra.compare(rhs, info.rhs) < 0)
//                updateVertexLowerHalf(info);
//        }
//        return this;
//    }
//
//    /**
//     * Notify that some successors of <code>predecessor</code> have ceased to be so. This change
//     * will be observed in future <code>successors()</code> (and <code>predecessor()</code>)
//     * calls on the accessor, even if this notification is ignored.
//     *
//     * The same remarks about lazy computation, thread-unsafety and reentrancy made on
//     * <code>notifyNewSuccessors()</code> apply to this method as well.
//     *
//     * This MUST either be called reentrantly from <code>runFrom()</code>, or the next
//     * <code>runFrom()</code> must use the same start node as the previous. If this is not the
//     * case, consider <code>notifyChangedEdgeSources()</code>. Failure to obey this will
//     * yield incorrect results
//     *
//     * @param predecessor Node with remoed successors
//     * @param removedSuccessorsSupplier A supplier to the collection of removed successors.
//     * @return <code>this</code>
//     */
//    public DStarLite<State, Cost>
//    notifyRemovedSuccessors(@Nonnull State predecessor,
//                            @Nonnull Supplier<Collection<State>> removedSuccessorsSupplier) {
//        Info info = getInfo(predecessor);
//        if (isSuccessorsReentrantChangeMeaningful(info))
//            updateVertex(info); /* For removed successors there is no partial update */
//        return this;
//    }

    public Result<State> getResult() {
        return result;
    }

    /* --- private methods */

    private Info newInfo(State state, Cost g, Cost rhs, Key key, List<State> best) {
        return new Info(state, g, rhs, key, best);
    }

    private Info getInfo(State state) {
        Info info = nodeMap.getOrDefault(state, null);
        if (info == null) {
            info = new Info(state);
//            notUpdated.add(state);
            nodeMap.put(state, info);
//            stateAbbrv.put(state, stateAbbrv.size()+1);
        }
        return info;
    }

//    /**
//     * @return <code>predecessor</code> iff a reentrant change on the successors of
//     *         <code>predecessor</code> should be acted upon.
//     */
//    private boolean isSuccessorsReentrantChangeMeaningful(Info predecessor) {
//        return predecessor.state != goal //goal's successors have no use
//                   /* did we knew the node existed? */
//                && (closed.contains(predecessor.state) || open.contains(predecessor))
//                   /* If no updateVertex() yet, wait for that instead of patching rhs */
//                && !notUpdated.contains(predecessor.state);
//    }

    private Cost min(Cost a, Cost b) {
        return costAlgebra.compare(a, b) > 0 ? b : a;
    }

    private Key calculateKey(Info info) {
        Preconditions.checkState(start != null);
        Cost m = min(info.g, info.rhs);
        return new Key(costAlgebra.add(costAlgebra.add(m, h.apply(start, info.state)), kM), m);
    }

    private void updateVertex(State u) {
        Info info = getInfo(u);
//        notUpdated.remove(u);
        if (u != goal) {
            Cost min = infinite;
            List<State> best = new ArrayList<>();
            for (State s : graphAccessor.successors(u)) {
                Info sInfo = getInfo(s);
                Cost cost = costAlgebra.add(graphAccessor.cost(u, sInfo.state), sInfo.g);
                int cmp =  costAlgebra.compare(cost, min);
                if (cmp < 0) {
                    min = cost;
                    best.clear();
                }
                if (cmp <= 0) best.add(s);
            }
            info.setRhs(min, best);
        }

        open.remove(info);
//        closed.remove(info.state);
        if (!info.isConsistent())
            open.add(info.updateKey());
//        else
//            closed.add(info.state);
    }

    private void computeShortestPath() {
        Info sti = getInfo(start);
        while (!open.isEmpty()
                && (open.peek().key.compareTo(calculateKey(sti)) < 0 || !sti.isConsistent())) {
            Info uInfo = open.remove();
            State u = uInfo.state;
            open.remove(uInfo);
//            assert !closed.contains(u);
            Key oldKey = uInfo.key;
            Key uKey = calculateKey(uInfo);
            if (oldKey.compareTo(uKey) < 0) {
                open.add(uInfo.setKey(uKey));
            } else {
//                closed.add(u);
                if (uInfo.isOverConsistent()) {
                    uInfo.g = uInfo.rhs;
                    for (State p : graphAccessor.predecessors(u)) updateVertex(p);
                } else {
                    uInfo.g = infinite;
                    for (State p : graphAccessor.predecessors(u)) updateVertex(p);
                    updateVertex(u);
                }
            }
        }
    }

    private Result<State> createResult(boolean multiResults) {
        Preconditions.checkState(start != null);
        if (costAlgebra.compare(getInfo(start).g, infinite) == 0)
            return new Result<>(Collections.emptyList());
        List<List<State>> paths = new ArrayList<>();

        if (multiResults) {
            Stack<Step> stack = new Stack<>();
            stack.push(new Step(start, null));

            while (!stack.isEmpty()) {
                Step step = stack.pop();
                if (step.state.equals(goal)) {
                    LinkedList<State> path = new LinkedList<>();
                    for (; step != null; step = step.previous) path.addFirst(step.state);
                    paths.add(path);
                } else {
                    Info info = getInfo(step.state);
                    for (State next : info.best) {
                        if (step.isLoop(next)) continue;
                        stack.push(new Step(next, step));
                    }
                }
            }
        } else {
            List<State> path = new ArrayList<>();
            for (State s = start; !s.equals(goal); s = getInfo(s).best.get(0))
                path.add(s);
            path.add(goal);
            paths.add(path);
        }

        return new Result<>(paths);
    }

    /* --- Inner classes: Builders, Node, Key, Algebra, ... */

    public interface GraphAccessor<State, Cost> {
        Collection<State> successors(@Nonnull State state);
        Collection<State> predecessors(@Nonnull State state);
        Cost cost(@Nonnull State predecessor, @Nonnull State successor);
    }

    public interface Algebra<Cost> {
        Cost zero();
        Cost infinite();
        int compare(Cost left, Cost right);
        Cost add(Cost left, Cost right);
    }

    public static final class Result<State> {
        @Nonnull private final Collection<List<State>> paths;

        private Result(@Nonnull Collection<List<State>> paths) {
            this.paths = Collections.unmodifiableCollection(paths);
        }

        public boolean hasPath() {
            return !paths.isEmpty();
        }
        @Nonnull
        public Collection<List<State>> getPaths() {
            return paths;
        }

        /**
         * Get a path.
         *
         * @throws NoSuchElementException if <code>hasPath() == false</code>.
         */
        @Nonnull
        public List<State> getPath() {
            if (!hasPath()) throw new NoSuchElementException();
            return paths.iterator().next();
        }
    }

    public static final class BuilderWithCost<Cost> {
        private Algebra<Cost> costAlgebra;

        private BuilderWithCost(Algebra<Cost> costAlgebra) {
            this.costAlgebra = costAlgebra;
        }

        public <State> BuilderWithState<Cost, State> withGraphAccessor(
                @Nonnull DStarLite.GraphAccessor<State, Cost> graphAccessor) {
            return new BuilderWithState<>(costAlgebra, null, graphAccessor);
        }
    }

    public static final class BuilderWithState<Cost, State> {
        private Algebra<Cost> costAlgebra;
        private Supplier<PriorityQueue<DStarLite<State, Cost>.Info>> supplier;
        private DStarLite.GraphAccessor<State, Cost> graphAccessor;
        private State goal;
        private BiFunction<State, State, Cost> h;

        private BuilderWithState(Algebra<Cost> costAlgebra,
                                 Supplier<PriorityQueue<DStarLite<State, Cost>.Info>> supplier,
                                 DStarLite.GraphAccessor<State, Cost> graphAccessor) {
            this.costAlgebra = costAlgebra;
            this.supplier = supplier == null ? PriorityQueue::new : supplier;
            this.graphAccessor = graphAccessor;
        }

        public BuilderWithState<Cost, State> withSupplier(
                Supplier<PriorityQueue<DStarLite<State, Cost>.Info>> supplier) {
            this.supplier = supplier;
            return this;
        }
        public BuilderWithState<Cost, State> withGraphAccessor(GraphAccessor<State, Cost> graphAccessor) {
            this.graphAccessor = graphAccessor;
            return this;
        }
        public BuilderWithState<Cost, State> withGoal(State goal) {
            this.goal = goal;
            return this;
        }
        public BuilderWithState<Cost, State> withHeuristic(BiFunction<State, State, Cost> h) {
            this.h = h;
            return this;
        }

        public DStarLite<State, Cost> build() {
            return new DStarLite<>(costAlgebra, graphAccessor, supplier, goal, h);
        }
    }

    public final class Info implements Comparable<Info> {
        @Nonnull State state;
        @Nonnull private Cost g;
        @Nonnull private Cost rhs;
        @Nonnull private Key key;
        private List<State> best;


        private Info(@Nonnull State state) {
            this(state, infinite, infinite,
                    new Key(infinite, infinite), null);
        }

        private Info(@Nonnull State state, @Nonnull Cost g, @Nonnull Cost rhs, @Nonnull Key key,
                     @Nullable List<State> best) {
            this.state = state;
            this.g = g;
            this.rhs = rhs;
            this.key = key;
            this.best = best;
        }

        private Info setG(@Nonnull Cost g) { this.g = g; return this; }
        private Info setRhs(@Nonnull Cost rhs, @Nullable List<State> best) {
            this.best = best;
            this.rhs = rhs;
            return this;
        }
        private Info setKey(@Nonnull Key key) { this.key = key; return this; }
        private Info updateKey() { key = calculateKey(this); return this; }

        private boolean      isConsistent() {
            return costAlgebra.compare(g, rhs) == 0;
        }
        private boolean  isOverConsistent()  {
            return costAlgebra.compare(g, rhs) >  0;
        }

        @Override
        public String toString() {
            return String.format("Info(%s, g=%s, rhs=%s, key=%s)", state, g, rhs, key);
        }


        @Override
        public int hashCode() {
            int result = state.hashCode();
            result = 31 * result + g.hashCode();
            result = 31 * result + rhs.hashCode();
            result = 31 * result + key.hashCode();
            result = 31 * result + (best != null ? best.hashCode() : 0);
            return result;
        }

//        @Override
//        public int hashCode() {
//            return new HashCodeBuilder().append(g).append(rhs).append(key).hashCode();
//        }

        @SuppressWarnings("EqualsWhichDoesntCheckParameterClass")
        @Override
        public boolean equals(Object o) {
            if (o == this) return true;
            @SuppressWarnings("unchecked") Info other = (Info) o;
            return state.equals(other.state) && key.equals(other.key) && g.equals(other.g)
                    && rhs.equals(other.rhs);
        }

        @Override
        public int compareTo(@Nonnull Info other) {
            int cmp = key.compareTo(other.key);
            if (cmp != 0) return cmp;
            cmp = costAlgebra.compare(rhs, other.rhs);
            if (cmp != 0) return cmp;
            cmp = costAlgebra.compare(g, other.g);
            return cmp;
        }
    }

    private final class Key implements Comparable<Key> {
        Cost k1, k2;

        Key(Cost k1, Cost k2) {
            this.k1 = k1;
            this.k2 = k2;
        }

        @Override
        public String toString() {
            return String.format("[%s,%s]", k1, k2);
        }

        @Override
        public int hashCode() {
            return 31 * k1.hashCode() + k2.hashCode();
        }

        @SuppressWarnings("EqualsWhichDoesntCheckParameterClass")
        @Override
        public boolean equals(Object o) {
            @SuppressWarnings("unchecked") Key other = (Key) o;
            return k1.equals(other.k1) && k2.equals(other.k2);
        }

        @Override
        public int compareTo(@Nonnull Key other) {
            int cmp = costAlgebra.compare(k1, other.k1);
            if (cmp == 0)
                cmp = costAlgebra.compare(k2, other.k2);
            return cmp;
        }
    }

    private final class Step {
        private final @Nonnull State state;
        private final Step previous;

        public Step(@Nonnull State state, @Nullable Step previous) {
            this.state = state;
            this.previous = previous;
        }

        @Override
        public String toString() {
            return String.format("Step@%h(%s, %s)", this, state, previous == null ? "null"
                            : "Step@" + Integer.toHexString(previous.hashCode()));
        }

        private boolean isLoop(State state) {
            for (Step s = this; s != null; s = s.previous) {
                if (s.state.equals(state)) return true;
            }
            return false;
        }
    }
}

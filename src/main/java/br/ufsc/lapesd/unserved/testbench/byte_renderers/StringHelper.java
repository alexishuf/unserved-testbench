package br.ufsc.lapesd.unserved.testbench.byte_renderers;

import br.ufsc.lapesd.unserved.testbench.iri.Content;
import br.ufsc.lapesd.unserved.testbench.iri.UnservedX;
import br.ufsc.lapesd.unserved.testbench.util.GeneralizedContentType;
import com.google.common.base.Preconditions;
import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.rdf.model.*;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.XSD;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Collection;

class StringHelper {
    private static Logger logger = LoggerFactory.getLogger(StringHelper.class);

    private static Collection<Resource> supportedClasses = Arrays.asList(
            Content.ContentAsText,
            Content.ContentAsBase64
    );
    public static Collection<Resource> getSupportedClasses() {
        return supportedClasses;
    }

    private static Collection<Resource> supportedDataTypes;
    public static Collection<Resource> getSupportedDataTypes() {
        if (supportedDataTypes == null) {
            supportedDataTypes = new ArrayList<>();
            Class<XSDDatatype> clazz = XSDDatatype.class;
            for (Field field : clazz.getFields()) {
                if (!Modifier.isStatic(field.getModifiers())) continue;
                if (!XSDDatatype.class.isAssignableFrom(field.getType())) continue;
                try {
                    XSDDatatype value = (XSDDatatype) field.get(null);
                    supportedDataTypes.add(ResourceFactory.createResource(value.getURI()));
                } catch (IllegalAccessException ignored) { }
            }
        }
        return supportedDataTypes;
    }

    public static  @Nullable Charset getCharsetFromRepresentation(@Nonnull Resource representation) {
        Statement statement = representation.getProperty(UnservedX.MediaType.mediaTypeValue);
        Charset charset = null;
        if (statement != null && statement.getObject().isLiteral()) {
            String lexicalForm = statement.getLiteral().getLexicalForm();
            GeneralizedContentType contentType = GeneralizedContentType.parse(lexicalForm);
            if (contentType == null) {
                logger.warn("Invalid ux-mt:mediaTypeValue: {}", lexicalForm);
            } else if (!contentType.isConcrete()) {
                logger.warn("Non-concrete MT {}", contentType);
            } else {
                String charsetName = contentType.getElements()[0].getParameter("charset");
                if (charsetName == null) charsetName = "UTF-8";
                try {
                    charset = Charset.forName(charsetName);
                } catch (Exception e) {
                    logger.error("Bad charset in ux-mt:mediaTypeValue {}.", lexicalForm, e);
                }
            }
        }
        return charset;
    }

    static Render getRender(@Nonnull RDFNode node, @Nullable Charset charset) {
        Preconditions.checkNotNull(node);
        if (node.isLiteral()) {
            return getRender(node.asLiteral(), charset);
        } else if (node.isResource()) {
            return getRender(node.asResource(), charset);
        }
        throw new UnsupportedOperationException("node is neither Literal nor Resource");
    }

    private static Render getRender(@Nonnull Resource res, @Nullable Charset charset) {
        Preconditions.checkNotNull(res);
        if (res.hasProperty(RDF.type, Content.ContentAsText)) {
            Statement statement = res.getProperty(Content.chars);
            if (statement == null || !statement.getObject().isLiteral()) return null;
            Literal literal = statement.getLiteral();
            return SimpleRender.fromString(literal.getLexicalForm(), Charset.forName("UTF-8"));
        }
        if (res.hasProperty(RDF.type, Content.ContentAsBase64)) {
            Statement statement = res.getProperty(Content.bytes);
            if (statement == null || !statement.getObject().isLiteral()) return null;
            String base64 = statement.getLiteral().getLexicalForm();
            byte[] bytes = Base64.getDecoder().decode(base64);

            statement = res.getProperty(Content.characterEncoding);
            Charset actualCharset = null;
            if (statement != null && statement.getObject().isLiteral()) {
                String charsetName = statement.getLiteral().getLexicalForm();
                try {
                    actualCharset = Charset.forName(charsetName);
                } catch (Exception e) {
                    logger.error("Bad charset {} from cnt:characterEncoding", charsetName, e);
                }
            }
            if (actualCharset != null && charset != null && !actualCharset.equals(charset)) {
                logger.warn("Charset {} from cnt:characterEncoding takes precedence " +
                        "over charset={}", actualCharset, charset);
            } else if (actualCharset == null && charset != null) {
                actualCharset = charset;
            } else {
                logger.warn("Assuming UTF-8 charset on Base64 decoding");
                actualCharset = Charset.forName("UTF-8");
            }
            return new SimpleRender(bytes, Render.Type.STRING, actualCharset);
        }
        if (res.hasProperty(RDF.type, Content.ContentAsXML)) {
            throw new UnsupportedOperationException("Not implemented, FIXME");
        }
        return null;
    }

    private static Render getRender(@Nonnull Literal literal, @Nullable Charset charset) {
        Preconditions.checkNotNull(literal);
        if (literal.getDatatype().equals(XSDDatatype.XSDbase64Binary)) {
            byte[] bytes = Base64.getDecoder().decode(literal.getLexicalForm());
            if (charset == null) {
                logger.warn("No charset given to decode xsd:base64Binary into a String. " +
                        "Assuming UTF-8");
                charset = Charset.forName("UTF-8");
            }
            return new SimpleRender(bytes, Render.Type.STRING, charset);
        }
        if (literal.getDatatype().equals(XSDDatatype.XSDhexBinary)) {
            throw new UnsupportedOperationException("Not implemented, FIXME");
        }
        if (literal.getDatatype().getURI().startsWith(XSD.getURI())) {
            return SimpleRender.fromString(literal.getLexicalForm(), Charset.forName("UTF-8"));
        }
        return null;
    }

}

package br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service;

import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.Condition;

import javax.annotation.Nonnull;
import java.util.Objects;

public class NodePostCond {
    public final @Nonnull Node node;
    public final @Nonnull Condition postCond;

    public NodePostCond(@Nonnull Node node, @Nonnull Condition postCond) {
        this.node = node;
        this.postCond = postCond;
    }

    @Nonnull
    public Node getNode() {
        return node;
    }

    @Nonnull
    public Condition getPostCond() {
        return postCond;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        NodePostCond that = (NodePostCond) o;
        return Objects.equals(node, that.node) &&
                Objects.equals(postCond, that.postCond);
    }

    @Override
    public int hashCode() {

        return Objects.hash(node, postCond);
    }
}

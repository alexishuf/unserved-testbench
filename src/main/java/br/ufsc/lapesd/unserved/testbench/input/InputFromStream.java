package br.ufsc.lapesd.unserved.testbench.input;

import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;

public class InputFromStream extends AbstractInput  {
    private final InputStream inputStream;
    private File tempFile;

    public InputFromStream(String mediaType, InputStream inputStream) {
        super(mediaType);
        this.inputStream = inputStream;
    }

    @Override
    public File toFile() throws IOException {
        if (tempFile != null) return tempFile;
        tempFile = Files.createTempFile("unserved-testbench", "").toFile();
        tempFile.deleteOnExit();
        try (FileOutputStream out = new FileOutputStream(tempFile)) {
            IOUtils.copy(inputStream, out);
        } finally {
            inputStream.close();
        }
        return tempFile;
    }

    @Override
    public void close() throws IOException {
        tempFile.delete();
    }

    @Override
    public String toString() {
        return String.format("InputFromStream(%s, %s)", getMediaType(), inputStream.toString());
    }
}

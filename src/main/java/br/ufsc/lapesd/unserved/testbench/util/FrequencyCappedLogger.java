package br.ufsc.lapesd.unserved.testbench.util;

import com.google.common.base.Stopwatch;
import org.slf4j.Logger;
import org.slf4j.Marker;

import javax.annotation.Nonnull;
import java.util.concurrent.TimeUnit;

public class FrequencyCappedLogger implements Logger {
    private final @Nonnull Logger delegate;
    private final long silenceMs;
    private final @Nonnull Stopwatch stopwatch;
    public long max = Long.MAX_VALUE;

    public FrequencyCappedLogger(@Nonnull Logger delegate) {
        this(delegate, 15000);
    }

    public FrequencyCappedLogger(@Nonnull Logger delegate, long silenceMs) {
        this.delegate = delegate;
        this.silenceMs = silenceMs;
        this.stopwatch = Stopwatch.createStarted();
    }

    public FrequencyCappedLogger setMax(long max) {
        this.max = max;
        return this;
    }
    public String progress(long current) {
        if (max == Long.MAX_VALUE)
            return String.valueOf(current);
        return String.format("%d/%d (%.2f)", current, max, 100*current/(double)max);
    }
    public String p(long current) {
        return progress(current);
    }

    public @Nonnull
    FrequencyCappedLogger reset() {
        stopwatch.reset();
        return this;
    }

    private boolean shouldLog() {
        if (!stopwatch.isRunning()) {
            stopwatch.start();
            return true;
        } else {
            if (stopwatch.elapsed(TimeUnit.MILLISECONDS) > silenceMs) {
                stopwatch.reset().start();
                return true;
            }
        }
        return false;
    }


    @Override
    public String getName() {
        return delegate.getName();
    }

    @Override
    public boolean isTraceEnabled() {
        return delegate.isTraceEnabled();
    }

    @Override
    public void trace(String s) {
        if (shouldLog()) delegate.trace(s);
    }

    @Override
    public void trace(String s, Object o) {
        if (shouldLog()) delegate.trace(s, o);
    }

    @Override
    public void trace(String s, Object o, Object o1) {
        if (shouldLog()) delegate.trace(s, o, o1);
    }

    @Override
    public void trace(String s, Object... objects) {
        if (shouldLog()) delegate.trace(s, objects);
    }

    @Override
    public void trace(String s, Throwable throwable) {
        if (shouldLog()) delegate.trace(s, throwable);
    }

    @Override
    public boolean isTraceEnabled(Marker marker) {
        return delegate.isTraceEnabled(marker);
    }

    @Override
    public void trace(Marker marker, String s) {
        if (shouldLog()) delegate.trace(marker, s);
    }

    @Override
    public void trace(Marker marker, String s, Object o) {
        if (shouldLog()) delegate.trace(marker, s, o);
    }

    @Override
    public void trace(Marker marker, String s, Object o, Object o1) {
        if (shouldLog()) delegate.trace(marker, s, o, o1);
    }

    @Override
    public void trace(Marker marker, String s, Object... objects) {
        if (shouldLog()) delegate.trace(marker, s, objects);
    }

    @Override
    public void trace(Marker marker, String s, Throwable throwable) {
        if (shouldLog()) delegate.trace(marker, s, throwable);
    }

    @Override
    public boolean isDebugEnabled() {
        return delegate.isDebugEnabled();
    }

    @Override
    public void debug(String s) {
        if (shouldLog()) delegate.debug(s);
    }

    @Override
    public void debug(String s, Object o) {
        if (shouldLog()) delegate.debug(s, o);
    }

    @Override
    public void debug(String s, Object o, Object o1) {
        if (shouldLog()) delegate.debug(s, o, o1);
    }

    @Override
    public void debug(String s, Object... objects) {
        if (shouldLog()) delegate.debug(s, objects);
    }

    @Override
    public void debug(String s, Throwable throwable) {
        if (shouldLog()) delegate.debug(s, throwable);
    }

    @Override
    public boolean isDebugEnabled(Marker marker) {
        return delegate.isDebugEnabled(marker);
    }

    @Override
    public void debug(Marker marker, String s) {
        if (shouldLog()) delegate.debug(marker, s);
    }

    @Override
    public void debug(Marker marker, String s, Object o) {
        if (shouldLog()) delegate.debug(marker, s, o);
    }

    @Override
    public void debug(Marker marker, String s, Object o, Object o1) {
        if (shouldLog()) delegate.debug(marker, s, o, o1);
    }

    @Override
    public void debug(Marker marker, String s, Object... objects) {
        if (shouldLog()) delegate.debug(marker, s, objects);
    }

    @Override
    public void debug(Marker marker, String s, Throwable throwable) {
        if (shouldLog()) delegate.debug(marker, s, throwable);
    }

    @Override
    public boolean isInfoEnabled() {
        return delegate.isInfoEnabled();
    }

    @Override
    public void info(String s) {
        if (shouldLog()) delegate.info(s);
    }

    @Override
    public void info(String s, Object o) {
        if (shouldLog()) delegate.info(s, o);
    }

    @Override
    public void info(String s, Object o, Object o1) {
        if (shouldLog()) delegate.info(s, o, o1);
    }

    @Override
    public void info(String s, Object... objects) {
        if (shouldLog()) delegate.info(s, objects);
    }

    @Override
    public void info(String s, Throwable throwable) {
        if (shouldLog()) delegate.info(s, throwable);
    }

    @Override
    public boolean isInfoEnabled(Marker marker) {
        return delegate.isInfoEnabled(marker);
    }

    @Override
    public void info(Marker marker, String s) {
        if (shouldLog()) delegate.info(marker, s);
    }

    @Override
    public void info(Marker marker, String s, Object o) {
        if (shouldLog()) delegate.info(marker, s, o);
    }

    @Override
    public void info(Marker marker, String s, Object o, Object o1) {
        if (shouldLog()) delegate.info(marker, s, o, o1);
    }

    @Override
    public void info(Marker marker, String s, Object... objects) {
        if (shouldLog()) delegate.info(marker, s, objects);
    }

    @Override
    public void info(Marker marker, String s, Throwable throwable) {
        if (shouldLog()) delegate.info(marker, s, throwable);
    }

    @Override
    public boolean isWarnEnabled() {
        return delegate.isWarnEnabled();
    }

    @Override
    public void warn(String s) {
        if (shouldLog()) delegate.warn(s);
    }

    @Override
    public void warn(String s, Object o) {
        if (shouldLog()) delegate.warn(s, o);
    }

    @Override
    public void warn(String s, Object... objects) {
        if (shouldLog()) delegate.warn(s, objects);
    }

    @Override
    public void warn(String s, Object o, Object o1) {
        if (shouldLog()) delegate.warn(s, o, o1);
    }

    @Override
    public void warn(String s, Throwable throwable) {
        if (shouldLog()) delegate.warn(s, throwable);
    }

    @Override
    public boolean isWarnEnabled(Marker marker) {
        return delegate.isWarnEnabled(marker);
    }

    @Override
    public void warn(Marker marker, String s) {
        if (shouldLog()) delegate.warn(marker, s);
    }

    @Override
    public void warn(Marker marker, String s, Object o) {
        if (shouldLog()) delegate.warn(marker, s, o);
    }

    @Override
    public void warn(Marker marker, String s, Object o, Object o1) {
        if (shouldLog()) delegate.warn(marker, s, o, o1);
    }

    @Override
    public void warn(Marker marker, String s, Object... objects) {
        if (shouldLog()) delegate.warn(marker, s, objects);
    }

    @Override
    public void warn(Marker marker, String s, Throwable throwable) {
        if (shouldLog()) delegate.warn(marker, s, throwable);
    }

    @Override
    public boolean isErrorEnabled() {
        return delegate.isErrorEnabled();
    }

    @Override
    public void error(String s) {
        if (shouldLog()) delegate.error(s);
    }

    @Override
    public void error(String s, Object o) {
        if (shouldLog()) delegate.error(s, o);
    }

    @Override
    public void error(String s, Object o, Object o1) {
        if (shouldLog()) delegate.error(s, o, o1);
    }

    @Override
    public void error(String s, Object... objects) {
        if (shouldLog()) delegate.error(s, objects);
    }

    @Override
    public void error(String s, Throwable throwable) {
        if (shouldLog()) delegate.error(s, throwable);
    }

    @Override
    public boolean isErrorEnabled(Marker marker) {
        return delegate.isErrorEnabled(marker);
    }

    @Override
    public void error(Marker marker, String s) {
        if (shouldLog()) delegate.error(marker, s);
    }

    @Override
    public void error(Marker marker, String s, Object o) {
        if (shouldLog()) delegate.error(marker, s, o);
    }

    @Override
    public void error(Marker marker, String s, Object o, Object o1) {
        if (shouldLog()) delegate.error(marker, s, o, o1);
    }

    @Override
    public void error(Marker marker, String s, Object... objects) {
        if (shouldLog()) delegate.error(marker, s, objects);
    }

    @Override
    public void error(Marker marker, String s, Throwable throwable) {
        if (shouldLog()) delegate.error(marker, s, throwable);
    }
}

package br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.lazy;

import com.google.common.base.Preconditions;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.function.BiFunction;

public class EdgeAlternatives {
    private final @Nonnull SetNode from;
    private final @Nonnull SetNode to;
    private Iterator<BiFunction<SetNode, SetNode, Iterator<SetNodeEdge>>> levelsIterator;
    private LinkedHashSet<SetNodeEdge> instantiated = new LinkedHashSet<>();
    private Iterator<SetNodeEdge> delegate = null;

    public EdgeAlternatives(@Nonnull SetNode from, @Nonnull SetNode to,
                            @Nonnull Collection<BiFunction<SetNode, SetNode, Iterator<SetNodeEdge>>>
                                    levels) {
        this.from = from;
        this.to = to;
        levelsIterator = levels.iterator();
    }

    public Iterator<SetNodeEdge> iterator() {
        return new LazyIterator();
    }

    public boolean remove(SetNodeEdge edge) {
        return instantiated.remove(edge);
    }

    private class LazyIterator implements Iterator<SetNodeEdge> {
        private Iterator<SetNodeEdge> instantiatedIterator;
        private SetNodeEdge next = null;

        public LazyIterator() {
            instantiatedIterator = instantiated.iterator();
        }

        @Override
        public boolean hasNext() {
            if (instantiatedIterator != null && instantiatedIterator.hasNext()) return true;
            instantiatedIterator = null; //exhausted
            if (next != null)
                return true; //has last delegate's result ready
            /* obtain a delegate, if not already has one */
            while ((delegate == null || !delegate.hasNext()) && levelsIterator.hasNext())
                delegate = levelsIterator.next().apply(from, to);
            if (delegate == null || !delegate.hasNext())
                return false; //failed to obtain a delegate, give up
            /* call the delegate, and store the result */
            next = delegate.next();
            instantiated.add(next);
            return true;
        }

        @Override
        public SetNodeEdge next() {
            /* Check consistency and initialize this.next, if needed. */
            Preconditions.checkState(hasNext());
            if (instantiatedIterator != null && instantiatedIterator.hasNext())
                return instantiatedIterator.next();
            assert next != null;
            return next;
        }
    }
}

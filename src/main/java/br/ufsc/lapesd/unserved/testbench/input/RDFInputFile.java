package br.ufsc.lapesd.unserved.testbench.input;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

public class RDFInputFile extends AbstractRDFInput {
    private static Logger logger = LoggerFactory.getLogger(RDFInputFile.class);
    private final File file;
    private boolean ownsFile = false;

    public RDFInputFile(@Nonnull File file, @Nonnull RDFFormat format) {
        super(format);
        this.file = file;
    }

    public RDFInputFile(RDFInput other) throws IOException {
        super(other.getFormat());
        File otherFile = other.toFile();
        file = createTempFile();
        Files.copy(otherFile.toPath(), file.toPath(), StandardCopyOption.REPLACE_EXISTING);
        ownsFile = true;
    }

    /**
     * Creates a {@link RDFInputFile} that owns the File, and will delete it on close().
     */
    public static RDFInputFile createOwningFile(@Nonnull File file, @Nonnull RDFFormat format) {
        RDFInputFile input = new RDFInputFile(file, format);
        input.ownsFile = true;
        file.deleteOnExit();
        return input;
    }

    @Override
    protected Model loadModel() throws IOException {
        Model model = ModelFactory.createDefaultModel();
        try (FileInputStream stream = new FileInputStream(file)) {
            RDFDataMgr.read(model, stream, getFormat().getLang());
        }
        return model;
    }

    @Override
    public File toFile() throws IOException {
        if (closed) throw new ClosedException();
        return file;
    }


    @Override
    public String getBaseURI() {
        try {
            return toURI();
        } catch (IOException e) {
            throw new RuntimeException("Unexpected exception", e);
        }
    }

    @Override
    public void close() throws IOException {
        if (ownsFile)
            Files.deleteIfExists(file.toPath());
        super.close();
    }

    @Override
    public String toString() {
        String str = "RDFInputFile(" + getFormat().getLang().getName();
        if (ownsFile) str += ",owns";
        str += "," + file + ")";
        return str;
    }
}

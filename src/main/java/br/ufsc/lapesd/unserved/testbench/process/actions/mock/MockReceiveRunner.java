package br.ufsc.lapesd.unserved.testbench.process.actions.mock;

import br.ufsc.lapesd.unserved.testbench.components.SimpleActionRunnerFactory;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.IndexedVariable;
import br.ufsc.lapesd.unserved.testbench.iri.Unserved;
import br.ufsc.lapesd.unserved.testbench.iri.UnservedP;
import br.ufsc.lapesd.unserved.testbench.iri.UnservedX;
import br.ufsc.lapesd.unserved.testbench.model.Message;
import br.ufsc.lapesd.unserved.testbench.model.Reaction;
import br.ufsc.lapesd.unserved.testbench.model.Variable;
import br.ufsc.lapesd.unserved.testbench.model.When;
import br.ufsc.lapesd.unserved.testbench.process.Context;
import br.ufsc.lapesd.unserved.testbench.process.actions.ActionExecutionException;
import br.ufsc.lapesd.unserved.testbench.process.actions.ActionRunner;
import br.ufsc.lapesd.unserved.testbench.process.data.DataContext;
import br.ufsc.lapesd.unserved.testbench.process.model.Receive;
import com.google.common.base.Preconditions;
import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.query.*;
import org.apache.jena.rdf.model.*;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.XSD;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.spinrdf.vocabulary.SP;

import javax.annotation.Nonnull;
import java.util.Collection;

import static br.ufsc.lapesd.unserved.testbench.util.BNodeShim.sparqlResource;
import static java.lang.String.format;
import static org.apache.jena.rdf.model.ResourceFactory.createResource;


public class MockReceiveRunner implements ActionRunner {
    private static Logger logger = LoggerFactory.getLogger(MockReceiveRunner.class);

    public static class Factory extends SimpleActionRunnerFactory {
        public Factory() {
            super(MockReceiveRunner.class, MockReceiveRunner.Factory.class,
                    new UnservedP().getModel().createResource(UnservedP.Receive.getURI()));
        }
    }

    @Override
    public void run(Resource resource, Context context) throws ActionExecutionException {
        Preconditions.checkArgument(resource.canAs(Receive.class));
        Receive receive = resource.as(Receive.class);
        int invocationIndex = receive.getInvocationIndex();
        logger.debug("Receiving {}, invocationIndex={}", receive.getMessage().toString(),
                                                         invocationIndex);

        /* check we had a previous send */
        When when = receive.getMessage().getWhen();
        Preconditions.checkArgument(when.canAs(Reaction.class));
        Reaction reaction = when.as(Reaction.class);
        Message sent = reaction.getReactionTo();
        Collection<MockSendReceiveContext> actionContexts;
        actionContexts = context.runners().getActionContexts(sent, MockSendReceiveContext.class);
        Preconditions.checkState(actionContexts.size() == 1);
        if (reaction.getReactionCardinality().equals(Unserved.one))
            actionContexts.iterator().next().close();

        DataContext data = context.data();
        for (Variable var : receive.getMessage().getVariablesRecursive()) {
            IndexedVariable iVar = new IndexedVariable(var, invocationIndex);
            Resource rep = iVar.getRepresentation();

            if (rep.equals(XSD.anyURI)) {
                setLiteral(data, iVar);
            } else {/*if (rep.equals(Unserved.Resource) || rep.equals(OperationRepresentation)) {*/
                setResource(data, receive.getMessage(), iVar);
            }
        }
        // add parts
        for (Variable var : receive.getMessage().getVariablesRecursive()) {
            IndexedVariable iVar = new IndexedVariable(var, invocationIndex);
            Resource rep = iVar.getRepresentation();
            if (!rep.equals(XSD.anyURI)) {
                Resource varInData = data.getResource(iVar);
                if (varInData != null) {
                    RDFNode value = iVar.enhance(varInData).getValue();
                    if (value.isResource())
                        addParts(data, iVar, value.asResource());
                }
            }
        }
    }

    public static String randomIRI(Variable var) {
        String typeName = getTypeName(var);
        return "http://example.org/a" + typeName + "-" + (int)(Math.random() * 1000000);
    }

    public static String randomString(Variable var) {
        return "str" + getTypeName(var) + "-" + (int)(Math.random() * 1000000);
    }

    public static long randomLong(Variable var) {
        return (long)(Math.random() * 1000000);
    }

    private static String getTypeName(Variable var) {
        String typeName = "Thing";
        Resource type = var.getType();
        if (type != null) {
            int idx = type.toString().lastIndexOf("#");
            if (idx >= 0)
                typeName = type.toString().substring(idx);
        }
        return typeName;
    }

    private void setSurrogate(@Nonnull DataContext data, @Nonnull Message reaction, @Nonnull Variable var) {

    }

    private RDFNode getReal(DataContext data, RDFNode term, Variable var, Resource varValue) {
        if (term.equals(var)) return varValue;
        if (!term.isResource()) return term;
        Resource r = term.asResource();
        Resource r2 = data.getResource(r);
        r = r2 != null ? r2 : r;
        if (!r.hasProperty(RDF.type, Unserved.Variable)) return r;
        Variable termVar = r.as(Variable.class);
        if (var instanceof IndexedVariable)
            termVar = new IndexedVariable(termVar, ((IndexedVariable)var).getIndex());
        return termVar.getValue(); //null if unset
    }

    protected void setResource(DataContext data, Message reaction, Variable var, Resource value) {
        data.setVariable(var, null, value);
        addPostConditions(data, reaction, var, value);
    }

    protected void setResource(DataContext data, Message reaction, Variable var) {
        Preconditions.checkArgument(data.getUnmodifiableModel().containsResource(var));
        Resource value = createResource(randomIRI(var));
        addPostConditions(data, reaction, var, value);
        data.setVariable(var, var.getRepresentation(), value);
    }

    private void addParts(DataContext data, Variable var, Resource value) {
        Model m = data.getUnmodifiableModel();
        try (QueryExecution ex = QueryExecutionFactory.create(QueryFactory.create(
                String.format("PREFIX u: <" + Unserved.PREFIX + ">\n" +
                        "PREFIX ux-rdf: <" + UnservedX.RDF.PREFIX + ">\n" +
                        "SELECT ?child ?prop WHERE {\n" +
                        "  %1$s u:part ?part.\n" +
                        "  ?part u:variable ?child;" +
                        "    u:partModifier ?mod." +
                        "  ?mod ux-rdf:property ?prop;" +
                        "    ux-rdf:propertyOf %1$s" +
                        "}\n", sparqlResource(var))), m)) {
            for (ResultSet rs = ex.execSelect(); rs.hasNext(); ) {
                QuerySolution sol = rs.next();
                Property prop = sol.getResource("prop").as(Property.class);
                Resource child = sol.getResource("child");
                RDFNode childValue = getReal(data, child, var, value);
                if (childValue != null)
                    data.addStatement(ResourceFactory.createStatement(value, prop, childValue));
            }
        }
    }

    protected void addPostConditions(DataContext data, Message reaction, Variable var,
                                     Resource value) {
        Model m = data.getUnmodifiableModel();
        try (QueryExecution exec = QueryExecutionFactory.create(QueryFactory.create(format(
                "PREFIX sp: <" + SP.NS + ">\n" +
                        "PREFIX rdf: <" + RDF.getURI() + ">\n" +
                        "PREFIX u: <" + Unserved.PREFIX + ">\n" +
                        "SELECT ?tp WHERE {\n" +
                        "  %1$s u:condition/(sp:elements?/(rdf:rest*/rdf:first))* ?tp." +
                        "  ?tp (sp:subject|sp:object) %2$s.\n" +
                        "}", sparqlResource(reaction), sparqlResource(var))), m)) {
            ResultSet results = exec.execSelect();
            while (results.hasNext()) {
                Resource tp = results.next().get("tp").asResource();
                Resource subject = tp.getRequiredProperty(SP.subject).getSubject();
                Property predicate = tp.getRequiredProperty(SP.predicate).getPredicate();
                RDFNode object = tp.getRequiredProperty(SP.object).getObject();
                subject = (Resource)getReal(data, subject, var, value);
                object = getReal(data, object, var, value);
                if (subject != null && object != null)
                    data.addStatement(ResourceFactory.createStatement(subject, predicate, object));
            }
        }
    }

    protected void setLiteral(DataContext context, Variable var) {
        if (var.getRepresentation().equals(XSD.anyURI)) {
            context.setVariable(var, null,
                    ResourceFactory.createTypedLiteral(randomIRI(var), XSDDatatype.XSDanyURI));
        } else if (var.getRepresentation().equals(XSD.xdouble)) {
            context.setVariable(var, null, ResourceFactory.createTypedLiteral(
                    String.valueOf(Math.random()), XSDDatatype.XSDdouble));
        } else if (var.getRepresentation().equals(XSD.xlong)) {
            context.setVariable(var, null, ResourceFactory.createTypedLiteral(
                    String.valueOf(Math.random()), XSDDatatype.XSDlong));
        } else if (var.getRepresentation().equals(XSD.xstring)) {
            context.setVariable(var, null,
                    ResourceFactory.createTypedLiteral(randomString(var), XSDDatatype.XSDstring));
        } else {
            context.setVariable(var, null, ResourceFactory.createPlainLiteral(randomString(var)));
        }
    }
}

package br.ufsc.lapesd.unserved.testbench.process.data.action;

import br.ufsc.lapesd.unserved.testbench.process.data.DataContext;
import com.google.common.base.Preconditions;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.jena.rdf.model.*;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Collection;

public class AddStatements implements DataContextAction {
    @Nonnull private final Collection<Statement> statements;

    public AddStatements(@Nonnull Collection<Statement> statements) {
        Preconditions.checkNotNull(statements);
        this.statements = statements;
    }

    public AddStatements(@Nonnull StmtIterator stmtIterator) {
        Preconditions.checkNotNull(stmtIterator);
        statements = new ArrayList<>();
        stmtIterator.forEachRemaining(statements::add);
    }

    @Override
    public void apply(DataContext context) {
        for (Statement stmt : statements) {
            Resource s = context.getResource(stmt.getSubject());
            RDFNode o = stmt.getObject();
            if (stmt.getObject().isResource())
                o = context.getResource(stmt.getResource());
            context.addStatement(ResourceFactory.createStatement(s, stmt.getPredicate(), o));
        }
    }

    @Override
    public String toString() {
        return String.format("AddStatements(%s)", statements.stream().limit(3)
                .map(s -> String.format("%s %s %s", s.getSubject(), s.getPredicate(), s.getObject()))
                .reduce((l, r) -> l + ", " + r).orElse(""));
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof AddStatements)) return false;
        AddStatements rhs = (AddStatements) o;
        return rhs.statements.equals(statements);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(statements).hashCode();
    }
}

package br.ufsc.lapesd.unserved.testbench.input;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.riot.RDFFormat;

import javax.annotation.Nonnull;
import java.io.IOException;

public class RDFInputModel extends AbstractRDFInput {
    private final Model model;
    private boolean ownsModel = true;

    public RDFInputModel(Model model, @Nonnull RDFFormat format) {
        super(format);
        this.model = model;
    }

    public static RDFInputModel notOwningModel(Model model, @Nonnull RDFFormat format) {
        RDFInputModel input = new RDFInputModel(model, format);
        input.ownsModel = false;
        return input;
    }

    @Override
    protected Model loadModel() throws IOException {
        return model;
    }

    @Override
    protected boolean ownsLoadedModel() {
        return ownsModel;
    }

    @Override
    public String toString() {
        return "RDFInputModel(" + getLang().getName() + "," + model.toString() + ")";
    }
}

package br.ufsc.lapesd.unserved.testbench.io_composer;

//import br.ufsc.lapesd.unserved.testbench.benchmarks.Utils;
//import br.ufsc.lapesd.unserved.testbench.components.DefaultComponentRegistry;
//import br.ufsc.lapesd.unserved.testbench.components.WrappingComponentRegistry;
//import br.ufsc.lapesd.unserved.testbench.io_composer.impl.DefaultIOComposerTimes;
//import br.ufsc.lapesd.unserved.testbench.io_composer.layered.BackwardAStarLayeredIOComposer;
//import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.lazy.NonEquivalentBackwardSetNodeProvider;
//import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.optimizations.ServiceCompressionOptimizer;
//import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.discovery.PreconditionsLayersState;
//import br.ufsc.lapesd.unserved.testbench.process.Interpreter;
//import br.ufsc.lapesd.unserved.testbench.process.UnservedPInterpreter;
//import br.ufsc.lapesd.unserved.testbench.process.actions.mock.MockReceiveRunner;
//import br.ufsc.lapesd.unserved.testbench.process.actions.mock.MockSendRunner;
//import br.ufsc.lapesd.unserved.testbench.util.FixedCacheTransitiveClosureGetter;
//import br.ufsc.lapesd.unserved.testbench.util.Probe;
//import com.google.common.collect.ListMultimap;
//import org.apache.jena.rdf.model.Resource;
//import org.apache.jena.vocabulary.RDFS;
//import org.testng.annotations.AfterClass;
//import org.testng.annotations.Test;
//
//import java.io.File;
//import java.io.FileOutputStream;
//import java.util.ArrayList;
//import java.util.List;

public class ProfilerTest {
//    ListMultimap<String, Double> ioSnapshot = null;
//    ListMultimap<String, Double> preconditionsSnapshot = null;
//
//    @AfterClass
//    public void showResults() {
//        System.out.println("\n---");
//        Probe.dump(System.out, ioSnapshot, preconditionsSnapshot);
//    }
//
//    @Test
//    void testIO() throws Exception {
//        doTestIO();
//        doTestIO();
//        Utils.preheatCooldown();
//        Probe.reset();
//        doTestIO();
//        ioSnapshot = Probe.snapshot();
//        Probe.dump(System.out);
//    }
//
//    void doTestIO() throws Exception {
//        WscConvertedTestBase tb = new WscConvertedTestBase() {
//            @Override
//            protected IOComposerBuilder getBuilder() {
//                return new BackwardAStarLayeredIOComposer.Builder()
//                        .withOptimizer(new ServiceCompressionOptimizer());
//            }
//        };
//        try {
//            tb.setUp();
//            tb.testProblem("06");
//        } finally {
//            tb.tearDown();
//        }
//
//    }
//
//    @Test
//    void testOMFG() throws Exception {
////        new FileOutputStream("/tmp/barrier").close();
////        while (new File("/tmp/barrier").exists()) Thread.sleep(100);
//
//        for (int i = 0; i < 10; i++) {
//            if (i == 2) Probe.reset();
//            IOComposerBuilder builder = new BackwardAStarLayeredIOComposer.Builder()
//                    .withOptimizer(new ServiceCompressionOptimizer())
//                    .setBackwardSetNodeProvider(new NonEquivalentBackwardSetNodeProvider().setUseConditions(true))
//                    .setLayersStateFactory(ci -> new PreconditionsLayersState(
//                            FixedCacheTransitiveClosureGetter.forward(RDFS.subClassOf)
//                                    .setThreshold(3)
//                                    .visitAll(ci.getSkolemizedUnion()).build()));
//            WrappingComponentRegistry componentRegistry = new WrappingComponentRegistry(
//                    DefaultComponentRegistry.getInstance());
//            componentRegistry.registerComponent(MockSendRunner.Factory.class);
//            componentRegistry.registerComponent(MockReceiveRunner.Factory.class);
//
//            Interpreter interpreter = new UnservedPInterpreter(componentRegistry);
//            ClassLoader cl = getClass().getClassLoader();
//            builder.addInput(WscConvertedTestBase.getWscTurtle("06", "taxonomy"));
//            builder.addInput(WscConvertedTestBase.getWscTurtle("06", "services"));
//            builder.addInput(WscConvertedTestBase.getWscTurtle("06", "known"));
//            List<Resource> wantedList = new ArrayList<>();
//            br.ufsc.lapesd.unserved.testbench.Utils.addWantedVars(builder, WscConvertedTestBase.getWscTurtle("06", "wanted"), wantedList);
//            IOComposer composer = builder.withBasicReasoning(false).build();
//            ((BackwardAStarLayeredIOComposer)composer).createGraph(new DefaultIOComposerTimes());
//            builder.close();
//            composer.close();
//            componentRegistry.close();
//        }
//        Probe.dump(System.out);
//    }
//
//    @Test
//    void testPreconditions() throws Exception {
//        doTestPreconditions();
//        doTestPreconditions();
//        Utils.preheatCooldown();
//        Probe.reset();
//        doTestPreconditions();
//        preconditionsSnapshot = Probe.snapshot();
//        Probe.dump(System.out);
//    }
//
//    void doTestPreconditions() throws Exception {
//        WscConvertedTestBase tb = new WscConvertedTestBase() {
//            @Override
//            protected IOComposerBuilder getBuilder() {
//                return new BackwardAStarLayeredIOComposer.Builder()
//                        .withOptimizer(new ServiceCompressionOptimizer())
//                        .setBackwardSetNodeProvider(new NonEquivalentBackwardSetNodeProvider().setUseConditions(true))
//                        .setLayersStateFactory(ci -> new PreconditionsLayersState(
//                                FixedCacheTransitiveClosureGetter.forward(RDFS.subClassOf)
//                                        .setThreshold(3)
//                                        .visitAll(ci.getSkolemizedUnion()).build()));
//            }
//        };
//        try {
//            tb.setUp();
//            tb.testProblem("06");
//        } finally {
//            tb.tearDown();
//        }
//    }
}

package br.ufsc.lapesd.unserved.testbench.io_composer;

import br.ufsc.lapesd.unserved.testbench.io_composer.backward.BackwardCompositionWorkingGraphFromAccessor;
import br.ufsc.lapesd.unserved.testbench.io_composer.backward.OutputIndexedBackwardGraphAccessor;
import br.ufsc.lapesd.unserved.testbench.io_composer.backward.ReplicatedBackwardIOADStar;
import org.testng.annotations.Test;

@Test
public class FromAccessorReplicatedBackwardIOADStarTests {
    public class ApiDemoTests extends ApiDemoTestBase {
        @Override
        protected IOComposerBuilder getBuilder() {
            BackwardCompositionWorkingGraphFromAccessor gb;
            gb = new BackwardCompositionWorkingGraphFromAccessor();
            gb.setGraphAccessorFactory(OutputIndexedBackwardGraphAccessor::new);
            return new ReplicatedBackwardIOADStar.Builder().setGraphBuilder(gb);
        }
    }

    @Test
    public class DescriptionGenerator extends DescriptionGeneratorTestBase {
        @Override
        protected IOComposerBuilder getBuilder() {
            BackwardCompositionWorkingGraphFromAccessor gb;
            gb = new BackwardCompositionWorkingGraphFromAccessor();
            gb.setGraphAccessorFactory(OutputIndexedBackwardGraphAccessor::new);
            return new ReplicatedBackwardIOADStar.Builder().setGraphBuilder(gb);
        }
    }
}

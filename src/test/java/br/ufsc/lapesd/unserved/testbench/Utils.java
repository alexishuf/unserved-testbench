package br.ufsc.lapesd.unserved.testbench;

import br.ufsc.lapesd.unserved.testbench.input.Input;
import br.ufsc.lapesd.unserved.testbench.input.InputFromStream;
import br.ufsc.lapesd.unserved.testbench.input.RDFInput;
import br.ufsc.lapesd.unserved.testbench.input.RDFInputStream;
import br.ufsc.lapesd.unserved.testbench.io_composer.IOComposerBuilder;
import br.ufsc.lapesd.unserved.testbench.io_composer.impl.IOComposerInput;
import br.ufsc.lapesd.unserved.testbench.iri.HTTP;
import br.ufsc.lapesd.unserved.testbench.iri.Unserved;
import br.ufsc.lapesd.unserved.testbench.iri.UnservedP;
import br.ufsc.lapesd.unserved.testbench.pragproof.PragmaticProof;
import br.ufsc.lapesd.unserved.testbench.reasoner.N3ReasonerException;
import br.ufsc.lapesd.unserved.testbench.reasoner.N3ReasonerFactory;
import br.ufsc.lapesd.unserved.testbench.reasoner.impl.EYEProcessFactory;
import br.ufsc.lapesd.unserved.testbench.reasoner.impl.RiotEnhancedN3ReasonerFactory;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFWriterRegistry;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.XSD;
import org.testng.Assert;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class Utils {
    static public Input resourceInput(String mediaType, String path) {
        ClassLoader loader = Utils.class.getClassLoader();
        return new InputFromStream(mediaType, loader.getResourceAsStream(path));
    }

    static public RDFInput resourceInput(String path, Lang lang) {
        ClassLoader loader = Utils.class.getClassLoader();
        InputStream stream = loader.getResourceAsStream(path);
        return new RDFInputStream(stream, RDFWriterRegistry.defaultSerialization(lang));
    }

    public static N3ReasonerFactory reasonerFactory() throws N3ReasonerException {
        return new RiotEnhancedN3ReasonerFactory(new EYEProcessFactory());
    }

    public static PragmaticProof.Builder addWantedVars(PragmaticProof.Builder builder,
                                                 RDFInput input) throws IOException {
        builder.addInput(input);
        input.getModel().listSubjectsWithProperty(RDF.type, Unserved.Variable)
                .forEachRemaining(builder::markWantedVariable);
        return builder;
    }

    public static IOComposerBuilder addWantedVars(IOComposerBuilder builder,
                                                  RDFInput input) throws IOException {
        return addWantedVars(builder, input, null);
    }

    public static IOComposerBuilder addWantedVars(IOComposerBuilder builder,
                                                  RDFInput input,
                                                  Collection<Resource> outVariables)
            throws IOException {
        builder.addInput(input);
        List<Resource> list = new ArrayList<>();
        try (QueryExecution ex = QueryExecutionFactory.create(QueryFactory.create(
                "PREFIX u: <" + Unserved.PREFIX + ">\n" +
                        "SELECT ?var WHERE { ?var a u:Variable, u:Wanted. }"), input.getModel())) {
            ex.execSelect().forEachRemaining(s -> list.add(s.getResource("var")));
        }
        if (outVariables != null) outVariables.addAll(list);
        list.forEach(builder::markWantedVariable);
        return builder;
    }

    public static IOComposerInput addWantedVars(IOComposerInput ci, RDFInput input,
                                                Collection<Resource> outVars) throws IOException {
        ci.addInput(input);
        List<Resource> list = new ArrayList<>();
        try (QueryExecution ex = QueryExecutionFactory.create(QueryFactory.create(
                "PREFIX u: <" + Unserved.PREFIX + ">\n" +
                        "SELECT ?var WHERE { ?var a u:Variable, u:Wanted. }\n"), input.getModel())){
            ex.execSelect().forEachRemaining(s -> list.add(s.getResource("var")));
        }
        if (outVars != null) outVars.addAll(list);
        list.forEach(ci::addWanted);
        return ci;
    }

    public static List<Resource> getResources(Model model, String varName, String sparql,
                                              String additionalPrologue) {
        try (QueryExecution ex = QueryExecutionFactory.create(
                QueryFactory.create("PREFIX unserved: <" + Unserved.PREFIX + ">\n" +
                        "PREFIX http: <" + HTTP.PREFIX + ">\n" +
                        "PREFIX http-methods: <" + HTTP.Methods.PREFIX + ">\n" +
                        "PREFIX unserved-p: <" + UnservedP.PREFIX + ">\n" +
                        "PREFIX xsd: <" + XSD.getURI() + ">\n" +
                        "PREFIX vocab: <http://www.markus-lanthaler.com/hydra/api-demo/vocab#>\n" +
             additionalPrologue +
                        "SELECT ?" + varName + " WHERE {\n" +
                        sparql +
                        "}\n"), model)) {
            List<Resource> list = new ArrayList<>();
            ResultSet results = ex.execSelect();
            while (results.hasNext()) list.add(results.next().getResource(varName));
            return list;
        }
    }
    public static List<Resource> getResources(Model model, String varName, String sparql) {
        return getResources(model, varName, sparql, "");
    }

    public static Resource getSingleResource(Model model, String varName, String sparql, String additionalPrologue) {
        List<Resource> list = getResources(model, varName, sparql, additionalPrologue);
        Assert.assertEquals(list.size(), 1);
        return list.get(0);
    }
    public static Resource getSingleResource(Model model, String varName, String sparql) {
        return getSingleResource(model, varName, sparql, "");
    }

    static {
        Unserved.init();
    }
}

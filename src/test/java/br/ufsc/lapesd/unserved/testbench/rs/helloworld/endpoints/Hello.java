package br.ufsc.lapesd.unserved.testbench.rs.helloworld.endpoints;

import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFFormat;
import org.apache.jena.sparql.vocabulary.FOAF;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import java.io.StringWriter;

@Path("hello")
public class Hello {

    @GET
    @Produces("text/turtle")
    public String get(@Context UriInfo info, @QueryParam("name") String name) {
        Model model = ModelFactory.createDefaultModel();
        model.setNsPrefix("rdfs", RDFS.getURI());
        model.setNsPrefix("foaf", FOAF.getURI());
        Resource resource = model.createResource(info.getAbsolutePath().toString());

        resource.addProperty(RDFS.label, ResourceFactory.createTypedLiteral("Hello World"));
        resource.addProperty(RDF.type, FOAF.Document);
        //a small abuse...
        resource.addProperty(FOAF.name,
                ResourceFactory.createTypedLiteral(name, XSDDatatype.XSDstring));

        StringWriter writer = new StringWriter();
        RDFDataMgr.write(writer, model, RDFFormat.TURTLE);
        return writer.getBuffer().toString();
    }
}

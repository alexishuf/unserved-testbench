package br.ufsc.lapesd.unserved.testbench.io_composer.layered;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class LayeredProgressTemplateTest {
    @DataProvider
    public static Object[][] matchData() {
        return new Object[][] {
                {new LayeredProgressTemplate(null, null, null, null),
                 new LayeredProgress(1, 2, 3, 4),
                 true},
                {new LayeredProgressTemplate(2.0, null, null, null),
                 new LayeredProgress(2, 3, 4, 5),
                 true},
                {new LayeredProgressTemplate(.5, null, null, null),
                 new LayeredProgress(2, 4, 0, 5),
                 true},
                {new LayeredProgressTemplate(.3, null, null, null),
                 new LayeredProgress(2, 10, 0, 5),
                 false},
                {new LayeredProgressTemplate(.3, 9, null, null),
                 new LayeredProgress(3, 10, 0, 5),
                 false},
                {new LayeredProgressTemplate(.3, 10, null, null),
                 new LayeredProgress(3, 10, 0, 5),
                 true},

                {new LayeredProgressTemplate(null, null, null, null),
                 new LayeredProgress(1, 2, 1, 2),
                 true},
                {new LayeredProgressTemplate(2.0, null, 2.0, null),
                 new LayeredProgress(2, 3, 2, 3),
                 true},
                {new LayeredProgressTemplate(.5, null, .5, null),
                 new LayeredProgress(2, 4, 2, 4),
                 true},
                {new LayeredProgressTemplate(.3, null, .3, null),
                 new LayeredProgress(2, 10, 2, 10),
                 false},
                {new LayeredProgressTemplate(.3, 9, .3, 9),
                 new LayeredProgress(3, 10, 3, 10),
                 false},
                {new LayeredProgressTemplate(.3, 10, .3, 10),
                 new LayeredProgress(3, 10, 3, 10),
                 true},
        };
    }

    @DataProvider
    public static Object[][] parseData() {
        return new Object[][] {
                {"*/*/*/*", new LayeredProgressTemplate(null, null, null, null)},
                {"0.5/*/*/*", new LayeredProgressTemplate(.5, null, null, null)},
                {"5/*/*/*", new LayeredProgressTemplate(5.0, null, null, null)},
                {"5.5/*/*/*", null},
                {"*/*/0.5/*", new LayeredProgressTemplate(null, null, .5, null)},
                {"*/*/5/*", new LayeredProgressTemplate(null, null, 5.0, null)},
                {"*/*/5.5/*", null},
                {"*/1/*/*", new LayeredProgressTemplate(null, 1, null, null)},
                {"*/*/*/4", new LayeredProgressTemplate(null, null, null, 4)},
                {"*/*/5.5", null},
                {"*/*/5", null},
                {"*/*/5/", null},
                {"*//5/*", null},
        };
    }

    @DataProvider
    public static Object[][] toStringData() {
        List<Object[]> list = new LinkedList<>(Arrays.asList(parseData()));
        list.removeIf(row -> row[1] == null);
        Object[][] objects = new Object[list.size()][];
        for (int i = 0; i < list.size(); i++) {
            objects[i] = new Object[] {list.get(i)[1], list.get(i)[0]};
        }
        return objects;
    }

    @Test(dataProvider = "matchData")
    public void testMatch(LayeredProgressTemplate template, LayeredProgress progress,
                          boolean expected) throws Exception {
        Assert.assertEquals(template.match(progress), expected);
    }

    @Test(dataProvider = "parseData")
    public void testParse(String text, LayeredProgressTemplate expected) throws Exception {
        Assert.assertEquals(LayeredProgressTemplate.parse(text), expected);
    }

    @Test(dataProvider = "toStringData")
    public void testToString(LayeredProgressTemplate template, String expected) throws Exception {
        Assert.assertEquals(template.toString(), expected);
    }
}
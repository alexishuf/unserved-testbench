package br.ufsc.lapesd.unserved.testbench.model;

import br.ufsc.lapesd.unserved.testbench.iri.Unserved;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.riot.Lang;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.InputStream;

@Test
public class EnhancedNodesTests {
    private Model model;
    private static String var = "https://alexishuf.bitbucket.io/2016/04/unserved/var#";
    private static String vocab = "http://www.markus-lanthaler.com/hydra/api-demo/vocab#";
    private Unserved unservedBackground;

    @BeforeMethod
    public void setUp() {
        Unserved.init();
        model = ModelFactory.createDefaultModel();
        ClassLoader loader = getClass().getClassLoader();
        InputStream stream = loader.getResourceAsStream("model/model.ttl");
        model.read(stream, null, Lang.TURTLE.getName());
        unservedBackground = new Unserved();
        model.add(unservedBackground.getModel());
    }

    @AfterMethod
    public void tearDown() throws Exception {
        model.close();
        unservedBackground.close();
    }

    @Test
    public void testVariable() {
        Resource var1 = model.createResource(EnhancedNodesTests.var + "var1");

        Assert.assertTrue(var1.canAs(Variable.class));
        Variable enh = var1.as(Variable.class);
        Assert.assertNotNull(enh);
        Assert.assertEquals(enh.getType(), model.createResource(vocab+"EntryPoint"));
    }

    @Test
    public void testOneCardinality() {
        Resource one = model.createResource(Unserved.one.toString());

        Assert.assertTrue(one.canAs(ValuedCardinality.class));
        ValuedCardinality enh = one.as(ValuedCardinality.class);
        Assert.assertNotNull(enh);
        Assert.assertEquals(enh.getCardinalityValue(), 1);
    }

    @Test
    public void testSpontaneous() {
        Resource spontaneous = model.createResource(Unserved.spontaneous.toString());

        Assert.assertTrue(spontaneous.canAs(Spontaneous.class));
        Spontaneous enh = spontaneous.as(Spontaneous.class);
        Assert.assertNotNull(enh);
    }

    @Test
    public void testMessage() {
        Resource msg1 = model.createResource(var + "msg1");

        Assert.assertTrue(msg1.canAs(Message.class));
        Message enh = msg1.as(Message.class);
        Assert.assertNotNull(enh);

        Assert.assertNotNull(enh.getFrom());
        Assert.assertEquals(enh.getFrom().toString(), var+"client");
        Assert.assertNotNull(enh.getTo());
        Assert.assertEquals(enh.getTo().toString(), var+"server");

        Assert.assertNotNull(enh.getWhen());
        Assert.assertTrue(enh.getWhen() instanceof Spontaneous);
    }

    @Test
    public void testPart() {
        Resource part1 = model.createResource(var + "part1");
        Assert.assertTrue(part1.canAs(Part.class));
        Part enh = part1.as(Part.class);
        Assert.assertNotNull(enh);
        Assert.assertEquals(enh.getVariable(), model.createResource(var+"var1"));
        Assert.assertNotNull(enh.getPartModifier());
    }

    @Test
    public void testReaction() {
        Resource reaction1 = model.createResource(var + "reaction1");
        Assert.assertTrue(reaction1.canAs(Reaction.class));
        Reaction enh = reaction1.as(Reaction.class);
        Assert.assertNotNull(enh);
        Assert.assertEquals(enh.getReactionTo(), model.createResource(var+"msg1"));
        Cardinality cardinality = enh.getReactionCardinality();
        Assert.assertNotNull(cardinality);
        Assert.assertTrue(cardinality instanceof ValuedCardinality);
        ValuedCardinality valued = (ValuedCardinality) cardinality;
        Assert.assertEquals(valued.getCardinalityValue(), 1);
    }
}

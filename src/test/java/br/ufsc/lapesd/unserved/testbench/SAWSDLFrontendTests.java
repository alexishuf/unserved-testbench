package br.ufsc.lapesd.unserved.testbench;

import br.ufsc.lapesd.unserved.testbench.input.Input;
import br.ufsc.lapesd.unserved.testbench.input.RDFInput;
import br.ufsc.lapesd.unserved.testbench.iri.Unserved;
import br.ufsc.lapesd.unserved.testbench.iri.UnservedX;
import br.ufsc.lapesd.unserved.testbench.sawsdl.SAWSDLFrontend;
import org.apache.commons.io.IOUtils;
import org.apache.jena.datatypes.xsd.impl.XMLLiteralType;
import org.apache.jena.query.*;
import org.apache.jena.rdf.model.Model;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;

@Test
public class SAWSDLFrontendTests {

    @Test
    public class BookAuthorService {
        private RDFInput result;
        private Model model;

        @BeforeClass
        public void setUp() throws Exception{
            Input input = Utils.resourceInput("application/wsdl+xml", "sws-tc/services/sawsdl_wsdl11/book_author_service.wsdl");
            SAWSDLFrontend frontend = new SAWSDLFrontend();
            result = frontend.compile(input);
            model = result.getModel();
        }

        @AfterClass
        public void tearDown() throws Exception {
            result.close();
            model.close();
        }

        public void testWsdlOneRequestReply() {
            QueryExecution execution = QueryExecutionFactory.create(
                    QueryFactory.create("PREFIX unserved: <" + Unserved.PREFIX + ">\n" +
                            "PREFIX ux-wsdl: <" + UnservedX.WSDL.PREFIX + ">\n" +
                            "SELECT * WHERE {\n" +
                            "  ?req a ux-wsdl:WsdlRequest.\n" +
                            "  ?reply a ux-wsdl:WsdlReply.\n" +
                            "  ?reply unserved:when ?when.\n" +
                            "  ?when a unserved:Reaction.\n" +
                            "  ?when unserved:reactionTo ?req.\n" +
                            "  ?when unserved:reactionCardinality unserved:one.\n" +
                            "}"), model);
            ResultSet resultSet = execution.execSelect();
            Assert.assertTrue(resultSet.hasNext());
            resultSet.next();
            Assert.assertFalse(resultSet.hasNext());
            execution.close();
        }

        public void testWsdlFileAsLiteral() throws IOException, ParserConfigurationException, SAXException {
            QueryExecution execution = QueryExecutionFactory.create(
                    QueryFactory.create("PREFIX ux-wsdl: <" + UnservedX.WSDL.PREFIX + ">\n" +
                            "SELECT * WHERE {\n" +
                            "  ?file a ux-wsdl:WsdlFile.\n" +
                            "  ?file ux-wsdl:wsdlContent ?content.\n" +
                            "}"), model);
            ResultSet results = execution.execSelect();
            Assert.assertTrue(results.hasNext());

            QuerySolution row = results.next();
            Assert.assertEquals(row.get("content").asLiteral().getDatatype(),
                    XMLLiteralType.theXMLLiteralType);

            String xmlString = row.get("content").asLiteral().getLexicalForm();
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            dbFactory.setNamespaceAware(true);
            DocumentBuilder db = dbFactory.newDocumentBuilder();
            Document document = db.parse(IOUtils.toInputStream(xmlString));
            Assert.assertNotNull(document);
            Assert.assertTrue(document.getChildNodes().getLength() == 1);

            Assert.assertFalse(results.hasNext());
            execution.close();
        }
    }
}

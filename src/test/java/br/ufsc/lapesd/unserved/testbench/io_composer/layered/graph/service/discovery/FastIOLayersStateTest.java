package br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.discovery;

import br.ufsc.lapesd.unserved.testbench.io_composer.impl.IOComposerInput;
import org.testng.annotations.Test;

@Test
public class FastIOLayersStateTest {
    @Test
    public class CompareTests extends ServiceGraphComparisonTestBase {
        @Override
        protected IOLayersState aLayersState(IOComposerInput composerInput) {
            return new IOLayersState(composerInput);
        }

        @Override
        protected IOLayersState bLayersState(IOComposerInput composerInput) {
            return new FastIOLayersState(new InputServiceIndex());
        }
    }
}

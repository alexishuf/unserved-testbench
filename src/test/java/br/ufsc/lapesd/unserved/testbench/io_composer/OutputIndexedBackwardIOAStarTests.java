package br.ufsc.lapesd.unserved.testbench.io_composer;

import br.ufsc.lapesd.unserved.testbench.io_composer.backward.BackwardIOAStar;
import br.ufsc.lapesd.unserved.testbench.io_composer.backward.OutputIndexedBackwardGraphAccessor;
import org.testng.annotations.Test;

@Test
public class OutputIndexedBackwardIOAStarTests {
    @Test
    public class ApiDemoTests extends ApiDemoTestBase {
        @Override
        protected IOComposerBuilder getBuilder() {
            return new BackwardIOAStar.Builder()
                    .setAccessorFactory(OutputIndexedBackwardGraphAccessor::new);
        }
    }

    @Test
    public class DescriptionGenerator extends DescriptionGeneratorTestBase {
        @Override
        protected IOComposerBuilder getBuilder() {
            return new BackwardIOAStar.Builder()
                    .setAccessorFactory(OutputIndexedBackwardGraphAccessor::new);
        }
    }
}

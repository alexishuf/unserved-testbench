package br.ufsc.lapesd.unserved.testbench;

import br.ufsc.lapesd.unserved.testbench.input.RDFInput;
import br.ufsc.lapesd.unserved.testbench.input.RDFInputStream;
import org.apache.jena.riot.RDFFormat;

import java.io.InputStream;

public class ApiDemoHelper {
    public static RDFInput loadRDF(String filename) {
        ClassLoader loader = ApiDemoHelper.class.getClassLoader();
        InputStream stream;
        stream = loader.getResourceAsStream("apidemo/" + filename);
        return new RDFInputStream(stream, RDFFormat.TTL);
    }
}

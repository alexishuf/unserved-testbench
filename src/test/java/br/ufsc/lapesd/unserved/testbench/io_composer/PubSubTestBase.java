package br.ufsc.lapesd.unserved.testbench.io_composer;

import br.ufsc.lapesd.unserved.testbench.Utils;
import br.ufsc.lapesd.unserved.testbench.composer.Composition;
import br.ufsc.lapesd.unserved.testbench.composer.ReplicatedComposer;
import br.ufsc.lapesd.unserved.testbench.input.RDFInputStream;
import br.ufsc.lapesd.unserved.testbench.model.Variable;
import br.ufsc.lapesd.unserved.testbench.process.Interpreter;
import br.ufsc.lapesd.unserved.testbench.process.data.DataContext;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.riot.RDFFormat;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.stream.Collectors;

@Test
public abstract class PubSubTestBase extends CompositionTestBase {
    private RDFInputStream getTTL(String filename) {
        final ClassLoader cl = PubSubTestBase.class.getClassLoader();
        return new RDFInputStream(cl.getResourceAsStream("io_composer/ps/" + filename), RDFFormat.TURTLE);
    }

    @Test
    public void test() throws Exception {
        builder.addInput(getTTL("services.ttl"));
        builder.addInput(getTTL("known.ttl"));
        RDFInputStream wantedRDF = getTTL("wanted.ttl");
        List<Resource> wanted = new ArrayList<>();
        Utils.addWantedVars(builder, wantedRDF, wanted);
        composer = builder.build();

        Assert.assertTrue(composer instanceof ReplicatedComposer);
        ReplicatedComposer composer = (ReplicatedComposer) this.composer;
        BlockingQueue<IOCompositionResult> queue = new LinkedBlockingQueue<>();
        interpreter.setResultConsumer(IOCompositionResult.class, r -> {
            try {
                queue.put(r);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        });

        try (Composition comp = composer.run();
             DataContext dataContext = comp.createDataContext()) {
            Interpreter.Task task = interpreter.interpret(dataContext, comp.getWorkflowRoot());
            for (int i = 0; i < 2; i++) {
                try (IOCompositionResult result = queue.take()) {
                    List<Variable> vars = wanted.stream().map(result::getWantedVariable)
                            .map(r -> r.as(Variable.class)).collect(Collectors.toList());
                    Assert.assertEquals(vars.size(), wanted.size());
                    for (int j = 0; j < vars.size(); j++) {
                        Assert.assertNotNull(vars.get(j),
                                "Missing " + wanted.get(j) + ", i=" + i);
                        Assert.assertTrue(vars.get(j).isValueBound(),
                                "Variable " + vars.get(j) + " is unbound. i=" + i);
                    }
                }
            }

            task.cancel().waitForDone();
            Assert.assertTrue(task.isDone());
        }
    }
}

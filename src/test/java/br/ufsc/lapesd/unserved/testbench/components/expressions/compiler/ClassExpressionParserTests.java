package br.ufsc.lapesd.unserved.testbench.components.expressions.compiler;

import br.ufsc.lapesd.unserved.testbench.Utils;
import br.ufsc.lapesd.unserved.testbench.components.expressions.*;
import br.ufsc.lapesd.unserved.testbench.components.expressions.Cardinality.CardinalityType;
import br.ufsc.lapesd.unserved.testbench.components.expressions.Connective.ConnectiveType;
import br.ufsc.lapesd.unserved.testbench.components.expressions.PropertyRestriction.RestrictionType;
import br.ufsc.lapesd.unserved.testbench.input.RDFInput;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;
import org.apache.jena.riot.Lang;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;

public class ClassExpressionParserTests {
    @DataProvider
    public Object[][] parseData() throws IOException {
        String p1 = "http://example.com/#";
        RDFInput in1 = Utils.resourceInput("components/expressions/parse-test.ttl", Lang.TURTLE);

        Model m1 = in1.getModel();
        Property prop1 = m1.createProperty(p1 + "prop1");

        Resource class1 = m1.createResource(p1 + "class1"),
                class2 = m1.createResource(p1 + "class2"),
                class3 = m1.createResource(p1 + "class3"),
                class4 = m1.createResource(p1 + "class4");
        ClassExpression expr1, expr2, expr3, expr4;
        expr1 = new AtomicClassExpression(ResourceFactory.createResource(p1 + "class1"));
        expr2 = new PropertyRestriction(RestrictionType.SOME_VALUES_FROM, prop1,
                Collections.singleton(class2), expr1);
        expr3 = new Connective(ConnectiveType.UNION, Arrays.asList(expr2, expr1),
                Collections.singleton(class3));
        expr4 = new Cardinality(CardinalityType.MAX_QUALIFIED_CARDINALITY, prop1, 1,
                Collections.singleton(class4), expr1);

        return new Object[][] {
                {class1, expr1},
                {class2, expr2},
                {class3, expr3},
                {class4, expr4},
        };
    }

    @Test(dataProvider = "parseData")
    public void testParse(Resource aClass, ClassExpression expected) throws Exception {
        ClassExpression parsed = new ClassExpressionParser().parse(aClass);
        Assert.assertEquals(parsed, expected);
    }
}
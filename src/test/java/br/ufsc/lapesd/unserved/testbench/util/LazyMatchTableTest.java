package br.ufsc.lapesd.unserved.testbench.util;

import br.ufsc.lapesd.unserved.testbench.iri.Testbench;
import br.ufsc.lapesd.unserved.testbench.util.MatchTable.Relation;
import com.google.common.base.Preconditions;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static br.ufsc.lapesd.unserved.testbench.util.MatchTable.Relation.*;
import static java.lang.Integer.parseInt;

public class LazyMatchTableTest {
    private Model model;
    private List<Resource> roots;
    private LazyMatchTable table;
    private Property childProperties[];

    @DataProvider
    public static Object[][] getData() {
        return new Object[][] {
                {"0", "0", SAME},
                {"0", "00", SUPERCLASS},
                {"0", "0010", SUPERCLASS},
                {"0", "0110", SUPERCLASS},
                {"0", "0101", SUPERCLASS},
                {"00", "001", SUPERCLASS},
                {"00", "0011", SUPERCLASS},
                {"0", "1", MISMATCH},
                {"00", "01", MISMATCH},
                {"001", "000", MISMATCH},
                {"00", "010", MISMATCH},
        };
    }

    @DataProvider
    public static Object[][] closureData() {
        return new Object[][] {
                {"0000", SUPERCLASS, Collections.singletonList("0000")},
                {"000", SUPERCLASS, Arrays.asList("000", "0000", "0001")},
                {"01", SUBCLASS, Arrays.asList("01", "0")},
                {"0001", SUBCLASS, Arrays.asList("0001", "000", "00", "0")},
        };
    }

    private Resource createClass(String name) {
        return model.createResource(Testbench.PREFIX + name).addProperty(RDF.type, RDFS.Class);
    }

    @BeforeMethod
    public void setUp() throws Exception {
        model = ModelFactory.createDefaultModel();
        childProperties = new Property[] {
                model.createProperty(Testbench.PREFIX + "child1"),
                model.createProperty(Testbench.PREFIX + "child2")};

        roots = Stream.of(createClass("0"), createClass("1"))
                .collect(Collectors.toList());
        for (Resource root : roots) {
            Stack<ImmutablePair<Resource, Integer>> stack = new Stack<>();
            stack.push(ImmutablePair.of(root, 0));
            while (!stack.isEmpty()) {
                ImmutablePair<Resource, Integer> state = stack.pop();
                for (int i = 0; state.getValue() < 3 && i < 2; i++) {
                    String name = state.getKey().getURI().split("#")[1] + String.valueOf(i);
                    Resource child = createClass(name).addProperty(RDFS.subClassOf, state.getKey());
                    state.getKey().addProperty(childProperties[i], child);
                    stack.push(ImmutablePair.of(child, state.getValue() + 1));
                }
            }
        }

        table = new LazyMatchTable(model, new BitSetMatchTable.Builder()
                .addAll(model.listResourcesWithProperty(RDF.type, RDFS.Class).toList()).build());
    }

    private Resource getType(String a) {
        Preconditions.checkArgument(!a.isEmpty());

        Resource type = roots.get(parseInt(a.substring(0, 1)));
        for (int i = 1; i < a.length(); i++)
            type = type.getPropertyResourceValue(childProperties[parseInt(a.substring(i, i+1))]);
        return type;
    }

    @AfterMethod
    public void tearDown() throws Exception {
        model.close();
        model = null;
        roots = null;
    }

    @Test(dataProvider = "getData")
    public void testGet(String leftAddress, String rightAddress, Relation expected) {
        Resource left = getType(leftAddress), right = getType(rightAddress);
        Assert.assertEquals(table.get(left, right), expected);
    }

    @Test(dataProvider = "getData")
    public void testGetReverse(String leftAddress, String rightAddress, Relation expected) {
        Resource left = getType(leftAddress), right = getType(rightAddress);
        if (expected == SUBCLASS) expected = SUPERCLASS;
        else if (expected == SUPERCLASS) expected = SUBCLASS;
        Assert.assertEquals(table.get(right, left), expected);
    }

    @Test
    public void testConsecutiveGets() {
        for (Object[] row : getData()) {
            String leftAddress = (String) row[0];
            String rightAddress = (String) row[1];
            Relation expected = (Relation) row[2];
            Resource left = getType(leftAddress), right = getType(rightAddress);
            Assert.assertEquals(table.get(left, right), expected);
        }
    }

    @Test(dataProvider = "closureData")
    public void testClosure(String address, Relation relation, List<String> addresses) {
        Resource left = getType(address);
        Set<Resource> expected = addresses.stream().map(this::getType).collect(Collectors.toSet());
        Set<Resource> actual = table.closureStream(left, relation).collect(Collectors.toSet());
        Assert.assertEquals(actual, expected);
    }

}
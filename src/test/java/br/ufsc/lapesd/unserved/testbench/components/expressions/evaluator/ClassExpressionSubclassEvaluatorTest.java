package br.ufsc.lapesd.unserved.testbench.components.expressions.evaluator;

import br.ufsc.lapesd.unserved.testbench.Utils;
import br.ufsc.lapesd.unserved.testbench.components.expressions.ClassExpression;
import br.ufsc.lapesd.unserved.testbench.components.expressions.compiler.ClassExpressionParser;
import br.ufsc.lapesd.unserved.testbench.components.expressions.evaluator.ClassExpressionEvaluator.Result;
import br.ufsc.lapesd.unserved.testbench.input.RDFInput;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.riot.Lang;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class ClassExpressionSubclassEvaluatorTest {

    @DataProvider
    public static Object[][] evaluateData() throws Exception {
        RDFInput in1 = Utils.resourceInput("components/expressions/subclass-test.ttl", Lang.TURTLE);
        String p1 = "http://example.com/#";
        Model m1 = in1.getModel();

        return new Object[][] {
                {m1.createResource(p1+"class1"),   m1.createResource(p1+"class1"), Result.TRUE},
                {m1.createResource(p1+"class1-1"), m1.createResource(p1+"class1"), Result.TRUE},
                {m1.createResource(p1+"class2"),   m1.createResource(p1+"class1"), Result.INDETERMINATE},
                {m1.createResource(p1+"class4"),   m1.createResource(p1+"class4"), Result.TRUE},
                {m1.createResource(p1+"class1"),   m1.createResource(p1+"class4"), Result.TRUE},
                {m1.createResource(p1+"class1-1"),   m1.createResource(p1+"class4"), Result.TRUE},
                {m1.createResource(p1+"class2"),   m1.createResource(p1+"class4"), Result.TRUE},
                {m1.createResource(p1+"class3"),   m1.createResource(p1+"class4"), Result.TRUE},
                {m1.createResource(p1+"class5"),   m1.createResource(p1+"class5"), Result.TRUE},
                {m1.createResource(p1+"class1"),   m1.createResource(p1+"class5"), Result.INDETERMINATE},
                {m1.createResource(p1+"class2"),   m1.createResource(p1+"class5"), Result.INDETERMINATE},
                {m1.createResource(p1+"class3"),   m1.createResource(p1+"class5"), Result.INDETERMINATE},
                {m1.createResource(p1+"class6"),   m1.createResource(p1+"class6"), Result.TRUE},
                {m1.createResource(p1+"class1"),   m1.createResource(p1+"class6"), Result.INDETERMINATE},
                {m1.createResource(p1+"class7"),   m1.createResource(p1+"class7"), Result.TRUE},
                {m1.createResource(p1+"class1"),   m1.createResource(p1+"class7"), Result.INDETERMINATE},
        };
    }

    @Test(dataProvider = "evaluateData")
    public void testEvaluate(Resource subclass, Resource superclass, Result expected)
            throws Exception {
        ClassExpression expression = new ClassExpressionParser().parse(superclass);
        ClassExpressionSubclassEvaluator evaluator;
        evaluator = new ClassExpressionSubclassEvaluator(expression, subclass);

        do {
            evaluator.evaluateStep();
        } while (evaluator.getResult() == Result.UNFINISHED);
        Assert.assertEquals(evaluator.getResult(), expected);
    }
}
package br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.discovery;

import br.ufsc.lapesd.unserved.testbench.Utils;
import br.ufsc.lapesd.unserved.testbench.benchmarks.DescriptionsGenerator;
import br.ufsc.lapesd.unserved.testbench.input.RDFInputFile;
import br.ufsc.lapesd.unserved.testbench.io_composer.impl.DefaultIOComposerTimes;
import br.ufsc.lapesd.unserved.testbench.io_composer.impl.IOComposerInput;
import br.ufsc.lapesd.unserved.testbench.io_composer.impl.IOComposerInputImpl;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.ForwardLayeredServiceGraphBuilder;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.LayeredServiceGraph;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.Node;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFFormat;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.File;
import java.nio.file.Files;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

@Test
public class FastPreconditionsLayersStateTest {
    @Test
    public class CompareTests extends ServiceGraphComparisonTestBase {
        @Override
        protected IOLayersState aLayersState(IOComposerInput composerInput) {
            return new IOLayersState(composerInput);
        }

        @Override
        protected IOLayersState bLayersState(IOComposerInput composerInput) {
            return new FastPreconditionsLayersState(new InputServiceIndex());
        }
    }

    private IOComposerInputImpl getDescriptionGeneratorCI(boolean preconditions) throws Exception {
        File descriptionFile = Files.createTempFile("description", ".ttl").toFile();
        File rulesFile = Files.createTempFile("rules", ".ttl").toFile();
        new DescriptionsGenerator.Builder(2)
                .setUniversal(false)
                .setRepeatRelations(false)
                .setConditions(2)
                .setPreconditions(preconditions)
                .setRel("rel")
                .setSkIRI("http://example.org/skolem#")
                .setUnservedOut(descriptionFile)
                .setRulesOut(rulesFile)
                .build().run();
        Assert.assertTrue(rulesFile.delete());
        IOComposerInputImpl ci = new IOComposerInputImpl();
        ci.addInput(RDFInputFile.createOwningFile(descriptionFile, RDFFormat.TURTLE));
        ci.addInput(Utils.resourceInput(
                "io_composer/description-generator-known.ttl", Lang.TURTLE));
        Utils.addWantedVars(ci, Utils.resourceInput(
                "io_composer/description-generator-wanted.ttl", Lang.TURTLE), null);
        ci.initSkolemizedUnion(true, true);
        return ci;
    }

    @Test
    public void testCompareServiceGraphForDescriptionGenerator()
            throws Exception {
        LayeredServiceGraph ioSG;
        LayeredServiceGraph prSG;
        List<Set<Node>> ioLayers;
        List<Set<Node>> prLayers;
        try (IOComposerInputImpl ioCI = getDescriptionGeneratorCI(false);
             IOComposerInputImpl prCI = getDescriptionGeneratorCI(true)) {

            ForwardLayeredServiceGraphBuilder ioBuilder = new ForwardLayeredServiceGraphBuilder();
            ioBuilder.withLayersStateFactory(IOLayersState::new);
            ioSG = ioBuilder.build(ioCI, new DefaultIOComposerTimes());

            ForwardLayeredServiceGraphBuilder prBuilder = new ForwardLayeredServiceGraphBuilder();
            prBuilder.withLayersStateFactory(aCI -> new FastPreconditionsLayersState(
                    new InputServiceIndex()));
            prSG = prBuilder.build(prCI, new DefaultIOComposerTimes());
        }

        ioLayers = ioSG.getLayers().stream().map(l ->new HashSet<>(l.getNodes())).collect(toList());
        prLayers = prSG.getLayers().stream().map(l ->new HashSet<>(l.getNodes())).collect(toList());
        Assert.assertEquals(prLayers.size(), ioLayers.size());
        for (int i = 0; i < ioLayers.size(); i++) {
            final int layerIdx = i;
            String helper = "layerIdx="+layerIdx;
            Assert.assertEquals(prLayers.get(i), ioLayers.get(i), helper);
            Assert.assertEquals(prLayers.get(i).stream()
                            .filter(n -> prSG.getNodeLayer(n) != layerIdx).collect(toList()),
                    emptyList(), helper);
            Assert.assertEquals(prLayers.get(i).stream()
                    .filter(n -> !prSG.getProviderSet(n).stream().collect(toSet())
                            .equals(ioSG.getProviderSet(n).stream().collect(toSet())))
                    .collect(toList()), emptyList(), helper);
            Assert.assertEquals(ioLayers.get(i).stream()
                            .flatMap(n -> ioSG.getProviderSet(n).stream())
                            .filter(n -> ioSG.getNodeLayer(n) >= layerIdx).collect(toList()),
                    emptyList(), helper);
            Assert.assertEquals(prLayers.get(i).stream()
                            .flatMap(n -> prSG.getProviderSet(n).stream())
                            .filter(n -> prSG.getNodeLayer(n) >= layerIdx).collect(toList()),
                    emptyList(), helper);
        }
    }

}
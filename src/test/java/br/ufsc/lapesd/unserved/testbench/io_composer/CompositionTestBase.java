package br.ufsc.lapesd.unserved.testbench.io_composer;

import br.ufsc.lapesd.unserved.testbench.components.DefaultComponentRegistry;
import br.ufsc.lapesd.unserved.testbench.components.WrappingComponentRegistry;
import br.ufsc.lapesd.unserved.testbench.process.Interpreter;
import br.ufsc.lapesd.unserved.testbench.process.UnservedPInterpreter;
import br.ufsc.lapesd.unserved.testbench.process.actions.mock.MockReceiveRunner;
import br.ufsc.lapesd.unserved.testbench.process.actions.mock.MockSendRunner;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;


public abstract class CompositionTestBase {
    protected IOComposerBuilder builder;
    protected IOComposer composer;
    protected Interpreter interpreter;
    protected WrappingComponentRegistry componentRegistry;

    protected abstract IOComposerBuilder getBuilder();

    @BeforeMethod
    public void setUp() throws Exception {
        builder = getBuilder();

        componentRegistry = new WrappingComponentRegistry(
                DefaultComponentRegistry.getInstance());
        componentRegistry.registerComponent(MockSendRunner.Factory.class);
        componentRegistry.registerComponent(MockReceiveRunner.Factory.class);

        interpreter = new UnservedPInterpreter(componentRegistry);
    }

    @AfterMethod
    public void tearDown() throws Exception {
        builder.close();
        builder = null;
        if (composer != null) {
            composer.close();
            composer = null;
        }
        interpreter = null;
        componentRegistry.close();
        componentRegistry = null;
    }
}

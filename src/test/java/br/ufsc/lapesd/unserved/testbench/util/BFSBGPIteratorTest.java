package br.ufsc.lapesd.unserved.testbench.util;

import br.ufsc.lapesd.unserved.testbench.Utils;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;
import org.apache.jena.riot.Lang;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class BFSBGPIteratorTest {
    static String NS = "http://example.org/ns#";
    static Property p = ResourceFactory.createProperty(NS+"p");

    @DataProvider
    public static Object[][] data() {
        Map<Resource, Set<Resource>> c1 = new HashMap<>();
        c1.put(r(2), new HashSet<>(Arrays.asList(r(2), r(3), r(4))));

        Map<Resource, Set<Resource>> c2 = new HashMap<>();
        c2.put(r(10), new HashSet<>(Arrays.asList(r(45), r(46))));

        Map<Resource, Set<Resource>> c3 = new HashMap<>();
        c3.put(r(22), new HashSet<>(Collections.singletonList(r(23))));

        return new Object[][] {
                {"bfs-1.ttl", 1, Arrays.asList(2, 3, 4), null},
                {"bfs-1.ttl", 10, Arrays.asList(11, 12, 13, 14), null},
                {"bfs-1.ttl", 20, Arrays.asList(20, 21, 22, 23), null},
                {"bfs-1.ttl", 1, Arrays.asList(2, 3, 4), c1},
                {"bfs-1.ttl", 10, Arrays.asList(45, 46), c2},
                {"bfs-1.ttl", 20, Arrays.asList(21, 22, 23), c3},
        };
    }

    private static Resource r(Model m, int id) {
        return m.createResource(NS+id);
    }
    private static Resource r(int id) {
        return ResourceFactory.createResource(NS+id);
    }

    @Test(dataProvider = "data")
    public void test(String name, int start, List<Integer> expected,
                     Map<Resource, Set<Resource>> cache) throws Exception {
        Model m = Utils.resourceInput("util/" + name, Lang.TURTLE).getModel();
        BFSBGPIterator.Builder builder = BFSBGPIterator.from(r(m, start));
        if (cache != null)
            builder.withCache(k -> cache.getOrDefault(k, null));

        Assert.assertEquals(builder.forward(p).toCollection(HashSet::new),
                expected.stream().map(id -> r(m, id)).collect(Collectors.toSet()));
    }

    @Test(dataProvider = "data")
    public void testWithInitial(String name, int start, List<Integer> expected,
                     Map<Resource, Set<Resource>> cache) throws Exception {
        Model m = Utils.resourceInput("util/" + name, Lang.TURTLE).getModel();
        BFSBGPIterator.Builder builder = BFSBGPIterator.from(r(m, start)).withInitial();
        if (cache != null)
            builder.withCache(k -> cache.getOrDefault(k, null));

        Set<Resource> expectedSet = Stream.concat(Stream.of(start), expected.stream())
                .map(id -> r(m, id)).collect(Collectors.toSet());
        Assert.assertEquals(builder.forward(p).toCollection(HashSet::new), expectedSet);
    }

}
package br.ufsc.lapesd.unserved.testbench.io_composer;

import br.ufsc.lapesd.unserved.testbench.io_composer.layered.GreedyBackwardLayeredIOComposer;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.lazy.SingleLeastServicesBackwardSetNodeProvider;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.optimizations.ServiceCompressionOptimizer;
import org.testng.annotations.Test;

@Test
public class GreedyBackwardLayeredIONoAdaptComposerTests {
    public class ApiDemoTests extends ApiDemoTestBase {
        @Override
        protected IOComposerBuilder getBuilder() {
            return new GreedyBackwardLayeredIOComposer.Builder()
                    .setBackwardSetNodeProvider(new SingleLeastServicesBackwardSetNodeProvider());
        }
    }

    public class DescriptionGenerator extends DescriptionGeneratorTestBase {
        @Override
        protected IOComposerBuilder getBuilder() {
            return new GreedyBackwardLayeredIOComposer.Builder()
                    .setBackwardSetNodeProvider(new SingleLeastServicesBackwardSetNodeProvider());
        }
    }

    public class WscConverted extends WscConvertedTestBase {
        @Override
        protected IOComposerBuilder getBuilder() {
            return new GreedyBackwardLayeredIOComposer.Builder()
                    .setBackwardSetNodeProvider(new SingleLeastServicesBackwardSetNodeProvider());
        }
    }

    public class PubSub extends PubSubTestBase {
        @Override
        protected IOComposerBuilder getBuilder() {
            return new GreedyBackwardLayeredIOComposer.Builder()
                    .setBackwardSetNodeProvider(new SingleLeastServicesBackwardSetNodeProvider());
        }
    }

    public class ApiDemoTestsWithCompression extends ApiDemoTestBase {
        @Override
        protected IOComposerBuilder getBuilder() {
            return new GreedyBackwardLayeredIOComposer.Builder()
                    .setBackwardSetNodeProvider(new SingleLeastServicesBackwardSetNodeProvider())
                    .withOptimizer(new ServiceCompressionOptimizer());
        }
    }

    public class DescriptionGeneratorWithCompression extends DescriptionGeneratorTestBase {
        @Override
        protected IOComposerBuilder getBuilder() {
            return new GreedyBackwardLayeredIOComposer.Builder()
                    .setBackwardSetNodeProvider(new SingleLeastServicesBackwardSetNodeProvider())
                    .withOptimizer(new ServiceCompressionOptimizer());
        }
    }

    public class WscConvertedWithCompression extends WscConvertedTestBase {
        @Override
        protected IOComposerBuilder getBuilder() {
            return new GreedyBackwardLayeredIOComposer.Builder()
                    .setBackwardSetNodeProvider(new SingleLeastServicesBackwardSetNodeProvider())
                    .withOptimizer(new ServiceCompressionOptimizer());
        }
    }

    public class PubSubWithCompression extends PubSubTestBase {
        @Override
        protected IOComposerBuilder getBuilder() {
            return new GreedyBackwardLayeredIOComposer.Builder()
                    .setBackwardSetNodeProvider(new SingleLeastServicesBackwardSetNodeProvider())
                    .withOptimizer(new ServiceCompressionOptimizer());
        }
    }
}

package br.ufsc.lapesd.unserved.testbench.httpcomponents;

import br.ufsc.lapesd.unserved.testbench.JaxRSServer;
import br.ufsc.lapesd.unserved.testbench.TestWorkflowRunner;
import br.ufsc.lapesd.unserved.testbench.Utils;
import br.ufsc.lapesd.unserved.testbench.byte_renderers.Render;
import br.ufsc.lapesd.unserved.testbench.byte_renderers.Renderer;
import br.ufsc.lapesd.unserved.testbench.byte_renderers.RendererException;
import br.ufsc.lapesd.unserved.testbench.components.ComponentRegistrationException;
import br.ufsc.lapesd.unserved.testbench.components.DefaultComponentRegistry;
import br.ufsc.lapesd.unserved.testbench.components.WrappingComponentRegistry;
import br.ufsc.lapesd.unserved.testbench.input.RDFInput;
import br.ufsc.lapesd.unserved.testbench.iri.SPARMediaType;
import br.ufsc.lapesd.unserved.testbench.iri.Testbench;
import br.ufsc.lapesd.unserved.testbench.iri.Unserved;
import br.ufsc.lapesd.unserved.testbench.iri.UnservedX;
import br.ufsc.lapesd.unserved.testbench.model.Variable;
import br.ufsc.lapesd.unserved.testbench.process.data.UpdatableRDFInputsDataContext;
import br.ufsc.lapesd.unserved.testbench.rs.helloworld.endpoints.Hello;
import br.ufsc.lapesd.unserved.testbench.util.GeneralizedContentType;
import br.ufsc.lapesd.unserved.testbench.util.UpdatableRDFInputs;
import org.apache.commons.io.IOUtils;
import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.rdf.model.*;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFFormat;
import org.apache.jena.riot.RDFLanguages;
import org.apache.jena.sparql.vocabulary.FOAF;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.XSD;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.annotation.Nullable;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;

@Test
public class HelloTest {
    private JaxRSServer server;
    private WrappingComponentRegistry componentRegistry;
    private TestWorkflowRunner runner;

    @BeforeMethod
    public void setUp() throws IOException, ComponentRegistrationException {
        Unserved.init();
        server = new JaxRSServer(Hello.class);
        componentRegistry = new WrappingComponentRegistry(DefaultComponentRegistry.getInstance());
        componentRegistry.registerComponent(HttpClientSendRunner.Factory.class);
        componentRegistry.registerComponent(HttpClientReceiveRunner.Factory.class);
        runner = new TestWorkflowRunner(componentRegistry);
    }

    @AfterMethod
    public void tearDown() throws Exception {
        server.close();
        componentRegistry.close();
        componentRegistry = null;
        runner = null;
    }


    @Test
    public void testInvokeHello() throws Exception {
        String descrNS = Testbench.IRI + "/rs/helloworld/unserved.ttl#";
        String wfNS = Testbench.IRI + "/httpcomponents/getHelloSequence.ttl#";

        RDFInput description = Utils.resourceInput("rs/helloworld/unserved.ttl", Lang.TURTLE);
        RDFInput workflow = Utils.resourceInput("httpcomponents/getHelloSequence.ttl", Lang.TURTLE);
        try (UpdatableRDFInputsDataContext dataContext = new UpdatableRDFInputsDataContext(
                new UpdatableRDFInputs(Collections.singleton(description.getModel()),
                        RDFFormat.TURTLE))) {
            /* set from1 to the server address */
            Variable from1 = dataContext.getResource(descrNS + "from1", Variable.class);
            Assert.assertNotNull(from1);
            Literal serverHost = ResourceFactory.createTypedLiteral("localhost:"
                    + server.getPort(), XSDDatatype.XSDstring);
            dataContext.setVariable(from1, XSD.xstring, serverHost);

            /* set "Niels Bohr" to in1 */
            Resource in1Resource = dataContext.getResource(descrNS + "in1");
            Assert.assertNotNull(in1Resource);
            Variable in1 = in1Resource.as(Variable.class);
            Literal bohr = ResourceFactory.createTypedLiteral("Niels Bohr", XSDDatatype.XSDstring);
            dataContext.setVariable(in1, XSD.xstring, bohr);

            /* run the workflow and get out1 Variable */
            runner.run(dataContext, workflow, wfNS + "root");
            Resource out1Resource = dataContext.getResource(descrNS + "out1");
            Assert.assertNotNull(out1Resource);
            Variable out1 = out1Resource.as(Variable.class);

            /* Check for ?subj foaf:name "Niels Bohr". */
            Assert.assertNotNull(out1.getValue());
            Model model = readRDF(out1);
            Assert.assertTrue(model.listStatements(null, FOAF.name, bohr).hasNext());
        }
    }

    private Model readRDF(Variable variable) throws RendererException {
        checkRDF(variable.getValueRepresentation(), null);
        Renderer renderer = componentRegistry.createRenderer(variable);
        Render render = renderer.format(variable);
        Assert.assertTrue(render.isString());

        Model model = ModelFactory.createDefaultModel();
        String renderString = new String(render.getBytes(), render.getCharset());
        InputStream inputStream = IOUtils.toInputStream(renderString);
        String mediaType = SPARMediaType.extractMediaType(variable.getValueRepresentation()
                .getProperty(UnservedX.RDF.rdfMediaType).getResource().getURI());
        RDFDataMgr.read(model, inputStream, RDFLanguages.contentTypeToLang(mediaType));
        return model;
    }

    private void checkRDF(Resource rep, @Nullable String mediaType) {
        Assert.assertNotNull(rep);
        Assert.assertTrue(rep.hasProperty(RDF.type, UnservedX.RDF.RDF));

        Statement statement = rep.getProperty(UnservedX.RDF.rdfMediaType);
        if (mediaType != null) {
            Assert.assertEquals(SPARMediaType.extractMediaType(statement.getResource().getURI()),
                    mediaType);
        } else {
            Assert.assertNotNull(statement.getResource());
        }

        statement = rep.getProperty(UnservedX.MediaType.mediaTypeValue);
        if (mediaType != null) {
            GeneralizedContentType ctype;
            ctype = GeneralizedContentType.parse(statement.getLiteral().getString());
            Assert.assertNotNull(ctype);
            Assert.assertTrue(ctype.isConcrete());
            Assert.assertEquals(ctype.getElements()[0].getName(), "text/turtle");
        } else {
            Assert.assertNotNull(statement.getLiteral().getString());
        }
    }
}

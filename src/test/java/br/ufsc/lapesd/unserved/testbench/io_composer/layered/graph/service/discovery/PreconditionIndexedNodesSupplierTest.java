package br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.discovery;

import br.ufsc.lapesd.unserved.testbench.io_composer.WscConvertedTestBase;
import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.And;
import br.ufsc.lapesd.unserved.testbench.io_composer.impl.IOComposerInputImpl;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.EndNode;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.Node;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.ServiceNode;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.StartNode;
import br.ufsc.lapesd.unserved.testbench.model.Message;
import br.ufsc.lapesd.unserved.testbench.util.FixedCacheTransitiveClosureGetter;
import org.apache.jena.vocabulary.RDFS;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static br.ufsc.lapesd.unserved.testbench.Utils.addWantedVars;
import static br.ufsc.lapesd.unserved.testbench.io_composer.WscConvertedTestBase.getWscTurtle;

public class PreconditionIndexedNodesSupplierTest {
    @DataProvider
    public static Object[][] problems() {
        return WscConvertedTestBase.problemNames();
    }

    @Test(dataProvider = "problems")
    public void testCompareWithInputIndexedNodesSupplier(String problem) throws Exception {
        IOComposerInputImpl ci = new IOComposerInputImpl();
        ci.addInput(getWscTurtle(problem, "taxonomy"));
        ci.addInput(getWscTurtle(problem, "services"));
        ci.addInput(getWscTurtle(problem, "known"));
        addWantedVars(ci, getWscTurtle(problem, "wanted"), null);
        ci.initSkolemizedUnion(true, true);
        StartNode start = new StartNode(ci.getKnownAsVariables(), And.empty());
        EndNode end = new EndNode(ci.getWantedAsVariables(), And.empty());

        IOLayersState ioState = new IOLayersState(ci);
        ioState.advance(Collections.singleton(start));

        FixedCacheTransitiveClosureGetter subClassGetter = FixedCacheTransitiveClosureGetter
                .forward(RDFS.subClassOf).visitAll(ci.getSkolemizedUnion()).build();
        PreconditionsLayersState pState = new PreconditionsLayersState(subClassGetter);
        pState.advance(Collections.singleton(start));

        InputIndexedNodesSupplier ii = null;
        PreconditionIndexedNodesSupplier pi = null;
        HashSet<Message> blacklist = new HashSet<>();
        try {
            ii = new InputIndexedNodesSupplier();
            ii.begin(ci, start, end);
            pi = new PreconditionIndexedNodesSupplier();
            pi.begin(ci, start, end);

            for (int layerIdx = 0; true; layerIdx++) {
                Set<Node> iiNodes = ii.getNodes(ioState, blacklist);
                Set<Node> piNodes = pi.getNodes(pState, blacklist);
                Assert.assertEquals(piNodes, iiNodes, "layerIdx="+layerIdx);

                iiNodes.stream().filter(n -> n instanceof ServiceNode)
                        .forEach(n -> blacklist.add(((ServiceNode)n).getConsequent()));
                if (iiNodes.isEmpty()) break;
            }

        } finally {
            if (pi != null) pi.end();
            if(ii != null) ii.end();
        }
    }
}
package br.ufsc.lapesd.unserved.testbench.util;

import br.ufsc.lapesd.unserved.testbench.input.RDFInput;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.riot.Lang;
import org.testng.Assert;
import org.testng.annotations.Test;

import static br.ufsc.lapesd.unserved.testbench.Utils.resourceInput;

public class UnservedReasoningTest {
    @Test
    public void test() throws Exception {
        Model in = resourceInput("util/UnservedReasoning-in.ttl",  Lang.TTL).takeModel(),
             out = resourceInput("util/UnservedReasoning-out.ttl", Lang.TTL).takeModel();
        Model wrapped = UnservedReasoning.wrap(in);
        for (StmtIterator it = out.listStatements(); it .hasNext(); ) {
            Statement statement = it.next();
            Assert.assertTrue(wrapped.contains(statement), "Missing " + statement);
        }
    }

    @Test
    public void testRDFInput() throws Exception {
        RDFInput in = resourceInput("util/UnservedReasoning-in.ttl", Lang.TTL);
        Model out = resourceInput("util/UnservedReasoning-out.ttl", Lang.TTL).takeModel();
        Model wrapped = UnservedReasoning.wrap(in).takeModel();
        for (StmtIterator it = out.listStatements(); it .hasNext(); ) {
            Statement statement = it.next();
            Assert.assertTrue(wrapped.contains(statement), "Missing " + statement);
        }
    }
}
package br.ufsc.lapesd.unserved.testbench.rs.helloworld.config;

import br.ufsc.lapesd.unserved.testbench.rs.helloworld.endpoints.Hello;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.Collections;
import java.util.Set;

@ApplicationPath("hello-app")
public class HelloApplication extends Application {
    @Override
    public Set<Class<?>> getClasses() {
        return Collections.singleton(Hello.class);
    }
}

package br.ufsc.lapesd.unserved.testbench.io_composer.conditions.index;

import br.ufsc.lapesd.unserved.testbench.Utils;
import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.atomic.TriplePattern;
import br.ufsc.lapesd.unserved.testbench.util.NaiveTransitiveClosureGetter;
import br.ufsc.lapesd.unserved.testbench.util.UnservedReasoning;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.riot.Lang;
import org.apache.jena.vocabulary.RDFS;
import org.spinrdf.vocabulary.SP;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class UnservedTriplePatternIndexTest {

    private static class TPInfo {
        Resource resource;
        TriplePattern tp;
        String name;
        String group;
        TriplePattern main;
        int value;
        boolean matches;

        public TPInfo(Resource resource) {
            this.resource = resource;
            Matcher mt = Pattern.compile("(.*)(\\w+)(\\d+)(-?)").matcher(resource.getURI());
            Assert.assertTrue(mt.matches());
            this.tp = TriplePattern.fromSPIN(resource);
            this.name = mt.group(2) + mt.group(3) + mt.group(4);
            this.group = mt.group(2);
            Assert.assertEquals(group.length(), 1);
            this.value = (group.charAt(0)-'a') * 100 + Integer.parseInt(mt.group(3));

            String mainUri = mt.group(1) + mt.group(2) + 1;
            this.main = TriplePattern.fromSPIN(resource.getModel().createResource(mainUri));
            this.matches = mt.group(4).isEmpty();
        }
    }

    @Test
    public void test() throws Exception {
        Model m = UnservedReasoning.wrap(Utils.resourceInput("io_composer/conditions/index" +
                "/UnservedTriplePatternIndex-1.ttl", Lang.TTL).takeModel());
        NaiveTransitiveClosureGetter closureGetter, propClosureGetter;
        closureGetter = new NaiveTransitiveClosureGetter(RDFS.subClassOf, false);
        propClosureGetter = new NaiveTransitiveClosureGetter(RDFS.subPropertyOf, false);
        UnservedTriplePatternIndex<Integer> index;
        index = new UnservedTriplePatternIndex<>(() -> new VariableSpecIndex<>(closureGetter),
                () -> new TransitiveIndex<>(closureGetter),
                () -> new TransitiveIndex<>(propClosureGetter));

        List<TPInfo> data = m.listSubjectsWithProperty(SP.subject).toList().stream()
                .map(TPInfo::new).collect(Collectors.toList());
        data.forEach(i -> index.put(i.tp, i.value));

        List<String> names = data.stream()
                .filter(i -> index.get(i.main).contains(i.value) != i.matches)
                .map(i -> i.name).collect(Collectors.toList());
        Assert.assertEquals(names, Collections.emptyList(), names.toString());
    }

}
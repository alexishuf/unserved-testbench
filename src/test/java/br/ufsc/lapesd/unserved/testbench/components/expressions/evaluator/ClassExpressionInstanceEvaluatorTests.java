package br.ufsc.lapesd.unserved.testbench.components.expressions.evaluator;

import br.ufsc.lapesd.unserved.testbench.Utils;
import br.ufsc.lapesd.unserved.testbench.components.expressions.ClassExpression;
import br.ufsc.lapesd.unserved.testbench.components.expressions.compiler.ClassExpressionParser;
import br.ufsc.lapesd.unserved.testbench.components.expressions.evaluator.ClassExpressionEvaluator.Result;
import br.ufsc.lapesd.unserved.testbench.input.RDFInput;
import org.apache.jena.rdf.model.*;
import org.apache.jena.riot.Lang;
import org.apache.jena.vocabulary.XSD;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;


public class ClassExpressionInstanceEvaluatorTests {
    @DataProvider
    public static Object[][] evaluateData() throws IOException {
        RDFInput in1 = Utils.resourceInput("components/expressions/instance-test.ttl", Lang.TURTLE);
        String p1 = "http://example.com/#";
        Model m1 = in1.getModel();
        Property prop1 = m1.createProperty(p1 + "prop1").as(Property.class);
        Literal lit1 = m1.createResource(p1 + "tag1").getProperty(prop1).getLiteral();
        Literal lit2 = m1.createResource(p1 + "tag2").getProperty(prop1).getLiteral();

        return new Object[][] {
                {m1.createResource(p1 + "class1"), m1.createResource(p1 + "ind1"),   Result.TRUE},
                {m1.createResource(p1 + "class2"), m1.createResource(p1 + "ind2-1"), Result.TRUE},
                {m1.createResource(p1 + "class2"), m1.createResource(p1 + "ind2-2"), Result.TRUE},
                {m1.createResource(p1 + "class2"), m1.createResource(p1 + "bad2-1"), Result.INDETERMINATE},
                {m1.createResource(p1 + "class2"), m1.createResource(p1 + "bad2-2"), Result.INDETERMINATE},
                {m1.createResource(p1 + "class2"), m1.createResource(p1 + "ind3-1"), Result.TRUE},
                {m1.createResource(p1 + "class3"), m1.createResource(p1 + "ind3-1"), Result.TRUE},
                {m1.createResource(p1 + "class3"), m1.createResource(p1 + "ind3-2"), Result.TRUE},
                {m1.createResource(p1 + "class3"), m1.createResource(p1 + "bad3-1"), Result.INDETERMINATE},
                {m1.createResource(p1 + "class3"), m1.createResource(p1 + "bad3-2"), Result.INDETERMINATE},
                {m1.createResource(p1 + "class4"), m1.createResource(p1 + "bad4-1"), Result.INDETERMINATE},
                {m1.createResource(p1 + "class4"), m1.createResource(p1 + "bad4-2"), Result.INDETERMINATE},
                {m1.createResource(XSD.xstring.getURI()), lit1,                      Result.TRUE},
                {m1.createResource(XSD.integer.getURI()), lit1,                      Result.INDETERMINATE},
                {m1.createResource(p1 + "class5"),        lit2,                      Result.TRUE},
                {m1.createResource(XSD.xstring.getURI()), lit2,                      Result.INDETERMINATE},
        };
    }

    @Test(dataProvider = "evaluateData")
    public void testEvaluate(Resource aClass, RDFNode aInstance, Result expected) throws Exception {
        ClassExpression expression = new ClassExpressionParser().parse(aClass);
        ClassExpressionInstanceEvaluator evaluator;
        evaluator = new ClassExpressionInstanceEvaluator(expression, aInstance);

        do{
            evaluator.evaluateStep();
        } while (evaluator.getResult() == Result.UNFINISHED);
        Assert.assertEquals(evaluator.getResult(), expected);
    }

}
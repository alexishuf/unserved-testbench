package br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service;

import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.And;
import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.atomic.TriplePattern;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.cost.CostInfo;
import br.ufsc.lapesd.unserved.testbench.model.Message;
import br.ufsc.lapesd.unserved.testbench.model.Variable;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.riot.Lang;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.ArrayList;
import java.util.NoSuchElementException;

import static br.ufsc.lapesd.unserved.testbench.Utils.resourceInput;
import static br.ufsc.lapesd.unserved.testbench.util.UnservedReasoning.wrap;
import static java.util.stream.Collectors.toSet;
import static java.util.stream.Stream.of;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertThrows;

public class ProviderSetUnionTest {
    private Model m;
    IOProviderHashSet a, b, c;
    ConditionProviderHashSet d;

    @BeforeMethod
    public void setUp() throws IOException {
        m = wrap(resourceInput("io_composer/ProviderSetUnionTest.ttl", Lang.TTL)).takeModel();
        a = new IOProviderHashSet();
        b = new IOProviderHashSet();
        c = new IOProviderHashSet();
        d = new ConditionProviderHashSet();
    }

    @AfterMethod
    public void tearDown() {
        m.close();
        a = b = c = null;
        d = null;
    }

    private Resource r(@Nonnull String lName) {
        return m.createResource("http://example.org/ns#" + lName);
    }

    private Variable v(@Nonnull String lName) { return r(lName).as(Variable.class); }
    private Message m(@Nonnull String lName) { return r(lName).as(Message.class); }
    private TriplePattern tp(@Nonnull String lName) { return TriplePattern.fromSPIN(r(lName)); }
    private ServiceNode n(int id) {
        return new ServiceNode(m("rq"+id), m("rp"+id), new ArrayList<>(),
                And.empty(), And.empty(), CostInfo.EMPTY);
    }

    @Test
    public void testEnforceUniqueTargets() {
        Assert.assertTrue(getClass().desiredAssertionStatus());
        a.addTarget(v("x1"));
        b.addTarget(v("x1"));
        c.addTarget(v("x1")).add(v("y1"), n(1), v("y1"));
        ProviderSetUnion union = new ProviderSetUnion().add(a);
        Assert.assertThrows(AssertionError.class, () -> union.add(b));
        Assert.assertThrows(AssertionError.class, () -> union.add(c));
    }

    @Test
    public void testGetNodes() {
        a.addTarget(v("x1")).add(v("x1"), n(1), v("y1"));
        b.addTarget(v("x2")).add(v("x2"), n(1), v("y2"));
        c.addTarget(v("x3")).add(v("x3"), n(2), v("y3"));
        ProviderSetUnion u = new ProviderSetUnion().add(a).add(b);
        assertEquals(u.stream().collect(toSet()), of(n(1)).collect(toSet()));
        u.add(c);
        assertEquals(u.stream().collect(toSet()), of(n(1), n(2)).collect(toSet()));
    }

    @Test
    public void testMixConditions() {
        a.addTarget(v("x1")).add(v("x1"), n(1), v("y1"));
        d.addTarget(v("x2")).add(v("x2"), n(1), v("y2"));
        d.addTarget(v("x3")).add(v("x3"), n(2), v("y3"));
        d.addCondition(tp("tpx1")).add(tp("tpx1"), n(1), tp("tpy1"));
        d.addCondition(tp("tpx2")).add(tp("tpx2"), n(1), tp("tpy2"));
        d.addCondition(tp("tpx3")).add(tp("tpx3"), n(2), tp("tpy3"));
        ProviderSetUnion u = new ProviderSetUnion().add(a).add(d);
        assertEquals(u.stream().collect(toSet()), of(n(1), n(2)).collect(toSet()));

        assertEquals(u.getPostcondition(tp("tpx1"), n(1)), tp("tpy1"));
        assertThrows(NoSuchElementException.class, () -> u.getPostcondition(tp("tpx1"), n(2)));
        assertEquals(u.getPostcondition(tp("tpx3"), n(2)), tp("tpy3"));
        assertEquals(u.getOutput(v("x1"), n(1)), v("y1"));
        assertThrows(NoSuchElementException.class, () -> u.getOutput(v("x2"), n(2)));
        assertEquals(u.getOutput(v("x3"), n(2)), v("y3"));
    }
}
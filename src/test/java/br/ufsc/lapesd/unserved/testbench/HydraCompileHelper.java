package br.ufsc.lapesd.unserved.testbench;

import br.ufsc.lapesd.unserved.testbench.hydra.HydraFrontend;
import br.ufsc.lapesd.unserved.testbench.input.RDFInput;
import br.ufsc.lapesd.unserved.testbench.input.RDFInputModel;
import br.ufsc.lapesd.unserved.testbench.reasoner.N3ReasonerException;
import br.ufsc.lapesd.unserved.testbench.util.Skolemizer;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFFormat;

import java.io.InputStream;
import java.util.Collections;

/**
 * Utility class to skolemize and compile Hydra api descriptions.
 */
public class HydraCompileHelper {
    public final RDFInput compiled;
    public final Model skolemizedInput;

    public HydraCompileHelper(Model skolemizedInput, RDFInput compiled) {
        this.skolemizedInput = skolemizedInput;
        this.compiled = compiled;
    }

    public static HydraCompileHelper compileResource(String path) throws N3ReasonerException, UnservedTranslatorException {
        return compile(HydraCompileHelper.class.getClassLoader().getResourceAsStream(path));
    }

    public static HydraCompileHelper compile(InputStream inputStream) throws N3ReasonerException, UnservedTranslatorException {
        Model apiDemoSource = ModelFactory.createDefaultModel();
        apiDemoSource.read(inputStream, null, Lang.JSONLD.getName());
        Model skolemized = ModelFactory.createDefaultModel();
        new Skolemizer().skolemizeModels(skolemized, apiDemoSource);

        //compile api-demo.jsonld
        HydraFrontend frontend = new HydraFrontend();
        RDFInput compiled = frontend.compile(Collections.singletonList(
                new RDFInputModel(skolemized, RDFFormat.TURTLE)));

        return new HydraCompileHelper(skolemized, compiled);
    }
}

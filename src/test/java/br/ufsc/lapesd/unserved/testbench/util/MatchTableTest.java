package br.ufsc.lapesd.unserved.testbench.util;

import br.ufsc.lapesd.unserved.testbench.iri.Testbench;
import br.ufsc.lapesd.unserved.testbench.util.MatchTable.Relation;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.*;
import java.util.stream.Collectors;

public class MatchTableTest {
    public static final int TYPE_COUNT = 100;
    private MatchTable table;
    private Model model;
    private List<Resource> types;

    @DataProvider
    public static Object[][] getData() {
        return new Object[][] {
                {1, 1, Relation.SAME},
                {1, 2, Relation.MISMATCH},
                {1, 3, Relation.SUPERCLASS},
                {3, 4, Relation.SUPERCLASS},
                {1, 4, Relation.SUBCLASS},
        };
    }

    @DataProvider
    public static Object[][] rightData() {
        return new Object[][] {
                {1, Relation.SUPERCLASS, Collections.singletonList(3)},
                {3, Relation.SUPERCLASS, Collections.singletonList(4)},
                {4, Relation.SUPERCLASS, Collections.singletonList(1)}, //yes, it is a loop
                {4, Relation.SUBCLASS, Collections.singletonList(3)},
                {3, Relation.SUBCLASS, Collections.singletonList(1)},
                {2, Relation.SAME, Collections.singletonList(2)},
        };
    }

    @DataProvider
    public static Object[][] closureData() {
        return new Object[][] {
                {1, Relation.SUPERCLASS, Arrays.asList(1, 3, 4)},
                {3, Relation.SUPERCLASS, Arrays.asList(1, 3, 4)},
                {4, Relation.SUPERCLASS, Arrays.asList(1, 3, 4)},
                {1, Relation.SUBCLASS, Arrays.asList(1, 4, 3)},
                {3, Relation.SUBCLASS, Arrays.asList(3, 1, 4)},
                {4, Relation.SUBCLASS, Arrays.asList(4, 3, 1)},

                {10, Relation.SUPERCLASS, Arrays.asList(10, 11, 12, 13)},
                {11, Relation.SUPERCLASS, Arrays.asList(11, 12, 13)},
                {12, Relation.SUPERCLASS, Arrays.asList(12, 13)},
                {10, Relation.SUBCLASS, Collections.singletonList(10)},
                {11, Relation.SUBCLASS, Arrays.asList(11, 10)},
                {12, Relation.SUBCLASS, Arrays.asList(12, 11, 10)},
                {13, Relation.SUBCLASS, Arrays.asList(13, 12, 11, 10)},
        };
    }

    @BeforeMethod
    public void setUp() throws Exception {
        model = ModelFactory.createDefaultModel();
        model.setNsPrefix("rdf", RDF.getURI());
        model.setNsPrefix("rdfs", RDFS.getURI());
        types = new ArrayList<>(TYPE_COUNT);
        BitSetMatchTable.Builder b = new BitSetMatchTable.Builder();
        for (int i = 0; i < TYPE_COUNT; i++) {
            Resource type = model.createResource(Testbench.PREFIX + i)
                    .addProperty(RDF.type, RDFS.Class);
            types.add(type);
            b.add(type);
        }
        table = b.build();

        table.set(types.get(1), types.get(2), Relation.MISMATCH);
        table.set(types.get(1), types.get(3), Relation.SUPERCLASS);
        table.set(types.get(3), types.get(4), Relation.SUPERCLASS);
        table.set(types.get(1), types.get(4), Relation.SUBCLASS);

        table.set(types.get(10), types.get(11), Relation.SUPERCLASS);
        table.set(types.get(11), types.get(12), Relation.SUPERCLASS);
        table.set(types.get(12), types.get(13), Relation.SUPERCLASS);
    }

    @AfterMethod
    public void tearDown() throws Exception {
        table = null;
        model.close();
        model = null;
        types.clear();
    }

    @Test
    public void testAllUnknown() {
        MatchTable table = new BitSetMatchTable.Builder().addAll(types).build();
        for (int i = 0; i < TYPE_COUNT; i++) {
            for (int j = 0; j < TYPE_COUNT; j++) {
                Relation expected = i == j ? Relation.SAME : Relation.UNKNOWN;
                Assert.assertEquals(table.get(types.get(i), types.get(j)), expected);
            }
        }
    }

    @Test(dataProvider = "getData")
    public void testGet(int left, int right, Relation expected) {
        Assert.assertEquals(table.get(types.get(left), types.get(right)), expected);
    }

    @Test(dataProvider = "getData")
    public void testGetReverse(int left, int right, Relation expected) {
        if (expected == Relation.SUBCLASS) expected = Relation.SUPERCLASS;
        else if (expected == Relation.SUPERCLASS) expected = Relation.SUBCLASS;
        Assert.assertEquals(table.get(types.get(right), types.get(left)), expected);
    }
    @Test
    public void testSet() {
        for (int i = 0; i < TYPE_COUNT; i++) {
            for (int j = 0; j < TYPE_COUNT; j++) {
                if (i == j) continue;
                MatchTable table = new BitSetMatchTable.Builder().addAll(types).build();
                table.set(types.get(i), types.get(j), Relation.SUPERCLASS);
                Assert.assertEquals(table.get(types.get(i), types.get(j)), Relation.SUPERCLASS);
                Assert.assertEquals(table.get(types.get(j), types.get(i)), Relation.SUBCLASS);
            }
        }
    }

    @Test
    public void testSetAll() {
        for (int i = 0; i < TYPE_COUNT; i++) {
            for (int j = i+1; j < TYPE_COUNT; j++) {
                table.set(types.get(i), types.get(j), Relation.SUPERCLASS);
                for (int k = 0; k <= i; k++) {
                    for (int l = k + 1; l <= j; l++) {
                        Assert.assertEquals(table.get(types.get(k), types.get(l)), Relation.SUPERCLASS);
                        Assert.assertEquals(table.get(types.get(l), types.get(k)), Relation.SUBCLASS);
                    }
                }
            }
        }
    }

    @Test(dataProvider = "rightData")
    public void testRight(int resourceIdx, Relation relation, List<Integer> expectedIndices) {
        Set<Resource> actual, expected;
        actual = table.rightStream(types.get(resourceIdx), relation).collect(Collectors.toSet());
        expected = expectedIndices.stream().map(types::get).collect(Collectors.toSet());
        Assert.assertEquals(actual, expected);
    }

    @Test(dataProvider = "closureData")
    public void testClosure(int resourceIdx, Relation relation, List<Integer> expectedIndices) {
        Set<Resource> actual, expected;
        actual = table.closureStream(types.get(resourceIdx), relation).collect(Collectors.toSet());
        expected = expectedIndices.stream().map(types::get).collect(Collectors.toSet());
        Assert.assertEquals(actual, expected);
    }

    @Test
    public void testSetSelfNotSame() {
        boolean caught;
        try {
            caught = false;
            table.set(types.get(50), types.get(50), Relation.MISMATCH);
        } catch (IllegalArgumentException e) {
            caught = true;
        }
        Assert.assertTrue(caught);

        try {
            caught = false;
            table.set(types.get(50), types.get(50), Relation.SUBCLASS);
        } catch (IllegalArgumentException e) {
            caught = true;
        }
        Assert.assertTrue(caught);

        try {
            caught = false;
            table.set(types.get(50), types.get(50), Relation.SUPERCLASS);
        } catch (IllegalArgumentException e) {
            caught = true;
        }
        Assert.assertTrue(caught);

        try {
            caught = false;
            table.set(types.get(50), types.get(50), Relation.UNKNOWN);
        } catch (IllegalArgumentException e) {
            caught = true;
        }
        Assert.assertTrue(caught);
    }

    @Test
    public void testSetSelfSame() {
        boolean caught = false;
        try {
            table.set(types.get(50), types.get(50), Relation.SAME);
        } catch (IllegalArgumentException e) {
            caught = true;
        }
        Assert.assertFalse(caught);
    }

    @Test
    public void testSetSame() {
        boolean caught = false;
        try {
            table.set(types.get(50), types.get(51), Relation.SAME);
        } catch (IllegalArgumentException e) {
            caught = true;
        }
        Assert.assertTrue(caught);
    }

    @Test
    public void testSetUnknown() {
        boolean caught = false;
        try {
            table.set(types.get(50), types.get(51), Relation.UNKNOWN);
            table.set(types.get(50), types.get(51), Relation.SUBCLASS);
            Assert.assertEquals(table.get(types.get(50), types.get(51)), Relation.SUBCLASS);
            table.set(types.get(50), types.get(51), Relation.UNKNOWN);
        } catch (IllegalArgumentException e) {
            caught = true;
        }
        Assert.assertEquals(table.get(types.get(50), types.get(51)), Relation.UNKNOWN);
        Assert.assertFalse(caught);
    }

}
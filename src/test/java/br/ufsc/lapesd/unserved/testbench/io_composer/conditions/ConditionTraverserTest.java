package br.ufsc.lapesd.unserved.testbench.io_composer.conditions;

import org.testng.Assert;
import org.testng.annotations.Test;

import javax.annotation.Nonnull;
import java.util.Arrays;

public class ConditionTraverserTest {

    @Test
    public void testStartFromReflection() {
        Boolean[] calls = {false, false, false};
        ConditionTraverser.visit(new And(), new ConditionVisitor() {
            @Override
            public boolean visit(@Nonnull Class<? extends Condition> aClass, @Nonnull Condition cond) {
                return calls[0] = true;
            }

            @Override
            public boolean visitAnd(@Nonnull And cond) {
                return calls[1] = true;
            }

            @Override
            public boolean visitConditionList(@Nonnull ConditionList cond) {
                return calls[2] = true;
            }
        });
        Assert.assertEquals(Arrays.asList(calls), Arrays.asList(true, false, false));
    }

    @Test
    public void testFallbackToConditionList() {
        Boolean[] calls = {false, false, false, false};
        ConditionTraverser.visit(new Or(), new ConditionVisitor() {
            @Override
            public boolean visit(@Nonnull Class<? extends Condition> aClass, @Nonnull Condition cond) {
                calls[0] = true;
                return false;
            }

            @Override
            public boolean visitConditionList(@Nonnull ConditionList cond) {
                return calls[1] = true;
            }

            @Override
            public boolean visitAnd(@Nonnull And cond) {
                return calls[2] = true;
            }

            @Override
            public boolean visitOr(@Nonnull Or cond) {
                calls[3] = true;
                return false;
            }
        });
        Assert.assertEquals(Arrays.asList(calls), Arrays.asList(true, true, false, true));
    }
}
package br.ufsc.lapesd.unserved.testbench.io_composer;

import br.ufsc.lapesd.unserved.testbench.io_composer.backward.OutputIndexedBackwardGraphAccessor;
import br.ufsc.lapesd.unserved.testbench.io_composer.backward.ReplicatedBackwardIOAStar;
import org.testng.annotations.Test;

@Test
public class OutputIndexedReplicatedBackwardIOAStarTests {
    @Test
    public class ApiDemoTests extends ApiDemoTestBase {
        @Override
        protected IOComposerBuilder getBuilder() {
            return new ReplicatedBackwardIOAStar.Builder()
                    .setAccessorFactory(OutputIndexedBackwardGraphAccessor::new);
        }
    }

    @Test
    public class DescriptionGenerator extends DescriptionGeneratorTestBase {
        @Override
        protected IOComposerBuilder getBuilder() {
            return new ReplicatedBackwardIOAStar.Builder()
                    .setAccessorFactory(OutputIndexedBackwardGraphAccessor::new);
        }
    }
}

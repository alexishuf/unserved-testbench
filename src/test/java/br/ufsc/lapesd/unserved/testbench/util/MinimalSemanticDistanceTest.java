package br.ufsc.lapesd.unserved.testbench.util;

import br.ufsc.lapesd.unserved.testbench.iri.Testbench;
import br.ufsc.lapesd.unserved.testbench.iri.Unserved;
import br.ufsc.lapesd.unserved.testbench.model.Variable;
import com.google.common.base.Preconditions;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.*;
import java.util.stream.Collectors;

import static br.ufsc.lapesd.unserved.testbench.util.MatchTable.Relation.*;
import static java.lang.Integer.parseInt;

public class MinimalSemanticDistanceTest {
    private Model model;
    private Map<String, Variable> variables;
    private List<Resource> roots;
    private Property[] childProperties;

    @DataProvider
    public static Object[][] selectData() {
        List<MatchTable.Relation> sameSub = Arrays.asList(SAME, SUBCLASS);
        List<MatchTable.Relation> sameSubSup = Arrays.asList(SAME, SUBCLASS, SUPERCLASS);
        List<MatchTable.Relation> subSup = Arrays.asList(SUBCLASS, SUPERCLASS);
        List<MatchTable.Relation> subSupSame = Arrays.asList(SUBCLASS, SUPERCLASS, SAME);
        List<MatchTable.Relation> supSub = Arrays.asList(SUPERCLASS, SUBCLASS);
        List<MatchTable.Relation> supSubSame = Arrays.asList(SUPERCLASS, SUBCLASS, SAME);
        List<MatchTable.Relation> sub = Collections.singletonList(SUBCLASS);
        List<MatchTable.Relation> sup = Collections.singletonList(SUPERCLASS);
        List<MatchTable.Relation> same = Collections.singletonList(SAME);

        return new Object[][] {
                {Arrays.asList("r0", "r1"), "v0", "r0", same},
                {Arrays.asList("r0", "r1"), "v0", "r0", sameSub},
                {Arrays.asList("r0", "r1"), "v0", null, sup},
                {Arrays.asList("r1", "r10"), "v0", null, sameSub},
                {Arrays.asList("r01", "r10"), "v0", "r01", sameSub},
                {Arrays.asList("r0", "r01", "r10"), "v0", "r0", sameSub},
                {Arrays.asList("r0", "r01", "r10"), "v0", "r01", sub},
                {Arrays.asList("r01", "r10"), "v0", null, same},

                {Arrays.asList("r0", "r01", "r010", "r10"), "v01", "r01", sameSubSup},
                {Arrays.asList("r0", "r01", "r010", "r10"), "v01", "r010", subSup},
                {Arrays.asList("r0", "r01", "r010", "r10"), "v01", "r010", subSupSame},
                {Arrays.asList("r0", "r01", "r010", "r10"), "v01", "r0", sup},
                {Arrays.asList("r0", "r01", "r010", "r10"), "v01", "r0", supSub},
                {Arrays.asList("r0", "r01", "r010", "r10"), "v01", "r0", supSubSame},
        };
    }

    @BeforeMethod
    public void setUp() throws Exception {
        model = ModelFactory.createDefaultModel();
        childProperties = new Property[] {
                model.createProperty(Testbench.PREFIX + "child1"),
                model.createProperty(Testbench.PREFIX + "child2")};

        roots = Arrays.asList(createClass("0"), createClass("1"));
        for (Resource root : roots) {
            Stack<ImmutablePair<Resource, Integer>> stack = new Stack<>();
            stack.push(ImmutablePair.of(root, 0));
            while (!stack.isEmpty()) {
                ImmutablePair<Resource, Integer> state = stack.pop();
                for (int i = 0; state.getValue() < 3 && i < 2; i++) {
                    String name = state.getKey().getURI().split("#")[1] + String.valueOf(i);
                    Resource child = createClass(name).addProperty(RDFS.subClassOf, state.getKey());
                    state.getKey().addProperty(childProperties[i], child);
                    stack.push(ImmutablePair.of(child, state.getValue() + 1));
                }
            }
        }

        variables = new HashMap<>();
        createVar("r0", "0", Unserved.Resource);
        createVar("r1", "1", Unserved.Resource);
        createVar("r10", "10", Unserved.Resource);
        createVar("r01", "01", Unserved.Resource);
        createVar("r010", "010", Unserved.Resource);

        createVar("v0", "0", Unserved.Resource);
        createVar("v01", "01", Unserved.Resource);
    }

    private Resource createClass(String name) {
        return model.createResource(Testbench.PREFIX + name).addProperty(RDF.type, RDFS.Class);
    }

    private Variable createVar(String name, String typeAddress, Resource representation) {
        Resource r = model.createResource(Testbench.PREFIX + name)
                .addProperty(RDF.type, Unserved.Variable)
                .addProperty(Unserved.type, getType(typeAddress));
        if (representation != null)
                r.addProperty(Unserved.representation, representation);
        Variable var = r.as(Variable.class);
        variables.put(name, var);
        return var;
    }

    private Resource getType(String a) {
        Preconditions.checkArgument(!a.isEmpty());

        Resource type = roots.get(parseInt(a.substring(0, 1)));
        for (int i = 1; i < a.length(); i++)
            type = type.getPropertyResourceValue(childProperties[parseInt(a.substring(i, i+1))]);
        return type;
    }

    @AfterMethod
    public void tearDown() throws Exception {
        model.close();
        model = null;
        variables = null;
        roots = null;
        childProperties = null;
    }

    @Test(dataProvider = "selectData")
    public void testSelect(Collection<String> candidateNames, String variableName,
                           String expectedName, List<MatchTable.Relation> preferences) {
        List<Variable> candidates = candidateNames.stream().map(variables::get)
                .collect(Collectors.toList());
        Variable variable = variables.get(variableName);
        Variable expected = expectedName == null ? null : variables.get(expectedName);
        Variable actual = new MinimalSemanticDistance().withPreferences(preferences)
                .select(candidates, variable);
        Assert.assertEquals(actual, expected);
    }

}
package br.ufsc.lapesd.unserved.testbench.io_composer;

import br.ufsc.lapesd.unserved.testbench.io_composer.layered.BackwardAStarLayeredIOComposer;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.lazy.NaiveBackwardSetNodeProvider;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.lazy.NonEquivalentBackwardSetNodeProvider;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.optimizations.BackwardClosureOptimizer;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.optimizations.ServiceCompressionOptimizer;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.discovery.FastIOLayersState;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.discovery.FastPreconditionsLayersState;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.discovery.InputServiceIndex;
import br.ufsc.lapesd.unserved.testbench.util.FixedCacheTransitiveClosureGetter;
import br.ufsc.lapesd.unserved.testbench.util.NaiveTransitiveClosureGetter;
import org.apache.jena.vocabulary.RDFS;
import org.testng.annotations.Test;

@Test
public class BackwardAStarLayeredIOComposerTests {
    public class ApiDemoTests extends ApiDemoTestBase {
        @Override
        protected IOComposerBuilder getBuilder() {
            return new BackwardAStarLayeredIOComposer.Builder();
        }
    }

    public class DescriptionGenerator extends DescriptionGeneratorTestBase {
        @Override
        protected IOComposerBuilder getBuilder() {
            return new BackwardAStarLayeredIOComposer.Builder();
        }
    }

    public class WscConverted extends WscConvertedTestBase {
        @Override
        protected IOComposerBuilder getBuilder() {
            return new BackwardAStarLayeredIOComposer.Builder();
        }
    }

    public class ConfigmateConverted extends ConfigmateConvertedTestBase {
        @Override
        protected IOComposerBuilder getBuilder() {
            return new BackwardAStarLayeredIOComposer.Builder()
                    .setBackwardSetNodeProvider(new NaiveBackwardSetNodeProvider()
                            .setUseConditions(true))
                    .setLayersStateFactory(ci -> new FastPreconditionsLayersState(
                            new InputServiceIndex(new NaiveTransitiveClosureGetter(
                                    RDFS.subClassOf, false), true)));
        }
    }

    public class PubSub extends PubSubTestBase {
        @Override
        protected IOComposerBuilder getBuilder() {
            return new BackwardAStarLayeredIOComposer.Builder();
        }
    }

    public class ApiDemoTestsWithCompression extends ApiDemoTestBase {
        @Override
        protected IOComposerBuilder getBuilder() {
            return new BackwardAStarLayeredIOComposer.Builder()
                    .withOptimizer(new ServiceCompressionOptimizer());
        }
    }

    public class DescriptionGeneratorWithCompression extends DescriptionGeneratorTestBase {
        @Override
        protected IOComposerBuilder getBuilder() {
            return new BackwardAStarLayeredIOComposer.Builder()
                    .withOptimizer(new ServiceCompressionOptimizer());
        }
    }

    public class WscConvertedWithCompression extends WscConvertedTestBase {
        @Override
        protected IOComposerBuilder getBuilder() {
            return new BackwardAStarLayeredIOComposer.Builder()
                    .withOptimizer(new ServiceCompressionOptimizer());
        }
    }

    public class WscConvertedWithBackClosureOptimizer extends WscConvertedTestBase {
        @Override
        protected IOComposerBuilder getBuilder() {
            return new BackwardAStarLayeredIOComposer.Builder()
                    .withOptimizer(new BackwardClosureOptimizer());
        }
    }

    public class WscConvertedWithServiceCompressionAndBackClosureOptimizer extends WscConvertedTestBase {
        @Override
        protected IOComposerBuilder getBuilder() {
            return new BackwardAStarLayeredIOComposer.Builder()
                    .withOptimizer(new ServiceCompressionOptimizer())
                    .withOptimizer(new BackwardClosureOptimizer());
        }
    }

    public class WscConvertedWithServiceCompressionAndBackClosureOptimizerAndCombinedJN extends WscConvertedTestBase {
        @Override
        protected IOComposerBuilder getBuilder() {
            return new BackwardAStarLayeredIOComposer.Builder()
                    .setBackwardSetNodeProvider(new NonEquivalentBackwardSetNodeProvider()
                            .setForceCombinedJumpNodes(true))
                    .withOptimizer(new ServiceCompressionOptimizer())
                    .withOptimizer(new BackwardClosureOptimizer());
        }
    }

    public class PubSubWithCompression extends PubSubTestBase {
        @Override
        protected IOComposerBuilder getBuilder() {
            return new BackwardAStarLayeredIOComposer.Builder()
                    .withOptimizer(new ServiceCompressionOptimizer());
        }
    }

    public class ApiDemoTestsWithPreconditions extends ApiDemoTestBase {
        @Override
        protected IOComposerBuilder getBuilder() {
            return new BackwardAStarLayeredIOComposer.Builder()
                    .setBackwardSetNodeProvider(new NonEquivalentBackwardSetNodeProvider().setUseConditions(true))
                    .setLayersStateFactory(ci -> new FastPreconditionsLayersState(
                            new InputServiceIndex(new NaiveTransitiveClosureGetter(RDFS.subClassOf, false), true)));
        }
    }

    public class WscConvertedWithPreconditionsAndCompression extends WscConvertedTestBase {
        @Override
        protected IOComposerBuilder getBuilder() {
            return new BackwardAStarLayeredIOComposer.Builder()
                    .withOptimizer(new ServiceCompressionOptimizer())
                    .setBackwardSetNodeProvider(new NonEquivalentBackwardSetNodeProvider().setUseConditions(true))
                    .setLayersStateFactory(ci -> new FastPreconditionsLayersState(
                            new InputServiceIndex(new NaiveTransitiveClosureGetter(RDFS.subClassOf, false), true)));
        }
    }

    public class WscConvertedWithPreconditionsAndCompressionAndBackClosureOptimizer extends WscConvertedTestBase {
        @Override
        protected IOComposerBuilder getBuilder() {
            return new BackwardAStarLayeredIOComposer.Builder()
                    .withOptimizer(new ServiceCompressionOptimizer())
                    .withOptimizer(new BackwardClosureOptimizer())
                    .setBackwardSetNodeProvider(new NonEquivalentBackwardSetNodeProvider().setUseConditions(true))
                    .setLayersStateFactory(ci -> new FastPreconditionsLayersState(
                            new InputServiceIndex(FixedCacheTransitiveClosureGetter.forward(RDFS.subClassOf)
                                    .setThreshold(3)
                                    .visitAll(ci.getSkolemizedUnion()).build(), true)));
        }
    }


    public class PubSubWithPreconditions extends PubSubTestBase {
        @Override
        protected IOComposerBuilder getBuilder() {
            return new BackwardAStarLayeredIOComposer.Builder()
                    .setBackwardSetNodeProvider(new NonEquivalentBackwardSetNodeProvider().setUseConditions(true))
                    .setLayersStateFactory(ci -> new FastPreconditionsLayersState(
                            new InputServiceIndex(new NaiveTransitiveClosureGetter(RDFS.subClassOf, false), true)));
        }
    }

    public class WscConvertedWithCombinedJN extends WscConvertedTestBase {
        @Override
        protected IOComposerBuilder getBuilder() {
            return new BackwardAStarLayeredIOComposer.Builder().setBackwardSetNodeProvider(
                    new NonEquivalentBackwardSetNodeProvider().setForceCombinedJumpNodes(true));
        }
    }

    public class WscConvertedWithListSubclasses extends WscConvertedTestBase {

        @Override
        protected IOComposerBuilder getBuilder() {
            return new BackwardAStarLayeredIOComposer.Builder().setLayersStateFactory(
                    ci -> new FastIOLayersState(new InputServiceIndex(
                            FixedCacheTransitiveClosureGetter.forward(RDFS.subClassOf)
                                    .setThreshold(7).visitAll(ci.getSkolemizedUnion()).build()),
                            true));
        }
    }
    public class WscConvertedWithNaiveTCG extends WscConvertedTestBase {

        @Override
        protected IOComposerBuilder getBuilder() {
            return new BackwardAStarLayeredIOComposer.Builder().setLayersStateFactory(
                    ci -> new FastIOLayersState(new InputServiceIndex(
                            new NaiveTransitiveClosureGetter(RDFS.subClassOf, false)),
                            false));
        }
    }
    public class WscConvertedWithNaiveTCGAndListSubclasses extends WscConvertedTestBase {

        @Override
        protected IOComposerBuilder getBuilder() {
            return new BackwardAStarLayeredIOComposer.Builder().setLayersStateFactory(
                    ci -> new FastIOLayersState(new InputServiceIndex(
                            new NaiveTransitiveClosureGetter(RDFS.subClassOf, false)),
                            true));
        }
    }

    public class WscConvertedWithCombinedJNAndListSubclassesAndNaiveTCG extends WscConvertedTestBase {
        @Override
        protected IOComposerBuilder getBuilder() {
            return new BackwardAStarLayeredIOComposer.Builder()
                    .setLayersStateFactory(ci -> new FastIOLayersState(new InputServiceIndex(
                            new NaiveTransitiveClosureGetter(RDFS.subClassOf, false)),
                            true))
                    .setBackwardSetNodeProvider(
                            new NonEquivalentBackwardSetNodeProvider()
                                    .setForceCombinedJumpNodes(true));
        }
    }

}



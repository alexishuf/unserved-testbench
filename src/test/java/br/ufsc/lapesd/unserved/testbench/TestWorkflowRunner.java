package br.ufsc.lapesd.unserved.testbench;

import br.ufsc.lapesd.unserved.testbench.components.ComponentRegistry;
import br.ufsc.lapesd.unserved.testbench.input.RDFInput;
import br.ufsc.lapesd.unserved.testbench.iri.Unserved;
import br.ufsc.lapesd.unserved.testbench.iri.UnservedP;
import br.ufsc.lapesd.unserved.testbench.process.*;
import br.ufsc.lapesd.unserved.testbench.process.actions.ActionExecutionException;
import br.ufsc.lapesd.unserved.testbench.process.actions.ActionRunner;
import br.ufsc.lapesd.unserved.testbench.process.data.DataContext;
import br.ufsc.lapesd.unserved.testbench.process.model.Action;
import br.ufsc.lapesd.unserved.testbench.process.model.ActionFactory;
import br.ufsc.lapesd.unserved.testbench.util.jena.UncloseableGraph;
import org.apache.jena.graph.Graph;
import org.apache.jena.graph.compose.MultiUnion;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class TestWorkflowRunner {
    private ComponentRegistry componentRegistry;

    public TestWorkflowRunner(ComponentRegistry componentRegistry) {
        this.componentRegistry = componentRegistry;
    }

    public DataContext run(DataContext dataContext,
                           RDFInput workflowInput,
                           String rootActionURI) throws IOException,
            ActionExecutionException {
        Model union = ModelFactory.createModelForGraph(new MultiUnion(new Graph[]{
                new UncloseableGraph(dataContext.getUnmodifiableModel().getGraph()),
                new UncloseableGraph(UnservedP.getInstance().getModel().getGraph()),
                new UncloseableGraph(Unserved.getInstance().getModel().getGraph()),
                new UncloseableGraph(workflowInput.getModel().getGraph())
        }));

        Resource rootActionResource = union.createResource(rootActionURI);
        Assert.assertTrue(union.containsResource(rootActionResource));
        Action rootAction = ActionFactory.asAction(rootActionResource);
        Assert.assertNotNull(rootAction);

        ActionRunner runner = componentRegistry.createActionRunner(rootAction);
        MyContext context = new MyContext(dataContext);
        runner.run(rootAction, context);

        return dataContext;
    }

    private class MyContext implements Context {
        private Logger logger = LoggerFactory.getLogger(MyContext.class);
        private final DataContext dataContext;
        private ActionRunnersContext actionRunnersContext = new DefaultActionRunnersContext();
        private UnservedPInterpreter interpreter = new UnservedPInterpreter(componentRegistry);
        private HashMap<String, Object> contextMap = new HashMap<>();
        private boolean cancelled = false, aborted = false;


        private MyContext(DataContext dataContext) {
            this.dataContext = dataContext;
        }

        @Override
        public boolean isCancelled() {
            return cancelled;
        }

        @Override
        public boolean isAborted() {
            return aborted;
        }

        @Override
        public void cancel() {
            cancelled = true;
        }

        @Override
        public void abort() {
            aborted = true;
        }

        @Override
        public DataContext data() {
            return dataContext;
        }

        @Override
        public ActionRunnersContext runners() {
            return actionRunnersContext;
        }

        @Override
        public ComponentRegistry componentRegistry() {
            return componentRegistry;
        }

        @Override
        public Interpreter interpreter() {
            return interpreter;
        }

        @Override
        public Map<String, Object> globalContext() {
            return contextMap;
        }

        @Override
        public void close() {
            try {
                actionRunnersContext.close();
            } catch (ActionRunnersContext.ActionContextsCloseException e) {
                logger.warn("Ignoring ActionContextsCloseException", e);
            }
        }
    }


}

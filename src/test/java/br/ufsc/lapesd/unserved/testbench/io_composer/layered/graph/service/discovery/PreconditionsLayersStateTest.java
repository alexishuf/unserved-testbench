package br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.discovery;

import br.ufsc.lapesd.unserved.testbench.Utils;
import br.ufsc.lapesd.unserved.testbench.benchmarks.DescriptionsGenerator;
import br.ufsc.lapesd.unserved.testbench.input.RDFInputFile;
import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.And;
import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.atomic.TriplePattern;
import br.ufsc.lapesd.unserved.testbench.io_composer.impl.DefaultIOComposerTimes;
import br.ufsc.lapesd.unserved.testbench.io_composer.impl.IOComposerInput;
import br.ufsc.lapesd.unserved.testbench.io_composer.impl.IOComposerInputImpl;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.ForwardLayeredServiceGraphBuilder;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.LayeredServiceGraph;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.Node;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.StartNode;
import br.ufsc.lapesd.unserved.testbench.model.Variable;
import br.ufsc.lapesd.unserved.testbench.util.NaiveTransitiveClosureGetter;
import br.ufsc.lapesd.unserved.testbench.util.UnservedReasoning;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFFormat;
import org.apache.jena.vocabulary.RDFS;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static br.ufsc.lapesd.unserved.testbench.Utils.resourceInput;
import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;
import static java.util.stream.Stream.of;


public class PreconditionsLayersStateTest {
    private Model m1, m2;

    private static Resource r(Model m, String lName) {
        return m.createResource("http://example.org/ns#" + lName);
    }
    private static Variable v(Model m, String lName) {
        return r(m, lName).as(Variable.class);
    }
    private static TriplePattern tp(Model m, String lName) {
        return TriplePattern.fromSPIN(r(m, lName));
    }

    @BeforeMethod
    public void setUp() throws IOException {
        m1 = UnservedReasoning.wrap(resourceInput("io_composer" +
                "/PreconditionsLayersStateTest-1.ttl", Lang.TTL)).takeModel();
        m2 = UnservedReasoning.wrap(resourceInput("io_composer" +
                "/PreconditionsLayersStateTest-2.ttl", Lang.TTL)).takeModel();
        IOComposerInputImpl ci = new IOComposerInputImpl();
        ci.initSkolemizedUnion(true, true);
    }

    @AfterMethod
    public void tearDown() {
        m1.close();
        m2.close();
    }

    @Test
    public void testGetOutputTriplePatterns1()  {
        List<TriplePattern> tps = PreconditionsLayersState.getOutputTriplePatterns(
                new StartNode(Collections.singleton(v(m1, "x")), And.empty()));
        Assert.assertTrue(tps.isEmpty());
    }

    @Test
    public void testGetOutputTriplePatterns2()  {
        List<TriplePattern> tps = PreconditionsLayersState.getOutputTriplePatterns(
                new StartNode(Collections.singleton(v(m1, "x")),
                        new And(tp(m1, "tp1"))));
        Assert.assertEquals(tps, Collections.singleton(tp(m1, "tp1")));
    }

    @Test
    public void testGetOutputTriplePatterns3()  {
        List<TriplePattern> tps = PreconditionsLayersState.getOutputTriplePatterns(
                new StartNode(Collections.singleton(v(m1, "a")),
                        new And(tp(m1, "tp1"))));
        Assert.assertEquals(new HashSet<>(tps),
                of(tp(m2, "tp1"), tp(m2, "tp2")).collect(Collectors.toSet()));
    }

    @Test
    public class CompareTests extends ServiceGraphComparisonTestBase {
        @Override
        protected IOLayersState aLayersState(IOComposerInput composerInput) {
            return new IOLayersState(composerInput);
        }

        @Override
        protected IOLayersState bLayersState(IOComposerInput composerInput) {
            return new PreconditionsLayersState(
                    new NaiveTransitiveClosureGetter(RDFS.subClassOf, false));
        }
    }

    private IOComposerInputImpl getDescriptionGeneratorCI(boolean preconditions) throws Exception {
        File descriptionFile = Files.createTempFile("description", ".ttl").toFile();
        File rulesFile = Files.createTempFile("rules", ".ttl").toFile();
        new DescriptionsGenerator.Builder(2)
                .setUniversal(false)
                .setRepeatRelations(false)
                .setConditions(2)
                .setPreconditions(preconditions)
                .setRel("rel")
                .setSkIRI("http://example.org/skolem#")
                .setUnservedOut(descriptionFile)
                .setRulesOut(rulesFile)
                .build().run();
        Assert.assertTrue(rulesFile.delete());
        IOComposerInputImpl ci = new IOComposerInputImpl();
        ci.addInput(RDFInputFile.createOwningFile(descriptionFile, RDFFormat.TURTLE));
        ci.addInput(Utils.resourceInput(
                "io_composer/description-generator-known.ttl", Lang.TURTLE));
        Utils.addWantedVars(ci, Utils.resourceInput(
                "io_composer/description-generator-wanted.ttl", Lang.TURTLE), null);
        ci.initSkolemizedUnion(true, true);
        return ci;
    }

    @Test(skipFailedInvocations = true)
    public void testCompareServiceGraphForDescriptionGenerator()
            throws Exception {
        LayeredServiceGraph ioSG;
        LayeredServiceGraph prSG;
        List<Set<Node>> ioLayers;
        List<Set<Node>> prLayers;
        try (IOComposerInputImpl ioCI = getDescriptionGeneratorCI(false);
             IOComposerInputImpl prCI = getDescriptionGeneratorCI(true)) {

            ForwardLayeredServiceGraphBuilder ioBuilder = new ForwardLayeredServiceGraphBuilder();
            ioBuilder.withLayersStateFactory(IOLayersState::new);
            ioSG = ioBuilder.build(ioCI, new DefaultIOComposerTimes());

            ForwardLayeredServiceGraphBuilder prBuilder = new ForwardLayeredServiceGraphBuilder();
            prBuilder.withLayersStateFactory(aCI -> new PreconditionsLayersState(
                    new NaiveTransitiveClosureGetter(RDFS.subClassOf, false)));
            prSG = prBuilder.build(prCI, new DefaultIOComposerTimes());
        }

        ioLayers = ioSG.getLayers().stream().map(l ->new HashSet<>(l.getNodes())).collect(toList());
        prLayers = prSG.getLayers().stream().map(l ->new HashSet<>(l.getNodes())).collect(toList());
        Assert.assertEquals(prLayers.size(), ioLayers.size());
        for (int i = 0; i < ioLayers.size(); i++) {
            final int layerIdx = i;
            String helper = "layerIdx="+layerIdx;
            Assert.assertEquals(prLayers.get(i), ioLayers.get(i), helper);
            Assert.assertEquals(prLayers.get(i).stream()
                            .filter(n -> prSG.getNodeLayer(n) != layerIdx).collect(toList()),
                    emptyList(), helper);
            Assert.assertEquals(prLayers.get(i).stream()
                    .filter(n -> !prSG.getProviderSet(n).stream().collect(toSet())
                            .equals(ioSG.getProviderSet(n).stream().collect(toSet())))
                    .collect(toList()), emptyList(), helper);
            Assert.assertEquals(ioLayers.get(i).stream()
                            .flatMap(n -> ioSG.getProviderSet(n).stream())
                            .filter(n -> ioSG.getNodeLayer(n) >= layerIdx).collect(toList()),
                    emptyList(), helper);
            Assert.assertEquals(prLayers.get(i).stream()
                            .flatMap(n -> prSG.getProviderSet(n).stream())
                            .filter(n -> prSG.getNodeLayer(n) >= layerIdx).collect(toList()),
                    emptyList(), helper);
        }
    }
}
package br.ufsc.lapesd.unserved.testbench.io_composer;

import br.ufsc.lapesd.unserved.testbench.Utils;
import br.ufsc.lapesd.unserved.testbench.composer.Composition;
import br.ufsc.lapesd.unserved.testbench.composer.ReplicatedComposer;
import br.ufsc.lapesd.unserved.testbench.input.RDFInput;
import br.ufsc.lapesd.unserved.testbench.iri.UnservedP;
import br.ufsc.lapesd.unserved.testbench.model.Variable;
import br.ufsc.lapesd.unserved.testbench.process.data.DataContext;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.RDF;
import org.testng.Assert;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;

import static java.util.stream.Collectors.toList;

public abstract class AbstractWscConvertedTestBase extends CompositionTestBase {

    public static class CheckInfo {
        AbstractWscConvertedTestBase test;
        List<Variable> results;

        public CheckInfo(AbstractWscConvertedTestBase test, List<Variable> results) {
            this.test = test;
            this.results = results;
        }
    }

    public void runTestProblem(RDFInput taxonomyInput, RDFInput servicesInput,
                           RDFInput knownInput, RDFInput wantedInput) throws Exception {
        runTestProblem(taxonomyInput, servicesInput, knownInput, wantedInput, null);
    }

    public void runTestProblem(RDFInput taxonomyInput, RDFInput servicesInput,
                               RDFInput knownInput, RDFInput wantedInput,
                               @Nullable Consumer<CheckInfo> additionalChecker) throws Exception {
        builder.addInput(taxonomyInput);
        builder.addInput(servicesInput);
        builder.addInput(knownInput);
        List<Resource> wantedList = new ArrayList<>();
        Utils.addWantedVars(builder, wantedInput, wantedList);
        composer = builder.withBasicReasoning(false).build();

        if (composer instanceof ReplicatedComposer) {
            ReplicatedComposer rComposer = (ReplicatedComposer) this.composer;
            CompletableFuture<IOCompositionResult> future = new CompletableFuture<>();
            interpreter.setResultConsumer(IOCompositionResult.class, future::complete);
            rComposer.run(interpreter).waitForDone();
            try (IOCompositionResult result = future.get()) {
                List<Variable> list = wantedList.stream().map(result::getWantedVariable)
                        .map(r -> r.as(Variable.class))
                        .collect(toList());
                Assert.assertTrue(list.stream().allMatch(Objects::nonNull));
                Assert.assertEquals(list.stream().filter(Variable::isValueBound).collect(toList()),
                                    list);
                if (additionalChecker != null)
                    additionalChecker.accept(new CheckInfo(this, list));
            }
        } else {
            try (Composition composition = composer.run();
                 DataContext data = composition.isNull() ? null : composition.createDataContext()) {
                Assert.assertFalse(composition.isNull());
                Assert.assertNotNull(data);

                Resource root = composition.getWorkflowRoot();
                Assert.assertNotNull(root);
                Assert.assertTrue(root.hasProperty(RDF.type, UnservedP.Sequence));
                Assert.assertTrue(root.hasProperty(RDF.type, UnservedP.Action));

                List<Variable> list = wantedList.stream().map(r -> data.getResource(r, Variable.class)).collect(toList());
                for (Variable wanted : list) {
                    wanted = data.getResource("https://alexishuf.bitbucket.io/2016/04/unserved/var#wanted", Variable.class);
                    Assert.assertNotNull(wanted);
                    Assert.assertFalse(wanted.isValueBound());
                    interpreter.interpret(data, root).waitForDone();
                    Assert.assertNotNull(wanted);
                    Assert.assertTrue(wanted.isValueBound());
                }
            }
        }
    }
}

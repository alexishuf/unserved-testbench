package br.ufsc.lapesd.unserved.testbench.pragproof;

import br.ufsc.lapesd.unserved.testbench.ApiDemoHelper;
import br.ufsc.lapesd.unserved.testbench.HydraCompileHelper;
import br.ufsc.lapesd.unserved.testbench.Utils;
import br.ufsc.lapesd.unserved.testbench.input.RDFInput;
import br.ufsc.lapesd.unserved.testbench.input.RDFInputModel;
import br.ufsc.lapesd.unserved.testbench.iri.Unserved;
import br.ufsc.lapesd.unserved.testbench.iri.UnservedP;
import br.ufsc.lapesd.unserved.testbench.pragproof.context.PragProofContext;
import br.ufsc.lapesd.unserved.testbench.pragproof.context.impl.DefaultPragProofContext;
import br.ufsc.lapesd.unserved.testbench.pragproof.model.PPO;
import br.ufsc.lapesd.unserved.testbench.process.model.Action;
import br.ufsc.lapesd.unserved.testbench.process.model.Sequence;
import br.ufsc.lapesd.unserved.testbench.reasoner.N3ReasonerException;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.riot.RDFFormat;
import org.apache.jena.vocabulary.RDF;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.NoSuchElementException;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Test
public class PragProofProverTests {
    @Test
    public class ApiDemoTest {
        private PragProofProver prover;
        private PragProofContext proofContext;
        private RDFInput apiDemoCompiled;
        private DefaultPragProofContext.Builder builder = null;

        @BeforeMethod
        public void setUp() throws Exception {
            HydraCompileHelper helper = HydraCompileHelper.compileResource(
                    "unserved/hydra/api-demo.jsonld");
            apiDemoCompiled = helper.compiled;

            //setup fields used on tests
            builder = new DefaultPragProofContext.Builder();
            builder.addInput(new RDFInputModel(helper.skolemizedInput, RDFFormat.TURTLE));
            builder.addInput(apiDemoCompiled);
            prover = new PragProofProver(Utils.reasonerFactory());
        }

        @AfterMethod
        public void tearDown() throws Exception {
            if (builder != null) {
                builder.close();
                builder = null;
            }
            if (proofContext != null) {
                proofContext.close();
                proofContext = null;
            }
            prover = null;
        }

        private DefaultPragProofContext.Builder
        addWantedVars(DefaultPragProofContext.Builder builder, String filename) throws IOException {
            RDFInput input = ApiDemoHelper.loadRDF(filename);
            builder.addInput(input);
            input.getModel().listSubjectsWithProperty(RDF.type, Unserved.Variable)
                    .forEachRemaining(builder::markWantedVariable);
            return builder;
        }


        @Test
        public void testGetEntryPoint() throws PragProofProverException, IOException, N3ReasonerException {
            builder.addInput(ApiDemoHelper.loadRDF("known-ep.ttl"));
            addWantedVars(builder, "wanted-ep.ttl");
            String proof = prover.prove(proofContext = builder.build());
            Assert.assertTrue(Pattern.compile("var:wanted a ppo:Grounded")
                    .matcher(proof).find());
        }

        @Test
        public void testRemoveInteractionRule() throws Exception {
            if (System.nanoTime() > 0)
                throw new SkipException("Broken test of abandoned composer");

            builder.addInput(ApiDemoHelper.loadRDF("known-ep.ttl"));
            addWantedVars(builder, "wanted-ep.ttl");
            proofContext = builder.build();

            Model additional = ModelFactory.createDefaultModel();
            DefaultPragProofContext emptyProofContext = new DefaultPragProofContext.Builder().build();
            ProofActions pre = ProofActions.extract(prover.prove(this.proofContext),
                    Utils.reasonerFactory(), emptyProofContext);
            Resource victim = pre.get().stream()
                    .filter(r -> r.getModel().contains(r, RDF.type, UnservedP.Action))
                    .filter(r -> r.hasProperty(PPO.tag))
                    .findFirst().orElseThrow(NoSuchElementException::new);
            Resource tag = victim.getPropertyResourceValue(PPO.tag);
            this.proofContext.getRules().removeInteractionRule(victim);

            ProofActions post = ProofActions.extract(prover.prove(this.proofContext),
                    Utils.reasonerFactory(), emptyProofContext);
            Assert.assertFalse(post.get().stream()
                    .filter(r -> r.getModel().contains(r, PPO.tag, tag))
                    .findAny().isPresent());

            pre.close();
            post.close();
            Assert.assertFalse(additional.isClosed());
        }

        @Test
        public void testTopLevelActionsOnly() throws Exception {
            if (System.nanoTime() > 0)
                throw new SkipException("Broken test of abandoned composer");

            builder.addInput(ApiDemoHelper.loadRDF("known-ep.ttl"));
            addWantedVars(builder, "wanted-ep.ttl");
            proofContext = builder.build();

            DefaultPragProofContext emptyProofContext = new DefaultPragProofContext.Builder().build();
            ProofActions proofActions;
            proofActions = ProofActions.extract(prover.prove(proofContext),
                    Utils.reasonerFactory(), emptyProofContext);

            Sequence sequence = proofActions.get().stream().filter(r -> r.canAs(Sequence.class))
                    .map(r -> r.as(Sequence.class)).findFirst().orElseThrow(Exception::new);
            Assert.assertEquals(sequence.getMembersLists().size(), 1);

            HashSet<Action> actions = sequence.getMembersLists().stream().reduce((l, r) -> {
                ArrayList<Action> list = new ArrayList<>(l);
                list.addAll(r);
                return list;
            }).orElseThrow(Exception::new).stream().collect(Collectors.toCollection(HashSet::new));
            Assert.assertEquals(actions.size(), 2);

            Assert.assertEquals(proofActions.get().stream().filter(actions::contains).count(), 0);
        }
    }
}

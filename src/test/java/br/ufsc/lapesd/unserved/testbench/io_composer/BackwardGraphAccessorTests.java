package br.ufsc.lapesd.unserved.testbench.io_composer;

import br.ufsc.lapesd.unserved.testbench.ApiDemoHelper;
import br.ufsc.lapesd.unserved.testbench.HydraCompileHelper;
import br.ufsc.lapesd.unserved.testbench.Utils;
import br.ufsc.lapesd.unserved.testbench.input.RDFInput;
import br.ufsc.lapesd.unserved.testbench.io_composer.backward.NaiveBackwardGraphAccessor;
import br.ufsc.lapesd.unserved.testbench.io_composer.impl.IOComposerInput;
import br.ufsc.lapesd.unserved.testbench.io_composer.impl.IOComposerInputImpl;
import br.ufsc.lapesd.unserved.testbench.io_composer.state.State;
import br.ufsc.lapesd.unserved.testbench.io_composer.state.StateActions;
import br.ufsc.lapesd.unserved.testbench.iri.Unserved;
import br.ufsc.lapesd.unserved.testbench.model.Message;
import br.ufsc.lapesd.unserved.testbench.model.Variable;
import es.usc.citius.hipster.model.Transition;
import org.apache.jena.vocabulary.RDF;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Iterator;
import java.util.stream.Collectors;

@Test
public class BackwardGraphAccessorTests {
    private static void markWanted(IOComposerInput input, RDFInput wanted) throws IOException {
        wanted.getModel().listSubjectsWithProperty(RDF.type, Unserved.Variable)
                .forEachRemaining(input::addWanted);
        input.addInput(wanted);
    }

    public class ApiDemoGetEntryPoint {
        private IOComposerInput input;
        private NaiveBackwardGraphAccessor accessor;

        @BeforeMethod
        public void setUp() throws Exception {
            HydraCompileHelper helper = HydraCompileHelper
                    .compileResource("unserved/hydra/api-demo.jsonld");
            input = new IOComposerInputImpl();
            input.addInput(helper.compiled);
            input.addInput(ApiDemoHelper.loadRDF("known-ep.ttl"));
            markWanted(input, ApiDemoHelper.loadRDF("wanted-ep.ttl"));
            input.initSkolemizedUnion(true, false);
            accessor = new NaiveBackwardGraphAccessor(input.getSkolemizedUnion(),
                    input.getWantedAsVariables());
        }

        @AfterMethod
        public void tearDown() throws IOException {
            input.close();
            accessor = null;
        }

        @Test
        public void testInitialState() throws Exception {
            State initial = accessor.getInitial();
            Assert.assertEquals(initial.getInputs(), input.getWantedAsVariables());
        }

        @Test
        public void testTransitions() throws Exception {
            Assert.assertEquals(input.getWantedAsVariables().size(), 2);
            Variable wantedEp = input.getWantedAsVariables().stream().filter(v -> v.getURI().endsWith("wanted")).findFirst().orElse(null);
            Assert.assertNotNull(wantedEp);

            State initial = accessor.getInitial();
            Iterator<Transition<Void, State>> it = accessor.getTransitions(initial).iterator();

            Assert.assertTrue(it.hasNext());

            Transition<Void, State> t = it.next();
            StateActions actions = t.getState().getActions();
            Message msg = Utils.getSingleResource(input.getSkolemizedUnion(), "msg",
                    "?msg http:mthd http-methods:GET.\n" +
                            "?msg unserved:part/unserved:variable ?var.\n" +
                            "?var unserved:type vocab:EntryPoint.\n" +
                            "?var unserved:representation xsd:anyURI.\n").as(Message.class);

            Assert.assertTrue(actions.getCopies().asList().isEmpty());
            Assert.assertEquals(actions.getAntecedents().size(), 2);
            Assert.assertTrue(actions.getAntecedents().contains(msg));
            Assert.assertEquals(actions.getConsequentSet().size(), 2);
            Assert.assertTrue(actions.getConsequentSet()
                    .containsAll(msg.getConsequent().collect(Collectors.toSet())));
            Assert.assertTrue(actions.getAssignments().asList().stream()
                    .anyMatch(c -> wantedEp.matches(c.to)));
        }
    }

    @Test
    public void testGoalWithCopyVar() throws IOException {
        IOComposerInput input = new IOComposerInputImpl();
        input.addInput(ApiDemoHelper.loadRDF("known-ep.ttl"));
        markWanted(input, ApiDemoHelper.loadRDF("wanted-epURI.ttl"));
        input.initSkolemizedUnion(true, false);
        NaiveBackwardGraphAccessor accessor = new NaiveBackwardGraphAccessor(
                input.getSkolemizedUnion(), input.getWantedAsVariables());

        State initial = accessor.getInitial();
        Iterator<Transition<Void, State>> it1 = accessor.getTransitions(initial).iterator();
        Assert.assertTrue(it1.hasNext());
        Transition<Void, State> t = it1.next();
        Assert.assertNotEquals(t.getState(), accessor.getGoal());

        Iterator<Transition<Void, State>> it2 = accessor.getTransitions(t.getState()).iterator();
        Assert.assertTrue(it2.hasNext());
        Transition<Void, State> t2 = it2.next();
        Assert.assertEquals(t2.getState(), accessor.getGoal());
        Assert.assertFalse(it2.hasNext());

        Assert.assertFalse(it1.hasNext());
    }
}

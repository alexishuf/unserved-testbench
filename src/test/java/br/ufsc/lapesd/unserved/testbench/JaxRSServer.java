package br.ufsc.lapesd.unserved.testbench;

import org.apache.cxf.endpoint.Server;
import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;

public class JaxRSServer implements AutoCloseable {
    private static Logger logger = LoggerFactory.getLogger(JaxRSServer.class);
    private int port;
    private final Server server;

    public JaxRSServer(Class<?>... endpoints) throws IOException {
        port = randomPort();
        JAXRSServerFactoryBean factory = new JAXRSServerFactoryBean();
        factory.setResourceClasses(endpoints);
//        for (Class<?> ep : endpoints) {
//            try {
//                Constructor<?> constructor = ep.getConstructor();
//                Object object = constructor.newInstance();
//                factory.setResourceProvider(ep, new SingletonResourceProvider(object));
//            } catch (ReflectiveOperationException e) {
//                throw new RuntimeException(e);
//            }
//        }
        factory.setAddress("http://localhost:" + port + "/");
        server = factory.create();

//        InetSocketAddress sockAddress = new InetSocketAddress(InetAddress.getLoopbackAddress(), 0);
//        server = HttpServer.create(sockAddress, 0);
//        server.createContext("/",
//                RuntimeDelegate.getInstance().createEndpoint(config, HttpHandler.class));
//        server.start();
    }

    private int randomPort() throws IOException {
        try (ServerSocket server = new ServerSocket()) {
            server.setReuseAddress(true);
            server.bind(new InetSocketAddress(InetAddress.getLoopbackAddress(), 0));
            assert server.isBound();
            return server.getLocalPort();
        } finally {
            try {
                Thread.sleep(100); //paranoid
            } catch (InterruptedException ignored) { }
        }
    }


    public int getPort() {
//        return server.getAddress().getPort();
        return port;
    }

    @Override
    public void close() throws Exception {
        server.stop();
    }
}

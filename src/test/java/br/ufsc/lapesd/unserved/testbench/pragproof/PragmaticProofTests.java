package br.ufsc.lapesd.unserved.testbench.pragproof;

import br.ufsc.lapesd.unserved.testbench.ApiDemoHelper;
import br.ufsc.lapesd.unserved.testbench.HydraCompileHelper;
import br.ufsc.lapesd.unserved.testbench.Utils;
import br.ufsc.lapesd.unserved.testbench.components.DefaultComponentRegistry;
import br.ufsc.lapesd.unserved.testbench.components.SimpleActionRunnerFactory;
import br.ufsc.lapesd.unserved.testbench.components.WrappingComponentRegistry;
import br.ufsc.lapesd.unserved.testbench.input.RDFInput;
import br.ufsc.lapesd.unserved.testbench.input.RDFInputModel;
import br.ufsc.lapesd.unserved.testbench.iri.Unserved;
import br.ufsc.lapesd.unserved.testbench.iri.UnservedP;
import br.ufsc.lapesd.unserved.testbench.model.Message;
import br.ufsc.lapesd.unserved.testbench.model.Variable;
import br.ufsc.lapesd.unserved.testbench.pragproof.execution.PragProofResult;
import br.ufsc.lapesd.unserved.testbench.process.UnservedPInterpreter;
import br.ufsc.lapesd.unserved.testbench.process.actions.ActionRunner;
import br.ufsc.lapesd.unserved.testbench.process.actions.mock.MockReceiveRunner;
import br.ufsc.lapesd.unserved.testbench.process.actions.mock.MockSendRunner;
import br.ufsc.lapesd.unserved.testbench.process.data.DataContext;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.riot.RDFFormat;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.concurrent.CompletableFuture;

import static org.apache.jena.rdf.model.ResourceFactory.createResource;

@Test()
public class PragmaticProofTests {

    public static class MagicEntrypointReceiveRunnerFactory extends SimpleActionRunnerFactory {
        public MagicEntrypointReceiveRunnerFactory() {
            super(MockReceiveRunner.class, MagicEntrypointReceiveRunnerFactory.class,
                    UnservedP.Receive);
        }

        @Override
        public ActionRunner newInstance() {
            return new MockReceiveRunner() {
                @Override
                protected void setResource(DataContext context, Message reaction, Variable var) {
                    if (var.getType().toString().endsWith("EntryPoint")) {
                        super.setResource(context, reaction, var,
                                createResource("http://example.org/EntryPoint"));
                    } else {
                        super.setResource(context, reaction, var);
                    }
                }
            };
        }
    };

    @Test
    public class ApiDemoTest {
        private PragmaticProof pragProof;
        private RDFInput apiDemoCompiled;
        private PragmaticProof.Builder builder;
        private WrappingComponentRegistry registry;
        private UnservedPInterpreter interpreter;


        @BeforeMethod
        public void setUp() throws Exception {
            HydraCompileHelper helper = HydraCompileHelper.compileResource(
                    "unserved/hydra/api-demo.jsonld");
            apiDemoCompiled = helper.compiled;

            //setup fields used on tests
            builder = new PragmaticProof.Builder(Utils.reasonerFactory());
            builder.addInput(new RDFInputModel(helper.skolemizedInput, RDFFormat.TURTLE));
            builder.addInput(apiDemoCompiled);

            registry = new WrappingComponentRegistry(DefaultComponentRegistry.getInstance());
            registry.registerComponent(MockSendRunner.Factory.class);
            registry.registerComponent(MockReceiveRunner.Factory.class);

            interpreter = new UnservedPInterpreter(registry);
        }

        @AfterMethod
        public void tearDown() throws Exception {
            builder.close();
            builder = null;
            if (pragProof != null) {
                pragProof.close();
                pragProof = null;
            }
        }

        @Test
        public void test() throws Exception {
            if (System.nanoTime() > 0)
                throw new SkipException("Broken test of abandoned composer");
            builder.addInput(ApiDemoHelper.loadRDF("known-ep.ttl"));
            Utils.addWantedVars(builder, ApiDemoHelper.loadRDF("wanted-ep.ttl"));
            registry.unregisterComponent(MockReceiveRunner.Factory.class);
            registry.registerComponent(MagicEntrypointReceiveRunnerFactory.class);
            pragProof = builder.build();

            CompletableFuture<PragProofResult> future = new CompletableFuture<>();
            interpreter.setResultConsumer(PragProofResult.class, future::complete);
            pragProof.run(interpreter).waitForDone();
            PragProofResult result = future.get();

            Assert.assertEquals(result.getInputWantedVariables().size(), 1);
            Resource inputVar = result.getInputWantedVariables().iterator().next();
            Resource var = result.getWantedVariable(inputVar);
            StmtIterator it = var.listProperties(Unserved.resourceValue);
            Assert.assertTrue(it.hasNext());
            Assert.assertEquals(it.next().getObject().asResource(),
                    createResource("http://example.org/EntryPoint"));
        }
    }
}

package br.ufsc.lapesd.unserved.testbench.io_composer.conditions.index;

import br.ufsc.lapesd.unserved.testbench.Utils;
import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.atomic.VariableSpec;
import br.ufsc.lapesd.unserved.testbench.iri.Unserved;
import br.ufsc.lapesd.unserved.testbench.model.Variable;
import br.ufsc.lapesd.unserved.testbench.util.FixedCacheTransitiveClosureGetter;
import br.ufsc.lapesd.unserved.testbench.util.NaiveTransitiveClosureGetter;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.riot.Lang;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toSet;

public class VariableSpecIndexTest {
    private Model mSub;
    private Model mSuper;

    @BeforeClass
    public void setUp() throws Exception {
        mSub = Utils.resourceInput("io_composer/conditions/index" +
                "/VariableSpecIndexTest-findSubclass.ttl", Lang.TTL).takeModel();
        mSuper = Utils.resourceInput("io_composer/conditions/index" +
                "/VariableSpecIndexTest-findSuperclass.ttl", Lang.TTL).takeModel();
    }

    @AfterClass
    public void tearDown() {
        mSub.close();
        mSuper.close();
    }

    private Resource r(Model m, String name) {
        return m.createResource("http://example.org/ns#" + name);
    }
    private Variable var(Model m, String name) { return r(m, name).as(Variable.class); }

    private void doTestGet(Model m, VariableSpecIndex<String> index) {
        List<Variable> vs = m.listSubjectsWithProperty(RDF.type, Unserved.Variable).toList()
                .stream().map(v -> v.as(Variable.class)).collect(Collectors.toList());

        vs.forEach(v -> index.put(new VariableSpec(v), v.getURI().replaceAll("^.*#", "")));

        List<ImmutablePair<Variable, String>> errors;
        errors = vs.stream().map(v -> v.getURI().replaceAll("^.*#", "")).map(name -> {
            Matcher matcher = Pattern.compile("(\\w)\\d+-?").matcher(name);
            Assert.assertTrue(matcher.matches());
            Variable queried = var(m, matcher.group(1) + "1");
            boolean contains = index.get(new VariableSpec(queried)).contains(name);
            return contains == !name.endsWith("-") ? null
                    : ImmutablePair.of(queried, name);
        }).filter(Objects::nonNull).collect(Collectors.toList());

        Assert.assertEquals(errors, Collections.emptyList(), errors.stream()
                .map(p -> p.left + " -> " + p.right)
                .reduce((l, r) -> l + "\n" + r).orElse(""));

        errors = vs.stream().map(v -> v.getURI().replaceAll("^.*#", "")).map(name -> {
            Matcher matcher = Pattern.compile("(\\w)\\d+-?").matcher(name);
            Assert.assertTrue(matcher.matches());
            Variable queried = var(m, matcher.group(1) + "1");
            Set<String> expected = index.get(new VariableSpec(queried));
            Set<String> collected = index.getStream(new VariableSpec(queried)).collect(toSet());
            return collected.equals(expected) ? null : ImmutablePair.of(queried, name);
        }).filter(Objects::nonNull).collect(Collectors.toList());
        Assert.assertEquals(errors, Collections.emptyList(), errors.stream()
                .map(p -> p.left + " -> " + p.right)
                .reduce((l, r) -> l + "\n" + r).orElse(""));

        errors = vs.stream().map(v -> v.getURI().replaceAll("^.*#", "")).map(name -> {
            Matcher matcher = Pattern.compile("(\\w)\\d+-?").matcher(name);
            Assert.assertTrue(matcher.matches());
            Variable queried = var(m, matcher.group(1) + "1");
            Set<String> expected = index.get(new VariableSpec(queried));
            String first = index.getFirst(new VariableSpec(queried));
            return expected.isEmpty() && first == null || expected.contains(first)
                    ? null : ImmutablePair.of(queried, name);
        }).filter(Objects::nonNull).collect(Collectors.toList());
        Assert.assertEquals(errors, Collections.emptyList(), errors.stream()
                .map(p -> p.left + " -> " + p.right)
                .reduce((l, r) -> l + "\n" + r).orElse(""));
    }

    @Test
    public void testGetWithNaive() {
        NaiveTransitiveClosureGetter getter =
                new NaiveTransitiveClosureGetter(RDFS.subClassOf, true);
        doTestGet(mSub, new VariableSpecIndex<>(getter));
    }

    @Test
    public void testGetWithNaiveClosureOnPut() {
        NaiveTransitiveClosureGetter getter =
                new NaiveTransitiveClosureGetter(RDFS.subClassOf, false);
        doTestGet(mSub, VariableSpecIndex.withClosureOnPut(getter));
    }

    @Test
    public void testGetWithFixedCache() {
        FixedCacheTransitiveClosureGetter getter;
        getter = FixedCacheTransitiveClosureGetter.backward(RDFS.subClassOf).setThreshold(2)
                .visit(r(mSub, "A")).visit(r(mSub, "F")).build();
        doTestGet(mSub, new VariableSpecIndex<>(getter));

        getter = FixedCacheTransitiveClosureGetter.backward(RDFS.subClassOf).setThreshold(4)
                .visit(r(mSub, "A")).visit(r(mSub, "F")).build();
        doTestGet(mSub, new VariableSpecIndex<>(getter));
    }

    @Test
    public void testGetWithFixedCacheClosureOnPut() {
        FixedCacheTransitiveClosureGetter getter;
        getter = FixedCacheTransitiveClosureGetter.forward(RDFS.subClassOf).setThreshold(2)
                .visit(r(mSub, "A")).visit(r(mSub, "F")).build();
        doTestGet(mSub, VariableSpecIndex.withClosureOnPut(getter));

        getter = FixedCacheTransitiveClosureGetter.forward(RDFS.subClassOf).setThreshold(4)
                .visit(r(mSub, "A")).visit(r(mSub, "F")).build();
        doTestGet(mSub, VariableSpecIndex.withClosureOnPut(getter));
    }

    @Test
    public void testGetSuperWithNaive() {
        NaiveTransitiveClosureGetter getter =
                new NaiveTransitiveClosureGetter(RDFS.subClassOf, false);
        doTestGet(mSuper, new VariableSpecIndex<>(getter));
    }

    @Test
    public void testGetSuperWithNaiveClosureOnPut() {
        NaiveTransitiveClosureGetter getter =
                new NaiveTransitiveClosureGetter(RDFS.subClassOf, true);
        doTestGet(mSuper, VariableSpecIndex.withClosureOnPut(getter));
    }
}
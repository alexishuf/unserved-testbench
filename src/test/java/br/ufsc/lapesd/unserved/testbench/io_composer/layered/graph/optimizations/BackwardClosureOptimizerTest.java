package br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.optimizations;

import br.ufsc.lapesd.unserved.testbench.io_composer.WscConvertedTestBase;
import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.And;
import br.ufsc.lapesd.unserved.testbench.io_composer.impl.DefaultIOComposerTimes;
import br.ufsc.lapesd.unserved.testbench.io_composer.impl.IOComposerInputImpl;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.*;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.cost.CostInfo;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.discovery.IOLayersState;
import br.ufsc.lapesd.unserved.testbench.iri.Unserved;
import br.ufsc.lapesd.unserved.testbench.model.Message;
import br.ufsc.lapesd.unserved.testbench.model.Variable;
import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.ResourceFactory;
import org.apache.jena.vocabulary.RDF;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.*;

import static br.ufsc.lapesd.unserved.testbench.Utils.addWantedVars;
import static br.ufsc.lapesd.unserved.testbench.io_composer.WscConvertedTestBase.getWscTurtle;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.singleton;
import static java.util.stream.Collectors.toList;

public class BackwardClosureOptimizerTest {
    @DataProvider
    public static Object[][] problems() {
        return WscConvertedTestBase.problemNames();
    }

    @Test
    public void testRemovesService() {
        Model m = ModelFactory.createDefaultModel();
        Variable varA = m.createResource().addProperty(RDF.type, Unserved.Variable)
                .addProperty(Unserved.literalValue,
                        ResourceFactory.createTypedLiteral("2", XSDDatatype.XSDint))
                .as(Variable.class);
        List<Variable> vs = new ArrayList<>();
        for (int i = 0; i < 10; i++)
            vs.add(m.createResource().addProperty(RDF.type, Unserved.Variable).as(Variable.class));
        Message ant1 = m.createResource().addProperty(RDF.type, Unserved.Message).as(Message.class);
        Message ant2 = m.createResource().addProperty(RDF.type, Unserved.Message).as(Message.class);
        Message con1 = m.createResource().addProperty(RDF.type, Unserved.Message).as(Message.class);
        Message con2 = m.createResource().addProperty(RDF.type, Unserved.Message).as(Message.class);


        List<ServiceLayer> layers = new ArrayList<>();
        StartNode start = new StartNode(singleton(varA), And.empty());
        EndNode end = new EndNode(singleton(vs.get(0)), And.empty());
        ServiceNode s1 = new ServiceNode(ant1, con1, new ArrayList<>(), And.empty(), And.empty(), CostInfo.EMPTY);
        ServiceNode s2 = new ServiceNode(ant2, con2, new ArrayList<>(), And.empty(), And.empty(), CostInfo.EMPTY);
        layers.add(new ServiceLayer(singleton(start)));
        layers.add(new ServiceLayer(asList(s1, s2)));
        layers.add(new ServiceLayer(singleton(end)));

        Map<Node, ProviderSet> psMap = new HashMap<>();
        IOProviderHashSet ps = new IOProviderHashSet();
        ps.add(vs.get(1), start, varA);
        psMap.put(s1, ps);
        ps = new IOProviderHashSet();                                   psMap.put(start, ps);
        ps = new IOProviderHashSet(); ps.add(vs.get(1), start, varA);   psMap.put(s1,    ps);
        ps = new IOProviderHashSet(); ps.add(vs.get(2), start, varA);   psMap.put(s2,    ps);
        ps = new IOProviderHashSet(); ps.add(vs.get(3), s1, vs.get(4)); psMap.put(end,   ps);

        Map<Node, Integer> nodeLayer = new HashMap<>();
        nodeLayer.put(start, 0);
        nodeLayer.put(s1,    1);
        nodeLayer.put(s2,    1);
        nodeLayer.put(end,   2);

        LayeredServiceGraph graph;
        graph = new LayeredServiceGraph(layers, psMap, start, end, nodeLayer);
        LayeredServiceGraph opt = new BackwardClosureOptimizer().apply(graph);
        Assert.assertEquals(opt.getLayers().get(0).getNodes(), Collections.singletonList(start));
        Assert.assertEquals(opt.getLayers().get(1).getNodes(), Collections.singletonList(s1));
        Assert.assertEquals(opt.getLayers().get(2).getNodes(), Collections.singletonList(end));
        checkValid(opt);
    }

    @Test(dataProvider = "problems")
    public void testWsc(@Nonnull String problem) throws IOException {
        try (IOComposerInputImpl ci = new IOComposerInputImpl()) {
            ci.addInput(getWscTurtle(problem, "taxonomy"));
            ci.addInput(getWscTurtle(problem, "services"));
            ci.addInput(getWscTurtle(problem, "known"));
            addWantedVars(ci, getWscTurtle(problem, "wanted"), null);
            ci.initSkolemizedUnion(true, true);

            LayeredServiceGraph graph = new ForwardLayeredServiceGraphBuilder()
                    .withLayersStateFactory(IOLayersState::new)
                    .build(ci, new DefaultIOComposerTimes());
            LayeredServiceGraph opt = new BackwardClosureOptimizer().apply(graph);

            Assert.assertEquals(opt.getLayers().size(), graph.getLayers().size());
            for (int i = 0; i < opt.getLayers().size(); i++) {
                List<Node> a = graph.getLayers().get(i).getNodes();
                List<Node> b = opt.getLayers().get(i).getNodes();
                Assert.assertEquals(b.stream().filter(n -> !a.contains(n)).collect(toList()),
                        emptyList());
            }
            checkValid(opt);
        }
    }

    private void checkValid(LayeredServiceGraph graph) {
        Queue<Node> queue = new LinkedList<>();
        queue.add(graph.getEndNode());
        Set<Node> visited = new HashSet<>();
        while (!queue.isEmpty()) {
            Node node = queue.remove();
            if (visited.contains(node)) continue;
            visited.add(node);
            graph.getProviderSet(node).stream().forEach(queue::add);
        }
        for (ServiceLayer layer : graph.getLayers()) {
            Assert.assertEquals(layer.getNodes().stream().filter(n -> !visited.contains(n))
                    .collect(toList()), emptyList());
        }
    }

    static {
        Unserved.init();
    }
}
package br.ufsc.lapesd.unserved.testbench.io_composer;

import br.ufsc.lapesd.unserved.testbench.ApiDemoHelper;
import br.ufsc.lapesd.unserved.testbench.HydraCompileHelper;
import br.ufsc.lapesd.unserved.testbench.Utils;
import br.ufsc.lapesd.unserved.testbench.components.SimpleActionRunnerFactory;
import br.ufsc.lapesd.unserved.testbench.composer.Composition;
import br.ufsc.lapesd.unserved.testbench.composer.ReplicatedComposer;
import br.ufsc.lapesd.unserved.testbench.input.RDFInput;
import br.ufsc.lapesd.unserved.testbench.iri.UnservedP;
import br.ufsc.lapesd.unserved.testbench.model.Message;
import br.ufsc.lapesd.unserved.testbench.model.Variable;
import br.ufsc.lapesd.unserved.testbench.process.actions.ActionRunner;
import br.ufsc.lapesd.unserved.testbench.process.actions.mock.MockReceiveRunner;
import br.ufsc.lapesd.unserved.testbench.process.data.DataContext;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.RDF;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.concurrent.CompletableFuture;

import static org.apache.jena.rdf.model.ResourceFactory.createResource;

public abstract class ApiDemoTestBase extends CompositionTestBase {
    @BeforeMethod
    @Override
    public void setUp() throws Exception {
        super.setUp();
        HydraCompileHelper helper = HydraCompileHelper.compileResource(
                "unserved/hydra/api-demo.jsonld");
        builder.addInput(helper.compiled);
        builder.addInput(ApiDemoHelper.loadRDF("known-ep.ttl"));
    }

    @Test
    public void testGetEntryPoint() throws Exception {
        RDFInput wantedInput = ApiDemoHelper.loadRDF("wanted-ep.ttl");
        Utils.addWantedVars(builder, wantedInput);
        componentRegistry.unregisterComponent(MockReceiveRunner.Factory.class);
        componentRegistry.registerComponent(MagicEntryPointReceiveRunnerFactory.class);
        composer = builder.build();

        Variable wanted;
        if (composer instanceof ReplicatedComposer) {
            ReplicatedComposer rComposer = (ReplicatedComposer) this.composer;
            CompletableFuture<IOCompositionResult> future = new CompletableFuture<>();
            interpreter.setResultConsumer(IOCompositionResult.class, future::complete);
            rComposer.run(interpreter).waitForDone();
            try (IOCompositionResult result = future.get()) {
                wanted = result.getWantedVariable(createResource("https://alexishuf.bitbucket.io/2016/04/unserved/var#wanted")).as(Variable.class);
                Assert.assertNotNull(wanted);
                Assert.assertTrue(wanted.isValueBound());
                Assert.assertEquals(wanted.getResourceValue(),
                        createResource("http://example.org/anEntryPoint"));
            }
        } else {
            try (Composition composition = composer.run();
                 DataContext data = composition.isNull() ? null : composition.createDataContext()) {
                Assert.assertFalse(composition.isNull());
                Assert.assertNotNull(data);

                Resource root = composition.getWorkflowRoot();
                Assert.assertNotNull(root);
                Assert.assertTrue(root.hasProperty(RDF.type, UnservedP.Sequence));
                Assert.assertTrue(root.hasProperty(RDF.type, UnservedP.Action));

                wanted = data.getResource("https://alexishuf.bitbucket.io/2016/04/unserved/var#wanted", Variable.class);
                Assert.assertNotNull(wanted);
                Assert.assertFalse(wanted.isValueBound());
                interpreter.interpret(data, root).waitForDone();
                Assert.assertNotNull(wanted);
                Assert.assertTrue(wanted.isValueBound());
                Assert.assertEquals(wanted.getResourceValue(),
                        createResource("http://example.org/anEntryPoint"));
            }
        }
    }

    public static class MagicEntryPointReceiveRunnerFactory extends SimpleActionRunnerFactory {

        public MagicEntryPointReceiveRunnerFactory() {
            super(MockReceiveRunner.class, MagicEntryPointReceiveRunnerFactory.class,
                    UnservedP.Receive);
        }

        @Override
        public ActionRunner newInstance() {
            return new MockReceiveRunner() {
                @Override
                protected void setResource(DataContext context, Message reaction, Variable var) {
                    if (var.getType().getURI().endsWith("EntryPoint")) {
                        super.setResource(context, reaction, var,
                                createResource("http://example.org/anEntryPoint"));
                    } else {
                        super.setResource(context, reaction, var);
                    }
                }
            };
        }
    }
}

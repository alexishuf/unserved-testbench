package br.ufsc.lapesd.unserved.testbench.io_composer;

import br.ufsc.lapesd.unserved.testbench.input.RDFInput;
import br.ufsc.lapesd.unserved.testbench.input.RDFInputStream;
import org.apache.jena.riot.RDFFormat;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public abstract class WscConvertedTestBase extends AbstractWscConvertedTestBase {
    public static RDFInput getWscTurtle(String problemName, String baseName) {
        ClassLoader cl = WscConvertedTestBase.class.getClassLoader();
        return new RDFInputStream(cl.getResourceAsStream("wsc08/" + problemName
                + "/" + baseName + ".ttl"),  RDFFormat.TURTLE);
    }

    @DataProvider
    public static Object[][] problemNames() {
        return new Object[][] {
                {"01"},
                {"03"},
                {"04"},
        };
    }

    @Test(dataProvider = "problemNames")
    public void testProblem(String problemName) throws Exception {
        runTestProblem(getWscTurtle(problemName, "taxonomy"),
                getWscTurtle(problemName, "services"),
                getWscTurtle(problemName, "known"),
                getWscTurtle(problemName, "wanted"));
    }
}

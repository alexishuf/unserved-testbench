package br.ufsc.lapesd.unserved.testbench.components;

import br.ufsc.lapesd.unserved.testbench.Utils;
import br.ufsc.lapesd.unserved.testbench.byte_renderers.Render;
import br.ufsc.lapesd.unserved.testbench.byte_renderers.Renderer;
import br.ufsc.lapesd.unserved.testbench.byte_renderers.RendererException;
import br.ufsc.lapesd.unserved.testbench.byte_renderers.SimpleRendererFactory;
import br.ufsc.lapesd.unserved.testbench.byte_renderers.xsd.XSDRendererFactory;
import br.ufsc.lapesd.unserved.testbench.input.RDFInput;
import br.ufsc.lapesd.unserved.testbench.iri.Content;
import br.ufsc.lapesd.unserved.testbench.iri.Unserved;
import br.ufsc.lapesd.unserved.testbench.iri.UnservedP;
import br.ufsc.lapesd.unserved.testbench.iri.UnservedX;
import br.ufsc.lapesd.unserved.testbench.model.Variable;
import br.ufsc.lapesd.unserved.testbench.process.Context;
import br.ufsc.lapesd.unserved.testbench.process.actions.ActionExecutionException;
import br.ufsc.lapesd.unserved.testbench.process.actions.ActionRunner;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.riot.Lang;
import org.apache.jena.vocabulary.XSD;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Collections;

public class RulesComponentRegistryTests {
    private static String prefix = "http://example.com/RulesComponentRegistryTests#";

    public static class MySendRunner implements ActionRunner {
        public static class Factory extends SimpleActionRunnerFactory {
            public Factory() {
                super(MySendRunner.class, Factory.class,
                        getResource());
            }

            private static Resource getResource() {
                try {
                    return Utils.resourceInput("components/my-send-runner.ttl", Lang.TURTLE)
                            .getModel().createResource(prefix + "ActionClass");
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }

        @Override
        public void run(Resource action, Context context) throws ActionExecutionException {
            throw new UnsupportedOperationException();
        }
    }

    public static class MyReceiveRunner implements ActionRunner {
        public static class Factory extends SimpleActionRunnerFactory {
            public Factory() {
                super(MyReceiveRunner.class, Factory.class, UnservedP.Receive);
            }

        }
        @Override
        public void run(Resource action, Context context) throws ActionExecutionException {
            throw new UnsupportedOperationException();
        }
    }

    public static class MyRDFRenderer implements Renderer {
        public static class Factory extends SimpleRendererFactory {
            public Factory() {
                super(MyRDFRenderer.class, Factory.class,
                        Collections.singleton(Content.ContentAsBase64),
                        Collections.singleton(UnservedX.RDF.RDF));
            }
        }

        @Override
        public Render format(Variable variable) throws RendererException {
            throw new UnsupportedOperationException();
        }
    }

    public static class MyStringRenderer implements Renderer {
        public static class Factory extends XSDRendererFactory {
            public Factory() {
                super(MyStringRenderer.class, Factory.class, XSD.xstring.getURI());
            }
        }

        @Override
        public Render format(Variable variable) throws RendererException {
            throw new UnsupportedOperationException();
        }
    }

    @BeforeClass
    public void setUp() {
        Unserved.init();
    }

    @Test
    public void testCreteActionRunner() throws Exception {
        RulesComponentRegistry reg = new RulesComponentRegistry();
        reg.registerComponent(MySendRunner.Factory.class);
        RDFInput sendInput = Utils.resourceInput("components/send-action.ttl", Lang.TURTLE);
        Resource send = sendInput.getModel().createResource(prefix + "send");
        ActionRunner runner = reg.createActionRunner(send);
        Assert.assertNotNull(runner);
        Assert.assertTrue(runner instanceof MySendRunner);
    }

    @Test
    public void testCreateReceiveRunner() throws Exception {
        RulesComponentRegistry reg = new RulesComponentRegistry();
        reg.registerComponent(MyReceiveRunner.Factory.class);
        RDFInput rcvInput = Utils.resourceInput("components/receive-action.ttl", Lang.TURTLE);
        Resource rcv = rcvInput.getModel().createResource(prefix + "receive");
        ActionRunner receiveRunner = reg.createActionRunner(rcv);
        Assert.assertNotNull(receiveRunner);
        Assert.assertTrue(receiveRunner instanceof MyReceiveRunner);
    }

    @Test
    public void testCreateRDFRenderer() throws Exception {
        RulesComponentRegistry reg = new RulesComponentRegistry();
        reg.registerComponent(MyRDFRenderer.Factory.class);
        RDFInput rdfInput = Utils.resourceInput("components/rdf-var.ttl", Lang.TURTLE);
        Resource rdf = rdfInput.getModel().createResource(prefix + "rdf");
        Assert.assertTrue(rdf.canAs(Variable.class));
        Renderer renderer = reg.createRenderer(rdf.as(Variable.class));
        Assert.assertNotNull(renderer);
        Assert.assertTrue(renderer instanceof MyRDFRenderer);
    }

    @Test
    public void testCreateStringRenderer() throws Exception {
        RulesComponentRegistry reg = new RulesComponentRegistry();
        reg.registerComponent(MyStringRenderer.Factory.class);
        RDFInput strInput = Utils.resourceInput("components/string-var.ttl", Lang.TURTLE);
        Resource str = strInput.getModel().createResource(prefix + "string");
        Assert.assertTrue(str.canAs(Variable.class));
        Renderer renderer = reg.createRenderer(str.as(Variable.class));
        Assert.assertNotNull(renderer);
        Assert.assertTrue(renderer instanceof MyStringRenderer);
    }
}
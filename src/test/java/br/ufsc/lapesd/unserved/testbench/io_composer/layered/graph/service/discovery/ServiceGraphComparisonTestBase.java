package br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.discovery;

import br.ufsc.lapesd.unserved.testbench.io_composer.WscConvertedTestBase;
import br.ufsc.lapesd.unserved.testbench.io_composer.conditions.And;
import br.ufsc.lapesd.unserved.testbench.io_composer.impl.DefaultIOComposerTimes;
import br.ufsc.lapesd.unserved.testbench.io_composer.impl.IOComposerInput;
import br.ufsc.lapesd.unserved.testbench.io_composer.impl.IOComposerInputImpl;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.optimizations.BackwardClosureOptimizer;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.optimizations.ServiceCompressionOptimizer;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.*;
import br.ufsc.lapesd.unserved.testbench.model.Message;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import javax.annotation.Nonnull;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static br.ufsc.lapesd.unserved.testbench.Utils.addWantedVars;
import static br.ufsc.lapesd.unserved.testbench.io_composer.WscConvertedTestBase.getWscTurtle;
import static java.util.Collections.emptyList;
import static java.util.Collections.emptySet;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;
import static org.apache.commons.lang3.tuple.ImmutablePair.of;

public abstract class ServiceGraphComparisonTestBase {

    protected abstract IOLayersState aLayersState(IOComposerInput composerInput);
    protected abstract IOLayersState bLayersState(IOComposerInput composerInput);

    @DataProvider
    public static Object[][] problems() {
        return WscConvertedTestBase.problemNames();
    }

    @SuppressWarnings("unchecked")
    @Test(dataProvider = "problems")
    public void testCompareProviderSetSupplier(@Nonnull String problem) throws Exception {
        try (IOComposerInputImpl ci = new IOComposerInputImpl()) {
            ci.addInput(getWscTurtle(problem, "taxonomy"));
            ci.addInput(getWscTurtle(problem, "services"));
            ci.addInput(getWscTurtle(problem, "known"));
            addWantedVars(ci, getWscTurtle(problem, "wanted"), null);
            ci.initSkolemizedUnion(false, false);
            StartNode start = new StartNode(ci.getKnownAsVariables(), And.empty());
            EndNode end = new EndNode(ci.getWantedAsVariables(), And.empty());

            IOLayersState aState = aLayersState(ci);
            aState.advance(Collections.singleton(start));

            IOLayersState bState = bLayersState(ci);
            bState.advance(Collections.singleton(start));

            NodesSupplier<IOLayersState> aNodeSupplier = null;
            NodesSupplier<IOLayersState> bNodeSupplier = null;
            ProviderSetsSupplier aPSSupplier = null;
            ProviderSetsSupplier bPSSupplier = null;
            HashSet<Message> blacklist = new HashSet<>();
            try {
                aNodeSupplier = (NodesSupplier<IOLayersState>) aState.createNodesSupplier();
                aNodeSupplier.begin(ci, start, end);
                aPSSupplier = aState.createProviderSetsSupplier();
                aPSSupplier.begin(ci, start, end);
                aPSSupplier.update(Collections.singleton(start));
                bNodeSupplier = (NodesSupplier<IOLayersState>) bState.createNodesSupplier();
                bNodeSupplier.begin(ci, start, end);
                bPSSupplier = bState.createProviderSetsSupplier();
                bPSSupplier.begin(ci, start, end);
                bPSSupplier.update(Collections.singleton(start));

                for (int layerIdx = 0; true; layerIdx++) {
                    Set<Node> aNodes = aNodeSupplier.getNodes(aState, blacklist);
                    Set<Node> bNodes = bNodeSupplier.getNodes(bState, blacklist);
                    String message = "layerIdx=" + layerIdx;
                    Assert.assertEquals(bNodes, aNodes, message);

                    aNodes.stream().filter(n -> n instanceof ServiceNode)
                            .forEach(n -> blacklist.add(((ServiceNode) n).getConsequent()));
                    if (aNodes.isEmpty()) break;

                    Map<Node, ProviderSet> aps = aPSSupplier.get(aNodes);
                    Map<Node, ProviderSet> bps = bPSSupplier.get(bNodes);
                    aPSSupplier.update(aNodes);
                    bPSSupplier.update(bNodes); //ignored
                    Assert.assertEquals(aps.keySet(), aNodes);
                    Assert.assertEquals(bps.keySet(), bNodes);
                    Assert.assertEquals(aps.values().stream().flatMap(ProviderSet::stream).filter(aNodes::contains).collect(toList()), emptyList());
                    Assert.assertEquals(bps.values().stream().filter(ps -> !ps.isSatisfied()).collect(Collectors.toSet()), new HashSet<>());
                    Assert.assertEquals(aps.values().stream().filter(ps -> !ps.isSatisfied()).collect(Collectors.toSet()), new HashSet<>());
                    Assert.assertEquals(aps.entrySet().stream().flatMap(e -> e.getValue().getAssignmentsStream().map(a -> of(e, a))).filter(p -> p.right.target.equals(p.right.output)).collect(toSet()), emptySet());
                    Assert.assertEquals(bps.entrySet().stream().flatMap(e -> e.getValue().getAssignmentsStream().map(a -> of(e, a))).filter(p -> p.right.target.equals(p.right.output)).collect(toSet()), emptySet());
                    Assert.assertEquals(toNodesMap(bps), toNodesMap(aps), message);
                }

            } finally {
                if (bNodeSupplier != null) bNodeSupplier.end();
                if (aPSSupplier != null) aPSSupplier.end();
                if (aNodeSupplier != null) aNodeSupplier.end();
                if (bPSSupplier != null) bPSSupplier.end();
            }
        }
    }

    private Map<Node, Set<Node>> toNodesMap(Map<Node, ProviderSet> map) {
        return map.entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey,
                e -> e.getValue().stream().collect(Collectors.toSet())));
    }


    @Test(dataProvider = "problems")
    public void testCompareServiceGraphs(@Nonnull String problem) throws Exception {
        doCompareServiceGraphs(problem, Function.identity(), false);
    }

    @Test(dataProvider = "problems")
    public void testCompareServiceGraphsWithCompression(@Nonnull String problem) throws Exception {
        doCompareServiceGraphs(problem, new ServiceCompressionOptimizer(), true);
    }

    @Test(dataProvider = "problems")
    public void
    testCompareServiceGraphsWithBackwardClosure(@Nonnull String problem) throws Exception {
        doCompareServiceGraphs(problem, new BackwardClosureOptimizer(), false);
    }
    public void doCompareServiceGraphs(@Nonnull String problem,
                                       @Nonnull Function<LayeredServiceGraph,
                                               LayeredServiceGraph> optimizer,
                                       boolean renameNodes) throws Exception {
        LayeredServiceGraph aSG_;
        LayeredServiceGraph bSG_;
        List<Set<Node>> aLayers;
        List<Set<Node>> bLayers;
        try (IOComposerInputImpl ci = new IOComposerInputImpl()) {
            ci.addInput(getWscTurtle(problem, "taxonomy"));
            ci.addInput(getWscTurtle(problem, "services"));
            ci.addInput(getWscTurtle(problem, "known"));
            addWantedVars(ci, getWscTurtle(problem, "wanted"), null);
            ci.initSkolemizedUnion(false, false);

            ForwardLayeredServiceGraphBuilder aBuilder = new ForwardLayeredServiceGraphBuilder();
            aBuilder.withLayersStateFactory(this::aLayersState);
            aSG_ = aBuilder.build(ci, new DefaultIOComposerTimes());

            ForwardLayeredServiceGraphBuilder bBuilder = new ForwardLayeredServiceGraphBuilder();
            bBuilder.withLayersStateFactory(this::bLayersState);
            bSG_ = bBuilder.build(ci, new DefaultIOComposerTimes());
        }

        final LayeredServiceGraph aSG = optimizer.apply(aSG_);
        final LayeredServiceGraph bSG = optimizer.apply(bSG_);
        aLayers = aSG.getLayers().stream().map(l ->new HashSet<>(l.getNodes())).collect(toList());
        bLayers = bSG.getLayers().stream().map(l ->new HashSet<>(l.getNodes())).collect(toList());
        Assert.assertEquals(bLayers.size(), aLayers.size());
        for (int i = 0; i < aLayers.size(); i++) {
            final int layerIdx = i;
            String helper = "layerIdx="+layerIdx;
            if (renameNodes) {
                Assert.assertEquals(bLayers.get(i).size(), aLayers.get(i).size(), helper);
            } else {
                Assert.assertEquals(bLayers.get(i), aLayers.get(i), helper);
                Assert.assertEquals(bLayers.get(i).stream()
                        .filter(n -> !bSG.getProviderSet(n).stream().collect(toSet())
                                .equals(aSG.getProviderSet(n).stream().collect(toSet())))
                        .collect(toList()), emptyList(), helper);
            }
            Assert.assertEquals(bLayers.get(i).stream()
                            .filter(n -> bSG.getNodeLayer(n) != layerIdx).collect(toList()),
                    emptyList(), helper); // no provider in same layer
            Assert.assertEquals(aLayers.get(i).stream()
                    .flatMap(n -> aSG.getProviderSet(n).getAssignmentsStream().map(a -> of(n, a)))
                    .filter(p -> p.right.target.equals(p.right.output))
                    .collect(toList()), emptyList()); //no self-assignments
            Assert.assertEquals(bLayers.get(i).stream()
                    .flatMap(n -> bSG.getProviderSet(n).getAssignmentsStream().map(a -> of(n, a)))
                    .filter(p -> p.right.target.equals(p.right.output))
                    .collect(toList()), emptyList()); //no self-assignments
            Assert.assertEquals(aLayers.get(i).stream()
                            .flatMap(n -> aSG.getProviderSet(n).stream())
                            .filter(n -> aSG.getNodeLayer(n) >= layerIdx).collect(toList()),
                    emptyList(), helper); // no providers in successor layers
            Assert.assertEquals(bLayers.get(i).stream()
                            .flatMap(n -> bSG.getProviderSet(n).stream())
                            .filter(n -> bSG.getNodeLayer(n) >= layerIdx).collect(toList()),
                    emptyList(), helper); // no provider in successor layers
        }
    }
}

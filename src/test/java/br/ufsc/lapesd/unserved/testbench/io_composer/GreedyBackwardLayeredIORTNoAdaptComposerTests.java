package br.ufsc.lapesd.unserved.testbench.io_composer;

import br.ufsc.lapesd.unserved.testbench.io_composer.layered.GreedyBackwardLayeredIOComposer;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.LayeredServiceSetGraphIOComposer;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.lazy.SingleLeastRTBackwardSetNodeProvider;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.optimizations.ServiceCompressionOptimizer;
import org.testng.annotations.Test;

@Test
public class GreedyBackwardLayeredIORTNoAdaptComposerTests {
    public class ApiDemoTests extends ApiDemoTestBase {
        @Override
        protected IOComposerBuilder getBuilder() {
            return new GreedyBackwardLayeredIOComposer.Builder()
                    .withCost(new LayeredServiceSetGraphIOComposer.RTCost(100))
                    .setBackwardSetNodeProvider(new SingleLeastRTBackwardSetNodeProvider());
        }
    }

    public class DescriptionGenerator extends DescriptionGeneratorTestBase {
        @Override
        protected IOComposerBuilder getBuilder() {
            return new GreedyBackwardLayeredIOComposer.Builder()
                    .withCost(new LayeredServiceSetGraphIOComposer.RTCost(100))
                    .setBackwardSetNodeProvider(new SingleLeastRTBackwardSetNodeProvider());
        }
    }

    public class WscConverted extends WscConvertedTestBase {
        @Override
        protected IOComposerBuilder getBuilder() {
            return new GreedyBackwardLayeredIOComposer.Builder()
                    .withCost(new LayeredServiceSetGraphIOComposer.RTCost(100))
                    .setBackwardSetNodeProvider(new SingleLeastRTBackwardSetNodeProvider());
        }
    }

    public class PubSub extends PubSubTestBase {
        @Override
        protected IOComposerBuilder getBuilder() {
            return new GreedyBackwardLayeredIOComposer.Builder()
                    .withCost(new LayeredServiceSetGraphIOComposer.RTCost(100))
                    .setBackwardSetNodeProvider(new SingleLeastRTBackwardSetNodeProvider());
        }
    }

    public class ApiDemoTestsWithCompression extends ApiDemoTestBase {
        @Override
        protected IOComposerBuilder getBuilder() {
            return new GreedyBackwardLayeredIOComposer.Builder()
                    .withCost(new LayeredServiceSetGraphIOComposer.RTCost(100))
                    .setBackwardSetNodeProvider(new SingleLeastRTBackwardSetNodeProvider())
                    .withOptimizer(new ServiceCompressionOptimizer());
        }
    }

    public class DescriptionGeneratorWithCompression extends DescriptionGeneratorTestBase {
        @Override
        protected IOComposerBuilder getBuilder() {
            return new GreedyBackwardLayeredIOComposer.Builder()
                    .withCost(new LayeredServiceSetGraphIOComposer.RTCost(100))
                    .setBackwardSetNodeProvider(new SingleLeastRTBackwardSetNodeProvider())
                    .withOptimizer(new ServiceCompressionOptimizer());
        }
    }

    public class WscConvertedWithCompression extends WscConvertedTestBase {
        @Override
        protected IOComposerBuilder getBuilder() {
            return new GreedyBackwardLayeredIOComposer.Builder()
                    .withCost(new LayeredServiceSetGraphIOComposer.RTCost(100))
                    .setBackwardSetNodeProvider(new SingleLeastRTBackwardSetNodeProvider())
                    .withOptimizer(new ServiceCompressionOptimizer());
        }
    }

    public class PubSubWithCompression extends PubSubTestBase {
        @Override
        protected IOComposerBuilder getBuilder() {
            return new GreedyBackwardLayeredIOComposer.Builder()
                    .withCost(new LayeredServiceSetGraphIOComposer.RTCost(100))
                    .setBackwardSetNodeProvider(new SingleLeastRTBackwardSetNodeProvider())
                    .withOptimizer(new ServiceCompressionOptimizer());
        }
    }
}

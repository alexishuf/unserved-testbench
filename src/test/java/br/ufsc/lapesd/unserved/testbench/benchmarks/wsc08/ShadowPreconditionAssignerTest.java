package br.ufsc.lapesd.unserved.testbench.benchmarks.wsc08;

import br.ufsc.lapesd.unserved.testbench.Algorithm;
import br.ufsc.lapesd.unserved.testbench.Utils;
import br.ufsc.lapesd.unserved.testbench.benchmarks.wsc08.ShadowPreconditionAssigner.Config;
import br.ufsc.lapesd.unserved.testbench.input.RDFInput;
import br.ufsc.lapesd.unserved.testbench.input.RDFInputFile;
import br.ufsc.lapesd.unserved.testbench.io_composer.IOComposerBuilder;
import br.ufsc.lapesd.unserved.testbench.io_composer.WscConvertedTestBase;
import br.ufsc.lapesd.unserved.testbench.io_composer.impl.DefaultIOComposerTimes;
import br.ufsc.lapesd.unserved.testbench.io_composer.impl.IOComposerInput;
import br.ufsc.lapesd.unserved.testbench.io_composer.impl.IOComposerInputImpl;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.LayeredServiceSetGraphIOComposer;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.lazy.LazyLayeredServiceSetGraph;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.lazy.NaiveBackwardSetNodeProvider;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.lazy.NonEquivalentBackwardSetNodeProvider;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.lazy.SetNode;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.optimizations.BackwardClosureOptimizer;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.optimizations.ServiceCompressionOptimizer;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.*;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.discovery.FastIOLayersState;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.discovery.FastPreconditionsLayersState;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.discovery.InputServiceIndex;
import br.ufsc.lapesd.unserved.testbench.model.Variable;
import br.ufsc.lapesd.unserved.testbench.util.NaiveTransitiveClosureGetter;
import org.apache.commons.io.FileUtils;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFFormat;
import org.apache.jena.vocabulary.RDFS;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static br.ufsc.lapesd.unserved.testbench.benchmarks.wsc08.ShadowPreconditionAssigner.Config.ConditionStrategy;
import static br.ufsc.lapesd.unserved.testbench.benchmarks.wsc08.ShadowPreconditionAssigner.Config.PredicateStrategy;
import static br.ufsc.lapesd.unserved.testbench.io_composer.WscConvertedTestBase.getWscTurtle;
import static java.util.stream.Collectors.toList;

@Test(enabled = true)
public class ShadowPreconditionAssignerTest {

    @Nonnull
    private static List<Config> getConfigs() {
        List<Config> cfgs = new ArrayList<>();
        for (int i = 0; i < 10; i++) cfgs.add(new Config());

        cfgs.get(0).predicateStrategy = PredicateStrategy.SINGLE;
        cfgs.get(0).conditionStrategy = ConditionStrategy.NO_DISJUNCTION;

        cfgs.get(1).predicateStrategy = PredicateStrategy.FIXED_SET;
        cfgs.get(1).predicateStrategyArgs = new HashMap<>();
        cfgs.get(1).predicateStrategyArgs.put("count", "20");
        cfgs.get(1).conditionStrategy = ConditionStrategy.NO_DISJUNCTION;

        cfgs.get(2).predicateStrategy = PredicateStrategy.SHADOW;
        cfgs.get(2).conditionStrategy = ConditionStrategy.NO_DISJUNCTION;


        cfgs.get(3).predicateStrategy = PredicateStrategy.SINGLE;
        cfgs.get(3).conditionStrategy = ConditionStrategy.NO_DISJUNCTION;
        cfgs.get(3).conditionStrategyArgs = new HashMap<>();
        cfgs.get(3).conditionStrategyArgs.put("spinPostconditions", "true");


        cfgs.get(4).predicateStrategy = PredicateStrategy.FIXED_SET;
        cfgs.get(4).predicateStrategyArgs = new HashMap<>();
        cfgs.get(4).predicateStrategyArgs.put("count", "20");
        cfgs.get(4).conditionStrategy = ConditionStrategy.NO_DISJUNCTION;
        cfgs.get(4).conditionStrategyArgs = new HashMap<>();
        cfgs.get(4).conditionStrategyArgs.put("spinPostconditions", "true");


        cfgs.get(5).predicateStrategy = PredicateStrategy.SHADOW;
        cfgs.get(5).conditionStrategy = ConditionStrategy.NO_DISJUNCTION;
        cfgs.get(5).conditionStrategyArgs = new HashMap<>();
        cfgs.get(5).conditionStrategyArgs.put("spinPostconditions", "true");


        cfgs.get(6).predicateStrategy = PredicateStrategy.SINGLE;
        cfgs.get(6).conditionStrategy = ConditionStrategy.DIVIDE;
        cfgs.get(6).conditionStrategyArgs = new HashMap<>();
        cfgs.get(6).conditionStrategyArgs.put("seed", "9");
        cfgs.get(6).conditionStrategyArgs.put("groups", "2");

        cfgs.get(7).predicateStrategy = PredicateStrategy.FIXED_SET;
        cfgs.get(7).predicateStrategyArgs = new HashMap<>();
        cfgs.get(7).predicateStrategyArgs.put("count", "20");
        cfgs.get(7).conditionStrategy = ConditionStrategy.DIVIDE;
        cfgs.get(7).conditionStrategyArgs = new HashMap<>();
        cfgs.get(7).conditionStrategyArgs.put("seed", "9");
        cfgs.get(7).conditionStrategyArgs.put("groups", "2");

        cfgs.get(8).predicateStrategy = PredicateStrategy.SHADOW;
        cfgs.get(8).conditionStrategy = ConditionStrategy.DIVIDE;
        cfgs.get(8).conditionStrategyArgs = new HashMap<>();
        cfgs.get(8).conditionStrategyArgs.put("seed", "9");
        cfgs.get(8).conditionStrategyArgs.put("groups", "2");

        cfgs.get(9).predicateStrategy = PredicateStrategy.SINGLE;
        cfgs.get(9).conditionStrategy = ConditionStrategy.NO_CONDITION;
        return cfgs;
    }

    @DataProvider
    public static Object[][] configs() {
        List<Object[]> rows = new ArrayList<>();
        getConfigs().forEach(c -> rows.add(new Object[]{c}));
        return rows.toArray(new Object[0][]);
    }

    @DataProvider
    public static Object[][] data() {
        List<Config> configs = getConfigs();

        List<Object[]> rows = new ArrayList<>();
        for (Object[] row : WscConvertedTestBase.problemNames()) {
//            Object[] row = WscConvertedTestBase.problemNames()[0];
            String problem = (String) row[0];
            for (Config cfg : configs) rows.add(new Object[]{problem, cfg});
        }
        return rows.toArray(new Object[0][]);
    }

    @Test(dataProvider = "data")
    public void testServiceGraph(@Nonnull String problemName,
                                 @Nonnull Config config) throws Exception {
        File dir = convert(problemName, config);
        try {
            LayeredServiceGraph ioSG;
            try (IOComposerInput ioCI = getWscComposerInput(problemName)) {
                ForwardLayeredServiceGraphBuilder ioBuilder = new ForwardLayeredServiceGraphBuilder();
                ioBuilder.withLayersStateFactory(ci -> new FastIOLayersState(new InputServiceIndex()));
                ioSG = ioBuilder.build(ioCI, new DefaultIOComposerTimes());
            }
            LayeredServiceGraph ioCompressedSG = new ServiceCompressionOptimizer().apply(ioSG);
            LayeredServiceGraph ioPrunedSG = new BackwardClosureOptimizer().apply(ioCompressedSG);
            for (int i = 0; i < 3; i++) {
                LayeredServiceGraph prSG;
                try (IOComposerInput prCI = getConvertedComposerInput(dir)) {
                    ForwardLayeredServiceGraphBuilder prBuilder = new ForwardLayeredServiceGraphBuilder();
                    NaiveTransitiveClosureGetter tcg =
                            new NaiveTransitiveClosureGetter(RDFS.subClassOf, false);
                    prBuilder.withLayersStateFactory(ci -> new FastPreconditionsLayersState(
                            new InputServiceIndex(tcg, true)));

                    prSG = prBuilder.build(prCI, new DefaultIOComposerTimes());
                }
                checkServiceGraphs(ioSG, prSG);

                prSG = new ServiceCompressionOptimizer().apply(prSG);
                checkServiceGraphs(ioCompressedSG, prSG);

                prSG = new BackwardClosureOptimizer().apply(prSG);
                checkServiceGraphs(ioPrunedSG, prSG);
            }
        } finally {
            FileUtils.deleteDirectory(dir);
        }
    }

    private void checkServiceGraphs(LayeredServiceGraph ioSG, LayeredServiceGraph prSG) {
        List<List<Node>> ioLayers = ioSG.getLayers().stream().map(ServiceLayer::getNodes)
                .collect(toList());
        List<List<Node>> prLayers = prSG.getLayers().stream().map(ServiceLayer::getNodes)
                .collect(toList());
        List<Map<String, ServiceNode>> ioNames = getWscServices(ioLayers);
        List<Map<String, ServiceNode>> prNames = getWscServices(prLayers);

        Assert.assertEquals(ioLayers.size(), prLayers.size());

        for (int i = 0; i < ioLayers.size(); i++) {
            final int lIdx = i;
            String helper = "layerIdx=" + i;

            Assert.assertEquals(ioLayers.get(i).stream().filter(n -> ioSG.getNodeLayer(n) != lIdx)
                    .collect(toList()), Collections.emptyList(), helper);
            Assert.assertEquals(prLayers.get(i).stream().filter(n -> prSG.getNodeLayer(n) != lIdx)
                    .collect(toList()), Collections.emptyList(), helper);

            Assert.assertEquals(ioNames.get(i).keySet(), prNames.get(i).keySet(), helper);

            for (Map.Entry<String, ServiceNode> prEntry : prNames.get(i).entrySet()) {
                String subHelper = helper + ";prN=" + prEntry.getKey();
                ProviderSet prPS = prSG.getProviderSet(prEntry.getValue());
                TreeSet<String> pr = getWscServices(prPS.stream());

                ServiceNode ioN = ioNames.get(i).get(prEntry.getKey());
                Assert.assertNotNull(ioN, subHelper);
                ProviderSet ioPS = ioSG.getProviderSet(ioN);
                TreeSet<String> io = getWscServices(ioPS.stream());
                Assert.assertEquals(pr, io, subHelper);

                for (Variable ioIn : ioPS.getTargets()) {
                    String ssHelper = subHelper + ";in=" + ioIn;
                    Variable prIn = prEntry.getValue().getInputs().stream()
                            .filter(v -> v.getType().equals(ioIn.getType()))
                            .findFirst().orElse(null);
                    Assert.assertNotNull(prIn, ssHelper);
                    Assert.assertEquals(getWscServices(ioPS.getProviders(ioIn).stream()),
                            getWscServices(prPS.getProviders(prIn).stream()), ssHelper);

                    String shadowURI = ioIn.getType().getURI()
                            .replace("taxonomy.owl", "taxonomy-shadow.owl");
                    Variable shIn = prEntry.getValue().getInputs().stream()
                            .filter(v -> v.getType().getURI().equals(shadowURI))
                            .findFirst().orElse(null);
                    Assert.assertNotNull(shIn, ssHelper);
                    Assert.assertEquals(getWscServices(ioPS.getProviders(ioIn).stream()),
                            getWscServices(prPS.getProviders(shIn).stream()), ssHelper);
                }

                io = getWscServices(ioSG.getPartialSuccessors(ioN).stream());
                pr = getWscServices(prSG.getPartialSuccessors(prEntry.getValue()).stream());
                Assert.assertEquals(io, pr, subHelper);
            }
        }
    }

    @Test(dataProvider = "configs")
    public void testCompositionTree(@Nonnull Config config) throws Exception {
        File dir = convert("01", config);
        try {
            LayeredServiceGraph ioSG, prSG;
            try (IOComposerInput ioCI = getWscComposerInput("01");
                 IOComposerInput prCI = getConvertedComposerInput(dir)) {
                ForwardLayeredServiceGraphBuilder ioBuilder = new ForwardLayeredServiceGraphBuilder();
                ioBuilder.withLayersStateFactory(ci -> new FastIOLayersState(new InputServiceIndex()));
                ioSG = ioBuilder.build(ioCI, new DefaultIOComposerTimes());

                ForwardLayeredServiceGraphBuilder prBuilder = new ForwardLayeredServiceGraphBuilder();
                NaiveTransitiveClosureGetter tcg =
                        new NaiveTransitiveClosureGetter(RDFS.subClassOf, false);
                prBuilder.withLayersStateFactory(ci -> new FastPreconditionsLayersState(
                        new InputServiceIndex(tcg, true)));

                prSG = prBuilder.build(prCI, new DefaultIOComposerTimes());
            }
            ioSG = new ServiceCompressionOptimizer().apply(ioSG);
            ioSG = new BackwardClosureOptimizer().apply(ioSG);
            prSG = new ServiceCompressionOptimizer().apply(prSG);
            prSG = new BackwardClosureOptimizer().apply(prSG);
            checkServiceGraphs(ioSG, prSG);

            for (int i = 0; i < 3; i++) {
                NonEquivalentBackwardSetNodeProvider ioSNP = new NonEquivalentBackwardSetNodeProvider();
                ioSNP.setForceCombinedJumpNodes(true);
                NonEquivalentBackwardSetNodeProvider prSNP = new NonEquivalentBackwardSetNodeProvider();
                prSNP.setUseConditions(true);
                LazyLayeredServiceSetGraph ioSSG = new LazyLayeredServiceSetGraph(ioSG, ioSNP);
                LazyLayeredServiceSetGraph prSSG = new LazyLayeredServiceSetGraph(prSG, prSNP);

                SetNode ioNode = ioSSG.getEnd(), prNode = prSSG.getEnd();
                for (int depth = 1; !ioNode.equals(ioSSG.getStart()) ; depth++) {
                    String helper = "depth="+depth;
                    Collection<SetNode> io = ioSSG.predecessors(ioNode);
                    Assert.assertEquals(io.size(), 1, helper);
                    Collection<SetNode> pr = prSSG.predecessors(prNode);
                    Assert.assertEquals(pr.size(), 1, helper);
                    Assert.assertEquals(getWscServices(io), getWscServices(pr), helper);
                    ioNode = io.iterator().next();
                    prNode = pr.iterator().next();
                }
            }
        } finally {
            FileUtils.deleteDirectory(dir);
        }
    }

    @Test(dataProvider = "data")
    public void testComposition(@Nonnull String problemName, @Nonnull Config config) throws Exception {
//        if (Integer.parseInt(problemName) < 3)
//            throw new SkipException("skipping FIXED_SET predicateStrategy");
        File dir = convert(problemName, config);
        WscConvertedTestBase tester = null;
        try {
            for (int i = 0; i < 2; i++) {
                tester = new WscConvertedTestBase() {
                    @Override
                    protected IOComposerBuilder getBuilder() {
                        IOComposerBuilder ioBuilder = Algorithm.BackwardAStarLayeredIO
                                .createIOComposerBuilder();
                        LayeredServiceSetGraphIOComposer.Builder builder =
                                (LayeredServiceSetGraphIOComposer.Builder) ioBuilder;
                        builder.withOptimizer(new ServiceCompressionOptimizer());
                        builder.withOptimizer(new BackwardClosureOptimizer());
//                        builder.setBackwardSetNodeProvider(new NaiveBackwardSetNodeProvider()
//                                .setUseConditions(true));
                        ((NaiveBackwardSetNodeProvider) builder.getBackwardSetNodeProvider())
                                .setUseConditions(true);
                        NaiveTransitiveClosureGetter tcg =
                                new NaiveTransitiveClosureGetter(RDFS.subClassOf, false);
                        builder.setLayersStateFactory(ci -> new FastPreconditionsLayersState(
                                new InputServiceIndex(tcg, true)));
                        return builder;
                    }
                };
                tester.setUp();
                tester.runTestProblem(new RDFInputFile(new File(dir, "taxonomy.ttl"), RDFFormat.TURTLE),
                        new RDFInputFile(new File(dir, "services.ttl"), RDFFormat.TURTLE),
                        new RDFInputFile(new File(dir, "known.ttl"), RDFFormat.TURTLE),
                        new RDFInputFile(new File(dir, "wanted.ttl"), RDFFormat.TURTLE));
                tester.tearDown();
                tester = null;
            }
        } finally {
            if (tester != null) tester.tearDown();
            FileUtils.deleteDirectory(dir);
        }
    }

    @Nonnull
    private TreeSet<String> getWscServices(@Nonnull Collection<SetNode> collection) {
        return getWscServices(collection.stream().flatMap(sn -> sn.getNodes().stream()));
    }

    @Nonnull
    private TreeSet<String> getWscServices(@Nonnull Stream<Node> stream) {
        return stream.map(n -> {
            if (n instanceof ServiceNode) {
                return ((ServiceNode) n).getAntecedent().getRequiredProperty(Wscc.wscService)
                        .getLiteral().getLexicalForm()
                        .replace("http://www.ws-challenge.org/WSC08Services/#", "");
            } else if (n instanceof StartNode) {
                return "StartNode";
            } else if (n instanceof EndNode) {
                return "EndNode";
            } else {
                return "UnknownNodeType";
            }
        }).collect(Collectors.toCollection(TreeSet::new));
    }

    @Nonnull
    private List<Map<String, ServiceNode>> getWscServices(@Nonnull List<List<Node>> layers) {
        return layers.stream().map(l -> l.stream()
                .filter(n -> n instanceof ServiceNode)
                .collect(Collectors.toMap(n -> ((ServiceNode) n).getAntecedent()
                                .getRequiredProperty(Wscc.wscService).getLiteral().getLexicalForm()
                                .replace("http://www.ws-challenge.org/WSC08Services/#", ""),
                        n -> (ServiceNode) n))
        ).collect(toList());
    }

    private IOComposerInput getWscComposerInput(@Nonnull String problemName) throws Exception {
        IOComposerInputImpl ci = new IOComposerInputImpl();
        ci.addInput(getWscTurtle(problemName, "taxonomy"));
        ci.addInput(getWscTurtle(problemName, "services"));
        ci.addInput(getWscTurtle(problemName, "known"));
        Utils.addWantedVars(ci, getWscTurtle(problemName, "wanted"), null);
        ci.initSkolemizedUnion(false, false);
        return ci;
    }

    private IOComposerInput getConvertedComposerInput(File convertedDir) throws Exception {
        convertedDir.deleteOnExit();
        IOComposerInputImpl ci = new IOComposerInputImpl();
        ci.addInput(new RDFInputFile(new File(convertedDir, "taxonomy.ttl"), RDFFormat.TURTLE));
        ci.addInput(new RDFInputFile(new File(convertedDir, "services.ttl"), RDFFormat.TURTLE));
        ci.addInput(new RDFInputFile(new File(convertedDir, "known.ttl"), RDFFormat.TURTLE));
        Utils.addWantedVars(ci, new RDFInputFile(new File(convertedDir, "wanted.ttl"),
                RDFFormat.TURTLE), null);
        ci.initSkolemizedUnion(false, false);
        return ci;
    }

    private File convert(String problem, Config config) throws IOException {
        File dir = Files.createTempDirectory(problem + "-").toFile();
        dir.deleteOnExit();
        try {
            toFile(problem, dir, "taxonomy.ttl");
            toFile(problem, dir, "services.ttl");
            toFile(problem, dir, "known.ttl");
            toFile(problem, dir, "wanted.ttl");
            new ShadowPreconditionAssigner(config).addToProblem(dir);
        } catch (Exception e) {
            FileUtils.deleteDirectory(dir);
            throw e;
        }
        return dir;
    }

    private void toFile(String problem, File outDir, String filename) throws IOException {
        try (FileOutputStream out = new FileOutputStream(new File(outDir, filename));
             RDFInput rdfInput = getWscTurtle(problem, filename.replaceAll("\\.ttl", ""))) {
            RDFDataMgr.write(out, rdfInput.getModel(), RDFFormat.TURTLE_FLAT);
        }
    }
}

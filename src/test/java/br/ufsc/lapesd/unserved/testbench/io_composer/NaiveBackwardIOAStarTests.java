package br.ufsc.lapesd.unserved.testbench.io_composer;

import br.ufsc.lapesd.unserved.testbench.io_composer.backward.BackwardIOAStar;
import org.testng.annotations.Test;

@Test
public class NaiveBackwardIOAStarTests {
    @Test
    public class ApiDemoTests extends ApiDemoTestBase {
        @Override
        protected IOComposerBuilder getBuilder() {
            return new BackwardIOAStar.Builder();
        }
    }

    @Test
    public class DescriptionGenerator extends DescriptionGeneratorTestBase {
        @Override
        protected IOComposerBuilder getBuilder() {
            return new BackwardIOAStar.Builder();
        }
    }
}

package br.ufsc.lapesd.unserved.testbench.io_composer;

import br.ufsc.lapesd.unserved.testbench.io_composer.layered.DStarLiteLayeredIOComposer;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.lazy.NonEquivalentBackwardSetNodeProvider;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.optimizations.ServiceCompressionOptimizer;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.discovery.FastPreconditionsLayersState;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.discovery.InputServiceIndex;
import br.ufsc.lapesd.unserved.testbench.util.FixedCacheTransitiveClosureGetter;
import br.ufsc.lapesd.unserved.testbench.util.NaiveTransitiveClosureGetter;
import org.apache.jena.vocabulary.RDFS;
import org.testng.annotations.Test;

@Test
public class DStarLiteLayeredIOComposerDefaultTests {
    public class ApiDemoTests extends ApiDemoTestBase {
        @Override
        protected IOComposerBuilder getBuilder() {
            return new DStarLiteLayeredIOComposer.Builder();
        }
    }

    public class DescriptionGenerator extends DescriptionGeneratorTestBase {
        @Override
        protected IOComposerBuilder getBuilder() {
            return new DStarLiteLayeredIOComposer.Builder();
        }
    }

    public class WscConverted extends WscConvertedTestBase {
        @Override
        protected IOComposerBuilder getBuilder() {
            return new DStarLiteLayeredIOComposer.Builder();
        }
    }

    public class PubSub extends PubSubTestBase {

        @Override
        protected IOComposerBuilder getBuilder() {
            return new DStarLiteLayeredIOComposer.Builder();
        }
    }

    public class ApiDemoTestsWithCompression extends ApiDemoTestBase {
        @Override
        protected IOComposerBuilder getBuilder() {
            return new DStarLiteLayeredIOComposer.Builder()
                    .withOptimizer(new ServiceCompressionOptimizer());
        }
    }

    public class DescriptionGeneratorWithCompression extends DescriptionGeneratorTestBase {
        @Override
        protected IOComposerBuilder getBuilder() {
            return new DStarLiteLayeredIOComposer.Builder()
                    .withOptimizer(new ServiceCompressionOptimizer());
        }
    }

    public class WscConvertedWithCompression extends WscConvertedTestBase {
        @Override
        protected IOComposerBuilder getBuilder() {
            return new DStarLiteLayeredIOComposer.Builder()
                    .withOptimizer(new ServiceCompressionOptimizer());
        }
    }

    public class PubSubWithCompression extends PubSubTestBase {
        @Override
        protected IOComposerBuilder getBuilder() {
            return new DStarLiteLayeredIOComposer.Builder()
                    .withOptimizer(new ServiceCompressionOptimizer());
        }
    }

    public class ApiDemoTestsWithPreconditions extends ApiDemoTestBase {
        @Override
        protected IOComposerBuilder getBuilder() {
            return new DStarLiteLayeredIOComposer.Builder()
                    .setBackwardSetNodeProvider(new NonEquivalentBackwardSetNodeProvider().setUseConditions(true))
                    .setLayersStateFactory(ci -> new FastPreconditionsLayersState(
                            new InputServiceIndex(new NaiveTransitiveClosureGetter(RDFS.subClassOf, false), true)));
        }
    }

    public class DescriptionGeneratorWithPreconditions extends DescriptionGeneratorWithPreconditionsTestBase {
        @Override
        protected IOComposerBuilder getBuilder() {
            return new DStarLiteLayeredIOComposer.Builder()
                    .setBackwardSetNodeProvider(new NonEquivalentBackwardSetNodeProvider().setUseConditions(true))
                    .setLayersStateFactory(ci -> new FastPreconditionsLayersState(
                            new InputServiceIndex(new NaiveTransitiveClosureGetter(RDFS.subClassOf, false), true)));
        }
    }

    public class WscConvertedWithPreconditions extends WscConvertedTestBase {
        @Override
        protected IOComposerBuilder getBuilder() {
            return new DStarLiteLayeredIOComposer.Builder()
                    .setBackwardSetNodeProvider(new NonEquivalentBackwardSetNodeProvider().setUseConditions(true))
                    .setLayersStateFactory(ci -> new FastPreconditionsLayersState(
                            new InputServiceIndex(new NaiveTransitiveClosureGetter(RDFS.subClassOf, false), true)));
        }
    }

    public class WscConvertedWithPreconditionsAndCompression extends WscConvertedTestBase {
        @Override
        protected IOComposerBuilder getBuilder() {
            return new DStarLiteLayeredIOComposer.Builder()
                    .setBackwardSetNodeProvider(new NonEquivalentBackwardSetNodeProvider().setUseConditions(true))
                    .withOptimizer(new ServiceCompressionOptimizer())
                    .setLayersStateFactory(ci -> new FastPreconditionsLayersState(
                            new InputServiceIndex(new NaiveTransitiveClosureGetter(RDFS.subClassOf, false), true)));
        }
    }

    public class WscConvertedWithPreconditionsAndFixedGetter extends WscConvertedTestBase {
        @Override
        protected IOComposerBuilder getBuilder() {
            return new DStarLiteLayeredIOComposer.Builder()
                    .setBackwardSetNodeProvider(new NonEquivalentBackwardSetNodeProvider().setUseConditions(true))
                    .setLayersStateFactory(ci -> new FastPreconditionsLayersState(new InputServiceIndex(
                            FixedCacheTransitiveClosureGetter.forward(RDFS.subClassOf)
                                    .setThreshold(3).visitAll(ci.getSkolemizedUnion()).build(), true)));
        }
    }


    public class PubSubWithPreconditions extends PubSubTestBase {
        @Override
        protected IOComposerBuilder getBuilder() {
            return new DStarLiteLayeredIOComposer.Builder()
                    .setBackwardSetNodeProvider(new NonEquivalentBackwardSetNodeProvider().setUseConditions(true))
                    .setLayersStateFactory(ci -> new FastPreconditionsLayersState(
                            new InputServiceIndex(new NaiveTransitiveClosureGetter(RDFS.subClassOf, false), true)));
        }
    }
}

package br.ufsc.lapesd.unserved.testbench.io_composer;

import br.ufsc.lapesd.unserved.testbench.Utils;
import br.ufsc.lapesd.unserved.testbench.benchmarks.DescriptionsGenerator;
import br.ufsc.lapesd.unserved.testbench.components.SimpleActionRunnerFactory;
import br.ufsc.lapesd.unserved.testbench.composer.Composition;
import br.ufsc.lapesd.unserved.testbench.composer.ReplicatedComposer;
import br.ufsc.lapesd.unserved.testbench.input.RDFInputFile;
import br.ufsc.lapesd.unserved.testbench.iri.UnservedP;
import br.ufsc.lapesd.unserved.testbench.model.Message;
import br.ufsc.lapesd.unserved.testbench.model.Variable;
import br.ufsc.lapesd.unserved.testbench.process.actions.ActionRunner;
import br.ufsc.lapesd.unserved.testbench.process.actions.mock.MockReceiveRunner;
import br.ufsc.lapesd.unserved.testbench.process.data.DataContext;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFFormat;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import static org.apache.jena.rdf.model.ResourceFactory.createResource;

@Test
public abstract class DescriptionGeneratorTestBase extends CompositionTestBase {
    private List<File> tempFiles = new ArrayList<>();
    private static final String varPrefix = "https://alexishuf.bitbucket.io/2016/04/unserved/var#";
    private static final String exPrefix = "http://example.org/#";

    private File getTempFile(String suffix) throws IOException {
        File file = Files.createTempFile("unserved-testbench", suffix).toFile();
        tempFiles.add(file);
        return file;
    }

    @BeforeMethod
    @Override
    public void setUp() throws Exception {
        super.setUp();
        builder.addInput(Utils.resourceInput(
                "io_composer/description-generator-known.ttl", Lang.TURTLE));
        Utils.addWantedVars(builder, Utils.resourceInput(
                "io_composer/description-generator-wanted.ttl", Lang.TURTLE));
    }

    @AfterMethod
    @Override
    public void tearDown() throws Exception {
        super.tearDown();
        tempFiles.forEach(File::delete);
    }

    @DataProvider
    public static Object[][] generatorParams() {
        return new Object[][] {
                {2, 1, 0, false, false},
                {8, 1, 0, false, false},
                {2, 2, 0, false, false},
                {8, 2, 0, false, false},
                {2, 1, 2, false, false},
                {8, 1, 2, false, false},
                {2, 2, 2, false, false},
                {8, 2, 2, false, false},

                {2, 1, 0, true, false},
                {8, 1, 0, true, false},
                {2, 2, 0, true, false},
                {8, 2, 0, true, false},
                {2, 1, 2, true, false},
                {8, 1, 2, true, false},
                {2, 2, 2, true, false},
                {8, 2, 2, true, false},
        };
    }

    @Test(dataProvider = "generatorParams")
    public void test(int chainLength, int conditions, int nonDummyChain,
                     boolean universal, boolean repeatRelations) throws Exception {
        componentRegistry.unregisterComponent(MockReceiveRunner.Factory.class);
        componentRegistry.registerComponent(MagicGoalReceiveRunnerFactory.class);

        File descriptionFile = getTempFile(".ttl"), rulesFile = getTempFile(".n3");
        new DescriptionsGenerator.Builder(chainLength)
                .setUniversal(universal)
                .setRepeatRelations(repeatRelations)
                .setConditions(conditions)
                .setRel(nonDummyChain > 0 ? "relDummy" : "rel")
                .setSkIRI(nonDummyChain > 0 ? "http://example.org/skolemDummy#"
                                            : "http://example.org/skolem#")
                .setUnservedOut(descriptionFile)
                .setRulesOut(rulesFile)
                .build().run();
        builder.addInput(new RDFInputFile(descriptionFile, RDFFormat.TURTLE));
        if (nonDummyChain > 0) {
            File nonDummyDescriptionFile = getTempFile(".ttl");
            new DescriptionsGenerator.Builder(chainLength)
                    .setUniversal(universal)
                    .setRepeatRelations(repeatRelations)
                    .setConditions(conditions)
                    .setUnservedOut(nonDummyDescriptionFile)
                    .setRulesOut(rulesFile)
                    .build().run();
            builder.addInput(new RDFInputFile(nonDummyDescriptionFile, RDFFormat.TURTLE));
        }

        IOComposer composer = builder.build();
        if (composer instanceof ReplicatedComposer) {
            ReplicatedComposer repComposer = (ReplicatedComposer) composer;
            CompletableFuture<IOCompositionResult> future = new CompletableFuture<>();
            interpreter.setResultConsumer(IOCompositionResult.class, future::complete);
            repComposer.run(interpreter);
            try (IOCompositionResult result = future.get()) {
                Resource wantedResource = result.getWantedVariable(createResource(varPrefix + "wanted"));
                Assert.assertNotNull(wantedResource);
                Assert.assertTrue(wantedResource.canAs(Variable.class));

                Variable wanted = wantedResource.as(Variable.class);
                Assert.assertTrue(wanted.isValueBound());
                Assert.assertEquals(wanted.getResourceValue(),
                        createResource("http://example.org/aGoal"));
            }
        } else {
            try (Composition comp = composer.run();
                 DataContext data = comp.isNull() ? null : comp.createDataContext()) {
                Assert.assertNotNull(data);
                Assert.assertNotNull(comp.getWorkflowRoot());

                Variable wanted = data.getResource(varPrefix + "wanted", Variable.class);
                Assert.assertNotNull(wanted);
                Assert.assertFalse(wanted.isValueBound());

                interpreter.interpret(data, comp.getWorkflowRoot()).waitForDone();
                Assert.assertTrue(wanted.isValueBound());
                Assert.assertEquals(wanted.getResourceValue(),
                        createResource("http://example.org/aGoal"));
            }
        }
    }

    public static class MagicGoalReceiveRunnerFactory extends SimpleActionRunnerFactory {

        public MagicGoalReceiveRunnerFactory() {
            super(MockReceiveRunner.class, MagicGoalReceiveRunnerFactory.class, UnservedP.Receive);
        }

        @Override
        public ActionRunner newInstance() {
            return new MockReceiveRunner() {
                @Override
                protected void setResource(DataContext context, Message reaction, Variable var) {
                    Resource t = var.getType();
                    if (t != null && t.getURI().equals(exPrefix + "Type-relGoal")) {
                        super.setResource(context, reaction, var,
                                createResource("http://example.org/aGoal"));
                    } else {
                        super.setResource(context, reaction, var);
                    }
                }
            };
        }
    }
}

package br.ufsc.lapesd.unserved.testbench.benchmarks;

import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.cost.CostInfo;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.cost.CostInfoParser;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.cost.ResponseTime;
import br.ufsc.lapesd.unserved.testbench.io_composer.layered.graph.service.cost.Throughput;
import br.ufsc.lapesd.unserved.testbench.iri.Unserved;
import br.ufsc.lapesd.unserved.testbench.iri.UnservedN;
import br.ufsc.lapesd.unserved.testbench.util.ResourceExtractor;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.XSD;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.File;

public class RandomNFPAssignerTest {

    @Test
    public void test() throws Exception {
        final String askTpl = "" +
                "PREFIX u: <" + Unserved.PREFIX + ">\n" +
                "PREFIX un: <" + UnservedN.PREFIX + ">\n" +
                "PREFIX xsd: <" + XSD.NS + ">\n" +
                "PREFIX rdf: <" + RDF.getURI()+ ">\n" +
                "ASK WHERE {\n" +
                "  <_:%1$s> u:nfp ?rt, ?tp.\n" +
                "  ?rt a un:ResponseTime.\n" +
                "  ?rt un:value \"%2$.3f\"^^xsd:double." +
                "  ?tp a un:Throughput.\n" +
                "  ?tp un:value \"%3$.3f\"^^xsd:double." +
                "}\n";
        final String singleNFPTpl = "" +
                "PREFIX u: <" + Unserved.PREFIX + ">\n" +
                "PREFIX un: <" + UnservedN.PREFIX + ">\n" +
                "PREFIX xsd: <" + XSD.NS + ">\n" +
                "PREFIX rdf: <" + RDF.getURI()+ ">\n" +
                "SELECT ?rt ?tp WHERE {\n" +
                "  <_:%1$s> u:nfp ?rt, ?tp.\n" +
                "  ?rt a un:ResponseTime.\n" +
                "  ?tp a un:Throughput.\n" +
                "}\n";

        File configFile = new ResourceExtractor().extract("benchmark/RandomNFPAssigner.config.json");
        try {
            RandomNFPAssigner assigner = RandomNFPAssigner.fromFile(configFile);
            Assert.assertEquals(assigner.getConfig().seed, new Long(666));
            Model m = ModelFactory.createDefaultModel();

            Resource r1 = m.createResource();
            assigner.appendTo(r1);
            Assert.assertTrue(QueryExecutionFactory.create(QueryFactory.create(
                    String.format(askTpl, r1.getId(), 501.230, 253.511)), m).execAsk());
            assigner.appendTo(r1);

            try (QueryExecution execution = QueryExecutionFactory.create(QueryFactory.create(String.format(singleNFPTpl, r1.getId())), m)) {
                ResultSet results = execution.execSelect();
                Assert.assertTrue(results.hasNext());
                results.next();
                Assert.assertFalse(results.hasNext());
            }

            CostInfo ci = new CostInfoParser().parse(r1);
            Assert.assertEquals(((ResponseTime)ci.get(ResponseTime.NAME)).getMilliseconds(), 501.230);
            Assert.assertEquals(((Throughput)ci.get(Throughput.NAME)).getValue(), 253.511);
        } finally {
            //noinspection ResultOfMethodCallIgnored
            configFile.delete();
        }
    }

}
package br.ufsc.lapesd.unserved.testbench.io_composer;

import br.ufsc.lapesd.unserved.testbench.input.RDFInput;
import br.ufsc.lapesd.unserved.testbench.input.RDFInputStream;
import br.ufsc.lapesd.unserved.testbench.model.Variable;
import org.apache.jena.riot.RDFFormat;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.function.Consumer;

public abstract class ConfigmateConvertedTestBase extends AbstractWscConvertedTestBase {
    public static RDFInput getConfigmateTurtle(String problemName, String baseName) {
        ClassLoader cl = WscConvertedTestBase.class.getClassLoader();
        return new RDFInputStream(cl.getResourceAsStream("configmate/" + problemName
                + "/" + baseName + ".ttl"),  RDFFormat.TURTLE);
    }

    @DataProvider
    public static Object[][] configmateProblemNames() {
        return new Object[][] {
                {"mma", "IOrchestrator.planTrip", mmaChecker()},
                {"discovery", "q1", null},
                {"mma-medium", "IOrchestrator.planTrip", null},
        };
    }

    private static Consumer<CheckInfo> mmaChecker() {
        return ci -> {
            Variable s1 = ci.results.stream().filter(v -> v.getURI().endsWith("wVars1")).findFirst().get();
            Variable s2 = ci.results.stream().filter(v -> v.getURI().endsWith("wVars2")).findFirst().get();
            Assert.assertNotEquals(s1.getResourceValue(), s2.getResourceValue());
        };
    }

    @Test(dataProvider = "configmateProblemNames")
    public void testProblem(String problemName, String goalName,
                            Consumer<CheckInfo> consumer) throws Exception {
        runTestProblem(getConfigmateTurtle(problemName, "taxonomy"),
                getConfigmateTurtle(problemName, "services"),
                getConfigmateTurtle(problemName,  goalName + "-known"),
                getConfigmateTurtle(problemName, goalName + "-wanted"),
                consumer);
    }
}

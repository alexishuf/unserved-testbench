package br.ufsc.lapesd.unserved.testbench.util;

import com.google.common.base.Preconditions;
import es.usc.citius.hipster.graph.GraphEdge;
import es.usc.citius.hipster.graph.HashBasedHipsterDirectedGraph;
import es.usc.citius.hipster.graph.HipsterDirectedGraph;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.*;
import java.util.function.BiFunction;
import java.util.stream.Collectors;


@Test
public class DStarLiteTests {

    private static class GraphBuilder {
        HashBasedHipsterDirectedGraph<Integer, Double> g = new HashBasedHipsterDirectedGraph<>();
        public GraphBuilder add(Integer vertex) {
            g.add(vertex);
            return this;
        }

        public GraphBuilder connect(Integer from, Integer to, Double value) {
            boolean hasEdge = false;
            for (GraphEdge<Integer, Double> e : g.outgoingEdgesOf(from))
                hasEdge |= e.getVertex2().equals(to);
            Preconditions.checkState(!hasEdge);
            g.add(from);
            g.add(to);
            g.connect(from, to, value);
            return this;
        }

        public HashBasedHipsterDirectedGraph<Integer, Double> build() {
            HashBasedHipsterDirectedGraph<Integer, Double> built = this.g;
            g = duplicateGraph(built);
            return built;
        }
    }

    private static HashBasedHipsterDirectedGraph<Integer, Double>
    duplicateGraph(HipsterDirectedGraph<Integer, Double> g) {
        HashBasedHipsterDirectedGraph<Integer, Double> dup = new HashBasedHipsterDirectedGraph<>();
        g.vertices().forEach(dup::add);
        g.edges().forEach(e -> dup.connect(e.getVertex1(), e.getVertex2(), e.getEdgeValue()));
        return dup;
    }

    private static class GraphChangeBuilder {
        private Set<Integer> changedSources = new HashSet<>();
        private List<Change> changes = new ArrayList<>();


        public GraphChangeBuilder disconnect(Integer from, Integer to) {
            changes.add(new Disconnect(from, to));
            changedSources.add(from);
            return this;
        }

        public GraphChangeBuilder connect(Integer from, Integer to, Double value) {
            disconnect(from, to);
            changes.add(new Connect(from, to, value));
            changedSources.add(from);
            return this;
        }

        public GraphChangeBuilder apply(HashBasedHipsterDirectedGraph<Integer, Double> g) {
            changes.forEach(c -> c.apply(g));
            return this;
        }

        public Collection<Integer> getChangedSources() {
            return changedSources;
        }

        private interface Change {
            void apply(HashBasedHipsterDirectedGraph<Integer, Double> g);
        }

        private static class Disconnect implements Change {
            private final Integer from, to;

            public Disconnect(Integer from, Integer to) {
                this.from = from;
                this.to = to;
            }

            @Override
            public void apply(HashBasedHipsterDirectedGraph<Integer, Double> g) {
                if (g.getConnected().containsKey(from)) {
                    g.getConnected().get(from).removeAll(g.getConnected().get(from).stream()
                            .filter(e -> e.getVertex2().equals(to)).collect(Collectors.toList()));
                }
                if (g.getConnected().containsKey(to)) {
                    g.getConnected().get(to).removeAll(g.getConnected().get(to).stream()
                            .filter(e -> e.getVertex1().equals(from)).collect(Collectors.toList()));
                }
            }
        }

        private static class Connect implements Change {
            private final Integer from, to;
            private final Double value;

            public Connect(Integer from, Integer to, Double value) {
                this.from = from;
                this.to = to;
                this.value = value;
            }

            @Override
            public void apply(HashBasedHipsterDirectedGraph<Integer, Double> g) {
                g.add(from);
                g.add(to);
                g.connect(from, to, value);
            }
        }

    }

    private static class HipsterDirectedGraphAccessor<V, E> implements DStarLite.GraphAccessor<V, E> {

        private final HipsterDirectedGraph<V, E> g;
        private final E infinity;
        public HipsterDirectedGraphAccessor(HipsterDirectedGraph<V, E> g, E infinity) {
            this.g = g;
            this.infinity = infinity;
        }

        @Override
        public Collection<V> successors(@Nonnull V v) {
            List<V> list = new ArrayList<V>();
            g.outgoingEdgesOf(v).forEach(e -> list.add(e.getVertex2()));
            HashSet<V> set = new HashSet<>(list);
            if (set.size() != list.size())
                throwMultipleEdges();
            return set;
        }

        @Override
        public Collection<V> predecessors(@Nonnull V v) {
            List<V> list = new ArrayList<V>();
            g.incomingEdgesOf(v).forEach(e -> list.add(e.getVertex1()));
            HashSet<V> set = new HashSet<>(list);
            if (set.size() != list.size())
                throwMultipleEdges();
            return set;
        }

        @Override
        public E cost(@Nonnull V predecessor, @Nonnull V successor) {
            E cost = null;
            for (GraphEdge<V, E> edge : g.outgoingEdgesOf(predecessor)) {
                if (edge.getVertex2().equals(successor)) {
                    if (cost != null)
                        throwMultipleEdges();
                    cost = edge.getEdgeValue();
                }
            }
            return cost == null ? infinity : cost;
        }

        private void throwMultipleEdges() {
            throw new IllegalStateException("The graph has multiple edges among vertices!");
        }
    }

    private static BiFunction<Integer, Integer, Double>
    hBFS(HipsterDirectedGraph<Integer, Double> g) {
        return (from, to) -> {
            Set<Integer> visited = new HashSet<>();
            Queue<ImmutablePair<Integer, Integer>> queue = new LinkedList<>();
            queue.add(ImmutablePair.of(from, 0));
            while (!queue.isEmpty()) {
                ImmutablePair<Integer, Integer> step = queue.remove();
                if (visited.contains(step.left)) continue;
                visited.add(step.left);
                if (step.left.equals(to))
                    return (double)step.right;
                g.outgoingEdgesOf(step.left).forEach(e ->
                        queue.add(ImmutablePair.of(e.getVertex2(), step.right+1)));
            }
            return Double.POSITIVE_INFINITY;
        };

    }

    private static BiFunction<Integer, Integer, Double> hL(int base) {
        return (a, b) -> {
            int aLayer = (a - (a % base)) / base, bLayer = (b - (b % base)) / base;
            return (double) (bLayer >= aLayer ? bLayer - aLayer : aLayer);
        };
    }

    @DataProvider
    public static Object[][] findPathsData() {
        BiFunction<Integer, Integer, Double> h0 = (a, b) -> 0.0;

        List<HashBasedHipsterDirectedGraph<Integer, Double>> gs = new ArrayList<>();
        GraphBuilder builder = new GraphBuilder();
        gs.add(builder.add(1).add(2).build());                                             //0
        gs.add(builder.connect(1, 2, 1.0).build());                                        //1
        gs.add(builder.connect(2, 3, 1.0).build());                                        //2
        gs.add(builder.connect(4, 1, 1.0).connect(5, 1, 4.0).build());                     //3
        gs.add(builder.connect(4, 5, 1.0).connect(5, 4, 4.0).build());                     //4
        gs.add(builder.connect(1, 1, 1.0).build());                                        //5
        gs.add(builder.connect(2, 2, 1.0).build());                                        //6
        gs.add(builder.connect(2, 1, 1.0).build());                                        //7
        gs.add(builder.connect(2, 6, 1.0).connect(6, 7, 1.0).connect(3, 7, 1.0).build());  //8
        gs.add(builder.connect(7, 8, 1.0).connect(2, 8, 1.0).build());                     //9

        builder = new GraphBuilder();
        gs.add(builder.connect(1, 2, 1.0).connect(2, 3, 2.0).connect(1, 3, 1.0).build());  //10
        gs.add(builder.connect(3, 4, 1.0).connect(1, 4, 2.0).build());                     //11
        gs.add(builder.connect(4, 5, 0.0).build());                                        //12
        gs.add(builder.connect(5, 5, 0.0).connect(5, 6, 1.0).build());                     //13
        gs.add(builder.connect(2, 1, 0.0).build());                                        //14

        HashBasedHipsterDirectedGraph<Integer, Double> rm2012 = new GraphBuilder()
                .connect( 0, 11, 1.0).connect( 0, 12, 2.0)
                .connect(11, 21, 1.0).connect(12, 22, 1.0)
                .connect(21, 31, 2.0).connect(22, 40, 1.0)
                .connect(31, 40, 1.0)
                .build();

        return new Object[][] {
                {gs.get( 0), h0, 1, 2,  true, null},
                {gs.get( 1), h0, 1, 2,  true, Arrays.asList(1, 2)},
                {gs.get( 2), h0, 2, 3,  true, Arrays.asList(2, 3)},
                {gs.get( 2), h0, 1, 3,  true, Arrays.asList(1, 2, 3)},
                {gs.get( 3), h0, 1, 3,  true, Arrays.asList(1, 2, 3)},
                {gs.get( 4), h0, 1, 3,  true, Arrays.asList(1, 2, 3)},
                {gs.get( 5), h0, 1, 3,  true, Arrays.asList(1, 2, 3)},
                {gs.get( 6), h0, 1, 3,  true, Arrays.asList(1, 2, 3)},
                {gs.get( 7), h0, 1, 3,  true, Arrays.asList(1, 2, 3)},
                {gs.get( 8), h0, 1, 3,  true, Arrays.asList(1, 2, 3)},
                {gs.get( 8), h0, 1, 7,  true, Arrays.asList(1, 2, 3, 7)},
                {gs.get( 8), h0, 1, 7,  true, Arrays.asList(1, 2, 6, 7)},
                {gs.get( 9), h0, 1, 8,  true, Arrays.asList(1, 2, 8)},
                {gs.get( 9), h0, 1, 8, false, Arrays.asList(1, 2, 6, 7, 8)},
                {gs.get( 9), h0, 1, 8, false, Arrays.asList(1, 2, 3, 7, 8)},

                {gs.get(10), h0, 1, 3,  true, Arrays.asList(1, 3)},
                {gs.get(10), h0, 1, 3, false, Arrays.asList(1, 2, 3)},
                {gs.get(11), h0, 1, 4,  true, Arrays.asList(1, 4)},
                {gs.get(11), h0, 1, 4,  true, Arrays.asList(1, 3, 4)},
                {gs.get(11), h0, 1, 4, false, Arrays.asList(1, 2, 3, 4)},
                {gs.get(12), h0, 1, 5,  true, Arrays.asList(1, 4, 5)},
                {gs.get(13), h0, 1, 6,  true, Arrays.asList(1, 4, 5, 6)},
                {gs.get(14), h0, 1, 6,  true, Arrays.asList(1, 4, 5, 6)},


                {gs.get( 0), hBFS(gs.get( 0)), 1, 2,  true, null},
                {gs.get( 1), hBFS(gs.get( 1)), 1, 2,  true, Arrays.asList(1, 2)},
                {gs.get( 2), hBFS(gs.get( 2)), 2, 3,  true, Arrays.asList(2, 3)},
                {gs.get( 2), hBFS(gs.get( 2)), 1, 3,  true, Arrays.asList(1, 2, 3)},
                {gs.get( 3), hBFS(gs.get( 3)), 1, 3,  true, Arrays.asList(1, 2, 3)},
                {gs.get( 4), hBFS(gs.get( 4)), 1, 3,  true, Arrays.asList(1, 2, 3)},
                {gs.get( 5), hBFS(gs.get( 5)), 1, 3,  true, Arrays.asList(1, 2, 3)},
                {gs.get( 6), hBFS(gs.get( 6)), 1, 3,  true, Arrays.asList(1, 2, 3)},
                {gs.get( 7), hBFS(gs.get( 7)), 1, 3,  true, Arrays.asList(1, 2, 3)},
                {gs.get( 8), hBFS(gs.get( 8)), 1, 3,  true, Arrays.asList(1, 2, 3)},
                {gs.get( 8), hBFS(gs.get( 8)), 1, 7,  true, Arrays.asList(1, 2, 3, 7)},
                {gs.get( 8), hBFS(gs.get( 8)), 1, 7,  true, Arrays.asList(1, 2, 6, 7)},
                {gs.get( 9), hBFS(gs.get( 9)), 1, 8,  true, Arrays.asList(1, 2, 8)},
                {gs.get( 9), hBFS(gs.get( 9)), 1, 8, false, Arrays.asList(1, 2, 6, 7, 8)},
                {gs.get( 9), hBFS(gs.get( 9)), 1, 8, false, Arrays.asList(1, 2, 3, 7, 8)},

                {gs.get(10), hBFS(gs.get(10)), 1, 3,  true, Arrays.asList(1, 3)},
                {gs.get(10), hBFS(gs.get(10)), 1, 3, false, Arrays.asList(1, 2, 3)},
                {gs.get(11), hBFS(gs.get(11)), 1, 4,  true, Arrays.asList(1, 4)},
                {gs.get(11), hBFS(gs.get(11)), 1, 4,  true, Arrays.asList(1, 3, 4)},
                {gs.get(12), hBFS(gs.get(12)), 1, 5,  true, Arrays.asList(1, 4, 5)},
                {gs.get(13), hBFS(gs.get(13)), 1, 6,  true, Arrays.asList(1, 4, 5, 6)},
                {gs.get(14), hBFS(gs.get(14)), 1, 6,  true, Arrays.asList(1, 4, 5, 6)},

                {rm2012, hL(10), 0, 40, true, Arrays.asList(0, 12, 22, 40)},
                {rm2012, hL(10), 0, 40, false, Arrays.asList(0, 11, 21, 31, 40)}
        };
    }

    @DataProvider
    public static Object[][] walkPathsData() {
        Object[][] source = findPathsData();
        List<Object[]> list = new ArrayList<>();
        for (Object[] row : source) {
            if (!((boolean) row[4]) || row[5] == null) continue;
            list.add(new Object[]{row[0], row[1], row[5]});
        }

        Object[][] array = new Object[list.size()][3];
        for (int i = 0; i < list.size(); i++) array[i] = list.get(i);
        return array;
    }

    @DataProvider
    public static Object[][] findPathsWithChangesData() {
        BiFunction<Integer, Integer, Double> h0 = (a, b) -> 0.0;
        List<HipsterDirectedGraph<Integer, Double>> gs = new ArrayList<>();
        List<GraphChangeBuilder> cbs = new ArrayList<>();
        GraphBuilder gb = new GraphBuilder();
        gs.add(gb.connect(1, 2, 1.0).connect(2, 3, 1.0).connect(2, 4, 1.0).connect(4, 3, 1.0)
                .build());                                                                     //0
        gs.add(gb.connect(4, 5, 1.0).connect(3, 5, 1.0).build());                              //1
        gs.add(gb.connect(5, 6, 1.0).build());                                                 //2
        cbs.add(new GraphChangeBuilder().connect(2, 3, 3.0));                                  //0
        cbs.add(new GraphChangeBuilder().disconnect(2, 3));                                    //1
        cbs.add(new GraphChangeBuilder().disconnect(4, 5));                                    //2

        gs.add(new GraphBuilder().connect(1, 2, 1.0).connect(2, 3, 1.0)
                .connect(3, 4, 1.0).build());                                                  //3
        cbs.add(new GraphChangeBuilder().disconnect(1, 2).connect(1, 10, 1.0)
                .connect(10, 3, 1.0));                                                         //3
        cbs.add(new GraphChangeBuilder().disconnect(1, 2).connect(1, 10, 0.0)
                .connect(10, 3, 0.0));                                                         //4
        cbs.add(new GraphChangeBuilder().disconnect(1, 2).connect(1, 10, 0.0)
                .connect(10, 1, 0.0).connect(10, 3, 0.0));                                     //5

        return new Object[][] {
                {gs.get(0), h0, 1, 3, true, Arrays.asList(1, 2, 3),
                        cbs.get(0), true, Arrays.asList(1, 2, 4, 3)},
                {gs.get(0), h0, 1, 3, true, Arrays.asList(1, 2, 3),
                        cbs.get(1), true, Arrays.asList(1, 2, 4, 3)},
                {gs.get(1), h0, 1, 5, true, Arrays.asList(1, 2, 4, 5),
                        cbs.get(2), true, Arrays.asList(1, 2, 3, 5)},
                {gs.get(2), h0, 1, 6, true, Arrays.asList(1, 2, 4, 5, 6),
                        cbs.get(2), true, Arrays.asList(1, 2, 3, 5, 6)},
                {gs.get(3), h0, 1, 4, true, Arrays.asList(1, 2, 3, 4),
                        cbs.get(3), true, Arrays.asList(1, 10, 3, 4)},
                {gs.get(3), h0, 1, 4, true, Arrays.asList(1, 2, 3, 4),
                        cbs.get(4), true, Arrays.asList(1, 10, 3, 4)},
                {gs.get(3), h0, 1, 4, true, Arrays.asList(1, 2, 3, 4),
                        cbs.get(5), true, Arrays.asList(1, 10, 3, 4)}
        };
    }

    private void checkPath(boolean positive, @Nullable List<Integer> path,
                           DStarLite.Result<Integer> result) {
        if (positive)
            Assert.assertEquals(result.hasPath(), (path != null));
        if (!positive && path == null)
            Assert.assertFalse(result.hasPath());
        if (path != null)
            Assert.assertEquals(result.getPaths().contains(path), positive);

        /* no path ever has repeated steps */
        Assert.assertTrue(result.getPaths().stream()
                .allMatch(list -> new HashSet<>(list).size() == list.size()));
    }

    @Test(dataProvider = "findPathsData")
    public void testFindPaths(@Nonnull HipsterDirectedGraph<Integer, Double> graph,
                              @Nonnull BiFunction<Integer, Integer, Double> h,
                              int from, int to, boolean positive, @Nullable List<Integer> path) {
        DStarLite<Integer, Double> dsl = DStarLite.withDoubleCost()
                .withGraphAccessor(new HipsterDirectedGraphAccessor<>(graph,
                        Double.POSITIVE_INFINITY))
                .withGoal(to).withHeuristic(h).build();
        checkPath(positive, path, dsl.runFrom(from).getResult());
    }

    @Test(dataProvider = "findPathsData")
    public void testFindPathsNullChanges(@Nonnull HipsterDirectedGraph<Integer, Double> graph,
                                         @Nonnull BiFunction<Integer, Integer, Double> h,
                                         int from, int to, boolean positive,
                                         @Nullable List<Integer> path) {
        DStarLite<Integer, Double> dsl = DStarLite.withDoubleCost()
                .withGraphAccessor(new HipsterDirectedGraphAccessor<>(graph,
                        Double.POSITIVE_INFINITY))
                .withGoal(to).withHeuristic(h).build();
        checkPath(positive, path, dsl.runFrom(from).getResult());
        dsl.notifyChangedEdgeSources(Collections.emptyList());
        checkPath(positive, path, dsl.runFrom(from).getResult());
    }

    @Test(dataProvider = "walkPathsData")
    public void testWalkPaths(@Nonnull HipsterDirectedGraph<Integer, Double> graph,
                              @Nonnull BiFunction<Integer, Integer, Double> h,
                              @Nonnull List<Integer> path) {
        DStarLite<Integer, Double> dsl = DStarLite.withDoubleCost()
                .withGraphAccessor(new HipsterDirectedGraphAccessor<>(graph,
                        Double.POSITIVE_INFINITY))
                .withGoal(path.get(path.size()-1)).withHeuristic(h).build();
        for (int i = 0; i < path.size(); i++) {
            List<Integer> remainder = new ArrayList<>();
            for (int j = i; j < path.size(); j++) remainder.add(path.get(j));
            checkPath(true, remainder, dsl.runFrom(path.get(i)).getResult());
        }
    }

    @Test(dataProvider = "findPathsWithChangesData")
    public void testFindPathsWithChanges(@Nonnull HashBasedHipsterDirectedGraph<Integer, Double> graph,
                                         @Nonnull BiFunction<Integer, Integer, Double> h,
                                         int from, int to, boolean positive,
                                         @Nullable List<Integer> path,
                                         @Nonnull GraphChangeBuilder changeBuilder,
                                         boolean positiveAfter,
                                         @Nullable List<Integer> pathAfter) {
        HashBasedHipsterDirectedGraph<Integer, Double> graphCopy = duplicateGraph(graph);
        DStarLite<Integer, Double> dsl = DStarLite.withDoubleCost()
                .withGraphAccessor(new HipsterDirectedGraphAccessor<>(graphCopy,
                        Double.POSITIVE_INFINITY))
                .withGoal(to).withHeuristic(h).build();
        checkPath(positive, path, dsl.runFrom(from).getResult());
        changeBuilder.apply(graphCopy);
        dsl.notifyChangedEdgeSources(changeBuilder.getChangedSources());
        checkPath(positiveAfter, pathAfter, dsl.runFrom(from).getResult());
    }
}

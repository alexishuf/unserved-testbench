package br.ufsc.lapesd.unserved.testbench.util;

import br.ufsc.lapesd.unserved.testbench.Utils;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.riot.Lang;
import org.apache.jena.vocabulary.RDFS;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import javax.annotation.Nonnull;
import java.util.*;
import java.util.stream.Collectors;

import static java.lang.String.join;
import static java.util.Collections.nCopies;

public class FixedCacheTransitiveClosureGetterTest {

    @DataProvider
    public static Object[][] treeData() {
        //                    0
        //         00                  01
        //    000       001       010       011
        // 0000 0001 0010 0011 0100 0101 0110 0111
        final boolean f = false, t = true;
        return new Object[][] {
                {4, 4, 3, f}, {4, 4, 7, f}, {4, 4, 15, f}, {4, 4, 1, f}, {4, 4, 2, f},
                {4, 3, 3, f}, {4, 3, 7, f}, {4, 3, 15, f}, {4, 3, 1, f}, {4, 3, 2, f},
                {4, 2, 3, f}, {4, 2, 7, f}, {4, 2, 15, f}, {4, 2, 1, f}, {4, 2, 2, f},

                {4, 4, 3, t}, {4, 4, 7, t}, {4, 4, 15, t}, {4, 4, 1, t}, {4, 4, 2, t},
                {4, 3, 3, t}, {4, 3, 7, t}, {4, 3, 15, t}, {4, 3, 1, t}, {4, 3, 2, t},
                {4, 2, 3, t}, {4, 2, 7, t}, {4, 2, 15, t}, {4, 2, 1, t}, {4, 2, 2, t},
        };
    }

    private static @Nonnull Resource node(@Nonnull Model model, @Nonnull String address) {
        return model.createResource("http://example.org/ns#" + address);
    }

    private static @Nonnull Set<Resource> nodes(@Nonnull Model model, @Nonnull String... addresses) {
        return Arrays.stream(addresses)
                .map(a -> model.createResource("http://example.org/ns#" + a))
                .collect(Collectors.toSet());
    }

    private static @Nonnull Model generateTree(int levels, int rightHeight, boolean selfSubClass) {
        Model model = ModelFactory.createDefaultModel();
        Queue<String> queue = new LinkedList<>();
        queue.add("0");
        while (!queue.isEmpty()) {
            String parentAddress = queue.remove();
            Resource parent = node(model, parentAddress);
            if (selfSubClass)
                parent.addProperty(RDFS.subClassOf, parent);
            if (parentAddress.length() >= levels)
                return model;
            node(model, parentAddress + "0").addProperty(RDFS.subClassOf, parent);
            queue.add(parentAddress + "0");
            if (parentAddress.length() < rightHeight) {
                node(model, parentAddress + "1").addProperty(RDFS.subClassOf, parent);
                queue.add(parentAddress + "1");
            }
        }
        return model;
    }

    @Test(dataProvider = "treeData")
    public void testTree(int height, int rightHeight, int threshold, boolean selfSubClass) {
        Model m = generateTree(height, rightHeight, selfSubClass);
        FixedCacheTransitiveClosureGetter.Builder builder = FixedCacheTransitiveClosureGetter
                .backward(RDFS.subClassOf).setThreshold(threshold).visit(node(m, "0"));
        FixedCacheTransitiveClosureGetter getter = builder.build();
        Map<Resource, Integer> counts = builder.getCountEstimates();

        Queue<String> queue = new LinkedList<>();
        queue.add("0");
        while (!queue.isEmpty()) {
            String address = queue.remove();
            Resource resource = node(m, address);
            if (!m.containsResource(resource)) continue;

            SortedSet<String> expected = BFSBGPIterator.from(resource).withInitial()
                    .backward(RDFS.subClassOf).toStream().map(Resource::toString)
                    .collect(Collectors.toCollection(TreeSet::new));
            SortedSet<String> actual = getter.getClosureStream(resource).map(Resource::toString)
                    .collect(Collectors.toCollection(TreeSet::new));
            Assert.assertEquals(actual, expected, "resource="+resource);

            int estimate = counts.getOrDefault(resource, 0);
            Assert.assertEquals(estimate, actual.size(), "resource="+resource);

            queue.add(address + "0");
            queue.add(address + "1");
        }
    }

    @Test
    public void testUpdateResultHasNoEffect() {
        Model m = generateTree(4, 4, false);
        FixedCacheTransitiveClosureGetter getter = FixedCacheTransitiveClosureGetter
                .backward(RDFS.subClassOf).setThreshold(3)
                .visit(node(m, "0")).build();
        HashSet<Resource> set0 = new HashSet<>(getter.getClosureSet(node(m, "0")));
        HashSet<Resource> set00 = new HashSet<>(getter.getClosureSet(node(m, "00")));
        try {
            getter.getClosureSet(node(m, "0")).remove(node(m, "01"));
            getter.getClosureSet(node(m, "00")).remove(node(m, "001"));
        } catch (UnsupportedOperationException ex) { /* an implementation MAY throw */ }
        Assert.assertEquals(getter.getClosureSet(node(m, "0")), set0);
        Assert.assertEquals(getter.getClosureSet(node(m, "00")), set00);
    }


    @Test(dataProvider = "treeData")
    public void testIsCached(int height, int rightHeight, int threshold, boolean selfSubClass) {
        Model m = generateTree(height, rightHeight, selfSubClass);
        FixedCacheTransitiveClosureGetter getter = FixedCacheTransitiveClosureGetter
                .backward(RDFS.subClassOf).setThreshold(threshold)
                .visit(node(m, "0")).build();
        double log2d = Math.log(threshold)/Math.log(2);
        int log2 = (int)Math.floor(log2d);

        Set<Resource> allNodes = BFSBGPIterator.from(node(m, "0")).withInitial()
                .backward(RDFS.subClassOf).toCollection(HashSet::new);
        int nodeCount = allNodes.size();

        if (threshold >= nodeCount) {
            Assert.assertTrue(allNodes.stream().noneMatch(getter::isCached));
        } else if (log2 == log2d && log2 > 0 && height == rightHeight) {
            Resource cached = node(m, join("", nCopies(height - log2, "0")));
            if (!m.listSubjectsWithProperty(RDFS.subClassOf, cached).hasNext()) {
                Assert.assertFalse(getter.isCached(cached)); //leafs are never cached
            } else {
                Assert.assertTrue(getter.isCached(cached));
                Set<Resource> set = new HashSet<>(getter.getClosureSet(cached));
                Assert.assertTrue(set.remove(cached));
                Assert.assertTrue(set.stream().noneMatch(getter::isCached));
            }
        } else {
            Assert.assertTrue(allNodes.stream().anyMatch(getter::isCached));
        }
    }

    @Test
    public void testBuildWithLoops() throws Exception {
        Model m = Utils.resourceInput("io_composer/conditions/index" +
                "/VariableSpecIndexTest-findSubclass.ttl", Lang.TTL).takeModel();
        FixedCacheTransitiveClosureGetter getter = FixedCacheTransitiveClosureGetter
                .backward(RDFS.subClassOf).setThreshold(2).build();
        Assert.assertEquals(getter.getClosureSet(node(m, "F")),
                nodes(m, "F", "G", "H"));
        Assert.assertEquals(getter.getClosureSet(node(m, "I")),
                nodes(m, "I", "J", "K"));
        Assert.assertEquals(getter.getClosureSet(node(m, "I")),
                nodes(m, "I", "J", "K"));
        Assert.assertEquals(getter.getClosureSet(node(m, "C")),
                nodes(m, "C", "I", "J", "K"));
        Assert.assertEquals(getter.getClosureSet(node(m, "A")),
                nodes(m, "A", "B", "C", "D", "E", "G", "I", "J", "K"));
    }
}
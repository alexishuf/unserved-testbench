package br.ufsc.lapesd.unserved.testbench;

import br.ufsc.lapesd.unserved.testbench.hydra.Hydra;
import br.ufsc.lapesd.unserved.testbench.hydra.HydraFrontend;
import br.ufsc.lapesd.unserved.testbench.input.RDFInput;
import br.ufsc.lapesd.unserved.testbench.iri.HTTP;
import br.ufsc.lapesd.unserved.testbench.iri.Unserved;
import br.ufsc.lapesd.unserved.testbench.iri.UnservedO;
import br.ufsc.lapesd.unserved.testbench.iri.UnservedX;
import br.ufsc.lapesd.unserved.testbench.reasoner.N3ReasonerException;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.riot.Lang;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.XSD;
import org.spinrdf.vocabulary.SP;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.IOException;

@Test
public class HydraFrontendTests {

    @Test
    public class ApiDemoTest {
        private RDFInput result;
        private Model model;

        @BeforeClass
        public void setUp() throws N3ReasonerException, UnservedTranslatorException, IOException {
            HydraFrontend translator = new HydraFrontend();
//            HydraFrontend translator = new HydraFrontend(
//                    new RiotEnhancedN3ReasonerFactory(new EYEProcessFactory()));
            String path = "unserved/hydra/api-demo.jsonld";
            result = translator.compile(Utils.resourceInput(path, Lang.JSONLD));
            model = result.getModel();
        }

        @AfterClass
        public void tearDown() throws IOException {
            result.close();
        }

        @Test
        public void testMessagesGenerated() {
            StmtIterator it = model.listStatements(null, RDF.type, Unserved.Message);
            final long count[] = {0};
            it.forEachRemaining(s -> ++count[0]);
            Assert.assertTrue(count[0] > 0, String.format("%1$d>0", count[0]));
        }

        @Test
        public void testMessagesWithReactions() {
            ResultSet results = QueryExecutionFactory.create(
                    QueryFactory.create("PREFIX unserved: <" + Unserved.PREFIX + ">\n" +
                            "SELECT * WHERE {\n" +
                            "  ?msg a unserved:Message.\n" +
                            "  ?msg unserved:when ?when.\n" +
                            "  ?when unserved:reactionTo ?other.\n" +
                            "  ?other a unserved:Message.\n" +
                    "}\n"), model).execSelect();
            Assert.assertTrue(results.hasNext());
        }

        @Test
        public void testGetEntryPoint() {
            ResultSet results = QueryExecutionFactory.create(QueryFactory.create(
                    "PREFIX u: <" + Unserved.PREFIX + ">\n" +
                            "PREFIX uo: <" + UnservedO.PREFIX + ">\n" +
                            "PREFIX sp: <" + SP.NS + ">\n" +
                            "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>\n" +
                            "PREFIX hm: <" + HTTP.Methods.PREFIX + ">\n" +
                            "PREFIX vocab: <http://www.markus-lanthaler.com/hydra/api-demo/vocab#>\n" +
                            "\n" +
                            "SELECT ?m WHERE {\n" +
                            "  ?req a u:Message.\n" +
                            "  ?req  u:part ?reqPart.\n" +
                            "  ?reqPart u:variable ?in.\n" +
                            "  ?in u:type vocab:EntryPoint.\n" +
                            "  ?in u:representation xsd:anyURI.\n" +
                            "  ?rep a u:Message.\n" +
                            "  ?rep u:when ?when.\n" +
                            "  ?when u:reactionTo ?req.\n" +
                            "  ?rep u:part ?repPart.\n" +
                            "  ?repPart u:variable ?out.\n" +
                            "  ?out u:type vocab:EntryPoint.\n" +
                            "  ?out u:representation u:Resource.\n" +
                            "  ?rep u:part/u:variable ?getVar.\n" +
                            "  ?getVar u:type hm:GET.\n" +
                            "  ?getVar u:representation uo:OperationRepresentation.\n" +
                            "  ?rep u:condition/sp:subject ?getVar.\n" +
                            "  ?rep u:condition/sp:predicate uo:operatesOn.\n" +
                            "  ?rep u:condition/sp:object/^u:variable/^u:part ?req.\n" +
                            "}\n"), model).execSelect();
            Assert.assertTrue(results.hasNext());
        }

        @Test
        public void testNoChaff() {
            Resource Chaff = ResourceFactory.createResource("http://www.w3.org/2000/10/swap/log#Chaff");
            Assert.assertFalse(model.listStatements(null, RDF.type, Chaff).hasNext());
        }

        private String prefixes = "PREFIX unserved:<" + Unserved.PREFIX + ">\n" +
                "PREFIX ux-http: <" + UnservedX.HTTP.PREFIX + ">\n" +
                "PREFIX ux-rdf: <" + UnservedX.RDF.PREFIX + ">\n" +
                "PREFIX http: <" + HTTP.PREFIX + ">\n" +
                "PREFIX http-methods: <" + HTTP.Methods.PREFIX + ">\n" +
                "PREFIX hydra: <" + Hydra.PREFIX + ">\n" +
                "PREFIX xsd: <" + XSD.NS + ">\n" +
                "PREFIX vocab: <http://www.markus-lanthaler.com/hydra/api-demo/vocab#>\n";

        private void ask(String sparql) {
            QueryExecution execution = QueryExecutionFactory.create(
                    QueryFactory.create(sparql), model);
            Assert.assertTrue(execution.execAsk());
            execution.close();
        }

        @Test
        public void testHttpRequestResponse() {
            ask(prefixes +
                    "ASK WHERE {\n" +
                    "  ?req a unserved:Message.\n" +
                    "  ?rep a unserved:Message.\n" +
                    "  ?rep unserved:when ?when.\n" +
                    "  ?when unserved:reactionTo ?req.\n" +
                    "  ?req a ux-http:HttpRequest.\n" +
                    "  ?req a http:Request.\n" +
                    "  ?rep a ux-http:HttpResponse.\n" +
                    "  ?rep a http:Response.\n" +
                    "}\n"
            );
        }

        @Test
        public void testHTTPMethodStringMapped() {
            ask(prefixes +
                    "ASK WHERE {\n" +
                    "  ?msg a http:Request.\n" +
                    "  ?msg unserved:part ?part.\n" +
                    "  ?part unserved:variable ?var.\n" +
                    "  ?var unserved:type vocab:User.\n" +
                    "  ?msg http:mthd http-methods:PUT.\n" +
                    "}"
            );
        }

        @Test
        public void testLinkAsVariable() {
            ask(prefixes + "ASK WHERE {\n" +
                    "  ?msg a http:Response.\n" +
                    "  ?msg a unserved:Message.\n" +
                    "  ?msg unserved:part ?part1.\n" +
                    "  ?part1 unserved:variable ?userVar.\n" +
                    "  ?userVar unserved:type vocab:User.\n" +
                    "  ?userVar unserved:part ?part2.\n" +
                    "  ?part2 unserved:partModifier ?part2Mod.\n" +
                    "  ?part2Mod a ux-rdf:AsProperty.\n" +
                    "  ?part2Mod ux-rdf:property <http://www.markus-lanthaler.com/hydra/api-demo/vocab#User/raisedIssues>.\n" +
                    "  ?part2Mod ux-rdf:propertyOf ?userVar.\n" +
                    "  ?part2 unserved:variable ?raisedVar.\n" +
                    "  ?raisedVar unserved:type hydra:Collection.\n" +
                    "}\n"
            );
        }
    }
}
